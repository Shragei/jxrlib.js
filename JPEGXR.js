var JPEGXR=(function(){
  var PixelFormat={
    '0D':{
      Name:'24bppRGB',
      Channels:3,
      ChannelType:'Uint8',
      ChannelOrder:'RGB',
      Padding:0,
      Alpha:false
    },
    '0C':{
      Name:'24bppGBR',
      Channels:3,
      ChannelType:'Uint8',
      ChannelOrder:'BGR',
      Padding:0,
      Alpha:false
    },
    '0E':{
      Name:'32bppGBR',
      Channels:3,
      ChannelType:'Uint8',
      ChannelOrder:'BGR',
      Padding:1,
      Alpha:false
    },
    '15':{
      Name:'48bppRGB',
      Channels:3,
      ChannelType:'Uint16',
      ChannelOrder:'RGB',
      Padding:0,
      Alpha:false
    },
    '12':{
      Name:'48bppRGBFixedPoint',
      Channels:3,
      ChannelType:'Int16',
      ChannelOrder:'RGB',
      Padding:0,
      Alpha:false
    },
    '3B':{
      Name:'48bppRGBHalf',
      Channels:3,
      ChannelType:'Float16',
      ChannelOrder:'RGB',
      Padding:0,
      Alpha:false
    },
    '18':{
      Name:'96bppRGBFixedPoint',
      Channels:3,
      ChannelType:'Int32',
      ChannelOrder:'RGB',
      Padding:0,
      Alpha:false
    },
    '40':{
      Name:'64bppRGBFixedPoint',
      Channels:3,
      ChannelType:'Int16',
      ChannelOrder:'RGB',
      Padding:0,
      Alpha:false
    },
    '42':{
      Name:'64bppRGBHalf',
      Channels:3,
      ChannelType:'Float16',
      ChannelOrder:'RGB',
      Padding:1,
      Alpha:false
    },
    '41':{
      Name:'128bppRGBFixedPoint',
      Channels:3,
      ChannelType:'Int32',
      ChannelOrder:'RGB',
      Padding:1,
      Alpha:false
    },
    '1B':{
      Name:'128bppRGBFloat',
      Channels:3,
      ChannelType:'Float32',
      ChannelOrder:'RGB',
      Padding:1,
      Alpha:false
    },
    '0F':{
      Name:'32bppRGBA',
      Channels:4,
      ChannelType:'Uint8',
      ChannelOrder:'RGBA',
      Padding:0,
      Alpha:true
    },
    '16':{
      Name:'64bppRGBA',
      Channels:4,
      ChannelType:'Uint16',
      ChannelOrder:'RGBA',
      Padding:0,
      Alpha:true
    },
    '1D':{
      Name:'64bppRGBAFixedPoint',
      Channels:4,
      ChannelType:'Int16',
      ChannelOrder:'RGBA',
      Padding:0,
      Alpha:true
    },
    '3A':{
      Name:'64bppRGBAHalf',
      Channels:4,
      ChannelType:'Float16',
      ChannelOrder:'RGBA',
      Padding:0,
      Alpha:true
    },
    '1E':{
      Name:'128bppRGBAFixedPoint',
      Channels:4,
      ChannelType:'Int32',
      ChannelOrder:'RGBA',
      Padding:0,
      Alpha:true
    },
    '19':{
      Name:'128bppRGBAFloat',
      Channels:4,
      ChannelType:'Float32',
      ChannelOrder:'RGBA',
      Padding:0,
      Alpha:true
    },
    '10':{
      Name:'32bppPBGRA',
      Channels:4,
      ChannelType:'Uint8',
      ChannelOrder:'RGBA',
      Padding:0,
      Alpha:true
    },
    '17':{
      Name:'64bppPBGRA',
      Channels:4,
      ChannelType:'Uint16',
      ChannelOrder:'BGRA',
      Padding:0,
      Alpha:true
    },
    '1A':{
      Name:'128bppPBGRAFloat',
      Channels:4,
      ChannelType:'Float32',
      ChannelOrder:'BGRA',
      Padding:0,
      Alpha:true
    },
    '08':{
      Name:'8bppGray',
      Channels:1,
      ChannelType:'Uint8',
      ChannelOrder:'Y',
      Padding:0,
      Alpha:false
    },
    '0B':{
      Name:'16bppGray',
      Channels:1,
      ChannelType:'Uint16',
      ChannelOrder:'Y',
      Padding:0,
      Alpha:false
    },
    '13':{
      Name:'16bppGrayFixedPoint',
      Channels:1,
      ChannelType:'Int16',
      ChannelOrder:'Y',
      Padding:0,
      Alpha:false
    },
    '3E':{
      Name:'16bppGrayHalf',
      Channels:1,
      ChannelType:'Float16',
      ChannelOrder:'Y',
      Padding:0,
      Alpha:false
    },
    '3F':{
      Name:'32bppGrayFixedPoint',
      Channels:1,
      ChannelType:'Int32',
      ChannelOrder:'Y',
      Padding:0,
      Alpha:false
    },
    '11':{
      Name:'32bppGrayFloat',
      Channels:1,
      ChannelType:'Float32',
      ChannelOrder:'Y',
      Padding:0,
      Alpha:false
    },
    '3D':{
      Name:'32bppRGBE',
      Channels:3,
      ChannelType:'Uint8',
      ChannelOrder:'RGBE',
      Padding:0,
      Alpha:false
    }
  };

  var worker=new Worker("worker.js");
  var images={};
  worker.onerror = function(event){
      throw new Error(event.message + " (" + event.filename + ":" + event.lineno + ")");
  };
  
  worker.addEventListener('message',function(event){
    var message=event.data;
    if(message.action=='ready') //eat the ready event
      return;
    if(message.action=='stdout'||message.action=='stderr'){
      console.log(message.line);
      return;
    }

    if(!(message.id in images)){
      throw new Error("JPEGXR: congruence error");
    }else{
      images[message.id].func(message);
    }
  });
  
  function jxr(url){
    var Meta={Resolution:{},Image:{},Size:{}};
    var postDecode=false;
    var buffer;
    var id;
    do{
      id=Math.floor(Math.random()*10000);
    }while(id in images);
 
    var proc=new Processor(url);

    proc.onload=function(proc){

      var tiff=new TIFF();
      tiff.Tags('0xBC01',function(type,count,value){
        var type=value[count-1].toString(16);
        type=type.length==1?'0'+type:type;
        if(type in PixelFormat)
          Meta.PixelFormat=PixelFormat[type];
        else
          Meta.PixelFormat=type;
      });
      tiff.Tags('0xBC02',function(type,count,value){
        Meta.SpatialTransform=value[0];
      });
      tiff.Tags('0xBC80',function(type,count,value){
        Meta.Size.Width=value[0];
      });
      tiff.Tags('0xBC81',function(type,count,value){
        Meta.Size.Height=value[0];
      });
      tiff.Tags('0xBC82',function(type,count,value){
        Meta.Resolution.Width=value[0];
      });
      tiff.Tags('0xBC83',function(type,count,value){
        Meta.Resolution.Height=value[0];
      });
      tiff.Tags('0xBCC0',function(type,count,value){
        Meta.Image.Offset=value[0];
      });
      tiff.Tags('0xBCC1',function(type,count,value){
        Meta.Image.Size=value[0];
      });
      tiff.Tags('0xBCC4',function(type,count,value){
        Meta.Image.Presence=value[0];
      });
    /*  tiff.Tags('0xBCC0',function(type,count,value){
        if(!Meta.Alpha)
          Meta.Alpha={};
        Meta.Alpha.Offset=value[0];
      });
      tiff.Tags('0xBCC1',function(type,count,value){
        if(!Meta.Alpha)
          Meta.Alpha={};
        Meta.Alpha.Size=value[0];
      });
      tiff.Tags('0xBCC4',function(type,count,value){
        if(!Meta.Alpha)
          Meta.Alpha={};
        Meta.Alpha.Presence=value[0];
      });*/
      
      tiff.execute(proc);
      proc.seek(Meta.Image.Offset,true);
      
      images[id]={};
      images[id].stream=new Uint8Array(proc.slice(Meta.Image.Size,false));

      images[id].func=function(message){
        switch(message.action){
          case 'stdout':
            console.log(message);
            break;

          case 'decode':
            if(message.ret){
              postDecode=true;
              this.buffer=message.image;
              this.meta=message.meta;
              this.height=this.meta.info.cHeight;
              this.width=this.meta.info.cWidth;
//              console.log('Decode time in ms: '+message.runtime);
            }else{
              this.cleanup();
              throw new Error('JPEGXR: Decode Error');
            }
            delete images[id];
            //While the image buffer is being used, decode the next image so it is waiting
            if(Object.keys(images).length>0){
              var nid=Object.keys(images)[0];
              worker.postMessage({action:'decode',buffer:images[nid].stream,id:nid});
            }
            
            if(message.ret)  
              this.onload(this.buffer);
              
            break;
          default:
            console.log(message);
        }
      }.bind(this);
      
      if(Object.keys(images).length==1){
        worker.postMessage({action:'decode',buffer:images[id].stream,id:id});
      }
    }.bind(this);
    
    this.execute=function(){
      if(typeof this.onload != 'function')
        throw new Error('JPEGXR: No onload callback function');
      proc.execute();
    }
    
    this.cleanup=function(){
     //if(!(message.id in images)){
    //  worker.postMessage({action:'cleanup',id:id});
    }
  }
  
  
  return jxr;
}());