// The Module object: Our interface to the outside world. We import
// and export values on it, and do the work to get that through
// closure compiler if necessary. There are various ways Module can be used:
// 1. Not defined. We create it here
// 2. A function parameter, function(Module) { ..generated code.. }
// 3. pre-run appended it, var Module = {}; ..generated code..
// 4. External script tag defines var Module.
// We need to do an eval in order to handle the closure compiler
// case, where this code here is minified but Module was defined
// elsewhere (e.g. case 4 above). We also need to check if Module
// already exists (e.g. case 3 above).
// Note that if you want to run closure, and also to use Module
// after the generated code, you will need to define   var Module = {};
// before the code. Then that object will be used in the code, and you
// can continue to use Module afterwards as well.
var Module;
if (!Module) Module = (typeof Module !== 'undefined' ? Module : null) || {};

// Sometimes an existing Module object exists with properties
// meant to overwrite the default module functionality. Here
// we collect those properties and reapply _after_ we configure
// the current environment's defaults to avoid having to be so
// defensive during initialization.
var moduleOverrides = {};
for (var key in Module) {
  if (Module.hasOwnProperty(key)) {
    moduleOverrides[key] = Module[key];
  }
}

// The environment setup code below is customized to use Module.
// *** Environment setup code ***
var ENVIRONMENT_IS_NODE = typeof process === 'object' && typeof require === 'function';
var ENVIRONMENT_IS_WEB = typeof window === 'object';
var ENVIRONMENT_IS_WORKER = typeof importScripts === 'function';
var ENVIRONMENT_IS_SHELL = !ENVIRONMENT_IS_WEB && !ENVIRONMENT_IS_NODE && !ENVIRONMENT_IS_WORKER;

if (ENVIRONMENT_IS_NODE) {
  // Expose functionality in the same simple way that the shells work
  // Note that we pollute the global namespace here, otherwise we break in node
  if (!Module['print']) Module['print'] = function print(x) {
    process['stdout'].write(x + '\n');
  };
  if (!Module['printErr']) Module['printErr'] = function printErr(x) {
    process['stderr'].write(x + '\n');
  };

  var nodeFS = require('fs');
  var nodePath = require('path');

  Module['read'] = function read(filename, binary) {
    filename = nodePath['normalize'](filename);
    var ret = nodeFS['readFileSync'](filename);
    // The path is absolute if the normalized version is the same as the resolved.
    if (!ret && filename != nodePath['resolve'](filename)) {
      filename = path.join(__dirname, '..', 'src', filename);
      ret = nodeFS['readFileSync'](filename);
    }
    if (ret && !binary) ret = ret.toString();
    return ret;
  };

  Module['readBinary'] = function readBinary(filename) { return Module['read'](filename, true) };

  Module['load'] = function load(f) {
    globalEval(read(f));
  };

  Module['arguments'] = process['argv'].slice(2);

  module['exports'] = Module;
}
else if (ENVIRONMENT_IS_SHELL) {
  if (!Module['print']) Module['print'] = print;
  if (typeof printErr != 'undefined') Module['printErr'] = printErr; // not present in v8 or older sm

  if (typeof read != 'undefined') {
    Module['read'] = read;
  } else {
    Module['read'] = function read() { throw 'no read() available (jsc?)' };
  }

  Module['readBinary'] = function readBinary(f) {
    return read(f, 'binary');
  };

  if (typeof scriptArgs != 'undefined') {
    Module['arguments'] = scriptArgs;
  } else if (typeof arguments != 'undefined') {
    Module['arguments'] = arguments;
  }

  this['Module'] = Module;

  eval("if (typeof gc === 'function' && gc.toString().indexOf('[native code]') > 0) var gc = undefined"); // wipe out the SpiderMonkey shell 'gc' function, which can confuse closure (uses it as a minified name, and it is then initted to a non-falsey value unexpectedly)
}
else if (ENVIRONMENT_IS_WEB || ENVIRONMENT_IS_WORKER) {
  Module['read'] = function read(url) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, false);
    xhr.send(null);
    return xhr.responseText;
  };

  if (typeof arguments != 'undefined') {
    Module['arguments'] = arguments;
  }

  if (typeof console !== 'undefined') {
    if (!Module['print']) Module['print'] = function print(x) {
      console.log(x);
    };
    if (!Module['printErr']) Module['printErr'] = function printErr(x) {
      console.log(x);
    };
  } else {
    // Probably a worker, and without console.log. We can do very little here...
    var TRY_USE_DUMP = false;
    if (!Module['print']) Module['print'] = (TRY_USE_DUMP && (typeof(dump) !== "undefined") ? (function(x) {
      dump(x);
    }) : (function(x) {
      // self.postMessage(x); // enable this if you want stdout to be sent as messages
    }));
  }

  if (ENVIRONMENT_IS_WEB) {
    window['Module'] = Module;
  } else {
    Module['load'] = importScripts;
  }
}
else {
  // Unreachable because SHELL is dependant on the others
  throw 'Unknown runtime environment. Where are we?';
}

function globalEval(x) {
  eval.call(null, x);
}
if (!Module['load'] == 'undefined' && Module['read']) {
  Module['load'] = function load(f) {
    globalEval(Module['read'](f));
  };
}
if (!Module['print']) {
  Module['print'] = function(){};
}
if (!Module['printErr']) {
  Module['printErr'] = Module['print'];
}
if (!Module['arguments']) {
  Module['arguments'] = [];
}
// *** Environment setup code ***

// Closure helpers
Module.print = Module['print'];
Module.printErr = Module['printErr'];

// Callbacks
Module['preRun'] = [];
Module['postRun'] = [];

// Merge back in the overrides
for (var key in moduleOverrides) {
  if (moduleOverrides.hasOwnProperty(key)) {
    Module[key] = moduleOverrides[key];
  }
}



// === Auto-generated preamble library stuff ===

//========================================
// Runtime code shared with compiler
//========================================

var Runtime = {
  setTempRet0: function (value) {
    tempRet0 = value;
  },
  getTempRet0: function () {
    return tempRet0;
  },
  stackSave: function () {
    return STACKTOP;
  },
  stackRestore: function (stackTop) {
    STACKTOP = stackTop;
  },
  forceAlign: function (target, quantum) {
    quantum = quantum || 4;
    if (quantum == 1) return target;
    if (isNumber(target) && isNumber(quantum)) {
      return Math.ceil(target/quantum)*quantum;
    } else if (isNumber(quantum) && isPowerOfTwo(quantum)) {
      return '(((' +target + ')+' + (quantum-1) + ')&' + -quantum + ')';
    }
    return 'Math.ceil((' + target + ')/' + quantum + ')*' + quantum;
  },
  isNumberType: function (type) {
    return type in Runtime.INT_TYPES || type in Runtime.FLOAT_TYPES;
  },
  isPointerType: function isPointerType(type) {
  return type[type.length-1] == '*';
},
  isStructType: function isStructType(type) {
  if (isPointerType(type)) return false;
  if (isArrayType(type)) return true;
  if (/<?\{ ?[^}]* ?\}>?/.test(type)) return true; // { i32, i8 } etc. - anonymous struct types
  // See comment in isStructPointerType()
  return type[0] == '%';
},
  INT_TYPES: {"i1":0,"i8":0,"i16":0,"i32":0,"i64":0},
  FLOAT_TYPES: {"float":0,"double":0},
  or64: function (x, y) {
    var l = (x | 0) | (y | 0);
    var h = (Math.round(x / 4294967296) | Math.round(y / 4294967296)) * 4294967296;
    return l + h;
  },
  and64: function (x, y) {
    var l = (x | 0) & (y | 0);
    var h = (Math.round(x / 4294967296) & Math.round(y / 4294967296)) * 4294967296;
    return l + h;
  },
  xor64: function (x, y) {
    var l = (x | 0) ^ (y | 0);
    var h = (Math.round(x / 4294967296) ^ Math.round(y / 4294967296)) * 4294967296;
    return l + h;
  },
  getNativeTypeSize: function (type) {
    switch (type) {
      case 'i1': case 'i8': return 1;
      case 'i16': return 2;
      case 'i32': return 4;
      case 'i64': return 8;
      case 'float': return 4;
      case 'double': return 8;
      default: {
        if (type[type.length-1] === '*') {
          return Runtime.QUANTUM_SIZE; // A pointer
        } else if (type[0] === 'i') {
          var bits = parseInt(type.substr(1));
          assert(bits % 8 === 0);
          return bits/8;
        } else {
          return 0;
        }
      }
    }
  },
  getNativeFieldSize: function (type) {
    return Math.max(Runtime.getNativeTypeSize(type), Runtime.QUANTUM_SIZE);
  },
  dedup: function dedup(items, ident) {
  var seen = {};
  if (ident) {
    return items.filter(function(item) {
      if (seen[item[ident]]) return false;
      seen[item[ident]] = true;
      return true;
    });
  } else {
    return items.filter(function(item) {
      if (seen[item]) return false;
      seen[item] = true;
      return true;
    });
  }
},
  set: function set() {
  var args = typeof arguments[0] === 'object' ? arguments[0] : arguments;
  var ret = {};
  for (var i = 0; i < args.length; i++) {
    ret[args[i]] = 0;
  }
  return ret;
},
  STACK_ALIGN: 8,
  getAlignSize: function (type, size, vararg) {
    // we align i64s and doubles on 64-bit boundaries, unlike x86
    if (!vararg && (type == 'i64' || type == 'double')) return 8;
    if (!type) return Math.min(size, 8); // align structures internally to 64 bits
    return Math.min(size || (type ? Runtime.getNativeFieldSize(type) : 0), Runtime.QUANTUM_SIZE);
  },
  calculateStructAlignment: function calculateStructAlignment(type) {
    type.flatSize = 0;
    type.alignSize = 0;
    var diffs = [];
    var prev = -1;
    var index = 0;
    type.flatIndexes = type.fields.map(function(field) {
      index++;
      var size, alignSize;
      if (Runtime.isNumberType(field) || Runtime.isPointerType(field)) {
        size = Runtime.getNativeTypeSize(field); // pack char; char; in structs, also char[X]s.
        alignSize = Runtime.getAlignSize(field, size);
      } else if (Runtime.isStructType(field)) {
        if (field[1] === '0') {
          // this is [0 x something]. When inside another structure like here, it must be at the end,
          // and it adds no size
          // XXX this happens in java-nbody for example... assert(index === type.fields.length, 'zero-length in the middle!');
          size = 0;
          if (Types.types[field]) {
            alignSize = Runtime.getAlignSize(null, Types.types[field].alignSize);
          } else {
            alignSize = type.alignSize || QUANTUM_SIZE;
          }
        } else {
          size = Types.types[field].flatSize;
          alignSize = Runtime.getAlignSize(null, Types.types[field].alignSize);
        }
      } else if (field[0] == 'b') {
        // bN, large number field, like a [N x i8]
        size = field.substr(1)|0;
        alignSize = 1;
      } else if (field[0] === '<') {
        // vector type
        size = alignSize = Types.types[field].flatSize; // fully aligned
      } else if (field[0] === 'i') {
        // illegal integer field, that could not be legalized because it is an internal structure field
        // it is ok to have such fields, if we just use them as markers of field size and nothing more complex
        size = alignSize = parseInt(field.substr(1))/8;
        assert(size % 1 === 0, 'cannot handle non-byte-size field ' + field);
      } else {
        assert(false, 'invalid type for calculateStructAlignment');
      }
      if (type.packed) alignSize = 1;
      type.alignSize = Math.max(type.alignSize, alignSize);
      var curr = Runtime.alignMemory(type.flatSize, alignSize); // if necessary, place this on aligned memory
      type.flatSize = curr + size;
      if (prev >= 0) {
        diffs.push(curr-prev);
      }
      prev = curr;
      return curr;
    });
    if (type.name_ && type.name_[0] === '[') {
      // arrays have 2 elements, so we get the proper difference. then we scale here. that way we avoid
      // allocating a potentially huge array for [999999 x i8] etc.
      type.flatSize = parseInt(type.name_.substr(1))*type.flatSize/2;
    }
    type.flatSize = Runtime.alignMemory(type.flatSize, type.alignSize);
    if (diffs.length == 0) {
      type.flatFactor = type.flatSize;
    } else if (Runtime.dedup(diffs).length == 1) {
      type.flatFactor = diffs[0];
    }
    type.needsFlattening = (type.flatFactor != 1);
    return type.flatIndexes;
  },
  generateStructInfo: function (struct, typeName, offset) {
    var type, alignment;
    if (typeName) {
      offset = offset || 0;
      type = (typeof Types === 'undefined' ? Runtime.typeInfo : Types.types)[typeName];
      if (!type) return null;
      if (type.fields.length != struct.length) {
        printErr('Number of named fields must match the type for ' + typeName + ': possibly duplicate struct names. Cannot return structInfo');
        return null;
      }
      alignment = type.flatIndexes;
    } else {
      var type = { fields: struct.map(function(item) { return item[0] }) };
      alignment = Runtime.calculateStructAlignment(type);
    }
    var ret = {
      __size__: type.flatSize
    };
    if (typeName) {
      struct.forEach(function(item, i) {
        if (typeof item === 'string') {
          ret[item] = alignment[i] + offset;
        } else {
          // embedded struct
          var key;
          for (var k in item) key = k;
          ret[key] = Runtime.generateStructInfo(item[key], type.fields[i], alignment[i]);
        }
      });
    } else {
      struct.forEach(function(item, i) {
        ret[item[1]] = alignment[i];
      });
    }
    return ret;
  },
  dynCall: function (sig, ptr, args) {
    if (args && args.length) {
      if (!args.splice) args = Array.prototype.slice.call(args);
      args.splice(0, 0, ptr);
      return Module['dynCall_' + sig].apply(null, args);
    } else {
      return Module['dynCall_' + sig].call(null, ptr);
    }
  },
  functionPointers: [null,null,null,null,null,null,null,null],
  addFunction: function (func) {
    for (var i = 0; i < Runtime.functionPointers.length; i++) {
      if (!Runtime.functionPointers[i]) {
        Runtime.functionPointers[i] = func;
        return 2*(1 + i);
      }
    }
    throw 'Finished up all reserved function pointers. Use a higher value for RESERVED_FUNCTION_POINTERS.';
  },
  removeFunction: function (index) {
    Runtime.functionPointers[(index-2)/2] = null;
  },
  getAsmConst: function (code, numArgs) {
    // code is a constant string on the heap, so we can cache these
    if (!Runtime.asmConstCache) Runtime.asmConstCache = {};
    var func = Runtime.asmConstCache[code];
    if (func) return func;
    var args = [];
    for (var i = 0; i < numArgs; i++) {
      args.push(String.fromCharCode(36) + i); // $0, $1 etc
    }
    var source = Pointer_stringify(code);
    if (source[0] === '"') {
      // tolerate EM_ASM("..code..") even though EM_ASM(..code..) is correct
      if (source.indexOf('"', 1) === source.length-1) {
        source = source.substr(1, source.length-2);
      } else {
        // something invalid happened, e.g. EM_ASM("..code($0)..", input)
        abort('invalid EM_ASM input |' + source + '|. Please use EM_ASM(..code..) (no quotes) or EM_ASM({ ..code($0).. }, input) (to input values)');
      }
    }
    try {
      var evalled = eval('(function(' + args.join(',') + '){ ' + source + ' })'); // new Function does not allow upvars in node
    } catch(e) {
      Module.printErr('error in executing inline EM_ASM code: ' + e + ' on: \n\n' + source + '\n\nwith args |' + args + '| (make sure to use the right one out of EM_ASM, EM_ASM_ARGS, etc.)');
      throw e;
    }
    return Runtime.asmConstCache[code] = evalled;
  },
  warnOnce: function (text) {
    if (!Runtime.warnOnce.shown) Runtime.warnOnce.shown = {};
    if (!Runtime.warnOnce.shown[text]) {
      Runtime.warnOnce.shown[text] = 1;
      Module.printErr(text);
    }
  },
  funcWrappers: {},
  getFuncWrapper: function (func, sig) {
    assert(sig);
    if (!Runtime.funcWrappers[func]) {
      Runtime.funcWrappers[func] = function dynCall_wrapper() {
        return Runtime.dynCall(sig, func, arguments);
      };
    }
    return Runtime.funcWrappers[func];
  },
  UTF8Processor: function () {
    var buffer = [];
    var needed = 0;
    this.processCChar = function (code) {
      code = code & 0xFF;

      if (buffer.length == 0) {
        if ((code & 0x80) == 0x00) {        // 0xxxxxxx
          return String.fromCharCode(code);
        }
        buffer.push(code);
        if ((code & 0xE0) == 0xC0) {        // 110xxxxx
          needed = 1;
        } else if ((code & 0xF0) == 0xE0) { // 1110xxxx
          needed = 2;
        } else {                            // 11110xxx
          needed = 3;
        }
        return '';
      }

      if (needed) {
        buffer.push(code);
        needed--;
        if (needed > 0) return '';
      }

      var c1 = buffer[0];
      var c2 = buffer[1];
      var c3 = buffer[2];
      var c4 = buffer[3];
      var ret;
      if (buffer.length == 2) {
        ret = String.fromCharCode(((c1 & 0x1F) << 6)  | (c2 & 0x3F));
      } else if (buffer.length == 3) {
        ret = String.fromCharCode(((c1 & 0x0F) << 12) | ((c2 & 0x3F) << 6)  | (c3 & 0x3F));
      } else {
        // http://mathiasbynens.be/notes/javascript-encoding#surrogate-formulae
        var codePoint = ((c1 & 0x07) << 18) | ((c2 & 0x3F) << 12) |
                        ((c3 & 0x3F) << 6)  | (c4 & 0x3F);
        ret = String.fromCharCode(
          Math.floor((codePoint - 0x10000) / 0x400) + 0xD800,
          (codePoint - 0x10000) % 0x400 + 0xDC00);
      }
      buffer.length = 0;
      return ret;
    }
    this.processJSString = function processJSString(string) {
      /* TODO: use TextEncoder when present,
        var encoder = new TextEncoder();
        encoder['encoding'] = "utf-8";
        var utf8Array = encoder['encode'](aMsg.data);
      */
      string = unescape(encodeURIComponent(string));
      var ret = [];
      for (var i = 0; i < string.length; i++) {
        ret.push(string.charCodeAt(i));
      }
      return ret;
    }
  },
  getCompilerSetting: function (name) {
    throw 'You must build with -s RETAIN_COMPILER_SETTINGS=1 for Runtime.getCompilerSetting or emscripten_get_compiler_setting to work';
  },
  stackAlloc: function (size) { var ret = STACKTOP;STACKTOP = (STACKTOP + size)|0;STACKTOP = (((STACKTOP)+7)&-8); return ret; },
  staticAlloc: function (size) { var ret = STATICTOP;STATICTOP = (STATICTOP + size)|0;STATICTOP = (((STATICTOP)+7)&-8); return ret; },
  dynamicAlloc: function (size) { var ret = DYNAMICTOP;DYNAMICTOP = (DYNAMICTOP + size)|0;DYNAMICTOP = (((DYNAMICTOP)+7)&-8); if (DYNAMICTOP >= TOTAL_MEMORY) enlargeMemory();; return ret; },
  alignMemory: function (size,quantum) { var ret = size = Math.ceil((size)/(quantum ? quantum : 8))*(quantum ? quantum : 8); return ret; },
  makeBigInt: function (low,high,unsigned) { var ret = (unsigned ? ((+((low>>>0)))+((+((high>>>0)))*(+4294967296))) : ((+((low>>>0)))+((+((high|0)))*(+4294967296)))); return ret; },
  GLOBAL_BASE: 8,
  QUANTUM_SIZE: 4,
  __dummy__: 0
}


Module['Runtime'] = Runtime;

function jsCall() {
  var args = Array.prototype.slice.call(arguments);
  return Runtime.functionPointers[args[0]].apply(null, args.slice(1));
}








//========================================
// Runtime essentials
//========================================

var __THREW__ = 0; // Used in checking for thrown exceptions.

var ABORT = false; // whether we are quitting the application. no code should run after this. set in exit() and abort()
var EXITSTATUS = 0;

var undef = 0;
// tempInt is used for 32-bit signed values or smaller. tempBigInt is used
// for 32-bit unsigned values or more than 32 bits. TODO: audit all uses of tempInt
var tempValue, tempInt, tempBigInt, tempInt2, tempBigInt2, tempPair, tempBigIntI, tempBigIntR, tempBigIntS, tempBigIntP, tempBigIntD, tempDouble, tempFloat;
var tempI64, tempI64b;
var tempRet0, tempRet1, tempRet2, tempRet3, tempRet4, tempRet5, tempRet6, tempRet7, tempRet8, tempRet9;

function assert(condition, text) {
  if (!condition) {
    abort('Assertion failed: ' + text);
  }
}

var globalScope = this;

// C calling interface. A convenient way to call C functions (in C files, or
// defined with extern "C").
//
// Note: LLVM optimizations can inline and remove functions, after which you will not be
//       able to call them. Closure can also do so. To avoid that, add your function to
//       the exports using something like
//
//         -s EXPORTED_FUNCTIONS='["_main", "_myfunc"]'
//
// @param ident      The name of the C function (note that C++ functions will be name-mangled - use extern "C")
// @param returnType The return type of the function, one of the JS types 'number', 'string' or 'array' (use 'number' for any C pointer, and
//                   'array' for JavaScript arrays and typed arrays; note that arrays are 8-bit).
// @param argTypes   An array of the types of arguments for the function (if there are no arguments, this can be ommitted). Types are as in returnType,
//                   except that 'array' is not possible (there is no way for us to know the length of the array)
// @param args       An array of the arguments to the function, as native JS values (as in returnType)
//                   Note that string arguments will be stored on the stack (the JS string will become a C string on the stack).
// @return           The return value, as a native JS value (as in returnType)
function ccall(ident, returnType, argTypes, args) {
  return ccallFunc(getCFunc(ident), returnType, argTypes, args);
}
Module["ccall"] = ccall;

// Returns the C function with a specified identifier (for C++, you need to do manual name mangling)
function getCFunc(ident) {
  try {
    var func = Module['_' + ident]; // closure exported function
    if (!func) func = eval('_' + ident); // explicit lookup
  } catch(e) {
  }
  assert(func, 'Cannot call unknown function ' + ident + ' (perhaps LLVM optimizations or closure removed it?)');
  return func;
}

// Internal function that does a C call using a function, not an identifier
function ccallFunc(func, returnType, argTypes, args) {
  var stack = 0;
  function toC(value, type) {
    if (type == 'string') {
      if (value === null || value === undefined || value === 0) return 0; // null string
      value = intArrayFromString(value);
      type = 'array';
    }
    if (type == 'array') {
      if (!stack) stack = Runtime.stackSave();
      var ret = Runtime.stackAlloc(value.length);
      writeArrayToMemory(value, ret);
      return ret;
    }
    return value;
  }
  function fromC(value, type) {
    if (type == 'string') {
      return Pointer_stringify(value);
    }
    assert(type != 'array');
    return value;
  }
  var i = 0;
  var cArgs = args ? args.map(function(arg) {
    return toC(arg, argTypes[i++]);
  }) : [];
  var ret = fromC(func.apply(null, cArgs), returnType);
  if (stack) Runtime.stackRestore(stack);
  return ret;
}

// Returns a native JS wrapper for a C function. This is similar to ccall, but
// returns a function you can call repeatedly in a normal way. For example:
//
//   var my_function = cwrap('my_c_function', 'number', ['number', 'number']);
//   alert(my_function(5, 22));
//   alert(my_function(99, 12));
//
function cwrap(ident, returnType, argTypes) {
  var func = getCFunc(ident);
  return function() {
    return ccallFunc(func, returnType, argTypes, Array.prototype.slice.call(arguments));
  }
}
Module["cwrap"] = cwrap;

// Sets a value in memory in a dynamic way at run-time. Uses the
// type data. This is the same as makeSetValue, except that
// makeSetValue is done at compile-time and generates the needed
// code then, whereas this function picks the right code at
// run-time.
// Note that setValue and getValue only do *aligned* writes and reads!
// Note that ccall uses JS types as for defining types, while setValue and
// getValue need LLVM types ('i8', 'i32') - this is a lower-level operation
function setValue(ptr, value, type, noSafe) {
  type = type || 'i8';
  if (type.charAt(type.length-1) === '*') type = 'i32'; // pointers are 32-bit
    switch(type) {
      case 'i1': HEAP8[((ptr)>>0)]=value; break;
      case 'i8': HEAP8[((ptr)>>0)]=value; break;
      case 'i16': HEAP16[((ptr)>>1)]=value; break;
      case 'i32': HEAP32[((ptr)>>2)]=value; break;
      case 'i64': (tempI64 = [value>>>0,(tempDouble=value,(+(Math_abs(tempDouble))) >= (+1) ? (tempDouble > (+0) ? ((Math_min((+(Math_floor((tempDouble)/(+4294967296)))), (+4294967295)))|0)>>>0 : (~~((+(Math_ceil((tempDouble - +(((~~(tempDouble)))>>>0))/(+4294967296))))))>>>0) : 0)],HEAP32[((ptr)>>2)]=tempI64[0],HEAP32[(((ptr)+(4))>>2)]=tempI64[1]); break;
      case 'float': HEAPF32[((ptr)>>2)]=value; break;
      case 'double': HEAPF64[((ptr)>>3)]=value; break;
      default: abort('invalid type for setValue: ' + type);
    }
}
Module['setValue'] = setValue;

// Parallel to setValue.
function getValue(ptr, type, noSafe) {
  type = type || 'i8';
  if (type.charAt(type.length-1) === '*') type = 'i32'; // pointers are 32-bit
    switch(type) {
      case 'i1': return HEAP8[((ptr)>>0)];
      case 'i8': return HEAP8[((ptr)>>0)];
      case 'i16': return HEAP16[((ptr)>>1)];
      case 'i32': return HEAP32[((ptr)>>2)];
      case 'i64': return HEAP32[((ptr)>>2)];
      case 'float': return HEAPF32[((ptr)>>2)];
      case 'double': return HEAPF64[((ptr)>>3)];
      default: abort('invalid type for setValue: ' + type);
    }
  return null;
}
Module['getValue'] = getValue;

var ALLOC_NORMAL = 0; // Tries to use _malloc()
var ALLOC_STACK = 1; // Lives for the duration of the current function call
var ALLOC_STATIC = 2; // Cannot be freed
var ALLOC_DYNAMIC = 3; // Cannot be freed except through sbrk
var ALLOC_NONE = 4; // Do not allocate
Module['ALLOC_NORMAL'] = ALLOC_NORMAL;
Module['ALLOC_STACK'] = ALLOC_STACK;
Module['ALLOC_STATIC'] = ALLOC_STATIC;
Module['ALLOC_DYNAMIC'] = ALLOC_DYNAMIC;
Module['ALLOC_NONE'] = ALLOC_NONE;

// allocate(): This is for internal use. You can use it yourself as well, but the interface
//             is a little tricky (see docs right below). The reason is that it is optimized
//             for multiple syntaxes to save space in generated code. So you should
//             normally not use allocate(), and instead allocate memory using _malloc(),
//             initialize it with setValue(), and so forth.
// @slab: An array of data, or a number. If a number, then the size of the block to allocate,
//        in *bytes* (note that this is sometimes confusing: the next parameter does not
//        affect this!)
// @types: Either an array of types, one for each byte (or 0 if no type at that position),
//         or a single type which is used for the entire block. This only matters if there
//         is initial data - if @slab is a number, then this does not matter at all and is
//         ignored.
// @allocator: How to allocate memory, see ALLOC_*
function allocate(slab, types, allocator, ptr) {
  var zeroinit, size;
  if (typeof slab === 'number') {
    zeroinit = true;
    size = slab;
  } else {
    zeroinit = false;
    size = slab.length;
  }

  var singleType = typeof types === 'string' ? types : null;

  var ret;
  if (allocator == ALLOC_NONE) {
    ret = ptr;
  } else {
    ret = [_malloc, Runtime.stackAlloc, Runtime.staticAlloc, Runtime.dynamicAlloc][allocator === undefined ? ALLOC_STATIC : allocator](Math.max(size, singleType ? 1 : types.length));
  }

  if (zeroinit) {
    var ptr = ret, stop;
    assert((ret & 3) == 0);
    stop = ret + (size & ~3);
    for (; ptr < stop; ptr += 4) {
      HEAP32[((ptr)>>2)]=0;
    }
    stop = ret + size;
    while (ptr < stop) {
      HEAP8[((ptr++)>>0)]=0;
    }
    return ret;
  }

  if (singleType === 'i8') {
    if (slab.subarray || slab.slice) {
      HEAPU8.set(slab, ret);
    } else {
      HEAPU8.set(new Uint8Array(slab), ret);
    }
    return ret;
  }

  var i = 0, type, typeSize, previousType;
  while (i < size) {
    var curr = slab[i];

    if (typeof curr === 'function') {
      curr = Runtime.getFunctionIndex(curr);
    }

    type = singleType || types[i];
    if (type === 0) {
      i++;
      continue;
    }

    if (type == 'i64') type = 'i32'; // special case: we have one i32 here, and one i32 later

    setValue(ret+i, curr, type);

    // no need to look up size unless type changes, so cache it
    if (previousType !== type) {
      typeSize = Runtime.getNativeTypeSize(type);
      previousType = type;
    }
    i += typeSize;
  }

  return ret;
}
Module['allocate'] = allocate;

function Pointer_stringify(ptr, /* optional */ length) {
  // TODO: use TextDecoder
  // Find the length, and check for UTF while doing so
  var hasUtf = false;
  var t;
  var i = 0;
  while (1) {
    t = HEAPU8[(((ptr)+(i))>>0)];
    if (t >= 128) hasUtf = true;
    else if (t == 0 && !length) break;
    i++;
    if (length && i == length) break;
  }
  if (!length) length = i;

  var ret = '';

  if (!hasUtf) {
    var MAX_CHUNK = 1024; // split up into chunks, because .apply on a huge string can overflow the stack
    var curr;
    while (length > 0) {
      curr = String.fromCharCode.apply(String, HEAPU8.subarray(ptr, ptr + Math.min(length, MAX_CHUNK)));
      ret = ret ? ret + curr : curr;
      ptr += MAX_CHUNK;
      length -= MAX_CHUNK;
    }
    return ret;
  }

  var utf8 = new Runtime.UTF8Processor();
  for (i = 0; i < length; i++) {
    t = HEAPU8[(((ptr)+(i))>>0)];
    ret += utf8.processCChar(t);
  }
  return ret;
}
Module['Pointer_stringify'] = Pointer_stringify;

// Given a pointer 'ptr' to a null-terminated UTF16LE-encoded string in the emscripten HEAP, returns
// a copy of that string as a Javascript String object.
function UTF16ToString(ptr) {
  var i = 0;

  var str = '';
  while (1) {
    var codeUnit = HEAP16[(((ptr)+(i*2))>>1)];
    if (codeUnit == 0)
      return str;
    ++i;
    // fromCharCode constructs a character from a UTF-16 code unit, so we can pass the UTF16 string right through.
    str += String.fromCharCode(codeUnit);
  }
}
Module['UTF16ToString'] = UTF16ToString;

// Copies the given Javascript String object 'str' to the emscripten HEAP at address 'outPtr',
// null-terminated and encoded in UTF16LE form. The copy will require at most (str.length*2+1)*2 bytes of space in the HEAP.
function stringToUTF16(str, outPtr) {
  for(var i = 0; i < str.length; ++i) {
    // charCodeAt returns a UTF-16 encoded code unit, so it can be directly written to the HEAP.
    var codeUnit = str.charCodeAt(i); // possibly a lead surrogate
    HEAP16[(((outPtr)+(i*2))>>1)]=codeUnit;
  }
  // Null-terminate the pointer to the HEAP.
  HEAP16[(((outPtr)+(str.length*2))>>1)]=0;
}
Module['stringToUTF16'] = stringToUTF16;

// Given a pointer 'ptr' to a null-terminated UTF32LE-encoded string in the emscripten HEAP, returns
// a copy of that string as a Javascript String object.
function UTF32ToString(ptr) {
  var i = 0;

  var str = '';
  while (1) {
    var utf32 = HEAP32[(((ptr)+(i*4))>>2)];
    if (utf32 == 0)
      return str;
    ++i;
    // Gotcha: fromCharCode constructs a character from a UTF-16 encoded code (pair), not from a Unicode code point! So encode the code point to UTF-16 for constructing.
    if (utf32 >= 0x10000) {
      var ch = utf32 - 0x10000;
      str += String.fromCharCode(0xD800 | (ch >> 10), 0xDC00 | (ch & 0x3FF));
    } else {
      str += String.fromCharCode(utf32);
    }
  }
}
Module['UTF32ToString'] = UTF32ToString;

// Copies the given Javascript String object 'str' to the emscripten HEAP at address 'outPtr',
// null-terminated and encoded in UTF32LE form. The copy will require at most (str.length+1)*4 bytes of space in the HEAP,
// but can use less, since str.length does not return the number of characters in the string, but the number of UTF-16 code units in the string.
function stringToUTF32(str, outPtr) {
  var iChar = 0;
  for(var iCodeUnit = 0; iCodeUnit < str.length; ++iCodeUnit) {
    // Gotcha: charCodeAt returns a 16-bit word that is a UTF-16 encoded code unit, not a Unicode code point of the character! We must decode the string to UTF-32 to the heap.
    var codeUnit = str.charCodeAt(iCodeUnit); // possibly a lead surrogate
    if (codeUnit >= 0xD800 && codeUnit <= 0xDFFF) {
      var trailSurrogate = str.charCodeAt(++iCodeUnit);
      codeUnit = 0x10000 + ((codeUnit & 0x3FF) << 10) | (trailSurrogate & 0x3FF);
    }
    HEAP32[(((outPtr)+(iChar*4))>>2)]=codeUnit;
    ++iChar;
  }
  // Null-terminate the pointer to the HEAP.
  HEAP32[(((outPtr)+(iChar*4))>>2)]=0;
}
Module['stringToUTF32'] = stringToUTF32;

function demangle(func) {
  var i = 3;
  // params, etc.
  var basicTypes = {
    'v': 'void',
    'b': 'bool',
    'c': 'char',
    's': 'short',
    'i': 'int',
    'l': 'long',
    'f': 'float',
    'd': 'double',
    'w': 'wchar_t',
    'a': 'signed char',
    'h': 'unsigned char',
    't': 'unsigned short',
    'j': 'unsigned int',
    'm': 'unsigned long',
    'x': 'long long',
    'y': 'unsigned long long',
    'z': '...'
  };
  var subs = [];
  var first = true;
  function dump(x) {
    //return;
    if (x) Module.print(x);
    Module.print(func);
    var pre = '';
    for (var a = 0; a < i; a++) pre += ' ';
    Module.print (pre + '^');
  }
  function parseNested() {
    i++;
    if (func[i] === 'K') i++; // ignore const
    var parts = [];
    while (func[i] !== 'E') {
      if (func[i] === 'S') { // substitution
        i++;
        var next = func.indexOf('_', i);
        var num = func.substring(i, next) || 0;
        parts.push(subs[num] || '?');
        i = next+1;
        continue;
      }
      if (func[i] === 'C') { // constructor
        parts.push(parts[parts.length-1]);
        i += 2;
        continue;
      }
      var size = parseInt(func.substr(i));
      var pre = size.toString().length;
      if (!size || !pre) { i--; break; } // counter i++ below us
      var curr = func.substr(i + pre, size);
      parts.push(curr);
      subs.push(curr);
      i += pre + size;
    }
    i++; // skip E
    return parts;
  }
  function parse(rawList, limit, allowVoid) { // main parser
    limit = limit || Infinity;
    var ret = '', list = [];
    function flushList() {
      return '(' + list.join(', ') + ')';
    }
    var name;
    if (func[i] === 'N') {
      // namespaced N-E
      name = parseNested().join('::');
      limit--;
      if (limit === 0) return rawList ? [name] : name;
    } else {
      // not namespaced
      if (func[i] === 'K' || (first && func[i] === 'L')) i++; // ignore const and first 'L'
      var size = parseInt(func.substr(i));
      if (size) {
        var pre = size.toString().length;
        name = func.substr(i + pre, size);
        i += pre + size;
      }
    }
    first = false;
    if (func[i] === 'I') {
      i++;
      var iList = parse(true);
      var iRet = parse(true, 1, true);
      ret += iRet[0] + ' ' + name + '<' + iList.join(', ') + '>';
    } else {
      ret = name;
    }
    paramLoop: while (i < func.length && limit-- > 0) {
      //dump('paramLoop');
      var c = func[i++];
      if (c in basicTypes) {
        list.push(basicTypes[c]);
      } else {
        switch (c) {
          case 'P': list.push(parse(true, 1, true)[0] + '*'); break; // pointer
          case 'R': list.push(parse(true, 1, true)[0] + '&'); break; // reference
          case 'L': { // literal
            i++; // skip basic type
            var end = func.indexOf('E', i);
            var size = end - i;
            list.push(func.substr(i, size));
            i += size + 2; // size + 'EE'
            break;
          }
          case 'A': { // array
            var size = parseInt(func.substr(i));
            i += size.toString().length;
            if (func[i] !== '_') throw '?';
            i++; // skip _
            list.push(parse(true, 1, true)[0] + ' [' + size + ']');
            break;
          }
          case 'E': break paramLoop;
          default: ret += '?' + c; break paramLoop;
        }
      }
    }
    if (!allowVoid && list.length === 1 && list[0] === 'void') list = []; // avoid (void)
    if (rawList) {
      if (ret) {
        list.push(ret + '?');
      }
      return list;
    } else {
      return ret + flushList();
    }
  }
  try {
    // Special-case the entry point, since its name differs from other name mangling.
    if (func == 'Object._main' || func == '_main') {
      return 'main()';
    }
    if (typeof func === 'number') func = Pointer_stringify(func);
    if (func[0] !== '_') return func;
    if (func[1] !== '_') return func; // C function
    if (func[2] !== 'Z') return func;
    switch (func[3]) {
      case 'n': return 'operator new()';
      case 'd': return 'operator delete()';
    }
    return parse();
  } catch(e) {
    return func;
  }
}

function demangleAll(text) {
  return text.replace(/__Z[\w\d_]+/g, function(x) { var y = demangle(x); return x === y ? x : (x + ' [' + y + ']') });
}

function stackTrace() {
  var stack = new Error().stack;
  return stack ? demangleAll(stack) : '(no stack trace available)'; // Stack trace is not available at least on IE10 and Safari 6.
}

// Memory management

var PAGE_SIZE = 4096;
function alignMemoryPage(x) {
  return (x+4095)&-4096;
}

var HEAP;
var HEAP8, HEAPU8, HEAP16, HEAPU16, HEAP32, HEAPU32, HEAPF32, HEAPF64;

var STATIC_BASE = 0, STATICTOP = 0, staticSealed = false; // static area
var STACK_BASE = 0, STACKTOP = 0, STACK_MAX = 0; // stack area
var DYNAMIC_BASE = 0, DYNAMICTOP = 0; // dynamic area handled by sbrk

function enlargeMemory() {
  abort('Cannot enlarge memory arrays. Either (1) compile with -s TOTAL_MEMORY=X with X higher than the current value ' + TOTAL_MEMORY + ', (2) compile with ALLOW_MEMORY_GROWTH which adjusts the size at runtime but prevents some optimizations, or (3) set Module.TOTAL_MEMORY before the program runs.');
}

var TOTAL_STACK = Module['TOTAL_STACK'] || 5242880;
var TOTAL_MEMORY = Module['TOTAL_MEMORY'] || 16777216;
var FAST_MEMORY = Module['FAST_MEMORY'] || 2097152;

var totalMemory = 4096;
while (totalMemory < TOTAL_MEMORY || totalMemory < 2*TOTAL_STACK) {
  if (totalMemory < 16*1024*1024) {
    totalMemory *= 2;
  } else {
    totalMemory += 16*1024*1024
  }
}
if (totalMemory !== TOTAL_MEMORY) {
  Module.printErr('increasing TOTAL_MEMORY to ' + totalMemory + ' to be more reasonable');
  TOTAL_MEMORY = totalMemory;
}

// Initialize the runtime's memory
// check for full engine support (use string 'subarray' to avoid closure compiler confusion)
assert(typeof Int32Array !== 'undefined' && typeof Float64Array !== 'undefined' && !!(new Int32Array(1)['subarray']) && !!(new Int32Array(1)['set']),
       'JS engine does not provide full typed array support');

var buffer = new ArrayBuffer(TOTAL_MEMORY);
HEAP8 = new Int8Array(buffer);
HEAP16 = new Int16Array(buffer);
HEAP32 = new Int32Array(buffer);
HEAPU8 = new Uint8Array(buffer);
HEAPU16 = new Uint16Array(buffer);
HEAPU32 = new Uint32Array(buffer);
HEAPF32 = new Float32Array(buffer);
HEAPF64 = new Float64Array(buffer);

// Endianness check (note: assumes compiler arch was little-endian)
HEAP32[0] = 255;
assert(HEAPU8[0] === 255 && HEAPU8[3] === 0, 'Typed arrays 2 must be run on a little-endian system');

Module['HEAP'] = HEAP;
Module['HEAP8'] = HEAP8;
Module['HEAP16'] = HEAP16;
Module['HEAP32'] = HEAP32;
Module['HEAPU8'] = HEAPU8;
Module['HEAPU16'] = HEAPU16;
Module['HEAPU32'] = HEAPU32;
Module['HEAPF32'] = HEAPF32;
Module['HEAPF64'] = HEAPF64;

function callRuntimeCallbacks(callbacks) {
  while(callbacks.length > 0) {
    var callback = callbacks.shift();
    if (typeof callback == 'function') {
      callback();
      continue;
    }
    var func = callback.func;
    if (typeof func === 'number') {
      if (callback.arg === undefined) {
        Runtime.dynCall('v', func);
      } else {
        Runtime.dynCall('vi', func, [callback.arg]);
      }
    } else {
      func(callback.arg === undefined ? null : callback.arg);
    }
  }
}

var __ATPRERUN__  = []; // functions called before the runtime is initialized
var __ATINIT__    = []; // functions called during startup
var __ATMAIN__    = []; // functions called when main() is to be run
var __ATEXIT__    = []; // functions called during shutdown
var __ATPOSTRUN__ = []; // functions called after the runtime has exited

var runtimeInitialized = false;

function preRun() {
  // compatibility - merge in anything from Module['preRun'] at this time
  if (Module['preRun']) {
    if (typeof Module['preRun'] == 'function') Module['preRun'] = [Module['preRun']];
    while (Module['preRun'].length) {
      addOnPreRun(Module['preRun'].shift());
    }
  }
  callRuntimeCallbacks(__ATPRERUN__);
}

function ensureInitRuntime() {
  if (runtimeInitialized) return;
  runtimeInitialized = true;
  callRuntimeCallbacks(__ATINIT__);
}

function preMain() {
  callRuntimeCallbacks(__ATMAIN__);
}

function exitRuntime() {
  callRuntimeCallbacks(__ATEXIT__);
}

function postRun() {
  // compatibility - merge in anything from Module['postRun'] at this time
  if (Module['postRun']) {
    if (typeof Module['postRun'] == 'function') Module['postRun'] = [Module['postRun']];
    while (Module['postRun'].length) {
      addOnPostRun(Module['postRun'].shift());
    }
  }
  callRuntimeCallbacks(__ATPOSTRUN__);
}

function addOnPreRun(cb) {
  __ATPRERUN__.unshift(cb);
}
Module['addOnPreRun'] = Module.addOnPreRun = addOnPreRun;

function addOnInit(cb) {
  __ATINIT__.unshift(cb);
}
Module['addOnInit'] = Module.addOnInit = addOnInit;

function addOnPreMain(cb) {
  __ATMAIN__.unshift(cb);
}
Module['addOnPreMain'] = Module.addOnPreMain = addOnPreMain;

function addOnExit(cb) {
  __ATEXIT__.unshift(cb);
}
Module['addOnExit'] = Module.addOnExit = addOnExit;

function addOnPostRun(cb) {
  __ATPOSTRUN__.unshift(cb);
}
Module['addOnPostRun'] = Module.addOnPostRun = addOnPostRun;

// Tools

// This processes a JS string into a C-line array of numbers, 0-terminated.
// For LLVM-originating strings, see parser.js:parseLLVMString function
function intArrayFromString(stringy, dontAddNull, length /* optional */) {
  var ret = (new Runtime.UTF8Processor()).processJSString(stringy);
  if (length) {
    ret.length = length;
  }
  if (!dontAddNull) {
    ret.push(0);
  }
  return ret;
}
Module['intArrayFromString'] = intArrayFromString;

function intArrayToString(array) {
  var ret = [];
  for (var i = 0; i < array.length; i++) {
    var chr = array[i];
    if (chr > 0xFF) {
      chr &= 0xFF;
    }
    ret.push(String.fromCharCode(chr));
  }
  return ret.join('');
}
Module['intArrayToString'] = intArrayToString;

// Write a Javascript array to somewhere in the heap
function writeStringToMemory(string, buffer, dontAddNull) {
  var array = intArrayFromString(string, dontAddNull);
  var i = 0;
  while (i < array.length) {
    var chr = array[i];
    HEAP8[(((buffer)+(i))>>0)]=chr;
    i = i + 1;
  }
}
Module['writeStringToMemory'] = writeStringToMemory;

function writeArrayToMemory(array, buffer) {
  for (var i = 0; i < array.length; i++) {
    HEAP8[(((buffer)+(i))>>0)]=array[i];
  }
}
Module['writeArrayToMemory'] = writeArrayToMemory;

function writeAsciiToMemory(str, buffer, dontAddNull) {
  for (var i = 0; i < str.length; i++) {
    HEAP8[(((buffer)+(i))>>0)]=str.charCodeAt(i);
  }
  if (!dontAddNull) HEAP8[(((buffer)+(str.length))>>0)]=0;
}
Module['writeAsciiToMemory'] = writeAsciiToMemory;

function unSign(value, bits, ignore) {
  if (value >= 0) {
    return value;
  }
  return bits <= 32 ? 2*Math.abs(1 << (bits-1)) + value // Need some trickery, since if bits == 32, we are right at the limit of the bits JS uses in bitshifts
                    : Math.pow(2, bits)         + value;
}
function reSign(value, bits, ignore) {
  if (value <= 0) {
    return value;
  }
  var half = bits <= 32 ? Math.abs(1 << (bits-1)) // abs is needed if bits == 32
                        : Math.pow(2, bits-1);
  if (value >= half && (bits <= 32 || value > half)) { // for huge values, we can hit the precision limit and always get true here. so don't do that
                                                       // but, in general there is no perfect solution here. With 64-bit ints, we get rounding and errors
                                                       // TODO: In i64 mode 1, resign the two parts separately and safely
    value = -2*half + value; // Cannot bitshift half, as it may be at the limit of the bits JS uses in bitshifts
  }
  return value;
}

// check for imul support, and also for correctness ( https://bugs.webkit.org/show_bug.cgi?id=126345 )
if (!Math['imul'] || Math['imul'](0xffffffff, 5) !== -5) Math['imul'] = function imul(a, b) {
  var ah  = a >>> 16;
  var al = a & 0xffff;
  var bh  = b >>> 16;
  var bl = b & 0xffff;
  return (al*bl + ((ah*bl + al*bh) << 16))|0;
};
Math.imul = Math['imul'];


var Math_abs = Math.abs;
var Math_cos = Math.cos;
var Math_sin = Math.sin;
var Math_tan = Math.tan;
var Math_acos = Math.acos;
var Math_asin = Math.asin;
var Math_atan = Math.atan;
var Math_atan2 = Math.atan2;
var Math_exp = Math.exp;
var Math_log = Math.log;
var Math_sqrt = Math.sqrt;
var Math_ceil = Math.ceil;
var Math_floor = Math.floor;
var Math_pow = Math.pow;
var Math_imul = Math.imul;
var Math_fround = Math.fround;
var Math_min = Math.min;

// A counter of dependencies for calling run(). If we need to
// do asynchronous work before running, increment this and
// decrement it. Incrementing must happen in a place like
// PRE_RUN_ADDITIONS (used by emcc to add file preloading).
// Note that you can add dependencies in preRun, even though
// it happens right before run - run will be postponed until
// the dependencies are met.
var runDependencies = 0;
var runDependencyWatcher = null;
var dependenciesFulfilled = null; // overridden to take different actions when all run dependencies are fulfilled

function addRunDependency(id) {
  runDependencies++;
  if (Module['monitorRunDependencies']) {
    Module['monitorRunDependencies'](runDependencies);
  }
}
Module['addRunDependency'] = addRunDependency;
function removeRunDependency(id) {
  runDependencies--;
  if (Module['monitorRunDependencies']) {
    Module['monitorRunDependencies'](runDependencies);
  }
  if (runDependencies == 0) {
    if (runDependencyWatcher !== null) {
      clearInterval(runDependencyWatcher);
      runDependencyWatcher = null;
    }
    if (dependenciesFulfilled) {
      var callback = dependenciesFulfilled;
      dependenciesFulfilled = null;
      callback(); // can add another dependenciesFulfilled
    }
  }
}
Module['removeRunDependency'] = removeRunDependency;

Module["preloadedImages"] = {}; // maps url to image data
Module["preloadedAudios"] = {}; // maps url to audio data


var memoryInitializer = null;

// === Body ===





STATIC_BASE = 8;

STATICTOP = STATIC_BASE + Runtime.alignMemory(10019);
/* global initializers */ __ATINIT__.push();


/* memory initializer */ allocate([5,0,0,0,4,0,0,0,8,0,0,0,7,0,0,0,7,0,0,0,12,0,0,0,6,0,0,0,6,0,0,0,12,0,0,0,6,0,0,0,6,0,0,0,7,0,0,0,7,0,0,0,12,0,0,0,6,0,0,0,6,0,0,0,12,0,0,0,6,0,0,0,6,0,0,0,7,0,0,0,7,0,0,0,0,0,0,0,4,0,0,0,1,0,0,0,2,0,0,0,3,0,0,0,5,0,0,0,6,0,0,0,7,0,0,0,0,0,0,0,105,77,111,100,101,108,66,105,116,115,32,60,32,49,54,0,105,109,97,103,101,47,101,110,99,111,100,101,47,115,101,103,101,110,99,46,99,0,0,0,67,111,100,101,67,111,101,102,102,115,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,2,0,0,0,1,0,0,0,2,0,0,0,2,0,0,0,3,0,0,0,1,0,0,0,2,0,0,0,2,0,0,0,3,0,0,0,2,0,0,0,3,0,0,0,3,0,0,0,4,0,0,0,0,0,0,0,2,0,0,0,2,0,0,0,2,0,0,0,2,0,0,0,2,0,0,0,3,0,0,0,2,0,0,0,2,0,0,0,3,0,0,0,3,0,0,0,2,0,0,0,3,0,0,0,2,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,2,0,0,0,1,0,0,0,4,0,0,0,3,0,0,0,3,0,0,0,5,0,0,0,6,0,0,0,2,0,0,0,7,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,2,0,0,0,1,0,0,0,3,0,0,0,3,0,0,0,4,0,0,0,1,0,0,0,3,0,0,0,3,0,0,0,4,0,0,0,2,0,0,0,4,0,0,0,4,0,0,0,5,0,0,0,0,0,0,0,2,0,0,0,2,0,0,0,1,0,0,0,2,0,0,0,2,0,0,0,2,0,0,0,2,0,0,0,2,0,0,0,2,0,0,0,2,0,0,0,2,0,0,0,1,0,0,0,2,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,3,0,0,0,2,0,0,0,3,0,0,0,1,0,0,0,1,0,0,0,2,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,6,0,0,0,2,0,0,0,7,0,0,0,1,0,0,0,3,0,0,0,2,0,0,0,3,0,0,0,0,0,0,0,1,0,0,0,2,0,0,0,2,0,0,0,3,0,0,0,3,0,0,0,4,0,0,0,4,0,0,0,4,0,0,0,4,0,0,0,4,0,0,0,4,0,0,0,4,0,0,0,4,0,0,0,0,0,0,0,1,0,0,0,2,0,0,0,2,0,0,0,3,0,0,0,3,0,0,0,4,0,0,0,4,0,0,0,4,0,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,2,0,0,0,3,0,0,0,4,0,0,0,4,0,0,0,3,0,0,0,3,0,0,0,2,0,0,0,1,0,0,0,131,0,0,0,127,0,0,0,123,0,0,0,119,0,0,0,115,0,0,0,111,0,0,0,107,0,0,0,103,0,0,0,99,0,0,0,95,0,0,0,91,0,0,0,87,0,0,0,83,0,0,0,79,0,0,0,75,0,0,0,71,0,0,0,67,0,0,0,63,0,0,0,59,0,0,0,55,0,0,0,51,0,0,0,47,0,0,0,43,0,0,0,39,0,0,0,35,0,0,0,31,0,0,0,27,0,0,0,23,0,0,0,19,0,0,0,15,0,0,0,11,0,0,0,7,0,0,0,0,0,0,0,5,0,0,0,9,0,0,0,13,0,0,0,17,0,0,0,21,0,0,0,25,0,0,0,29,0,0,0,33,0,0,0,37,0,0,0,41,0,0,0,45,0,0,0,49,0,0,0,53,0,0,0,57,0,0,0,61,0,0,0,65,0,0,0,69,0,0,0,73,0,0,0,77,0,0,0,81,0,0,0,85,0,0,0,89,0,0,0,93,0,0,0,97,0,0,0,101,0,0,0,105,0,0,0,109,0,0,0,113,0,0,0,117,0,0,0,121,0,0,0,125,0,0,0,129,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,2,0,0,0,2,0,0,0,3,0,0,0,3,0,0,0,3,0,0,0,3,0,0,0,4,0,0,0,4,0,0,0,4,0,0,0,4,0,0,0,5,0,0,0,5,0,0,0,5,0,0,0,5,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,2,0,0,0,2,0,0,0,2,0,0,0,105,65,98,115,76,101,118,101,108,32,62,32,48,0,0,0,69,110,99,111,100,101,83,105,103,110,105,102,105,99,97,110,116,65,98,115,76,101,118,101,108,0,0,0,0,0,0,0,105,70,105,120,101,100,32,60,32,51,48,0,0,0,0,0,119,43,98,0,0,0,0,0,2,0,0,0,4,0,0,0,0,0,0,0,4,0,0,0,8,0,0,0,16,0,0,0,16,0,0,0,16,0,0,0,16,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,99,70,117,108,108,67,104,97,110,110,101,108,32,60,61,32,49,54,0,0,0,0,0,0,105,109,97,103,101,47,101,110,99,111,100,101,47,115,116,114,101,110,99,46,99,0,0,0,112,97,100,72,111,114,105,122,111,110,116,97,108,108,121,0,112,83,67,45,62,87,77,73,83,67,80,46,99,67,104,97,110,110,101,108,32,60,61,32,49,54,0,0,0,0,0,0,99,67,104,97,110,110,101,108,32,60,61,32,49,54,0,0,105,110,112,117,116,77,66,82,111,119,0,0,0,0,0,0,48,0,0,0,0,0,0,0,112,83,67,45,62,109,95,98,83,101,99,111,110,100,97,114,121,32,61,61,32,70,65,76,83,69,0,0,0,0,0,0,99,102,69,120,116,32,61,61,32,67,70,95,82,71,66,0,99,102,69,120,116,32,61,61,32,89,95,79,78,76,89,0,84,105,108,101,32,119,105,100,116,104,32,109,117,115,116,32,98,101,32,97,116,32,108,101,97,115,116,32,50,32,77,66,32,119,105,100,101,32,102,111,114,32,104,97,114,100,32,116,105,108,101,115,44,32,115,117,98,115,97,109,112,108,101,100,32,99,104,114,111,109,97,44,32,97,110,100,32,116,119,111,32,108,101,118,101,108,115,32,111,102,32,111,118,101,114,108,97,112,33,0,0,0,0,0,70,108,111,97,116,32,111,114,32,82,71,66,69,32,105,109,97,103,101,115,32,109,117,115,116,32,98,101,32,101,110,99,111,100,101,100,32,119,105,116,104,32,89,85,86,32,52,52,52,33,0,0,0,0,0,0,65,108,112,104,97,32,105,115,32,110,111,116,32,115,117,112,112,111,114,116,101,100,32,102,111,114,32,116,104,105,115,32,112,105,120,101,108,32,102,111,114,109,97,116,33,0,0,0,66,68,95,49,32,105,109,97,103,101,32,109,117,115,116,32,98,101,32,98,108,97,99,107,45,97,110,100,32,119,104,105,116,101,33,0,0,0,0,0,85,110,115,117,112,112,111,114,116,101,100,32,66,68,95,49,48,32,105,109,97,103,101,32,102,111,114,109,97,116,33,0,85,110,115,117,112,112,111,114,116,101,100,32,66,68,95,53,54,53,32,105,109,97,103,101,32,102,111,114,109,97,116,33,0,0,0,0,0,0,0,0,85,110,115,117,112,112,111,114,116,101,100,32,66,68,95,53,32,105,109,97,103,101,32,102,111,114,109,97,116,33,0,0,73,109,97,103,101,32,119,105,100,116,104,32,109,117,115,116,32,98,101,32,97,116,32,108,101,97,115,116,32,50,32,77,66,32,119,105,100,101,32,102,111,114,32,115,117,98,115,97,109,112,108,101,100,32,99,104,114,111,109,97,32,97,110,100,32,116,119,111,32,108,101,118,101,108,115,32,111,102,32,111,118,101,114,108,97,112,33,0,85,110,115,117,114,112,112,111,114,116,101,100,32,105,109,97,103,101,32,115,105,122,101,33,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,2,0,0,0,1,0,0,0,2,0,0,0,2,0,0,0,3,0,0,0,1,0,0,0,2,0,0,0,2,0,0,0,3,0,0,0,2,0,0,0,3,0,0,0,3,0,0,0,4,0,0,0,5,0,0,0,4,0,0,0,8,0,0,0,7,0,0,0,7,0,0,0,12,0,0,0,6,0,0,0,6,0,0,0,12,0,0,0,6,0,0,0,6,0,0,0,7,0,0,0,7,0,0,0,12,0,0,0,6,0,0,0,6,0,0,0,12,0,0,0,6,0,0,0,6,0,0,0,7,0,0,0,7,0,0,0,0,0,0,0,73,110,115,117,102,102,105,99,105,101,110,116,32,109,101,109,111,114,121,32,116,111,32,105,110,105,116,32,100,101,99,111,100,101,114,46,0,0,0,0,115,116,114,80,111,115,116,80,114,111,99,73,110,102,111,91,106,93,91,105,93,32,33,61,32,78,85,76,76,0,0,0,105,109,97,103,101,47,100,101,99,111,100,101,47,112,111,115,116,112,114,111,99,101,115,115,46,99,0,0,0,0,0,0,105,110,105,116,80,111,115,116,80,114,111,99,0,0,0,0,1,0,0,0,2,0,0,0,3,0,0,0,5,0,0,0,7,0,0,0,1,0,0,0,2,0,0,0,3,0,0,0,5,0,0,0,7,0,0,0,1,0,0,0,2,0,0,0,3,0,0,0,4,0,0,0,5,0,0,0,0,0,0,0,4,0,0,0,1,0,0,0,2,0,0,0,3,0,0,0,5,0,0,0,6,0,0,0,7,0,0,0,0,0,0,0,112,83,67,45,62,109,95,68,112,97,114,97,109,45,62,98,83,107,105,112,70,108,101,120,98,105,116,115,32,61,61,32,48,32,124,124,32,112,83,67,45,62,87,77,73,83,67,80,46,98,102,66,105,116,115,116,114,101,97,109,70,111,114,109,97,116,32,61,61,32,70,82,69,81,85,69,78,67,89,32,124,124,32,112,83,67,45,62,87,77,73,83,67,80,46,115,98,83,117,98,98,97,110,100,32,61,61,32,83,66,95,78,79,95,70,76,69,88,66,73,84,83,0,0,0,0,0,0,105,109,97,103,101,47,100,101,99,111,100,101,47,115,101,103,100,101,99,46,99,0,0,0,68,101,99,111,100,101,67,111,101,102,102,115,0,0,0,0,105,84,114,105,109,32,61,61,32,48,0,0,0,0,0,0,68,101,99,111,100,101,66,108,111,99,107,65,100,97,112,116,105,118,101,0,0,0,0,0,105,81,80,32,61,61,32,49,0,0,0,0,0,0,0,0,105,83,82,110,32,62,61,32,48,32,38,38,32,105,83,82,110,32,60,32,51,0,0,0,68,101,99,111,100,101,66,108,111,99,107,72,105,103,104,112,97,115,115,0,0,0,0,0,0,0,0,0,2,0,0,0,1,0,0,0,2,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,4,0,0,0,2,0,0,0,8,0,0,0,12,0,0,0,1,0,0,0,0,0,0,0,15,0,0,0,3,0,0,0,12,0,0,0,1,0,0,0,2,0,0,0,4,0,0,0,8,0,0,0,5,0,0,0,6,0,0,0,9,0,0,0,10,0,0,0,7,0,0,0,11,0,0,0,13,0,0,0,14,0,0,0,6,0,0,0,9,0,0,0,10,0,0,0,12,0,0,0,0,0,0,0,1,0,0,0,4,0,0,0,5,0,0,0,2,0,0,0,3,0,0,0,4,0,0,0,6,0,0,0,10,0,0,0,14,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,2,0,0,0,2,0,0,0,2,0,0,0,105,73,110,100,101,120,32,60,61,32,54,0,0,0,0,0,68,101,99,111,100,101,83,105,103,110,105,102,105,99,97,110,116,65,98,115,76,101,118,101,108,0,0,0,0,0,0,0,48,32,60,61,32,40,73,51,50,41,99,66,105,116,115,32,38,38,32,99,66,105,116,115,32,60,61,32,49,54,0,0,95,102,108,117,115,104,66,105,116,49,54,0,0,0,0,0,40,112,73,79,45,62,105,77,97,115,107,32,38,32,49,41,32,61,61,32,48,0,0,0,95,112,101,101,107,66,105,116,49,54,0,0,0,0,0,0,68,101,99,111,100,101,66,108,111,99,107,0,0,0,0,0,105,83,121,109,98,111,108,32,62,61,32,48,0,0,0,0,95,103,101,116,72,117,102,102,83,104,111,114,116,0,0,0,99,67,104,97,110,110,101,108,32,60,61,32,49,54,0,0,105,109,97,103,101,47,100,101,99,111,100,101,47,115,116,114,100,101,99,46,99,0,0,0,111,117,116,112,117,116,78,67,104,97,110,110,101,108,0,0,48,0,0,0,0,0,0,0,112,83,67,45,62,109,95,112,97,114,97,109,46,98,83,99,97,108,101,100,65,114,105,116,104,32,61,61,32,112,83,67,45,62,109,95,112,78,101,120,116,83,67,45,62,109,95,112,97,114,97,109,46,98,83,99,97,108,101,100,65,114,105,116,104,0,0,0,0,0,0,0,111,117,116,112,117,116,77,66,82,111,119,0,0,0,0,0,0,0,0,0,1,0,0,0,2,0,0,0,3,0,0,0,2,0,0,0,3,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,3,0,0,0,2,0,0,0,3,0,0,0,2,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,3,0,0,0,0,0,0,0,2,0,0,0,3,0,0,0,1,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,1,0,0,0,3,0,0,0,2,0,0,0,0,0,0,0,3,0,0,0,1,0,0,0,99,102,69,120,116,32,61,61,32,67,70,95,82,71,66,0,99,102,69,120,116,32,61,61,32,89,95,79,78,76,89,32,38,38,32,112,83,67,45,62,109,95,112,97,114,97,109,46,99,102,67,111,108,111,114,70,111,114,109,97,116,32,61,61,32,89,95,79,78,76,89,0,111,117,116,112,117,116,78,67,104,97,110,110,101,108,84,104,117,109,98,110,97,105,108,0,116,83,99,97,108,101,32,61,61,32,40,115,105,122,101,95,116,41,40,49,85,32,60,60,32,110,66,105,116,115,41,0,100,101,99,111,100,101,84,104,117,109,98,110,97,105,108,0,10,37,100,32,104,111,114,105,122,111,110,116,97,108,32,116,105,108,101,115,58,10,0,0,32,32,32,32,111,102,102,115,101,116,32,111,102,32,116,105,108,101,32,37,100,32,105,110,32,77,66,115,58,32,37,100,10,0,0,0,0,0,0,0,10,37,100,32,118,101,114,116,105,99,97,108,32,116,105,108,101,115,58,10,0,0,0,0,98,105,116,115,116,114,101,97,109,32,115,105,122,101,32,102,111,114,32,116,105,108,101,32,40,37,100,44,32,37,100,41,58,32,37,100,46,10,0,0,98,105,116,115,116,114,101,97,109,32,115,105,122,101,32,102,111,114,32,116,105,108,101,32,40,37,100,44,32,37,100,41,58,32,117,110,107,110,111,119,110,46,10,0,0,0,0,0,98,105,116,115,116,114,101,97,109,32,115,105,122,101,32,111,102,32,40,68,67,44,32,76,80,44,32,65,67,44,32,70,76,41,32,102,111,114,32,116,105,108,101,32,40,37,100,44,32,37,100,41,58,32,37,100,32,37,100,32,37,100,32,37,100,46,10,0,0,0,0,0,98,105,116,115,116,114,101,97,109,32,115,105,122,101,32,111,102,32,40,68,67,44,32,76,80,44,32,65,67,44,32,70,76,41,32,102,111,114,32,116,105,108,101,32,40,37,100,44,32,37,100,41,58,32,37,100,32,37,100,32,37,100,32,117,110,107,110,111,119,110,46,10,0,0,0,0,0,0,0,0,1,1,2,2,2,4,4,4,255,255,255,0,0,0,0,0,112,83,67,32,33,61,32,78,85,76,76,0,0,0,0,0,82,101,97,100,87,77,73,72,101,97,100,101,114,0,0,0,87,77,80,72,79,84,79,0,2,0,0,0,4,0,0,0,0,0,0,0,4,0,0,0,8,0,0,0,16,0,0,0,16,0,0,0,16,0,0,0,16,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,117,105,70,105,114,115,116,77,66,82,111,119,32,37,100,32,117,105,76,97,115,116,77,66,82,111,119,32,37,100,10,0,100,97,116,97,32,37,100,10,0,0,0,0,0,0,0,0,10,70,114,101,113,117,101,110,99,121,32,111,114,100,101,114,32,98,105,116,115,116,114,101,97,109,0,0,0,0,0,0,10,115,116,114,101,97,109,105,110,103,32,109,111,100,101,44,32,110,111,32,105,110,100,101,120,32,116,97,98,108,101,46,0,0,0,0,0,0,0,0,10,83,112,97,116,105,97,108,32,111,114,100,101,114,32,98,105,116,115,116,114,101,97,109,0,0,0,0,0,0,0,0,1,2,3,5,6,7,9,10,11,13,14,15,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,2,0,0,0,1,0,0,0,2,0,0,0,2,0,0,0,3,0,0,0,1,0,0,0,2,0,0,0,2,0,0,0,3,0,0,0,2,0,0,0,3,0,0,0,3,0,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,2,0,0,0,4,0,0,0,2,0,0,0,2,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,116,32,62,61,32,48,0,0,105,109,97,103,101,47,115,121,115,47,97,100,97,112,116,104,117,102,102,46,99,0,0,0,65,100,97,112,116,68,105,115,99,114,105,109,105,110,97,110,116,0,0,0,0,0,0,0,116,32,60,32,103,77,97,120,84,97,98,108,101,115,91,105,83,121,109,93,0,0,0,0,4,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,2,0,0,0,0,0,0,0,3,0,0,0,1,0,0,0,3,0,0,0,0,0,0,0,19,0,19,0,19,0,19,0,27,0,27,0,27,0,27,0,10,0,10,0,10,0,10,0,10,0,10,0,10,0,10,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,2,0,0,0,1,0,0,0,3,0,0,0,0,0,0,0,4,0,0,0,1,0,0,0,4,0,0,0,5,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,3,0,0,0,1,0,0,0,3,0,0,0,2,0,0,0,3,0,0,0,3,0,0,0,3,0,0,0,0,0,0,0,255,255,255,255,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,28,0,28,0,36,0,36,0,19,0,19,0,19,0,19,0,10,0,10,0,10,0,10,0,10,0,10,0,10,0,10,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,11,0,11,0,11,0,11,0,19,0,19,0,19,0,19,0,27,0,27,0,27,0,27,0,35,0,35,0,35,0,35,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,5,0,0,0,1,0,0,0,3,0,0,0,1,0,0,0,5,0,0,0,1,0,0,0,2,0,0,0,1,0,0,0,4,0,0,0,6,0,0,0,1,0,0,0,2,0,0,0,0,0,0,0,4,0,0,0,2,0,0,0,2,0,0,0,1,0,0,0,4,0,0,0,3,0,0,0,2,0,0,0,1,0,0,0,3,0,0,0,6,0,0,0,0,0,0,0,4,0,0,0,1,0,0,0,4,0,0,0,1,0,0,0,2,0,0,0,2,0,0,0,2,0,0,0,3,0,0,0,2,0,0,0,1,0,0,0,3,0,0,0,6,0,0,0,0,0,0,0,5,0,0,0,1,0,0,0,5,0,0,0,1,0,0,0,2,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,4,0,0,0,1,0,0,0,3,0,0,0,255,255,255,255,1,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,254,255,255,255,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,255,255,255,255,255,255,255,255,0,0,0,0,1,0,0,0,254,255,255,255,0,0,0,0,13,0,29,0,44,0,44,0,19,0,19,0,19,0,19,0,34,0,34,0,34,0,34,0,34,0,34,0,34,0,34,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,12,0,12,0,28,0,28,0,43,0,43,0,43,0,43,0,2,0,2,0,2,0,2,0,2,0,2,0,2,0,2,0,18,0,18,0,18,0,18,0,18,0,18,0,18,0,18,0,34,0,34,0,34,0,34,0,34,0,34,0,34,0,34,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,0,4,0,12,0,12,0,43,0,43,0,43,0,43,0,18,0,18,0,18,0,18,0,18,0,18,0,18,0,18,0,26,0,26,0,26,0,26,0,26,0,26,0,26,0,26,0,34,0,34,0,34,0,34,0,34,0,34,0,34,0,34,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,0,13,0,36,0,36,0,43,0,43,0,43,0,43,0,18,0,18,0,18,0,18,0,18,0,18,0,18,0,18,0,25,0,25,0,25,0,25,0,25,0,25,0,25,0,25,0,25,0,25,0,25,0,25,0,25,0,25,0,25,0,25,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,0,0,0,1,0,0,0,2,0,0,0,2,0,0,0,2,0,0,0,3,0,0,0,2,0,0,0,1,0,0,0,3,0,0,0,1,0,0,0,4,0,0,0,0,0,0,0,5,0,0,0,1,0,0,0,5,0,0,0,7,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,2,0,0,0,1,0,0,0,3,0,0,0,1,0,0,0,4,0,0,0,1,0,0,0,5,0,0,0,0,0,0,0,6,0,0,0,1,0,0,0,6,0,0,0,1,0,0,0,0,0,0,0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,0,0,0,0,45,0,53,0,36,0,36,0,27,0,27,0,27,0,27,0,2,0,2,0,2,0,2,0,2,0,2,0,2,0,2,0,10,0,10,0,10,0,10,0,10,0,10,0,10,0,10,0,18,0,18,0,18,0,18,0,18,0,18,0,18,0,18,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,32,128,37,0,28,0,28,0,19,0,19,0,19,0,19,0,10,0,10,0,10,0,10,0,10,0,10,0,10,0,10,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,5,0,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,8,0,0,0,2,0,0,0,2,0,0,0,1,0,0,0,3,0,0,0,1,0,0,0,5,0,0,0,1,0,0,0,4,0,0,0,3,0,0,0,2,0,0,0,2,0,0,0,3,0,0,0,0,0,0,0,5,0,0,0,3,0,0,0,3,0,0,0,8,0,0,0,1,0,0,0,3,0,0,0,2,0,0,0,3,0,0,0,1,0,0,0,4,0,0,0,3,0,0,0,3,0,0,0,4,0,0,0,3,0,0,0,5,0,0,0,3,0,0,0,0,0,0,0,4,0,0,0,3,0,0,0,2,0,0,0,53,0,21,0,28,0,28,0,11,0,11,0,11,0,11,0,43,0,43,0,43,0,43,0,59,0,59,0,59,0,59,0,2,0,2,0,2,0,2,0,2,0,2,0,2,0,2,0,34,0,34,0,34,0,34,0,34,0,34,0,34,0,34,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,52,0,52,0,20,0,20,0,3,0,3,0,3,0,3,0,11,0,11,0,11,0,11,0,27,0,27,0,27,0,27,0,35,0,35,0,35,0,35,0,43,0,43,0,43,0,43,0,58,0,58,0,58,0,58,0,58,0,58,0,58,0,58,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,9,0,0,0,2,0,0,0,3,0,0,0,0,0,0,0,5,0,0,0,2,0,0,0,4,0,0,0,1,0,0,0,5,0,0,0,2,0,0,0,5,0,0,0,1,0,0,0,1,0,0,0,3,0,0,0,3,0,0,0,3,0,0,0,5,0,0,0,3,0,0,0,4,0,0,0,9,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,3,0,0,0,2,0,0,0,3,0,0,0,1,0,0,0,4,0,0,0,1,0,0,0,6,0,0,0,3,0,0,0,3,0,0,0,1,0,0,0,5,0,0,0,0,0,0,0,7,0,0,0,1,0,0,0,7,0,0,0,2,0,0,0,2,0,0,0,1,0,0,0,1,0,0,0,255,255,255,255,254,255,255,255,254,255,255,255,254,255,255,255,253,255,255,255,0,0,0,0,13,0,29,0,37,0,61,0,20,0,20,0,68,0,68,0,3,0,3,0,3,0,3,0,51,0,51,0,51,0,51,0,41,0,41,0,41,0,41,0,41,0,41,0,41,0,41,0,41,0,41,0,41,0,41,0,41,0,41,0,41,0,41,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,32,128,53,0,28,0,28,0,11,0,11,0,11,0,11,0,19,0,19,0,19,0,19,0,43,0,43,0,43,0,43,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,34,128,4,0,7,0,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,12,0,0,0,1,0,0,0,5,0,0,0,1,0,0,0,6,0,0,0,0,0,0,0,7,0,0,0,1,0,0,0,7,0,0,0,4,0,0,0,5,0,0,0,2,0,0,0,3,0,0,0,5,0,0,0,5,0,0,0,1,0,0,0,1,0,0,0,6,0,0,0,5,0,0,0,1,0,0,0,4,0,0,0,7,0,0,0,5,0,0,0,3,0,0,0,3,0,0,0,12,0,0,0,2,0,0,0,4,0,0,0,2,0,0,0,5,0,0,0,0,0,0,0,6,0,0,0,1,0,0,0,6,0,0,0,3,0,0,0,4,0,0,0,2,0,0,0,3,0,0,0,3,0,0,0,5,0,0,0,3,0,0,0,2,0,0,0,3,0,0,0,3,0,0,0,4,0,0,0,3,0,0,0,1,0,0,0,5,0,0,0,5,0,0,0,3,0,0,0,12,0,0,0,3,0,0,0,2,0,0,0,1,0,0,0,3,0,0,0,0,0,0,0,7,0,0,0,1,0,0,0,7,0,0,0,1,0,0,0,5,0,0,0,2,0,0,0,3,0,0,0,2,0,0,0,7,0,0,0,3,0,0,0,3,0,0,0,4,0,0,0,3,0,0,0,5,0,0,0,3,0,0,0,3,0,0,0,7,0,0,0,1,0,0,0,4,0,0,0,12,0,0,0,1,0,0,0,3,0,0,0,3,0,0,0,2,0,0,0,0,0,0,0,7,0,0,0,1,0,0,0,5,0,0,0,2,0,0,0,5,0,0,0,2,0,0,0,3,0,0,0,1,0,0,0,7,0,0,0,3,0,0,0,3,0,0,0,3,0,0,0,5,0,0,0,4,0,0,0,3,0,0,0,1,0,0,0,6,0,0,0,5,0,0,0,3,0,0,0,12,0,0,0,2,0,0,0,3,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,7,0,0,0,1,0,0,0,4,0,0,0,2,0,0,0,7,0,0,0,3,0,0,0,3,0,0,0,0,0,0,0,8,0,0,0,2,0,0,0,4,0,0,0,3,0,0,0,7,0,0,0,3,0,0,0,4,0,0,0,1,0,0,0,8,0,0,0,1,0,0,0,5,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,255,255,255,255,2,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,2,0,0,0,255,255,255,255,255,255,255,255,255,255,255,255,0,0,0,0,254,255,255,255,255,255,255,255,0,0,0,0,0,0,0,0,254,255,255,255,255,255,255,255,255,255,255,255,1,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,254,255,255,255,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,254,255,255,255,0,0,0,0,255,255,255,255,255,255,255,255,254,255,255,255,255,255,255,255,254,255,255,255,254,255,255,255,32,128,5,0,76,0,76,0,37,0,53,0,69,0,85,0,43,0,43,0,43,0,43,0,91,0,91,0,91,0,91,0,57,0,57,0,57,0,57,0,57,0,57,0,57,0,57,0,57,0,57,0,57,0,57,0,57,0,57,0,57,0,57,0,34,128,1,0,2,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,32,128,85,0,13,0,53,0,4,0,4,0,36,0,36,0,43,0,43,0,43,0,43,0,67,0,67,0,67,0,67,0,75,0,75,0,75,0,75,0,91,0,91,0,91,0,91,0,58,0,58,0,58,0,58,0,58,0,58,0,58,0,58,0,2,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,32,128,37,0,92,0,92,0,11,0,11,0,11,0,11,0,43,0,43,0,43,0,43,0,59,0,59,0,59,0,59,0,67,0,67,0,67,0,67,0,75,0,75,0,75,0,75,0,2,0,2,0,2,0,2,0,2,0,2,0,2,0,2,0,34,128,36,128,2,0,3,0,6,0,10,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,32,128,29,0,37,0,69,0,3,0,3,0,3,0,3,0,43,0,43,0,43,0,43,0,59,0,59,0,59,0,59,0,75,0,75,0,75,0,75,0,91,0,91,0,91,0,91,0,10,0,10,0,10,0,10,0,10,0,10,0,10,0,10,0,34,128,10,0,2,0,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,32,128,93,0,28,0,28,0,60,0,60,0,76,0,76,0,3,0,3,0,3,0,3,0,43,0,43,0,43,0,43,0,9,0,9,0,9,0,9,0,9,0,9,0,9,0,9,0,9,0,9,0,9,0,9,0,9,0,9,0,9,0,9,0,34,128,36,128,38,128,2,0,4,0,8,0,6,0,10,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,48,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,4,0,0,0,5,0,0,0,2,0,0,0,8,0,0,0,6,0,0,0,9,0,0,0,3,0,0,0,12,0,0,0,10,0,0,0,7,0,0,0,13,0,0,0,11,0,0,0,14,0,0,0,15,0,0,0,0,0,0,0,1,0,0,0,4,0,0,0,5,0,0,0,2,0,0,0,8,0,0,0,6,0,0,0,9,0,0,0,3,0,0,0,12,0,0,0,10,0,0,0,7,0,0,0,13,0,0,0,11,0,0,0,14,0,0,0,15,0,0,0,0,0,0,0,4,0,0,0,8,0,0,0,5,0,0,0,1,0,0,0,12,0,0,0,9,0,0,0,6,0,0,0,2,0,0,0,13,0,0,0,3,0,0,0,15,0,0,0,7,0,0,0,10,0,0,0,14,0,0,0,11,0,0,0,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,2,0,0,0,2,0,0,0,2,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,240,0,0,0,12,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,240,0,0,0,120,0,0,0,80,0,0,0,60,0,0,0,48,0,0,0,40,0,0,0,34,0,0,0,30,0,0,0,27,0,0,0,24,0,0,0,22,0,0,0,20,0,0,0,18,0,0,0,17,0,0,0,16,0,0,0,0,0,0,0,12,0,0,0,6,0,0,0,4,0,0,0,3,0,0,0,2,0,0,0,2,0,0,0,2,0,0,0,2,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,16,0,0,0,8,0,0,0,5,0,0,0,4,0,0,0,3,0,0,0,3,0,0,0,2,0,0,0,2,0,0,0,2,0,0,0,2,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,120,0,0,0,37,0,0,0,2,0,0,0,120,0,0,0,18,0,0,0,1,0,0,0,0,0,0,0,64,0,0,0,16,0,0,0,80,0,0,0,128,0,0,0,192,0,0,0,144,0,0,0,208,0,0,0,32,0,0,0,96,0,0,0,48,0,0,0,112,0,0,0,160,0,0,0,224,0,0,0,176,0,0,0,240,0,0,0,0,0,0,0,32,0,0,0,16,0,0,0,48,0,0,0,0,0,0,0,64,0,0,0,16,0,0,0,80,0,0,0,32,0,0,0,96,0,0,0,48,0,0,0,112,0,0,0,0,0,0,0,5,0,0,0,1,0,0,0,6,0,0,0,10,0,0,0,12,0,0,0,8,0,0,0,14,0,0,0,2,0,0,0,4,0,0,0,3,0,0,0,7,0,0,0,9,0,0,0,13,0,0,0,11,0,0,0,15,0,0,0,0,0,0,0,5,0,0,0,1,0,0,0,6,0,0,0,10,0,0,0,12,0,0,0,8,0,0,0,14,0,0,0,2,0,0,0,4,0,0,0,3,0,0,0,7,0,0,0,9,0,0,0,13,0,0,0,11,0,0,0,15,0,0,0,0,0,0,0,128,0,0,0,64,0,0,0,208,0,0,0,32,0,0,0,240,0,0,0,48,0,0,0,224,0,0,0,16,0,0,0,192,0,0,0,80,0,0,0,144,0,0,0,112,0,0,0,176,0,0,0,96,0,0,0,160,0,0,0,0,1,5,4,64,65,69,68,128,129,133,132,192,193,197,196,2,3,7,6,66,67,71,70,130,131,135,134,194,195,199,198,10,11,15,14,74,75,79,78,138,139,143,142,202,203,207,206,8,9,13,12,72,73,77,76,136,137,141,140,200,201,205,204,16,17,21,20,80,81,85,84,144,145,149,148,208,209,213,212,18,19,23,22,82,83,87,86,146,147,151,150,210,211,215,214,26,27,31,30,90,91,95,94,154,155,159,158,218,219,223,222,24,25,29,28,88,89,93,92,152,153,157,156,216,217,221,220,32,33,37,36,96,97,101,100,160,161,165,164,224,225,229,228,34,35,39,38,98,99,103,102,162,163,167,166,226,227,231,230,42,43,47,46,106,107,111,110,170,171,175,174,234,235,239,238,40,41,45,44,104,105,109,108,168,169,173,172,232,233,237,236,48,49,53,52,112,113,117,116,176,177,181,180,240,241,245,244,50,51,55,54,114,115,119,118,178,179,183,182,242,243,247,246,58,59,63,62,122,123,127,126,186,187,191,190,250,251,255,254,56,57,61,60,120,121,125,124,184,185,189,188,248,249,253,252,0,1,5,4,32,33,37,36,2,3,7,6,34,35,39,38,10,11,15,14,42,43,47,46,8,9,13,12,40,41,45,44,16,17,21,20,48,49,53,52,18,19,23,22,50,51,55,54,26,27,31,30,58,59,63,62,24,25,29,28,56,57,61,60,87,77,80,72,79,84,79,0,0,0,0,0,4,0,0,0,8,0,0,0,16,0,0,0,16,0,0,0,16,0,0,0,16,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,112,83,67,45,62,109,95,98,83,101,99,111,110,100,97,114,121,32,61,61,32,70,65,76,83,69,0,0,0,0,0,0,105,109,97,103,101,47,115,121,115,47,115,116,114,99,111,100,101,99,46,99,0,0,0,0,97,100,118,97,110,99,101,77,82,80,116,114,0,0,0,0,67,97,110,110,111,116,32,111,112,101,110,32,102,105,108,101,32,37,115,10,0,0,0,0,48,32,61,61,32,112,83,66,45,62,99,66,105,116,76,101,102,116,0,0,0,0,0,0,100,101,116,97,99,104,95,83,66,0,0,0,0,0,0,0,112,83,67,45,62,87,77,73,83,67,80,46,98,102,66,105,116,115,116,114,101,97,109,70,111,114,109,97,116,32,61,61,32,83,80,65,84,73,65,76,32,38,38,32,112,83,67,45,62,87,77,73,83,67,80,46,99,78,117,109,79,102,83,108,105,99,101,77,105,110,117,115,49,72,32,43,32,112,83,67,45,62,87,77,73,83,67,80,46,99,78,117,109,79,102,83,108,105,99,101,77,105,110,117,115,49,86,32,61,61,32,48,0,0,0,0,0,0,0,0,97,108,108,111,99,97,116,101,66,105,116,73,79,73,110,102,111,0,0,0,0,0,0,0,48,32,60,61,32,40,73,51,50,41,99,66,105,116,115,32,38,38,32,99,66,105,116,115,32,60,61,32,49,54,0,0,112,101,101,107,66,105,116,49,54,0,0,0,0,0,0,0,102,108,117,115,104,66,105,116,49,54,0,0,0,0,0,0,40,112,73,79,45,62,105,77,97,115,107,32,38,32,49,41,32,61,61,32,48,0,0,0,48,32,60,61,32,40,73,51,50,41,99,66,105,116,115,32,38,38,32,99,66,105,116,115,32,60,61,32,51,50,0,0,103,101,116,66,105,116,51,50,0,0,0,0,0,0,0,0,99,66,105,116,115,32,60,61,32,49,54,0,0,0,0,0,112,117,116,66,105,116,49,54,122,0,0,0,0,0,0,0,48,32,61,61,32,117,105,66,105,116,115,32,62,62,32,99,66,105,116,115,0,0,0,0,112,117,116,66,105,116,49,54,0,0,0,0,0,0,0,0,112,117,116,66,105,116,51,50,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,171,170,170,170,1,0,0,0,0,0,0,0,2,0,0,0,205,204,204,204,2,0,0,0,171,170,170,170,2,0,0,0,147,36,73,146,2,0,0,0,0,0,0,0,3,0,0,0,228,56,142,227,3,0,0,0,205,204,204,204,3,0,0,0,163,139,46,186,3,0,0,0,171,170,170,170,3,0,0,0,158,216,137,157,3,0,0,0,147,36,73,146,3,0,0,0,137,136,136,136,3,0,0,0,0,0,0,0,4,0,0,0,241,240,240,240,4,0,0,0,228,56,142,227,4,0,0,0,230,53,148,215,4,0,0,0,205,204,204,204,4,0,0,0,196,48,12,195,4,0,0,0,163,139,46,186,4,0,0,0,201,66,22,178,4,0,0,0,171,170,170,170,4,0,0,0,62,10,215,163,4,0,0,0,158,216,137,157,4,0,0,0,238,37,180,151,4,0,0,0,147,36,73,146,4,0,0,0,9,203,61,141,4,0,0,0,137,136,136,136,4,0,0,0,67,8,33,132,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], "i8", ALLOC_NONE, Runtime.GLOBAL_BASE);




var tempDoublePtr = Runtime.alignMemory(allocate(12, "i8", ALLOC_STATIC), 8);

assert(tempDoublePtr % 8 == 0);

function copyTempFloat(ptr) { // functions, because inlining this code increases code size too much

  HEAP8[tempDoublePtr] = HEAP8[ptr];

  HEAP8[tempDoublePtr+1] = HEAP8[ptr+1];

  HEAP8[tempDoublePtr+2] = HEAP8[ptr+2];

  HEAP8[tempDoublePtr+3] = HEAP8[ptr+3];

}

function copyTempDouble(ptr) {

  HEAP8[tempDoublePtr] = HEAP8[ptr];

  HEAP8[tempDoublePtr+1] = HEAP8[ptr+1];

  HEAP8[tempDoublePtr+2] = HEAP8[ptr+2];

  HEAP8[tempDoublePtr+3] = HEAP8[ptr+3];

  HEAP8[tempDoublePtr+4] = HEAP8[ptr+4];

  HEAP8[tempDoublePtr+5] = HEAP8[ptr+5];

  HEAP8[tempDoublePtr+6] = HEAP8[ptr+6];

  HEAP8[tempDoublePtr+7] = HEAP8[ptr+7];

}


  
   
  Module["_rand_r"] = _rand_r;
  
  var ___rand_seed=allocate([0x0273459b, 0, 0, 0], "i32", ALLOC_STATIC); 
  Module["_rand"] = _rand;

   
  Module["_memset"] = _memset;

  function _time(ptr) {
      var ret = Math.floor(Date.now()/1000);
      if (ptr) {
        HEAP32[((ptr)>>2)]=ret;
      }
      return ret;
    }

  function ___assert_fail(condition, filename, line, func) {
      ABORT = true;
      throw 'Assertion failed: ' + Pointer_stringify(condition) + ', at: ' + [filename ? Pointer_stringify(filename) : 'unknown filename', line, func ? Pointer_stringify(func) : 'unknown function'] + ' at ' + stackTrace();
    }

  
  
  var ERRNO_CODES={EPERM:1,ENOENT:2,ESRCH:3,EINTR:4,EIO:5,ENXIO:6,E2BIG:7,ENOEXEC:8,EBADF:9,ECHILD:10,EAGAIN:11,EWOULDBLOCK:11,ENOMEM:12,EACCES:13,EFAULT:14,ENOTBLK:15,EBUSY:16,EEXIST:17,EXDEV:18,ENODEV:19,ENOTDIR:20,EISDIR:21,EINVAL:22,ENFILE:23,EMFILE:24,ENOTTY:25,ETXTBSY:26,EFBIG:27,ENOSPC:28,ESPIPE:29,EROFS:30,EMLINK:31,EPIPE:32,EDOM:33,ERANGE:34,ENOMSG:42,EIDRM:43,ECHRNG:44,EL2NSYNC:45,EL3HLT:46,EL3RST:47,ELNRNG:48,EUNATCH:49,ENOCSI:50,EL2HLT:51,EDEADLK:35,ENOLCK:37,EBADE:52,EBADR:53,EXFULL:54,ENOANO:55,EBADRQC:56,EBADSLT:57,EDEADLOCK:35,EBFONT:59,ENOSTR:60,ENODATA:61,ETIME:62,ENOSR:63,ENONET:64,ENOPKG:65,EREMOTE:66,ENOLINK:67,EADV:68,ESRMNT:69,ECOMM:70,EPROTO:71,EMULTIHOP:72,EDOTDOT:73,EBADMSG:74,ENOTUNIQ:76,EBADFD:77,EREMCHG:78,ELIBACC:79,ELIBBAD:80,ELIBSCN:81,ELIBMAX:82,ELIBEXEC:83,ENOSYS:38,ENOTEMPTY:39,ENAMETOOLONG:36,ELOOP:40,EOPNOTSUPP:95,EPFNOSUPPORT:96,ECONNRESET:104,ENOBUFS:105,EAFNOSUPPORT:97,EPROTOTYPE:91,ENOTSOCK:88,ENOPROTOOPT:92,ESHUTDOWN:108,ECONNREFUSED:111,EADDRINUSE:98,ECONNABORTED:103,ENETUNREACH:101,ENETDOWN:100,ETIMEDOUT:110,EHOSTDOWN:112,EHOSTUNREACH:113,EINPROGRESS:115,EALREADY:114,EDESTADDRREQ:89,EMSGSIZE:90,EPROTONOSUPPORT:93,ESOCKTNOSUPPORT:94,EADDRNOTAVAIL:99,ENETRESET:102,EISCONN:106,ENOTCONN:107,ETOOMANYREFS:109,EUSERS:87,EDQUOT:122,ESTALE:116,ENOTSUP:95,ENOMEDIUM:123,EILSEQ:84,EOVERFLOW:75,ECANCELED:125,ENOTRECOVERABLE:131,EOWNERDEAD:130,ESTRPIPE:86};
  
  var ERRNO_MESSAGES={0:"Success",1:"Not super-user",2:"No such file or directory",3:"No such process",4:"Interrupted system call",5:"I/O error",6:"No such device or address",7:"Arg list too long",8:"Exec format error",9:"Bad file number",10:"No children",11:"No more processes",12:"Not enough core",13:"Permission denied",14:"Bad address",15:"Block device required",16:"Mount device busy",17:"File exists",18:"Cross-device link",19:"No such device",20:"Not a directory",21:"Is a directory",22:"Invalid argument",23:"Too many open files in system",24:"Too many open files",25:"Not a typewriter",26:"Text file busy",27:"File too large",28:"No space left on device",29:"Illegal seek",30:"Read only file system",31:"Too many links",32:"Broken pipe",33:"Math arg out of domain of func",34:"Math result not representable",35:"File locking deadlock error",36:"File or path name too long",37:"No record locks available",38:"Function not implemented",39:"Directory not empty",40:"Too many symbolic links",42:"No message of desired type",43:"Identifier removed",44:"Channel number out of range",45:"Level 2 not synchronized",46:"Level 3 halted",47:"Level 3 reset",48:"Link number out of range",49:"Protocol driver not attached",50:"No CSI structure available",51:"Level 2 halted",52:"Invalid exchange",53:"Invalid request descriptor",54:"Exchange full",55:"No anode",56:"Invalid request code",57:"Invalid slot",59:"Bad font file fmt",60:"Device not a stream",61:"No data (for no delay io)",62:"Timer expired",63:"Out of streams resources",64:"Machine is not on the network",65:"Package not installed",66:"The object is remote",67:"The link has been severed",68:"Advertise error",69:"Srmount error",70:"Communication error on send",71:"Protocol error",72:"Multihop attempted",73:"Cross mount point (not really error)",74:"Trying to read unreadable message",75:"Value too large for defined data type",76:"Given log. name not unique",77:"f.d. invalid for this operation",78:"Remote address changed",79:"Can   access a needed shared lib",80:"Accessing a corrupted shared lib",81:".lib section in a.out corrupted",82:"Attempting to link in too many libs",83:"Attempting to exec a shared library",84:"Illegal byte sequence",86:"Streams pipe error",87:"Too many users",88:"Socket operation on non-socket",89:"Destination address required",90:"Message too long",91:"Protocol wrong type for socket",92:"Protocol not available",93:"Unknown protocol",94:"Socket type not supported",95:"Not supported",96:"Protocol family not supported",97:"Address family not supported by protocol family",98:"Address already in use",99:"Address not available",100:"Network interface is not configured",101:"Network is unreachable",102:"Connection reset by network",103:"Connection aborted",104:"Connection reset by peer",105:"No buffer space available",106:"Socket is already connected",107:"Socket is not connected",108:"Can't send after socket shutdown",109:"Too many references",110:"Connection timed out",111:"Connection refused",112:"Host is down",113:"Host is unreachable",114:"Socket already connected",115:"Connection already in progress",116:"Stale file handle",122:"Quota exceeded",123:"No medium (in tape drive)",125:"Operation canceled",130:"Previous owner died",131:"State not recoverable"};
  
  
  var ___errno_state=0;function ___setErrNo(value) {
      // For convenient setting and returning of errno.
      HEAP32[((___errno_state)>>2)]=value;
      return value;
    }
  
  var PATH={splitPath:function (filename) {
        var splitPathRe = /^(\/?|)([\s\S]*?)((?:\.{1,2}|[^\/]+?|)(\.[^.\/]*|))(?:[\/]*)$/;
        return splitPathRe.exec(filename).slice(1);
      },normalizeArray:function (parts, allowAboveRoot) {
        // if the path tries to go above the root, `up` ends up > 0
        var up = 0;
        for (var i = parts.length - 1; i >= 0; i--) {
          var last = parts[i];
          if (last === '.') {
            parts.splice(i, 1);
          } else if (last === '..') {
            parts.splice(i, 1);
            up++;
          } else if (up) {
            parts.splice(i, 1);
            up--;
          }
        }
        // if the path is allowed to go above the root, restore leading ..s
        if (allowAboveRoot) {
          for (; up--; up) {
            parts.unshift('..');
          }
        }
        return parts;
      },normalize:function (path) {
        var isAbsolute = path.charAt(0) === '/',
            trailingSlash = path.substr(-1) === '/';
        // Normalize the path
        path = PATH.normalizeArray(path.split('/').filter(function(p) {
          return !!p;
        }), !isAbsolute).join('/');
        if (!path && !isAbsolute) {
          path = '.';
        }
        if (path && trailingSlash) {
          path += '/';
        }
        return (isAbsolute ? '/' : '') + path;
      },dirname:function (path) {
        var result = PATH.splitPath(path),
            root = result[0],
            dir = result[1];
        if (!root && !dir) {
          // No dirname whatsoever
          return '.';
        }
        if (dir) {
          // It has a dirname, strip trailing slash
          dir = dir.substr(0, dir.length - 1);
        }
        return root + dir;
      },basename:function (path) {
        // EMSCRIPTEN return '/'' for '/', not an empty string
        if (path === '/') return '/';
        var lastSlash = path.lastIndexOf('/');
        if (lastSlash === -1) return path;
        return path.substr(lastSlash+1);
      },extname:function (path) {
        return PATH.splitPath(path)[3];
      },join:function () {
        var paths = Array.prototype.slice.call(arguments, 0);
        return PATH.normalize(paths.join('/'));
      },join2:function (l, r) {
        return PATH.normalize(l + '/' + r);
      },resolve:function () {
        var resolvedPath = '',
          resolvedAbsolute = false;
        for (var i = arguments.length - 1; i >= -1 && !resolvedAbsolute; i--) {
          var path = (i >= 0) ? arguments[i] : FS.cwd();
          // Skip empty and invalid entries
          if (typeof path !== 'string') {
            throw new TypeError('Arguments to path.resolve must be strings');
          } else if (!path) {
            continue;
          }
          resolvedPath = path + '/' + resolvedPath;
          resolvedAbsolute = path.charAt(0) === '/';
        }
        // At this point the path should be resolved to a full absolute path, but
        // handle relative paths to be safe (might happen when process.cwd() fails)
        resolvedPath = PATH.normalizeArray(resolvedPath.split('/').filter(function(p) {
          return !!p;
        }), !resolvedAbsolute).join('/');
        return ((resolvedAbsolute ? '/' : '') + resolvedPath) || '.';
      },relative:function (from, to) {
        from = PATH.resolve(from).substr(1);
        to = PATH.resolve(to).substr(1);
        function trim(arr) {
          var start = 0;
          for (; start < arr.length; start++) {
            if (arr[start] !== '') break;
          }
          var end = arr.length - 1;
          for (; end >= 0; end--) {
            if (arr[end] !== '') break;
          }
          if (start > end) return [];
          return arr.slice(start, end - start + 1);
        }
        var fromParts = trim(from.split('/'));
        var toParts = trim(to.split('/'));
        var length = Math.min(fromParts.length, toParts.length);
        var samePartsLength = length;
        for (var i = 0; i < length; i++) {
          if (fromParts[i] !== toParts[i]) {
            samePartsLength = i;
            break;
          }
        }
        var outputParts = [];
        for (var i = samePartsLength; i < fromParts.length; i++) {
          outputParts.push('..');
        }
        outputParts = outputParts.concat(toParts.slice(samePartsLength));
        return outputParts.join('/');
      }};
  
  var TTY={ttys:[],init:function () {
        // https://github.com/kripken/emscripten/pull/1555
        // if (ENVIRONMENT_IS_NODE) {
        //   // currently, FS.init does not distinguish if process.stdin is a file or TTY
        //   // device, it always assumes it's a TTY device. because of this, we're forcing
        //   // process.stdin to UTF8 encoding to at least make stdin reading compatible
        //   // with text files until FS.init can be refactored.
        //   process['stdin']['setEncoding']('utf8');
        // }
      },shutdown:function () {
        // https://github.com/kripken/emscripten/pull/1555
        // if (ENVIRONMENT_IS_NODE) {
        //   // inolen: any idea as to why node -e 'process.stdin.read()' wouldn't exit immediately (with process.stdin being a tty)?
        //   // isaacs: because now it's reading from the stream, you've expressed interest in it, so that read() kicks off a _read() which creates a ReadReq operation
        //   // inolen: I thought read() in that case was a synchronous operation that just grabbed some amount of buffered data if it exists?
        //   // isaacs: it is. but it also triggers a _read() call, which calls readStart() on the handle
        //   // isaacs: do process.stdin.pause() and i'd think it'd probably close the pending call
        //   process['stdin']['pause']();
        // }
      },register:function (dev, ops) {
        TTY.ttys[dev] = { input: [], output: [], ops: ops };
        FS.registerDevice(dev, TTY.stream_ops);
      },stream_ops:{open:function (stream) {
          var tty = TTY.ttys[stream.node.rdev];
          if (!tty) {
            throw new FS.ErrnoError(ERRNO_CODES.ENODEV);
          }
          stream.tty = tty;
          stream.seekable = false;
        },close:function (stream) {
          // flush any pending line data
          if (stream.tty.output.length) {
            stream.tty.ops.put_char(stream.tty, 10);
          }
        },read:function (stream, buffer, offset, length, pos /* ignored */) {
          if (!stream.tty || !stream.tty.ops.get_char) {
            throw new FS.ErrnoError(ERRNO_CODES.ENXIO);
          }
          var bytesRead = 0;
          for (var i = 0; i < length; i++) {
            var result;
            try {
              result = stream.tty.ops.get_char(stream.tty);
            } catch (e) {
              throw new FS.ErrnoError(ERRNO_CODES.EIO);
            }
            if (result === undefined && bytesRead === 0) {
              throw new FS.ErrnoError(ERRNO_CODES.EAGAIN);
            }
            if (result === null || result === undefined) break;
            bytesRead++;
            buffer[offset+i] = result;
          }
          if (bytesRead) {
            stream.node.timestamp = Date.now();
          }
          return bytesRead;
        },write:function (stream, buffer, offset, length, pos) {
          if (!stream.tty || !stream.tty.ops.put_char) {
            throw new FS.ErrnoError(ERRNO_CODES.ENXIO);
          }
          for (var i = 0; i < length; i++) {
            try {
              stream.tty.ops.put_char(stream.tty, buffer[offset+i]);
            } catch (e) {
              throw new FS.ErrnoError(ERRNO_CODES.EIO);
            }
          }
          if (length) {
            stream.node.timestamp = Date.now();
          }
          return i;
        }},default_tty_ops:{get_char:function (tty) {
          if (!tty.input.length) {
            var result = null;
            if (ENVIRONMENT_IS_NODE) {
              result = process['stdin']['read']();
              if (!result) {
                if (process['stdin']['_readableState'] && process['stdin']['_readableState']['ended']) {
                  return null;  // EOF
                }
                return undefined;  // no data available
              }
            } else if (typeof window != 'undefined' &&
              typeof window.prompt == 'function') {
              // Browser.
              result = window.prompt('Input: ');  // returns null on cancel
              if (result !== null) {
                result += '\n';
              }
            } else if (typeof readline == 'function') {
              // Command line.
              result = readline();
              if (result !== null) {
                result += '\n';
              }
            }
            if (!result) {
              return null;
            }
            tty.input = intArrayFromString(result, true);
          }
          return tty.input.shift();
        },put_char:function (tty, val) {
          if (val === null || val === 10) {
            Module['print'](tty.output.join(''));
            tty.output = [];
          } else {
            tty.output.push(TTY.utf8.processCChar(val));
          }
        }},default_tty1_ops:{put_char:function (tty, val) {
          if (val === null || val === 10) {
            Module['printErr'](tty.output.join(''));
            tty.output = [];
          } else {
            tty.output.push(TTY.utf8.processCChar(val));
          }
        }}};
  
  var MEMFS={ops_table:null,CONTENT_OWNING:1,CONTENT_FLEXIBLE:2,CONTENT_FIXED:3,mount:function (mount) {
        return MEMFS.createNode(null, '/', 16384 | 511 /* 0777 */, 0);
      },createNode:function (parent, name, mode, dev) {
        if (FS.isBlkdev(mode) || FS.isFIFO(mode)) {
          // no supported
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        if (!MEMFS.ops_table) {
          MEMFS.ops_table = {
            dir: {
              node: {
                getattr: MEMFS.node_ops.getattr,
                setattr: MEMFS.node_ops.setattr,
                lookup: MEMFS.node_ops.lookup,
                mknod: MEMFS.node_ops.mknod,
                rename: MEMFS.node_ops.rename,
                unlink: MEMFS.node_ops.unlink,
                rmdir: MEMFS.node_ops.rmdir,
                readdir: MEMFS.node_ops.readdir,
                symlink: MEMFS.node_ops.symlink
              },
              stream: {
                llseek: MEMFS.stream_ops.llseek
              }
            },
            file: {
              node: {
                getattr: MEMFS.node_ops.getattr,
                setattr: MEMFS.node_ops.setattr
              },
              stream: {
                llseek: MEMFS.stream_ops.llseek,
                read: MEMFS.stream_ops.read,
                write: MEMFS.stream_ops.write,
                allocate: MEMFS.stream_ops.allocate,
                mmap: MEMFS.stream_ops.mmap
              }
            },
            link: {
              node: {
                getattr: MEMFS.node_ops.getattr,
                setattr: MEMFS.node_ops.setattr,
                readlink: MEMFS.node_ops.readlink
              },
              stream: {}
            },
            chrdev: {
              node: {
                getattr: MEMFS.node_ops.getattr,
                setattr: MEMFS.node_ops.setattr
              },
              stream: FS.chrdev_stream_ops
            },
          };
        }
        var node = FS.createNode(parent, name, mode, dev);
        if (FS.isDir(node.mode)) {
          node.node_ops = MEMFS.ops_table.dir.node;
          node.stream_ops = MEMFS.ops_table.dir.stream;
          node.contents = {};
        } else if (FS.isFile(node.mode)) {
          node.node_ops = MEMFS.ops_table.file.node;
          node.stream_ops = MEMFS.ops_table.file.stream;
          node.contents = [];
          node.contentMode = MEMFS.CONTENT_FLEXIBLE;
        } else if (FS.isLink(node.mode)) {
          node.node_ops = MEMFS.ops_table.link.node;
          node.stream_ops = MEMFS.ops_table.link.stream;
        } else if (FS.isChrdev(node.mode)) {
          node.node_ops = MEMFS.ops_table.chrdev.node;
          node.stream_ops = MEMFS.ops_table.chrdev.stream;
        }
        node.timestamp = Date.now();
        // add the new node to the parent
        if (parent) {
          parent.contents[name] = node;
        }
        return node;
      },ensureFlexible:function (node) {
        if (node.contentMode !== MEMFS.CONTENT_FLEXIBLE) {
          var contents = node.contents;
          node.contents = Array.prototype.slice.call(contents);
          node.contentMode = MEMFS.CONTENT_FLEXIBLE;
        }
      },node_ops:{getattr:function (node) {
          var attr = {};
          // device numbers reuse inode numbers.
          attr.dev = FS.isChrdev(node.mode) ? node.id : 1;
          attr.ino = node.id;
          attr.mode = node.mode;
          attr.nlink = 1;
          attr.uid = 0;
          attr.gid = 0;
          attr.rdev = node.rdev;
          if (FS.isDir(node.mode)) {
            attr.size = 4096;
          } else if (FS.isFile(node.mode)) {
            attr.size = node.contents.length;
          } else if (FS.isLink(node.mode)) {
            attr.size = node.link.length;
          } else {
            attr.size = 0;
          }
          attr.atime = new Date(node.timestamp);
          attr.mtime = new Date(node.timestamp);
          attr.ctime = new Date(node.timestamp);
          // NOTE: In our implementation, st_blocks = Math.ceil(st_size/st_blksize),
          //       but this is not required by the standard.
          attr.blksize = 4096;
          attr.blocks = Math.ceil(attr.size / attr.blksize);
          return attr;
        },setattr:function (node, attr) {
          if (attr.mode !== undefined) {
            node.mode = attr.mode;
          }
          if (attr.timestamp !== undefined) {
            node.timestamp = attr.timestamp;
          }
          if (attr.size !== undefined) {
            MEMFS.ensureFlexible(node);
            var contents = node.contents;
            if (attr.size < contents.length) contents.length = attr.size;
            else while (attr.size > contents.length) contents.push(0);
          }
        },lookup:function (parent, name) {
          throw FS.genericErrors[ERRNO_CODES.ENOENT];
        },mknod:function (parent, name, mode, dev) {
          return MEMFS.createNode(parent, name, mode, dev);
        },rename:function (old_node, new_dir, new_name) {
          // if we're overwriting a directory at new_name, make sure it's empty.
          if (FS.isDir(old_node.mode)) {
            var new_node;
            try {
              new_node = FS.lookupNode(new_dir, new_name);
            } catch (e) {
            }
            if (new_node) {
              for (var i in new_node.contents) {
                throw new FS.ErrnoError(ERRNO_CODES.ENOTEMPTY);
              }
            }
          }
          // do the internal rewiring
          delete old_node.parent.contents[old_node.name];
          old_node.name = new_name;
          new_dir.contents[new_name] = old_node;
          old_node.parent = new_dir;
        },unlink:function (parent, name) {
          delete parent.contents[name];
        },rmdir:function (parent, name) {
          var node = FS.lookupNode(parent, name);
          for (var i in node.contents) {
            throw new FS.ErrnoError(ERRNO_CODES.ENOTEMPTY);
          }
          delete parent.contents[name];
        },readdir:function (node) {
          var entries = ['.', '..']
          for (var key in node.contents) {
            if (!node.contents.hasOwnProperty(key)) {
              continue;
            }
            entries.push(key);
          }
          return entries;
        },symlink:function (parent, newname, oldpath) {
          var node = MEMFS.createNode(parent, newname, 511 /* 0777 */ | 40960, 0);
          node.link = oldpath;
          return node;
        },readlink:function (node) {
          if (!FS.isLink(node.mode)) {
            throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
          }
          return node.link;
        }},stream_ops:{read:function (stream, buffer, offset, length, position) {
          var contents = stream.node.contents;
          if (position >= contents.length)
            return 0;
          var size = Math.min(contents.length - position, length);
          assert(size >= 0);
          if (size > 8 && contents.subarray) { // non-trivial, and typed array
            buffer.set(contents.subarray(position, position + size), offset);
          } else
          {
            for (var i = 0; i < size; i++) {
              buffer[offset + i] = contents[position + i];
            }
          }
          return size;
        },write:function (stream, buffer, offset, length, position, canOwn) {
          var node = stream.node;
          node.timestamp = Date.now();
          var contents = node.contents;
          if (length && contents.length === 0 && position === 0 && buffer.subarray) {
            // just replace it with the new data
            if (canOwn && offset === 0) {
              node.contents = buffer; // this could be a subarray of Emscripten HEAP, or allocated from some other source.
              node.contentMode = (buffer.buffer === HEAP8.buffer) ? MEMFS.CONTENT_OWNING : MEMFS.CONTENT_FIXED;
            } else {
              node.contents = new Uint8Array(buffer.subarray(offset, offset+length));
              node.contentMode = MEMFS.CONTENT_FIXED;
            }
            return length;
          }
          MEMFS.ensureFlexible(node);
          var contents = node.contents;
          while (contents.length < position) contents.push(0);
          for (var i = 0; i < length; i++) {
            contents[position + i] = buffer[offset + i];
          }
          return length;
        },llseek:function (stream, offset, whence) {
          var position = offset;
          if (whence === 1) {  // SEEK_CUR.
            position += stream.position;
          } else if (whence === 2) {  // SEEK_END.
            if (FS.isFile(stream.node.mode)) {
              position += stream.node.contents.length;
            }
          }
          if (position < 0) {
            throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
          }
          stream.ungotten = [];
          stream.position = position;
          return position;
        },allocate:function (stream, offset, length) {
          MEMFS.ensureFlexible(stream.node);
          var contents = stream.node.contents;
          var limit = offset + length;
          while (limit > contents.length) contents.push(0);
        },mmap:function (stream, buffer, offset, length, position, prot, flags) {
          if (!FS.isFile(stream.node.mode)) {
            throw new FS.ErrnoError(ERRNO_CODES.ENODEV);
          }
          var ptr;
          var allocated;
          var contents = stream.node.contents;
          // Only make a new copy when MAP_PRIVATE is specified.
          if ( !(flags & 2) &&
                (contents.buffer === buffer || contents.buffer === buffer.buffer) ) {
            // We can't emulate MAP_SHARED when the file is not backed by the buffer
            // we're mapping to (e.g. the HEAP buffer).
            allocated = false;
            ptr = contents.byteOffset;
          } else {
            // Try to avoid unnecessary slices.
            if (position > 0 || position + length < contents.length) {
              if (contents.subarray) {
                contents = contents.subarray(position, position + length);
              } else {
                contents = Array.prototype.slice.call(contents, position, position + length);
              }
            }
            allocated = true;
            ptr = _malloc(length);
            if (!ptr) {
              throw new FS.ErrnoError(ERRNO_CODES.ENOMEM);
            }
            buffer.set(contents, ptr);
          }
          return { ptr: ptr, allocated: allocated };
        }}};
  
  var IDBFS={dbs:{},indexedDB:function () {
        return window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
      },DB_VERSION:21,DB_STORE_NAME:"FILE_DATA",mount:function (mount) {
        // reuse all of the core MEMFS functionality
        return MEMFS.mount.apply(null, arguments);
      },syncfs:function (mount, populate, callback) {
        IDBFS.getLocalSet(mount, function(err, local) {
          if (err) return callback(err);
  
          IDBFS.getRemoteSet(mount, function(err, remote) {
            if (err) return callback(err);
  
            var src = populate ? remote : local;
            var dst = populate ? local : remote;
  
            IDBFS.reconcile(src, dst, callback);
          });
        });
      },getDB:function (name, callback) {
        // check the cache first
        var db = IDBFS.dbs[name];
        if (db) {
          return callback(null, db);
        }
  
        var req;
        try {
          req = IDBFS.indexedDB().open(name, IDBFS.DB_VERSION);
        } catch (e) {
          return callback(e);
        }
        req.onupgradeneeded = function(e) {
          var db = e.target.result;
          var transaction = e.target.transaction;
  
          var fileStore;
  
          if (db.objectStoreNames.contains(IDBFS.DB_STORE_NAME)) {
            fileStore = transaction.objectStore(IDBFS.DB_STORE_NAME);
          } else {
            fileStore = db.createObjectStore(IDBFS.DB_STORE_NAME);
          }
  
          fileStore.createIndex('timestamp', 'timestamp', { unique: false });
        };
        req.onsuccess = function() {
          db = req.result;
  
          // add to the cache
          IDBFS.dbs[name] = db;
          callback(null, db);
        };
        req.onerror = function() {
          callback(this.error);
        };
      },getLocalSet:function (mount, callback) {
        var entries = {};
  
        function isRealDir(p) {
          return p !== '.' && p !== '..';
        };
        function toAbsolute(root) {
          return function(p) {
            return PATH.join2(root, p);
          }
        };
  
        var check = FS.readdir(mount.mountpoint).filter(isRealDir).map(toAbsolute(mount.mountpoint));
  
        while (check.length) {
          var path = check.pop();
          var stat;
  
          try {
            stat = FS.stat(path);
          } catch (e) {
            return callback(e);
          }
  
          if (FS.isDir(stat.mode)) {
            check.push.apply(check, FS.readdir(path).filter(isRealDir).map(toAbsolute(path)));
          }
  
          entries[path] = { timestamp: stat.mtime };
        }
  
        return callback(null, { type: 'local', entries: entries });
      },getRemoteSet:function (mount, callback) {
        var entries = {};
  
        IDBFS.getDB(mount.mountpoint, function(err, db) {
          if (err) return callback(err);
  
          var transaction = db.transaction([IDBFS.DB_STORE_NAME], 'readonly');
          transaction.onerror = function() { callback(this.error); };
  
          var store = transaction.objectStore(IDBFS.DB_STORE_NAME);
          var index = store.index('timestamp');
  
          index.openKeyCursor().onsuccess = function(event) {
            var cursor = event.target.result;
  
            if (!cursor) {
              return callback(null, { type: 'remote', db: db, entries: entries });
            }
  
            entries[cursor.primaryKey] = { timestamp: cursor.key };
  
            cursor.continue();
          };
        });
      },loadLocalEntry:function (path, callback) {
        var stat, node;
  
        try {
          var lookup = FS.lookupPath(path);
          node = lookup.node;
          stat = FS.stat(path);
        } catch (e) {
          return callback(e);
        }
  
        if (FS.isDir(stat.mode)) {
          return callback(null, { timestamp: stat.mtime, mode: stat.mode });
        } else if (FS.isFile(stat.mode)) {
          return callback(null, { timestamp: stat.mtime, mode: stat.mode, contents: node.contents });
        } else {
          return callback(new Error('node type not supported'));
        }
      },storeLocalEntry:function (path, entry, callback) {
        try {
          if (FS.isDir(entry.mode)) {
            FS.mkdir(path, entry.mode);
          } else if (FS.isFile(entry.mode)) {
            FS.writeFile(path, entry.contents, { encoding: 'binary', canOwn: true });
          } else {
            return callback(new Error('node type not supported'));
          }
  
          FS.utime(path, entry.timestamp, entry.timestamp);
        } catch (e) {
          return callback(e);
        }
  
        callback(null);
      },removeLocalEntry:function (path, callback) {
        try {
          var lookup = FS.lookupPath(path);
          var stat = FS.stat(path);
  
          if (FS.isDir(stat.mode)) {
            FS.rmdir(path);
          } else if (FS.isFile(stat.mode)) {
            FS.unlink(path);
          }
        } catch (e) {
          return callback(e);
        }
  
        callback(null);
      },loadRemoteEntry:function (store, path, callback) {
        var req = store.get(path);
        req.onsuccess = function(event) { callback(null, event.target.result); };
        req.onerror = function() { callback(this.error); };
      },storeRemoteEntry:function (store, path, entry, callback) {
        var req = store.put(entry, path);
        req.onsuccess = function() { callback(null); };
        req.onerror = function() { callback(this.error); };
      },removeRemoteEntry:function (store, path, callback) {
        var req = store.delete(path);
        req.onsuccess = function() { callback(null); };
        req.onerror = function() { callback(this.error); };
      },reconcile:function (src, dst, callback) {
        var total = 0;
  
        var create = [];
        Object.keys(src.entries).forEach(function (key) {
          var e = src.entries[key];
          var e2 = dst.entries[key];
          if (!e2 || e.timestamp > e2.timestamp) {
            create.push(key);
            total++;
          }
        });
  
        var remove = [];
        Object.keys(dst.entries).forEach(function (key) {
          var e = dst.entries[key];
          var e2 = src.entries[key];
          if (!e2) {
            remove.push(key);
            total++;
          }
        });
  
        if (!total) {
          return callback(null);
        }
  
        var errored = false;
        var completed = 0;
        var db = src.type === 'remote' ? src.db : dst.db;
        var transaction = db.transaction([IDBFS.DB_STORE_NAME], 'readwrite');
        var store = transaction.objectStore(IDBFS.DB_STORE_NAME);
  
        function done(err) {
          if (err) {
            if (!done.errored) {
              done.errored = true;
              return callback(err);
            }
            return;
          }
          if (++completed >= total) {
            return callback(null);
          }
        };
  
        transaction.onerror = function() { done(this.error); };
  
        // sort paths in ascending order so directory entries are created
        // before the files inside them
        create.sort().forEach(function (path) {
          if (dst.type === 'local') {
            IDBFS.loadRemoteEntry(store, path, function (err, entry) {
              if (err) return done(err);
              IDBFS.storeLocalEntry(path, entry, done);
            });
          } else {
            IDBFS.loadLocalEntry(path, function (err, entry) {
              if (err) return done(err);
              IDBFS.storeRemoteEntry(store, path, entry, done);
            });
          }
        });
  
        // sort paths in descending order so files are deleted before their
        // parent directories
        remove.sort().reverse().forEach(function(path) {
          if (dst.type === 'local') {
            IDBFS.removeLocalEntry(path, done);
          } else {
            IDBFS.removeRemoteEntry(store, path, done);
          }
        });
      }};
  
  var NODEFS={isWindows:false,staticInit:function () {
        NODEFS.isWindows = !!process.platform.match(/^win/);
      },mount:function (mount) {
        assert(ENVIRONMENT_IS_NODE);
        return NODEFS.createNode(null, '/', NODEFS.getMode(mount.opts.root), 0);
      },createNode:function (parent, name, mode, dev) {
        if (!FS.isDir(mode) && !FS.isFile(mode) && !FS.isLink(mode)) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        var node = FS.createNode(parent, name, mode);
        node.node_ops = NODEFS.node_ops;
        node.stream_ops = NODEFS.stream_ops;
        return node;
      },getMode:function (path) {
        var stat;
        try {
          stat = fs.lstatSync(path);
          if (NODEFS.isWindows) {
            // On Windows, directories return permission bits 'rw-rw-rw-', even though they have 'rwxrwxrwx', so 
            // propagate write bits to execute bits.
            stat.mode = stat.mode | ((stat.mode & 146) >> 1);
          }
        } catch (e) {
          if (!e.code) throw e;
          throw new FS.ErrnoError(ERRNO_CODES[e.code]);
        }
        return stat.mode;
      },realPath:function (node) {
        var parts = [];
        while (node.parent !== node) {
          parts.push(node.name);
          node = node.parent;
        }
        parts.push(node.mount.opts.root);
        parts.reverse();
        return PATH.join.apply(null, parts);
      },flagsToPermissionStringMap:{0:"r",1:"r+",2:"r+",64:"r",65:"r+",66:"r+",129:"rx+",193:"rx+",514:"w+",577:"w",578:"w+",705:"wx",706:"wx+",1024:"a",1025:"a",1026:"a+",1089:"a",1090:"a+",1153:"ax",1154:"ax+",1217:"ax",1218:"ax+",4096:"rs",4098:"rs+"},flagsToPermissionString:function (flags) {
        if (flags in NODEFS.flagsToPermissionStringMap) {
          return NODEFS.flagsToPermissionStringMap[flags];
        } else {
          return flags;
        }
      },node_ops:{getattr:function (node) {
          var path = NODEFS.realPath(node);
          var stat;
          try {
            stat = fs.lstatSync(path);
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
          // node.js v0.10.20 doesn't report blksize and blocks on Windows. Fake them with default blksize of 4096.
          // See http://support.microsoft.com/kb/140365
          if (NODEFS.isWindows && !stat.blksize) {
            stat.blksize = 4096;
          }
          if (NODEFS.isWindows && !stat.blocks) {
            stat.blocks = (stat.size+stat.blksize-1)/stat.blksize|0;
          }
          return {
            dev: stat.dev,
            ino: stat.ino,
            mode: stat.mode,
            nlink: stat.nlink,
            uid: stat.uid,
            gid: stat.gid,
            rdev: stat.rdev,
            size: stat.size,
            atime: stat.atime,
            mtime: stat.mtime,
            ctime: stat.ctime,
            blksize: stat.blksize,
            blocks: stat.blocks
          };
        },setattr:function (node, attr) {
          var path = NODEFS.realPath(node);
          try {
            if (attr.mode !== undefined) {
              fs.chmodSync(path, attr.mode);
              // update the common node structure mode as well
              node.mode = attr.mode;
            }
            if (attr.timestamp !== undefined) {
              var date = new Date(attr.timestamp);
              fs.utimesSync(path, date, date);
            }
            if (attr.size !== undefined) {
              fs.truncateSync(path, attr.size);
            }
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
        },lookup:function (parent, name) {
          var path = PATH.join2(NODEFS.realPath(parent), name);
          var mode = NODEFS.getMode(path);
          return NODEFS.createNode(parent, name, mode);
        },mknod:function (parent, name, mode, dev) {
          var node = NODEFS.createNode(parent, name, mode, dev);
          // create the backing node for this in the fs root as well
          var path = NODEFS.realPath(node);
          try {
            if (FS.isDir(node.mode)) {
              fs.mkdirSync(path, node.mode);
            } else {
              fs.writeFileSync(path, '', { mode: node.mode });
            }
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
          return node;
        },rename:function (oldNode, newDir, newName) {
          var oldPath = NODEFS.realPath(oldNode);
          var newPath = PATH.join2(NODEFS.realPath(newDir), newName);
          try {
            fs.renameSync(oldPath, newPath);
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
        },unlink:function (parent, name) {
          var path = PATH.join2(NODEFS.realPath(parent), name);
          try {
            fs.unlinkSync(path);
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
        },rmdir:function (parent, name) {
          var path = PATH.join2(NODEFS.realPath(parent), name);
          try {
            fs.rmdirSync(path);
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
        },readdir:function (node) {
          var path = NODEFS.realPath(node);
          try {
            return fs.readdirSync(path);
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
        },symlink:function (parent, newName, oldPath) {
          var newPath = PATH.join2(NODEFS.realPath(parent), newName);
          try {
            fs.symlinkSync(oldPath, newPath);
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
        },readlink:function (node) {
          var path = NODEFS.realPath(node);
          try {
            return fs.readlinkSync(path);
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
        }},stream_ops:{open:function (stream) {
          var path = NODEFS.realPath(stream.node);
          try {
            if (FS.isFile(stream.node.mode)) {
              stream.nfd = fs.openSync(path, NODEFS.flagsToPermissionString(stream.flags));
            }
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
        },close:function (stream) {
          try {
            if (FS.isFile(stream.node.mode) && stream.nfd) {
              fs.closeSync(stream.nfd);
            }
          } catch (e) {
            if (!e.code) throw e;
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
        },read:function (stream, buffer, offset, length, position) {
          // FIXME this is terrible.
          var nbuffer = new Buffer(length);
          var res;
          try {
            res = fs.readSync(stream.nfd, nbuffer, 0, length, position);
          } catch (e) {
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
          if (res > 0) {
            for (var i = 0; i < res; i++) {
              buffer[offset + i] = nbuffer[i];
            }
          }
          return res;
        },write:function (stream, buffer, offset, length, position) {
          // FIXME this is terrible.
          var nbuffer = new Buffer(buffer.subarray(offset, offset + length));
          var res;
          try {
            res = fs.writeSync(stream.nfd, nbuffer, 0, length, position);
          } catch (e) {
            throw new FS.ErrnoError(ERRNO_CODES[e.code]);
          }
          return res;
        },llseek:function (stream, offset, whence) {
          var position = offset;
          if (whence === 1) {  // SEEK_CUR.
            position += stream.position;
          } else if (whence === 2) {  // SEEK_END.
            if (FS.isFile(stream.node.mode)) {
              try {
                var stat = fs.fstatSync(stream.nfd);
                position += stat.size;
              } catch (e) {
                throw new FS.ErrnoError(ERRNO_CODES[e.code]);
              }
            }
          }
  
          if (position < 0) {
            throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
          }
  
          stream.position = position;
          return position;
        }}};
  
  var _stdin=allocate(1, "i32*", ALLOC_STATIC);
  
  var _stdout=allocate(1, "i32*", ALLOC_STATIC);
  
  var _stderr=allocate(1, "i32*", ALLOC_STATIC);
  
  function _fflush(stream) {
      // int fflush(FILE *stream);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/fflush.html
      // we don't currently perform any user-space buffering of data
    }var FS={root:null,mounts:[],devices:[null],streams:[],nextInode:1,nameTable:null,currentPath:"/",initialized:false,ignorePermissions:true,trackingDelegate:{},tracking:{openFlags:{READ:1,WRITE:2}},ErrnoError:null,genericErrors:{},handleFSError:function (e) {
        if (!(e instanceof FS.ErrnoError)) throw e + ' : ' + stackTrace();
        return ___setErrNo(e.errno);
      },lookupPath:function (path, opts) {
        path = PATH.resolve(FS.cwd(), path);
        opts = opts || {};
  
        var defaults = {
          follow_mount: true,
          recurse_count: 0
        };
        for (var key in defaults) {
          if (opts[key] === undefined) {
            opts[key] = defaults[key];
          }
        }
  
        if (opts.recurse_count > 8) {  // max recursive lookup of 8
          throw new FS.ErrnoError(ERRNO_CODES.ELOOP);
        }
  
        // split the path
        var parts = PATH.normalizeArray(path.split('/').filter(function(p) {
          return !!p;
        }), false);
  
        // start at the root
        var current = FS.root;
        var current_path = '/';
  
        for (var i = 0; i < parts.length; i++) {
          var islast = (i === parts.length-1);
          if (islast && opts.parent) {
            // stop resolving
            break;
          }
  
          current = FS.lookupNode(current, parts[i]);
          current_path = PATH.join2(current_path, parts[i]);
  
          // jump to the mount's root node if this is a mountpoint
          if (FS.isMountpoint(current)) {
            if (!islast || (islast && opts.follow_mount)) {
              current = current.mounted.root;
            }
          }
  
          // by default, lookupPath will not follow a symlink if it is the final path component.
          // setting opts.follow = true will override this behavior.
          if (!islast || opts.follow) {
            var count = 0;
            while (FS.isLink(current.mode)) {
              var link = FS.readlink(current_path);
              current_path = PATH.resolve(PATH.dirname(current_path), link);
              
              var lookup = FS.lookupPath(current_path, { recurse_count: opts.recurse_count });
              current = lookup.node;
  
              if (count++ > 40) {  // limit max consecutive symlinks to 40 (SYMLOOP_MAX).
                throw new FS.ErrnoError(ERRNO_CODES.ELOOP);
              }
            }
          }
        }
  
        return { path: current_path, node: current };
      },getPath:function (node) {
        var path;
        while (true) {
          if (FS.isRoot(node)) {
            var mount = node.mount.mountpoint;
            if (!path) return mount;
            return mount[mount.length-1] !== '/' ? mount + '/' + path : mount + path;
          }
          path = path ? node.name + '/' + path : node.name;
          node = node.parent;
        }
      },hashName:function (parentid, name) {
        var hash = 0;
  
  
        for (var i = 0; i < name.length; i++) {
          hash = ((hash << 5) - hash + name.charCodeAt(i)) | 0;
        }
        return ((parentid + hash) >>> 0) % FS.nameTable.length;
      },hashAddNode:function (node) {
        var hash = FS.hashName(node.parent.id, node.name);
        node.name_next = FS.nameTable[hash];
        FS.nameTable[hash] = node;
      },hashRemoveNode:function (node) {
        var hash = FS.hashName(node.parent.id, node.name);
        if (FS.nameTable[hash] === node) {
          FS.nameTable[hash] = node.name_next;
        } else {
          var current = FS.nameTable[hash];
          while (current) {
            if (current.name_next === node) {
              current.name_next = node.name_next;
              break;
            }
            current = current.name_next;
          }
        }
      },lookupNode:function (parent, name) {
        var err = FS.mayLookup(parent);
        if (err) {
          throw new FS.ErrnoError(err);
        }
        var hash = FS.hashName(parent.id, name);
        for (var node = FS.nameTable[hash]; node; node = node.name_next) {
          var nodeName = node.name;
          if (node.parent.id === parent.id && nodeName === name) {
            return node;
          }
        }
        // if we failed to find it in the cache, call into the VFS
        return FS.lookup(parent, name);
      },createNode:function (parent, name, mode, rdev) {
        if (!FS.FSNode) {
          FS.FSNode = function(parent, name, mode, rdev) {
            if (!parent) {
              parent = this;  // root node sets parent to itself
            }
            this.parent = parent;
            this.mount = parent.mount;
            this.mounted = null;
            this.id = FS.nextInode++;
            this.name = name;
            this.mode = mode;
            this.node_ops = {};
            this.stream_ops = {};
            this.rdev = rdev;
          };
  
          FS.FSNode.prototype = {};
  
          // compatibility
          var readMode = 292 | 73;
          var writeMode = 146;
  
          // NOTE we must use Object.defineProperties instead of individual calls to
          // Object.defineProperty in order to make closure compiler happy
          Object.defineProperties(FS.FSNode.prototype, {
            read: {
              get: function() { return (this.mode & readMode) === readMode; },
              set: function(val) { val ? this.mode |= readMode : this.mode &= ~readMode; }
            },
            write: {
              get: function() { return (this.mode & writeMode) === writeMode; },
              set: function(val) { val ? this.mode |= writeMode : this.mode &= ~writeMode; }
            },
            isFolder: {
              get: function() { return FS.isDir(this.mode); },
            },
            isDevice: {
              get: function() { return FS.isChrdev(this.mode); },
            },
          });
        }
  
        var node = new FS.FSNode(parent, name, mode, rdev);
  
        FS.hashAddNode(node);
  
        return node;
      },destroyNode:function (node) {
        FS.hashRemoveNode(node);
      },isRoot:function (node) {
        return node === node.parent;
      },isMountpoint:function (node) {
        return !!node.mounted;
      },isFile:function (mode) {
        return (mode & 61440) === 32768;
      },isDir:function (mode) {
        return (mode & 61440) === 16384;
      },isLink:function (mode) {
        return (mode & 61440) === 40960;
      },isChrdev:function (mode) {
        return (mode & 61440) === 8192;
      },isBlkdev:function (mode) {
        return (mode & 61440) === 24576;
      },isFIFO:function (mode) {
        return (mode & 61440) === 4096;
      },isSocket:function (mode) {
        return (mode & 49152) === 49152;
      },flagModes:{"r":0,"rs":1052672,"r+":2,"w":577,"wx":705,"xw":705,"w+":578,"wx+":706,"xw+":706,"a":1089,"ax":1217,"xa":1217,"a+":1090,"ax+":1218,"xa+":1218},modeStringToFlags:function (str) {
        var flags = FS.flagModes[str];
        if (typeof flags === 'undefined') {
          throw new Error('Unknown file open mode: ' + str);
        }
        return flags;
      },flagsToPermissionString:function (flag) {
        var accmode = flag & 2097155;
        var perms = ['r', 'w', 'rw'][accmode];
        if ((flag & 512)) {
          perms += 'w';
        }
        return perms;
      },nodePermissions:function (node, perms) {
        if (FS.ignorePermissions) {
          return 0;
        }
        // return 0 if any user, group or owner bits are set.
        if (perms.indexOf('r') !== -1 && !(node.mode & 292)) {
          return ERRNO_CODES.EACCES;
        } else if (perms.indexOf('w') !== -1 && !(node.mode & 146)) {
          return ERRNO_CODES.EACCES;
        } else if (perms.indexOf('x') !== -1 && !(node.mode & 73)) {
          return ERRNO_CODES.EACCES;
        }
        return 0;
      },mayLookup:function (dir) {
        return FS.nodePermissions(dir, 'x');
      },mayCreate:function (dir, name) {
        try {
          var node = FS.lookupNode(dir, name);
          return ERRNO_CODES.EEXIST;
        } catch (e) {
        }
        return FS.nodePermissions(dir, 'wx');
      },mayDelete:function (dir, name, isdir) {
        var node;
        try {
          node = FS.lookupNode(dir, name);
        } catch (e) {
          return e.errno;
        }
        var err = FS.nodePermissions(dir, 'wx');
        if (err) {
          return err;
        }
        if (isdir) {
          if (!FS.isDir(node.mode)) {
            return ERRNO_CODES.ENOTDIR;
          }
          if (FS.isRoot(node) || FS.getPath(node) === FS.cwd()) {
            return ERRNO_CODES.EBUSY;
          }
        } else {
          if (FS.isDir(node.mode)) {
            return ERRNO_CODES.EISDIR;
          }
        }
        return 0;
      },mayOpen:function (node, flags) {
        if (!node) {
          return ERRNO_CODES.ENOENT;
        }
        if (FS.isLink(node.mode)) {
          return ERRNO_CODES.ELOOP;
        } else if (FS.isDir(node.mode)) {
          if ((flags & 2097155) !== 0 ||  // opening for write
              (flags & 512)) {
            return ERRNO_CODES.EISDIR;
          }
        }
        return FS.nodePermissions(node, FS.flagsToPermissionString(flags));
      },MAX_OPEN_FDS:4096,nextfd:function (fd_start, fd_end) {
        fd_start = fd_start || 0;
        fd_end = fd_end || FS.MAX_OPEN_FDS;
        for (var fd = fd_start; fd <= fd_end; fd++) {
          if (!FS.streams[fd]) {
            return fd;
          }
        }
        throw new FS.ErrnoError(ERRNO_CODES.EMFILE);
      },getStream:function (fd) {
        return FS.streams[fd];
      },createStream:function (stream, fd_start, fd_end) {
        if (!FS.FSStream) {
          FS.FSStream = function(){};
          FS.FSStream.prototype = {};
          // compatibility
          Object.defineProperties(FS.FSStream.prototype, {
            object: {
              get: function() { return this.node; },
              set: function(val) { this.node = val; }
            },
            isRead: {
              get: function() { return (this.flags & 2097155) !== 1; }
            },
            isWrite: {
              get: function() { return (this.flags & 2097155) !== 0; }
            },
            isAppend: {
              get: function() { return (this.flags & 1024); }
            }
          });
        }
        // clone it, so we can return an instance of FSStream
        var newStream = new FS.FSStream();
        for (var p in stream) {
          newStream[p] = stream[p];
        }
        stream = newStream;
        var fd = FS.nextfd(fd_start, fd_end);
        stream.fd = fd;
        FS.streams[fd] = stream;
        return stream;
      },closeStream:function (fd) {
        FS.streams[fd] = null;
      },getStreamFromPtr:function (ptr) {
        return FS.streams[ptr - 1];
      },getPtrForStream:function (stream) {
        return stream ? stream.fd + 1 : 0;
      },chrdev_stream_ops:{open:function (stream) {
          var device = FS.getDevice(stream.node.rdev);
          // override node's stream ops with the device's
          stream.stream_ops = device.stream_ops;
          // forward the open call
          if (stream.stream_ops.open) {
            stream.stream_ops.open(stream);
          }
        },llseek:function () {
          throw new FS.ErrnoError(ERRNO_CODES.ESPIPE);
        }},major:function (dev) {
        return ((dev) >> 8);
      },minor:function (dev) {
        return ((dev) & 0xff);
      },makedev:function (ma, mi) {
        return ((ma) << 8 | (mi));
      },registerDevice:function (dev, ops) {
        FS.devices[dev] = { stream_ops: ops };
      },getDevice:function (dev) {
        return FS.devices[dev];
      },getMounts:function (mount) {
        var mounts = [];
        var check = [mount];
  
        while (check.length) {
          var m = check.pop();
  
          mounts.push(m);
  
          check.push.apply(check, m.mounts);
        }
  
        return mounts;
      },syncfs:function (populate, callback) {
        if (typeof(populate) === 'function') {
          callback = populate;
          populate = false;
        }
  
        var mounts = FS.getMounts(FS.root.mount);
        var completed = 0;
  
        function done(err) {
          if (err) {
            if (!done.errored) {
              done.errored = true;
              return callback(err);
            }
            return;
          }
          if (++completed >= mounts.length) {
            callback(null);
          }
        };
  
        // sync all mounts
        mounts.forEach(function (mount) {
          if (!mount.type.syncfs) {
            return done(null);
          }
          mount.type.syncfs(mount, populate, done);
        });
      },mount:function (type, opts, mountpoint) {
        var root = mountpoint === '/';
        var pseudo = !mountpoint;
        var node;
  
        if (root && FS.root) {
          throw new FS.ErrnoError(ERRNO_CODES.EBUSY);
        } else if (!root && !pseudo) {
          var lookup = FS.lookupPath(mountpoint, { follow_mount: false });
  
          mountpoint = lookup.path;  // use the absolute path
          node = lookup.node;
  
          if (FS.isMountpoint(node)) {
            throw new FS.ErrnoError(ERRNO_CODES.EBUSY);
          }
  
          if (!FS.isDir(node.mode)) {
            throw new FS.ErrnoError(ERRNO_CODES.ENOTDIR);
          }
        }
  
        var mount = {
          type: type,
          opts: opts,
          mountpoint: mountpoint,
          mounts: []
        };
  
        // create a root node for the fs
        var mountRoot = type.mount(mount);
        mountRoot.mount = mount;
        mount.root = mountRoot;
  
        if (root) {
          FS.root = mountRoot;
        } else if (node) {
          // set as a mountpoint
          node.mounted = mount;
  
          // add the new mount to the current mount's children
          if (node.mount) {
            node.mount.mounts.push(mount);
          }
        }
  
        return mountRoot;
      },unmount:function (mountpoint) {
        var lookup = FS.lookupPath(mountpoint, { follow_mount: false });
  
        if (!FS.isMountpoint(lookup.node)) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
  
        // destroy the nodes for this mount, and all its child mounts
        var node = lookup.node;
        var mount = node.mounted;
        var mounts = FS.getMounts(mount);
  
        Object.keys(FS.nameTable).forEach(function (hash) {
          var current = FS.nameTable[hash];
  
          while (current) {
            var next = current.name_next;
  
            if (mounts.indexOf(current.mount) !== -1) {
              FS.destroyNode(current);
            }
  
            current = next;
          }
        });
  
        // no longer a mountpoint
        node.mounted = null;
  
        // remove this mount from the child mounts
        var idx = node.mount.mounts.indexOf(mount);
        assert(idx !== -1);
        node.mount.mounts.splice(idx, 1);
      },lookup:function (parent, name) {
        return parent.node_ops.lookup(parent, name);
      },mknod:function (path, mode, dev) {
        var lookup = FS.lookupPath(path, { parent: true });
        var parent = lookup.node;
        var name = PATH.basename(path);
        var err = FS.mayCreate(parent, name);
        if (err) {
          throw new FS.ErrnoError(err);
        }
        if (!parent.node_ops.mknod) {
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        return parent.node_ops.mknod(parent, name, mode, dev);
      },create:function (path, mode) {
        mode = mode !== undefined ? mode : 438 /* 0666 */;
        mode &= 4095;
        mode |= 32768;
        return FS.mknod(path, mode, 0);
      },mkdir:function (path, mode) {
        mode = mode !== undefined ? mode : 511 /* 0777 */;
        mode &= 511 | 512;
        mode |= 16384;
        return FS.mknod(path, mode, 0);
      },mkdev:function (path, mode, dev) {
        if (typeof(dev) === 'undefined') {
          dev = mode;
          mode = 438 /* 0666 */;
        }
        mode |= 8192;
        return FS.mknod(path, mode, dev);
      },symlink:function (oldpath, newpath) {
        var lookup = FS.lookupPath(newpath, { parent: true });
        var parent = lookup.node;
        var newname = PATH.basename(newpath);
        var err = FS.mayCreate(parent, newname);
        if (err) {
          throw new FS.ErrnoError(err);
        }
        if (!parent.node_ops.symlink) {
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        return parent.node_ops.symlink(parent, newname, oldpath);
      },rename:function (old_path, new_path) {
        var old_dirname = PATH.dirname(old_path);
        var new_dirname = PATH.dirname(new_path);
        var old_name = PATH.basename(old_path);
        var new_name = PATH.basename(new_path);
        // parents must exist
        var lookup, old_dir, new_dir;
        try {
          lookup = FS.lookupPath(old_path, { parent: true });
          old_dir = lookup.node;
          lookup = FS.lookupPath(new_path, { parent: true });
          new_dir = lookup.node;
        } catch (e) {
          throw new FS.ErrnoError(ERRNO_CODES.EBUSY);
        }
        // need to be part of the same mount
        if (old_dir.mount !== new_dir.mount) {
          throw new FS.ErrnoError(ERRNO_CODES.EXDEV);
        }
        // source must exist
        var old_node = FS.lookupNode(old_dir, old_name);
        // old path should not be an ancestor of the new path
        var relative = PATH.relative(old_path, new_dirname);
        if (relative.charAt(0) !== '.') {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        // new path should not be an ancestor of the old path
        relative = PATH.relative(new_path, old_dirname);
        if (relative.charAt(0) !== '.') {
          throw new FS.ErrnoError(ERRNO_CODES.ENOTEMPTY);
        }
        // see if the new path already exists
        var new_node;
        try {
          new_node = FS.lookupNode(new_dir, new_name);
        } catch (e) {
          // not fatal
        }
        // early out if nothing needs to change
        if (old_node === new_node) {
          return;
        }
        // we'll need to delete the old entry
        var isdir = FS.isDir(old_node.mode);
        var err = FS.mayDelete(old_dir, old_name, isdir);
        if (err) {
          throw new FS.ErrnoError(err);
        }
        // need delete permissions if we'll be overwriting.
        // need create permissions if new doesn't already exist.
        err = new_node ?
          FS.mayDelete(new_dir, new_name, isdir) :
          FS.mayCreate(new_dir, new_name);
        if (err) {
          throw new FS.ErrnoError(err);
        }
        if (!old_dir.node_ops.rename) {
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        if (FS.isMountpoint(old_node) || (new_node && FS.isMountpoint(new_node))) {
          throw new FS.ErrnoError(ERRNO_CODES.EBUSY);
        }
        // if we are going to change the parent, check write permissions
        if (new_dir !== old_dir) {
          err = FS.nodePermissions(old_dir, 'w');
          if (err) {
            throw new FS.ErrnoError(err);
          }
        }
        try {
          if (FS.trackingDelegate['willMovePath']) {
            FS.trackingDelegate['willMovePath'](old_path, new_path);
          }
        } catch(e) {
          console.log("FS.trackingDelegate['willMovePath']('"+old_path+"', '"+new_path+"') threw an exception: " + e.message);
        }
        // remove the node from the lookup hash
        FS.hashRemoveNode(old_node);
        // do the underlying fs rename
        try {
          old_dir.node_ops.rename(old_node, new_dir, new_name);
        } catch (e) {
          throw e;
        } finally {
          // add the node back to the hash (in case node_ops.rename
          // changed its name)
          FS.hashAddNode(old_node);
        }
        try {
          if (FS.trackingDelegate['onMovePath']) FS.trackingDelegate['onMovePath'](old_path, new_path);
        } catch(e) {
          console.log("FS.trackingDelegate['onMovePath']('"+old_path+"', '"+new_path+"') threw an exception: " + e.message);
        }
      },rmdir:function (path) {
        var lookup = FS.lookupPath(path, { parent: true });
        var parent = lookup.node;
        var name = PATH.basename(path);
        var node = FS.lookupNode(parent, name);
        var err = FS.mayDelete(parent, name, true);
        if (err) {
          throw new FS.ErrnoError(err);
        }
        if (!parent.node_ops.rmdir) {
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        if (FS.isMountpoint(node)) {
          throw new FS.ErrnoError(ERRNO_CODES.EBUSY);
        }
        try {
          if (FS.trackingDelegate['willDeletePath']) {
            FS.trackingDelegate['willDeletePath'](path);
          }
        } catch(e) {
          console.log("FS.trackingDelegate['willDeletePath']('"+path+"') threw an exception: " + e.message);
        }
        parent.node_ops.rmdir(parent, name);
        FS.destroyNode(node);
        try {
          if (FS.trackingDelegate['onDeletePath']) FS.trackingDelegate['onDeletePath'](path);
        } catch(e) {
          console.log("FS.trackingDelegate['onDeletePath']('"+path+"') threw an exception: " + e.message);
        }
      },readdir:function (path) {
        var lookup = FS.lookupPath(path, { follow: true });
        var node = lookup.node;
        if (!node.node_ops.readdir) {
          throw new FS.ErrnoError(ERRNO_CODES.ENOTDIR);
        }
        return node.node_ops.readdir(node);
      },unlink:function (path) {
        var lookup = FS.lookupPath(path, { parent: true });
        var parent = lookup.node;
        var name = PATH.basename(path);
        var node = FS.lookupNode(parent, name);
        var err = FS.mayDelete(parent, name, false);
        if (err) {
          // POSIX says unlink should set EPERM, not EISDIR
          if (err === ERRNO_CODES.EISDIR) err = ERRNO_CODES.EPERM;
          throw new FS.ErrnoError(err);
        }
        if (!parent.node_ops.unlink) {
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        if (FS.isMountpoint(node)) {
          throw new FS.ErrnoError(ERRNO_CODES.EBUSY);
        }
        try {
          if (FS.trackingDelegate['willDeletePath']) {
            FS.trackingDelegate['willDeletePath'](path);
          }
        } catch(e) {
          console.log("FS.trackingDelegate['willDeletePath']('"+path+"') threw an exception: " + e.message);
        }
        parent.node_ops.unlink(parent, name);
        FS.destroyNode(node);
        try {
          if (FS.trackingDelegate['onDeletePath']) FS.trackingDelegate['onDeletePath'](path);
        } catch(e) {
          console.log("FS.trackingDelegate['onDeletePath']('"+path+"') threw an exception: " + e.message);
        }
      },readlink:function (path) {
        var lookup = FS.lookupPath(path);
        var link = lookup.node;
        if (!link.node_ops.readlink) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        return link.node_ops.readlink(link);
      },stat:function (path, dontFollow) {
        var lookup = FS.lookupPath(path, { follow: !dontFollow });
        var node = lookup.node;
        if (!node.node_ops.getattr) {
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        return node.node_ops.getattr(node);
      },lstat:function (path) {
        return FS.stat(path, true);
      },chmod:function (path, mode, dontFollow) {
        var node;
        if (typeof path === 'string') {
          var lookup = FS.lookupPath(path, { follow: !dontFollow });
          node = lookup.node;
        } else {
          node = path;
        }
        if (!node.node_ops.setattr) {
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        node.node_ops.setattr(node, {
          mode: (mode & 4095) | (node.mode & ~4095),
          timestamp: Date.now()
        });
      },lchmod:function (path, mode) {
        FS.chmod(path, mode, true);
      },fchmod:function (fd, mode) {
        var stream = FS.getStream(fd);
        if (!stream) {
          throw new FS.ErrnoError(ERRNO_CODES.EBADF);
        }
        FS.chmod(stream.node, mode);
      },chown:function (path, uid, gid, dontFollow) {
        var node;
        if (typeof path === 'string') {
          var lookup = FS.lookupPath(path, { follow: !dontFollow });
          node = lookup.node;
        } else {
          node = path;
        }
        if (!node.node_ops.setattr) {
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        node.node_ops.setattr(node, {
          timestamp: Date.now()
          // we ignore the uid / gid for now
        });
      },lchown:function (path, uid, gid) {
        FS.chown(path, uid, gid, true);
      },fchown:function (fd, uid, gid) {
        var stream = FS.getStream(fd);
        if (!stream) {
          throw new FS.ErrnoError(ERRNO_CODES.EBADF);
        }
        FS.chown(stream.node, uid, gid);
      },truncate:function (path, len) {
        if (len < 0) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        var node;
        if (typeof path === 'string') {
          var lookup = FS.lookupPath(path, { follow: true });
          node = lookup.node;
        } else {
          node = path;
        }
        if (!node.node_ops.setattr) {
          throw new FS.ErrnoError(ERRNO_CODES.EPERM);
        }
        if (FS.isDir(node.mode)) {
          throw new FS.ErrnoError(ERRNO_CODES.EISDIR);
        }
        if (!FS.isFile(node.mode)) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        var err = FS.nodePermissions(node, 'w');
        if (err) {
          throw new FS.ErrnoError(err);
        }
        node.node_ops.setattr(node, {
          size: len,
          timestamp: Date.now()
        });
      },ftruncate:function (fd, len) {
        var stream = FS.getStream(fd);
        if (!stream) {
          throw new FS.ErrnoError(ERRNO_CODES.EBADF);
        }
        if ((stream.flags & 2097155) === 0) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        FS.truncate(stream.node, len);
      },utime:function (path, atime, mtime) {
        var lookup = FS.lookupPath(path, { follow: true });
        var node = lookup.node;
        node.node_ops.setattr(node, {
          timestamp: Math.max(atime, mtime)
        });
      },open:function (path, flags, mode, fd_start, fd_end) {
        flags = typeof flags === 'string' ? FS.modeStringToFlags(flags) : flags;
        mode = typeof mode === 'undefined' ? 438 /* 0666 */ : mode;
        if ((flags & 64)) {
          mode = (mode & 4095) | 32768;
        } else {
          mode = 0;
        }
        var node;
        if (typeof path === 'object') {
          node = path;
        } else {
          path = PATH.normalize(path);
          try {
            var lookup = FS.lookupPath(path, {
              follow: !(flags & 131072)
            });
            node = lookup.node;
          } catch (e) {
            // ignore
          }
        }
        // perhaps we need to create the node
        if ((flags & 64)) {
          if (node) {
            // if O_CREAT and O_EXCL are set, error out if the node already exists
            if ((flags & 128)) {
              throw new FS.ErrnoError(ERRNO_CODES.EEXIST);
            }
          } else {
            // node doesn't exist, try to create it
            node = FS.mknod(path, mode, 0);
          }
        }
        if (!node) {
          throw new FS.ErrnoError(ERRNO_CODES.ENOENT);
        }
        // can't truncate a device
        if (FS.isChrdev(node.mode)) {
          flags &= ~512;
        }
        // check permissions
        var err = FS.mayOpen(node, flags);
        if (err) {
          throw new FS.ErrnoError(err);
        }
        // do truncation if necessary
        if ((flags & 512)) {
          FS.truncate(node, 0);
        }
        // we've already handled these, don't pass down to the underlying vfs
        flags &= ~(128 | 512);
  
        // register the stream with the filesystem
        var stream = FS.createStream({
          node: node,
          path: FS.getPath(node),  // we want the absolute path to the node
          flags: flags,
          seekable: true,
          position: 0,
          stream_ops: node.stream_ops,
          // used by the file family libc calls (fopen, fwrite, ferror, etc.)
          ungotten: [],
          error: false
        }, fd_start, fd_end);
        // call the new stream's open function
        if (stream.stream_ops.open) {
          stream.stream_ops.open(stream);
        }
        if (Module['logReadFiles'] && !(flags & 1)) {
          if (!FS.readFiles) FS.readFiles = {};
          if (!(path in FS.readFiles)) {
            FS.readFiles[path] = 1;
            Module['printErr']('read file: ' + path);
          }
        }
        try {
          if (FS.trackingDelegate['onOpenFile']) {
            var trackingFlags = 0;
            if ((flags & 2097155) !== 1) {
              trackingFlags |= FS.tracking.openFlags.READ;
            }
            if ((flags & 2097155) !== 0) {
              trackingFlags |= FS.tracking.openFlags.WRITE;
            }
            FS.trackingDelegate['onOpenFile'](path, trackingFlags);
          }
        } catch(e) {
          console.log("FS.trackingDelegate['onOpenFile']('"+path+"', flags) threw an exception: " + e.message);
        }
        return stream;
      },close:function (stream) {
        try {
          if (stream.stream_ops.close) {
            stream.stream_ops.close(stream);
          }
        } catch (e) {
          throw e;
        } finally {
          FS.closeStream(stream.fd);
        }
      },llseek:function (stream, offset, whence) {
        if (!stream.seekable || !stream.stream_ops.llseek) {
          throw new FS.ErrnoError(ERRNO_CODES.ESPIPE);
        }
        return stream.stream_ops.llseek(stream, offset, whence);
      },read:function (stream, buffer, offset, length, position) {
        if (length < 0 || position < 0) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        if ((stream.flags & 2097155) === 1) {
          throw new FS.ErrnoError(ERRNO_CODES.EBADF);
        }
        if (FS.isDir(stream.node.mode)) {
          throw new FS.ErrnoError(ERRNO_CODES.EISDIR);
        }
        if (!stream.stream_ops.read) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        var seeking = true;
        if (typeof position === 'undefined') {
          position = stream.position;
          seeking = false;
        } else if (!stream.seekable) {
          throw new FS.ErrnoError(ERRNO_CODES.ESPIPE);
        }
        var bytesRead = stream.stream_ops.read(stream, buffer, offset, length, position);
        if (!seeking) stream.position += bytesRead;
        return bytesRead;
      },write:function (stream, buffer, offset, length, position, canOwn) {
        if (length < 0 || position < 0) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        if ((stream.flags & 2097155) === 0) {
          throw new FS.ErrnoError(ERRNO_CODES.EBADF);
        }
        if (FS.isDir(stream.node.mode)) {
          throw new FS.ErrnoError(ERRNO_CODES.EISDIR);
        }
        if (!stream.stream_ops.write) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        var seeking = true;
        if (typeof position === 'undefined') {
          position = stream.position;
          seeking = false;
        } else if (!stream.seekable) {
          throw new FS.ErrnoError(ERRNO_CODES.ESPIPE);
        }
        if (stream.flags & 1024) {
          // seek to the end before writing in append mode
          FS.llseek(stream, 0, 2);
        }
        var bytesWritten = stream.stream_ops.write(stream, buffer, offset, length, position, canOwn);
        if (!seeking) stream.position += bytesWritten;
        try {
          if (stream.path && FS.trackingDelegate['onWriteToFile']) FS.trackingDelegate['onWriteToFile'](stream.path);
        } catch(e) {
          console.log("FS.trackingDelegate['onWriteToFile']('"+path+"') threw an exception: " + e.message);
        }
        return bytesWritten;
      },allocate:function (stream, offset, length) {
        if (offset < 0 || length <= 0) {
          throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
        }
        if ((stream.flags & 2097155) === 0) {
          throw new FS.ErrnoError(ERRNO_CODES.EBADF);
        }
        if (!FS.isFile(stream.node.mode) && !FS.isDir(node.mode)) {
          throw new FS.ErrnoError(ERRNO_CODES.ENODEV);
        }
        if (!stream.stream_ops.allocate) {
          throw new FS.ErrnoError(ERRNO_CODES.EOPNOTSUPP);
        }
        stream.stream_ops.allocate(stream, offset, length);
      },mmap:function (stream, buffer, offset, length, position, prot, flags) {
        // TODO if PROT is PROT_WRITE, make sure we have write access
        if ((stream.flags & 2097155) === 1) {
          throw new FS.ErrnoError(ERRNO_CODES.EACCES);
        }
        if (!stream.stream_ops.mmap) {
          throw new FS.ErrnoError(ERRNO_CODES.ENODEV);
        }
        return stream.stream_ops.mmap(stream, buffer, offset, length, position, prot, flags);
      },ioctl:function (stream, cmd, arg) {
        if (!stream.stream_ops.ioctl) {
          throw new FS.ErrnoError(ERRNO_CODES.ENOTTY);
        }
        return stream.stream_ops.ioctl(stream, cmd, arg);
      },readFile:function (path, opts) {
        opts = opts || {};
        opts.flags = opts.flags || 'r';
        opts.encoding = opts.encoding || 'binary';
        if (opts.encoding !== 'utf8' && opts.encoding !== 'binary') {
          throw new Error('Invalid encoding type "' + opts.encoding + '"');
        }
        var ret;
        var stream = FS.open(path, opts.flags);
        var stat = FS.stat(path);
        var length = stat.size;
        var buf = new Uint8Array(length);
        FS.read(stream, buf, 0, length, 0);
        if (opts.encoding === 'utf8') {
          ret = '';
          var utf8 = new Runtime.UTF8Processor();
          for (var i = 0; i < length; i++) {
            ret += utf8.processCChar(buf[i]);
          }
        } else if (opts.encoding === 'binary') {
          ret = buf;
        }
        FS.close(stream);
        return ret;
      },writeFile:function (path, data, opts) {
        opts = opts || {};
        opts.flags = opts.flags || 'w';
        opts.encoding = opts.encoding || 'utf8';
        if (opts.encoding !== 'utf8' && opts.encoding !== 'binary') {
          throw new Error('Invalid encoding type "' + opts.encoding + '"');
        }
        var stream = FS.open(path, opts.flags, opts.mode);
        if (opts.encoding === 'utf8') {
          var utf8 = new Runtime.UTF8Processor();
          var buf = new Uint8Array(utf8.processJSString(data));
          FS.write(stream, buf, 0, buf.length, 0, opts.canOwn);
        } else if (opts.encoding === 'binary') {
          FS.write(stream, data, 0, data.length, 0, opts.canOwn);
        }
        FS.close(stream);
      },cwd:function () {
        return FS.currentPath;
      },chdir:function (path) {
        var lookup = FS.lookupPath(path, { follow: true });
        if (!FS.isDir(lookup.node.mode)) {
          throw new FS.ErrnoError(ERRNO_CODES.ENOTDIR);
        }
        var err = FS.nodePermissions(lookup.node, 'x');
        if (err) {
          throw new FS.ErrnoError(err);
        }
        FS.currentPath = lookup.path;
      },createDefaultDirectories:function () {
        FS.mkdir('/tmp');
      },createDefaultDevices:function () {
        // create /dev
        FS.mkdir('/dev');
        // setup /dev/null
        FS.registerDevice(FS.makedev(1, 3), {
          read: function() { return 0; },
          write: function() { return 0; }
        });
        FS.mkdev('/dev/null', FS.makedev(1, 3));
        // setup /dev/tty and /dev/tty1
        // stderr needs to print output using Module['printErr']
        // so we register a second tty just for it.
        TTY.register(FS.makedev(5, 0), TTY.default_tty_ops);
        TTY.register(FS.makedev(6, 0), TTY.default_tty1_ops);
        FS.mkdev('/dev/tty', FS.makedev(5, 0));
        FS.mkdev('/dev/tty1', FS.makedev(6, 0));
        // we're not going to emulate the actual shm device,
        // just create the tmp dirs that reside in it commonly
        FS.mkdir('/dev/shm');
        FS.mkdir('/dev/shm/tmp');
      },createStandardStreams:function () {
        // TODO deprecate the old functionality of a single
        // input / output callback and that utilizes FS.createDevice
        // and instead require a unique set of stream ops
  
        // by default, we symlink the standard streams to the
        // default tty devices. however, if the standard streams
        // have been overwritten we create a unique device for
        // them instead.
        if (Module['stdin']) {
          FS.createDevice('/dev', 'stdin', Module['stdin']);
        } else {
          FS.symlink('/dev/tty', '/dev/stdin');
        }
        if (Module['stdout']) {
          FS.createDevice('/dev', 'stdout', null, Module['stdout']);
        } else {
          FS.symlink('/dev/tty', '/dev/stdout');
        }
        if (Module['stderr']) {
          FS.createDevice('/dev', 'stderr', null, Module['stderr']);
        } else {
          FS.symlink('/dev/tty1', '/dev/stderr');
        }
  
        // open default streams for the stdin, stdout and stderr devices
        var stdin = FS.open('/dev/stdin', 'r');
        HEAP32[((_stdin)>>2)]=FS.getPtrForStream(stdin);
        assert(stdin.fd === 0, 'invalid handle for stdin (' + stdin.fd + ')');
  
        var stdout = FS.open('/dev/stdout', 'w');
        HEAP32[((_stdout)>>2)]=FS.getPtrForStream(stdout);
        assert(stdout.fd === 1, 'invalid handle for stdout (' + stdout.fd + ')');
  
        var stderr = FS.open('/dev/stderr', 'w');
        HEAP32[((_stderr)>>2)]=FS.getPtrForStream(stderr);
        assert(stderr.fd === 2, 'invalid handle for stderr (' + stderr.fd + ')');
      },ensureErrnoError:function () {
        if (FS.ErrnoError) return;
        FS.ErrnoError = function ErrnoError(errno) {
          this.errno = errno;
          for (var key in ERRNO_CODES) {
            if (ERRNO_CODES[key] === errno) {
              this.code = key;
              break;
            }
          }
          this.message = ERRNO_MESSAGES[errno];
        };
        FS.ErrnoError.prototype = new Error();
        FS.ErrnoError.prototype.constructor = FS.ErrnoError;
        // Some errors may happen quite a bit, to avoid overhead we reuse them (and suffer a lack of stack info)
        [ERRNO_CODES.ENOENT].forEach(function(code) {
          FS.genericErrors[code] = new FS.ErrnoError(code);
          FS.genericErrors[code].stack = '<generic error, no stack>';
        });
      },staticInit:function () {
        FS.ensureErrnoError();
  
        FS.nameTable = new Array(4096);
  
        FS.mount(MEMFS, {}, '/');
  
        FS.createDefaultDirectories();
        FS.createDefaultDevices();
      },init:function (input, output, error) {
        assert(!FS.init.initialized, 'FS.init was previously called. If you want to initialize later with custom parameters, remove any earlier calls (note that one is automatically added to the generated code)');
        FS.init.initialized = true;
  
        FS.ensureErrnoError();
  
        // Allow Module.stdin etc. to provide defaults, if none explicitly passed to us here
        Module['stdin'] = input || Module['stdin'];
        Module['stdout'] = output || Module['stdout'];
        Module['stderr'] = error || Module['stderr'];
  
        FS.createStandardStreams();
      },quit:function () {
        FS.init.initialized = false;
        for (var i = 0; i < FS.streams.length; i++) {
          var stream = FS.streams[i];
          if (!stream) {
            continue;
          }
          FS.close(stream);
        }
      },getMode:function (canRead, canWrite) {
        var mode = 0;
        if (canRead) mode |= 292 | 73;
        if (canWrite) mode |= 146;
        return mode;
      },joinPath:function (parts, forceRelative) {
        var path = PATH.join.apply(null, parts);
        if (forceRelative && path[0] == '/') path = path.substr(1);
        return path;
      },absolutePath:function (relative, base) {
        return PATH.resolve(base, relative);
      },standardizePath:function (path) {
        return PATH.normalize(path);
      },findObject:function (path, dontResolveLastLink) {
        var ret = FS.analyzePath(path, dontResolveLastLink);
        if (ret.exists) {
          return ret.object;
        } else {
          ___setErrNo(ret.error);
          return null;
        }
      },analyzePath:function (path, dontResolveLastLink) {
        // operate from within the context of the symlink's target
        try {
          var lookup = FS.lookupPath(path, { follow: !dontResolveLastLink });
          path = lookup.path;
        } catch (e) {
        }
        var ret = {
          isRoot: false, exists: false, error: 0, name: null, path: null, object: null,
          parentExists: false, parentPath: null, parentObject: null
        };
        try {
          var lookup = FS.lookupPath(path, { parent: true });
          ret.parentExists = true;
          ret.parentPath = lookup.path;
          ret.parentObject = lookup.node;
          ret.name = PATH.basename(path);
          lookup = FS.lookupPath(path, { follow: !dontResolveLastLink });
          ret.exists = true;
          ret.path = lookup.path;
          ret.object = lookup.node;
          ret.name = lookup.node.name;
          ret.isRoot = lookup.path === '/';
        } catch (e) {
          ret.error = e.errno;
        };
        return ret;
      },createFolder:function (parent, name, canRead, canWrite) {
        var path = PATH.join2(typeof parent === 'string' ? parent : FS.getPath(parent), name);
        var mode = FS.getMode(canRead, canWrite);
        return FS.mkdir(path, mode);
      },createPath:function (parent, path, canRead, canWrite) {
        parent = typeof parent === 'string' ? parent : FS.getPath(parent);
        var parts = path.split('/').reverse();
        while (parts.length) {
          var part = parts.pop();
          if (!part) continue;
          var current = PATH.join2(parent, part);
          try {
            FS.mkdir(current);
          } catch (e) {
            // ignore EEXIST
          }
          parent = current;
        }
        return current;
      },createFile:function (parent, name, properties, canRead, canWrite) {
        var path = PATH.join2(typeof parent === 'string' ? parent : FS.getPath(parent), name);
        var mode = FS.getMode(canRead, canWrite);
        return FS.create(path, mode);
      },createDataFile:function (parent, name, data, canRead, canWrite, canOwn) {
        var path = name ? PATH.join2(typeof parent === 'string' ? parent : FS.getPath(parent), name) : parent;
        var mode = FS.getMode(canRead, canWrite);
        var node = FS.create(path, mode);
        if (data) {
          if (typeof data === 'string') {
            var arr = new Array(data.length);
            for (var i = 0, len = data.length; i < len; ++i) arr[i] = data.charCodeAt(i);
            data = arr;
          }
          // make sure we can write to the file
          FS.chmod(node, mode | 146);
          var stream = FS.open(node, 'w');
          FS.write(stream, data, 0, data.length, 0, canOwn);
          FS.close(stream);
          FS.chmod(node, mode);
        }
        return node;
      },createDevice:function (parent, name, input, output) {
        var path = PATH.join2(typeof parent === 'string' ? parent : FS.getPath(parent), name);
        var mode = FS.getMode(!!input, !!output);
        if (!FS.createDevice.major) FS.createDevice.major = 64;
        var dev = FS.makedev(FS.createDevice.major++, 0);
        // Create a fake device that a set of stream ops to emulate
        // the old behavior.
        FS.registerDevice(dev, {
          open: function(stream) {
            stream.seekable = false;
          },
          close: function(stream) {
            // flush any pending line data
            if (output && output.buffer && output.buffer.length) {
              output(10);
            }
          },
          read: function(stream, buffer, offset, length, pos /* ignored */) {
            var bytesRead = 0;
            for (var i = 0; i < length; i++) {
              var result;
              try {
                result = input();
              } catch (e) {
                throw new FS.ErrnoError(ERRNO_CODES.EIO);
              }
              if (result === undefined && bytesRead === 0) {
                throw new FS.ErrnoError(ERRNO_CODES.EAGAIN);
              }
              if (result === null || result === undefined) break;
              bytesRead++;
              buffer[offset+i] = result;
            }
            if (bytesRead) {
              stream.node.timestamp = Date.now();
            }
            return bytesRead;
          },
          write: function(stream, buffer, offset, length, pos) {
            for (var i = 0; i < length; i++) {
              try {
                output(buffer[offset+i]);
              } catch (e) {
                throw new FS.ErrnoError(ERRNO_CODES.EIO);
              }
            }
            if (length) {
              stream.node.timestamp = Date.now();
            }
            return i;
          }
        });
        return FS.mkdev(path, mode, dev);
      },createLink:function (parent, name, target, canRead, canWrite) {
        var path = PATH.join2(typeof parent === 'string' ? parent : FS.getPath(parent), name);
        return FS.symlink(target, path);
      },forceLoadFile:function (obj) {
        if (obj.isDevice || obj.isFolder || obj.link || obj.contents) return true;
        var success = true;
        if (typeof XMLHttpRequest !== 'undefined') {
          throw new Error("Lazy loading should have been performed (contents set) in createLazyFile, but it was not. Lazy loading only works in web workers. Use --embed-file or --preload-file in emcc on the main thread.");
        } else if (Module['read']) {
          // Command-line.
          try {
            // WARNING: Can't read binary files in V8's d8 or tracemonkey's js, as
            //          read() will try to parse UTF8.
            obj.contents = intArrayFromString(Module['read'](obj.url), true);
          } catch (e) {
            success = false;
          }
        } else {
          throw new Error('Cannot load without read() or XMLHttpRequest.');
        }
        if (!success) ___setErrNo(ERRNO_CODES.EIO);
        return success;
      },createLazyFile:function (parent, name, url, canRead, canWrite) {
        // Lazy chunked Uint8Array (implements get and length from Uint8Array). Actual getting is abstracted away for eventual reuse.
        function LazyUint8Array() {
          this.lengthKnown = false;
          this.chunks = []; // Loaded chunks. Index is the chunk number
        }
        LazyUint8Array.prototype.get = function LazyUint8Array_get(idx) {
          if (idx > this.length-1 || idx < 0) {
            return undefined;
          }
          var chunkOffset = idx % this.chunkSize;
          var chunkNum = Math.floor(idx / this.chunkSize);
          return this.getter(chunkNum)[chunkOffset];
        }
        LazyUint8Array.prototype.setDataGetter = function LazyUint8Array_setDataGetter(getter) {
          this.getter = getter;
        }
        LazyUint8Array.prototype.cacheLength = function LazyUint8Array_cacheLength() {
            // Find length
            var xhr = new XMLHttpRequest();
            xhr.open('HEAD', url, false);
            xhr.send(null);
            if (!(xhr.status >= 200 && xhr.status < 300 || xhr.status === 304)) throw new Error("Couldn't load " + url + ". Status: " + xhr.status);
            var datalength = Number(xhr.getResponseHeader("Content-length"));
            var header;
            var hasByteServing = (header = xhr.getResponseHeader("Accept-Ranges")) && header === "bytes";
            var chunkSize = 1024*1024; // Chunk size in bytes
  
            if (!hasByteServing) chunkSize = datalength;
  
            // Function to get a range from the remote URL.
            var doXHR = (function(from, to) {
              if (from > to) throw new Error("invalid range (" + from + ", " + to + ") or no bytes requested!");
              if (to > datalength-1) throw new Error("only " + datalength + " bytes available! programmer error!");
  
              // TODO: Use mozResponseArrayBuffer, responseStream, etc. if available.
              var xhr = new XMLHttpRequest();
              xhr.open('GET', url, false);
              if (datalength !== chunkSize) xhr.setRequestHeader("Range", "bytes=" + from + "-" + to);
  
              // Some hints to the browser that we want binary data.
              if (typeof Uint8Array != 'undefined') xhr.responseType = 'arraybuffer';
              if (xhr.overrideMimeType) {
                xhr.overrideMimeType('text/plain; charset=x-user-defined');
              }
  
              xhr.send(null);
              if (!(xhr.status >= 200 && xhr.status < 300 || xhr.status === 304)) throw new Error("Couldn't load " + url + ". Status: " + xhr.status);
              if (xhr.response !== undefined) {
                return new Uint8Array(xhr.response || []);
              } else {
                return intArrayFromString(xhr.responseText || '', true);
              }
            });
            var lazyArray = this;
            lazyArray.setDataGetter(function(chunkNum) {
              var start = chunkNum * chunkSize;
              var end = (chunkNum+1) * chunkSize - 1; // including this byte
              end = Math.min(end, datalength-1); // if datalength-1 is selected, this is the last block
              if (typeof(lazyArray.chunks[chunkNum]) === "undefined") {
                lazyArray.chunks[chunkNum] = doXHR(start, end);
              }
              if (typeof(lazyArray.chunks[chunkNum]) === "undefined") throw new Error("doXHR failed!");
              return lazyArray.chunks[chunkNum];
            });
  
            this._length = datalength;
            this._chunkSize = chunkSize;
            this.lengthKnown = true;
        }
        if (typeof XMLHttpRequest !== 'undefined') {
          if (!ENVIRONMENT_IS_WORKER) throw 'Cannot do synchronous binary XHRs outside webworkers in modern browsers. Use --embed-file or --preload-file in emcc';
          var lazyArray = new LazyUint8Array();
          Object.defineProperty(lazyArray, "length", {
              get: function() {
                  if(!this.lengthKnown) {
                      this.cacheLength();
                  }
                  return this._length;
              }
          });
          Object.defineProperty(lazyArray, "chunkSize", {
              get: function() {
                  if(!this.lengthKnown) {
                      this.cacheLength();
                  }
                  return this._chunkSize;
              }
          });
  
          var properties = { isDevice: false, contents: lazyArray };
        } else {
          var properties = { isDevice: false, url: url };
        }
  
        var node = FS.createFile(parent, name, properties, canRead, canWrite);
        // This is a total hack, but I want to get this lazy file code out of the
        // core of MEMFS. If we want to keep this lazy file concept I feel it should
        // be its own thin LAZYFS proxying calls to MEMFS.
        if (properties.contents) {
          node.contents = properties.contents;
        } else if (properties.url) {
          node.contents = null;
          node.url = properties.url;
        }
        // override each stream op with one that tries to force load the lazy file first
        var stream_ops = {};
        var keys = Object.keys(node.stream_ops);
        keys.forEach(function(key) {
          var fn = node.stream_ops[key];
          stream_ops[key] = function forceLoadLazyFile() {
            if (!FS.forceLoadFile(node)) {
              throw new FS.ErrnoError(ERRNO_CODES.EIO);
            }
            return fn.apply(null, arguments);
          };
        });
        // use a custom read function
        stream_ops.read = function stream_ops_read(stream, buffer, offset, length, position) {
          if (!FS.forceLoadFile(node)) {
            throw new FS.ErrnoError(ERRNO_CODES.EIO);
          }
          var contents = stream.node.contents;
          if (position >= contents.length)
            return 0;
          var size = Math.min(contents.length - position, length);
          assert(size >= 0);
          if (contents.slice) { // normal array
            for (var i = 0; i < size; i++) {
              buffer[offset + i] = contents[position + i];
            }
          } else {
            for (var i = 0; i < size; i++) { // LazyUint8Array from sync binary XHR
              buffer[offset + i] = contents.get(position + i);
            }
          }
          return size;
        };
        node.stream_ops = stream_ops;
        return node;
      },createPreloadedFile:function (parent, name, url, canRead, canWrite, onload, onerror, dontCreateFile, canOwn) {
        Browser.init();
        // TODO we should allow people to just pass in a complete filename instead
        // of parent and name being that we just join them anyways
        var fullname = name ? PATH.resolve(PATH.join2(parent, name)) : parent;
        function processData(byteArray) {
          function finish(byteArray) {
            if (!dontCreateFile) {
              FS.createDataFile(parent, name, byteArray, canRead, canWrite, canOwn);
            }
            if (onload) onload();
            removeRunDependency('cp ' + fullname);
          }
          var handled = false;
          Module['preloadPlugins'].forEach(function(plugin) {
            if (handled) return;
            if (plugin['canHandle'](fullname)) {
              plugin['handle'](byteArray, fullname, finish, function() {
                if (onerror) onerror();
                removeRunDependency('cp ' + fullname);
              });
              handled = true;
            }
          });
          if (!handled) finish(byteArray);
        }
        addRunDependency('cp ' + fullname);
        if (typeof url == 'string') {
          Browser.asyncLoad(url, function(byteArray) {
            processData(byteArray);
          }, onerror);
        } else {
          processData(url);
        }
      },indexedDB:function () {
        return window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
      },DB_NAME:function () {
        return 'EM_FS_' + window.location.pathname;
      },DB_VERSION:20,DB_STORE_NAME:"FILE_DATA",saveFilesToDB:function (paths, onload, onerror) {
        onload = onload || function(){};
        onerror = onerror || function(){};
        var indexedDB = FS.indexedDB();
        try {
          var openRequest = indexedDB.open(FS.DB_NAME(), FS.DB_VERSION);
        } catch (e) {
          return onerror(e);
        }
        openRequest.onupgradeneeded = function openRequest_onupgradeneeded() {
          console.log('creating db');
          var db = openRequest.result;
          db.createObjectStore(FS.DB_STORE_NAME);
        };
        openRequest.onsuccess = function openRequest_onsuccess() {
          var db = openRequest.result;
          var transaction = db.transaction([FS.DB_STORE_NAME], 'readwrite');
          var files = transaction.objectStore(FS.DB_STORE_NAME);
          var ok = 0, fail = 0, total = paths.length;
          function finish() {
            if (fail == 0) onload(); else onerror();
          }
          paths.forEach(function(path) {
            var putRequest = files.put(FS.analyzePath(path).object.contents, path);
            putRequest.onsuccess = function putRequest_onsuccess() { ok++; if (ok + fail == total) finish() };
            putRequest.onerror = function putRequest_onerror() { fail++; if (ok + fail == total) finish() };
          });
          transaction.onerror = onerror;
        };
        openRequest.onerror = onerror;
      },loadFilesFromDB:function (paths, onload, onerror) {
        onload = onload || function(){};
        onerror = onerror || function(){};
        var indexedDB = FS.indexedDB();
        try {
          var openRequest = indexedDB.open(FS.DB_NAME(), FS.DB_VERSION);
        } catch (e) {
          return onerror(e);
        }
        openRequest.onupgradeneeded = onerror; // no database to load from
        openRequest.onsuccess = function openRequest_onsuccess() {
          var db = openRequest.result;
          try {
            var transaction = db.transaction([FS.DB_STORE_NAME], 'readonly');
          } catch(e) {
            onerror(e);
            return;
          }
          var files = transaction.objectStore(FS.DB_STORE_NAME);
          var ok = 0, fail = 0, total = paths.length;
          function finish() {
            if (fail == 0) onload(); else onerror();
          }
          paths.forEach(function(path) {
            var getRequest = files.get(path);
            getRequest.onsuccess = function getRequest_onsuccess() {
              if (FS.analyzePath(path).exists) {
                FS.unlink(path);
              }
              FS.createDataFile(PATH.dirname(path), PATH.basename(path), getRequest.result, true, true, true);
              ok++;
              if (ok + fail == total) finish();
            };
            getRequest.onerror = function getRequest_onerror() { fail++; if (ok + fail == total) finish() };
          });
          transaction.onerror = onerror;
        };
        openRequest.onerror = onerror;
      }};
  
  
  
  
  function _mkport() { throw 'TODO' }var SOCKFS={mount:function (mount) {
        return FS.createNode(null, '/', 16384 | 511 /* 0777 */, 0);
      },createSocket:function (family, type, protocol) {
        var streaming = type == 1;
        if (protocol) {
          assert(streaming == (protocol == 6)); // if SOCK_STREAM, must be tcp
        }
  
        // create our internal socket structure
        var sock = {
          family: family,
          type: type,
          protocol: protocol,
          server: null,
          peers: {},
          pending: [],
          recv_queue: [],
          sock_ops: SOCKFS.websocket_sock_ops
        };
  
        // create the filesystem node to store the socket structure
        var name = SOCKFS.nextname();
        var node = FS.createNode(SOCKFS.root, name, 49152, 0);
        node.sock = sock;
  
        // and the wrapping stream that enables library functions such
        // as read and write to indirectly interact with the socket
        var stream = FS.createStream({
          path: name,
          node: node,
          flags: FS.modeStringToFlags('r+'),
          seekable: false,
          stream_ops: SOCKFS.stream_ops
        });
  
        // map the new stream to the socket structure (sockets have a 1:1
        // relationship with a stream)
        sock.stream = stream;
  
        return sock;
      },getSocket:function (fd) {
        var stream = FS.getStream(fd);
        if (!stream || !FS.isSocket(stream.node.mode)) {
          return null;
        }
        return stream.node.sock;
      },stream_ops:{poll:function (stream) {
          var sock = stream.node.sock;
          return sock.sock_ops.poll(sock);
        },ioctl:function (stream, request, varargs) {
          var sock = stream.node.sock;
          return sock.sock_ops.ioctl(sock, request, varargs);
        },read:function (stream, buffer, offset, length, position /* ignored */) {
          var sock = stream.node.sock;
          var msg = sock.sock_ops.recvmsg(sock, length);
          if (!msg) {
            // socket is closed
            return 0;
          }
          buffer.set(msg.buffer, offset);
          return msg.buffer.length;
        },write:function (stream, buffer, offset, length, position /* ignored */) {
          var sock = stream.node.sock;
          return sock.sock_ops.sendmsg(sock, buffer, offset, length);
        },close:function (stream) {
          var sock = stream.node.sock;
          sock.sock_ops.close(sock);
        }},nextname:function () {
        if (!SOCKFS.nextname.current) {
          SOCKFS.nextname.current = 0;
        }
        return 'socket[' + (SOCKFS.nextname.current++) + ']';
      },websocket_sock_ops:{createPeer:function (sock, addr, port) {
          var ws;
  
          if (typeof addr === 'object') {
            ws = addr;
            addr = null;
            port = null;
          }
  
          if (ws) {
            // for sockets that've already connected (e.g. we're the server)
            // we can inspect the _socket property for the address
            if (ws._socket) {
              addr = ws._socket.remoteAddress;
              port = ws._socket.remotePort;
            }
            // if we're just now initializing a connection to the remote,
            // inspect the url property
            else {
              var result = /ws[s]?:\/\/([^:]+):(\d+)/.exec(ws.url);
              if (!result) {
                throw new Error('WebSocket URL must be in the format ws(s)://address:port');
              }
              addr = result[1];
              port = parseInt(result[2], 10);
            }
          } else {
            // create the actual websocket object and connect
            try {
              // runtimeConfig gets set to true if WebSocket runtime configuration is available.
              var runtimeConfig = (Module['websocket'] && ('object' === typeof Module['websocket']));
  
              // The default value is 'ws://' the replace is needed because the compiler replaces "//" comments with '#'
              // comments without checking context, so we'd end up with ws:#, the replace swaps the "#" for "//" again.
              var url = 'ws:#'.replace('#', '//');
  
              if (runtimeConfig) {
                if ('string' === typeof Module['websocket']['url']) {
                  url = Module['websocket']['url']; // Fetch runtime WebSocket URL config.
                }
              }
  
              if (url === 'ws://' || url === 'wss://') { // Is the supplied URL config just a prefix, if so complete it.
                url = url + addr + ':' + port;
              }
  
              // Make the WebSocket subprotocol (Sec-WebSocket-Protocol) default to binary if no configuration is set.
              var subProtocols = 'binary'; // The default value is 'binary'
  
              if (runtimeConfig) {
                if ('string' === typeof Module['websocket']['subprotocol']) {
                  subProtocols = Module['websocket']['subprotocol']; // Fetch runtime WebSocket subprotocol config.
                }
              }
  
              // The regex trims the string (removes spaces at the beginning and end, then splits the string by
              // <any space>,<any space> into an Array. Whitespace removal is important for Websockify and ws.
              subProtocols = subProtocols.replace(/^ +| +$/g,"").split(/ *, */);
  
              // The node ws library API for specifying optional subprotocol is slightly different than the browser's.
              var opts = ENVIRONMENT_IS_NODE ? {'protocol': subProtocols.toString()} : subProtocols;
  
              // If node we use the ws library.
              var WebSocket = ENVIRONMENT_IS_NODE ? require('ws') : window['WebSocket'];
              ws = new WebSocket(url, opts);
              ws.binaryType = 'arraybuffer';
            } catch (e) {
              throw new FS.ErrnoError(ERRNO_CODES.EHOSTUNREACH);
            }
          }
  
  
          var peer = {
            addr: addr,
            port: port,
            socket: ws,
            dgram_send_queue: []
          };
  
          SOCKFS.websocket_sock_ops.addPeer(sock, peer);
          SOCKFS.websocket_sock_ops.handlePeerEvents(sock, peer);
  
          // if this is a bound dgram socket, send the port number first to allow
          // us to override the ephemeral port reported to us by remotePort on the
          // remote end.
          if (sock.type === 2 && typeof sock.sport !== 'undefined') {
            peer.dgram_send_queue.push(new Uint8Array([
                255, 255, 255, 255,
                'p'.charCodeAt(0), 'o'.charCodeAt(0), 'r'.charCodeAt(0), 't'.charCodeAt(0),
                ((sock.sport & 0xff00) >> 8) , (sock.sport & 0xff)
            ]));
          }
  
          return peer;
        },getPeer:function (sock, addr, port) {
          return sock.peers[addr + ':' + port];
        },addPeer:function (sock, peer) {
          sock.peers[peer.addr + ':' + peer.port] = peer;
        },removePeer:function (sock, peer) {
          delete sock.peers[peer.addr + ':' + peer.port];
        },handlePeerEvents:function (sock, peer) {
          var first = true;
  
          var handleOpen = function () {
            try {
              var queued = peer.dgram_send_queue.shift();
              while (queued) {
                peer.socket.send(queued);
                queued = peer.dgram_send_queue.shift();
              }
            } catch (e) {
              // not much we can do here in the way of proper error handling as we've already
              // lied and said this data was sent. shut it down.
              peer.socket.close();
            }
          };
  
          function handleMessage(data) {
            assert(typeof data !== 'string' && data.byteLength !== undefined);  // must receive an ArrayBuffer
            data = new Uint8Array(data);  // make a typed array view on the array buffer
  
  
            // if this is the port message, override the peer's port with it
            var wasfirst = first;
            first = false;
            if (wasfirst &&
                data.length === 10 &&
                data[0] === 255 && data[1] === 255 && data[2] === 255 && data[3] === 255 &&
                data[4] === 'p'.charCodeAt(0) && data[5] === 'o'.charCodeAt(0) && data[6] === 'r'.charCodeAt(0) && data[7] === 't'.charCodeAt(0)) {
              // update the peer's port and it's key in the peer map
              var newport = ((data[8] << 8) | data[9]);
              SOCKFS.websocket_sock_ops.removePeer(sock, peer);
              peer.port = newport;
              SOCKFS.websocket_sock_ops.addPeer(sock, peer);
              return;
            }
  
            sock.recv_queue.push({ addr: peer.addr, port: peer.port, data: data });
          };
  
          if (ENVIRONMENT_IS_NODE) {
            peer.socket.on('open', handleOpen);
            peer.socket.on('message', function(data, flags) {
              if (!flags.binary) {
                return;
              }
              handleMessage((new Uint8Array(data)).buffer);  // copy from node Buffer -> ArrayBuffer
            });
            peer.socket.on('error', function() {
              // don't throw
            });
          } else {
            peer.socket.onopen = handleOpen;
            peer.socket.onmessage = function peer_socket_onmessage(event) {
              handleMessage(event.data);
            };
          }
        },poll:function (sock) {
          if (sock.type === 1 && sock.server) {
            // listen sockets should only say they're available for reading
            // if there are pending clients.
            return sock.pending.length ? (64 | 1) : 0;
          }
  
          var mask = 0;
          var dest = sock.type === 1 ?  // we only care about the socket state for connection-based sockets
            SOCKFS.websocket_sock_ops.getPeer(sock, sock.daddr, sock.dport) :
            null;
  
          if (sock.recv_queue.length ||
              !dest ||  // connection-less sockets are always ready to read
              (dest && dest.socket.readyState === dest.socket.CLOSING) ||
              (dest && dest.socket.readyState === dest.socket.CLOSED)) {  // let recv return 0 once closed
            mask |= (64 | 1);
          }
  
          if (!dest ||  // connection-less sockets are always ready to write
              (dest && dest.socket.readyState === dest.socket.OPEN)) {
            mask |= 4;
          }
  
          if ((dest && dest.socket.readyState === dest.socket.CLOSING) ||
              (dest && dest.socket.readyState === dest.socket.CLOSED)) {
            mask |= 16;
          }
  
          return mask;
        },ioctl:function (sock, request, arg) {
          switch (request) {
            case 21531:
              var bytes = 0;
              if (sock.recv_queue.length) {
                bytes = sock.recv_queue[0].data.length;
              }
              HEAP32[((arg)>>2)]=bytes;
              return 0;
            default:
              return ERRNO_CODES.EINVAL;
          }
        },close:function (sock) {
          // if we've spawned a listen server, close it
          if (sock.server) {
            try {
              sock.server.close();
            } catch (e) {
            }
            sock.server = null;
          }
          // close any peer connections
          var peers = Object.keys(sock.peers);
          for (var i = 0; i < peers.length; i++) {
            var peer = sock.peers[peers[i]];
            try {
              peer.socket.close();
            } catch (e) {
            }
            SOCKFS.websocket_sock_ops.removePeer(sock, peer);
          }
          return 0;
        },bind:function (sock, addr, port) {
          if (typeof sock.saddr !== 'undefined' || typeof sock.sport !== 'undefined') {
            throw new FS.ErrnoError(ERRNO_CODES.EINVAL);  // already bound
          }
          sock.saddr = addr;
          sock.sport = port || _mkport();
          // in order to emulate dgram sockets, we need to launch a listen server when
          // binding on a connection-less socket
          // note: this is only required on the server side
          if (sock.type === 2) {
            // close the existing server if it exists
            if (sock.server) {
              sock.server.close();
              sock.server = null;
            }
            // swallow error operation not supported error that occurs when binding in the
            // browser where this isn't supported
            try {
              sock.sock_ops.listen(sock, 0);
            } catch (e) {
              if (!(e instanceof FS.ErrnoError)) throw e;
              if (e.errno !== ERRNO_CODES.EOPNOTSUPP) throw e;
            }
          }
        },connect:function (sock, addr, port) {
          if (sock.server) {
            throw new FS.ErrnoError(ERRNO_CODS.EOPNOTSUPP);
          }
  
          // TODO autobind
          // if (!sock.addr && sock.type == 2) {
          // }
  
          // early out if we're already connected / in the middle of connecting
          if (typeof sock.daddr !== 'undefined' && typeof sock.dport !== 'undefined') {
            var dest = SOCKFS.websocket_sock_ops.getPeer(sock, sock.daddr, sock.dport);
            if (dest) {
              if (dest.socket.readyState === dest.socket.CONNECTING) {
                throw new FS.ErrnoError(ERRNO_CODES.EALREADY);
              } else {
                throw new FS.ErrnoError(ERRNO_CODES.EISCONN);
              }
            }
          }
  
          // add the socket to our peer list and set our
          // destination address / port to match
          var peer = SOCKFS.websocket_sock_ops.createPeer(sock, addr, port);
          sock.daddr = peer.addr;
          sock.dport = peer.port;
  
          // always "fail" in non-blocking mode
          throw new FS.ErrnoError(ERRNO_CODES.EINPROGRESS);
        },listen:function (sock, backlog) {
          if (!ENVIRONMENT_IS_NODE) {
            throw new FS.ErrnoError(ERRNO_CODES.EOPNOTSUPP);
          }
          if (sock.server) {
             throw new FS.ErrnoError(ERRNO_CODES.EINVAL);  // already listening
          }
          var WebSocketServer = require('ws').Server;
          var host = sock.saddr;
          sock.server = new WebSocketServer({
            host: host,
            port: sock.sport
            // TODO support backlog
          });
  
          sock.server.on('connection', function(ws) {
            if (sock.type === 1) {
              var newsock = SOCKFS.createSocket(sock.family, sock.type, sock.protocol);
  
              // create a peer on the new socket
              var peer = SOCKFS.websocket_sock_ops.createPeer(newsock, ws);
              newsock.daddr = peer.addr;
              newsock.dport = peer.port;
  
              // push to queue for accept to pick up
              sock.pending.push(newsock);
            } else {
              // create a peer on the listen socket so calling sendto
              // with the listen socket and an address will resolve
              // to the correct client
              SOCKFS.websocket_sock_ops.createPeer(sock, ws);
            }
          });
          sock.server.on('closed', function() {
            sock.server = null;
          });
          sock.server.on('error', function() {
            // don't throw
          });
        },accept:function (listensock) {
          if (!listensock.server) {
            throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
          }
          var newsock = listensock.pending.shift();
          newsock.stream.flags = listensock.stream.flags;
          return newsock;
        },getname:function (sock, peer) {
          var addr, port;
          if (peer) {
            if (sock.daddr === undefined || sock.dport === undefined) {
              throw new FS.ErrnoError(ERRNO_CODES.ENOTCONN);
            }
            addr = sock.daddr;
            port = sock.dport;
          } else {
            // TODO saddr and sport will be set for bind()'d UDP sockets, but what
            // should we be returning for TCP sockets that've been connect()'d?
            addr = sock.saddr || 0;
            port = sock.sport || 0;
          }
          return { addr: addr, port: port };
        },sendmsg:function (sock, buffer, offset, length, addr, port) {
          if (sock.type === 2) {
            // connection-less sockets will honor the message address,
            // and otherwise fall back to the bound destination address
            if (addr === undefined || port === undefined) {
              addr = sock.daddr;
              port = sock.dport;
            }
            // if there was no address to fall back to, error out
            if (addr === undefined || port === undefined) {
              throw new FS.ErrnoError(ERRNO_CODES.EDESTADDRREQ);
            }
          } else {
            // connection-based sockets will only use the bound
            addr = sock.daddr;
            port = sock.dport;
          }
  
          // find the peer for the destination address
          var dest = SOCKFS.websocket_sock_ops.getPeer(sock, addr, port);
  
          // early out if not connected with a connection-based socket
          if (sock.type === 1) {
            if (!dest || dest.socket.readyState === dest.socket.CLOSING || dest.socket.readyState === dest.socket.CLOSED) {
              throw new FS.ErrnoError(ERRNO_CODES.ENOTCONN);
            } else if (dest.socket.readyState === dest.socket.CONNECTING) {
              throw new FS.ErrnoError(ERRNO_CODES.EAGAIN);
            }
          }
  
          // create a copy of the incoming data to send, as the WebSocket API
          // doesn't work entirely with an ArrayBufferView, it'll just send
          // the entire underlying buffer
          var data;
          if (buffer instanceof Array || buffer instanceof ArrayBuffer) {
            data = buffer.slice(offset, offset + length);
          } else {  // ArrayBufferView
            data = buffer.buffer.slice(buffer.byteOffset + offset, buffer.byteOffset + offset + length);
          }
  
          // if we're emulating a connection-less dgram socket and don't have
          // a cached connection, queue the buffer to send upon connect and
          // lie, saying the data was sent now.
          if (sock.type === 2) {
            if (!dest || dest.socket.readyState !== dest.socket.OPEN) {
              // if we're not connected, open a new connection
              if (!dest || dest.socket.readyState === dest.socket.CLOSING || dest.socket.readyState === dest.socket.CLOSED) {
                dest = SOCKFS.websocket_sock_ops.createPeer(sock, addr, port);
              }
              dest.dgram_send_queue.push(data);
              return length;
            }
          }
  
          try {
            // send the actual data
            dest.socket.send(data);
            return length;
          } catch (e) {
            throw new FS.ErrnoError(ERRNO_CODES.EINVAL);
          }
        },recvmsg:function (sock, length) {
          // http://pubs.opengroup.org/onlinepubs/7908799/xns/recvmsg.html
          if (sock.type === 1 && sock.server) {
            // tcp servers should not be recv()'ing on the listen socket
            throw new FS.ErrnoError(ERRNO_CODES.ENOTCONN);
          }
  
          var queued = sock.recv_queue.shift();
          if (!queued) {
            if (sock.type === 1) {
              var dest = SOCKFS.websocket_sock_ops.getPeer(sock, sock.daddr, sock.dport);
  
              if (!dest) {
                // if we have a destination address but are not connected, error out
                throw new FS.ErrnoError(ERRNO_CODES.ENOTCONN);
              }
              else if (dest.socket.readyState === dest.socket.CLOSING || dest.socket.readyState === dest.socket.CLOSED) {
                // return null if the socket has closed
                return null;
              }
              else {
                // else, our socket is in a valid state but truly has nothing available
                throw new FS.ErrnoError(ERRNO_CODES.EAGAIN);
              }
            } else {
              throw new FS.ErrnoError(ERRNO_CODES.EAGAIN);
            }
          }
  
          // queued.data will be an ArrayBuffer if it's unadulterated, but if it's
          // requeued TCP data it'll be an ArrayBufferView
          var queuedLength = queued.data.byteLength || queued.data.length;
          var queuedOffset = queued.data.byteOffset || 0;
          var queuedBuffer = queued.data.buffer || queued.data;
          var bytesRead = Math.min(length, queuedLength);
          var res = {
            buffer: new Uint8Array(queuedBuffer, queuedOffset, bytesRead),
            addr: queued.addr,
            port: queued.port
          };
  
  
          // push back any unread data for TCP connections
          if (sock.type === 1 && bytesRead < queuedLength) {
            var bytesRemaining = queuedLength - bytesRead;
            queued.data = new Uint8Array(queuedBuffer, queuedOffset + bytesRead, bytesRemaining);
            sock.recv_queue.unshift(queued);
          }
  
          return res;
        }}};function _recv(fd, buf, len, flags) {
      var sock = SOCKFS.getSocket(fd);
      if (!sock) {
        ___setErrNo(ERRNO_CODES.EBADF);
        return -1;
      }
      // TODO honor flags
      return _read(fd, buf, len);
    }
  
  function _pread(fildes, buf, nbyte, offset) {
      // ssize_t pread(int fildes, void *buf, size_t nbyte, off_t offset);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/read.html
      var stream = FS.getStream(fildes);
      if (!stream) {
        ___setErrNo(ERRNO_CODES.EBADF);
        return -1;
      }
      try {
        var slab = HEAP8;
        return FS.read(stream, slab, buf, nbyte, offset);
      } catch (e) {
        FS.handleFSError(e);
        return -1;
      }
    }function _read(fildes, buf, nbyte) {
      // ssize_t read(int fildes, void *buf, size_t nbyte);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/read.html
      var stream = FS.getStream(fildes);
      if (!stream) {
        ___setErrNo(ERRNO_CODES.EBADF);
        return -1;
      }
  
  
      try {
        var slab = HEAP8;
        return FS.read(stream, slab, buf, nbyte);
      } catch (e) {
        FS.handleFSError(e);
        return -1;
      }
    }function _fread(ptr, size, nitems, stream) {
      // size_t fread(void *restrict ptr, size_t size, size_t nitems, FILE *restrict stream);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/fread.html
      var bytesToRead = nitems * size;
      if (bytesToRead == 0) {
        return 0;
      }
      var bytesRead = 0;
      var streamObj = FS.getStreamFromPtr(stream);
      if (!streamObj) {
        ___setErrNo(ERRNO_CODES.EBADF);
        return 0;
      }
      while (streamObj.ungotten.length && bytesToRead > 0) {
        HEAP8[((ptr++)>>0)]=streamObj.ungotten.pop();
        bytesToRead--;
        bytesRead++;
      }
      var err = _read(streamObj.fd, ptr, bytesToRead);
      if (err == -1) {
        if (streamObj) streamObj.error = true;
        return 0;
      }
      bytesRead += err;
      if (bytesRead < bytesToRead) streamObj.eof = true;
      return Math.floor(bytesRead / size);
    }

  
  function _close(fildes) {
      // int close(int fildes);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/close.html
      var stream = FS.getStream(fildes);
      if (!stream) {
        ___setErrNo(ERRNO_CODES.EBADF);
        return -1;
      }
      try {
        FS.close(stream);
        return 0;
      } catch (e) {
        FS.handleFSError(e);
        return -1;
      }
    }
  
  function _fsync(fildes) {
      // int fsync(int fildes);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/fsync.html
      var stream = FS.getStream(fildes);
      if (stream) {
        // We write directly to the file system, so there's nothing to do here.
        return 0;
      } else {
        ___setErrNo(ERRNO_CODES.EBADF);
        return -1;
      }
    }
  
  function _fileno(stream) {
      // int fileno(FILE *stream);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/fileno.html
      stream = FS.getStreamFromPtr(stream);
      if (!stream) return -1;
      return stream.fd;
    }function _fclose(stream) {
      // int fclose(FILE *stream);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/fclose.html
      var fd = _fileno(stream);
      _fsync(fd);
      return _close(fd);
    }

  function _abort() {
      Module['abort']();
    }

  
  
  
  
  function _send(fd, buf, len, flags) {
      var sock = SOCKFS.getSocket(fd);
      if (!sock) {
        ___setErrNo(ERRNO_CODES.EBADF);
        return -1;
      }
      // TODO honor flags
      return _write(fd, buf, len);
    }
  
  function _pwrite(fildes, buf, nbyte, offset) {
      // ssize_t pwrite(int fildes, const void *buf, size_t nbyte, off_t offset);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/write.html
      var stream = FS.getStream(fildes);
      if (!stream) {
        ___setErrNo(ERRNO_CODES.EBADF);
        return -1;
      }
      try {
        var slab = HEAP8;
        return FS.write(stream, slab, buf, nbyte, offset);
      } catch (e) {
        FS.handleFSError(e);
        return -1;
      }
    }function _write(fildes, buf, nbyte) {
      // ssize_t write(int fildes, const void *buf, size_t nbyte);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/write.html
      var stream = FS.getStream(fildes);
      if (!stream) {
        ___setErrNo(ERRNO_CODES.EBADF);
        return -1;
      }
  
  
      try {
        var slab = HEAP8;
        return FS.write(stream, slab, buf, nbyte);
      } catch (e) {
        FS.handleFSError(e);
        return -1;
      }
    }function _fwrite(ptr, size, nitems, stream) {
      // size_t fwrite(const void *restrict ptr, size_t size, size_t nitems, FILE *restrict stream);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/fwrite.html
      var bytesToWrite = nitems * size;
      if (bytesToWrite == 0) return 0;
      var fd = _fileno(stream);
      var bytesWritten = _write(fd, ptr, bytesToWrite);
      if (bytesWritten == -1) {
        var streamObj = FS.getStreamFromPtr(stream);
        if (streamObj) streamObj.error = true;
        return 0;
      } else {
        return Math.floor(bytesWritten / size);
      }
    }
  
  
   
  Module["_strlen"] = _strlen;
  
  function __reallyNegative(x) {
      return x < 0 || (x === 0 && (1/x) === -Infinity);
    }function __formatString(format, varargs) {
      var textIndex = format;
      var argIndex = 0;
      function getNextArg(type) {
        // NOTE: Explicitly ignoring type safety. Otherwise this fails:
        //       int x = 4; printf("%c\n", (char)x);
        var ret;
        if (type === 'double') {
          ret = (HEAP32[((tempDoublePtr)>>2)]=HEAP32[(((varargs)+(argIndex))>>2)],HEAP32[(((tempDoublePtr)+(4))>>2)]=HEAP32[(((varargs)+((argIndex)+(4)))>>2)],(+(HEAPF64[(tempDoublePtr)>>3])));
        } else if (type == 'i64') {
          ret = [HEAP32[(((varargs)+(argIndex))>>2)],
                 HEAP32[(((varargs)+(argIndex+4))>>2)]];
  
        } else {
          type = 'i32'; // varargs are always i32, i64, or double
          ret = HEAP32[(((varargs)+(argIndex))>>2)];
        }
        argIndex += Runtime.getNativeFieldSize(type);
        return ret;
      }
  
      var ret = [];
      var curr, next, currArg;
      while(1) {
        var startTextIndex = textIndex;
        curr = HEAP8[((textIndex)>>0)];
        if (curr === 0) break;
        next = HEAP8[((textIndex+1)>>0)];
        if (curr == 37) {
          // Handle flags.
          var flagAlwaysSigned = false;
          var flagLeftAlign = false;
          var flagAlternative = false;
          var flagZeroPad = false;
          var flagPadSign = false;
          flagsLoop: while (1) {
            switch (next) {
              case 43:
                flagAlwaysSigned = true;
                break;
              case 45:
                flagLeftAlign = true;
                break;
              case 35:
                flagAlternative = true;
                break;
              case 48:
                if (flagZeroPad) {
                  break flagsLoop;
                } else {
                  flagZeroPad = true;
                  break;
                }
              case 32:
                flagPadSign = true;
                break;
              default:
                break flagsLoop;
            }
            textIndex++;
            next = HEAP8[((textIndex+1)>>0)];
          }
  
          // Handle width.
          var width = 0;
          if (next == 42) {
            width = getNextArg('i32');
            textIndex++;
            next = HEAP8[((textIndex+1)>>0)];
          } else {
            while (next >= 48 && next <= 57) {
              width = width * 10 + (next - 48);
              textIndex++;
              next = HEAP8[((textIndex+1)>>0)];
            }
          }
  
          // Handle precision.
          var precisionSet = false, precision = -1;
          if (next == 46) {
            precision = 0;
            precisionSet = true;
            textIndex++;
            next = HEAP8[((textIndex+1)>>0)];
            if (next == 42) {
              precision = getNextArg('i32');
              textIndex++;
            } else {
              while(1) {
                var precisionChr = HEAP8[((textIndex+1)>>0)];
                if (precisionChr < 48 ||
                    precisionChr > 57) break;
                precision = precision * 10 + (precisionChr - 48);
                textIndex++;
              }
            }
            next = HEAP8[((textIndex+1)>>0)];
          }
          if (precision < 0) {
            precision = 6; // Standard default.
            precisionSet = false;
          }
  
          // Handle integer sizes. WARNING: These assume a 32-bit architecture!
          var argSize;
          switch (String.fromCharCode(next)) {
            case 'h':
              var nextNext = HEAP8[((textIndex+2)>>0)];
              if (nextNext == 104) {
                textIndex++;
                argSize = 1; // char (actually i32 in varargs)
              } else {
                argSize = 2; // short (actually i32 in varargs)
              }
              break;
            case 'l':
              var nextNext = HEAP8[((textIndex+2)>>0)];
              if (nextNext == 108) {
                textIndex++;
                argSize = 8; // long long
              } else {
                argSize = 4; // long
              }
              break;
            case 'L': // long long
            case 'q': // int64_t
            case 'j': // intmax_t
              argSize = 8;
              break;
            case 'z': // size_t
            case 't': // ptrdiff_t
            case 'I': // signed ptrdiff_t or unsigned size_t
              argSize = 4;
              break;
            default:
              argSize = null;
          }
          if (argSize) textIndex++;
          next = HEAP8[((textIndex+1)>>0)];
  
          // Handle type specifier.
          switch (String.fromCharCode(next)) {
            case 'd': case 'i': case 'u': case 'o': case 'x': case 'X': case 'p': {
              // Integer.
              var signed = next == 100 || next == 105;
              argSize = argSize || 4;
              var currArg = getNextArg('i' + (argSize * 8));
              var origArg = currArg;
              var argText;
              // Flatten i64-1 [low, high] into a (slightly rounded) double
              if (argSize == 8) {
                currArg = Runtime.makeBigInt(currArg[0], currArg[1], next == 117);
              }
              // Truncate to requested size.
              if (argSize <= 4) {
                var limit = Math.pow(256, argSize) - 1;
                currArg = (signed ? reSign : unSign)(currArg & limit, argSize * 8);
              }
              // Format the number.
              var currAbsArg = Math.abs(currArg);
              var prefix = '';
              if (next == 100 || next == 105) {
                if (argSize == 8 && i64Math) argText = i64Math.stringify(origArg[0], origArg[1], null); else
                argText = reSign(currArg, 8 * argSize, 1).toString(10);
              } else if (next == 117) {
                if (argSize == 8 && i64Math) argText = i64Math.stringify(origArg[0], origArg[1], true); else
                argText = unSign(currArg, 8 * argSize, 1).toString(10);
                currArg = Math.abs(currArg);
              } else if (next == 111) {
                argText = (flagAlternative ? '0' : '') + currAbsArg.toString(8);
              } else if (next == 120 || next == 88) {
                prefix = (flagAlternative && currArg != 0) ? '0x' : '';
                if (argSize == 8 && i64Math) {
                  if (origArg[1]) {
                    argText = (origArg[1]>>>0).toString(16);
                    var lower = (origArg[0]>>>0).toString(16);
                    while (lower.length < 8) lower = '0' + lower;
                    argText += lower;
                  } else {
                    argText = (origArg[0]>>>0).toString(16);
                  }
                } else
                if (currArg < 0) {
                  // Represent negative numbers in hex as 2's complement.
                  currArg = -currArg;
                  argText = (currAbsArg - 1).toString(16);
                  var buffer = [];
                  for (var i = 0; i < argText.length; i++) {
                    buffer.push((0xF - parseInt(argText[i], 16)).toString(16));
                  }
                  argText = buffer.join('');
                  while (argText.length < argSize * 2) argText = 'f' + argText;
                } else {
                  argText = currAbsArg.toString(16);
                }
                if (next == 88) {
                  prefix = prefix.toUpperCase();
                  argText = argText.toUpperCase();
                }
              } else if (next == 112) {
                if (currAbsArg === 0) {
                  argText = '(nil)';
                } else {
                  prefix = '0x';
                  argText = currAbsArg.toString(16);
                }
              }
              if (precisionSet) {
                while (argText.length < precision) {
                  argText = '0' + argText;
                }
              }
  
              // Add sign if needed
              if (currArg >= 0) {
                if (flagAlwaysSigned) {
                  prefix = '+' + prefix;
                } else if (flagPadSign) {
                  prefix = ' ' + prefix;
                }
              }
  
              // Move sign to prefix so we zero-pad after the sign
              if (argText.charAt(0) == '-') {
                prefix = '-' + prefix;
                argText = argText.substr(1);
              }
  
              // Add padding.
              while (prefix.length + argText.length < width) {
                if (flagLeftAlign) {
                  argText += ' ';
                } else {
                  if (flagZeroPad) {
                    argText = '0' + argText;
                  } else {
                    prefix = ' ' + prefix;
                  }
                }
              }
  
              // Insert the result into the buffer.
              argText = prefix + argText;
              argText.split('').forEach(function(chr) {
                ret.push(chr.charCodeAt(0));
              });
              break;
            }
            case 'f': case 'F': case 'e': case 'E': case 'g': case 'G': {
              // Float.
              var currArg = getNextArg('double');
              var argText;
              if (isNaN(currArg)) {
                argText = 'nan';
                flagZeroPad = false;
              } else if (!isFinite(currArg)) {
                argText = (currArg < 0 ? '-' : '') + 'inf';
                flagZeroPad = false;
              } else {
                var isGeneral = false;
                var effectivePrecision = Math.min(precision, 20);
  
                // Convert g/G to f/F or e/E, as per:
                // http://pubs.opengroup.org/onlinepubs/9699919799/functions/printf.html
                if (next == 103 || next == 71) {
                  isGeneral = true;
                  precision = precision || 1;
                  var exponent = parseInt(currArg.toExponential(effectivePrecision).split('e')[1], 10);
                  if (precision > exponent && exponent >= -4) {
                    next = ((next == 103) ? 'f' : 'F').charCodeAt(0);
                    precision -= exponent + 1;
                  } else {
                    next = ((next == 103) ? 'e' : 'E').charCodeAt(0);
                    precision--;
                  }
                  effectivePrecision = Math.min(precision, 20);
                }
  
                if (next == 101 || next == 69) {
                  argText = currArg.toExponential(effectivePrecision);
                  // Make sure the exponent has at least 2 digits.
                  if (/[eE][-+]\d$/.test(argText)) {
                    argText = argText.slice(0, -1) + '0' + argText.slice(-1);
                  }
                } else if (next == 102 || next == 70) {
                  argText = currArg.toFixed(effectivePrecision);
                  if (currArg === 0 && __reallyNegative(currArg)) {
                    argText = '-' + argText;
                  }
                }
  
                var parts = argText.split('e');
                if (isGeneral && !flagAlternative) {
                  // Discard trailing zeros and periods.
                  while (parts[0].length > 1 && parts[0].indexOf('.') != -1 &&
                         (parts[0].slice(-1) == '0' || parts[0].slice(-1) == '.')) {
                    parts[0] = parts[0].slice(0, -1);
                  }
                } else {
                  // Make sure we have a period in alternative mode.
                  if (flagAlternative && argText.indexOf('.') == -1) parts[0] += '.';
                  // Zero pad until required precision.
                  while (precision > effectivePrecision++) parts[0] += '0';
                }
                argText = parts[0] + (parts.length > 1 ? 'e' + parts[1] : '');
  
                // Capitalize 'E' if needed.
                if (next == 69) argText = argText.toUpperCase();
  
                // Add sign.
                if (currArg >= 0) {
                  if (flagAlwaysSigned) {
                    argText = '+' + argText;
                  } else if (flagPadSign) {
                    argText = ' ' + argText;
                  }
                }
              }
  
              // Add padding.
              while (argText.length < width) {
                if (flagLeftAlign) {
                  argText += ' ';
                } else {
                  if (flagZeroPad && (argText[0] == '-' || argText[0] == '+')) {
                    argText = argText[0] + '0' + argText.slice(1);
                  } else {
                    argText = (flagZeroPad ? '0' : ' ') + argText;
                  }
                }
              }
  
              // Adjust case.
              if (next < 97) argText = argText.toUpperCase();
  
              // Insert the result into the buffer.
              argText.split('').forEach(function(chr) {
                ret.push(chr.charCodeAt(0));
              });
              break;
            }
            case 's': {
              // String.
              var arg = getNextArg('i8*');
              var argLength = arg ? _strlen(arg) : '(null)'.length;
              if (precisionSet) argLength = Math.min(argLength, precision);
              if (!flagLeftAlign) {
                while (argLength < width--) {
                  ret.push(32);
                }
              }
              if (arg) {
                for (var i = 0; i < argLength; i++) {
                  ret.push(HEAPU8[((arg++)>>0)]);
                }
              } else {
                ret = ret.concat(intArrayFromString('(null)'.substr(0, argLength), true));
              }
              if (flagLeftAlign) {
                while (argLength < width--) {
                  ret.push(32);
                }
              }
              break;
            }
            case 'c': {
              // Character.
              if (flagLeftAlign) ret.push(getNextArg('i8'));
              while (--width > 0) {
                ret.push(32);
              }
              if (!flagLeftAlign) ret.push(getNextArg('i8'));
              break;
            }
            case 'n': {
              // Write the length written so far to the next parameter.
              var ptr = getNextArg('i32*');
              HEAP32[((ptr)>>2)]=ret.length;
              break;
            }
            case '%': {
              // Literal percent sign.
              ret.push(curr);
              break;
            }
            default: {
              // Unknown specifiers remain untouched.
              for (var i = startTextIndex; i < textIndex + 2; i++) {
                ret.push(HEAP8[((i)>>0)]);
              }
            }
          }
          textIndex += 2;
          // TODO: Support a/A (hex float) and m (last error) specifiers.
          // TODO: Support %1${specifier} for arg selection.
        } else {
          ret.push(curr);
          textIndex += 1;
        }
      }
      return ret;
    }function _fprintf(stream, format, varargs) {
      // int fprintf(FILE *restrict stream, const char *restrict format, ...);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/printf.html
      var result = __formatString(format, varargs);
      var stack = Runtime.stackSave();
      var ret = _fwrite(allocate(result, 'i8', ALLOC_STACK), 1, result.length, stream);
      Runtime.stackRestore(stack);
      return ret;
    }function _printf(format, varargs) {
      // int printf(const char *restrict format, ...);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/printf.html
      var stdout = HEAP32[((_stdout)>>2)];
      return _fprintf(stdout, format, varargs);
    }


  
  function _open(path, oflag, varargs) {
      // int open(const char *path, int oflag, ...);
      // http://pubs.opengroup.org/onlinepubs/009695399/functions/open.html
      var mode = HEAP32[((varargs)>>2)];
      path = Pointer_stringify(path);
      try {
        var stream = FS.open(path, oflag, mode);
        return stream.fd;
      } catch (e) {
        FS.handleFSError(e);
        return -1;
      }
    }function _fopen(filename, mode) {
      // FILE *fopen(const char *restrict filename, const char *restrict mode);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/fopen.html
      var flags;
      mode = Pointer_stringify(mode);
      if (mode[0] == 'r') {
        if (mode.indexOf('+') != -1) {
          flags = 2;
        } else {
          flags = 0;
        }
      } else if (mode[0] == 'w') {
        if (mode.indexOf('+') != -1) {
          flags = 2;
        } else {
          flags = 1;
        }
        flags |= 64;
        flags |= 512;
      } else if (mode[0] == 'a') {
        if (mode.indexOf('+') != -1) {
          flags = 2;
        } else {
          flags = 1;
        }
        flags |= 64;
        flags |= 1024;
      } else {
        ___setErrNo(ERRNO_CODES.EINVAL);
        return 0;
      }
      var fd = _open(filename, flags, allocate([0x1FF, 0, 0, 0], 'i32', ALLOC_STACK));  // All creation permissions.
      return fd === -1 ? 0 : FS.getPtrForStream(FS.getStream(fd));
    }

  function _feof(stream) {
      // int feof(FILE *stream);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/feof.html
      stream = FS.getStreamFromPtr(stream);
      return Number(stream && stream.eof);
    }

  
  function _emscripten_memcpy_big(dest, src, num) {
      HEAPU8.set(HEAPU8.subarray(src, src+num), dest);
      return dest;
    } 
  Module["_memcpy"] = _memcpy;

  function _sysconf(name) {
      // long sysconf(int name);
      // http://pubs.opengroup.org/onlinepubs/009695399/functions/sysconf.html
      switch(name) {
        case 30: return PAGE_SIZE;
        case 132:
        case 133:
        case 12:
        case 137:
        case 138:
        case 15:
        case 235:
        case 16:
        case 17:
        case 18:
        case 19:
        case 20:
        case 149:
        case 13:
        case 10:
        case 236:
        case 153:
        case 9:
        case 21:
        case 22:
        case 159:
        case 154:
        case 14:
        case 77:
        case 78:
        case 139:
        case 80:
        case 81:
        case 79:
        case 82:
        case 68:
        case 67:
        case 164:
        case 11:
        case 29:
        case 47:
        case 48:
        case 95:
        case 52:
        case 51:
        case 46:
          return 200809;
        case 27:
        case 246:
        case 127:
        case 128:
        case 23:
        case 24:
        case 160:
        case 161:
        case 181:
        case 182:
        case 242:
        case 183:
        case 184:
        case 243:
        case 244:
        case 245:
        case 165:
        case 178:
        case 179:
        case 49:
        case 50:
        case 168:
        case 169:
        case 175:
        case 170:
        case 171:
        case 172:
        case 97:
        case 76:
        case 32:
        case 173:
        case 35:
          return -1;
        case 176:
        case 177:
        case 7:
        case 155:
        case 8:
        case 157:
        case 125:
        case 126:
        case 92:
        case 93:
        case 129:
        case 130:
        case 131:
        case 94:
        case 91:
          return 1;
        case 74:
        case 60:
        case 69:
        case 70:
        case 4:
          return 1024;
        case 31:
        case 42:
        case 72:
          return 32;
        case 87:
        case 26:
        case 33:
          return 2147483647;
        case 34:
        case 1:
          return 47839;
        case 38:
        case 36:
          return 99;
        case 43:
        case 37:
          return 2048;
        case 0: return 2097152;
        case 3: return 65536;
        case 28: return 32768;
        case 44: return 32767;
        case 75: return 16384;
        case 39: return 1000;
        case 89: return 700;
        case 71: return 256;
        case 40: return 255;
        case 2: return 100;
        case 180: return 64;
        case 25: return 20;
        case 5: return 16;
        case 6: return 6;
        case 73: return 4;
        case 84: return 1;
      }
      ___setErrNo(ERRNO_CODES.EINVAL);
      return -1;
    }

  
  function _fputs(s, stream) {
      // int fputs(const char *restrict s, FILE *restrict stream);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/fputs.html
      var fd = _fileno(stream);
      return _write(fd, s, _strlen(s));
    }
  
  function _fputc(c, stream) {
      // int fputc(int c, FILE *stream);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/fputc.html
      var chr = unSign(c & 0xFF);
      HEAP8[((_fputc.ret)>>0)]=chr;
      var fd = _fileno(stream);
      var ret = _write(fd, _fputc.ret, 1);
      if (ret == -1) {
        var streamObj = FS.getStreamFromPtr(stream);
        if (streamObj) streamObj.error = true;
        return -1;
      } else {
        return chr;
      }
    }function _puts(s) {
      // int puts(const char *s);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/puts.html
      // NOTE: puts() always writes an extra newline.
      var stdout = HEAP32[((_stdout)>>2)];
      var ret = _fputs(s, stdout);
      if (ret < 0) {
        return ret;
      } else {
        var newlineRet = _fputc(10, stdout);
        return (newlineRet < 0) ? -1 : ret + 1;
      }
    }


  
  function _lseek(fildes, offset, whence) {
      // off_t lseek(int fildes, off_t offset, int whence);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/lseek.html
      var stream = FS.getStream(fildes);
      if (!stream) {
        ___setErrNo(ERRNO_CODES.EBADF);
        return -1;
      }
      try {
        return FS.llseek(stream, offset, whence);
      } catch (e) {
        FS.handleFSError(e);
        return -1;
      }
    }function _fseek(stream, offset, whence) {
      // int fseek(FILE *stream, long offset, int whence);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/fseek.html
      var fd = _fileno(stream);
      var ret = _lseek(fd, offset, whence);
      if (ret == -1) {
        return -1;
      }
      stream = FS.getStreamFromPtr(stream);
      stream.eof = false;
      return 0;
    }

  function ___errno_location() {
      return ___errno_state;
    }

  function _ftell(stream) {
      // long ftell(FILE *stream);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/ftell.html
      stream = FS.getStreamFromPtr(stream);
      if (!stream) {
        ___setErrNo(ERRNO_CODES.EBADF);
        return -1;
      }
      if (FS.isChrdev(stream.node.mode)) {
        ___setErrNo(ERRNO_CODES.ESPIPE);
        return -1;
      } else {
        return stream.position;
      }
    }

   
  Module["_strcpy"] = _strcpy;

  var Browser={mainLoop:{scheduler:null,method:"",shouldPause:false,paused:false,queue:[],pause:function () {
          Browser.mainLoop.shouldPause = true;
        },resume:function () {
          if (Browser.mainLoop.paused) {
            Browser.mainLoop.paused = false;
            Browser.mainLoop.scheduler();
          }
          Browser.mainLoop.shouldPause = false;
        },updateStatus:function () {
          if (Module['setStatus']) {
            var message = Module['statusMessage'] || 'Please wait...';
            var remaining = Browser.mainLoop.remainingBlockers;
            var expected = Browser.mainLoop.expectedBlockers;
            if (remaining) {
              if (remaining < expected) {
                Module['setStatus'](message + ' (' + (expected - remaining) + '/' + expected + ')');
              } else {
                Module['setStatus'](message);
              }
            } else {
              Module['setStatus']('');
            }
          }
        }},isFullScreen:false,pointerLock:false,moduleContextCreatedCallbacks:[],workers:[],init:function () {
        if (!Module["preloadPlugins"]) Module["preloadPlugins"] = []; // needs to exist even in workers
  
        if (Browser.initted || ENVIRONMENT_IS_WORKER) return;
        Browser.initted = true;
  
        try {
          new Blob();
          Browser.hasBlobConstructor = true;
        } catch(e) {
          Browser.hasBlobConstructor = false;
          console.log("warning: no blob constructor, cannot create blobs with mimetypes");
        }
        Browser.BlobBuilder = typeof MozBlobBuilder != "undefined" ? MozBlobBuilder : (typeof WebKitBlobBuilder != "undefined" ? WebKitBlobBuilder : (!Browser.hasBlobConstructor ? console.log("warning: no BlobBuilder") : null));
        Browser.URLObject = typeof window != "undefined" ? (window.URL ? window.URL : window.webkitURL) : undefined;
        if (!Module.noImageDecoding && typeof Browser.URLObject === 'undefined') {
          console.log("warning: Browser does not support creating object URLs. Built-in browser image decoding will not be available.");
          Module.noImageDecoding = true;
        }
  
        // Support for plugins that can process preloaded files. You can add more of these to
        // your app by creating and appending to Module.preloadPlugins.
        //
        // Each plugin is asked if it can handle a file based on the file's name. If it can,
        // it is given the file's raw data. When it is done, it calls a callback with the file's
        // (possibly modified) data. For example, a plugin might decompress a file, or it
        // might create some side data structure for use later (like an Image element, etc.).
  
        var imagePlugin = {};
        imagePlugin['canHandle'] = function imagePlugin_canHandle(name) {
          return !Module.noImageDecoding && /\.(jpg|jpeg|png|bmp)$/i.test(name);
        };
        imagePlugin['handle'] = function imagePlugin_handle(byteArray, name, onload, onerror) {
          var b = null;
          if (Browser.hasBlobConstructor) {
            try {
              b = new Blob([byteArray], { type: Browser.getMimetype(name) });
              if (b.size !== byteArray.length) { // Safari bug #118630
                // Safari's Blob can only take an ArrayBuffer
                b = new Blob([(new Uint8Array(byteArray)).buffer], { type: Browser.getMimetype(name) });
              }
            } catch(e) {
              Runtime.warnOnce('Blob constructor present but fails: ' + e + '; falling back to blob builder');
            }
          }
          if (!b) {
            var bb = new Browser.BlobBuilder();
            bb.append((new Uint8Array(byteArray)).buffer); // we need to pass a buffer, and must copy the array to get the right data range
            b = bb.getBlob();
          }
          var url = Browser.URLObject.createObjectURL(b);
          var img = new Image();
          img.onload = function img_onload() {
            assert(img.complete, 'Image ' + name + ' could not be decoded');
            var canvas = document.createElement('canvas');
            canvas.width = img.width;
            canvas.height = img.height;
            var ctx = canvas.getContext('2d');
            ctx.drawImage(img, 0, 0);
            Module["preloadedImages"][name] = canvas;
            Browser.URLObject.revokeObjectURL(url);
            if (onload) onload(byteArray);
          };
          img.onerror = function img_onerror(event) {
            console.log('Image ' + url + ' could not be decoded');
            if (onerror) onerror();
          };
          img.src = url;
        };
        Module['preloadPlugins'].push(imagePlugin);
  
        var audioPlugin = {};
        audioPlugin['canHandle'] = function audioPlugin_canHandle(name) {
          return !Module.noAudioDecoding && name.substr(-4) in { '.ogg': 1, '.wav': 1, '.mp3': 1 };
        };
        audioPlugin['handle'] = function audioPlugin_handle(byteArray, name, onload, onerror) {
          var done = false;
          function finish(audio) {
            if (done) return;
            done = true;
            Module["preloadedAudios"][name] = audio;
            if (onload) onload(byteArray);
          }
          function fail() {
            if (done) return;
            done = true;
            Module["preloadedAudios"][name] = new Audio(); // empty shim
            if (onerror) onerror();
          }
          if (Browser.hasBlobConstructor) {
            try {
              var b = new Blob([byteArray], { type: Browser.getMimetype(name) });
            } catch(e) {
              return fail();
            }
            var url = Browser.URLObject.createObjectURL(b); // XXX we never revoke this!
            var audio = new Audio();
            audio.addEventListener('canplaythrough', function() { finish(audio) }, false); // use addEventListener due to chromium bug 124926
            audio.onerror = function audio_onerror(event) {
              if (done) return;
              console.log('warning: browser could not fully decode audio ' + name + ', trying slower base64 approach');
              function encode64(data) {
                var BASE = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
                var PAD = '=';
                var ret = '';
                var leftchar = 0;
                var leftbits = 0;
                for (var i = 0; i < data.length; i++) {
                  leftchar = (leftchar << 8) | data[i];
                  leftbits += 8;
                  while (leftbits >= 6) {
                    var curr = (leftchar >> (leftbits-6)) & 0x3f;
                    leftbits -= 6;
                    ret += BASE[curr];
                  }
                }
                if (leftbits == 2) {
                  ret += BASE[(leftchar&3) << 4];
                  ret += PAD + PAD;
                } else if (leftbits == 4) {
                  ret += BASE[(leftchar&0xf) << 2];
                  ret += PAD;
                }
                return ret;
              }
              audio.src = 'data:audio/x-' + name.substr(-3) + ';base64,' + encode64(byteArray);
              finish(audio); // we don't wait for confirmation this worked - but it's worth trying
            };
            audio.src = url;
            // workaround for chrome bug 124926 - we do not always get oncanplaythrough or onerror
            Browser.safeSetTimeout(function() {
              finish(audio); // try to use it even though it is not necessarily ready to play
            }, 10000);
          } else {
            return fail();
          }
        };
        Module['preloadPlugins'].push(audioPlugin);
  
        // Canvas event setup
  
        var canvas = Module['canvas'];
        if (canvas) {
          // forced aspect ratio can be enabled by defining 'forcedAspectRatio' on Module
          // Module['forcedAspectRatio'] = 4 / 3;
          
          canvas.requestPointerLock = canvas['requestPointerLock'] ||
                                      canvas['mozRequestPointerLock'] ||
                                      canvas['webkitRequestPointerLock'] ||
                                      canvas['msRequestPointerLock'] ||
                                      function(){};
          canvas.exitPointerLock = document['exitPointerLock'] ||
                                   document['mozExitPointerLock'] ||
                                   document['webkitExitPointerLock'] ||
                                   document['msExitPointerLock'] ||
                                   function(){}; // no-op if function does not exist
          canvas.exitPointerLock = canvas.exitPointerLock.bind(document);
  
          function pointerLockChange() {
            Browser.pointerLock = document['pointerLockElement'] === canvas ||
                                  document['mozPointerLockElement'] === canvas ||
                                  document['webkitPointerLockElement'] === canvas ||
                                  document['msPointerLockElement'] === canvas;
          }
  
          document.addEventListener('pointerlockchange', pointerLockChange, false);
          document.addEventListener('mozpointerlockchange', pointerLockChange, false);
          document.addEventListener('webkitpointerlockchange', pointerLockChange, false);
          document.addEventListener('mspointerlockchange', pointerLockChange, false);
  
          if (Module['elementPointerLock']) {
            canvas.addEventListener("click", function(ev) {
              if (!Browser.pointerLock && canvas.requestPointerLock) {
                canvas.requestPointerLock();
                ev.preventDefault();
              }
            }, false);
          }
        }
      },createContext:function (canvas, useWebGL, setInModule, webGLContextAttributes) {
        var ctx;
        var errorInfo = '?';
        function onContextCreationError(event) {
          errorInfo = event.statusMessage || errorInfo;
        }
        try {
          if (useWebGL) {
            var contextAttributes = {
              antialias: false,
              alpha: false
            };
  
            if (webGLContextAttributes) {
              for (var attribute in webGLContextAttributes) {
                contextAttributes[attribute] = webGLContextAttributes[attribute];
              }
            }
  
  
            canvas.addEventListener('webglcontextcreationerror', onContextCreationError, false);
            try {
              ['experimental-webgl', 'webgl'].some(function(webglId) {
                return ctx = canvas.getContext(webglId, contextAttributes);
              });
            } finally {
              canvas.removeEventListener('webglcontextcreationerror', onContextCreationError, false);
            }
          } else {
            ctx = canvas.getContext('2d');
          }
          if (!ctx) throw ':(';
        } catch (e) {
          Module.print('Could not create canvas: ' + [errorInfo, e]);
          return null;
        }
        if (useWebGL) {
          // Set the background of the WebGL canvas to black
          canvas.style.backgroundColor = "black";
  
          // Warn on context loss
          canvas.addEventListener('webglcontextlost', function(event) {
            alert('WebGL context lost. You will need to reload the page.');
          }, false);
        }
        if (setInModule) {
          GLctx = Module.ctx = ctx;
          Module.useWebGL = useWebGL;
          Browser.moduleContextCreatedCallbacks.forEach(function(callback) { callback() });
          Browser.init();
        }
        return ctx;
      },destroyContext:function (canvas, useWebGL, setInModule) {},fullScreenHandlersInstalled:false,lockPointer:undefined,resizeCanvas:undefined,requestFullScreen:function (lockPointer, resizeCanvas) {
        Browser.lockPointer = lockPointer;
        Browser.resizeCanvas = resizeCanvas;
        if (typeof Browser.lockPointer === 'undefined') Browser.lockPointer = true;
        if (typeof Browser.resizeCanvas === 'undefined') Browser.resizeCanvas = false;
  
        var canvas = Module['canvas'];
        function fullScreenChange() {
          Browser.isFullScreen = false;
          var canvasContainer = canvas.parentNode;
          if ((document['webkitFullScreenElement'] || document['webkitFullscreenElement'] ||
               document['mozFullScreenElement'] || document['mozFullscreenElement'] ||
               document['fullScreenElement'] || document['fullscreenElement'] ||
               document['msFullScreenElement'] || document['msFullscreenElement'] ||
               document['webkitCurrentFullScreenElement']) === canvasContainer) {
            canvas.cancelFullScreen = document['cancelFullScreen'] ||
                                      document['mozCancelFullScreen'] ||
                                      document['webkitCancelFullScreen'] ||
                                      document['msExitFullscreen'] ||
                                      document['exitFullscreen'] ||
                                      function() {};
            canvas.cancelFullScreen = canvas.cancelFullScreen.bind(document);
            if (Browser.lockPointer) canvas.requestPointerLock();
            Browser.isFullScreen = true;
            if (Browser.resizeCanvas) Browser.setFullScreenCanvasSize();
          } else {
            
            // remove the full screen specific parent of the canvas again to restore the HTML structure from before going full screen
            canvasContainer.parentNode.insertBefore(canvas, canvasContainer);
            canvasContainer.parentNode.removeChild(canvasContainer);
            
            if (Browser.resizeCanvas) Browser.setWindowedCanvasSize();
          }
          if (Module['onFullScreen']) Module['onFullScreen'](Browser.isFullScreen);
          Browser.updateCanvasDimensions(canvas);
        }
  
        if (!Browser.fullScreenHandlersInstalled) {
          Browser.fullScreenHandlersInstalled = true;
          document.addEventListener('fullscreenchange', fullScreenChange, false);
          document.addEventListener('mozfullscreenchange', fullScreenChange, false);
          document.addEventListener('webkitfullscreenchange', fullScreenChange, false);
          document.addEventListener('MSFullscreenChange', fullScreenChange, false);
        }
  
        // create a new parent to ensure the canvas has no siblings. this allows browsers to optimize full screen performance when its parent is the full screen root
        var canvasContainer = document.createElement("div");
        canvas.parentNode.insertBefore(canvasContainer, canvas);
        canvasContainer.appendChild(canvas);
        
        // use parent of canvas as full screen root to allow aspect ratio correction (Firefox stretches the root to screen size)
        canvasContainer.requestFullScreen = canvasContainer['requestFullScreen'] ||
                                            canvasContainer['mozRequestFullScreen'] ||
                                            canvasContainer['msRequestFullscreen'] ||
                                           (canvasContainer['webkitRequestFullScreen'] ? function() { canvasContainer['webkitRequestFullScreen'](Element['ALLOW_KEYBOARD_INPUT']) } : null);
        canvasContainer.requestFullScreen();
      },requestAnimationFrame:function requestAnimationFrame(func) {
        if (typeof window === 'undefined') { // Provide fallback to setTimeout if window is undefined (e.g. in Node.js)
          setTimeout(func, 1000/60);
        } else {
          if (!window.requestAnimationFrame) {
            window.requestAnimationFrame = window['requestAnimationFrame'] ||
                                           window['mozRequestAnimationFrame'] ||
                                           window['webkitRequestAnimationFrame'] ||
                                           window['msRequestAnimationFrame'] ||
                                           window['oRequestAnimationFrame'] ||
                                           window['setTimeout'];
          }
          window.requestAnimationFrame(func);
        }
      },safeCallback:function (func) {
        return function() {
          if (!ABORT) return func.apply(null, arguments);
        };
      },safeRequestAnimationFrame:function (func) {
        return Browser.requestAnimationFrame(function() {
          if (!ABORT) func();
        });
      },safeSetTimeout:function (func, timeout) {
        Module['noExitRuntime'] = true;
        return setTimeout(function() {
          if (!ABORT) func();
        }, timeout);
      },safeSetInterval:function (func, timeout) {
        Module['noExitRuntime'] = true;
        return setInterval(function() {
          if (!ABORT) func();
        }, timeout);
      },getMimetype:function (name) {
        return {
          'jpg': 'image/jpeg',
          'jpeg': 'image/jpeg',
          'png': 'image/png',
          'bmp': 'image/bmp',
          'ogg': 'audio/ogg',
          'wav': 'audio/wav',
          'mp3': 'audio/mpeg'
        }[name.substr(name.lastIndexOf('.')+1)];
      },getUserMedia:function (func) {
        if(!window.getUserMedia) {
          window.getUserMedia = navigator['getUserMedia'] ||
                                navigator['mozGetUserMedia'];
        }
        window.getUserMedia(func);
      },getMovementX:function (event) {
        return event['movementX'] ||
               event['mozMovementX'] ||
               event['webkitMovementX'] ||
               0;
      },getMovementY:function (event) {
        return event['movementY'] ||
               event['mozMovementY'] ||
               event['webkitMovementY'] ||
               0;
      },getMouseWheelDelta:function (event) {
        return Math.max(-1, Math.min(1, event.type === 'DOMMouseScroll' ? event.detail : -event.wheelDelta));
      },mouseX:0,mouseY:0,mouseMovementX:0,mouseMovementY:0,touches:{},lastTouches:{},calculateMouseEvent:function (event) { // event should be mousemove, mousedown or mouseup
        if (Browser.pointerLock) {
          // When the pointer is locked, calculate the coordinates
          // based on the movement of the mouse.
          // Workaround for Firefox bug 764498
          if (event.type != 'mousemove' &&
              ('mozMovementX' in event)) {
            Browser.mouseMovementX = Browser.mouseMovementY = 0;
          } else {
            Browser.mouseMovementX = Browser.getMovementX(event);
            Browser.mouseMovementY = Browser.getMovementY(event);
          }
          
          // check if SDL is available
          if (typeof SDL != "undefined") {
          	Browser.mouseX = SDL.mouseX + Browser.mouseMovementX;
          	Browser.mouseY = SDL.mouseY + Browser.mouseMovementY;
          } else {
          	// just add the mouse delta to the current absolut mouse position
          	// FIXME: ideally this should be clamped against the canvas size and zero
          	Browser.mouseX += Browser.mouseMovementX;
          	Browser.mouseY += Browser.mouseMovementY;
          }        
        } else {
          // Otherwise, calculate the movement based on the changes
          // in the coordinates.
          var rect = Module["canvas"].getBoundingClientRect();
          var cw = Module["canvas"].width;
          var ch = Module["canvas"].height;
  
          // Neither .scrollX or .pageXOffset are defined in a spec, but
          // we prefer .scrollX because it is currently in a spec draft.
          // (see: http://www.w3.org/TR/2013/WD-cssom-view-20131217/)
          var scrollX = ((typeof window.scrollX !== 'undefined') ? window.scrollX : window.pageXOffset);
          var scrollY = ((typeof window.scrollY !== 'undefined') ? window.scrollY : window.pageYOffset);
  
          if (event.type === 'touchstart' || event.type === 'touchend' || event.type === 'touchmove') {
            var touch = event.touch;
            if (touch === undefined) {
              return; // the "touch" property is only defined in SDL
  
            }
            var adjustedX = touch.pageX - (scrollX + rect.left);
            var adjustedY = touch.pageY - (scrollY + rect.top);
  
            adjustedX = adjustedX * (cw / rect.width);
            adjustedY = adjustedY * (ch / rect.height);
  
            var coords = { x: adjustedX, y: adjustedY };
            
            if (event.type === 'touchstart') {
              Browser.lastTouches[touch.identifier] = coords;
              Browser.touches[touch.identifier] = coords;
            } else if (event.type === 'touchend' || event.type === 'touchmove') {
              Browser.lastTouches[touch.identifier] = Browser.touches[touch.identifier];
              Browser.touches[touch.identifier] = { x: adjustedX, y: adjustedY };
            } 
            return;
          }
  
          var x = event.pageX - (scrollX + rect.left);
          var y = event.pageY - (scrollY + rect.top);
  
          // the canvas might be CSS-scaled compared to its backbuffer;
          // SDL-using content will want mouse coordinates in terms
          // of backbuffer units.
          x = x * (cw / rect.width);
          y = y * (ch / rect.height);
  
          Browser.mouseMovementX = x - Browser.mouseX;
          Browser.mouseMovementY = y - Browser.mouseY;
          Browser.mouseX = x;
          Browser.mouseY = y;
        }
      },xhrLoad:function (url, onload, onerror) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.responseType = 'arraybuffer';
        xhr.onload = function xhr_onload() {
          if (xhr.status == 200 || (xhr.status == 0 && xhr.response)) { // file URLs can return 0
            onload(xhr.response);
          } else {
            onerror();
          }
        };
        xhr.onerror = onerror;
        xhr.send(null);
      },asyncLoad:function (url, onload, onerror, noRunDep) {
        Browser.xhrLoad(url, function(arrayBuffer) {
          assert(arrayBuffer, 'Loading data file "' + url + '" failed (no arrayBuffer).');
          onload(new Uint8Array(arrayBuffer));
          if (!noRunDep) removeRunDependency('al ' + url);
        }, function(event) {
          if (onerror) {
            onerror();
          } else {
            throw 'Loading data file "' + url + '" failed.';
          }
        });
        if (!noRunDep) addRunDependency('al ' + url);
      },resizeListeners:[],updateResizeListeners:function () {
        var canvas = Module['canvas'];
        Browser.resizeListeners.forEach(function(listener) {
          listener(canvas.width, canvas.height);
        });
      },setCanvasSize:function (width, height, noUpdates) {
        var canvas = Module['canvas'];
        Browser.updateCanvasDimensions(canvas, width, height);
        if (!noUpdates) Browser.updateResizeListeners();
      },windowedWidth:0,windowedHeight:0,setFullScreenCanvasSize:function () {
        // check if SDL is available   
        if (typeof SDL != "undefined") {
        	var flags = HEAPU32[((SDL.screen+Runtime.QUANTUM_SIZE*0)>>2)];
        	flags = flags | 0x00800000; // set SDL_FULLSCREEN flag
        	HEAP32[((SDL.screen+Runtime.QUANTUM_SIZE*0)>>2)]=flags
        }
        Browser.updateResizeListeners();
      },setWindowedCanvasSize:function () {
        // check if SDL is available       
        if (typeof SDL != "undefined") {
        	var flags = HEAPU32[((SDL.screen+Runtime.QUANTUM_SIZE*0)>>2)];
        	flags = flags & ~0x00800000; // clear SDL_FULLSCREEN flag
        	HEAP32[((SDL.screen+Runtime.QUANTUM_SIZE*0)>>2)]=flags
        }
        Browser.updateResizeListeners();
      },updateCanvasDimensions:function (canvas, wNative, hNative) {
        if (wNative && hNative) {
          canvas.widthNative = wNative;
          canvas.heightNative = hNative;
        } else {
          wNative = canvas.widthNative;
          hNative = canvas.heightNative;
        }
        var w = wNative;
        var h = hNative;
        if (Module['forcedAspectRatio'] && Module['forcedAspectRatio'] > 0) {
          if (w/h < Module['forcedAspectRatio']) {
            w = Math.round(h * Module['forcedAspectRatio']);
          } else {
            h = Math.round(w / Module['forcedAspectRatio']);
          }
        }
        if (((document['webkitFullScreenElement'] || document['webkitFullscreenElement'] ||
             document['mozFullScreenElement'] || document['mozFullscreenElement'] ||
             document['fullScreenElement'] || document['fullscreenElement'] ||
             document['msFullScreenElement'] || document['msFullscreenElement'] ||
             document['webkitCurrentFullScreenElement']) === canvas.parentNode) && (typeof screen != 'undefined')) {
           var factor = Math.min(screen.width / w, screen.height / h);
           w = Math.round(w * factor);
           h = Math.round(h * factor);
        }
        if (Browser.resizeCanvas) {
          if (canvas.width  != w) canvas.width  = w;
          if (canvas.height != h) canvas.height = h;
          if (typeof canvas.style != 'undefined') {
            canvas.style.removeProperty( "width");
            canvas.style.removeProperty("height");
          }
        } else {
          if (canvas.width  != wNative) canvas.width  = wNative;
          if (canvas.height != hNative) canvas.height = hNative;
          if (typeof canvas.style != 'undefined') {
            if (w != wNative || h != hNative) {
              canvas.style.setProperty( "width", w + "px", "important");
              canvas.style.setProperty("height", h + "px", "important");
            } else {
              canvas.style.removeProperty( "width");
              canvas.style.removeProperty("height");
            }
          }
        }
      }};

  function _sbrk(bytes) {
      // Implement a Linux-like 'memory area' for our 'process'.
      // Changes the size of the memory area by |bytes|; returns the
      // address of the previous top ('break') of the memory area
      // We control the "dynamic" memory - DYNAMIC_BASE to DYNAMICTOP
      var self = _sbrk;
      if (!self.called) {
        DYNAMICTOP = alignMemoryPage(DYNAMICTOP); // make sure we start out aligned
        self.called = true;
        assert(Runtime.dynamicAlloc);
        self.alloc = Runtime.dynamicAlloc;
        Runtime.dynamicAlloc = function() { abort('cannot dynamically allocate, sbrk now has control') };
      }
      var ret = DYNAMICTOP;
      if (bytes != 0) self.alloc(bytes);
      return ret;  // Previous break location.
    }

  
  function _malloc(bytes) {
      /* Over-allocate to make sure it is byte-aligned by 8.
       * This will leak memory, but this is only the dummy
       * implementation (replaced by dlmalloc normally) so
       * not an issue.
       */
      var ptr = Runtime.dynamicAlloc(bytes + 8);
      return (ptr+8) & 0xFFFFFFF8;
    }
  Module["_malloc"] = _malloc;function _tmpnam(s, dir, prefix) {
      // char *tmpnam(char *s);
      // http://pubs.opengroup.org/onlinepubs/000095399/functions/tmpnam.html
      // NOTE: The dir and prefix arguments are for internal use only.
      var folder = FS.findObject(dir || '/tmp');
      if (!folder || !folder.isFolder) {
        dir = '/tmp';
        folder = FS.findObject(dir);
        if (!folder || !folder.isFolder) return 0;
      }
      var name = prefix || 'file';
      do {
        name += String.fromCharCode(65 + Math.floor(Math.random() * 25));
      } while (name in folder.contents);
      var result = dir + '/' + name;
      if (!_tmpnam.buffer) _tmpnam.buffer = _malloc(256);
      if (!s) s = _tmpnam.buffer;
      writeAsciiToMemory(result, s);
      return s;
    }

   
  Module["_llvm_bswap_i32"] = _llvm_bswap_i32;
FS.staticInit();__ATINIT__.unshift({ func: function() { if (!Module["noFSInit"] && !FS.init.initialized) FS.init() } });__ATMAIN__.push({ func: function() { FS.ignorePermissions = false } });__ATEXIT__.push({ func: function() { FS.quit() } });Module["FS_createFolder"] = FS.createFolder;Module["FS_createPath"] = FS.createPath;Module["FS_createDataFile"] = FS.createDataFile;Module["FS_createPreloadedFile"] = FS.createPreloadedFile;Module["FS_createLazyFile"] = FS.createLazyFile;Module["FS_createLink"] = FS.createLink;Module["FS_createDevice"] = FS.createDevice;
___errno_state = Runtime.staticAlloc(4); HEAP32[((___errno_state)>>2)]=0;
__ATINIT__.unshift({ func: function() { TTY.init() } });__ATEXIT__.push({ func: function() { TTY.shutdown() } });TTY.utf8 = new Runtime.UTF8Processor();
if (ENVIRONMENT_IS_NODE) { var fs = require("fs"); NODEFS.staticInit(); }
__ATINIT__.push({ func: function() { SOCKFS.root = FS.mount(SOCKFS, {}, null); } });
_fputc.ret = allocate([0], "i8", ALLOC_STATIC);
Module["requestFullScreen"] = function Module_requestFullScreen(lockPointer, resizeCanvas) { Browser.requestFullScreen(lockPointer, resizeCanvas) };
  Module["requestAnimationFrame"] = function Module_requestAnimationFrame(func) { Browser.requestAnimationFrame(func) };
  Module["setCanvasSize"] = function Module_setCanvasSize(width, height, noUpdates) { Browser.setCanvasSize(width, height, noUpdates) };
  Module["pauseMainLoop"] = function Module_pauseMainLoop() { Browser.mainLoop.pause() };
  Module["resumeMainLoop"] = function Module_resumeMainLoop() { Browser.mainLoop.resume() };
  Module["getUserMedia"] = function Module_getUserMedia() { Browser.getUserMedia() }
STACK_BASE = STACKTOP = Runtime.alignMemory(STATICTOP);

staticSealed = true; // seal the static portion of memory

STACK_MAX = STACK_BASE + 5242880;

DYNAMIC_BASE = DYNAMICTOP = Runtime.alignMemory(STACK_MAX);

assert(DYNAMIC_BASE < TOTAL_MEMORY, "TOTAL_MEMORY not big enough for stack");

 var ctlz_i8 = allocate([8,7,6,6,5,5,5,5,4,4,4,4,4,4,4,4,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], "i8", ALLOC_DYNAMIC);
 var cttz_i8 = allocate([8,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,4,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,5,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,4,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,6,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,4,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,5,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,4,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,7,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,4,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,5,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,4,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,6,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,4,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,5,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,4,0,1,0,2,0,1,0,3,0,1,0,2,0,1,0], "i8", ALLOC_DYNAMIC);

var Math_min = Math.min;
function invoke_ii(index,a1) {
  try {
    return Module["dynCall_ii"](index,a1);
  } catch(e) {
    if (typeof e !== 'number' && e !== 'longjmp') throw e;
    asm["setThrew"](1, 0);
  }
}

function invoke_iiii(index,a1,a2,a3) {
  try {
    return Module["dynCall_iiii"](index,a1,a2,a3);
  } catch(e) {
    if (typeof e !== 'number' && e !== 'longjmp') throw e;
    asm["setThrew"](1, 0);
  }
}

function invoke_iii(index,a1,a2) {
  try {
    return Module["dynCall_iii"](index,a1,a2);
  } catch(e) {
    if (typeof e !== 'number' && e !== 'longjmp') throw e;
    asm["setThrew"](1, 0);
  }
}

function invoke_viii(index,a1,a2,a3) {
  try {
    Module["dynCall_viii"](index,a1,a2,a3);
  } catch(e) {
    if (typeof e !== 'number' && e !== 'longjmp') throw e;
    asm["setThrew"](1, 0);
  }
}

function asmPrintInt(x, y) {
  Module.print('int ' + x + ',' + y);// + ' ' + new Error().stack);
}
function asmPrintFloat(x, y) {
  Module.print('float ' + x + ',' + y);// + ' ' + new Error().stack);
}
// EMSCRIPTEN_START_ASM
var asm=(function(global,env,buffer){"use asm";var a=new global.Int8Array(buffer);var b=new global.Int16Array(buffer);var c=new global.Int32Array(buffer);var d=new global.Uint8Array(buffer);var e=new global.Uint16Array(buffer);var f=new global.Uint32Array(buffer);var g=new global.Float32Array(buffer);var h=new global.Float64Array(buffer);var i=env.STACKTOP|0;var j=env.STACK_MAX|0;var k=env.tempDoublePtr|0;var l=env.ABORT|0;var m=env.cttz_i8|0;var n=env.ctlz_i8|0;var o=env.___rand_seed|0;var p=0;var q=0;var r=0;var s=0;var t=+env.NaN,u=+env.Infinity;var v=0,w=0,x=0,y=0,z=0.0,A=0,B=0,C=0,D=0.0;var E=0;var F=0;var G=0;var H=0;var I=0;var J=0;var K=0;var L=0;var M=0;var N=0;var O=global.Math.floor;var P=global.Math.abs;var Q=global.Math.sqrt;var R=global.Math.pow;var S=global.Math.cos;var T=global.Math.sin;var U=global.Math.tan;var V=global.Math.acos;var W=global.Math.asin;var X=global.Math.atan;var Y=global.Math.atan2;var Z=global.Math.exp;var _=global.Math.log;var $=global.Math.ceil;var aa=global.Math.imul;var ba=env.abort;var ca=env.assert;var da=env.asmPrintInt;var ea=env.asmPrintFloat;var fa=env.min;var ga=env.jsCall;var ha=env.invoke_ii;var ia=env.invoke_iiii;var ja=env.invoke_iii;var ka=env.invoke_viii;var la=env._send;var ma=env._fread;var na=env._lseek;var oa=env._open;var pa=env.___assert_fail;var qa=env._write;var ra=env._fflush;var sa=env._time;var ta=env._pwrite;var ua=env.__reallyNegative;var va=env._sbrk;var wa=env._emscripten_memcpy_big;var xa=env._fileno;var ya=env._sysconf;var za=env.___setErrNo;var Aa=env._fseek;var Ba=env._pread;var Ca=env._puts;var Da=env._printf;var Ea=env._fclose;var Fa=env._feof;var Ga=env._fsync;var Ha=env._ftell;var Ia=env.___errno_location;var Ja=env._recv;var Ka=env._fputc;var La=env._mkport;var Ma=env._read;var Na=env._abort;var Oa=env._fwrite;var Pa=env._tmpnam;var Qa=env._fprintf;var Ra=env.__formatString;var Sa=env._fputs;var Ta=env._fopen;var Ua=env._close;var Va=0.0;
// EMSCRIPTEN_START_FUNCS
function _a(a){a=a|0;var b=0;b=i;i=i+a|0;i=i+7&-8;return b|0}function $a(){return i|0}function ab(a){a=a|0;i=a}function bb(a,b){a=a|0;b=b|0;if((p|0)==0){p=a;q=b}}function cb(b){b=b|0;a[k>>0]=a[b>>0];a[k+1>>0]=a[b+1>>0];a[k+2>>0]=a[b+2>>0];a[k+3>>0]=a[b+3>>0]}function db(b){b=b|0;a[k>>0]=a[b>>0];a[k+1>>0]=a[b+1>>0];a[k+2>>0]=a[b+2>>0];a[k+3>>0]=a[b+3>>0];a[k+4>>0]=a[b+4>>0];a[k+5>>0]=a[b+5>>0];a[k+6>>0]=a[b+6>>0];a[k+7>>0]=a[b+7>>0]}function eb(a){a=a|0;E=a}function fb(){return E|0}function gb(a,b,d){a=a|0;b=b|0;d=d|0;var e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0;e=i;if((d|0)<0){d=0}else{d=(d|0)>15?15:d}c[a+34148>>2]=(d|0)>0;if((b+ -1|0)>>>0>4095|(a|0)==0){m=-1;i=e;return m|0}h=b*592|0;g=ke(h)|0;f=a+34308|0;c[f>>2]=g;if((g|0)==0){c[a+34312>>2]=0;m=-1;i=e;return m|0}re(g|0,0,h|0)|0;c[a+34312>>2]=b;a=c[a+34128>>2]|0;if((a|0)==6|(a|0)==0){a=5}else{a=(a|0)==4?5:9}if((b|0)>0){l=0}else{m=0;i=e;return m|0}a:while(1){j=g+(l*592|0)|0;m=Xc(a,0)|0;h=g+(l*592|0)+16|0;c[h>>2]=m;if((m|0)==0){d=-1;b=15;break}k=Xc(5,0)|0;m=g+(l*592|0)+20|0;c[m>>2]=k;if((k|0)==0){d=-1;b=15;break}else{k=0}do{n=Xc(c[8+(k<<2)>>2]|0,0)|0;c[g+(l*592|0)+(k<<2)+24>>2]=n;k=k+1|0;if((n|0)==0){d=-1;b=15;break a}}while((k|0)<21);c[(c[h>>2]|0)+24>>2]=0;c[(c[m>>2]|0)+24>>2]=0;c[(c[g+(l*592|0)+24>>2]|0)+24>>2]=0;c[(c[g+(l*592|0)+28>>2]|0)+24>>2]=0;c[(c[g+(l*592|0)+32>>2]|0)+24>>2]=0;c[(c[g+(l*592|0)+36>>2]|0)+24>>2]=0;c[(c[g+(l*592|0)+40>>2]|0)+24>>2]=0;c[(c[g+(l*592|0)+44>>2]|0)+24>>2]=0;c[(c[g+(l*592|0)+48>>2]|0)+24>>2]=0;c[(c[g+(l*592|0)+52>>2]|0)+24>>2]=0;c[(c[g+(l*592|0)+56>>2]|0)+24>>2]=0;c[(c[g+(l*592|0)+60>>2]|0)+24>>2]=0;c[(c[g+(l*592|0)+64>>2]|0)+24>>2]=0;c[(c[g+(l*592|0)+68>>2]|0)+24>>2]=0;c[(c[g+(l*592|0)+72>>2]|0)+24>>2]=0;c[(c[g+(l*592|0)+76>>2]|0)+24>>2]=0;c[(c[g+(l*592|0)+80>>2]|0)+24>>2]=0;c[(c[g+(l*592|0)+84>>2]|0)+24>>2]=0;c[(c[g+(l*592|0)+88>>2]|0)+24>>2]=0;c[(c[g+(l*592|0)+92>>2]|0)+24>>2]=0;c[(c[g+(l*592|0)+96>>2]|0)+24>>2]=0;c[(c[g+(l*592|0)+100>>2]|0)+24>>2]=0;c[(c[g+(l*592|0)+104>>2]|0)+24>>2]=0;nb(j);ob(j);$c(j);_c(j);c[g+(l*592|0)+584>>2]=d;l=l+1|0;if((l|0)>=(b|0)){d=0;b=15;break}g=c[f>>2]|0}if((b|0)==15){i=e;return d|0}return 0}function hb(a){a=a|0;var b=0;b=i;c[(c[a+16>>2]|0)+24>>2]=0;c[(c[a+20>>2]|0)+24>>2]=0;c[(c[a+24>>2]|0)+24>>2]=0;c[(c[a+28>>2]|0)+24>>2]=0;c[(c[a+32>>2]|0)+24>>2]=0;c[(c[a+36>>2]|0)+24>>2]=0;c[(c[a+40>>2]|0)+24>>2]=0;c[(c[a+44>>2]|0)+24>>2]=0;c[(c[a+48>>2]|0)+24>>2]=0;c[(c[a+52>>2]|0)+24>>2]=0;c[(c[a+56>>2]|0)+24>>2]=0;c[(c[a+60>>2]|0)+24>>2]=0;c[(c[a+64>>2]|0)+24>>2]=0;c[(c[a+68>>2]|0)+24>>2]=0;c[(c[a+72>>2]|0)+24>>2]=0;c[(c[a+76>>2]|0)+24>>2]=0;c[(c[a+80>>2]|0)+24>>2]=0;c[(c[a+84>>2]|0)+24>>2]=0;c[(c[a+88>>2]|0)+24>>2]=0;c[(c[a+92>>2]|0)+24>>2]=0;c[(c[a+96>>2]|0)+24>>2]=0;c[(c[a+100>>2]|0)+24>>2]=0;c[(c[a+104>>2]|0)+24>>2]=0;nb(a);ob(a);$c(a);_c(a);i=b;return}function ib(b,e,f,g){b=b|0;e=e|0;f=f|0;g=g|0;var h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0;l=i;i=i+16|0;k=l;r=c[b+34292>>2]|0;s=c[b+34272>>2]|0;f=c[e>>2]|0;j=k;c[j>>2]=0;c[j+4>>2]=0;j=e+532|0;m=e+540|0;n=c[m>>2]|0;g=c[b+34128>>2]|0;h=c[b+34156>>2]|0;Td(b,f)|0;q=b+34176|0;if((c[q>>2]|0)==0){t=r+(s*240|0)+192|0;if((d[t>>0]|0)>1){y=qe()|0;t=((y|0)%(d[t>>0]|0)|0)&255}else{t=0}a[b+34116>>0]=t;t=r+(s*240|0)+193|0;if((d[t>>0]|0)>1){y=qe()|0;t=((y|0)%(d[t>>0]|0)|0)&255}else{t=0}a[b+34117>>0]=t}t=r+(s*240|0)+195|0;if((a[t>>0]|0)==0?(d[r+(s*240|0)+193>>0]|0)>1:0){a[b+34117>>0]=a[b+34116>>0]|0}do{if((c[b+104>>2]|0)==0?(p=b+116|0,(c[p>>2]|0)!=3):0){r=a[r+(s*240|0)+194>>0]|0;do{if(!(r<<24>>24==0)){s=a[b+34116>>0]|0;if(s<<24>>24==0){Pd(f,0,1);break}else{Pd(f,1,1);Pd(f,(s&255)+ -1|0,r&255);break}}}while(0);if((c[p>>2]|0)!=2?(o=a[t>>0]|0,!(o<<24>>24==0)):0){p=a[b+34117>>0]|0;if(p<<24>>24==0){Pd(f,0,1);break}else{Pd(f,1,1);Pd(f,(p&255)+ -1|0,o&255);break}}}}while(0);if((c[q>>2]|0)==0){Wa[c[b+34420>>2]&31](b)|0}Pb(b);if((g&-5|0)==0|(g|0)==6){if((h|0)>0){o=k+4|0;m=e+544|0;p=e+36|0;t=0;s=k;while(1){r=c[b+(t<<6)+32960>>2]|0;q=(r|0)>-1?r:0-r|0;u=q>>n;if((u|0)==0){Pd(f,0,1)}else{Pd(f,1,1);jb(u,c[p>>2]|0,f);c[s>>2]=(c[s>>2]|0)+1}bd(f,q,n);if((r|0)!=0){Pd(f,r>>>31,1)}t=t+1|0;if((t|0)==(h|0)){break}else{n=c[m>>2]|0;s=o}}}}else{v=c[b+32960>>2]|0;u=(v|0)>-1?v:0-v|0;r=c[b+33024>>2]|0;p=(r|0)>-1?r:0-r|0;q=c[b+33088>>2]|0;o=(q|0)>-1?q:0-q|0;w=u>>n;t=e+544|0;s=c[t>>2]|0;if((s|0)==0){n=p;s=o}else{n=p>>s;s=o>>s}y=c[m>>2]|0;z=(w|0)!=0;x=(n|0)!=0;m=(s|0)!=0;A=((x&1)<<1|(z&1)<<2|m&1)<<1;B=c[(c[e+32>>2]|0)+4>>2]|0;Pd(f,c[B+((A|1)<<2)>>2]|0,c[B+(A+2<<2)>>2]|0);if(z){jb(w,c[e+36>>2]|0,f);c[k>>2]=(c[k>>2]|0)+1}bd(f,u,y);if((v|0)!=0){Pd(f,v>>>31,1)}u=k+4|0;t=c[t>>2]|0;if(x){jb(n,c[e+40>>2]|0,f);c[u>>2]=(c[u>>2]|0)+1}bd(f,p,t);if((r|0)!=0){Pd(f,r>>>31,1)}if(m){jb(s,c[e+40>>2]|0,f);c[u>>2]=(c[u>>2]|0)+1}bd(f,o,t);if((q|0)!=0){Pd(f,q>>>31,1)}}Zc(g,h,k,j);if((c[b+34288>>2]|0)==0){i=l;return 0}if((c[b+116>>2]|0)!=3){i=l;return 0}Yc(c[e+32>>2]|0);Yc(c[e+36>>2]|0);Yc(c[e+40>>2]|0);i=l;return 0}function jb(a,b,d){a=a|0;b=b|0;d=d|0;var e=0,f=0,g=0,h=0;f=i;if((a|0)==0){pa(1104,144,60,1120)}a=a+ -1|0;if(!(a>>>0>15)){g=c[1016+(a<<2)>>2]|0;h=c[1080+(g<<2)>>2]|0;e=b+28|0;c[e>>2]=(c[e>>2]|0)+(c[(c[b+8>>2]|0)+(g<<2)>>2]|0);g=g<<1;e=c[b+4>>2]|0;Pd(d,c[e+((g|1)<<2)>>2]|0,c[e+(g+2<<2)>>2]|0);Qd(d,a,h);i=f;return}g=a>>5;a:do{if((g|0)==0){e=4}else{h=4;while(1){h=h+1|0;g=g>>1;if((h|0)>=30){break}if((g|0)==0){e=h;break a}}pa(1152,144,70,1120)}}while(0);h=b+28|0;c[h>>2]=(c[h>>2]|0)+(c[(c[b+8>>2]|0)+24>>2]|0);h=c[b+4>>2]|0;Pd(d,c[h+52>>2]|0,c[h+56>>2]|0);do{if((e|0)>18){Pd(d,15,4);if((e|0)>21){Pd(d,3,2);bd(d,e+ -22|0,3);break}else{Pd(d,e+ -19|0,2);break}}else{Pd(d,e+ -4|0,4)}}while(0);Qd(d,a,e);i=f;return}function kb(b,d,e,f){b=b|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0;j=i;i=i+3280|0;p=j+3208|0;e=j;o=j+1160|0;f=j+1096|0;w=j+1032|0;m=j+8|0;k=c[b+34128>>2]|0;l=c[b+34156>>2]|0;v=(k|0)==1;n=(k|0)==2;z=k+ -1|0;x=z>>>0<2;B=x?1:l;h=c[d+4>>2]|0;u=d+108|0;g=d+512|0;y=d+520|0;C=c[y>>2]|0;J=e;c[J>>2]=0;c[J+4>>2]=0;if((l|0)>16){J=-1;i=j;return J|0}do{if((c[b+104>>2]|0)!=0?(A=a[(c[b+34292>>2]|0)+((c[b+34272>>2]|0)*240|0)+194>>0]|0,!(A<<24>>24==0)):0){D=a[b+34116>>0]|0;if(D<<24>>24==0){Pd(h,0,1);break}else{Pd(h,1,1);Pd(h,(D&255)+ -1|0,A&255);break}}}while(0);A=(l|0)>0;if(A){D=0;do{c[w+(D<<2)>>2]=b+(D<<6)+32960;D=D+1|0}while((D|0)!=(l|0))}if((c[b+34284>>2]|0)!=0){c[u>>2]=32767;c[d+116>>2]=32;c[d+124>>2]=30;c[d+132>>2]=28;c[d+140>>2]=26;c[d+148>>2]=24;c[d+156>>2]=22;c[d+164>>2]=20;c[d+172>>2]=18;c[d+180>>2]=16;c[d+188>>2]=14;c[d+196>>2]=12;c[d+204>>2]=10;c[d+212>>2]=8;c[d+220>>2]=6;c[d+228>>2]=4}if((B|0)>0){E=d+524|0;D=0;while(1){c[f+(D<<2)>>2]=lb(c[w+(D<<2)>>2]|0,m+(D<<6)|0,u,C,0,o+(D<<7)|0)|0;D=D+1|0;if((D|0)==(B|0)){break}else{C=c[E>>2]|0}}}if(x){C=v&1;D=v?6:14;B=c[d+524>>2]|0;E=0;F=0;u=0;do{I=c[96+((u>>1)+C<<2)>>2]|0;J=u&1;H=c[(c[w+(J+1<<2)>>2]|0)+(I<<2)>>2]|0;G=((H|0)>-1?H:0-H|0)>>B;c[p+(J<<5)+(I<<2)>>2]=G;if((G|0)==0){F=F+1|0}else{I=E<<1;c[o+(I<<2)+128>>2]=F;c[o+((I|1)<<2)+128>>2]=(H|0)<0?0-G|0:G;E=E+1|0;F=0}u=u+1|0}while((u|0)<(D|0));c[f+4>>2]=E}u=x?2:l;if(!(z>>>0<3)){if(A){z=0;do{Pd(h,(c[f+(z<<2)>>2]|0)>0|0,1);z=z+1|0}while((z|0)!=(l|0))}}else{C=u<<2;F=C+ -5|0;A=d+556|0;D=c[A>>2]|0;z=d+552|0;B=c[z>>2]|0;E=((c[f+4>>2]|0)>0)<<1|(c[f>>2]|0)>0;if((u|0)==3){E=((c[f+8>>2]|0)>0)<<2|E}do{if((B|0)<1|(D|0)<0){G=(D|0)<(B|0)?F-E|0:E;if((G|0)==0){Pd(h,0,1);break}else if((G|0)==1){Pd(h,u+1&6,u);break}else{Pd(h,C+ -4+G|0,u+1|0);break}}else{Pd(h,E,u)}}while(0);C=1-(((E|0)==(F|0))<<2)+D|0;B=1-(((E|0)==0)<<2)+B|0;if((C|0)<-8){C=-8}else{C=(C|0)>7?7:C}c[A>>2]=C;if((B|0)<-8){A=-8}else{A=(B|0)>7?7:B}c[z>>2]=A}E=c[y>>2]|0;if((E|0)<=14?(c[d+524>>2]|0)<=14:0){A=18}else{A=19}a:do{if((u|0)>0){z=e+4|0;y=d+524|0;x=x^1;B=v?4:8;C=c[w+4>>2]|0;w=c[w+8>>2]|0;D=d+24|0;if(v){s=0;t=E;v=e;while(1){E=c[f+(s<<2)>>2]|0;if((E|0)!=0){c[v>>2]=(c[v>>2]|0)+E;mb((s|0)>0|0,o+(s<<7)|0,E,D,5,h,((s|0)==1?10:1)+(n&(s|0)==1&1)|0)}b:do{if((t|0)!=0){if((s|0)==0|x){v=1;while(1){J=c[m+(s<<6)+(v<<2)>>2]|0;Pd(h,J>>1,(J&1)+t|0);v=v+1|0;if((v|0)==16){break b}}}else{v=1}do{F=C+(v<<2)|0;E=c[F>>2]|0;Za[A&31](h,(E|0)>-1?E:0-E|0,t);if((c[p+(v<<2)>>2]|0)==0?(q=c[F>>2]|0,(q|0)!=0):0){Pd(h,q>>>31,1)}E=w+(v<<2)|0;F=c[E>>2]|0;Za[A&31](h,(F|0)>-1?F:0-F|0,t);if((c[p+(v<<2)+32>>2]|0)==0?(r=c[E>>2]|0,(r|0)!=0):0){Pd(h,r>>>31,1)}v=v+1|0}while((v|0)<(B|0))}}while(0);s=s+1|0;if((s|0)==(u|0)){break a}t=c[y>>2]|0;v=z}}else{q=0;r=E;E=e;while(1){v=c[f+(q<<2)>>2]|0;if((v|0)!=0){c[E>>2]=(c[E>>2]|0)+v;mb((q|0)>0|0,o+(q<<7)|0,v,D,5,h,n&(q|0)==1?2:1)}c:do{if((r|0)!=0){if((q|0)==0|x){v=1;while(1){J=c[m+(q<<6)+(v<<2)>>2]|0;Pd(h,J>>1,(J&1)+r|0);v=v+1|0;if((v|0)==16){break c}}}else{v=1}do{F=C+(v<<2)|0;E=c[F>>2]|0;Za[A&31](h,(E|0)>-1?E:0-E|0,r);if((c[p+(v<<2)>>2]|0)==0?(s=c[F>>2]|0,(s|0)!=0):0){Pd(h,s>>>31,1)}E=w+(v<<2)|0;F=c[E>>2]|0;Za[A&31](h,(F|0)>-1?F:0-F|0,r);if((c[p+(v<<2)+32>>2]|0)==0?(t=c[E>>2]|0,(t|0)!=0):0){Pd(h,t>>>31,1)}v=v+1|0}while((v|0)<(B|0))}}while(0);q=q+1|0;if((q|0)==(u|0)){break a}r=c[y>>2]|0;E=z}}}}while(0);Td(b,h)|0;Zc(k,l,e,g);if((c[b+34288>>2]|0)==0){J=0;i=j;return J|0}nb(d);J=0;i=j;return J|0}function lb(a,b,d,e,f,g){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;var h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0;h=i;if((e|0)==0){e=c[a+(c[d+12>>2]<<2)>>2]|0;if((e|0)==0){m=0;b=1;e=2}else{m=d+8|0;c[m>>2]=(c[m>>2]|0)+1;c[g>>2]=0;c[g+4>>2]=e;m=1;b=0;e=2}do{j=d+(e<<3)|0;k=c[a+(c[d+(e<<3)+4>>2]<<2)>>2]|0;if((k|0)==0){b=b+1|0}else{r=(c[j>>2]|0)+1|0;c[j>>2]=r;f=d+(e+ -1<<3)|0;if(r>>>0>(c[f>>2]|0)>>>0){q=j;p=c[q>>2]|0;q=c[q+4>>2]|0;n=f;o=c[n+4>>2]|0;r=j;c[r>>2]=c[n>>2];c[r+4>>2]=o;r=f;c[r>>2]=p;c[r+4>>2]=q}r=m<<1;c[g+(r<<2)>>2]=b;c[g+((r|1)<<2)>>2]=k;m=m+1|0;b=0}e=e+1|0}while((e|0)!=16);i=h;return m|0}if((e|0)<=(f|0)){b=(1<<e)+ -1|0;j=b<<1|1;f=c[a+(c[d+12>>2]<<2)>>2]|0;if((f+b|0)>>>0<j>>>0){m=0;f=1;k=2}else{k=((f|0)>-1?f:0-f|0)>>e;r=d+8|0;c[r>>2]=(c[r>>2]|0)+1;c[g>>2]=0;c[g+4>>2]=(f|0)<0?0-k|0:k;m=1;f=0;k=2}do{o=d+(k<<3)|0;l=c[a+(c[d+(k<<3)+4>>2]<<2)>>2]|0;if((l+b|0)>>>0<j>>>0){f=f+1|0}else{n=((l|0)>-1?l:0-l|0)>>e;r=(c[o>>2]|0)+1|0;c[o>>2]=r;p=d+(k+ -1<<3)|0;if(r>>>0>(c[p>>2]|0)>>>0){q=o;s=c[q>>2]|0;q=c[q+4>>2]|0;u=p;t=c[u+4>>2]|0;r=o;c[r>>2]=c[u>>2];c[r+4>>2]=t;r=p;c[r>>2]=s;c[r+4>>2]=q}o=m<<1;c[g+(o<<2)>>2]=f;c[g+((o|1)<<2)>>2]=(l|0)<0?0-n|0:n;m=m+1|0;f=0}k=k+1|0}while((k|0)!=16);i=h;return m|0}k=(1<<e)+ -1|0;j=k<<1|1;l=c[d+12>>2]|0;m=c[a+(l<<2)>>2]|0;n=(m+k|0)>>>0<j>>>0;if((f|0)==0&(e|0)<6){if(n){c[b+(l<<2)>>2]=c[752+(m+32<<2)>>2];m=0;f=1;l=2}else{u=(m|0)>-1?m:0-m|0;f=u>>e;c[b+(l<<2)>>2]=(u&k)<<1;u=d+8|0;c[u>>2]=(c[u>>2]|0)+1;c[g>>2]=0;c[g+4>>2]=(m|0)<0?0-f|0:f;m=1;f=0;l=2}do{n=d+(l<<3)|0;p=c[d+(l<<3)+4>>2]|0;q=c[a+(p<<2)>>2]|0;if((q+k|0)>>>0<j>>>0){c[b+(p<<2)>>2]=c[752+(q+32<<2)>>2];f=f+1|0}else{o=q>>31;q=(o^q)-o|0;c[b+(p<<2)>>2]=(q&k)<<1;u=(c[n>>2]|0)+1|0;c[n>>2]=u;p=d+(l+ -1<<3)|0;if(u>>>0>(c[p>>2]|0)>>>0){t=n;s=c[t>>2]|0;t=c[t+4>>2]|0;v=p;r=c[v+4>>2]|0;u=n;c[u>>2]=c[v>>2];c[u+4>>2]=r;u=p;c[u>>2]=s;c[u+4>>2]=t}v=m<<1;c[g+(v<<2)>>2]=f;c[g+((v|1)<<2)>>2]=(q>>e^o)-o;m=m+1|0;f=0}l=l+1|0}while((l|0)!=16);i=h;return m|0}else{if(n){n=m>>31;m=n+m>>f;u=m-n|0;v=u>>31;c[b+(l<<2)>>2]=((v^u)<<2)+(v&6)|(m|0)!=(n|0);m=0;l=1;n=2}else{v=(m|0)>-1?m:0-m|0;n=v>>e;c[b+(l<<2)>>2]=(v&k)>>>f<<1;v=d+8|0;c[v>>2]=(c[v>>2]|0)+1;c[g>>2]=0;c[g+4>>2]=(m|0)<0?0-n|0:n;m=1;l=0;n=2}do{q=d+(n<<3)|0;r=c[d+(n<<3)+4>>2]|0;p=c[a+(r<<2)>>2]|0;if((p+k|0)>>>0<j>>>0){v=p>>31;u=v+p>>f;s=u-v|0;t=s>>31;c[b+(r<<2)>>2]=((t^s)<<2)+(t&6)|(u|0)!=(v|0);l=l+1|0}else{v=(p|0)>-1?p:0-p|0;o=v>>e;c[b+(r<<2)>>2]=(v&k)>>>f<<1;v=(c[q>>2]|0)+1|0;c[q>>2]=v;r=d+(n+ -1<<3)|0;if(v>>>0>(c[r>>2]|0)>>>0){u=q;t=c[u>>2]|0;u=c[u+4>>2]|0;w=r;s=c[w+4>>2]|0;v=q;c[v>>2]=c[w>>2];c[v+4>>2]=s;v=r;c[v>>2]=t;c[v+4>>2]=u}q=m<<1;c[g+(q<<2)>>2]=l;c[g+((q|1)<<2)>>2]=(p|0)<0?0-o|0:o;m=m+1|0;l=0}n=n+1|0}while((n|0)!=16);i=h;return m|0}return 0}function mb(a,b,d,e,f,g,h){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0;j=i;k=c[b+4>>2]|0;o=(c[b>>2]|0)==0;l=o&1;n=(k+1|0)>>>0>2;if((d|0)==1){m=0}else{m=(c[b+8>>2]|0)>0?2:1}r=(n&1)<<1|l|m<<2;a=a*3|0;q=c[e+(a+f<<2)>>2]|0;p=q+28|0;c[p>>2]=(c[p>>2]|0)+(c[(c[q+8>>2]|0)+(r<<2)>>2]|0);p=q+32|0;c[p>>2]=(c[p>>2]|0)+(c[(c[q+12>>2]|0)+(r<<2)>>2]|0);r=r<<1;q=c[q+4>>2]|0;Pd(g,c[q+((r|1)<<2)>>2]<<1|k>>>31,(c[q+(r+2<<2)>>2]|0)+1|0);l=m&l;if(n){jb(((k|0)>-1?k:0-k|0)+ -1|0,c[e+(f+6+l<<2)>>2]|0,g)}if(!o){qb(c[b>>2]|0,15-h|0,c[e>>2]|0,g)}if((d|0)<=1){i=j;return}k=d+ -1|0;a=a+1+f|0;f=f+6|0;h=h+1+(c[b>>2]|0)|0;n=1;do{o=n<<1;p=b+(o<<2)|0;if((m|0)==2){qb(c[p>>2]|0,15-h|0,c[e>>2]|0,g)}h=h+1+(c[p>>2]|0)|0;if((n|0)==(k|0)){m=0}else{m=(c[b+(o+2<<2)>>2]|0)>0?2:1}r=c[b+((o|1)<<2)>>2]|0;q=(r+1|0)>>>0>2;p=q&1|m<<1;o=r>>>31;do{if((h|0)>=15){if((h|0)==15){Pd(g,c[568+(p<<2)>>2]<<1|o,(c[584+(p<<2)>>2]|0)+1|0);break}else{Pd(g,p<<1|o,2);break}}else{s=c[e+(a+l<<2)>>2]|0;t=s+28|0;c[t>>2]=(c[t>>2]|0)+(c[(c[s+8>>2]|0)+(p<<2)>>2]|0);t=s+32|0;c[t>>2]=(c[t>>2]|0)+(c[(c[s+12>>2]|0)+(p<<2)>>2]|0);p=p<<1;s=c[s+4>>2]|0;Pd(g,c[s+((p|1)<<2)>>2]<<1|o,(c[s+(p+2<<2)>>2]|0)+1|0)}}while(0);l=m&l;if(q){jb(((r|0)>-1?r:0-r|0)+ -1|0,c[e+(f+l<<2)>>2]|0,g)}n=n+1|0}while((n|0)!=(d|0));i=j;return}function nb(a){a=a|0;var b=0;b=i;Yc(c[a+24>>2]|0);Yc(c[a+28>>2]|0);Yc(c[a+32>>2]|0);Yc(c[a+36>>2]|0);Yc(c[a+40>>2]|0);Yc(c[a+44>>2]|0);Yc(c[a+48>>2]|0);Yc(c[a+52>>2]|0);Yc(c[a+56>>2]|0);Yc(c[a+60>>2]|0);Yc(c[a+64>>2]|0);Yc(c[a+68>>2]|0);Yc(c[a+72>>2]|0);i=b;return}function ob(a){a=a|0;var b=0;b=i;Yc(c[a+16>>2]|0);Yc(c[a+20>>2]|0);Yc(c[a+76>>2]|0);Yc(c[a+80>>2]|0);Yc(c[a+84>>2]|0);Yc(c[a+88>>2]|0);Yc(c[a+92>>2]|0);Yc(c[a+96>>2]|0);Yc(c[a+100>>2]|0);Yc(c[a+104>>2]|0);i=b;return}function pb(b,d,e,f){b=b|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0,Y=0;l=i;i=i+208|0;k=l;h=l+72|0;e=l+8|0;j=c[d+8>>2]|0;f=c[d+12>>2]|0;do{if((c[b+104>>2]|0)!=0?(m=a[(c[b+34292>>2]|0)+((c[b+34272>>2]|0)*240|0)+195>>0]|0,!(m<<24>>24==0)):0){n=a[b+34117>>0]|0;if(n<<24>>24==0){Pd(j,0,1);break}else{Pd(j,1,1);Pd(j,(n&255)+ -1|0,m&255);break}}}while(0);if((c[b+34284>>2]|0)!=0){c[d+364>>2]=32767;c[d+236>>2]=32767;c[d+372>>2]=32;c[d+244>>2]=32;c[d+380>>2]=30;c[d+252>>2]=30;c[d+388>>2]=28;c[d+260>>2]=28;c[d+396>>2]=26;c[d+268>>2]=26;c[d+404>>2]=24;c[d+276>>2]=24;c[d+412>>2]=22;c[d+284>>2]=22;c[d+420>>2]=20;c[d+292>>2]=20;c[d+428>>2]=18;c[d+300>>2]=18;c[d+436>>2]=16;c[d+308>>2]=16;c[d+444>>2]=14;c[d+316>>2]=14;c[d+452>>2]=12;c[d+324>>2]=12;c[d+460>>2]=10;c[d+332>>2]=10;c[d+468>>2]=8;c[d+340>>2]=8;c[d+476>>2]=6;c[d+348>>2]=6;c[d+484>>2]=4;c[d+356>>2]=4}q=b+34128|0;s=c[q>>2]|0;if((s&-3|0)==4){v=c[b+34156>>2]|0}else{v=1}Qb(b,d);Td(b,j)|0;if((v|0)>0){n=(s|0)==1;p=(s|0)==2;r=d+20|0;o=d+16|0;t=d+28|0;u=0;A=0;C=0;y=c[b+34056>>2]|0;x=c[b+34060>>2]|0;do{z=c[b+(u<<2)+34052>>2]|0;do{if(!n){if((s|0)==3){m=1;B=y|x|z;break}else if((s|0)==2){z=(x<<17&8388608|x<<18&4194304|x<<25&1073741824|x<<24&-2147483648|z<<8&983040|z<<12&251658240)+(x<<12&32768|x<<13&16384|y<<10&8192|y<<16&1048576|y<<15&2097152|y<<23&268435456|y<<22&536870912)+(x<<5&128|x<<6&64|y<<4&16|y<<3&32|y<<11&4096|z&15|z<<4&3840)|0;m=0;B=z;break}else{m=0;B=z;break}}else{z=(x<<10&2048|x<<5&32|y<<4&16|y<<9&1024|z&15|z<<2&960|z<<4&61440)+(x<<20&8388608|x<<15&131072|y<<14&65536|y<<19&4194304|z<<6&3932160)|0;m=0;B=z}}while(0);do{if(!p){if(n){w=(B&16515072|0)!=0|0;B=((B&4032|0)!=0)<<1|(B&63|0)!=0|((B&258048|0)!=0)<<2;break}else{w=(B&61440|0)!=0|0;B=((B&240|0)!=0)<<1|(B&15|0)!=0|((B&3840|0)!=0)<<2;break}}else{w=B>>>0>16777215|0;B=((B&65280|0)!=0)<<1|(B&255|0)!=0|((B&16711680|0)!=0)<<2}}while(0);w=w<<3|B;W=c[r>>2]|0;X=c[184+(w<<2)>>2]|0;Y=X<<1;V=c[W+4>>2]|0;Pd(j,c[V+((Y|1)<<2)>>2]|0,c[V+(Y+2<<2)>>2]|0);Y=W+28|0;c[Y>>2]=(c[Y>>2]|0)+(c[(c[W+8>>2]|0)+(X<<2)>>2]|0);if((w|0)==0|(w|0)==15){w=0}else{Pd(j,c[312+(w<<2)>>2]|0,c[248+(w<<2)>>2]|0);w=0}do{if((s|0)==1){B=z&63;D=A;z=z>>6}else if((s|0)==2){B=z&255;D=A;z=z>>8}else if((s|0)==3){D=y&15;C=x&15;B=((C|0)!=0)<<5|z&15|((D|0)!=0)<<4;y=y>>4;x=x>>4;z=z>>4}else{B=z&15;D=A;z=z>>4}do{if((B|0)!=0){E=B>>4;A=B&15;if(p){D=E&3;C=B>>>6&3;B=(D|0)!=0|0;B=(C|0)==0?B:B|2}else{B=E}G=(B|0)!=0;F=c[376+(A<<2)>>2]|0;if(G){E=(61152>>>A&1|0)==0?F+5|0:8}else{E=F+ -1|0}X=c[o>>2]|0;Y=E<<1;W=c[X+4>>2]|0;Pd(j,c[W+((Y|1)<<2)>>2]|0,c[W+(Y+2<<2)>>2]|0);Y=X+28|0;c[Y>>2]=(c[Y>>2]|0)+(c[(c[X+8>>2]|0)+(E<<2)>>2]|0);do{if(G){if((B|0)==1){Pd(j,1,1);break}else{Pd(j,3-B|0,2);break}}}while(0);do{if((E|0)==8){if((1632>>>A&1|0)==0){Pd(j,5-F|0,2);break}else{Pd(j,1,1);break}}}while(0);if(!((A|0)==0|(A|0)==15)){Pd(j,c[504+(A<<2)>>2]|0,c[440+(A<<2)>>2]|0)}if(m){A=(c[t>>2]|0)+4|0;if((D|0)!=0?(Y=c[184+(D<<2)>>2]<<1,X=c[A>>2]|0,Pd(j,c[X+(Y+ -1<<2)>>2]|0,c[X+(Y<<2)>>2]|0),(D|0)!=15):0){Pd(j,c[312+(D<<2)>>2]|0,c[248+(D<<2)>>2]|0)}if((C|0)==0){A=D;C=0;break}Y=c[184+(C<<2)>>2]<<1;X=c[A>>2]|0;Pd(j,c[X+(Y+ -1<<2)>>2]|0,c[X+(Y<<2)>>2]|0);if((C|0)==15){A=D;C=15;break}Pd(j,c[312+(C<<2)>>2]|0,c[248+(C<<2)>>2]|0);A=D;break}if(p){if((D|0)==1){Pd(j,1,1)}else if((D|0)!=0){Pd(j,3-D|0,2)}if((C|0)==1){Pd(j,1,1);A=D;C=1;break}else if((C|0)==0){A=D;C=0;break}else{Pd(j,3-C|0,2);A=D;break}}else{A=D}}else{A=D}}while(0);w=w+1|0}while((w|0)!=4);u=u+1|0}while((u|0)!=(v|0))}t=c[q>>2]|0;s=c[b+34156>>2]|0;q=(t|0)==1;r=(t|0)==2;m=(t+ -1|0)>>>0<2?1:s;n=d+492|0;I=c[d+500>>2]|0;Y=k;c[Y>>2]=0;c[Y+4>>2]=0;if((I|0)>=16){pa(128,144,862,168)}p=d+584|0;K=c[p>>2]|0;o=b+116|0;u=(c[o>>2]|0)==1;if((K|0)>(I|0)){if(u){F=0;G=0;K=0}else{F=0;G=0;K=0;g=67}}else{if(u){F=0;G=0;K=0}else{G=I-K|0;F=G;G=(1<<G)+ -1|0;g=67}}if((g|0)==67){Td(b,f)|0}A=(c[b+33984>>2]|0)==1?d+364|0:d+236|0;a:do{if((m|0)>0){y=b+33992|0;v=b+33996|0;x=d+24|0;z=d+504|0;w=k+4|0;E=0;u=0;B=4;L=k;b:while(1){J=c[b+(u<<2)+33988>>2]|0;do{if(!q){if(!r){if((B|0)>0){g=75;break}else{break}}else{B=8;J=(c[y>>2]<<16)+J+(c[v>>2]<<24)|0;g=75;break}}else{B=6;J=(c[y>>2]<<16)+J+(c[v>>2]<<20)|0;g=75}}while(0);if((g|0)==75){g=0;C=b+(u<<2)+34460|0;D=0;H=0;do{Q=(D|0)<4;O=(F|0)==0;N=F+1|0;M=b+(D+ -3<<2)+34460|0;S=b+((D+ -4>>1)+1<<2)+34460|0;R=D<<2&4;P=H;T=0;while(1){do{if(!Q){if(q){U=(c[M>>2]|0)+(c[8160+(T<<2)>>2]<<2)|0;break}if(r){U=(c[S>>2]|0)+(c[8176+(T+R<<2)>>2]<<2)|0}else{U=0}}else{U=(c[C>>2]|0)+(c[8096+(P<<2)>>2]<<2)|0}}while(0);if((J&1|0)==0){if(!O){W=1;do{Y=c[U+(c[8208+(W<<2)>>2]<<2)>>2]|0;V=((Y|0)>-1?Y:0-Y|0)>>K;X=V&G;if((V|0)==0){V=F}else{V=N;X=X<<1|Y>>>31}Pd(f,X,V);W=W+1|0}while((W|0)!=16)}}else{Y=lb(U,e,A,I,K,h)|0;c[L>>2]=(c[L>>2]|0)+Y;mb(E,h,Y,x,13,j,1);if(!O){U=1;do{Y=c[e+(c[8208+(U<<2)>>2]<<2)>>2]|0;Pd(f,Y>>1,(Y&1)+F|0);U=U+1|0}while((U|0)!=16)}}T=T+1|0;J=J>>1;if((T|0)==4){break}else{P=P+1|0}}H=H+4|0;if((D|0)==3){I=c[z>>2]|0;if((I|0)>=16){break b}K=c[p>>2]|0;if((K|0)<=(I|0)?(c[o>>2]|0)!=1:0){G=I-K|0;E=1;F=G;G=(1<<G)+ -1|0;L=w}else{E=1;F=0;G=0;K=0;L=w}}D=D+1|0}while((D|0)<(B|0))}u=u+1|0;if((u|0)>=(m|0)){break a}}pa(128,144,949,168)}}while(0);Zc(t,s,k,n);if((c[b+34288>>2]|0)==0){i=l;return 0}Yc(c[d+16>>2]|0);Yc(c[d+20>>2]|0);Yc(c[d+76>>2]|0);Yc(c[d+80>>2]|0);Yc(c[d+84>>2]|0);Yc(c[d+88>>2]|0);Yc(c[d+92>>2]|0);Yc(c[d+96>>2]|0);Yc(c[d+100>>2]|0);Yc(c[d+104>>2]|0);i=l;return 0}function qb(a,b,d,e){a=a|0;b=b|0;d=d|0;e=e|0;var f=0,g=0;f=i;if((b|0)>=5){b=c[7736+(b<<2)>>2]|0;g=c[600+(a+ -1+(b*14|0)<<2)>>2]|0;b=c[7800+(g+(b*5|0)<<2)>>2]|0;g=g<<1;d=c[d+4>>2]|0;Pd(e,c[d+((g|1)<<2)>>2]|0,c[d+(g+2<<2)>>2]|0);bd(e,a+1|0,b);i=f;return}if((b|0)<=1){i=f;return}Pd(e,(b|0)!=(a|0)|0,b+ -4+(c[736+(b-a<<2)>>2]|0)|0);i=f;return}function rb(a,b,e,f,g){a=a|0;b=b|0;e=e|0;f=f|0;g=g|0;var h=0;h=i;e=(e&255)>2?2:e;if(!(f>>>0>1)){bd(b,d[(c[a>>2]|0)+(g*20|0)>>0]|0,8);i=h;return}bd(b,e&255,2);bd(b,d[(c[a>>2]|0)+(g*20|0)>>0]|0,8);if(e<<24>>24==1){bd(b,d[(c[a+4>>2]|0)+(g*20|0)>>0]|0,8);i=h;return}else if(e<<24>>24==0){i=h;return}else{e=1;do{bd(b,d[(c[a+(e<<2)>>2]|0)+(g*20|0)>>0]|0,8);e=e+1|0}while((e|0)!=(f|0));i=h;return}}function sb(b,d){b=b|0;d=d|0;var e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0;e=i;f=b;b=(c[b+34928>>2]|0)==0?1:2;a:while(1){if((c[f+34180>>2]&1|0)!=0){l=f+34292|0;h=c[l>>2]|0;o=f+34272|0;j=c[o>>2]|0;g=h+(j*240|0)+204|0;a[g>>0]=(qe()|0)&3;if((c[f+34268>>2]|0)==(0-(c[o>>2]|0)|0)){n=f+132|0;k=f+34156|0;o=0;while(1){m=o+1|0;if((Fd((c[l>>2]|0)+(o*240|0)|0,c[k>>2]|0,1)|0)!=0){f=-1;d=14;break a}if(m>>>0>(c[n>>2]|0)>>>0){break}else{o=m}}}else{k=f+34156|0}if((c[k>>2]|0)==0){m=0}else{l=0;do{m=((qe()|0)&47)+1&255;a[c[h+(j*240|0)+(l<<2)>>2]>>0]=m;l=l+1|0;m=c[k>>2]|0}while(l>>>0<m>>>0)}l=h+(j*240|0)|0;Gd(l,a[g>>0]|0,m,0,1,c[f+34140>>2]|0);if((c[k>>2]|0)==0){m=0}else{n=0;do{m=c[h+(j*240|0)+(n<<2)>>2]|0;c[m+8>>2]=c[m+4>>2]>>1;n=n+1|0;m=c[k>>2]|0}while(n>>>0<m>>>0)}rb(l,d,a[g>>0]|0,m,0)}b=b+ -1|0;if((b|0)==0){f=0;d=14;break}else{f=c[f+34928>>2]|0}}if((d|0)==14){i=e;return f|0}return 0}function tb(b,e){b=b|0;e=e|0;var f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0;f=i;g=b;h=(c[b+34928>>2]|0)==0?1:2;a:while(1){do{if((c[g+116>>2]|0)!=3?(c[g+34180>>2]&2|0)!=0:0){l=c[g+34292>>2]|0;n=g+34272|0;k=c[n>>2]|0;o=(qe()|0)&1^1;p=l+(k*240|0)+196|0;c[p>>2]=o;bd(e,o,1);o=l+(k*240|0)+194|0;a[o>>0]=0;if((c[p>>2]|0)==1){q=1}else{q=((qe()|0)&15)+1&31}b=l+(k*240|0)+192|0;a[b>>0]=q;j=l+(k*240|0)+64|0;if((c[g+34268>>2]|0)!=0){Ed(j);q=a[b>>0]|0}m=g+34156|0;if((Fd(j,c[m>>2]|0,q&255)|0)!=0){g=-1;e=18;break a}if((c[p>>2]|0)==1){Id(g,c[n>>2]|0);break}bd(e,(d[b>>0]|0)+ -1|0,4);a[o>>0]=Kd(a[b>>0]|0)|0;if((a[b>>0]|0)!=0){n=g+34140|0;o=0;q=0;while(1){s=(qe()|0)&3;p=l+(k*240|0)+o+205|0;a[p>>0]=s;if((c[m>>2]|0)==0){r=0}else{s=0;t=0;do{r=(qe()|0|1)&255;a[(c[l+(k*240|0)+(s<<2)+64>>2]|0)+(o*20|0)>>0]=r;t=t+1<<24>>24;s=t&255;r=c[m>>2]|0}while(s>>>0<r>>>0);s=a[p>>0]|0}Gd(j,s,r,o,1,c[n>>2]|0);rb(j,e,a[p>>0]|0,c[m>>2]|0,o);q=q+1<<24>>24;if((q&255)<(d[b>>0]|0)){o=q&255}else{break}}}}}while(0);h=h+ -1|0;if((h|0)==0){g=0;e=18;break}else{g=c[g+34928>>2]|0}}if((e|0)==18){i=f;return g|0}return 0}function ub(b,e){b=b|0;e=e|0;var f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0;f=i;g=b;b=(c[b+34928>>2]|0)==0?1:2;a:while(1){do{if(!(((c[g+116>>2]|0)+ -2|0)>>>0<2)?(c[g+34180>>2]&4|0)!=0:0){h=c[g+34292>>2]|0;p=g+34272|0;j=c[p>>2]|0;o=(qe()|0)&1^1;n=h+(j*240|0)+200|0;c[n>>2]=o;bd(e,o,1);o=h+(j*240|0)+195|0;a[o>>0]=0;if((c[n>>2]|0)==1){q=a[h+(j*240|0)+192>>0]|0}else{q=((qe()|0)&15)+1&255}k=h+(j*240|0)+193|0;a[k>>0]=q;l=h+(j*240|0)+128|0;if((c[g+34268>>2]|0)!=0){Ed(l);q=a[k>>0]|0}m=g+34156|0;if((Fd(l,c[m>>2]|0,q&255)|0)!=0){g=-1;e=19;break a}q=d[k>>0]|0;if((c[n>>2]|0)==1){Jd(g,q,c[p>>2]|0);break}bd(e,q+ -1|0,4);a[o>>0]=Kd(a[k>>0]|0)|0;if((a[k>>0]|0)!=0){n=g+34140|0;o=0;q=0;while(1){s=(qe()|0)&3;p=h+(j*240|0)+o+221|0;a[p>>0]=s;if((c[m>>2]|0)==0){r=0}else{s=0;r=0;do{t=(qe()|0|1)&255;a[(c[h+(j*240|0)+(s<<2)+128>>2]|0)+(o*20|0)>>0]=t;r=r+1<<24>>24;s=r&255;t=c[m>>2]|0}while(s>>>0<t>>>0);r=t;s=a[p>>0]|0}Gd(l,s,r,o,0,c[n>>2]|0);rb(l,e,a[p>>0]|0,c[m>>2]|0,o);q=q+1<<24>>24;if((q&255)<(d[k>>0]|0)){o=q&255}else{break}}}}}while(0);b=b+ -1|0;if((b|0)==0){g=0;e=19;break}else{g=c[g+34928>>2]|0}}if((e|0)==19){i=f;return g|0}return 0}function vb(a,b,e){a=a|0;b=b|0;e=e|0;var f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0;f=i;i=i+16|0;h=f;k=c[a+34272>>2]|0;g=a+34308|0;n=c[g>>2]|0;j=n+(k*592|0)|0;do{if((((c[a+34276>>2]|0)!=0?(c[a+34280>>2]|0)!=0:0)?(c[a+34932>>2]|0)==0:0)?(c[a+34176>>2]|0)==0:0){m=(aa((c[a+132>>2]|0)+1|0,c[a+34268>>2]|0)|0)+k|0;p=(c[a+104>>2]|0)==0;o=c[j>>2]|0;bd(o,0,8);bd(o,0,8);bd(o,1,8);m=m<<3&248;if(p){bd(o,m,8);if((c[a+34148>>2]|0)!=0){bd(c[j>>2]|0,c[n+(k*592|0)+584>>2]|0,4)}sb(a,c[j>>2]|0)|0;tb(a,c[j>>2]|0)|0;ub(a,c[j>>2]|0)|0;break}bd(o,m|1,8);sb(a,c[j>>2]|0)|0;o=a+34236|0;if((((d[o>>0]|0)>1?(p=n+(k*592|0)+4|0,q=c[p>>2]|0,bd(q,0,8),bd(q,0,8),bd(q,1,8),bd(q,m|2,8),tb(a,c[p>>2]|0)|0,(d[o>>0]|0)>2):0)?(q=n+(k*592|0)+8|0,p=c[q>>2]|0,bd(p,0,8),bd(p,0,8),bd(p,1,8),bd(p,m|3,8),ub(a,c[q>>2]|0)|0,(d[o>>0]|0)>3):0)?(l=n+(k*592|0)+12|0,q=c[l>>2]|0,bd(q,0,8),bd(q,0,8),bd(q,1,8),bd(q,m|4,8),(c[a+34148>>2]|0)!=0):0){bd(c[l>>2]|0,c[n+(k*592|0)+584>>2]|0,4)}}}while(0);if((ib(a,j,b,e)|0)!=0){q=-1;i=f;return q|0}k=a+116|0;if((c[k>>2]|0)!=3){if((kb(a,j,b,e)|0)!=0){q=-1;i=f;return q|0}if(!(((c[k>>2]|0)+ -2|0)>>>0<2)?(pb(a,j,b,e)|0)!=0:0){q=-1;i=f;return q|0}}if((b+1|0)!=(c[a+34332>>2]|0)){q=0;i=f;return q|0}j=e+1|0;b=a+34336|0;k=c[b>>2]|0;if((j|0)!=(k|0)){l=c[a+34268>>2]|0;if(!(l>>>0<(c[a+16520>>2]|0)>>>0)){q=0;i=f;return q|0}if(((c[a+(l+1<<2)+16524>>2]|0)+ -1|0)!=(e|0)){q=0;i=f;return q|0}}if(!((c[a+34928>>2]|0)!=0?(c[a+34932>>2]|0)==0:0)){e=a+34300|0;if((c[e>>2]|0)!=0){n=a+34296|0;o=a+34920|0;m=a+34268|0;l=a+34264|0;k=0;do{Rd(c[(c[n>>2]|0)+(k<<2)>>2]|0);p=c[(c[o>>2]|0)+(k<<2)>>2]|0;Ya[c[p+40>>2]&31](p,h)|0;p=c[h>>2]|0;p=(Ud(c[(c[n>>2]|0)+(k<<2)>>2]|0)|0)+p|0;q=(aa(c[m>>2]|0,c[e>>2]|0)|0)+k|0;c[(c[l>>2]|0)+(q<<2)>>2]=p;k=k+1|0}while(k>>>0<(c[e>>2]|0)>>>0);k=c[b>>2]|0}}if((j|0)==(k|0)){q=0;i=f;return q|0}a=a+132|0;h=0;while(1){hb((c[g>>2]|0)+(h*592|0)|0);h=h+1|0;if(h>>>0>(c[a>>2]|0)>>>0){g=0;break}}i=f;return g|0}function wb(a){a=a|0;var b=0,d=0,e=0,f=0,g=0,h=0,j=0,k=0,l=0;e=i;g=a+34328|0;if((c[g>>2]|0)!=0){h=(c[a+34928>>2]|0)!=0;f=h&1;if((c[a+34324>>2]|0)!=0){if(h){g=a;h=0;while(1){Nb(g);k=g+34328|0;j=g+34324|0;dd(g,(c[k>>2]|0)+ -1|0,(c[j>>2]|0)+ -1|0);l=c[g+34928>>2]|0;c[l+34268>>2]=c[g+34268>>2];c[l+34272>>2]=c[g+34272>>2];j=vb(g,(c[k>>2]|0)+ -1|0,(c[j>>2]|0)+ -1|0)|0;if((j|0)!=0){f=13;break}j=c[g+34928>>2]|0;c[j+34324>>2]=c[g+34324>>2];c[j+34328>>2]=c[g+34328>>2];h=h+1|0;if(h>>>0>f>>>0){j=0;f=13;break}else{g=j}}if((f|0)==13){i=e;return j|0}}else{h=0;while(1){Nb(a);j=a+34324|0;dd(a,(c[g>>2]|0)+ -1|0,(c[j>>2]|0)+ -1|0);j=vb(a,(c[g>>2]|0)+ -1|0,(c[j>>2]|0)+ -1|0)|0;if((j|0)!=0){f=13;break}h=h+1|0;if(h>>>0>f>>>0){j=0;f=13;break}}if((f|0)==13){i=e;return j|0}}}else{d=f;b=h}}else{b=(c[a+34928>>2]|0)!=0;d=b&1}f=0;while(1){Nb(a);if(b){l=c[a+34928>>2]|0;c[l+34324>>2]=c[a+34324>>2];c[l+34328>>2]=c[a+34328>>2];a=l}f=f+1|0;if(f>>>0>d>>>0){j=0;break}}i=e;return j|0}function xb(a){a=a|0;var b=0,d=0,e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0;b=i;if((c[a+104>>2]|0)==0){d=(c[a+16520>>2]|0)!=(0-(c[a+132>>2]|0)|0)}else{d=1}c[a+34144>>2]=d&1;if((Ad(a)|0)!=0){n=-1;i=b;return n|0}Yd(c[a+34248>>2]|0,c[a+124>>2]|0)|0;d=a+34300|0;e=c[d>>2]|0;if((e|0)==0){n=0;i=b;return n|0}j=e<<2;f=ke(j)|0;e=a+34920|0;c[e>>2]=f;if((f|0)==0){n=-1;i=b;return n|0}re(f|0,0,j|0)|0;f=a+34336|0;l=c[f>>2]|0;g=a+34332|0;m=c[g>>2]|0;n=aa(m,l)|0;h=a+108|0;k=c[h>>2]|0;do{if((aa(n,k)|0)>>>0>67108863){n=ke(j)|0;c[a+34924>>2]=n;if((n|0)==0){n=-1;i=b;return n|0}else{re(n|0,0,j|0)|0;break}}}while(0);j=a+34924|0;a=a+34296|0;n=0;while(1){if((aa(aa(m,l)|0,k)|0)>>>0>67108863){m=ke(4096)|0;c[(c[j>>2]|0)+(n<<2)>>2]=m;if((c[(c[j>>2]|0)+(n<<2)>>2]|0)==0){e=-1;d=17;break}k=Pa(0)|0;if((k|0)==0){e=-1;d=17;break}ue(c[(c[j>>2]|0)+(n<<2)>>2]|0,k|0)|0;if((id((c[e>>2]|0)+(n<<2)|0,k,1168)|0)!=0){e=-1;d=17;break}}else{if((pd((c[e>>2]|0)+(n<<2)|0)|0)!=0){e=-1;d=17;break}}Yd(c[(c[a>>2]|0)+(n<<2)>>2]|0,c[(c[e>>2]|0)+(n<<2)>>2]|0)|0;n=n+1|0;if(!(n>>>0<(c[d>>2]|0)>>>0)){e=0;d=17;break}l=c[f>>2]|0;m=c[g>>2]|0;k=c[h>>2]|0}if((d|0)==17){i=b;return e|0}return 0}function yb(b){b=b|0;var e=0,f=0,g=0,h=0,j=0,k=0;e=i;f=c[b+34248>>2]|0;j=b+34128|0;bd(f,c[j>>2]|0,3);bd(f,c[b+34140>>2]|0,1);g=b+116|0;bd(f,c[g>>2]|0,4);j=c[j>>2]|0;if((j|0)==6){bd(f,(c[b+34156>>2]|0)+ -1|0,4);bd(f,0,4)}else if((j|0)==3|(j|0)==2|(j|0)==1){bd(f,0,4);bd(f,0,4)}switch(c[b+16>>2]|0){case 7:{j=b+32908|0;k=a[j>>0]|0;if(k<<24>>24==0){a[j>>0]=13;k=13}bd(f,k&255,8);bd(f,a[b+32909>>0]|0,8);break};case 6:case 5:{k=b+32908|0;j=a[k>>0]|0;if(j<<24>>24==0){a[k>>0]=10;j=10}bd(f,j&255,8);break};case 3:case 2:{bd(f,d[b+32908>>0]|0,8);break};default:{}}j=b+34180|0;bd(f,c[j>>2]&1^1,1);k=c[j>>2]|0;if((k&1|0)==0){rb(c[b+34292>>2]|0,f,k>>>3&3,c[b+34156>>2]|0,0)}if((c[g>>2]|0)==3){Rd(f);i=e;return 0}bd(f,(c[j>>2]|0)>>>9&1^1,1);k=c[j>>2]|0;if((k&512|0)!=0?(bd(f,k>>>1&1^1,1),h=c[j>>2]|0,(h&2|0)==0):0){rb((c[b+34292>>2]|0)+64|0,f,h>>>5&3,c[b+34156>>2]|0,0)}if((c[g>>2]|0)==2){Rd(f);i=e;return 0}bd(f,(c[j>>2]|0)>>>10&1^1,1);g=c[j>>2]|0;if((g&1024|0)==0){Rd(f);i=e;return 0}bd(f,g>>>2&1^1,1);g=c[j>>2]|0;if((g&4|0)!=0){Rd(f);i=e;return 0}rb((c[b+34292>>2]|0)+128|0,f,g>>>7&3,c[b+34156>>2]|0,0);Rd(f);i=e;return 0}function zb(b){b=b|0;var d=0,e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0;d=i;k=b+34248|0;e=c[k>>2]|0;l=b+4|0;if(((c[l>>2]|0)+15|0)>>>0>4095){f=1}else{f=((c[b+8>>2]|0)+15|0)>>>0>4095}m=f&1^1;g=b+34160|0;if((c[b+34176>>2]|0)!=0){h=b+34160|0;if((c[g>>2]|0)==0){j=6}else{g=1}}else{c[g+0>>2]=0;c[g+4>>2]=0;c[g+8>>2]=0;c[g+12>>2]=0;h=b+34160|0;j=6}if((j|0)==6){if((c[b+34164>>2]|0)==0?(c[b+34168>>2]|0)==0:0){g=(c[b+34172>>2]|0)!=0}else{g=1}}bd(e,a[8720]|0,8);bd(c[k>>2]|0,a[8721>>0]|0,8);bd(c[k>>2]|0,a[8722>>0]|0,8);bd(c[k>>2]|0,a[8723>>0]|0,8);bd(c[k>>2]|0,a[8724>>0]|0,8);bd(c[k>>2]|0,a[8725>>0]|0,8);bd(c[k>>2]|0,a[8726>>0]|0,8);bd(c[k>>2]|0,a[8727>>0]|0,8);bd(e,1,4);if((c[b+32916>>2]|0)==0){bd(e,1,4)}else{bd(e,9,4)}k=b+132|0;if((c[k>>2]|0)==0){n=(c[b+16520>>2]|0)!=0}else{n=1}bd(e,n&1,1);bd(e,c[b+104>>2]|0,1);bd(e,c[b+64>>2]|0,3);bd(e,c[b+34144>>2]|0,1);bd(e,c[b+100>>2]|0,2);bd(e,m,1);bd(e,1,1);bd(e,g&1,1);bd(e,c[b+34148>>2]|0,1);bd(e,0,1);bd(e,0,2);bd(e,c[b+34136>>2]|0,1);bd(e,c[b+12>>2]|0,4);m=c[b+16>>2]|0;if((m|0)==0?(c[b+32912>>2]|0)!=0:0){bd(e,15,4)}else{bd(e,m,4)}m=f?32:16;Qd(e,(c[l>>2]|0)+ -1|0,m);Qd(e,(c[b+8>>2]|0)+ -1|0,m);m=c[k>>2]|0;l=b+16520|0;if(!((m|0)==0?(c[l>>2]|0)==0:0)){j=20}if((j|0)==20?(bd(e,m,12),bd(e,c[l>>2]|0,12),(c[k>>2]|0)!=0):0){j=f?16:8;l=0;do{n=l;l=l+1|0;bd(e,(c[b+(l<<2)+136>>2]|0)-(c[b+(n<<2)+136>>2]|0)|0,j)}while(l>>>0<(c[k>>2]|0)>>>0)}j=b+16520|0;if((c[j>>2]|0)!=0){f=f?16:8;k=0;do{n=k;k=k+1|0;bd(e,(c[b+(k<<2)+16524>>2]|0)-(c[b+(n<<2)+16524>>2]|0)|0,f)}while(k>>>0<(c[j>>2]|0)>>>0)}if(!g){Rd(e);yb(b)|0;i=d;return 0}bd(e,c[h>>2]|0,6);bd(e,c[b+34164>>2]|0,6);bd(e,c[b+34168>>2]|0,6);bd(e,c[b+34172>>2]|0,6);Rd(e);yb(b)|0;i=d;return 0}function Ab(b){b=b|0;var e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0;e=i;f=c[b+34128>>2]|0;h=c[b+12>>2]|0;if((h&-5|0)==3|(h|0)==4|(h|0)==8){if((f+ -1|0)>>>0<2){g=5}else{g=4}}else{if((h|0)==2&(f|0)==1){g=5}else{g=4}}if((g|0)==4){k=b+34240|0;c[k>>2]=0}else if((g|0)==5){u=(c[b+32924>>2]|0)==0;k=b+34240|0;c[k>>2]=u&1;if(u){u=((h|0)==2?128:256)|((f|0)==1?32:0);t=c[b+34332>>2]|0;f=(aa(t,u)|0)+256|0;if(((aa(t>>>16,u)|0)&33488896|0)!=0|f>>>0>1073741822){u=-1;i=e;return u|0}u=f<<2;t=ke(u)|0;c[b+34780>>2]=t;u=ke(u)|0;c[b+34784>>2]=u;if((t|0)==0|(u|0)==0){u=-1;i=e;return u|0}}}c[b+34268>>2]=0;c[b+34272>>2]=0;if((Cd(b)|0)!=0){u=-1;i=e;return u|0}f=b+34176|0;j=b+34180|0;if((c[f>>2]|0)!=0){h=b+34180|0;if((c[j>>2]&1|0)==0){j=h;q=0;l=0;k=0;r=0;n=0;m=0;h=0;p=0;o=0;g=25}else{l=0;k=0;n=0;m=0;p=0;o=0}}else{c[j>>2]=336;h=b+80|0;n=a[h>>0]|0;if(((n&255)<2?(c[b+116>>2]|0)==0:0)?(c[k>>2]|0)==0:0){g=0}else{g=(c[b+32928>>2]|0)==0}c[b+34140>>2]=((c[b+16>>2]|0)+ -5|0)>>>0<3?0:g&1;c[j>>2]=1872;g=(c[b+34136>>2]|0)==0;if(g){h=n}else{h=a[((c[b+34156>>2]|0)==1?b+89|0:h)>>0]|0}j=a[b+83>>0]|0;k=j<<24>>24==0?h:j;l=k&255;j=a[b+86>>0]|0;m=j<<24>>24==0?h:j;j=m&255;if(!g?(c[b+34156>>2]|0)==1:0){o=a[b+89>>0]|0;g=20}else{o=a[b+81>>0]|0;o=o<<24>>24==0?n:o;if(g){g=22}else{g=20}}if((g|0)==20){if((c[b+34156>>2]|0)==1){p=a[b+89>>0]|0}else{g=22}}if((g|0)==22){g=a[b+82>>0]|0;p=g<<24>>24==0?n:g}g=a[b+84>>0]|0;n=g<<24>>24==0?l:g&255;g=a[b+87>>0]|0;g=g<<24>>24==0?j:g&255;q=a[b+85>>0]|0;l=q<<24>>24==0?l:q&255;q=a[b+88>>0]|0;s=q<<24>>24==0?j:q&255;j=b+34180|0;q=(k&255)<2?0:k;l=l>>>0<2?0:l&255;k=n>>>0<2?0:n&255;r=(m&255)<2?0:m;n=s>>>0<2?0:s&255;m=g>>>0<2?0:g&255;h=(h&255)<2?0:h;p=(p&255)<2?0:p;o=(o&255)<2?0:o;g=25}if((g|0)==25){s=b+34292|0;g=b+34156|0;if((Fd(c[s>>2]|0,c[g>>2]|0,1)|0)!=0){u=-1;i=e;return u|0}Hd(b,0);if((c[g>>2]|0)==0){u=0}else{t=0;do{if((c[f>>2]|0)==0){if((t|0)==0){u=h}else{u=(t|0)==1?q:r}a[b+t+34184>>0]=u;a[c[(c[s>>2]|0)+(t<<2)>>2]>>0]=u}else{a[c[(c[s>>2]|0)+(t<<2)>>2]>>0]=a[b+t+34184>>0]|0}t=t+1|0;u=c[g>>2]|0}while(t>>>0<u>>>0)}Gd(c[s>>2]|0,(c[j>>2]|0)>>>3&3,u,0,1,c[b+34140>>2]|0);if((c[g>>2]|0)==0){h=j}else{h=c[s>>2]|0;q=0;while(1){u=c[h+(q<<2)>>2]|0;c[u+8>>2]=c[u+4>>2]>>1;q=q+1|0;if(!(q>>>0<(c[g>>2]|0)>>>0)){h=j;break}}}}g=b+116|0;j=c[g>>2]|0;if((j|0)!=3){if((c[h>>2]&2|0)==0){j=b+34292|0;q=b+34156|0;if((Fd((c[j>>2]|0)+64|0,c[q>>2]|0,1)|0)!=0){u=-1;i=e;return u|0}Hd(b,1);if((c[q>>2]|0)==0){s=0}else{r=0;do{if((c[f>>2]|0)==0){if((r|0)==0){s=o}else{s=(r|0)==1?k:m}a[b+r+34200>>0]=s;a[c[(c[j>>2]|0)+(r<<2)+64>>2]>>0]=s}else{a[c[(c[j>>2]|0)+(r<<2)+64>>2]>>0]=a[b+r+34200>>0]|0}r=r+1|0;s=c[q>>2]|0}while(r>>>0<s>>>0)}Gd((c[j>>2]|0)+64|0,(c[h>>2]|0)>>>5&3,s,0,1,c[b+34140>>2]|0);j=c[g>>2]|0}if((j|0)!=2?(c[h>>2]&4|0)==0:0){g=b+34292|0;j=b+34156|0;if((Fd((c[g>>2]|0)+128|0,c[j>>2]|0,1)|0)!=0){u=-1;i=e;return u|0}Hd(b,2);if((c[j>>2]|0)==0){m=0}else{m=0;while(1){if((c[f>>2]|0)==0){if((m|0)==0){k=p}else{k=(m|0)==1?l:n}a[b+m+34216>>0]=k;a[c[(c[g>>2]|0)+(m<<2)+128>>2]>>0]=k}else{a[c[(c[g>>2]|0)+(m<<2)+128>>2]>>0]=a[b+m+34216>>0]|0}k=m+1|0;m=c[j>>2]|0;if(k>>>0<m>>>0){m=k}else{break}}}Gd((c[g>>2]|0)+128|0,(c[h>>2]|0)>>>7&3,m,0,0,c[b+34140>>2]|0)}}if((_d(b)|0)!=0){u=-1;i=e;return u|0}f=c[b+132>>2]|0;if(f>>>0>4095){u=-1;i=e;return u|0}if((gb(b,f+1|0,d[b+120>>0]|0)|0)!=0){u=-1;i=e;return u|0}if((c[b+34932>>2]|0)==0){xb(b)|0;Bd(b)|0;zb(b)|0;u=0;i=e;return u|0}else{u=c[b+34928>>2]|0;c[b+34248>>2]=c[u+34248>>2];c[b+34296>>2]=c[u+34296>>2];c[b+34300>>2]=c[u+34300>>2];a[b+34236>>0]=a[u+34236>>0]|0;c[b+34920>>2]=c[u+34920>>2];c[b+34264>>2]=c[u+34264>>2];Bd(b)|0;u=0;i=e;return u|0}return 0}function Bb(a,b,d){a=a|0;b=b|0;d=d|0;var e=0,f=0,g=0,h=0,j=0,k=0,l=0;e=i;b=(b|0)==0?1:b;b=b>>>0>d>>>0?1:b;g=b>>>0>4096?4096:b;b=0;j=0;while(1){h=j+1|0;if(!(h>>>0<g>>>0)){break}j=c[a+(j<<2)>>2]|0;if((j|0)==0|j>>>0>65535){f=4;break}b=j+b|0;if(b>>>0<d>>>0){j=h}else{g=h;break}}if((f|0)==4){h=d+ -1|0;while(1){if((((h+g|0)>>>0)/(g>>>0)|0)>>>0>65535){g=g+1|0}else{break}}if(g>>>0>1){h=g;k=d;while(1){j=h+ -1|0;l=((j+k|0)>>>0)/(h>>>0)|0;c[a+(g-h<<2)>>2]=l;if(j>>>0>1){h=j;k=k-l|0}else{break}}}}if((d-b|0)>>>0>65536){b=d+ -1|0;while(1){if((((b+g|0)>>>0)/(g>>>0)|0)>>>0>65535){g=g+1|0}else{break}}if(g>>>0>1){f=g;while(1){b=f+ -1|0;h=((b+d|0)>>>0)/(f>>>0)|0;c[a+(g-f<<2)>>2]=h;if(b>>>0>1){f=b;d=d-h|0}else{f=14;break}}}}else{f=14}if((f|0)==14){if(g>>>0>1){f=c[a>>2]|0;d=1;do{l=a+(d<<2)|0;f=(c[l>>2]|0)+f|0;c[l>>2]=f;d=d+1|0}while((d|0)!=(g|0))}}f=g+ -1|0;if((f|0)==0){c[a>>2]=0;i=e;return g|0}else{d=g}while(1){c[a+(f<<2)>>2]=c[a+(d+ -2<<2)>>2];d=f+ -1|0;if((d|0)==0){break}else{l=f;f=d;d=l}}c[a>>2]=0;i=e;return g|0}function Cb(b,e){b=b|0;e=e|0;var f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0;f=i;l=c[b>>2]|0;if(!(l>>>0>268435456)?(k=b+4|0,n=c[k>>2]|0,!(n>>>0>268435456|(l|0)==0|(n|0)==0)):0){g=e+16|0;n=c[g>>2]|0;if(((n+ -1|0)>>>0<2?(c[e+24>>2]|0)==2:0)?(l+15|0)>>>0<32:0){Ca(1776)|0;n=-1;i=f;return n|0}h=e+40|0;if((c[h>>2]|0)>>>0>3){c[h>>2]=0}m=c[b+12>>2]|0;do{if((m|0)==8){if(((c[b+8>>2]|0)==7?(c[b+16>>2]|0)==16:0)?(c[b+20>>2]|0)==0:0){if((m|0)==10){j=16;break}else if((m|0)==9){j=21;break}else{break}}Ca(1744)|0;n=-1;i=f;return n|0}else if((m|0)==10){j=16}else if((m|0)==9){j=21}}while(0);do{if((j|0)==16){if(((c[b+8>>2]|0)==7?(c[b+16>>2]|0)==16:0)?(c[b+20>>2]|0)==0:0){if((m|0)==9){j=21;break}else{break}}Ca(1704)|0;n=-1;i=f;return n|0}}while(0);do{if((j|0)==21){if(((c[b+8>>2]|0)==7?(c[b+16>>2]|0)==32:0)?(c[b+20>>2]|0)==0:0){break}Ca(1672)|0;n=-1;i=f;return n|0}}while(0);if((m+ -8|0)>>>0>2|n>>>0<3){j=n}else{c[g>>2]=3;j=3}do{if((m|0)==0){if((c[b+8>>2]|0)==0){c[g>>2]=0;j=0;break}Ca(1632)|0;n=-1;i=f;return n|0}}while(0);n=e+20|0;if((c[n>>2]|0)!=1){c[n>>2]=1}do{if((d[e+36>>0]|0)>1){if(!(((c[b+8>>2]|0)+ -1|0)>>>0<2)?!((m|0)==0|(m|0)==9|(m|0)==8):0){break}Ca(1584)|0;n=-1;i=f;return n|0}}while(0);do{if((j+ -1|0)>>>0<2){if(!((m|0)==7|(m|0)==4)?(c[b+8>>2]|0)!=8:0){break}Ca(1528)|0;n=-1;i=f;return n|0}}while(0);n=e+56|0;c[n>>2]=(Bb(e+60|0,(c[n>>2]|0)+1|0,(l+15|0)>>>4)|0)+ -1;l=e+16448|0;n=e+16444|0;k=Bb(l,(c[n>>2]|0)+1|0,((c[k>>2]|0)+15|0)>>>4)|0;j=k+ -1|0;c[n>>2]=j;if(((c[e+32840>>2]|0)!=0?((c[g>>2]|0)+ -1|0)>>>0<2:0)?(c[e+24>>2]|0)==2:0){a:do{if((k|0)>1){l=c[l>>2]|0;n=1;while(1){m=c[e+(n<<2)+16448>>2]|0;n=n+1|0;if((m-l|0)<2){k=0;break a}if((n|0)<(k|0)){l=m}else{k=1;break}}}else{k=1}}while(0);if(!(k&((((c[b>>2]|0)+15|0)>>>4)-(c[e+(j<<2)+16448>>2]|0)|0)>1)){Ca(1424)|0;n=-1;i=f;return n|0}}e=e+32|0;if((c[e>>2]|0)>>>0>16){n=-1;i=f;return n|0}k=b+8|0;l=c[k>>2]|0;j=c[g>>2]|0;do{if((l|0)==0){if((j|0)==0){j=62}else{l=0;j=56}}else{if((j|0)==2){if(l>>>0<2){l=1;j=56;break}}else{if((j|0)==3&l>>>0<3){j=56;break}}if((l|0)==6){c[g>>2]=6;l=6;j=59}else{l=j;j=59}}}while(0);if((j|0)==56){c[g>>2]=l;j=59}b:do{if((j|0)==59){do{if((c[k>>2]|0)==4&(l|0)==6){c[g>>2]=4}else{if((l|0)==0){j=62;break b}else if((l|0)==4){break}else if((l|0)==6){break b}c[e>>2]=3;break b}}while(0);c[e>>2]=4}}while(0);if((j|0)==62){c[e>>2]=1}if((c[h>>2]|0)>>>0>4){c[h>>2]=0}a[b+28>>0]=0;a[b+29>>0]=0;n=0;i=f;return n|0}Ca(1864)|0;n=-1;i=f;return n|0}function Db(b,d,e){b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0;f=i;if((Cb(b,d)|0)!=0){u=-1;i=f;return u|0}c[e>>2]=0;j=c[1176+(c[d+20>>2]<<2)>>2]|0;k=c[d+16>>2]|0;g=j<<8;m=aa(c[1184+(k<<2)>>2]|0,j<<4)|0;u=(c[b>>2]|0)+15|0;l=u>>>4;p=c[d+32>>2]|0;h=(aa(p+ -1|0,m)|0)+g|0;if((aa(h,u>>>19)|0)>>>0>65535){u=-1;i=f;return u|0}n=(aa(l<<1,h)|0)+59798|0;h=ke(n)|0;if((h|0)==0){u=-1;i=f;return u|0}re(h|0,0,n|0)|0;r=h+35064|0;c[h+34128>>2]=k;n=h+34136|0;c[n>>2]=(a[d+36>>0]|0)==3;o=h+34156|0;c[o>>2]=p;s=h+34160|0;k=h+34340|0;c[s+0>>2]=0;c[s+4>>2]=0;c[s+8>>2]=0;c[s+12>>2]=0;c[k>>2]=j;c[h+34244>>2]=0;c[h+34176>>2]=0;c[h>>2]=35064;k=h+4|0;s=k+0|0;q=b+0|0;t=s+72|0;do{c[s>>2]=c[q>>2];s=s+4|0;q=q+4|0}while((s|0)<(t|0));te(h+76|0,d|0,32860)|0;q=h+32909|0;s=a[q>>0]|0;if(s<<24>>24==0){a[q>>0]=-124;s=-124}a[q>>0]=(s&255)+128;c[h+34324>>2]=0;c[h+34328>>2]=0;q=h+34332|0;c[q>>2]=((c[k>>2]|0)+15|0)>>>4;c[h+34336>>2]=((c[h+8>>2]|0)+15|0)>>>4;c[h+34408>>2]=18;c[h+34420>>2]=19;c[h+34424>>2]=20;c[h+34428>>2]=20;c[h+34432>>2]=20;c[h+34436>>2]=20;c[h+34440>>2]=20;c[h+34444>>2]=20;c[h+34448>>2]=20;c[h+34452>>2]=20;c[h+34456>>2]=20;k=h+34928|0;c[k>>2]=0;c[h+34932>>2]=0;u=r+127&-128;if((p|0)!=0){s=h+34524|0;r=h+34588|0;p=g;t=0;while(1){c[s+(t<<2)>>2]=u;v=c[q>>2]|0;c[r+(t<<2)>>2]=u+(aa(v,p)|0);u=u+(aa((c[q>>2]|0)+v|0,p)|0)|0;t=t+1|0;if(t>>>0<(c[o>>2]|0)>>>0){p=m}else{break}}}m=h+34248|0;c[m>>2]=(u+16383&-16384)+8192;if((Ab(h)|0)!=0){v=-1;i=f;return v|0}if((c[n>>2]|0)==0){l=0}else{n=(aa(j<<9,l)|0)+35191|0;l=ke(n)|0;if((l|0)==0){v=-1;i=f;return v|0}re(l|0,0,n|0)|0;n=l+35064|0;c[l+34128>>2]=0;c[l+34156>>2]=1;c[l+34136>>2]=1;c[l+34340>>2]=j;c[l>>2]=35064;j=l+4|0;s=j+0|0;q=b+0|0;t=s+72|0;do{c[s>>2]=c[q>>2];s=s+4|0;q=q+4|0}while((s|0)<(t|0));te(l+76|0,d|0,32860)|0;b=l+32909|0;d=a[b>>0]|0;if(d<<24>>24==0){a[b>>0]=-124;d=-124}a[b>>0]=(d&255)+128;c[l+34324>>2]=0;c[l+34328>>2]=0;v=((c[j>>2]|0)+15|0)>>>4;c[l+34332>>2]=v;c[l+34336>>2]=((c[l+8>>2]|0)+15|0)>>>4;c[l+34408>>2]=18;c[l+34420>>2]=19;c[l+34424>>2]=20;c[l+34428>>2]=20;c[l+34432>>2]=20;c[l+34436>>2]=20;c[l+34440>>2]=20;c[l+34444>>2]=20;c[l+34448>>2]=20;c[l+34452>>2]=20;c[l+34456>>2]=20;u=n+127&-128;c[l+34524>>2]=u;c[l+34588>>2]=u+(aa(v,g)|0);c[l+34248>>2]=c[m>>2];c[l+34928>>2]=h;c[l+34932>>2]=1;Ab(l)|0;yb(l)|0}c[k>>2]=l;c[e>>2]=h;if((c[h+34300>>2]|0)!=0){v=0;i=f;return v|0}v=c[m>>2]|0;Rd(v);bd(v,4,16);bd(v,111,8);bd(v,255,8);bd(v,1,16);v=0;i=f;return v|0}function Eb(a,b){a=a|0;b=b|0;var d=0,e=0,f=0,g=0,h=0,j=0;d=i;f=c[a+34928>>2]|0;if((c[a>>2]|0)!=35064){h=-1;i=d;return h|0}e=a+32936|0;c[e+0>>2]=c[b+0>>2];c[e+4>>2]=c[b+4>>2];c[e+8>>2]=c[b+8>>2];c[e+12>>2]=c[b+12>>2];c[e+16>>2]=c[b+16>>2];c[e+20>>2]=c[b+20>>2];e=a+34328|0;c[e>>2]=0;ed(a);if((f|0)!=0){h=f+32936|0;c[h+0>>2]=c[b+0>>2];c[h+4>>2]=c[b+4>>2];c[h+8>>2]=c[b+8>>2];c[h+12>>2]=c[b+12>>2];c[h+16>>2]=c[b+16>>2];c[h+20>>2]=c[b+20>>2]}b=a+34324|0;if((c[b>>2]|0)==0){h=a+34428|0;g=a+34424|0;f=a+34432|0}else{h=a+34440|0;g=a+34436|0;f=a+34444|0}f=c[f>>2]|0;h=c[h>>2]|0;g=c[g>>2]|0;Wa[c[a+34408>>2]&31](a)|0;if((Wa[g&31](a)|0)!=0){h=-1;i=d;return h|0}fd(a);c[e>>2]=1;g=a+34332|0;a:do{if((c[g>>2]|0)>>>0>1){while(1){if((Wa[h&31](a)|0)!=0){a=-1;break}fd(a);j=(c[e>>2]|0)+1|0;c[e>>2]=j;if(!(j>>>0<(c[g>>2]|0)>>>0)){break a}}i=d;return a|0}}while(0);if((Wa[f&31](a)|0)!=0){j=-1;i=d;return j|0}if((c[b>>2]|0)==0){e=1}else{gd(a);e=(c[b>>2]|0)+1|0}c[b>>2]=e;hd(a);j=0;i=d;return j|0}function Fb(a){a=a|0;var b=0,e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0;h=i;v=c[a+34128>>2]|0;j=(c[a+12>>2]|0)==2;p=(v|0)==1;g=j&1^1;e=a+34780|0;f=a+34784|0;u=a+34332|0;t=j?7:8;s=g+7|0;r=a+34324|0;q=a+34336|0;y=(v|0)==2;x=y&1;w=8-x|0;v=1;do{if(!j){z=c[((v|0)==1?e:f)>>2]|0;if(y){A=c[a+(v<<2)+34716>>2]|0}else{A=z}B=0;do{E=c[z+((d[8402+(B<<4)>>0]|0)<<2)>>2]|0;H=c[z+((d[8401+(B<<4)>>0]|0)<<2)>>2]|0;F=c[z+((d[8400+(B<<4)>>0]|0)<<2)>>2]|0;D=E+(F<<1)+(H+F+H<<2)|0;if(c[u>>2]<<4>>>0>2){C=2;I=0;while(1){c[A+((d[((I&14)>>>x)+(8400+(B<<4))>>0]|0)+(I>>>4<<w)<<2)>>2]=E+8+D>>4;G=I+3|0;G=c[z+((d[(G&15)+(8400+(B<<4))>>0]|0|G>>>4<<8)<<2)>>2]|0;I=I+4|0;J=C+2|0;D=F+(E<<1)+(G+E+H<<2)|0;if(J>>>0<c[u>>2]<<4>>>0){K=C;F=E;C=J;H=G;E=c[z+((d[(I&14)+(8400+(B<<4))>>0]|0|I>>>4<<8)<<2)>>2]|0;I=K}else{break}}}else{E=F;C=0}c[A+((d[((C&14)>>>x)+(8400+(B<<4))>>0]|0)+(C>>>4<<w)<<2)>>2]=E+8+D>>4;B=B+1|0}while((B|0)!=16)}if(p?(b=c[a+(v<<2)+34716>>2]|0,k=c[((v|0)==1?e:f)>>2]|0,K=c[u>>2]|0,n=K<<t,m=K<<3,o=m+n|0,l=o+m|0,m=l+m|0,(K&536870911|0)!=0):0){C=a+(v<<2)+34652|0;B=0;do{E=B>>>3;z=E<<s;D=B&7;A=D<<g;if((c[r>>2]|0)==0){G=c[k+((d[8432+A>>0]|0)+z<<2)>>2]|0;I=c[k+((d[8416+A>>0]|0)+z<<2)>>2]|0;F=d[8400+A>>0]|0;E=E<<6;J=G;H=I}else{J=k+(l+B<<2)|0;I=c[J>>2]|0;H=k+(m+B<<2)|0;F=d[8400+A>>0]|0;E=E<<6;c[(c[C>>2]|0)+((d[8712+D>>0]|0)+E<<2)>>2]=(c[k+(n+B<<2)>>2]|0)+8+(I<<1)+(I+(c[k+(o+B<<2)>>2]|0)+(c[H>>2]|0)<<2)+(c[k+(F+z<<2)>>2]|0)>>4;J=c[J>>2]|0;H=c[H>>2]|0;I=c[k+((d[8416+A>>0]|0)+z<<2)>>2]|0;G=c[k+((d[8432+A>>0]|0)+z<<2)>>2]|0}K=c[k+(F+z<<2)>>2]|0;F=(K<<1)+8+G+J+(I+K+H<<2)>>4;H=K;K=0;while(1){c[b+((d[8656+(K>>>1<<3)+D>>0]|0)+E<<2)>>2]=F;L=I;I=c[k+((d[8400+(K+3<<4)+A>>0]|0)+z<<2)>>2]|0;J=c[k+((d[8400+(K+4<<4)+A>>0]|0)+z<<2)>>2]|0;K=K+2|0;F=(G<<1)+8+J+H+(I+G+L<<2)>>4;if(!(K>>>0<12)){break}else{H=G;G=J}}c[b+((d[8704+D>>0]|0)+E<<2)>>2]=F;if(((c[r>>2]|0)+1|0)==(c[q>>2]|0)){c[b+((d[8712+D>>0]|0)+E<<2)>>2]=G+8+J+(J<<1)+(J+I+(c[k+((d[8640+A>>0]|0)+z<<2)>>2]|0)<<2)>>4}else{c[k+(n+B<<2)>>2]=c[k+((d[8592+A>>0]|0)+z<<2)>>2];c[k+(o+B<<2)>>2]=c[k+((d[8608+A>>0]|0)+z<<2)>>2];c[k+(l+B<<2)>>2]=c[k+((d[8624+A>>0]|0)+z<<2)>>2];c[k+(m+B<<2)>>2]=c[k+((d[8640+A>>0]|0)+z<<2)>>2]}B=B+1|0}while(B>>>0<c[u>>2]<<3>>>0)}v=v+1|0}while((v|0)!=3);i=h;return}function Gb(a){a=a|0;var b=0,e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0;b=i;i=i+64|0;e=b;k=c[a+4>>2]|0;f=a+34332|0;o=c[f>>2]|0;if((k|0)==(o<<4|0)){i=b;return}g=c[((c[a+32924>>2]|0)==0?a+12|0:a+34128|0)>>2]|0;m=c[a+108>>2]|0;h=k+ -1|0;j=(g+ -1|0)>>>0<2|(g|0)==0?1:m;if(!(j>>>0<17)){pa(1224,1248,1698,1272)}if(!(m>>>0<17)){pa(1288,1248,1700,1272)}if((m|0)!=0){l=0;do{s=l&15;c[e+(s<<2)>>2]=c[a+(s<<2)+34716>>2];l=l+1|0}while(l>>>0<m>>>0)}if((c[a+34240>>2]|0)!=0){c[e+4>>2]=c[a+34780>>2];c[e+8>>2]=c[a+34784>>2]}a=h>>>4;l=a<<8;m=h&15;n=(j|0)==0;p=o;r=o;o=0;do{q=d[8400+(o<<4)+m>>0]|0|l;s=r<<4;a:do{if(k>>>0<s>>>0){if(n){q=k;while(1){q=q+1|0;if(!(q>>>0<s>>>0)){break a}}}else{p=k}while(1){s=d[(p&15)+(8400+(o<<4))>>0]|0|p>>>4<<8;r=0;do{t=c[e+((r&15)<<2)>>2]|0;c[t+(s<<2)>>2]=c[t+(q<<2)>>2];r=r+1|0}while((r|0)!=(j|0));r=c[f>>2]|0;p=p+1|0;if(!(p>>>0<r<<4>>>0)){p=r;break}}}}while(0);o=o+1|0}while((o|0)!=16);if((g|0)==2){j=h>>>1;g=a<<7;h=j&7;j=j+1|0;k=0;do{l=(d[8400+(k<<4)+h>>0]|0)+g|0;if(j>>>0<p<<3>>>0){m=c[e+4>>2]|0;a=c[e+8>>2]|0;n=j;do{p=(d[(n&7)+(8400+(k<<4))>>0]|0)+(n>>>3<<7)|0;c[m+(p<<2)>>2]=c[m+(l<<2)>>2];c[a+(p<<2)>>2]=c[a+(l<<2)>>2];n=n+1|0;p=c[f>>2]|0}while(n>>>0<p<<3>>>0)}k=k+1|0}while((k|0)!=16);i=b;return}else if((g|0)==1){g=h>>>1;h=a<<6;j=g&7;g=g+1|0;k=0;do{l=(d[8656+(k<<3)+j>>0]|0)+h|0;if(g>>>0<p<<3>>>0){m=c[e+4>>2]|0;a=c[e+8>>2]|0;n=g;do{p=(d[(n&7)+(8656+(k<<3))>>0]|0)+(n>>>3<<6)|0;c[m+(p<<2)>>2]=c[m+(l<<2)>>2];c[a+(p<<2)>>2]=c[a+(l<<2)>>2];n=n+1|0;p=c[f>>2]|0}while(n>>>0<p<<3>>>0)}k=k+1|0}while((k|0)!=8);i=b;return}else{i=b;return}}function Hb(f){f=f|0;var h=0,j=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0.0;h=i;if((c[f+34932>>2]|0)!=0){I=0;i=h;return I|0}v=c[f+34928>>2]|0;if((v|0)==0){I=0;i=h;return I|0}j=(c[v+34140>>2]|0)!=0?3:0;w=c[f+16>>2]|0;o=((c[f+12>>2]|0)==4?4:3)+(c[f+24>>2]|0)|0;n=c[f+32940>>2]|0;r=c[f+4>>2]|0;p=c[v+34716>>2]|0;q=f+20|0;x=(r|0)==0;m=f+32944|0;l=f+34332|0;s=r+ -1|0;t=s>>>4<<8;s=s&15;u=0;f=c[f+32936>>2]|0;a:while(1){switch(w|0){case 2:{B=(c[q>>2]|0)>>>4;if(!x){z=d[v+32908>>0]|0;y=0;A=f;while(1){c[p+((d[(y&15)+(8400+(u<<4))>>0]|y>>>4<<8)<<2)>>2]=(e[A+(o<<1)>>1]|0)+ -32768>>z<<j;y=y+1|0;if((y|0)==(r|0)){break}else{A=A+(B<<1)|0}}}break};case 6:{A=(c[q>>2]|0)>>>5;if(!x){z=d[v+32908>>0]|0;y=0;B=f;while(1){c[p+((d[(y&15)+(8400+(u<<4))>>0]|y>>>4<<8)<<2)>>2]=c[B+(o<<2)>>2]>>z<<j;y=y+1|0;if((y|0)==(r|0)){break}else{B=B+(A<<2)|0}}}break};case 7:{y=(c[q>>2]|0)>>>5;if(!x){C=(a[v+32909>>0]|0)+ -127|0;B=d[v+32908>>0]|0;A=23-B|0;z=1<<A+ -1;E=0;D=f;while(1){J=+g[D+(o<<2)>>2];if(J==0.0){F=0}else{F=(g[k>>2]=J,c[k>>2]|0);H=F>>>23&255;G=F&8388607;I=(H|0)==0;G=I?G:G|8388608;H=C+H+(I&1)|0;if((H|0)<2){if((H|0)<1){G=G>>>(1-H|0)}H=G>>>23&1}F=F>>31;F=(((G&8388607)+z>>A)+(H<<B)^F)-F|0}c[p+((d[(E&15)+(8400+(u<<4))>>0]|E>>>4<<8)<<2)>>2]=F<<j;E=E+1|0;if((E|0)==(r|0)){break}else{D=D+(y<<2)|0}}}break};case 1:{A=(c[q>>2]|0)>>>3;if(!x){z=0;y=f;while(1){c[p+((d[(z&15)+(8400+(u<<4))>>0]|z>>>4<<8)<<2)>>2]=(d[y+o>>0]|0)+ -128<<j;z=z+1|0;if((z|0)==(r|0)){break}else{y=y+A|0}}}break};case 4:{y=(c[q>>2]|0)>>>4;if(!x){A=0;z=f;while(1){H=b[z+(o<<1)>>1]|0;I=H>>31;c[p+((d[(A&15)+(8400+(u<<4))>>0]|A>>>4<<8)<<2)>>2]=(H&32767^I)-I<<j;A=A+1|0;if((A|0)==(r|0)){break}else{z=z+(y<<1)|0}}}break};case 3:{y=(c[q>>2]|0)>>>4;if(!x){z=d[v+32908>>0]|0;B=0;A=f;while(1){c[p+((d[(B&15)+(8400+(u<<4))>>0]|B>>>4<<8)<<2)>>2]=b[A+(o<<1)>>1]>>z<<j;B=B+1|0;if((B|0)==(r|0)){break}else{A=A+(y<<1)|0}}}break};default:{j=-1;l=34;break a}}y=u+1|0;if(y>>>0<n>>>0){f=f+(c[m>>2]|0)|0}if(r>>>0<c[l>>2]<<4>>>0){z=p+((d[8400+(u<<4)+s>>0]|t)<<2)|0;A=r;do{c[p+((d[(A&15)+(8400+(u<<4))>>0]|A>>>4<<8)<<2)>>2]=c[z>>2];A=A+1|0}while(A>>>0<c[l>>2]<<4>>>0)}if(y>>>0<16){u=y}else{j=0;l=34;break}}if((l|0)==34){i=h;return j|0}return 0}function Ib(f){f=f|0;var h=0,j=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0,Y=0,Z=0,_=0,$=0,aa=0,ba=0,ca=0,da=0,ea=0,fa=0,ga=0,ha=0,ia=0,ja=0,ka=0,la=0,ma=0,na=0,oa=0,qa=0,ra=0.0;h=i;i=i+128|0;y=h+64|0;t=h;l=(c[f+34140>>2]|0)!=0?3:0;r=c[f+16>>2]|0;x=c[f+12>>2]|0;u=f+34128|0;M=c[u>>2]|0;R=c[f+20>>2]|0;m=R>>>3;if((x|0)!=1){if((c[f+32924>>2]|0)==0){n=1}else{n=(M|0)==1?2:1}}else{n=2}o=c[f+32940>>2]|0;p=c[f+4>>2]|0;s=(c[f+28>>2]|0)!=0?2:0;z=2-s|0;ea=c[f+32936>>2]|0;O=a[f+32908>>0]|0;T=a[f+32909>>0]|0;q=c[f+34716>>2]|0;A=c[f+34720>>2]|0;C=c[f+34724>>2]|0;if((ad(f,p,o)|0)!=0){qa=-1;i=h;return qa|0}j=f+34240|0;if((c[j>>2]|0)==0){B=(M|0)==0;w=f+34784|0;v=f+34780|0;A=B?q:A;I=B?q:C}else{A=f+34780|0;I=f+34784|0;w=I;v=A;A=c[A>>2]|0;I=c[I>>2]|0}L=f+32924|0;K=f+24|0;J=128<<l;B=f+34932|0;C=(p|0)==0;G=f+34156|0;E=t+4|0;F=t+8|0;D=(M|0)==4;H=f+34728|0;M=(M|0)==0;N=R>>>4;O=O&255;Q=32768>>>O<<l;P=f+108|0;R=R>>>5;S=-2147483648>>O<<l;da=(T<<24>>24)+ -127|0;U=23-O|0;ca=1<<U+ -1;W=(x|0)==7;V=-16<<l;X=l+1|0;Y=-32<<l;Z=-512<<l;$=(x|0)==0;_=f+32912|0;aa=y+4|0;ba=y+8|0;T=f+32944|0;fa=0;a:while(1){do{if(!(fa>>>0<16)){l=191;break a}b:do{if((c[L>>2]|0)==0){do{switch(r|0){case 1:{ha=ea+(c[K>>2]|0)|0;switch(x|0){case 4:{if(D){ia=c[H>>2]|0}else{ia=q}if(C){break b}else{ga=0}while(1){la=d[ha>>0]<<l;oa=d[ha+1>>0]<<l;na=d[ha+3>>0]<<l;ma=(d[ha+2>>0]<<l)-la|0;la=(ma+1>>1)-oa+la|0;oa=(la>>1)-na+oa|0;qa=d[(ga&15)+(8400+(fa<<4))>>0]|ga>>>4<<8;c[A+(qa<<2)>>2]=la;c[I+(qa<<2)>>2]=0-ma;c[ia+(qa<<2)>>2]=(oa+1>>1)+na;c[q+(qa<<2)>>2]=J-oa;ga=ga+1|0;if((ga|0)==(p|0)){break}else{ha=ha+m|0}}break};case 6:case 3:case 0:{ia=c[G>>2]|0;if(!(ia>>>0<17)){l=45;break a}ga=(ia|0)==0;if(!ga){ja=0;do{qa=ja&15;c[t+(qa<<2)>>2]=c[f+(qa<<2)+34716>>2];ja=ja+1|0}while((ja|0)!=(ia|0))}if((c[j>>2]|0)!=0){c[E>>2]=c[v>>2];c[F>>2]=c[w>>2]}if(C){break b}else{ja=0}while(1){ka=d[(ja&15)+(8400+(fa<<4))>>0]|ja>>>4<<8;if(!ga){la=0;do{qa=la&15;c[(c[t+(qa<<2)>>2]|0)+(ka<<2)>>2]=(d[ha+qa>>0]<<l)-J;la=la+1|0}while((la|0)!=(ia|0))}ja=ja+1|0;if((ja|0)==(p|0)){break}else{ha=ha+m|0}}break};case 7:{if((c[B>>2]|0)!=0){l=41;break a}if(C){break b}else{ga=0}while(1){oa=d[ha+z>>0]<<l;na=d[ha+1>>0]<<l;ma=(d[ha+s>>0]<<l)-oa|0;oa=(ma+1>>1)-na+oa|0;qa=d[(ga&15)+(8400+(fa<<4))>>0]|ga>>>4<<8;c[A+(qa<<2)>>2]=0-oa;c[I+(qa<<2)>>2]=ma;c[q+(qa<<2)>>2]=na-J+(oa>>1);ga=ga+1|0;if((ga|0)==(p|0)){break}else{ha=ha+m|0}}break};case 8:{if(C){break b}else{ga=0}while(1){qa=a[ha+3>>0]|0;ka=qa&255;ma=d[ha>>0]|0;if(!(qa<<24>>24==0)){ia=ka+ -1|0;ja=(ia|0)>0;if((ma&128|0)==0&ja){la=ia;na=1;while(1){qa=ma<<1;ma=qa|na;na=la+ -1|0;if((qa&128|0)==0&(na|0)>0){la=na;na=0}else{break}}}else{la=ka;na=ia}if((na|0)!=0){ma=ma&127|la<<7}la=ma<<l;na=d[ha+1>>0]|0;if((na&128|0)==0&ja){ma=ia;oa=1;while(1){qa=na<<1;na=qa|oa;oa=ma+ -1|0;if((qa&128|0)==0&(oa|0)>0){ma=oa;oa=0}else{break}}}else{ma=ka;oa=ia}if((oa|0)!=0){na=na&127|ma<<7}ma=na<<l;na=d[ha+2>>0]|0;if((na&128|0)==0&ja){ja=1;while(1){qa=na<<1;na=qa|ja;ja=ia+ -1|0;if((qa&128|0)==0&(ja|0)>0){ia=ja;ja=0}else{ka=ia;ia=ja;break}}}if((ia|0)!=0){na=na&127|ka<<7}}else{na=0;la=0;ma=0}na=(na<<l)-la|0;oa=(na+1>>1)-ma+la|0;qa=d[(ga&15)+(8400+(fa<<4))>>0]|ga>>>4<<8;c[A+(qa<<2)>>2]=0-oa;c[I+(qa<<2)>>2]=na;c[q+(qa<<2)>>2]=(oa>>1)+ma;ga=ga+1|0;if((ga|0)==(p|0)){break}else{ha=ha+m|0}}break};case 2:{if(C){break b}else{ga=0}while(1){ia=ga>>>4;if(!M){qa=(d[(ga>>>1&7)+(8400+(fa<<4))>>0]|0)+(ia<<7)|0;c[A+(qa<<2)>>2]=(d[ha>>0]<<l)-J;c[I+(qa<<2)>>2]=(d[ha+2>>0]<<l)-J}qa=ia<<8;oa=ga&14;c[q+((d[8400+(fa<<4)+oa>>0]|qa)<<2)>>2]=(d[ha+1>>0]<<l)-J;c[q+((d[(oa|1)+(8400+(fa<<4))>>0]|qa)<<2)>>2]=(d[ha+3>>0]<<l)-J;ga=ga+2|0;if(!(ga>>>0<p>>>0)){break}else{ha=ha+m|0}}break};case 1:{if(C){break b}ga=fa+1|0;ia=fa>>>1;ja=0;while(1){ka=ja>>>4;if(!M){qa=(d[(ja>>>1&7)+(8656+(ia<<3))>>0]|0)+(ka<<6)|0;c[A+(qa<<2)>>2]=(d[ha+4>>0]<<l)-J;c[I+(qa<<2)>>2]=(d[ha+5>>0]<<l)-J}qa=ka<<8;na=ja&14;c[q+((d[8400+(fa<<4)+na>>0]|qa)<<2)>>2]=(d[ha>>0]<<l)-J;oa=na|1;c[q+((d[8400+(fa<<4)+oa>>0]|qa)<<2)>>2]=(d[ha+1>>0]<<l)-J;c[q+((d[8400+(ga<<4)+na>>0]|qa)<<2)>>2]=(d[ha+2>>0]<<l)-J;c[q+((d[8400+(ga<<4)+oa>>0]|qa)<<2)>>2]=(d[ha+3>>0]<<l)-J;ja=ja+2|0;if(!(ja>>>0<p>>>0)){break}else{ha=ha+m|0}}break};default:{l=77;break a}}break};case 2:{ga=ea+(c[K>>2]<<1)|0;switch(x|0){case 6:case 3:case 0:{ha=c[P>>2]|0;if(C){break b}ia=(ha|0)==0;ja=0;while(1){ka=d[(ja&15)+(8400+(fa<<4))>>0]|ja>>>4<<8;if(!ia){la=0;do{c[(c[f+(la<<2)+34716>>2]|0)+(ka<<2)>>2]=((e[ga+(la<<1)>>1]|0)>>>O<<l)-Q;la=la+1|0}while((la|0)!=(ha|0))}ja=ja+1|0;if((ja|0)==(p|0)){break}else{ga=ga+(N<<1)|0}}break};case 7:{if(C){break b}else{ha=0}while(1){oa=(e[ga>>1]|0)>>>O<<l;na=(e[ga+2>>1]|0)>>>O<<l;ma=((e[ga+4>>1]|0)>>>O<<l)-oa|0;oa=(ma+1>>1)-na+oa|0;qa=d[(ha&15)+(8400+(fa<<4))>>0]|ha>>>4<<8;c[A+(qa<<2)>>2]=0-oa;c[I+(qa<<2)>>2]=ma;c[q+(qa<<2)>>2]=na-Q+(oa>>1);ha=ha+1|0;if((ha|0)==(p|0)){break}else{ga=ga+(N<<1)|0}}break};case 2:{if(C){break b}else{ha=0}while(1){ia=ha>>>4;if(!M){qa=(d[(ha>>>1&7)+(8400+(fa<<4))>>0]|0)+(ia<<7)|0;c[A+(qa<<2)>>2]=(e[ga>>1]<<l)-Q;c[I+(qa<<2)>>2]=(e[ga+4>>1]<<l)-Q}qa=ia<<8;oa=ha&14;c[q+((d[8400+(fa<<4)+oa>>0]|qa)<<2)>>2]=(e[ga+2>>1]<<l)-Q;c[q+((d[(oa|1)+(8400+(fa<<4))>>0]|qa)<<2)>>2]=(e[ga+6>>1]<<l)-Q;ha=ha+2|0;if(!(ha>>>0<p>>>0)){break}else{ga=ga+(N<<1)|0}}break};case 1:{if(C){break b}ja=fa+1|0;ia=fa>>>1;ha=0;while(1){ka=ha>>>4;if(!M){qa=(d[(ha>>>1&7)+(8656+(ia<<3))>>0]|0)+(ka<<6)|0;c[A+(qa<<2)>>2]=(e[ga+8>>1]<<l)-Q;c[I+(qa<<2)>>2]=(e[ga+10>>1]<<l)-Q}qa=ka<<8;na=ha&14;c[q+((d[8400+(fa<<4)+na>>0]|qa)<<2)>>2]=(e[ga>>1]<<l)-Q;oa=na|1;c[q+((d[8400+(fa<<4)+oa>>0]|qa)<<2)>>2]=(e[ga+2>>1]<<l)-Q;c[q+((d[8400+(ja<<4)+na>>0]|qa)<<2)>>2]=(e[ga+4>>1]<<l)-Q;c[q+((d[8400+(ja<<4)+oa>>0]|qa)<<2)>>2]=(e[ga+6>>1]<<l)-Q;ha=ha+2|0;if(!(ha>>>0<p>>>0)){break}else{ga=ga+(N<<1)|0}}break};case 4:{if(D){ha=c[H>>2]|0}else{ha=q}if(C){break b}else{ia=0}while(1){la=(e[ga>>1]|0)>>>O<<l;oa=(e[ga+2>>1]|0)>>>O<<l;na=(e[ga+6>>1]|0)>>>O<<l;ma=((e[ga+4>>1]|0)>>>O<<l)-la|0;la=(ma+1>>1)-oa+la|0;oa=(la>>1)-na+oa|0;qa=d[(ia&15)+(8400+(fa<<4))>>0]|ia>>>4<<8;c[A+(qa<<2)>>2]=la;c[I+(qa<<2)>>2]=0-ma;c[ha+(qa<<2)>>2]=(oa+1>>1)+na;c[q+(qa<<2)>>2]=Q-oa;ia=ia+1|0;if((ia|0)==(p|0)){break}else{ga=ga+(N<<1)|0}}break};default:{l=99;break a}}break};case 4:{ka=ea+(c[K>>2]<<1)|0;if((x|0)==7){if(C){break b}else{ga=0}while(1){qa=b[ka>>1]|0;na=qa>>31;na=(qa&32767^na)-na<<l;qa=b[ka+2>>1]|0;oa=qa>>31;oa=(qa&32767^oa)-oa<<l;qa=b[ka+4>>1]|0;ma=qa>>31;ma=((qa&32767^ma)-ma<<l)-na|0;na=(ma+1>>1)-oa+na|0;qa=d[(ga&15)+(8400+(fa<<4))>>0]|ga>>>4<<8;c[A+(qa<<2)>>2]=0-na;c[I+(qa<<2)>>2]=ma;c[q+(qa<<2)>>2]=(na>>1)+oa;ga=ga+1|0;if((ga|0)==(p|0)){break b}else{ka=ka+(N<<1)|0}}}else if(!((x|0)==6|(x|0)==3|(x|0)==0)){l=121;break a}ga=c[P>>2]|0;if(C){break b}ha=(ga|0)==0;ia=0;while(1){la=d[(ia&15)+(8400+(fa<<4))>>0]|ia>>>4<<8;if(!ha){ja=0;do{oa=b[ka+(ja<<1)>>1]|0;qa=oa>>31;c[(c[f+(ja<<2)+34716>>2]|0)+(la<<2)>>2]=(oa&32767^qa)-qa<<l;ja=ja+1|0}while((ja|0)!=(ga|0))}ia=ia+1|0;if((ia|0)==(p|0)){break}else{ka=ka+(N<<1)|0}}break};case 3:{ga=ea+(c[K>>2]<<1)|0;switch(x|0){case 7:{if(C){break b}else{ha=0}while(1){na=b[ga>>1]>>O<<l;oa=b[ga+2>>1]>>O<<l;ma=(b[ga+4>>1]>>O<<l)-na|0;na=(ma+1>>1)-oa+na|0;qa=d[(ha&15)+(8400+(fa<<4))>>0]|ha>>>4<<8;c[A+(qa<<2)>>2]=0-na;c[I+(qa<<2)>>2]=ma;c[q+(qa<<2)>>2]=(na>>1)+oa;ha=ha+1|0;if((ha|0)==(p|0)){break}else{ga=ga+(N<<1)|0}}break};case 6:case 3:case 0:{ja=c[P>>2]|0;if(C){break b}ia=(ja|0)==0;ha=0;while(1){ka=d[(ha&15)+(8400+(fa<<4))>>0]|ha>>>4<<8;if(!ia){la=0;do{c[(c[f+(la<<2)+34716>>2]|0)+(ka<<2)>>2]=b[ga+(la<<1)>>1]>>O<<l;la=la+1|0}while((la|0)!=(ja|0))}ha=ha+1|0;if((ha|0)==(p|0)){break}else{ga=ga+(N<<1)|0}}break};case 4:{if(D){ia=c[H>>2]|0}else{ia=q}if(C){break b}else{ha=0}while(1){la=b[ga>>1]>>O<<l;oa=b[ga+2>>1]>>O<<l;na=b[ga+6>>1]>>O<<l;ma=(b[ga+4>>1]>>O<<l)-la|0;la=(ma+1>>1)-oa+la|0;oa=(la>>1)-na+oa|0;qa=d[(ha&15)+(8400+(fa<<4))>>0]|ha>>>4<<8;c[A+(qa<<2)>>2]=la;c[I+(qa<<2)>>2]=0-ma;c[ia+(qa<<2)>>2]=(oa+1>>1)+na;c[q+(qa<<2)>>2]=0-oa;ha=ha+1|0;if((ha|0)==(p|0)){break}else{ga=ga+(N<<1)|0}}break};default:{l=112;break a}}break};case 5:{ja=ea+(c[K>>2]<<2)|0;if((x|0)==7){if(C){break b}else{ga=0}while(1){oa=(c[ja>>2]|0)>>>O<<l;na=(c[ja+4>>2]|0)>>>O<<l;ma=((c[ja+8>>2]|0)>>>O<<l)-oa|0;oa=(ma+1>>1)-na+oa|0;qa=d[(ga&15)+(8400+(fa<<4))>>0]|ga>>>4<<8;c[A+(qa<<2)>>2]=0-oa;c[I+(qa<<2)>>2]=ma;c[q+(qa<<2)>>2]=na-S+(oa>>1);ga=ga+1|0;if((ga|0)==(p|0)){break b}else{ja=ja+(R<<2)|0}}}else if(!((x|0)==6|(x|0)==3|(x|0)==0)){l=130;break a}ia=c[P>>2]|0;if(C){break b}ha=(ia|0)==0;ga=0;while(1){ka=d[(ga&15)+(8400+(fa<<4))>>0]|ga>>>4<<8;if(!ha){la=0;do{c[(c[f+(la<<2)+34716>>2]|0)+(ka<<2)>>2]=(c[ja+(la<<2)>>2]|0)>>>O<<l;la=la+1|0}while((la|0)!=(ia|0))}ga=ga+1|0;if((ga|0)==(p|0)){break}else{ja=ja+(R<<2)|0}}break};case 6:{ja=ea+(c[K>>2]<<2)|0;if((x|0)==7){if(C){break b}else{ga=0}while(1){na=c[ja>>2]>>O<<l;oa=c[ja+4>>2]>>O<<l;ma=(c[ja+8>>2]>>O<<l)-na|0;na=(ma+1>>1)-oa+na|0;qa=d[(ga&15)+(8400+(fa<<4))>>0]|ga>>>4<<8;c[A+(qa<<2)>>2]=0-na;c[I+(qa<<2)>>2]=ma;c[q+(qa<<2)>>2]=(na>>1)+oa;ga=ga+1|0;if((ga|0)==(p|0)){break b}else{ja=ja+(R<<2)|0}}}else if(!((x|0)==6|(x|0)==3|(x|0)==0)){l=139;break a}ia=c[P>>2]|0;if(C){break b}ha=(ia|0)==0;ga=0;while(1){ka=d[(ga&15)+(8400+(fa<<4))>>0]|ga>>>4<<8;if(!ha){la=0;do{c[(c[f+(la<<2)+34716>>2]|0)+(ka<<2)>>2]=c[ja+(la<<2)>>2]>>O<<l;la=la+1|0}while((la|0)!=(ia|0))}ga=ga+1|0;if((ga|0)==(p|0)){break}else{ja=ja+(R<<2)|0}}break};case 7:{ha=ea+(c[K>>2]<<2)|0;if((x|0)==6|(x|0)==3|(x|0)==0){ga=c[P>>2]|0;if(C){break b}ja=(ga|0)==0;ia=0;while(1){ka=d[(ia&15)+(8400+(fa<<4))>>0]|ia>>>4<<8;if(!ja){la=0;do{ra=+g[ha+(la<<2)>>2];if(ra==0.0){ma=0}else{ma=(g[k>>2]=ra,c[k>>2]|0);oa=ma>>>23&255;na=ma&8388607;qa=(oa|0)==0;na=qa?na:na|8388608;oa=da+oa+(qa&1)|0;if((oa|0)<2){if((oa|0)<1){na=na>>>(1-oa|0)}oa=na>>>23&1}ma=ma>>31;ma=(((na&8388607)+ca>>U)+(oa<<O)^ma)-ma|0}c[(c[f+(la<<2)+34716>>2]|0)+(ka<<2)>>2]=ma<<l;la=la+1|0}while((la|0)!=(ga|0))}ia=ia+1|0;if((ia|0)==(p|0)){break b}else{ha=ha+(R<<2)|0}}}else if((x|0)!=7){l=172;break a}if(C){break b}else{ga=0}while(1){ra=+g[ha>>2];if(ra==0.0){ia=0}else{ia=(g[k>>2]=ra,c[k>>2]|0);la=ia>>>23&255;ja=ia&8388607;ka=(la|0)==0;ja=ka?ja:ja|8388608;ka=da+la+(ka&1)|0;if((ka|0)<2){if((ka|0)<1){ja=ja>>>(1-ka|0)}ka=ja>>>23&1}ia=ia>>31;ia=(((ja&8388607)+ca>>U)+(ka<<O)^ia)-ia|0}ia=ia<<l;ra=+g[ha+4>>2];if(ra==0.0){ja=0}else{ja=(g[k>>2]=ra,c[k>>2]|0);la=ja>>>23&255;ka=ja&8388607;ma=(la|0)==0;ka=ma?ka:ka|8388608;la=da+la+(ma&1)|0;if((la|0)<2){if((la|0)<1){ka=ka>>>(1-la|0)}la=ka>>>23&1}ja=ja>>31;ja=(((ka&8388607)+ca>>U)+(la<<O)^ja)-ja|0}ja=ja<<l;ra=+g[ha+8>>2];if(ra==0.0){ka=0}else{ka=(g[k>>2]=ra,c[k>>2]|0);na=ka>>>23&255;la=ka&8388607;ma=(na|0)==0;la=ma?la:la|8388608;ma=da+na+(ma&1)|0;if((ma|0)<2){if((ma|0)<1){la=la>>>(1-ma|0)}ma=la>>>23&1}ka=ka>>31;ka=(((la&8388607)+ca>>U)+(ma<<O)^ka)-ka|0}na=(ka<<l)-ia|0;oa=(na+1>>1)-ja+ia|0;qa=d[(ga&15)+(8400+(fa<<4))>>0]|ga>>>4<<8;c[A+(qa<<2)>>2]=0-oa;c[I+(qa<<2)>>2]=na;c[q+(qa<<2)>>2]=(oa>>1)+ja;ga=ga+1|0;if((ga|0)==(p|0)){break}else{ha=ha+(R<<2)|0}}break};case 8:{if(!W){l=175;break a}if(C){break b}else{ha=0;ga=ea}while(1){oa=d[ga>>0]|0;ma=d[ga+1>>0]|0;na=(ma<<3&24|oa>>>5)<<l;oa=(oa&31)<<l;ma=((ma>>>2&31)<<l)-oa|0;oa=(ma+1>>1)-na+oa|0;qa=d[(ha&15)+(8400+(fa<<4))>>0]|ha>>>4<<8;c[A+(qa<<2)>>2]=0-oa;c[I+(qa<<2)>>2]=ma;c[q+(qa<<2)>>2]=na+V+(oa>>1);ha=ha+1|0;if((ha|0)==(p|0)){break}else{ga=ga+m|0}}break};case 10:{if(!W){l=179;break a}if(C){break b}else{ha=0;ga=ea}while(1){oa=d[ga>>0]|0;ma=d[ga+1>>0]|0;na=(ma<<3&56|oa>>>5)<<l;oa=(oa&31)<<X;ma=(ma>>>3<<X)-oa|0;oa=(ma+1>>1)-na+oa|0;qa=d[(ha&15)+(8400+(fa<<4))>>0]|ha>>>4<<8;c[A+(qa<<2)>>2]=0-oa;c[I+(qa<<2)>>2]=ma;c[q+(qa<<2)>>2]=na+Y+(oa>>1);ha=ha+1|0;if((ha|0)==(p|0)){break}else{ga=ga+m|0}}break};case 9:{if(!W){l=183;break a}if(C){break b}else{ha=0;ga=ea}while(1){na=d[ga+1>>0]|0;ma=d[ga+2>>0]|0;oa=(na<<8&768|d[ga>>0])<<l;na=(ma<<6&960|na>>>2)<<l;ma=((d[ga+3>>0]<<4&1008|ma>>>4)<<l)-oa|0;oa=(ma+1>>1)-na+oa|0;qa=d[(ha&15)+(8400+(fa<<4))>>0]|ha>>>4<<8;c[A+(qa<<2)>>2]=0-oa;c[I+(qa<<2)>>2]=ma;c[q+(qa<<2)>>2]=na+Z+(oa>>1);ha=ha+1|0;if((ha|0)==(p|0)){break}else{ga=ga+m|0}}break};case 0:{if(!$){l=187;break a}if(C){break b}else{ga=0}do{c[q+((d[(ga&15)+(8400+(fa<<4))>>0]|ga>>>4<<8)<<2)>>2]=(((d[ea+(ga>>>3)>>0]|0)>>>(ga&7^7))+(c[_>>2]|0)&1)<<l;ga=ga+1|0}while((ga|0)!=(p|0));break};default:{break b}}}while(0)}else{ga=ea+(c[K>>2]<<2)|0;switch(c[u>>2]|0){case 6:case 3:case 0:{ha=c[G>>2]|0;if(!(ha>>>0<17)){l=18;break a}ia=(ha|0)==0;if(!ia){ja=0;do{qa=ja&15;c[y+(qa<<2)>>2]=c[f+(qa<<2)+34716>>2];ja=ja+1|0}while((ja|0)!=(ha|0))}if((c[j>>2]|0)!=0){c[aa>>2]=c[v>>2];c[ba>>2]=c[w>>2]}if(C){break b}else{ja=0}while(1){la=d[(ja&15)+(8400+(fa<<4))>>0]|ja>>>4<<8;if(!ia){ka=0;do{qa=ka&15;c[(c[y+(qa<<2)>>2]|0)+(la<<2)>>2]=c[ga+(qa<<2)>>2];ka=ka+1|0}while((ka|0)!=(ha|0))}ja=ja+1|0;if((ja|0)==(p|0)){break}else{ga=ga+(ha<<2)|0}}break};case 2:{if(C){break b}else{ha=0}while(1){ia=ha>>>4;if(!M){qa=(d[(ha>>>1&7)+(8400+(fa<<4))>>0]|0)+(ia<<7)|0;c[A+(qa<<2)>>2]=c[ga>>2];c[I+(qa<<2)>>2]=c[ga+8>>2]}qa=ia<<8;oa=ha&14;c[q+((d[8400+(fa<<4)+oa>>0]|qa)<<2)>>2]=c[ga+4>>2];c[q+((d[(oa|1)+(8400+(fa<<4))>>0]|qa)<<2)>>2]=c[ga+12>>2];ha=ha+2|0;if(!(ha>>>0<p>>>0)){break}else{ga=ga+16|0}}break};case 1:{if(C){break b}ja=fa+1|0;ia=fa>>>1;ha=0;while(1){ka=ha>>>4;if(!M){qa=(d[(ha>>>1&7)+(8656+(ia<<3))>>0]|0)+(ka<<6)|0;c[A+(qa<<2)>>2]=c[ga+16>>2];c[I+(qa<<2)>>2]=c[ga+20>>2]}qa=ka<<8;na=ha&14;c[q+((d[8400+(fa<<4)+na>>0]|qa)<<2)>>2]=c[ga>>2];oa=na|1;c[q+((d[8400+(fa<<4)+oa>>0]|qa)<<2)>>2]=c[ga+4>>2];c[q+((d[8400+(ja<<4)+na>>0]|qa)<<2)>>2]=c[ga+8>>2];c[q+((d[8400+(ja<<4)+oa>>0]|qa)<<2)>>2]=c[ga+12>>2];ha=ha+2|0;if(!(ha>>>0<p>>>0)){break}else{ga=ga+24|0}}break};default:{l=32;break a}}}}while(0);fa=fa+n|0}while(!(fa>>>0<o>>>0));ea=ea+(c[T>>2]|0)|0}switch(l|0){case 18:{pa(1320,1248,1856,1336);break};case 32:{pa(1352,1248,1899,1336);break};case 41:{pa(1360,1248,1909,1336);break};case 45:{pa(1320,1248,1928,1336);break};case 77:{pa(1352,1248,2003,1336);break};case 99:{pa(1352,1248,2086,1336);break};case 112:{pa(1352,1248,2140,1336);break};case 121:{pa(1352,1248,2178,1336);break};case 130:{pa(1352,1248,2215,1336);break};case 139:{pa(1352,1248,2251,1336);break};case 172:{pa(1352,1248,2288,1336);break};case 175:{pa(1392,1248,2296,1336);break};case 179:{pa(1392,1248,2313,1336);break};case 183:{pa(1392,1248,2330,1336);break};case 187:{pa(1408,1248,2345,1336);break};case 191:{Gb(f);if((c[j>>2]|0)!=0){Fb(f)}if((a[f+112>>0]|0)==3?(Hb(f)|0)!=0:0){qa=-1;i=h;return qa|0}qa=0;i=h;return qa|0}}return 0}function Jb(a){a=a|0;var b=0,d=0,e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0;b=i;fe(a);ee(a,a+4|0,a+8|0,a+12|0);g=a+60|0;h=a+56|0;j=a+52|0;d=a+48|0;k=c[g>>2]|0;l=c[h>>2]|0;e=(c[d>>2]|0)+k|0;f=l-(c[j>>2]|0)|0;m=f>>1;l=m-l|0;n=(l*3|0)+4>>3;l=l-(((k-(e>>1)+n|0)*3|0)+3>>2)|0;m=l-m|0;l=n+k+((l*3|0)+3>>3)|0;c[g>>2]=l;c[h>>2]=m;c[j>>2]=m+f;c[d>>2]=e-l;d=a+20|0;l=a+16|0;e=a+28|0;j=a+24|0;f=c[e>>2]|0;m=c[j>>2]|0;h=(c[l>>2]|0)-f|0;g=m+(c[d>>2]|0)|0;f=(h+1>>1)+f|0;h=h-((g*3|0)+4>>3)|0;k=((h*3|0)+4>>3)+g|0;m=(g+1>>1)-m-((f*3|0)+4>>3)|0;g=m+(h>>1)|0;f=((m*3|0)+4>>3)+f-(k+1>>1)|0;c[d>>2]=f+k;c[l>>2]=h-g;c[e>>2]=f;c[j>>2]=g;j=a+40|0;g=a+32|0;e=a+44|0;a=a+36|0;f=c[e>>2]|0;l=c[a>>2]|0;h=(c[g>>2]|0)-f|0;d=l+(c[j>>2]|0)|0;f=(h+1>>1)+f|0;h=h-((d*3|0)+4>>3)|0;k=((h*3|0)+4>>3)+d|0;l=(d+1>>1)-l-((f*3|0)+4>>3)|0;d=l+(h>>1)|0;f=((l*3|0)+4>>3)+f-(k+1>>1)|0;c[j>>2]=f+k;c[g>>2]=h-d;c[e>>2]=f;c[a>>2]=d;i=b;return}function Kb(a){a=a|0;var b=0,d=0,e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0;b=i;k=a+768|0;g=a+192|0;p=a+960|0;de(a,k,g,p);o=a+256|0;n=a+512|0;d=a+448|0;l=a+704|0;de(o,n,d,l);m=a+64|0;f=a+832|0;j=a+128|0;r=a+896|0;de(m,f,j,r);t=a+320|0;h=a+576|0;e=a+384|0;s=a+640|0;de(t,h,e,s);ee(a,o,m,t);t=c[s>>2]|0;a=c[r>>2]|0;m=(c[p>>2]|0)+t|0;o=a-(c[l>>2]|0)|0;q=o>>1;a=q-a|0;u=(a*3|0)+4>>3;a=a-(((t-(m>>1)+u|0)*3|0)+3>>2)|0;q=a-q|0;a=u+t+((a*3|0)+3>>3)|0;c[s>>2]=a;c[r>>2]=q;c[l>>2]=q+o;c[p>>2]=m-a;a=c[h>>2]|0;p=c[f>>2]|0;m=(c[k>>2]|0)-a|0;l=p+(c[n>>2]|0)|0;a=(m+1>>1)+a|0;m=m-((l*3|0)+4>>3)|0;o=((m*3|0)+4>>3)+l|0;p=(l+1>>1)-p-((a*3|0)+4>>3)|0;l=p+(m>>1)|0;a=((p*3|0)+4>>3)+a-(o+1>>1)|0;c[n>>2]=a+o;c[k>>2]=m-l;c[h>>2]=a;c[f>>2]=l;f=c[e>>2]|0;l=c[d>>2]|0;h=(c[g>>2]|0)-f|0;a=l+(c[j>>2]|0)|0;f=(h+1>>1)+f|0;h=h-((a*3|0)+4>>3)|0;k=((h*3|0)+4>>3)+a|0;l=(a+1>>1)-l-((f*3|0)+4>>3)|0;a=l+(h>>1)|0;f=((l*3|0)+4>>3)+f-(k+1>>1)|0;c[j>>2]=f+k;c[g>>2]=h-a;c[e>>2]=f;c[d>>2]=a;i=b;return}function Lb(a,b,d){a=a|0;b=b|0;d=d|0;var e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0;e=i;f=72-d|0;s=a+(f<<2)|0;d=64-d|0;q=b+(d<<2)|0;t=a+48|0;r=b+16|0;p=c[r>>2]|0;o=c[q>>2]|0;m=o+(c[t>>2]|0)|0;n=(c[s>>2]|0)-p|0;o=(m-n>>1)-o|0;p=(n>>1)+p|0;c[t>>2]=m-((p*3|0)+4>>3);c[s>>2]=o+n;c[r>>2]=o;c[q>>2]=p;p=a+52|0;o=a+(f+1<<2)|0;n=b+20|0;m=b+(d+1<<2)|0;l=c[n>>2]|0;k=c[m>>2]|0;h=k+(c[p>>2]|0)|0;j=(c[o>>2]|0)-l|0;k=(h-j>>1)-k|0;l=(j>>1)+l|0;c[p>>2]=h-((l*3|0)+4>>3);c[o>>2]=k+j;c[n>>2]=k;c[m>>2]=l;l=a+56|0;k=a+(f+2<<2)|0;j=b+24|0;h=b+(d+2<<2)|0;g=c[j>>2]|0;x=c[h>>2]|0;u=x+(c[l>>2]|0)|0;v=(c[k>>2]|0)-g|0;x=(u-v>>1)-x|0;g=(v>>1)+g|0;c[l>>2]=u-((g*3|0)+4>>3);c[k>>2]=x+v;c[j>>2]=x;c[h>>2]=g;g=a+60|0;f=a+(f+3<<2)|0;a=b+28|0;d=b+(d+3<<2)|0;x=c[a>>2]|0;b=c[d>>2]|0;v=b+(c[g>>2]|0)|0;u=(c[f>>2]|0)-x|0;b=(v-u>>1)-b|0;x=(u>>1)+x|0;c[g>>2]=v-((x*3|0)+4>>3);c[f>>2]=b+u;c[a>>2]=b;c[d>>2]=x;x=c[t>>2]|0;b=(c[q>>2]|0)-(x>>7)+(x>>10)-(x*3>>4)|0;x=x-(b*3>>3)|0;b=(x>>1)-b|0;c[t>>2]=x-b;c[q>>2]=b;b=c[p>>2]|0;x=(c[m>>2]|0)-(b>>7)+(b>>10)-(b*3>>4)|0;b=b-(x*3>>3)|0;x=(b>>1)-x|0;c[p>>2]=b-x;c[m>>2]=x;x=c[l>>2]|0;b=(c[h>>2]|0)-(x>>7)+(x>>10)-(x*3>>4)|0;x=x-(b*3>>3)|0;b=(x>>1)-b|0;c[l>>2]=x-b;c[h>>2]=b;b=c[g>>2]|0;x=(c[d>>2]|0)-(b>>7)+(b>>10)-(b*3>>4)|0;b=b-(x*3>>3)|0;x=(b>>1)-x|0;c[g>>2]=b-x;c[d>>2]=x;x=c[j>>2]|0;b=(c[a>>2]|0)-(x+1>>1)|0;c[a>>2]=b;c[j>>2]=(b+1>>1)+x;x=c[r>>2]|0;b=(c[n>>2]|0)-(x+1>>1)|0;c[n>>2]=b;c[r>>2]=(b+1>>1)+x;x=c[o>>2]|0;b=(c[f>>2]|0)-(x+1>>1)|0;c[f>>2]=b;c[o>>2]=(b+1>>1)+x;x=c[s>>2]|0;b=(c[k>>2]|0)-(x+1>>1)|0;c[k>>2]=b;c[s>>2]=(b+1>>1)+x;x=c[q>>2]|0;b=c[m>>2]|0;u=(c[d>>2]|0)+x|0;v=(c[h>>2]|0)-b|0;w=v>>1;b=w+b|0;y=(b*3|0)+4>>3;b=b-(((x-(u>>1)+y|0)*3|0)+2>>2)|0;w=b-w|0;b=y+x+((b*3|0)+6>>3)|0;c[q>>2]=b;c[m>>2]=w;c[h>>2]=w+v;c[d>>2]=u-b;de(t,s,r,q);de(p,o,n,m);de(l,k,j,h);de(g,f,a,d);i=e;return}function Mb(a,b){a=a|0;b=b|0;var d=0,e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0;d=i;s=a+ -384|0;q=a+384|0;r=b+ -448|0;p=b+320|0;o=c[r>>2]|0;m=c[p>>2]|0;l=m+(c[s>>2]|0)|0;n=(c[q>>2]|0)-o|0;m=(l-n>>1)-m|0;o=(n>>1)+o|0;c[s>>2]=l-((o*3|0)+4>>3);c[q>>2]=m+n;c[r>>2]=m;c[p>>2]=o;o=a+ -128|0;m=a+128|0;n=b+ -192|0;l=b+64|0;k=c[n>>2]|0;h=c[l>>2]|0;g=h+(c[o>>2]|0)|0;j=(c[m>>2]|0)-k|0;h=(g-j>>1)-h|0;k=(j>>1)+k|0;c[o>>2]=g-((k*3|0)+4>>3);c[m>>2]=h+j;c[n>>2]=h;c[l>>2]=k;k=a+ -320|0;h=a+448|0;j=b+ -512|0;g=b+256|0;f=c[j>>2]|0;e=c[g>>2]|0;t=e+(c[k>>2]|0)|0;x=(c[h>>2]|0)-f|0;e=(t-x>>1)-e|0;f=(x>>1)+f|0;c[k>>2]=t-((f*3|0)+4>>3);c[h>>2]=e+x;c[j>>2]=e;c[g>>2]=f;f=a+ -64|0;a=a+192|0;e=b+ -256|0;x=c[e>>2]|0;t=c[b>>2]|0;v=t+(c[f>>2]|0)|0;u=(c[a>>2]|0)-x|0;t=(v-u>>1)-t|0;x=(u>>1)+x|0;c[f>>2]=v-((x*3|0)+4>>3);c[a>>2]=t+u;c[e>>2]=t;c[b>>2]=x;x=c[s>>2]|0;t=(c[p>>2]|0)-(x>>7)+(x>>10)-(x*3>>4)|0;x=x-(t*3>>3)|0;t=(x>>1)-t|0;c[s>>2]=x-t;c[p>>2]=t;t=c[o>>2]|0;x=(c[l>>2]|0)-(t>>7)+(t>>10)-(t*3>>4)|0;t=t-(x*3>>3)|0;x=(t>>1)-x|0;c[o>>2]=t-x;c[l>>2]=x;x=c[k>>2]|0;t=(c[g>>2]|0)-(x>>7)+(x>>10)-(x*3>>4)|0;x=x-(t*3>>3)|0;t=(x>>1)-t|0;c[k>>2]=x-t;c[g>>2]=t;t=c[f>>2]|0;x=(c[b>>2]|0)-(t>>7)+(t>>10)-(t*3>>4)|0;t=t-(x*3>>3)|0;x=(t>>1)-x|0;c[f>>2]=t-x;c[b>>2]=x;x=c[n>>2]|0;t=(c[r>>2]|0)-(x+1>>1)|0;c[r>>2]=t;c[n>>2]=(t+1>>1)+x;x=c[e>>2]|0;t=(c[j>>2]|0)-(x+1>>1)|0;c[j>>2]=t;c[e>>2]=(t+1>>1)+x;x=c[h>>2]|0;t=(c[q>>2]|0)-(x+1>>1)|0;c[q>>2]=t;c[h>>2]=(t+1>>1)+x;x=c[a>>2]|0;t=(c[m>>2]|0)-(x+1>>1)|0;c[m>>2]=t;c[a>>2]=(t+1>>1)+x;x=c[b>>2]|0;t=c[g>>2]|0;u=(c[p>>2]|0)+x|0;v=(c[l>>2]|0)-t|0;w=v>>1;t=w+t|0;y=(t*3|0)+4>>3;t=t-(((x-(u>>1)+y|0)*3|0)+2>>2)|0;w=t-w|0;t=y+x+((t*3|0)+6>>3)|0;c[b>>2]=t;c[g>>2]=w;c[l>>2]=w+v;c[p>>2]=u-t;de(s,r,q,p);de(o,n,m,l);de(k,j,h,g);de(f,e,a,b);i=d;return}function Nb(a){a=a|0;var b=0,d=0,e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0;k=i;n=c[a+100>>2]|0;I=c[a+34128>>2]|0;s=c[a+34328>>2]|0;j=(s|0)==0;l=c[a+34332>>2]|0;h=(s|0)==(l|0);t=c[a+34324>>2]|0;e=(t|0)==0;g=(t|0)==(c[a+34336>>2]|0);f=j|h;p=e|g;d=j|e;m=(s|0)==1;l=(s|0)==(l+ -1|0);q=(I|0)==1;o=(I|0)==2;if((I+ -1|0)>>>0<2){r=1}else{r=c[a+34156>>2]|0}a:do{if((c[a+32916>>2]|0)!=0){if(!j){x=c[a+34356>>2]|0;z=a+34372|0;c[z>>2]=0;w=a+34368|0;c[w>>2]=0;y=a+34356|0;if((x|0)!=0){v=c[a+16520>>2]|0;if(!(x>>>0>v>>>0)?(s+ -1|0)==(c[a+(x<<2)+16524>>2]|0):0){c[z>>2]=1}}else{b=7}}else{c[a+34360>>2]=0;c[a+34356>>2]=0;c[a+34372>>2]=0;w=a+34368|0;c[w>>2]=0;y=a+34356|0;b=7}if((b|0)==7){x=0;v=c[a+16520>>2]|0}if(x>>>0<v>>>0?(u=x+1|0,(s|0)==(c[a+(u<<2)+16524>>2]|0)):0){c[a+34360>>2]=1;c[y>>2]=u}else{c[a+34360>>2]=0;u=x}if(u>>>0<v>>>0?(s+1|0)==(c[a+(u+1<<2)+16524>>2]|0):0){c[w>>2]=1}if(e){c[a+34364>>2]=0;c[a+34352>>2]=0;t=0;break}v=(c[a+34348>>2]|0)==(t|0);if(!v){u=a+34352|0;w=c[u>>2]|0;do{if(w>>>0<(c[a+132>>2]|0)>>>0){w=w+1|0;if((t|0)!=(c[a+(w<<2)+136>>2]|0)){if(v){break a}else{break}}else{c[a+34364>>2]=1;c[u>>2]=w;break a}}}while(0);c[a+34364>>2]=0}}else{I=a+34360|0;c[I+0>>2]=0;c[I+4>>2]=0;c[I+8>>2]=0;c[I+12>>2]=0}}while(0);c[a+34344>>2]=s;c[a+34348>>2]=t;if((r|0)>0){C=(n|0)==0;D=(n|0)==2;z=a+34360|0;t=a+34364|0;E=a+34140|0;B=j?0:-64;A=h?0:192;s=j?48:-16;y=h?48:240;x=h|g;v=h?-16:240;w=0;do{u=c[a+(w<<2)+34652>>2]|0;F=c[a+(w<<2)+34716>>2]|0;do{if(!C){if(!(!e?(c[t>>2]|0)==0:0)){b=34}do{if((b|0)==34){b=0;if(!(!j?(c[z>>2]|0)==0:0)){K=F+4|0;G=F+8|0;I=F+12|0;J=c[G>>2]|0;H=c[I>>2]|0;M=H+(c[F>>2]|0)|0;L=J+(c[K>>2]|0)|0;J=J-(L+1>>1)|0;H=H-(M+1>>1)-(J+1>>1)|0;J=(H+1>>1)+J|0;M=H+M|0;H=(M>>1)-H|0;M=M-((H*3|0)+4>>3)|0;H=H-(M>>7)+(M>>10)-(M*3>>4)|0;M=M-(H*3>>3)|0;H=(M>>1)-H|0;M=M-H|0;L=J+L|0;J=(L>>1)-J|0;L=L-((J*3|0)+4>>3)|0;J=J-(L>>7)+(L>>10)-(L*3>>4)|0;L=L-(J*3>>3)|0;J=(L>>1)-J|0;L=L-J|0;H=(M+1>>1)+H|0;J=(L+1>>1)+J|0;c[F>>2]=M-H;c[K>>2]=L-J;c[G>>2]=J;c[I>>2]=H}if(!e?(c[t>>2]|0)==0:0){break}if(!h?(c[z>>2]|0)==0:0){break}G=F+ -236|0;I=F+ -240|0;K=F+ -228|0;M=F+ -232|0;J=c[K>>2]|0;L=c[M>>2]|0;N=L+(c[G>>2]|0)|0;H=J+(c[I>>2]|0)|0;J=J-(H+1>>1)|0;L=L-(N+1>>1)-(J+1>>1)|0;J=(L+1>>1)+J|0;N=L+N|0;L=(N>>1)-L|0;N=N-((L*3|0)+4>>3)|0;L=L-(N>>7)+(N>>10)-(N*3>>4)|0;N=N-(L*3>>3)|0;L=(N>>1)-L|0;N=N-L|0;H=J+H|0;J=(H>>1)-J|0;H=H-((J*3|0)+4>>3)|0;J=J-(H>>7)+(H>>10)-(H*3>>4)|0;H=H-(J*3>>3)|0;J=(H>>1)-J|0;H=H-J|0;L=(N+1>>1)+L|0;J=(H+1>>1)+J|0;c[G>>2]=N-L;c[I>>2]=H-J;c[K>>2]=J;c[M>>2]=L}}while(0);if(!(!g?(c[t>>2]|0)==0:0)){b=44}do{if((b|0)==44){b=0;if(!(!j?(c[z>>2]|0)==0:0)){H=u+232|0;J=u+236|0;L=u+224|0;N=u+228|0;K=c[L>>2]|0;M=c[N>>2]|0;G=M+(c[H>>2]|0)|0;I=K+(c[J>>2]|0)|0;K=K-(I+1>>1)|0;M=M-(G+1>>1)-(K+1>>1)|0;K=(M+1>>1)+K|0;G=M+G|0;M=(G>>1)-M|0;G=G-((M*3|0)+4>>3)|0;M=M-(G>>7)+(G>>10)-(G*3>>4)|0;G=G-(M*3>>3)|0;M=(G>>1)-M|0;G=G-M|0;I=K+I|0;K=(I>>1)-K|0;I=I-((K*3|0)+4>>3)|0;K=K-(I>>7)+(I>>10)-(I*3>>4)|0;I=I-(K*3>>3)|0;K=(I>>1)-K|0;I=I-K|0;M=(G+1>>1)+M|0;K=(I+1>>1)+K|0;c[H>>2]=G-M;c[J>>2]=I-K;c[L>>2]=K;c[N>>2]=M}if(!g?(c[t>>2]|0)==0:0){break}if(!h?(c[z>>2]|0)==0:0){break}H=u+ -4|0;J=u+ -8|0;L=u+ -12|0;N=u+ -16|0;K=c[L>>2]|0;M=c[N>>2]|0;G=M+(c[H>>2]|0)|0;I=K+(c[J>>2]|0)|0;K=K-(I+1>>1)|0;M=M-(G+1>>1)-(K+1>>1)|0;K=(M+1>>1)+K|0;G=M+G|0;M=(G>>1)-M|0;G=G-((M*3|0)+4>>3)|0;M=M-(G>>7)+(G>>10)-(G*3>>4)|0;G=G-(M*3>>3)|0;M=(G>>1)-M|0;G=G-M|0;I=K+I|0;K=(I>>1)-K|0;I=I-((K*3|0)+4>>3)|0;K=K-(I>>7)+(I>>10)-(I*3>>4)|0;I=I-(K*3>>3)|0;K=(I>>1)-K|0;I=I-K|0;M=(G+1>>1)+M|0;K=(I+1>>1)+K|0;c[H>>2]=G-M;c[J>>2]=I-K;c[L>>2]=K;c[N>>2]=M}}while(0);if(!x){if(!e?(c[t>>2]|0)==0:0){if(j){G=0}else{G=(c[z>>2]|0)!=0?0:-64}do{Lb(u+((G|48)<<2)|0,F+(G<<2)|0,0);G=G+64|0}while((G|0)<192)}else{if(j){G=0}else{G=(c[z>>2]|0)!=0?0:-64}do{I=F+((G|5)<<2)|0;M=F+((G|4)<<2)|0;N=G;G=G+64|0;L=F+(G<<2)|0;H=F+(N+65<<2)|0;K=c[L>>2]|0;J=c[H>>2]|0;P=J+(c[I>>2]|0)|0;O=K+(c[M>>2]|0)|0;K=K-(O+1>>1)|0;J=J-(P+1>>1)-(K+1>>1)|0;K=(J+1>>1)+K|0;P=J+P|0;J=(P>>1)-J|0;P=P-((J*3|0)+4>>3)|0;J=J-(P>>7)+(P>>10)-(P*3>>4)|0;P=P-(J*3>>3)|0;J=(P>>1)-J|0;P=P-J|0;O=K+O|0;K=(O>>1)-K|0;O=O-((K*3|0)+4>>3)|0;K=K-(O>>7)+(O>>10)-(O*3>>4)|0;O=O-(K*3>>3)|0;K=(O>>1)-K|0;O=O-K|0;J=(P+1>>1)+J|0;K=(O+1>>1)+K|0;c[I>>2]=P-J;c[M>>2]=O-K;c[L>>2]=K;c[H>>2]=J;H=F+((N|7)<<2)|0;J=F+((N|6)<<2)|0;L=F+(N+66<<2)|0;N=F+(N+67<<2)|0;K=c[L>>2]|0;M=c[N>>2]|0;O=M+(c[H>>2]|0)|0;I=K+(c[J>>2]|0)|0;K=K-(I+1>>1)|0;M=M-(O+1>>1)-(K+1>>1)|0;K=(M+1>>1)+K|0;O=M+O|0;M=(O>>1)-M|0;O=O-((M*3|0)+4>>3)|0;M=M-(O>>7)+(O>>10)-(O*3>>4)|0;O=O-(M*3>>3)|0;M=(O>>1)-M|0;O=O-M|0;I=K+I|0;K=(I>>1)-K|0;I=I-((K*3|0)+4>>3)|0;K=K-(I>>7)+(I>>10)-(I*3>>4)|0;I=I-(K*3>>3)|0;K=(I>>1)-K|0;I=I-K|0;M=(O+1>>1)+M|0;K=(I+1>>1)+K|0;c[H>>2]=O-M;c[J>>2]=I-K;c[L>>2]=K;c[N>>2]=M}while((G|0)<192)}if(!j?(c[z>>2]|0)==0:0){O=F+ -192|0;Lb(F+ -256|0,O,0);P=F+ -128|0;Lb(O,P,0);Lb(P,F+ -64|0,0)}else{if(!e?(c[t>>2]|0)==0:0){P=u+232|0;G=u+224|0;K=F+8|0;O=c[F>>2]|0;M=c[K>>2]|0;J=M+(c[P>>2]|0)|0;N=O+(c[G>>2]|0)|0;O=O-(N+1>>1)|0;M=M-(J+1>>1)-(O+1>>1)|0;O=(M+1>>1)+O|0;J=M+J|0;M=(J>>1)-M|0;J=J-((M*3|0)+4>>3)|0;M=M-(J>>7)+(J>>10)-(J*3>>4)|0;J=J-(M*3>>3)|0;M=(J>>1)-M|0;J=J-M|0;N=O+N|0;O=(N>>1)-O|0;N=N-((O*3|0)+4>>3)|0;O=O-(N>>7)+(N>>10)-(N*3>>4)|0;N=N-(O*3>>3)|0;O=(N>>1)-O|0;N=N-O|0;M=(J+1>>1)+M|0;O=(N+1>>1)+O|0;c[P>>2]=J-M;c[G>>2]=N-O;c[F>>2]=O;c[K>>2]=M;K=u+236|0;M=u+228|0;O=F+4|0;G=F+12|0;N=c[O>>2]|0;P=c[G>>2]|0;J=P+(c[K>>2]|0)|0;L=N+(c[M>>2]|0)|0;N=N-(L+1>>1)|0;P=P-(J+1>>1)-(N+1>>1)|0;N=(P+1>>1)+N|0;J=P+J|0;P=(J>>1)-P|0;J=J-((P*3|0)+4>>3)|0;P=P-(J>>7)+(J>>10)-(J*3>>4)|0;J=J-(P*3>>3)|0;P=(J>>1)-P|0;J=J-P|0;L=N+L|0;N=(L>>1)-N|0;L=L-((N*3|0)+4>>3)|0;N=N-(L>>7)+(L>>10)-(L*3>>4)|0;L=L-(N*3>>3)|0;N=(L>>1)-N|0;L=L-N|0;P=(J+1>>1)+P|0;N=(L+1>>1)+N|0;c[K>>2]=J-P;c[M>>2]=L-N;c[O>>2]=N;c[G>>2]=P;G=-64}else{G=-64}do{I=F+(G+74<<2)|0;M=F+(G+72<<2)|0;N=F+(G+80<<2)|0;J=F+(G+82<<2)|0;P=c[N>>2]|0;L=c[J>>2]|0;K=L+(c[I>>2]|0)|0;O=P+(c[M>>2]|0)|0;P=P-(O+1>>1)|0;L=L-(K+1>>1)-(P+1>>1)|0;P=(L+1>>1)+P|0;K=L+K|0;L=(K>>1)-L|0;K=K-((L*3|0)+4>>3)|0;L=L-(K>>7)+(K>>10)-(K*3>>4)|0;K=K-(L*3>>3)|0;L=(K>>1)-L|0;K=K-L|0;O=P+O|0;P=(O>>1)-P|0;O=O-((P*3|0)+4>>3)|0;P=P-(O>>7)+(O>>10)-(O*3>>4)|0;O=O-(P*3>>3)|0;P=(O>>1)-P|0;O=O-P|0;L=(K+1>>1)+L|0;P=(O+1>>1)+P|0;c[I>>2]=K-L;c[M>>2]=O-P;c[N>>2]=P;c[J>>2]=L;J=F+(G+75<<2)|0;L=F+(G+73<<2)|0;N=F+(G+81<<2)|0;P=F+(G+83<<2)|0;M=c[N>>2]|0;O=c[P>>2]|0;I=O+(c[J>>2]|0)|0;K=M+(c[L>>2]|0)|0;M=M-(K+1>>1)|0;O=O-(I+1>>1)-(M+1>>1)|0;M=(O+1>>1)+M|0;I=O+I|0;O=(I>>1)-O|0;I=I-((O*3|0)+4>>3)|0;O=O-(I>>7)+(I>>10)-(I*3>>4)|0;I=I-(O*3>>3)|0;O=(I>>1)-O|0;I=I-O|0;K=M+K|0;M=(K>>1)-M|0;K=K-((M*3|0)+4>>3)|0;M=M-(K>>7)+(K>>10)-(K*3>>4)|0;K=K-(M*3>>3)|0;M=(K>>1)-M|0;K=K-M|0;O=(I+1>>1)+O|0;M=(K+1>>1)+M|0;c[J>>2]=I-O;c[L>>2]=K-M;c[N>>2]=M;c[P>>2]=O;G=G+16|0}while((G|0)<-16)}O=F+64|0;Lb(F,O,0);P=F+128|0;Lb(O,P,0);Lb(P,F+192|0,0);P=F+320|0;Lb(F+256|0,P,0);O=F+384|0;Lb(P,O,0);Lb(O,F+448|0,0);O=F+576|0;Lb(F+512|0,O,0);P=F+640|0;Lb(O,P,0);Lb(P,F+704|0,0)}if(!(!g?(c[t>>2]|0)==0:0)){if(j){G=48}else{G=(c[z>>2]|0)!=0?48:-16}if((G|0)<(v|0)){do{I=u+((G|15)<<2)|0;M=u+((G|14)<<2)|0;N=u+(G+74<<2)|0;J=u+(G+75<<2)|0;P=c[N>>2]|0;L=c[J>>2]|0;K=L+(c[I>>2]|0)|0;O=P+(c[M>>2]|0)|0;P=P-(O+1>>1)|0;L=L-(K+1>>1)-(P+1>>1)|0;P=(L+1>>1)+P|0;K=L+K|0;L=(K>>1)-L|0;K=K-((L*3|0)+4>>3)|0;L=L-(K>>7)+(K>>10)-(K*3>>4)|0;K=K-(L*3>>3)|0;L=(K>>1)-L|0;K=K-L|0;O=P+O|0;P=(O>>1)-P|0;O=O-((P*3|0)+4>>3)|0;P=P-(O>>7)+(O>>10)-(O*3>>4)|0;O=O-(P*3>>3)|0;P=(O>>1)-P|0;O=O-P|0;L=(K+1>>1)+L|0;P=(O+1>>1)+P|0;c[I>>2]=K-L;c[M>>2]=O-P;c[N>>2]=P;c[J>>2]=L;J=u+((G|13)<<2)|0;L=u+((G|12)<<2)|0;N=u+(G+72<<2)|0;P=u+(G+73<<2)|0;M=c[N>>2]|0;O=c[P>>2]|0;I=O+(c[J>>2]|0)|0;K=M+(c[L>>2]|0)|0;M=M-(K+1>>1)|0;O=O-(I+1>>1)-(M+1>>1)|0;M=(O+1>>1)+M|0;I=O+I|0;O=(I>>1)-O|0;I=I-((O*3|0)+4>>3)|0;O=O-(I>>7)+(I>>10)-(I*3>>4)|0;I=I-(O*3>>3)|0;O=(I>>1)-O|0;I=I-O|0;K=M+K|0;M=(K>>1)-M|0;K=K-((M*3|0)+4>>3)|0;M=M-(K>>7)+(K>>10)-(K*3>>4)|0;K=K-(M*3>>3)|0;M=(K>>1)-M|0;K=K-M|0;O=(I+1>>1)+O|0;M=(K+1>>1)+M|0;c[J>>2]=I-O;c[L>>2]=K-M;c[N>>2]=M;c[P>>2]=O;G=G+64|0}while((G|0)<(v|0))}}if(h){if(g){break}}else{if((c[z>>2]|0)==0|g){break}}if(!e?(c[t>>2]|0)==0:0){J=u+ -4|0;N=u+ -12|0;O=F+ -236|0;K=F+ -228|0;G=c[O>>2]|0;M=c[K>>2]|0;L=M+(c[J>>2]|0)|0;P=G+(c[N>>2]|0)|0;G=G-(P+1>>1)|0;M=M-(L+1>>1)-(G+1>>1)|0;G=(M+1>>1)+G|0;L=M+L|0;M=(L>>1)-M|0;L=L-((M*3|0)+4>>3)|0;M=M-(L>>7)+(L>>10)-(L*3>>4)|0;L=L-(M*3>>3)|0;M=(L>>1)-M|0;L=L-M|0;P=G+P|0;G=(P>>1)-G|0;P=P-((G*3|0)+4>>3)|0;G=G-(P>>7)+(P>>10)-(P*3>>4)|0;P=P-(G*3>>3)|0;G=(P>>1)-G|0;P=P-G|0;M=(L+1>>1)+M|0;G=(P+1>>1)+G|0;c[J>>2]=L-M;c[N>>2]=P-G;c[O>>2]=G;c[K>>2]=M;K=u+ -8|0;M=u+ -16|0;O=F+ -240|0;G=F+ -232|0;N=c[O>>2]|0;P=c[G>>2]|0;J=P+(c[K>>2]|0)|0;L=N+(c[M>>2]|0)|0;N=N-(L+1>>1)|0;P=P-(J+1>>1)-(N+1>>1)|0;N=(P+1>>1)+N|0;J=P+J|0;P=(J>>1)-P|0;J=J-((P*3|0)+4>>3)|0;P=P-(J>>7)+(J>>10)-(J*3>>4)|0;J=J-(P*3>>3)|0;P=(J>>1)-P|0;J=J-P|0;L=N+L|0;N=(L>>1)-N|0;L=L-((N*3|0)+4>>3)|0;N=N-(L>>7)+(L>>10)-(L*3>>4)|0;L=L-(N*3>>3)|0;N=(L>>1)-N|0;L=L-N|0;P=(J+1>>1)+P|0;N=(L+1>>1)+N|0;c[K>>2]=J-P;c[M>>2]=L-N;c[O>>2]=N;c[G>>2]=P;G=-64}else{G=-64}do{I=F+((G|15)<<2)|0;M=F+((G|13)<<2)|0;N=F+(G+21<<2)|0;J=F+(G+23<<2)|0;P=c[N>>2]|0;L=c[J>>2]|0;K=L+(c[I>>2]|0)|0;O=P+(c[M>>2]|0)|0;P=P-(O+1>>1)|0;L=L-(K+1>>1)-(P+1>>1)|0;P=(L+1>>1)+P|0;K=L+K|0;L=(K>>1)-L|0;K=K-((L*3|0)+4>>3)|0;L=L-(K>>7)+(K>>10)-(K*3>>4)|0;K=K-(L*3>>3)|0;L=(K>>1)-L|0;K=K-L|0;O=P+O|0;P=(O>>1)-P|0;O=O-((P*3|0)+4>>3)|0;P=P-(O>>7)+(O>>10)-(O*3>>4)|0;O=O-(P*3>>3)|0;P=(O>>1)-P|0;O=O-P|0;L=(K+1>>1)+L|0;P=(O+1>>1)+P|0;c[I>>2]=K-L;c[M>>2]=O-P;c[N>>2]=P;c[J>>2]=L;J=F+((G|14)<<2)|0;L=F+((G|12)<<2)|0;N=F+(G+20<<2)|0;P=F+(G+22<<2)|0;M=c[N>>2]|0;O=c[P>>2]|0;I=O+(c[J>>2]|0)|0;K=M+(c[L>>2]|0)|0;M=M-(K+1>>1)|0;O=O-(I+1>>1)-(M+1>>1)|0;M=(O+1>>1)+M|0;I=O+I|0;O=(I>>1)-O|0;I=I-((O*3|0)+4>>3)|0;O=O-(I>>7)+(I>>10)-(I*3>>4)|0;I=I-(O*3>>3)|0;O=(I>>1)-O|0;I=I-O|0;K=M+K|0;M=(K>>1)-M|0;K=K-((M*3|0)+4>>3)|0;M=M-(K>>7)+(K>>10)-(K*3>>4)|0;K=K-(M*3>>3)|0;M=(K>>1)-M|0;K=K-M|0;O=(I+1>>1)+O|0;M=(K+1>>1)+M|0;c[J>>2]=I-O;c[L>>2]=K-M;c[N>>2]=M;c[P>>2]=O;G=G+16|0}while((G|0)<-16)}}while(0);if(!(e|(s|0)>=(y|0))){G=s;do{Jb(u+(G<<2)|0);G=G+64|0}while((G|0)<(y|0))}if(!(g|(B|0)>=(A|0))){G=B;do{Jb(F+(G<<2)|0);Jb(F+((G|16)<<2)|0);Jb(F+((G|32)<<2)|0);G=G+64|0}while((G|0)<(A|0))}do{if(D){if(!(!e?(c[t>>2]|0)==0:0)){b=89}do{if((b|0)==89){b=0;if(!(!j?(c[z>>2]|0)==0:0)){L=F+256|0;N=F+64|0;P=F+320|0;M=c[N>>2]|0;O=c[P>>2]|0;J=O+(c[F>>2]|0)|0;K=M+(c[L>>2]|0)|0;M=M-(K+1>>1)|0;O=O-(J+1>>1)-(M+1>>1)|0;M=(O+1>>1)+M|0;J=O+J|0;O=(J>>1)-O|0;J=J-((O*3|0)+4>>3)|0;O=O-(J>>7)+(J>>10)-(J*3>>4)|0;J=J-(O*3>>3)|0;O=(J>>1)-O|0;J=J-O|0;K=M+K|0;M=(K>>1)-M|0;K=K-((M*3|0)+4>>3)|0;M=M-(K>>7)+(K>>10)-(K*3>>4)|0;K=K-(M*3>>3)|0;M=(K>>1)-M|0;K=K-M|0;O=(J+1>>1)+O|0;M=(K+1>>1)+M|0;c[F>>2]=J-O;c[L>>2]=K-M;c[N>>2]=M;c[P>>2]=O}if(!e?(c[t>>2]|0)==0:0){break}if(!h?(c[z>>2]|0)==0:0){break}J=F+ -512|0;L=F+ -256|0;N=F+ -448|0;P=F+ -192|0;M=c[N>>2]|0;O=c[P>>2]|0;I=O+(c[J>>2]|0)|0;K=M+(c[L>>2]|0)|0;M=M-(K+1>>1)|0;O=O-(I+1>>1)-(M+1>>1)|0;M=(O+1>>1)+M|0;I=O+I|0;O=(I>>1)-O|0;I=I-((O*3|0)+4>>3)|0;O=O-(I>>7)+(I>>10)-(I*3>>4)|0;I=I-(O*3>>3)|0;O=(I>>1)-O|0;I=I-O|0;K=M+K|0;M=(K>>1)-M|0;K=K-((M*3|0)+4>>3)|0;M=M-(K>>7)+(K>>10)-(K*3>>4)|0;K=K-(M*3>>3)|0;M=(K>>1)-M|0;K=K-M|0;O=(I+1>>1)+O|0;M=(K+1>>1)+M|0;c[J>>2]=I-O;c[L>>2]=K-M;c[N>>2]=M;c[P>>2]=O}}while(0);if(!(!g?(c[t>>2]|0)==0:0)){b=99}do{if((b|0)==99){b=0;if(!(!j?(c[z>>2]|0)==0:0)){J=u+128|0;L=u+384|0;N=u+192|0;P=u+448|0;M=c[N>>2]|0;O=c[P>>2]|0;I=O+(c[J>>2]|0)|0;K=M+(c[L>>2]|0)|0;M=M-(K+1>>1)|0;O=O-(I+1>>1)-(M+1>>1)|0;M=(O+1>>1)+M|0;I=O+I|0;O=(I>>1)-O|0;I=I-((O*3|0)+4>>3)|0;O=O-(I>>7)+(I>>10)-(I*3>>4)|0;I=I-(O*3>>3)|0;O=(I>>1)-O|0;I=I-O|0;K=M+K|0;M=(K>>1)-M|0;K=K-((M*3|0)+4>>3)|0;M=M-(K>>7)+(K>>10)-(K*3>>4)|0;K=K-(M*3>>3)|0;M=(K>>1)-M|0;K=K-M|0;O=(I+1>>1)+O|0;M=(K+1>>1)+M|0;c[J>>2]=I-O;c[L>>2]=K-M;c[N>>2]=M;c[P>>2]=O}if(!g?(c[t>>2]|0)==0:0){break}if(!h?(c[z>>2]|0)==0:0){break}J=u+ -384|0;L=u+ -128|0;N=u+ -320|0;P=u+ -64|0;M=c[N>>2]|0;O=c[P>>2]|0;I=O+(c[J>>2]|0)|0;K=M+(c[L>>2]|0)|0;M=M-(K+1>>1)|0;O=O-(I+1>>1)-(M+1>>1)|0;M=(O+1>>1)+M|0;I=O+I|0;O=(I>>1)-O|0;I=I-((O*3|0)+4>>3)|0;O=O-(I>>7)+(I>>10)-(I*3>>4)|0;I=I-(O*3>>3)|0;O=(I>>1)-O|0;I=I-O|0;K=M+K|0;M=(K>>1)-M|0;K=K-((M*3|0)+4>>3)|0;M=M-(K>>7)+(K>>10)-(K*3>>4)|0;K=K-(M*3>>3)|0;M=(K>>1)-M|0;K=K-M|0;O=(I+1>>1)+O|0;M=(K+1>>1)+M|0;c[J>>2]=I-O;c[L>>2]=K-M;c[N>>2]=M;c[P>>2]=O}}while(0);if(f){if(p){break}else{b=110}}else{G=c[z>>2]|0;if(!((G|0)==0|p)){b=110}}if((b|0)==110){b=0;do{if((c[t>>2]|0)==0){if(!(!j?(c[z>>2]|0)==0:0)){O=u+128|0;P=u+192|0;J=F+64|0;N=c[F>>2]|0;L=c[J>>2]|0;I=L+(c[O>>2]|0)|0;M=N+(c[P>>2]|0)|0;N=N-(M+1>>1)|0;L=L-(I+1>>1)-(N+1>>1)|0;N=(L+1>>1)+N|0;I=L+I|0;L=(I>>1)-L|0;I=I-((L*3|0)+4>>3)|0;L=L-(I>>7)+(I>>10)-(I*3>>4)|0;I=I-(L*3>>3)|0;L=(I>>1)-L|0;I=I-L|0;M=N+M|0;N=(M>>1)-N|0;M=M-((N*3|0)+4>>3)|0;N=N-(M>>7)+(M>>10)-(M*3>>4)|0;M=M-(N*3>>3)|0;N=(M>>1)-N|0;M=M-N|0;L=(I+1>>1)+L|0;N=(M+1>>1)+N|0;c[O>>2]=I-L;c[P>>2]=M-N;c[F>>2]=N;c[J>>2]=L;J=u+384|0;L=u+448|0;N=F+256|0;P=F+320|0;M=c[N>>2]|0;O=c[P>>2]|0;I=O+(c[J>>2]|0)|0;K=M+(c[L>>2]|0)|0;M=M-(K+1>>1)|0;O=O-(I+1>>1)-(M+1>>1)|0;M=(O+1>>1)+M|0;I=O+I|0;O=(I>>1)-O|0;I=I-((O*3|0)+4>>3)|0;O=O-(I>>7)+(I>>10)-(I*3>>4)|0;I=I-(O*3>>3)|0;O=(I>>1)-O|0;I=I-O|0;K=M+K|0;M=(K>>1)-M|0;K=K-((M*3|0)+4>>3)|0;M=M-(K>>7)+(K>>10)-(K*3>>4)|0;K=K-(M*3>>3)|0;M=(K>>1)-M|0;K=K-M|0;O=(I+1>>1)+O|0;M=(K+1>>1)+M|0;c[J>>2]=I-O;c[L>>2]=K-M;c[N>>2]=M;c[P>>2]=O}if(!h?(c[z>>2]|0)==0:0){break}I=u+ -384|0;M=u+ -320|0;N=F+ -512|0;J=F+ -448|0;P=c[N>>2]|0;L=c[J>>2]|0;K=L+(c[I>>2]|0)|0;O=P+(c[M>>2]|0)|0;P=P-(O+1>>1)|0;L=L-(K+1>>1)-(P+1>>1)|0;P=(L+1>>1)+P|0;K=L+K|0;L=(K>>1)-L|0;K=K-((L*3|0)+4>>3)|0;L=L-(K>>7)+(K>>10)-(K*3>>4)|0;K=K-(L*3>>3)|0;L=(K>>1)-L|0;K=K-L|0;O=P+O|0;P=(O>>1)-P|0;O=O-((P*3|0)+4>>3)|0;P=P-(O>>7)+(O>>10)-(O*3>>4)|0;O=O-(P*3>>3)|0;P=(O>>1)-P|0;O=O-P|0;L=(K+1>>1)+L|0;P=(O+1>>1)+P|0;c[I>>2]=K-L;c[M>>2]=O-P;c[N>>2]=P;c[J>>2]=L;J=u+ -128|0;L=u+ -64|0;N=F+ -256|0;P=F+ -192|0;M=c[N>>2]|0;O=c[P>>2]|0;I=O+(c[J>>2]|0)|0;K=M+(c[L>>2]|0)|0;M=M-(K+1>>1)|0;O=O-(I+1>>1)-(M+1>>1)|0;M=(O+1>>1)+M|0;I=O+I|0;O=(I>>1)-O|0;I=I-((O*3|0)+4>>3)|0;O=O-(I>>7)+(I>>10)-(I*3>>4)|0;I=I-(O*3>>3)|0;O=(I>>1)-O|0;I=I-O|0;K=M+K|0;M=(K>>1)-M|0;K=K-((M*3|0)+4>>3)|0;M=M-(K>>7)+(K>>10)-(K*3>>4)|0;K=K-(M*3>>3)|0;M=(K>>1)-M|0;K=K-M|0;O=(I+1>>1)+O|0;M=(K+1>>1)+M|0;c[J>>2]=I-O;c[L>>2]=K-M;c[N>>2]=M;c[P>>2]=O}}while(0);if(f){break}G=c[z>>2]|0}if((G|0)==0){if(!p?(c[t>>2]|0)==0:0){Mb(u,F);break}if(!(!e?(c[t>>2]|0)==0:0)){O=F+ -512|0;P=F+ -256|0;J=F+256|0;N=c[F>>2]|0;L=c[J>>2]|0;I=L+(c[O>>2]|0)|0;M=N+(c[P>>2]|0)|0;N=N-(M+1>>1)|0;L=L-(I+1>>1)-(N+1>>1)|0;N=(L+1>>1)+N|0;I=L+I|0;L=(I>>1)-L|0;I=I-((L*3|0)+4>>3)|0;L=L-(I>>7)+(I>>10)-(I*3>>4)|0;I=I-(L*3>>3)|0;L=(I>>1)-L|0;I=I-L|0;M=N+M|0;N=(M>>1)-N|0;M=M-((N*3|0)+4>>3)|0;N=N-(M>>7)+(M>>10)-(M*3>>4)|0;M=M-(N*3>>3)|0;N=(M>>1)-N|0;M=M-N|0;L=(I+1>>1)+L|0;N=(M+1>>1)+N|0;c[O>>2]=I-L;c[P>>2]=M-N;c[F>>2]=N;c[J>>2]=L;J=F+ -448|0;L=F+ -192|0;N=F+64|0;P=F+320|0;M=c[N>>2]|0;O=c[P>>2]|0;I=O+(c[J>>2]|0)|0;K=M+(c[L>>2]|0)|0;M=M-(K+1>>1)|0;O=O-(I+1>>1)-(M+1>>1)|0;M=(O+1>>1)+M|0;I=O+I|0;O=(I>>1)-O|0;I=I-((O*3|0)+4>>3)|0;O=O-(I>>7)+(I>>10)-(I*3>>4)|0;I=I-(O*3>>3)|0;O=(I>>1)-O|0;I=I-O|0;K=M+K|0;M=(K>>1)-M|0;K=K-((M*3|0)+4>>3)|0;M=M-(K>>7)+(K>>10)-(K*3>>4)|0;K=K-(M*3>>3)|0;M=(K>>1)-M|0;K=K-M|0;O=(I+1>>1)+O|0;M=(K+1>>1)+M|0;c[J>>2]=I-O;c[L>>2]=K-M;c[N>>2]=M;c[P>>2]=O}if(!g?(c[t>>2]|0)==0:0){break}N=u+128|0;I=u+ -384|0;M=u+ -128|0;J=u+384|0;P=c[N>>2]|0;L=c[J>>2]|0;K=L+(c[I>>2]|0)|0;O=P+(c[M>>2]|0)|0;P=P-(O+1>>1)|0;L=L-(K+1>>1)-(P+1>>1)|0;P=(L+1>>1)+P|0;K=L+K|0;L=(K>>1)-L|0;K=K-((L*3|0)+4>>3)|0;L=L-(K>>7)+(K>>10)-(K*3>>4)|0;K=K-(L*3>>3)|0;L=(K>>1)-L|0;K=K-L|0;O=P+O|0;P=(O>>1)-P|0;O=O-((P*3|0)+4>>3)|0;P=P-(O>>7)+(O>>10)-(O*3>>4)|0;O=O-(P*3>>3)|0;P=(O>>1)-P|0;O=O-P|0;L=(K+1>>1)+L|0;P=(O+1>>1)+P|0;c[I>>2]=K-L;c[M>>2]=O-P;c[N>>2]=P;c[J>>2]=L;J=u+ -320|0;L=u+ -64|0;N=u+192|0;P=u+448|0;M=c[N>>2]|0;O=c[P>>2]|0;I=O+(c[J>>2]|0)|0;K=M+(c[L>>2]|0)|0;M=M-(K+1>>1)|0;O=O-(I+1>>1)-(M+1>>1)|0;M=(O+1>>1)+M|0;I=O+I|0;O=(I>>1)-O|0;I=I-((O*3|0)+4>>3)|0;O=O-(I>>7)+(I>>10)-(I*3>>4)|0;I=I-(O*3>>3)|0;O=(I>>1)-O|0;I=I-O|0;K=M+K|0;M=(K>>1)-M|0;K=K-((M*3|0)+4>>3)|0;M=M-(K>>7)+(K>>10)-(K*3>>4)|0;K=K-(M*3>>3)|0;M=(K>>1)-M|0;K=K-M|0;O=(I+1>>1)+O|0;M=(K+1>>1)+M|0;c[J>>2]=I-O;c[L>>2]=K-M;c[N>>2]=M;c[P>>2]=O}}}while(0);if(!d){F=u+ -1024|0;if(!((c[E>>2]|0)==0|(w|0)==0)){c[F>>2]=c[F>>2]>>1;P=u+ -960|0;c[P>>2]=c[P>>2]>>1;P=u+ -896|0;c[P>>2]=c[P>>2]>>1;P=u+ -832|0;c[P>>2]=c[P>>2]>>1;P=u+ -768|0;c[P>>2]=c[P>>2]>>1;P=u+ -704|0;c[P>>2]=c[P>>2]>>1;P=u+ -640|0;c[P>>2]=c[P>>2]>>1;P=u+ -576|0;c[P>>2]=c[P>>2]>>1;P=u+ -512|0;c[P>>2]=c[P>>2]>>1;P=u+ -448|0;c[P>>2]=c[P>>2]>>1;P=u+ -384|0;c[P>>2]=c[P>>2]>>1;P=u+ -320|0;c[P>>2]=c[P>>2]>>1;P=u+ -256|0;c[P>>2]=c[P>>2]>>1;P=u+ -192|0;c[P>>2]=c[P>>2]>>1;P=u+ -128|0;c[P>>2]=c[P>>2]>>1;P=u+ -64|0;c[P>>2]=c[P>>2]>>1}Kb(F)}w=w+1|0}while((w|0)!=(r|0))}r=q?2:0;if(q){x=(n|0)==0;t=(n|0)==2;u=a+34364|0;v=a+34360|0;s=a+34368|0;y=a+34372|0;w=a+34140|0;q=j?0:-32;z=h?0:32;A=j?16:-16;B=h?16:48;C=h|g;D=h?-16:32;E=0;do{H=E;E=E+1|0;F=c[a+(E<<2)+34652>>2]|0;G=c[a+(E<<2)+34716>>2]|0;do{if(!x){if(!(!e?(c[u>>2]|0)==0:0)){b=139}do{if((b|0)==139){b=0;if(!(!j?(c[v>>2]|0)==0:0)){L=G+4|0;N=G+8|0;P=G+12|0;M=c[N>>2]|0;O=c[P>>2]|0;J=O+(c[G>>2]|0)|0;K=M+(c[L>>2]|0)|0;M=M-(K+1>>1)|0;O=O-(J+1>>1)-(M+1>>1)|0;M=(O+1>>1)+M|0;J=O+J|0;O=(J>>1)-O|0;J=J-((O*3|0)+4>>3)|0;O=O-(J>>7)+(J>>10)-(J*3>>4)|0;J=J-(O*3>>3)|0;O=(J>>1)-O|0;J=J-O|0;K=M+K|0;M=(K>>1)-M|0;K=K-((M*3|0)+4>>3)|0;M=M-(K>>7)+(K>>10)-(K*3>>4)|0;K=K-(M*3>>3)|0;M=(K>>1)-M|0;K=K-M|0;O=(J+1>>1)+O|0;M=(K+1>>1)+M|0;c[G>>2]=J-O;c[L>>2]=K-M;c[N>>2]=M;c[P>>2]=O}if(!e?(c[u>>2]|0)==0:0){break}if(!h?(c[v>>2]|0)==0:0){break}J=G+ -108|0;L=G+ -112|0;N=G+ -100|0;P=G+ -104|0;M=c[N>>2]|0;O=c[P>>2]|0;I=O+(c[J>>2]|0)|0;K=M+(c[L>>2]|0)|0;M=M-(K+1>>1)|0;O=O-(I+1>>1)-(M+1>>1)|0;M=(O+1>>1)+M|0;I=O+I|0;O=(I>>1)-O|0;I=I-((O*3|0)+4>>3)|0;O=O-(I>>7)+(I>>10)-(I*3>>4)|0;I=I-(O*3>>3)|0;O=(I>>1)-O|0;I=I-O|0;K=M+K|0;M=(K>>1)-M|0;K=K-((M*3|0)+4>>3)|0;M=M-(K>>7)+(K>>10)-(K*3>>4)|0;K=K-(M*3>>3)|0;M=(K>>1)-M|0;K=K-M|0;O=(I+1>>1)+O|0;M=(K+1>>1)+M|0;c[J>>2]=I-O;c[L>>2]=K-M;c[N>>2]=M;c[P>>2]=O}}while(0);if(!(!g?(c[u>>2]|0)==0:0)){b=149}do{if((b|0)==149){b=0;if(!(!j?(c[v>>2]|0)==0:0)){J=F+104|0;L=F+108|0;N=F+96|0;P=F+100|0;M=c[N>>2]|0;O=c[P>>2]|0;I=O+(c[J>>2]|0)|0;K=M+(c[L>>2]|0)|0;M=M-(K+1>>1)|0;O=O-(I+1>>1)-(M+1>>1)|0;M=(O+1>>1)+M|0;I=O+I|0;O=(I>>1)-O|0;I=I-((O*3|0)+4>>3)|0;O=O-(I>>7)+(I>>10)-(I*3>>4)|0;I=I-(O*3>>3)|0;O=(I>>1)-O|0;I=I-O|0;K=M+K|0;M=(K>>1)-M|0;K=K-((M*3|0)+4>>3)|0;M=M-(K>>7)+(K>>10)-(K*3>>4)|0;K=K-(M*3>>3)|0;M=(K>>1)-M|0;K=K-M|0;O=(I+1>>1)+O|0;M=(K+1>>1)+M|0;c[J>>2]=I-O;c[L>>2]=K-M;c[N>>2]=M;c[P>>2]=O}if(!g?(c[u>>2]|0)==0:0){break}if(!h?(c[v>>2]|0)==0:0){break}J=F+ -4|0;L=F+ -8|0;N=F+ -12|0;P=F+ -16|0;M=c[N>>2]|0;O=c[P>>2]|0;I=O+(c[J>>2]|0)|0;K=M+(c[L>>2]|0)|0;M=M-(K+1>>1)|0;O=O-(I+1>>1)-(M+1>>1)|0;M=(O+1>>1)+M|0;I=O+I|0;O=(I>>1)-O|0;I=I-((O*3|0)+4>>3)|0;O=O-(I>>7)+(I>>10)-(I*3>>4)|0;I=I-(O*3>>3)|0;O=(I>>1)-O|0;I=I-O|0;K=M+K|0;M=(K>>1)-M|0;K=K-((M*3|0)+4>>3)|0;M=M-(K>>7)+(K>>10)-(K*3>>4)|0;K=K-(M*3>>3)|0;M=(K>>1)-M|0;K=K-M|0;O=(I+1>>1)+O|0;M=(K+1>>1)+M|0;c[J>>2]=I-O;c[L>>2]=K-M;c[N>>2]=M;c[P>>2]=O}}while(0);if(!C){if(!e?(c[u>>2]|0)==0:0){if(j){I=0}else{I=(c[v>>2]|0)!=0?0:-32}while(1){Lb(F+((I|16)<<2)|0,G+(I<<2)|0,32);if((I|0)<0){I=I+32|0}else{break}}}else{if(j){I=0}else{I=(c[v>>2]|0)!=0?0:-32}do{R=G+((I|5)<<2)|0;L=G+((I|4)<<2)|0;P=I;I=I+32|0;M=G+(I<<2)|0;Q=G+(P+33<<2)|0;O=c[M>>2]|0;K=c[Q>>2]|0;J=K+(c[R>>2]|0)|0;N=O+(c[L>>2]|0)|0;O=O-(N+1>>1)|0;K=K-(J+1>>1)-(O+1>>1)|0;O=(K+1>>1)+O|0;J=K+J|0;K=(J>>1)-K|0;J=J-((K*3|0)+4>>3)|0;K=K-(J>>7)+(J>>10)-(J*3>>4)|0;J=J-(K*3>>3)|0;K=(J>>1)-K|0;J=J-K|0;N=O+N|0;O=(N>>1)-O|0;N=N-((O*3|0)+4>>3)|0;O=O-(N>>7)+(N>>10)-(N*3>>4)|0;N=N-(O*3>>3)|0;O=(N>>1)-O|0;N=N-O|0;K=(J+1>>1)+K|0;O=(N+1>>1)+O|0;c[R>>2]=J-K;c[L>>2]=N-O;c[M>>2]=O;c[Q>>2]=K;Q=G+((P|7)<<2)|0;K=G+((P|6)<<2)|0;M=G+(P+34<<2)|0;O=G+(P+35<<2)|0;L=c[M>>2]|0;N=c[O>>2]|0;R=N+(c[Q>>2]|0)|0;J=L+(c[K>>2]|0)|0;L=L-(J+1>>1)|0;N=N-(R+1>>1)-(L+1>>1)|0;L=(N+1>>1)+L|0;R=N+R|0;N=(R>>1)-N|0;R=R-((N*3|0)+4>>3)|0;N=N-(R>>7)+(R>>10)-(R*3>>4)|0;R=R-(N*3>>3)|0;N=(R>>1)-N|0;R=R-N|0;J=L+J|0;L=(J>>1)-L|0;J=J-((L*3|0)+4>>3)|0;L=L-(J>>7)+(J>>10)-(J*3>>4)|0;J=J-(L*3>>3)|0;L=(J>>1)-L|0;J=J-L|0;N=(R+1>>1)+N|0;L=(J+1>>1)+L|0;c[Q>>2]=R-N;c[K>>2]=J-L;c[M>>2]=L;c[O>>2]=N}while((P|0)<0)}if(!j?(c[v>>2]|0)==0:0){Lb(G+ -128|0,G+ -64|0,32);I=G+64|0}else{if(!e?(c[u>>2]|0)==0:0){Q=F+104|0;R=F+96|0;L=G+8|0;P=c[G>>2]|0;N=c[L>>2]|0;K=N+(c[Q>>2]|0)|0;O=P+(c[R>>2]|0)|0;P=P-(O+1>>1)|0;N=N-(K+1>>1)-(P+1>>1)|0;P=(N+1>>1)+P|0;K=N+K|0;N=(K>>1)-N|0;K=K-((N*3|0)+4>>3)|0;N=N-(K>>7)+(K>>10)-(K*3>>4)|0;K=K-(N*3>>3)|0;N=(K>>1)-N|0;K=K-N|0;O=P+O|0;P=(O>>1)-P|0;O=O-((P*3|0)+4>>3)|0;P=P-(O>>7)+(O>>10)-(O*3>>4)|0;O=O-(P*3>>3)|0;P=(O>>1)-P|0;O=O-P|0;N=(K+1>>1)+N|0;P=(O+1>>1)+P|0;c[Q>>2]=K-N;c[R>>2]=O-P;c[G>>2]=P;c[L>>2]=N;L=F+108|0;N=F+100|0;P=G+4|0;R=G+12|0;O=c[P>>2]|0;Q=c[R>>2]|0;K=Q+(c[L>>2]|0)|0;M=O+(c[N>>2]|0)|0;O=O-(M+1>>1)|0;Q=Q-(K+1>>1)-(O+1>>1)|0;O=(Q+1>>1)+O|0;K=Q+K|0;Q=(K>>1)-Q|0;K=K-((Q*3|0)+4>>3)|0;Q=Q-(K>>7)+(K>>10)-(K*3>>4)|0;K=K-(Q*3>>3)|0;Q=(K>>1)-Q|0;K=K-Q|0;M=O+M|0;O=(M>>1)-O|0;M=M-((O*3|0)+4>>3)|0;O=O-(M>>7)+(M>>10)-(M*3>>4)|0;M=M-(O*3>>3)|0;O=(M>>1)-O|0;M=M-O|0;Q=(K+1>>1)+Q|0;O=(M+1>>1)+O|0;c[L>>2]=K-Q;c[N>>2]=M-O;c[P>>2]=O;c[R>>2]=Q}Q=G+40|0;R=G+32|0;I=G+64|0;L=G+72|0;P=c[I>>2]|0;N=c[L>>2]|0;K=N+(c[Q>>2]|0)|0;O=P+(c[R>>2]|0)|0;P=P-(O+1>>1)|0;N=N-(K+1>>1)-(P+1>>1)|0;P=(N+1>>1)+P|0;K=N+K|0;N=(K>>1)-N|0;K=K-((N*3|0)+4>>3)|0;N=N-(K>>7)+(K>>10)-(K*3>>4)|0;K=K-(N*3>>3)|0;N=(K>>1)-N|0;K=K-N|0;O=P+O|0;P=(O>>1)-P|0;O=O-((P*3|0)+4>>3)|0;P=P-(O>>7)+(O>>10)-(O*3>>4)|0;O=O-(P*3>>3)|0;P=(O>>1)-P|0;O=O-P|0;N=(K+1>>1)+N|0;P=(O+1>>1)+P|0;c[Q>>2]=K-N;c[R>>2]=O-P;c[I>>2]=P;c[L>>2]=N;L=G+44|0;N=G+36|0;P=G+68|0;R=G+76|0;O=c[P>>2]|0;Q=c[R>>2]|0;K=Q+(c[L>>2]|0)|0;M=O+(c[N>>2]|0)|0;O=O-(M+1>>1)|0;Q=Q-(K+1>>1)-(O+1>>1)|0;O=(Q+1>>1)+O|0;K=Q+K|0;Q=(K>>1)-Q|0;K=K-((Q*3|0)+4>>3)|0;Q=Q-(K>>7)+(K>>10)-(K*3>>4)|0;K=K-(Q*3>>3)|0;Q=(K>>1)-Q|0;K=K-Q|0;M=O+M|0;O=(M>>1)-O|0;M=M-((O*3|0)+4>>3)|0;O=O-(M>>7)+(M>>10)-(M*3>>4)|0;M=M-(O*3>>3)|0;O=(M>>1)-O|0;M=M-O|0;Q=(K+1>>1)+Q|0;O=(M+1>>1)+O|0;c[L>>2]=K-Q;c[N>>2]=M-O;c[P>>2]=O;c[R>>2]=Q}Lb(G,I,32)}if(!(!g?(c[u>>2]|0)==0:0)){if(j){I=16}else{I=(c[v>>2]|0)!=0?16:-16}if((I|0)<(D|0)){do{K=F+((I|15)<<2)|0;O=F+((I|14)<<2)|0;P=F+(I+42<<2)|0;L=F+(I+43<<2)|0;R=c[P>>2]|0;N=c[L>>2]|0;M=N+(c[K>>2]|0)|0;Q=R+(c[O>>2]|0)|0;R=R-(Q+1>>1)|0;N=N-(M+1>>1)-(R+1>>1)|0;R=(N+1>>1)+R|0;M=N+M|0;N=(M>>1)-N|0;M=M-((N*3|0)+4>>3)|0;N=N-(M>>7)+(M>>10)-(M*3>>4)|0;M=M-(N*3>>3)|0;N=(M>>1)-N|0;M=M-N|0;Q=R+Q|0;R=(Q>>1)-R|0;Q=Q-((R*3|0)+4>>3)|0;R=R-(Q>>7)+(Q>>10)-(Q*3>>4)|0;Q=Q-(R*3>>3)|0;R=(Q>>1)-R|0;Q=Q-R|0;N=(M+1>>1)+N|0;R=(Q+1>>1)+R|0;c[K>>2]=M-N;c[O>>2]=Q-R;c[P>>2]=R;c[L>>2]=N;L=F+((I|13)<<2)|0;N=F+((I|12)<<2)|0;P=F+(I+40<<2)|0;R=F+(I+41<<2)|0;O=c[P>>2]|0;Q=c[R>>2]|0;K=Q+(c[L>>2]|0)|0;M=O+(c[N>>2]|0)|0;O=O-(M+1>>1)|0;Q=Q-(K+1>>1)-(O+1>>1)|0;O=(Q+1>>1)+O|0;K=Q+K|0;Q=(K>>1)-Q|0;K=K-((Q*3|0)+4>>3)|0;Q=Q-(K>>7)+(K>>10)-(K*3>>4)|0;K=K-(Q*3>>3)|0;Q=(K>>1)-Q|0;K=K-Q|0;M=O+M|0;O=(M>>1)-O|0;M=M-((O*3|0)+4>>3)|0;O=O-(M>>7)+(M>>10)-(M*3>>4)|0;M=M-(O*3>>3)|0;O=(M>>1)-O|0;M=M-O|0;Q=(K+1>>1)+Q|0;O=(M+1>>1)+O|0;c[L>>2]=K-Q;c[N>>2]=M-O;c[P>>2]=O;c[R>>2]=Q;I=I+32|0}while((I|0)<(D|0))}}if(h){if(g){break}}else{if((c[v>>2]|0)==0|g){break}}if(!e?(c[u>>2]|0)==0:0){K=F+ -4|0;O=F+ -12|0;P=G+ -108|0;L=G+ -100|0;R=c[P>>2]|0;N=c[L>>2]|0;M=N+(c[K>>2]|0)|0;Q=R+(c[O>>2]|0)|0;R=R-(Q+1>>1)|0;N=N-(M+1>>1)-(R+1>>1)|0;R=(N+1>>1)+R|0;M=N+M|0;N=(M>>1)-N|0;M=M-((N*3|0)+4>>3)|0;N=N-(M>>7)+(M>>10)-(M*3>>4)|0;M=M-(N*3>>3)|0;N=(M>>1)-N|0;M=M-N|0;Q=R+Q|0;R=(Q>>1)-R|0;Q=Q-((R*3|0)+4>>3)|0;R=R-(Q>>7)+(Q>>10)-(Q*3>>4)|0;Q=Q-(R*3>>3)|0;R=(Q>>1)-R|0;Q=Q-R|0;N=(M+1>>1)+N|0;R=(Q+1>>1)+R|0;c[K>>2]=M-N;c[O>>2]=Q-R;c[P>>2]=R;c[L>>2]=N;L=F+ -8|0;N=F+ -16|0;P=G+ -112|0;R=G+ -104|0;O=c[P>>2]|0;Q=c[R>>2]|0;K=Q+(c[L>>2]|0)|0;M=O+(c[N>>2]|0)|0;O=O-(M+1>>1)|0;Q=Q-(K+1>>1)-(O+1>>1)|0;O=(Q+1>>1)+O|0;K=Q+K|0;Q=(K>>1)-Q|0;K=K-((Q*3|0)+4>>3)|0;Q=Q-(K>>7)+(K>>10)-(K*3>>4)|0;K=K-(Q*3>>3)|0;Q=(K>>1)-Q|0;K=K-Q|0;M=O+M|0;O=(M>>1)-O|0;M=M-((O*3|0)+4>>3)|0;O=O-(M>>7)+(M>>10)-(M*3>>4)|0;M=M-(O*3>>3)|0;O=(M>>1)-O|0;M=M-O|0;Q=(K+1>>1)+Q|0;O=(M+1>>1)+O|0;c[L>>2]=K-Q;c[N>>2]=M-O;c[P>>2]=O;c[R>>2]=Q}K=G+ -68|0;O=G+ -76|0;P=G+ -44|0;L=G+ -36|0;R=c[P>>2]|0;N=c[L>>2]|0;M=N+(c[K>>2]|0)|0;Q=R+(c[O>>2]|0)|0;R=R-(Q+1>>1)|0;N=N-(M+1>>1)-(R+1>>1)|0;R=(N+1>>1)+R|0;M=N+M|0;N=(M>>1)-N|0;M=M-((N*3|0)+4>>3)|0;N=N-(M>>7)+(M>>10)-(M*3>>4)|0;M=M-(N*3>>3)|0;N=(M>>1)-N|0;M=M-N|0;Q=R+Q|0;R=(Q>>1)-R|0;Q=Q-((R*3|0)+4>>3)|0;R=R-(Q>>7)+(Q>>10)-(Q*3>>4)|0;Q=Q-(R*3>>3)|0;R=(Q>>1)-R|0;Q=Q-R|0;N=(M+1>>1)+N|0;R=(Q+1>>1)+R|0;c[K>>2]=M-N;c[O>>2]=Q-R;c[P>>2]=R;c[L>>2]=N;L=G+ -72|0;N=G+ -80|0;P=G+ -48|0;R=G+ -40|0;O=c[P>>2]|0;Q=c[R>>2]|0;K=Q+(c[L>>2]|0)|0;M=O+(c[N>>2]|0)|0;O=O-(M+1>>1)|0;Q=Q-(K+1>>1)-(O+1>>1)|0;O=(Q+1>>1)+O|0;K=Q+K|0;Q=(K>>1)-Q|0;K=K-((Q*3|0)+4>>3)|0;Q=Q-(K>>7)+(K>>10)-(K*3>>4)|0;K=K-(Q*3>>3)|0;Q=(K>>1)-Q|0;K=K-Q|0;M=O+M|0;O=(M>>1)-O|0;M=M-((O*3|0)+4>>3)|0;O=O-(M>>7)+(M>>10)-(M*3>>4)|0;M=M-(O*3>>3)|0;O=(M>>1)-O|0;M=M-O|0;Q=(K+1>>1)+Q|0;O=(M+1>>1)+O|0;c[L>>2]=K-Q;c[N>>2]=M-O;c[P>>2]=O;c[R>>2]=Q}}while(0);if(!(e|(A|0)>=(B|0))){I=A;do{Jb(F+(I<<2)|0);I=I+32|0}while((I|0)<(B|0))}if(!(g|(q|0)>=(z|0))){I=q;do{Jb(G+(I<<2)|0);I=I+32|0}while((I|0)<(z|0))}do{if(t){if(!(!m?(c[y>>2]|0)==0:0)){b=194}do{if((b|0)==194){b=0;if(!e?(c[u>>2]|0)==0:0){break}R=G+ -256|0;c[R>>2]=(c[R>>2]|0)-(c[G+ -128>>2]|0)}}while(0);if(!(!l?(c[s>>2]|0)==0:0)){b=199}do{if((b|0)==199){b=0;if(!e?(c[u>>2]|0)==0:0){break}c[a+(H<<3)+34376>>2]=c[G>>2]}}while(0);if(!(!h?(c[v>>2]|0)==0:0)){b=204}do{if((b|0)==204){b=0;if(!e?(c[u>>2]|0)==0:0){break}R=G+ -128|0;c[R>>2]=(c[R>>2]|0)-(c[a+(H<<3)+34376>>2]|0)}}while(0);if(!(!m?(c[y>>2]|0)==0:0)){b=209}do{if((b|0)==209){b=0;if(!g?(c[u>>2]|0)==0:0){break}R=F+ -192|0;c[R>>2]=(c[R>>2]|0)-(c[F+ -64>>2]|0)}}while(0);if(!(!l?(c[s>>2]|0)==0:0)){b=214}do{if((b|0)==214){b=0;if(!g?(c[u>>2]|0)==0:0){break}c[a+(H<<3)+34380>>2]=c[F+64>>2]}}while(0);if(!(!h?(c[v>>2]|0)==0:0)){b=219}do{if((b|0)==219){b=0;if(!g?(c[u>>2]|0)==0:0){break}R=F+ -64|0;c[R>>2]=(c[R>>2]|0)-(c[a+(H<<3)+34380>>2]|0)}}while(0);if(f){if(!p){b=225}}else{if((c[v>>2]|0)==0|p){b=233}else{b=225}}if((b|0)==225){b=0;do{if((c[u>>2]|0)==0){if(!(!j?(c[v>>2]|0)==0:0)){P=F+64|0;R=c[P>>2]|0;Q=(c[G>>2]|0)-(R+2>>2)|0;R=R-(Q>>5)-(Q>>9)-(Q>>13)-(Q+1>>1)|0;c[P>>2]=R;c[G>>2]=Q-(R+2>>2)}if(!h?(c[v>>2]|0)==0:0){break}O=F+ -64|0;R=G+ -128|0;Q=c[O>>2]|0;P=(c[R>>2]|0)-(Q+2>>2)|0;Q=Q-(P>>5)-(P>>9)-(P>>13)-(P+1>>1)|0;c[O>>2]=Q;c[R>>2]=P-(Q+2>>2)}}while(0);if(!f){b=233}}b:do{if((b|0)==233){b=0;if(!(!p?(c[u>>2]|0)==0:0)){b=235}do{if((b|0)==235){b=0;if((c[v>>2]|0)!=0){if(p){break b}if((c[u>>2]|0)==0){break}else{break b}}if(!(!e?(c[u>>2]|0)==0:0)){P=G+ -128|0;R=c[P>>2]|0;Q=(c[G>>2]|0)-(R+2>>2)|0;R=R-(Q>>5)-(Q>>9)-(Q>>13)-(Q+1>>1)|0;c[P>>2]=R;c[G>>2]=Q-(R+2>>2)}if(!g?(c[u>>2]|0)==0:0){break b}R=F+64|0;O=F+ -64|0;Q=c[O>>2]|0;P=(c[R>>2]|0)-(Q+2>>2)|0;Q=Q-(P>>5)-(P>>9)-(P>>13)-(P+1>>1)|0;c[O>>2]=Q;c[R>>2]=P-(Q+2>>2);break b}}while(0);if((c[v>>2]|0)==0){M=F+ -64|0;O=F+64|0;Q=G+ -128|0;K=c[Q>>2]|0;J=c[G>>2]|0;R=J+(c[M>>2]|0)|0;P=K+(c[O>>2]|0)|0;N=P-(R+2>>2)|0;L=R-(N>>5)-(N>>9)-(N>>13)-(N+1>>1)|0;N=N-(L+2>>2)|0;R=(L+1>>1)+(J-(R+1>>1))|0;P=(N+1>>1)+(K-(P+1>>1))|0;c[M>>2]=L-R;c[O>>2]=N-P;c[Q>>2]=P;c[G>>2]=R}}}while(0);if(!(!m?(c[y>>2]|0)==0:0)){b=248}do{if((b|0)==248){b=0;if(!e?(c[u>>2]|0)==0:0){break}R=G+ -256|0;c[R>>2]=(c[R>>2]|0)+(c[G+ -128>>2]|0)}}while(0);if(!(!l?(c[s>>2]|0)==0:0)){b=253}do{if((b|0)==253){b=0;if(!e?(c[u>>2]|0)==0:0){break}c[a+(H<<3)+34392>>2]=c[G>>2]}}while(0);if(!(!h?(c[v>>2]|0)==0:0)){b=258}do{if((b|0)==258){b=0;if(!e?(c[u>>2]|0)==0:0){break}R=G+ -128|0;c[R>>2]=(c[R>>2]|0)+(c[a+(H<<3)+34392>>2]|0)}}while(0);if(!(!m?(c[y>>2]|0)==0:0)){b=263}do{if((b|0)==263){b=0;if(!g?(c[u>>2]|0)==0:0){break}R=F+ -192|0;c[R>>2]=(c[R>>2]|0)+(c[F+ -64>>2]|0)}}while(0);if(!(!l?(c[s>>2]|0)==0:0)){b=268}do{if((b|0)==268){b=0;if(!g?(c[u>>2]|0)==0:0){break}c[a+(H<<3)+34396>>2]=c[F+64>>2]}}while(0);if(!h?(c[v>>2]|0)==0:0){break}if(!g?(c[u>>2]|0)==0:0){break}R=F+ -64|0;c[R>>2]=(c[R>>2]|0)+(c[a+(H<<3)+34396>>2]|0)}}while(0);do{if(!d){G=F+ -256|0;H=F+ -128|0;I=F+ -192|0;F=F+ -64|0;if((c[w>>2]|0)==0){de(G,H,I,F);break}else{R=c[I>>2]>>1;Q=c[F>>2]>>1;O=Q+(c[G>>2]>>1)|0;P=(c[H>>2]>>1)-R|0;N=O-P>>1;Q=N-Q|0;R=N-R|0;c[G>>2]=O-R;c[H>>2]=Q+P;c[I>>2]=Q;c[F>>2]=R;break}}}while(0)}while((E|0)<(r|0))}p=o?2:0;if(!o){i=k;return}o=(n|0)==0;y=(n|0)==2;z=a+34364|0;A=a+34360|0;w=a+34368|0;x=a+34372|0;v=a+34140|0;u=j?0:-64;t=h?0:64;s=j?48:-16;r=h?48:112;q=h|g;C=h?-16:112;n=0;do{E=n;n=n+1|0;B=c[a+(n<<2)+34652>>2]|0;D=c[a+(n<<2)+34716>>2]|0;do{if(!o){if(!(!e?(c[z>>2]|0)==0:0)){b=284}do{if((b|0)==284){b=0;if(!(!j?(c[A>>2]|0)==0:0)){N=D+4|0;P=D+8|0;R=D+12|0;O=c[P>>2]|0;Q=c[R>>2]|0;L=Q+(c[D>>2]|0)|0;M=O+(c[N>>2]|0)|0;O=O-(M+1>>1)|0;Q=Q-(L+1>>1)-(O+1>>1)|0;O=(Q+1>>1)+O|0;L=Q+L|0;Q=(L>>1)-Q|0;L=L-((Q*3|0)+4>>3)|0;Q=Q-(L>>7)+(L>>10)-(L*3>>4)|0;L=L-(Q*3>>3)|0;Q=(L>>1)-Q|0;L=L-Q|0;M=O+M|0;O=(M>>1)-O|0;M=M-((O*3|0)+4>>3)|0;O=O-(M>>7)+(M>>10)-(M*3>>4)|0;M=M-(O*3>>3)|0;O=(M>>1)-O|0;M=M-O|0;Q=(L+1>>1)+Q|0;O=(M+1>>1)+O|0;c[D>>2]=L-Q;c[N>>2]=M-O;c[P>>2]=O;c[R>>2]=Q}if(!e?(c[z>>2]|0)==0:0){break}if(!h?(c[A>>2]|0)==0:0){break}L=D+ -236|0;N=D+ -240|0;P=D+ -228|0;R=D+ -232|0;O=c[P>>2]|0;Q=c[R>>2]|0;K=Q+(c[L>>2]|0)|0;M=O+(c[N>>2]|0)|0;O=O-(M+1>>1)|0;Q=Q-(K+1>>1)-(O+1>>1)|0;O=(Q+1>>1)+O|0;K=Q+K|0;Q=(K>>1)-Q|0;K=K-((Q*3|0)+4>>3)|0;Q=Q-(K>>7)+(K>>10)-(K*3>>4)|0;K=K-(Q*3>>3)|0;Q=(K>>1)-Q|0;K=K-Q|0;M=O+M|0;O=(M>>1)-O|0;M=M-((O*3|0)+4>>3)|0;O=O-(M>>7)+(M>>10)-(M*3>>4)|0;M=M-(O*3>>3)|0;O=(M>>1)-O|0;M=M-O|0;Q=(K+1>>1)+Q|0;O=(M+1>>1)+O|0;c[L>>2]=K-Q;c[N>>2]=M-O;c[P>>2]=O;c[R>>2]=Q}}while(0);if(!(!g?(c[z>>2]|0)==0:0)){b=294}do{if((b|0)==294){b=0;if(!(!j?(c[A>>2]|0)==0:0)){L=B+232|0;N=B+236|0;P=B+224|0;R=B+228|0;O=c[P>>2]|0;Q=c[R>>2]|0;K=Q+(c[L>>2]|0)|0;M=O+(c[N>>2]|0)|0;O=O-(M+1>>1)|0;Q=Q-(K+1>>1)-(O+1>>1)|0;O=(Q+1>>1)+O|0;K=Q+K|0;Q=(K>>1)-Q|0;K=K-((Q*3|0)+4>>3)|0;Q=Q-(K>>7)+(K>>10)-(K*3>>4)|0;K=K-(Q*3>>3)|0;Q=(K>>1)-Q|0;K=K-Q|0;M=O+M|0;O=(M>>1)-O|0;M=M-((O*3|0)+4>>3)|0;O=O-(M>>7)+(M>>10)-(M*3>>4)|0;M=M-(O*3>>3)|0;O=(M>>1)-O|0;M=M-O|0;Q=(K+1>>1)+Q|0;O=(M+1>>1)+O|0;c[L>>2]=K-Q;c[N>>2]=M-O;c[P>>2]=O;c[R>>2]=Q}if(!g?(c[z>>2]|0)==0:0){break}if(!h?(c[A>>2]|0)==0:0){break}L=B+ -4|0;N=B+ -8|0;P=B+ -12|0;R=B+ -16|0;O=c[P>>2]|0;Q=c[R>>2]|0;K=Q+(c[L>>2]|0)|0;M=O+(c[N>>2]|0)|0;O=O-(M+1>>1)|0;Q=Q-(K+1>>1)-(O+1>>1)|0;O=(Q+1>>1)+O|0;K=Q+K|0;Q=(K>>1)-Q|0;K=K-((Q*3|0)+4>>3)|0;Q=Q-(K>>7)+(K>>10)-(K*3>>4)|0;K=K-(Q*3>>3)|0;Q=(K>>1)-Q|0;K=K-Q|0;M=O+M|0;O=(M>>1)-O|0;M=M-((O*3|0)+4>>3)|0;O=O-(M>>7)+(M>>10)-(M*3>>4)|0;M=M-(O*3>>3)|0;O=(M>>1)-O|0;M=M-O|0;Q=(K+1>>1)+Q|0;O=(M+1>>1)+O|0;c[L>>2]=K-Q;c[N>>2]=M-O;c[P>>2]=O;c[R>>2]=Q}}while(0);if(!q){if(!e?(c[z>>2]|0)==0:0){if(j){F=0}else{F=(c[A>>2]|0)!=0?0:-64}while(1){Lb(B+((F|48)<<2)|0,D+(F<<2)|0,0);if((F|0)<0){F=F+64|0}else{break}}}else{if(j){F=0}else{F=(c[A>>2]|0)!=0?0:-64}do{J=D+((F|5)<<2)|0;N=D+((F|4)<<2)|0;R=F;F=F+64|0;O=D+(F<<2)|0;K=D+(R+65<<2)|0;Q=c[O>>2]|0;M=c[K>>2]|0;L=M+(c[J>>2]|0)|0;P=Q+(c[N>>2]|0)|0;Q=Q-(P+1>>1)|0;M=M-(L+1>>1)-(Q+1>>1)|0;Q=(M+1>>1)+Q|0;L=M+L|0;M=(L>>1)-M|0;L=L-((M*3|0)+4>>3)|0;M=M-(L>>7)+(L>>10)-(L*3>>4)|0;L=L-(M*3>>3)|0;M=(L>>1)-M|0;L=L-M|0;P=Q+P|0;Q=(P>>1)-Q|0;P=P-((Q*3|0)+4>>3)|0;Q=Q-(P>>7)+(P>>10)-(P*3>>4)|0;P=P-(Q*3>>3)|0;Q=(P>>1)-Q|0;P=P-Q|0;M=(L+1>>1)+M|0;Q=(P+1>>1)+Q|0;c[J>>2]=L-M;c[N>>2]=P-Q;c[O>>2]=Q;c[K>>2]=M;K=D+((R|7)<<2)|0;M=D+((R|6)<<2)|0;O=D+(R+66<<2)|0;Q=D+(R+67<<2)|0;N=c[O>>2]|0;P=c[Q>>2]|0;J=P+(c[K>>2]|0)|0;L=N+(c[M>>2]|0)|0;N=N-(L+1>>1)|0;P=P-(J+1>>1)-(N+1>>1)|0;N=(P+1>>1)+N|0;J=P+J|0;P=(J>>1)-P|0;J=J-((P*3|0)+4>>3)|0;P=P-(J>>7)+(J>>10)-(J*3>>4)|0;J=J-(P*3>>3)|0;P=(J>>1)-P|0;J=J-P|0;L=N+L|0;N=(L>>1)-N|0;L=L-((N*3|0)+4>>3)|0;N=N-(L>>7)+(L>>10)-(L*3>>4)|0;L=L-(N*3>>3)|0;N=(L>>1)-N|0;L=L-N|0;P=(J+1>>1)+P|0;N=(L+1>>1)+N|0;c[K>>2]=J-P;c[M>>2]=L-N;c[O>>2]=N;c[Q>>2]=P}while((R|0)<0)}if(!j?(c[A>>2]|0)==0:0){Q=D+ -192|0;Lb(D+ -256|0,Q,0);R=D+ -128|0;Lb(Q,R,0);Lb(R,D+ -64|0,0)}else{if(!e?(c[z>>2]|0)==0:0){R=B+232|0;F=B+224|0;M=D+8|0;Q=c[D>>2]|0;O=c[M>>2]|0;L=O+(c[R>>2]|0)|0;P=Q+(c[F>>2]|0)|0;Q=Q-(P+1>>1)|0;O=O-(L+1>>1)-(Q+1>>1)|0;Q=(O+1>>1)+Q|0;L=O+L|0;O=(L>>1)-O|0;L=L-((O*3|0)+4>>3)|0;O=O-(L>>7)+(L>>10)-(L*3>>4)|0;L=L-(O*3>>3)|0;O=(L>>1)-O|0;L=L-O|0;P=Q+P|0;Q=(P>>1)-Q|0;P=P-((Q*3|0)+4>>3)|0;Q=Q-(P>>7)+(P>>10)-(P*3>>4)|0;P=P-(Q*3>>3)|0;Q=(P>>1)-Q|0;P=P-Q|0;O=(L+1>>1)+O|0;Q=(P+1>>1)+Q|0;c[R>>2]=L-O;c[F>>2]=P-Q;c[D>>2]=Q;c[M>>2]=O;M=B+236|0;O=B+228|0;Q=D+4|0;F=D+12|0;P=c[Q>>2]|0;R=c[F>>2]|0;L=R+(c[M>>2]|0)|0;N=P+(c[O>>2]|0)|0;P=P-(N+1>>1)|0;R=R-(L+1>>1)-(P+1>>1)|0;P=(R+1>>1)+P|0;L=R+L|0;R=(L>>1)-R|0;L=L-((R*3|0)+4>>3)|0;R=R-(L>>7)+(L>>10)-(L*3>>4)|0;L=L-(R*3>>3)|0;R=(L>>1)-R|0;L=L-R|0;N=P+N|0;P=(N>>1)-P|0;N=N-((P*3|0)+4>>3)|0;P=P-(N>>7)+(N>>10)-(N*3>>4)|0;N=N-(P*3>>3)|0;P=(N>>1)-P|0;N=N-P|0;R=(L+1>>1)+R|0;P=(N+1>>1)+P|0;c[M>>2]=L-R;c[O>>2]=N-P;c[Q>>2]=P;c[F>>2]=R;F=0}else{F=0}do{M=D+((F|10)<<2)|0;Q=D+((F|8)<<2)|0;R=F;F=F+16|0;P=D+(F<<2)|0;L=D+(R+18<<2)|0;O=c[P>>2]|0;N=c[L>>2]|0;J=N+(c[M>>2]|0)|0;K=O+(c[Q>>2]|0)|0;O=O-(K+1>>1)|0;N=N-(J+1>>1)-(O+1>>1)|0;O=(N+1>>1)+O|0;J=N+J|0;N=(J>>1)-N|0;J=J-((N*3|0)+4>>3)|0;N=N-(J>>7)+(J>>10)-(J*3>>4)|0;J=J-(N*3>>3)|0;N=(J>>1)-N|0;J=J-N|0;K=O+K|0;O=(K>>1)-O|0;K=K-((O*3|0)+4>>3)|0;O=O-(K>>7)+(K>>10)-(K*3>>4)|0;K=K-(O*3>>3)|0;O=(K>>1)-O|0;K=K-O|0;N=(J+1>>1)+N|0;O=(K+1>>1)+O|0;c[M>>2]=J-N;c[Q>>2]=K-O;c[P>>2]=O;c[L>>2]=N;L=D+((R|11)<<2)|0;N=D+((R|9)<<2)|0;P=D+(R+17<<2)|0;R=D+(R+19<<2)|0;O=c[P>>2]|0;Q=c[R>>2]|0;K=Q+(c[L>>2]|0)|0;M=O+(c[N>>2]|0)|0;O=O-(M+1>>1)|0;Q=Q-(K+1>>1)-(O+1>>1)|0;O=(Q+1>>1)+O|0;K=Q+K|0;Q=(K>>1)-Q|0;K=K-((Q*3|0)+4>>3)|0;Q=Q-(K>>7)+(K>>10)-(K*3>>4)|0;K=K-(Q*3>>3)|0;Q=(K>>1)-Q|0;K=K-Q|0;M=O+M|0;O=(M>>1)-O|0;M=M-((O*3|0)+4>>3)|0;O=O-(M>>7)+(M>>10)-(M*3>>4)|0;M=M-(O*3>>3)|0;O=(M>>1)-O|0;M=M-O|0;Q=(K+1>>1)+Q|0;O=(M+1>>1)+O|0;c[L>>2]=K-Q;c[N>>2]=M-O;c[P>>2]=O;c[R>>2]=Q}while((F|0)<48)}Q=D+64|0;Lb(D,Q,0);R=D+128|0;Lb(Q,R,0);Lb(R,D+192|0,0)}if(!(!g?(c[z>>2]|0)==0:0)){if(j){F=48}else{F=(c[A>>2]|0)!=0?48:-16}if((F|0)<(C|0)){do{K=B+((F|15)<<2)|0;O=B+((F|14)<<2)|0;P=B+(F+74<<2)|0;L=B+(F+75<<2)|0;R=c[P>>2]|0;N=c[L>>2]|0;M=N+(c[K>>2]|0)|0;Q=R+(c[O>>2]|0)|0;R=R-(Q+1>>1)|0;N=N-(M+1>>1)-(R+1>>1)|0;R=(N+1>>1)+R|0;M=N+M|0;N=(M>>1)-N|0;M=M-((N*3|0)+4>>3)|0;N=N-(M>>7)+(M>>10)-(M*3>>4)|0;M=M-(N*3>>3)|0;N=(M>>1)-N|0;M=M-N|0;Q=R+Q|0;R=(Q>>1)-R|0;Q=Q-((R*3|0)+4>>3)|0;R=R-(Q>>7)+(Q>>10)-(Q*3>>4)|0;Q=Q-(R*3>>3)|0;R=(Q>>1)-R|0;Q=Q-R|0;N=(M+1>>1)+N|0;R=(Q+1>>1)+R|0;c[K>>2]=M-N;c[O>>2]=Q-R;c[P>>2]=R;c[L>>2]=N;L=B+((F|13)<<2)|0;N=B+((F|12)<<2)|0;P=B+(F+72<<2)|0;R=B+(F+73<<2)|0;O=c[P>>2]|0;Q=c[R>>2]|0;K=Q+(c[L>>2]|0)|0;M=O+(c[N>>2]|0)|0;O=O-(M+1>>1)|0;Q=Q-(K+1>>1)-(O+1>>1)|0;O=(Q+1>>1)+O|0;K=Q+K|0;Q=(K>>1)-Q|0;K=K-((Q*3|0)+4>>3)|0;Q=Q-(K>>7)+(K>>10)-(K*3>>4)|0;K=K-(Q*3>>3)|0;Q=(K>>1)-Q|0;K=K-Q|0;M=O+M|0;O=(M>>1)-O|0;M=M-((O*3|0)+4>>3)|0;O=O-(M>>7)+(M>>10)-(M*3>>4)|0;M=M-(O*3>>3)|0;O=(M>>1)-O|0;M=M-O|0;Q=(K+1>>1)+Q|0;O=(M+1>>1)+O|0;c[L>>2]=K-Q;c[N>>2]=M-O;c[P>>2]=O;c[R>>2]=Q;F=F+64|0}while((F|0)<(C|0))}}if(h){if(g){break}}else{if((c[A>>2]|0)==0|g){break}}if(!e?(c[z>>2]|0)==0:0){L=B+ -4|0;P=B+ -12|0;Q=D+ -236|0;M=D+ -228|0;F=c[Q>>2]|0;O=c[M>>2]|0;N=O+(c[L>>2]|0)|0;R=F+(c[P>>2]|0)|0;F=F-(R+1>>1)|0;O=O-(N+1>>1)-(F+1>>1)|0;F=(O+1>>1)+F|0;N=O+N|0;O=(N>>1)-O|0;N=N-((O*3|0)+4>>3)|0;O=O-(N>>7)+(N>>10)-(N*3>>4)|0;N=N-(O*3>>3)|0;O=(N>>1)-O|0;N=N-O|0;R=F+R|0;F=(R>>1)-F|0;R=R-((F*3|0)+4>>3)|0;F=F-(R>>7)+(R>>10)-(R*3>>4)|0;R=R-(F*3>>3)|0;F=(R>>1)-F|0;R=R-F|0;O=(N+1>>1)+O|0;F=(R+1>>1)+F|0;c[L>>2]=N-O;c[P>>2]=R-F;c[Q>>2]=F;c[M>>2]=O;M=B+ -8|0;O=B+ -16|0;Q=D+ -240|0;F=D+ -232|0;P=c[Q>>2]|0;R=c[F>>2]|0;L=R+(c[M>>2]|0)|0;N=P+(c[O>>2]|0)|0;P=P-(N+1>>1)|0;R=R-(L+1>>1)-(P+1>>1)|0;P=(R+1>>1)+P|0;L=R+L|0;R=(L>>1)-R|0;L=L-((R*3|0)+4>>3)|0;R=R-(L>>7)+(L>>10)-(L*3>>4)|0;L=L-(R*3>>3)|0;R=(L>>1)-R|0;L=L-R|0;N=P+N|0;P=(N>>1)-P|0;N=N-((P*3|0)+4>>3)|0;P=P-(N>>7)+(N>>10)-(N*3>>4)|0;N=N-(P*3>>3)|0;P=(N>>1)-P|0;N=N-P|0;R=(L+1>>1)+R|0;P=(N+1>>1)+P|0;c[M>>2]=L-R;c[O>>2]=N-P;c[Q>>2]=P;c[F>>2]=R;F=-64}else{F=-64}do{K=D+((F|15)<<2)|0;O=D+((F|13)<<2)|0;P=D+(F+21<<2)|0;L=D+(F+23<<2)|0;R=c[P>>2]|0;N=c[L>>2]|0;M=N+(c[K>>2]|0)|0;Q=R+(c[O>>2]|0)|0;R=R-(Q+1>>1)|0;N=N-(M+1>>1)-(R+1>>1)|0;R=(N+1>>1)+R|0;M=N+M|0;N=(M>>1)-N|0;M=M-((N*3|0)+4>>3)|0;N=N-(M>>7)+(M>>10)-(M*3>>4)|0;M=M-(N*3>>3)|0;N=(M>>1)-N|0;M=M-N|0;Q=R+Q|0;R=(Q>>1)-R|0;Q=Q-((R*3|0)+4>>3)|0;R=R-(Q>>7)+(Q>>10)-(Q*3>>4)|0;Q=Q-(R*3>>3)|0;R=(Q>>1)-R|0;Q=Q-R|0;N=(M+1>>1)+N|0;R=(Q+1>>1)+R|0;c[K>>2]=M-N;c[O>>2]=Q-R;c[P>>2]=R;c[L>>2]=N;L=D+((F|14)<<2)|0;N=D+((F|12)<<2)|0;P=D+(F+20<<2)|0;R=D+(F+22<<2)|0;O=c[P>>2]|0;Q=c[R>>2]|0;K=Q+(c[L>>2]|0)|0;M=O+(c[N>>2]|0)|0;O=O-(M+1>>1)|0;Q=Q-(K+1>>1)-(O+1>>1)|0;O=(Q+1>>1)+O|0;K=Q+K|0;Q=(K>>1)-Q|0;K=K-((Q*3|0)+4>>3)|0;Q=Q-(K>>7)+(K>>10)-(K*3>>4)|0;K=K-(Q*3>>3)|0;Q=(K>>1)-Q|0;K=K-Q|0;M=O+M|0;O=(M>>1)-O|0;M=M-((O*3|0)+4>>3)|0;O=O-(M>>7)+(M>>10)-(M*3>>4)|0;M=M-(O*3>>3)|0;O=(M>>1)-O|0;M=M-O|0;Q=(K+1>>1)+Q|0;O=(M+1>>1)+O|0;c[L>>2]=K-Q;c[N>>2]=M-O;c[P>>2]=O;c[R>>2]=Q;F=F+16|0}while((F|0)<-16)}}while(0);if(!(e|(s|0)>=(r|0))){F=s;do{Jb(B+(F<<2)|0);F=F+64|0}while((F|0)<(r|0))}if(!(g|(u|0)>=(t|0))){F=u;do{Jb(D+(F<<2)|0);Jb(D+((F|16)<<2)|0);Jb(D+((F|32)<<2)|0);F=F+64|0}while((F|0)<(t|0))}do{if(y){if(!(!m?(c[x>>2]|0)==0:0)){b=339}do{if((b|0)==339){b=0;if(!e?(c[z>>2]|0)==0:0){break}R=D+ -512|0;c[R>>2]=(c[R>>2]|0)-(c[D+ -256>>2]|0)}}while(0);if(!(!l?(c[w>>2]|0)==0:0)){b=344}do{if((b|0)==344){b=0;if(!e?(c[z>>2]|0)==0:0){break}c[a+(E<<3)+34376>>2]=c[D>>2]}}while(0);if(!(!h?(c[A>>2]|0)==0:0)){b=349}do{if((b|0)==349){b=0;if(!e?(c[z>>2]|0)==0:0){break}R=D+ -256|0;c[R>>2]=(c[R>>2]|0)-(c[a+(E<<3)+34376>>2]|0)}}while(0);if(!(!m?(c[x>>2]|0)==0:0)){b=354}do{if((b|0)==354){b=0;if(!g?(c[z>>2]|0)==0:0){break}R=B+ -320|0;c[R>>2]=(c[R>>2]|0)-(c[B+ -64>>2]|0)}}while(0);if(!(!l?(c[w>>2]|0)==0:0)){b=359}do{if((b|0)==359){if(!g?(c[z>>2]|0)==0:0){break}c[a+(E<<3)+34380>>2]=c[B+192>>2]}}while(0);if(!h?(c[A>>2]|0)==0:0){b=367}else{b=364}do{if((b|0)==364){if(!g?(c[z>>2]|0)==0:0){b=368;break}b=B+ -64|0;c[b>>2]=(c[b>>2]|0)-(c[a+(E<<3)+34380>>2]|0);b=367}}while(0);if((b|0)==367){if(g){b=393}else{b=368}}if((b|0)==368){b=0;if(!f?(c[A>>2]|0)==0:0){b=386}else{do{if(!e?(c[z>>2]|0)==0:0){if(!(!j?(c[A>>2]|0)==0:0)){P=B+192|0;R=c[P>>2]|0;Q=(c[D>>2]|0)-(R+2>>2)|0;R=R-(Q>>5)-(Q>>9)-(Q>>13)-(Q+1>>1)|0;c[P>>2]=R;c[D>>2]=Q-(R+2>>2)}if(!h?(c[A>>2]|0)==0:0){break}O=B+ -64|0;R=D+ -256|0;Q=c[O>>2]|0;P=(c[R>>2]|0)-(Q+2>>2)|0;Q=Q-(P>>5)-(P>>9)-(P>>13)-(P+1>>1)|0;c[O>>2]=Q;c[R>>2]=P-(Q+2>>2)}}while(0);if(!(!j?(c[A>>2]|0)==0:0)){O=D+64|0;R=D+128|0;Q=c[O>>2]|0;P=(c[R>>2]|0)-(Q+2>>2)|0;Q=Q-(P>>5)-(P>>9)-(P>>13)-(P+1>>1)|0;c[O>>2]=Q;c[R>>2]=P-(Q+2>>2)}if(!(!h?(c[A>>2]|0)==0:0)){O=D+ -192|0;R=D+ -128|0;Q=c[O>>2]|0;P=(c[R>>2]|0)-(Q+2>>2)|0;Q=Q-(P>>5)-(P>>9)-(P>>13)-(P+1>>1)|0;c[O>>2]=Q;c[R>>2]=P-(Q+2>>2)}if(!f?(c[A>>2]|0)==0:0){b=386}}if((b|0)==386){b=0;if(!e?(c[z>>2]|0)==0:0){N=B+ -64|0;P=B+192|0;R=D+ -256|0;L=c[R>>2]|0;K=c[D>>2]|0;F=K+(c[N>>2]|0)|0;Q=L+(c[P>>2]|0)|0;O=Q-(F+2>>2)|0;M=F-(O>>5)-(O>>9)-(O>>13)-(O+1>>1)|0;O=O-(M+2>>2)|0;F=(M+1>>1)+(K-(F+1>>1))|0;Q=(O+1>>1)+(L-(Q+1>>1))|0;c[N>>2]=M-F;c[P>>2]=O-Q;c[R>>2]=Q}else{Q=D+ -256|0;F=c[Q>>2]|0;R=(c[D>>2]|0)-(F+2>>2)|0;F=F-(R>>5)-(R>>9)-(R>>13)-(R+1>>1)|0;c[Q>>2]=F;F=R-(F+2>>2)|0}c[D>>2]=F;L=D+ -192|0;N=D+64|0;P=D+ -128|0;R=D+128|0;J=c[P>>2]|0;I=c[R>>2]|0;Q=I+(c[L>>2]|0)|0;O=J+(c[N>>2]|0)|0;M=O-(Q+2>>2)|0;K=Q-(M>>5)-(M>>9)-(M>>13)-(M+1>>1)|0;M=M-(K+2>>2)|0;Q=(K+1>>1)+(I-(Q+1>>1))|0;O=(M+1>>1)+(J-(O+1>>1))|0;c[L>>2]=K-Q;c[N>>2]=M-O;c[P>>2]=O;c[R>>2]=Q}if(!g){if(!((c[z>>2]|0)==0|f)){b=394}}else{b=393}}if((b|0)==393?(b=0,!f):0){b=394}if((b|0)==394?(b=0,(c[A>>2]|0)==0):0){O=B+ -64|0;R=B+192|0;Q=c[O>>2]|0;P=(c[R>>2]|0)-(Q+2>>2)|0;Q=Q-(P>>5)-(P>>9)-(P>>13)-(P+1>>1)|0;c[O>>2]=Q;c[R>>2]=P-(Q+2>>2)}if(!(!m?(c[x>>2]|0)==0:0)){b=398}do{if((b|0)==398){b=0;if(!e?(c[z>>2]|0)==0:0){break}R=D+ -512|0;c[R>>2]=(c[R>>2]|0)+(c[D+ -256>>2]|0)}}while(0);if(!(!l?(c[w>>2]|0)==0:0)){b=403}do{if((b|0)==403){b=0;if(!e?(c[z>>2]|0)==0:0){break}c[a+(E<<3)+34392>>2]=c[D>>2]}}while(0);if(!(!h?(c[A>>2]|0)==0:0)){b=408}do{if((b|0)==408){b=0;if(!e?(c[z>>2]|0)==0:0){break}R=D+ -256|0;c[R>>2]=(c[R>>2]|0)+(c[a+(E<<3)+34392>>2]|0)}}while(0);if(!(!m?(c[x>>2]|0)==0:0)){b=413}do{if((b|0)==413){b=0;if(!g?(c[z>>2]|0)==0:0){break}R=B+ -320|0;c[R>>2]=(c[R>>2]|0)+(c[B+ -64>>2]|0)}}while(0);if(!(!l?(c[w>>2]|0)==0:0)){b=418}do{if((b|0)==418){b=0;if(!g?(c[z>>2]|0)==0:0){break}c[a+(E<<3)+34396>>2]=c[B+192>>2]}}while(0);if(!h?(c[A>>2]|0)==0:0){break}if(!g?(c[z>>2]|0)==0:0){break}R=B+ -64|0;c[R>>2]=(c[R>>2]|0)+(c[a+(E<<3)+34396>>2]|0)}}while(0);if(!d){D=B+ -512|0;G=B+ -256|0;E=B+ -448|0;F=B+ -192|0;if((c[v>>2]|0)==0){de(D,G,E,F);F=B+ -384|0;de(F,B+ -128|0,B+ -320|0,B+ -64|0);E=c[D>>2]|0;F=c[F>>2]|0}else{L=c[E>>2]>>1;N=c[F>>2]>>1;R=N+(c[D>>2]>>1)|0;P=(c[G>>2]>>1)-L|0;Q=R-P>>1;N=Q-N|0;L=Q-L|0;R=R-L|0;c[D>>2]=R;c[G>>2]=N+P;c[E>>2]=N;c[F>>2]=L;L=B+ -384|0;N=B+ -128|0;P=B+ -320|0;E=B+ -64|0;Q=c[P>>2]>>1;O=c[E>>2]>>1;F=O+(c[L>>2]>>1)|0;M=(c[N>>2]>>1)-Q|0;K=F-M>>1;O=K-O|0;Q=K-Q|0;F=F-Q|0;c[L>>2]=F;c[N>>2]=O+M;c[P>>2]=O;c[E>>2]=Q;E=R}R=F-E|0;c[B+ -384>>2]=R;c[D>>2]=(R+1>>1)+E}}while((n|0)<(p|0));i=k;return}function Ob(a){a=a|0;var b=0,e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0;b=i;f=c[a+34292>>2]|0;g=c[a+34272>>2]|0;e=c[a+34128>>2]|0;h=a+34156|0;r=c[h>>2]|0;if((c[a+34176>>2]|0)==0){if((r|0)<=0){i=b;return 0}j=(e|0)==1;k=(e&-2|0)==2|j;l=(e|0)==2;m=j?4:16;n=a+34116|0;o=a+34117|0;p=a+116|0;q=0;do{s=(q|0)<1|k^1;r=s|l;x=r?s?16:8:m;if(r|j^1){r=(x|0)==8?8176:8096}else{r=8160;x=4}u=c[f+(g*240|0)+(q<<2)>>2]|0;t=c[f+(g*240|0)+(q<<2)+64>>2]|0;G=d[n>>0]|0;F=c[f+(g*240|0)+(q<<2)+128>>2]|0;B=d[o>>0]|0;A=c[a+(q<<2)+34460>>2]|0;v=u+12|0;y=u+8|0;u=u+16|0;H=c[p>>2]|0;D=(H+ -2|0)>>>0<2;z=F+(B*20|0)+12|0;C=F+(B*20|0)+8|0;B=F+(B*20|0)+16|0;F=t+(G*20|0)+12|0;w=t+(G*20|0)+8|0;G=t+(G*20|0)+16|0;t=0;do{s=c[r+(t<<2)>>2]|0;I=A+(s<<2)|0;if((t|0)!=0){if((H|0)==3){H=3}else{K=c[F>>2]|0;M=c[I>>2]|0;L=c[G>>2]|0;J=M>>31;M=(M>>>31)+(c[w>>2]|0)+(J^M)|0;if((K|0)==0){K=M>>L}else{Ge(M|0,0,K|0,0)|0;K=E>>>L}c[I>>2]=(K^J)-J}}else{K=c[v>>2]|0;M=c[I>>2]|0;L=c[u>>2]|0;J=M>>31;M=(M>>>31)+(c[y>>2]|0)+(J^M)|0;if((K|0)==0){K=M>>L}else{Ge(M|0,0,K|0,0)|0;K=E>>>L}c[I>>2]=(K^J)-J}if(!D){I=1;do{M=c[z>>2]|0;J=A+(I+s<<2)|0;L=c[J>>2]|0;N=c[B>>2]|0;K=L>>31;L=(L>>>31)+(c[C>>2]|0)+(K^L)|0;if((M|0)==0){L=L>>N}else{Ge(L|0,0,M|0,0)|0;L=E>>>N}c[J>>2]=(L^K)-K;I=I+1|0}while((I|0)!=16)}t=t+1|0}while((t|0)<(x|0));q=q+1|0;r=c[h>>2]|0}while((q|0)<(r|0))}f=a+34156|0;if((r|0)<=0){i=b;return 0}if((e|0)==1){e=0;do{g=c[a+(e<<2)+34460>>2]|0;if((e|0)>0){c[a+(e<<6)+32960>>2]=c[g+(c[2040]<<2)>>2];c[a+(e<<6)+32964>>2]=c[g+(c[8164>>2]<<2)>>2];c[a+(e<<6)+32968>>2]=c[g+(c[8168>>2]<<2)>>2];c[a+(e<<6)+32972>>2]=c[g+(c[8172>>2]<<2)>>2]}else{c[a+(e<<6)+32960>>2]=c[g+(c[8336>>2]<<2)>>2];c[a+(e<<6)+32964>>2]=c[g+(c[8340>>2]<<2)>>2];c[a+(e<<6)+32968>>2]=c[g+(c[8344>>2]<<2)>>2];c[a+(e<<6)+32972>>2]=c[g+(c[8348>>2]<<2)>>2];c[a+(e<<6)+32976>>2]=c[g+(c[8352>>2]<<2)>>2];c[a+(e<<6)+32980>>2]=c[g+(c[8356>>2]<<2)>>2];c[a+(e<<6)+32984>>2]=c[g+(c[8360>>2]<<2)>>2];c[a+(e<<6)+32988>>2]=c[g+(c[8364>>2]<<2)>>2];c[a+(e<<6)+32992>>2]=c[g+(c[8368>>2]<<2)>>2];c[a+(e<<6)+32996>>2]=c[g+(c[8372>>2]<<2)>>2];c[a+(e<<6)+33e3>>2]=c[g+(c[8376>>2]<<2)>>2];c[a+(e<<6)+33004>>2]=c[g+(c[8380>>2]<<2)>>2];c[a+(e<<6)+33008>>2]=c[g+(c[8384>>2]<<2)>>2];c[a+(e<<6)+33012>>2]=c[g+(c[8388>>2]<<2)>>2];c[a+(e<<6)+33016>>2]=c[g+(c[8392>>2]<<2)>>2];c[a+(e<<6)+33020>>2]=c[g+(c[8396>>2]<<2)>>2];r=c[f>>2]|0}e=e+1|0}while((e|0)<(r|0));i=b;return 0}else if((e|0)==2){e=0;do{g=c[a+(e<<2)+34460>>2]|0;if((e|0)>0){c[a+(e<<6)+32960>>2]=c[g+(c[2044]<<2)>>2];c[a+(e<<6)+32964>>2]=c[g+(c[8180>>2]<<2)>>2];c[a+(e<<6)+32968>>2]=c[g+(c[8184>>2]<<2)>>2];c[a+(e<<6)+32972>>2]=c[g+(c[8188>>2]<<2)>>2];c[a+(e<<6)+32976>>2]=c[g+(c[8192>>2]<<2)>>2];c[a+(e<<6)+32980>>2]=c[g+(c[8196>>2]<<2)>>2];c[a+(e<<6)+32984>>2]=c[g+(c[8200>>2]<<2)>>2];c[a+(e<<6)+32988>>2]=c[g+(c[8204>>2]<<2)>>2]}else{c[a+(e<<6)+32960>>2]=c[g+(c[8336>>2]<<2)>>2];c[a+(e<<6)+32964>>2]=c[g+(c[8340>>2]<<2)>>2];c[a+(e<<6)+32968>>2]=c[g+(c[8344>>2]<<2)>>2];c[a+(e<<6)+32972>>2]=c[g+(c[8348>>2]<<2)>>2];c[a+(e<<6)+32976>>2]=c[g+(c[8352>>2]<<2)>>2];c[a+(e<<6)+32980>>2]=c[g+(c[8356>>2]<<2)>>2];c[a+(e<<6)+32984>>2]=c[g+(c[8360>>2]<<2)>>2];c[a+(e<<6)+32988>>2]=c[g+(c[8364>>2]<<2)>>2];c[a+(e<<6)+32992>>2]=c[g+(c[8368>>2]<<2)>>2];c[a+(e<<6)+32996>>2]=c[g+(c[8372>>2]<<2)>>2];c[a+(e<<6)+33e3>>2]=c[g+(c[8376>>2]<<2)>>2];c[a+(e<<6)+33004>>2]=c[g+(c[8380>>2]<<2)>>2];c[a+(e<<6)+33008>>2]=c[g+(c[8384>>2]<<2)>>2];c[a+(e<<6)+33012>>2]=c[g+(c[8388>>2]<<2)>>2];c[a+(e<<6)+33016>>2]=c[g+(c[8392>>2]<<2)>>2];c[a+(e<<6)+33020>>2]=c[g+(c[8396>>2]<<2)>>2];r=c[f>>2]|0}e=e+1|0}while((e|0)<(r|0));i=b;return 0}else{e=0;do{N=c[a+(e<<2)+34460>>2]|0;c[a+(e<<6)+32960>>2]=c[N+(c[8336>>2]<<2)>>2];c[a+(e<<6)+32964>>2]=c[N+(c[8340>>2]<<2)>>2];c[a+(e<<6)+32968>>2]=c[N+(c[8344>>2]<<2)>>2];c[a+(e<<6)+32972>>2]=c[N+(c[8348>>2]<<2)>>2];c[a+(e<<6)+32976>>2]=c[N+(c[8352>>2]<<2)>>2];c[a+(e<<6)+32980>>2]=c[N+(c[8356>>2]<<2)>>2];c[a+(e<<6)+32984>>2]=c[N+(c[8360>>2]<<2)>>2];c[a+(e<<6)+32988>>2]=c[N+(c[8364>>2]<<2)>>2];c[a+(e<<6)+32992>>2]=c[N+(c[8368>>2]<<2)>>2];c[a+(e<<6)+32996>>2]=c[N+(c[8372>>2]<<2)>>2];c[a+(e<<6)+33e3>>2]=c[N+(c[8376>>2]<<2)>>2];c[a+(e<<6)+33004>>2]=c[N+(c[8380>>2]<<2)>>2];c[a+(e<<6)+33008>>2]=c[N+(c[8384>>2]<<2)>>2];c[a+(e<<6)+33012>>2]=c[N+(c[8388>>2]<<2)>>2];c[a+(e<<6)+33016>>2]=c[N+(c[8392>>2]<<2)>>2];c[a+(e<<6)+33020>>2]=c[N+(c[8396>>2]<<2)>>2];e=e+1|0}while((e|0)<(c[f>>2]|0));i=b;return 0}return 0}function Pb(a){a=a|0;var b=0,d=0,e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0;g=i;m=c[a+34128>>2]|0;if((m+ -1|0)>>>0<2){j=1}else{j=c[a+34156>>2]|0}h=c[a+34328>>2]|0;f=h+ -1|0;o=a+32960|0;d=be(a,f)|0;e=d&3;d=d&12;b=ae(o,m)|0;c[a+33984>>2]=2-b;ce(a,o,f,m);if((j|0)>0){k=h+ -2|0;l=0;do{n=a+(l<<6)+32960|0;if((e|0)==0){c[n>>2]=(c[n>>2]|0)-(c[(c[a+(l<<2)+34788>>2]|0)+(k*40|0)+8>>2]|0)}else if((e|0)==2){c[n>>2]=(c[n>>2]|0)-((c[(c[a+(l<<2)+34852>>2]|0)+(f*40|0)+8>>2]|0)+(c[(c[a+(l<<2)+34788>>2]|0)+(k*40|0)+8>>2]|0)>>1)}else if((e|0)==1){c[n>>2]=(c[n>>2]|0)-(c[(c[a+(l<<2)+34852>>2]|0)+(f*40|0)+8>>2]|0)}if((d|0)==4){n=c[(c[a+(l<<2)+34852>>2]|0)+(f*40|0)+36>>2]|0;o=a+(l<<6)+32976|0;c[o>>2]=(c[o>>2]|0)-(c[n+12>>2]|0);o=a+(l<<6)+32992|0;c[o>>2]=(c[o>>2]|0)-(c[n+16>>2]|0);o=a+(l<<6)+33008|0;c[o>>2]=(c[o>>2]|0)-(c[n+20>>2]|0)}else if((d|0)==0){n=c[(c[a+(l<<2)+34788>>2]|0)+(k*40|0)+36>>2]|0;o=a+(l<<6)+32964|0;c[o>>2]=(c[o>>2]|0)-(c[n>>2]|0);o=a+(l<<6)+32968|0;c[o>>2]=(c[o>>2]|0)-(c[n+4>>2]|0);o=a+(l<<6)+32972|0;c[o>>2]=(c[o>>2]|0)-(c[n+8>>2]|0)}n=c[a+(l<<2)+34460>>2]|0;if((b|0)==0){o=0;do{q=o+192|0;p=q|5;r=n+(p<<2)|0;c[r>>2]=(c[r>>2]|0)-(c[n+(p+ -64<<2)>>2]|0);r=q|1;p=n+(r<<2)|0;c[p>>2]=(c[p>>2]|0)-(c[n+(r+ -64<<2)>>2]|0);q=q|6;p=n+(q<<2)|0;c[p>>2]=(c[p>>2]|0)-(c[n+(q+ -64<<2)>>2]|0);p=o+128|0;q=p|5;r=n+(q<<2)|0;c[r>>2]=(c[r>>2]|0)-(c[n+(q+ -64<<2)>>2]|0);r=p|1;q=n+(r<<2)|0;c[q>>2]=(c[q>>2]|0)-(c[n+(r+ -64<<2)>>2]|0);p=p|6;q=n+(p<<2)|0;c[q>>2]=(c[q>>2]|0)-(c[n+(p+ -64<<2)>>2]|0);q=o+64|0;p=q|5;r=n+(p<<2)|0;c[r>>2]=(c[r>>2]|0)-(c[n+(p+ -64<<2)>>2]|0);r=q|1;p=n+(r<<2)|0;c[p>>2]=(c[p>>2]|0)-(c[n+(r+ -64<<2)>>2]|0);q=q|6;p=n+(q<<2)|0;c[p>>2]=(c[p>>2]|0)-(c[n+(q+ -64<<2)>>2]|0);o=o+16|0}while((o|0)<64)}else if((b|0)==1){o=0;do{q=o|58;r=n+(q<<2)|0;c[r>>2]=(c[r>>2]|0)-(c[n+(q+ -16<<2)>>2]|0);r=o|50;q=n+(r<<2)|0;c[q>>2]=(c[q>>2]|0)-(c[n+(r+ -16<<2)>>2]|0);q=o|57;r=n+(q<<2)|0;c[r>>2]=(c[r>>2]|0)-(c[n+(q+ -16<<2)>>2]|0);r=o|42;q=n+(r<<2)|0;c[q>>2]=(c[q>>2]|0)-(c[n+(r+ -16<<2)>>2]|0);q=o|34;r=n+(q<<2)|0;c[r>>2]=(c[r>>2]|0)-(c[n+(q+ -16<<2)>>2]|0);r=o|41;q=n+(r<<2)|0;c[q>>2]=(c[q>>2]|0)-(c[n+(r+ -16<<2)>>2]|0);q=o|26;r=n+(q<<2)|0;c[r>>2]=(c[r>>2]|0)-(c[n+(q+ -16<<2)>>2]|0);r=o|18;q=n+(r<<2)|0;c[q>>2]=(c[q>>2]|0)-(c[n+(r+ -16<<2)>>2]|0);q=o|25;r=n+(q<<2)|0;c[r>>2]=(c[r>>2]|0)-(c[n+(q+ -16<<2)>>2]|0);o=o+64|0}while((o|0)<193)}l=l+1|0}while((l|0)!=(j|0))}if((m|0)==2){j=(e|0)==1;k=h+ -2|0;h=1;do{l=a+(h<<6)+32960|0;do{if(!j){if((e|0)==0){c[l>>2]=(c[l>>2]|0)-(c[(c[a+(h<<2)+34788>>2]|0)+(k*40|0)+8>>2]|0);break}else if((e|0)==2){c[l>>2]=(c[l>>2]|0)-((c[(c[a+(h<<2)+34788>>2]|0)+(k*40|0)+8>>2]|0)+1+(c[(c[a+(h<<2)+34852>>2]|0)+(f*40|0)+8>>2]|0)>>1);break}else{break}}else{c[l>>2]=(c[l>>2]|0)-(c[(c[a+(h<<2)+34852>>2]|0)+(f*40|0)+8>>2]|0)}}while(0);if((d|0)==0){q=a+(h<<2)+34788|0;r=a+(h<<6)+32976|0;c[r>>2]=(c[r>>2]|0)-(c[(c[(c[q>>2]|0)+(k*40|0)+36>>2]|0)+16>>2]|0);r=a+(h<<6)+32964|0;c[r>>2]=(c[r>>2]|0)-(c[c[(c[q>>2]|0)+(k*40|0)+36>>2]>>2]|0);r=a+(h<<6)+32980|0;c[r>>2]=(c[r>>2]|0)-(c[(c[(c[q>>2]|0)+(k*40|0)+36>>2]|0)+8>>2]|0)}else if((d|0)!=4){if(j){r=a+(h<<6)+32984|0;c[r>>2]=(c[r>>2]|0)-(c[a+(h<<6)+32968>>2]|0)}}else{q=a+(h<<2)+34852|0;r=a+(h<<6)+32976|0;c[r>>2]=(c[r>>2]|0)-(c[(c[(c[q>>2]|0)+(f*40|0)+36>>2]|0)+16>>2]|0);r=a+(h<<6)+32968|0;p=c[r>>2]|0;o=a+(h<<6)+32984|0;c[o>>2]=(c[o>>2]|0)-p;c[r>>2]=p-(c[(c[(c[q>>2]|0)+(f*40|0)+36>>2]|0)+12>>2]|0)}l=c[a+(h<<2)+34460>>2]|0;if((b|0)==0){r=l+276|0;c[r>>2]=(c[r>>2]|0)-(c[l+20>>2]|0);r=l+260|0;c[r>>2]=(c[r>>2]|0)-(c[l+4>>2]|0);r=l+280|0;c[r>>2]=(c[r>>2]|0)-(c[l+24>>2]|0);r=l+340|0;c[r>>2]=(c[r>>2]|0)-(c[l+84>>2]|0);r=l+324|0;c[r>>2]=(c[r>>2]|0)-(c[l+68>>2]|0);r=l+344|0;c[r>>2]=(c[r>>2]|0)-(c[l+88>>2]|0);r=l+404|0;c[r>>2]=(c[r>>2]|0)-(c[l+148>>2]|0);r=l+388|0;c[r>>2]=(c[r>>2]|0)-(c[l+132>>2]|0);r=l+408|0;c[r>>2]=(c[r>>2]|0)-(c[l+152>>2]|0);r=l+468|0;c[r>>2]=(c[r>>2]|0)-(c[l+212>>2]|0);r=l+452|0;c[r>>2]=(c[r>>2]|0)-(c[l+196>>2]|0);r=l+472|0;c[r>>2]=(c[r>>2]|0)-(c[l+216>>2]|0)}else if((b|0)==1){m=48;do{r=m|10;q=l+(r<<2)|0;c[q>>2]=(c[q>>2]|0)-(c[l+(r+ -16<<2)>>2]|0);q=m|2;r=l+(q<<2)|0;c[r>>2]=(c[r>>2]|0)-(c[l+(q+ -16<<2)>>2]|0);r=m|9;q=l+(r<<2)|0;c[q>>2]=(c[q>>2]|0)-(c[l+(r+ -16<<2)>>2]|0);q=m+64|0;r=q|10;p=l+(r<<2)|0;c[p>>2]=(c[p>>2]|0)-(c[l+(r+ -16<<2)>>2]|0);p=q|2;r=l+(p<<2)|0;c[r>>2]=(c[r>>2]|0)-(c[l+(p+ -16<<2)>>2]|0);q=q|9;r=l+(q<<2)|0;c[r>>2]=(c[r>>2]|0)-(c[l+(q+ -16<<2)>>2]|0);m=m+ -16|0}while((m|0)>0)}h=h+1|0}while((h|0)!=3);i=g;return}else if((m|0)==1){h=h+ -2|0;j=1;do{k=a+(j<<6)+32960|0;if((e|0)==0){c[k>>2]=(c[k>>2]|0)-(c[(c[a+(j<<2)+34788>>2]|0)+(h*40|0)+8>>2]|0)}else if((e|0)==1){c[k>>2]=(c[k>>2]|0)-(c[(c[a+(j<<2)+34852>>2]|0)+(f*40|0)+8>>2]|0)}else if((e|0)==2){c[k>>2]=(c[k>>2]|0)-((c[(c[a+(j<<2)+34788>>2]|0)+(h*40|0)+8>>2]|0)+1+(c[(c[a+(j<<2)+34852>>2]|0)+(f*40|0)+8>>2]|0)>>1)}if((d|0)==4){r=a+(j<<6)+32968|0;c[r>>2]=(c[r>>2]|0)-(c[(c[(c[a+(j<<2)+34852>>2]|0)+(f*40|0)+36>>2]|0)+4>>2]|0)}else if((d|0)==0){r=a+(j<<6)+32964|0;c[r>>2]=(c[r>>2]|0)-(c[c[(c[a+(j<<2)+34788>>2]|0)+(h*40|0)+36>>2]>>2]|0)}k=c[a+(j<<2)+34460>>2]|0;if((b|0)==0){r=k+148|0;c[r>>2]=(c[r>>2]|0)-(c[k+20>>2]|0);r=k+132|0;c[r>>2]=(c[r>>2]|0)-(c[k+4>>2]|0);r=k+152|0;c[r>>2]=(c[r>>2]|0)-(c[k+24>>2]|0);r=k+212|0;c[r>>2]=(c[r>>2]|0)-(c[k+84>>2]|0);r=k+196|0;c[r>>2]=(c[r>>2]|0)-(c[k+68>>2]|0);r=k+216|0;c[r>>2]=(c[r>>2]|0)-(c[k+88>>2]|0)}else if((b|0)==1){r=k+104|0;c[r>>2]=(c[r>>2]|0)-(c[k+40>>2]|0);r=k+72|0;c[r>>2]=(c[r>>2]|0)-(c[k+8>>2]|0);r=k+100|0;c[r>>2]=(c[r>>2]|0)-(c[k+36>>2]|0);r=k+232|0;c[r>>2]=(c[r>>2]|0)-(c[k+168>>2]|0);r=k+200|0;c[r>>2]=(c[r>>2]|0)-(c[k+136>>2]|0);r=k+228|0;c[r>>2]=(c[r>>2]|0)-(c[k+164>>2]|0)}j=j+1|0}while((j|0)!=3);i=g;return}else{i=g;return}}function Qb(a,b){a=a|0;b=b|0;var d=0,e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0;f=i;k=c[a+34328>>2]|0;e=k+ -1|0;j=a+34156|0;if((c[j>>2]|0)<=0){i=f;return}g=a+34128|0;h=a+34276|0;k=k+ -2|0;m=a+34280|0;p=b+580|0;l=b+564|0;n=b+572|0;o=0;do{r=c[g>>2]|0;q=(o|0)>0;if(q){if((r|0)!=2){r=(r|0)==1;if(r){r=8160;s=4}else{s=r?4:16;d=6}}else{s=8;d=6}}else{s=16;d=6}if((d|0)==6){d=0;r=(s|0)==8?8176:8096}v=(1<<c[b+((q&1)<<2)+500>>2])+ -1|0;w=v<<1|1;u=c[a+(o<<2)+34460>>2]|0;q=0;t=0;do{x=c[r+(t<<2)>>2]|0;z=1;while(1){y=z+1|0;if(!(((c[u+(z+x<<2)>>2]|0)+v|0)>>>0<w>>>0)){d=11;break}if((y|0)<16){z=y}else{break}}if((d|0)==11){d=0;q=q|1<<t}t=t+1|0}while((t|0)<(s|0));r=c[a+(o<<2)+34788>>2]|0;c[r+(e*40|0)+4>>2]=q;c[a+(o<<2)+33988>>2]=q;do{if((s|0)!=16){u=q&65535;t=(u|0)==0;if((s|0)==8){if(t){s=0}else{s=0;do{s=(c[1896+((u&15)<<2)>>2]|0)+s|0;u=u>>4}while((u|0)!=0);s=s<<1}if((c[h>>2]|0)!=0){if((c[m>>2]|0)==0){r=(c[(c[a+(o<<2)+34852>>2]|0)+(e*40|0)+4>>2]|0)>>>6&1}else{r=1}}else{r=(c[r+(k*40|0)+4>>2]|0)>>>1&1}t=c[p>>2]|0;if((t|0)!=1)if((t|0)==0){z=q<<2;q=(z&12|q<<1&2|z&48|z&192|r)^q}else{q=q^255}r=s+ -3+(c[l>>2]|0)|0;c[l>>2]=r;do{if((r+16|0)>>>0>31){if((r|0)<0){c[l>>2]=-16;r=-16;break}else{c[l>>2]=15;r=15;break}}}while(0);s=(c[n>>2]|0)+(13-s)|0;c[n>>2]=s;do{if((s+16|0)>>>0>31){if((s|0)<0){c[n>>2]=-16;s=-16;break}else{c[n>>2]=15;s=15;break}}}while(0);do{if((r|0)<0){if((r|0)<(s|0)){c[p>>2]=1;break}else{c[p>>2]=2;break}}else{if((s|0)<0){c[p>>2]=2;break}else{c[p>>2]=0;break}}}while(0);c[a+(o<<2)+34052>>2]=q;break}else{if(t){s=0}else{s=0;do{s=(c[1896+((u&15)<<2)>>2]|0)+s|0;u=u>>4}while((u|0)!=0);s=s<<2}if((c[h>>2]|0)!=0){if((c[m>>2]|0)==0){r=(c[(c[a+(o<<2)+34852>>2]|0)+(e*40|0)+4>>2]|0)>>>2&1}else{r=1}}else{r=(c[r+(k*40|0)+4>>2]|0)>>>1&1}t=c[p>>2]|0;if((t|0)!=1)if((t|0)==0){q=(q<<2&12|q<<1&2|r)^q}else{q=q^15}r=s+ -3+(c[l>>2]|0)|0;c[l>>2]=r;do{if((r+16|0)>>>0>31){if((r|0)<0){c[l>>2]=-16;r=-16;break}else{c[l>>2]=15;r=15;break}}}while(0);s=(c[n>>2]|0)+(13-s)|0;c[n>>2]=s;do{if((s+16|0)>>>0>31){if((s|0)<0){c[n>>2]=-16;s=-16;break}else{c[n>>2]=15;s=15;break}}}while(0);do{if((r|0)<0){if((r|0)<(s|0)){c[p>>2]=1;break}else{c[p>>2]=2;break}}else{if((s|0)<0){c[p>>2]=2;break}else{c[p>>2]=0;break}}}while(0);c[a+(o<<2)+34052>>2]=q;break}}else{t=q&65535;if((t|0)==0){s=0}else{s=0;do{s=(c[1896+((t&15)<<2)>>2]|0)+s|0;t=t>>4}while((t|0)!=0)}if((c[h>>2]|0)!=0){if((c[m>>2]|0)==0){u=(c[(c[a+(o<<2)+34852>>2]|0)+(e*40|0)+4>>2]|0)>>>10&1}else{u=1}}else{u=(c[r+(k*40|0)+4>>2]|0)>>>5&1}w=q<<2;t=(o|0)!=0|0;r=b+(t<<2)+576|0;v=c[r>>2]|0;if((v|0)!=1)if((v|0)==0){q=(q<<6&13056|w&52224|w&204|q<<1&34|q<<3&16|u)^q}else{q=q^65535}v=b+(t<<2)+560|0;u=s+ -3+(c[v>>2]|0)|0;c[v>>2]=u;do{if((u+16|0)>>>0>31){if((u|0)<0){c[v>>2]=-16;u=-16;break}else{c[v>>2]=15;u=15;break}}}while(0);t=b+(t<<2)+568|0;s=(c[t>>2]|0)+(13-s)|0;c[t>>2]=s;do{if((s+16|0)>>>0>31){if((s|0)<0){c[t>>2]=-16;s=-16;break}else{c[t>>2]=15;s=15;break}}}while(0);do{if((u|0)<0){if((u|0)<(s|0)){c[r>>2]=1;break}else{c[r>>2]=2;break}}else{if((s|0)<0){c[r>>2]=2;break}else{c[r>>2]=0;break}}}while(0);c[a+(o<<2)+34052>>2]=q}}while(0);o=o+1|0}while((o|0)<(c[j>>2]|0));i=f;return}function Rb(a,b){a=a|0;b=b|0;var d=0,e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0;d=i;if((b+ -1|0)>>>0>4095|(a|0)==0){n=-1;i=d;return n|0}f=b*592|0;g=ke(f)|0;e=a+34308|0;c[e>>2]=g;if((g|0)==0){c[a+34312>>2]=0;n=-1;i=d;return n|0}re(g|0,0,f|0)|0;c[a+34312>>2]=b;f=c[a+34128>>2]|0;if((f|0)==6|(f|0)==0){f=5}else{f=(f|0)==4?5:9}if((b|0)>0){h=0}else{n=0;i=d;return n|0}a:while(1){j=g+(h*592|0)|0;a=g+(h*592|0)+16|0;k=Xc(f,1)|0;if((k|0)==0){e=8;break}c[a>>2]=k;k=g+(h*592|0)+20|0;l=Xc(5,1)|0;if((l|0)==0){e=11;break}c[k>>2]=l;m=0;do{l=g+(h*592|0)+(m<<2)+24|0;n=Xc(c[1960+(m<<2)>>2]|0,1)|0;if((n|0)==0){e=13;break a}c[l>>2]=n;m=m+1|0}while((m|0)<21);c[(c[a>>2]|0)+24>>2]=0;c[(c[k>>2]|0)+24>>2]=0;c[(c[g+(h*592|0)+24>>2]|0)+24>>2]=0;c[(c[g+(h*592|0)+28>>2]|0)+24>>2]=0;c[(c[g+(h*592|0)+32>>2]|0)+24>>2]=0;c[(c[g+(h*592|0)+36>>2]|0)+24>>2]=0;c[(c[g+(h*592|0)+40>>2]|0)+24>>2]=0;c[(c[g+(h*592|0)+44>>2]|0)+24>>2]=0;c[(c[g+(h*592|0)+48>>2]|0)+24>>2]=0;c[(c[g+(h*592|0)+52>>2]|0)+24>>2]=0;c[(c[g+(h*592|0)+56>>2]|0)+24>>2]=0;c[(c[g+(h*592|0)+60>>2]|0)+24>>2]=0;c[(c[g+(h*592|0)+64>>2]|0)+24>>2]=0;c[(c[g+(h*592|0)+68>>2]|0)+24>>2]=0;c[(c[g+(h*592|0)+72>>2]|0)+24>>2]=0;c[(c[g+(h*592|0)+76>>2]|0)+24>>2]=0;c[(c[g+(h*592|0)+80>>2]|0)+24>>2]=0;c[(c[g+(h*592|0)+84>>2]|0)+24>>2]=0;c[(c[g+(h*592|0)+88>>2]|0)+24>>2]=0;c[(c[g+(h*592|0)+92>>2]|0)+24>>2]=0;c[(c[g+(h*592|0)+96>>2]|0)+24>>2]=0;c[(c[g+(h*592|0)+100>>2]|0)+24>>2]=0;c[(c[g+(h*592|0)+104>>2]|0)+24>>2]=0;ac(j)|0;ec(j)|0;$c(j);_c(j);h=h+1|0;if((h|0)>=(b|0)){b=0;e=17;break}g=c[e>>2]|0}if((e|0)==8){c[a>>2]=0;Ca(2048)|0;n=-1;i=d;return n|0}else if((e|0)==11){c[k>>2]=0;Ca(2048)|0;n=-1;i=d;return n|0}else if((e|0)==13){c[l>>2]=0;Ca(2048)|0;n=-1;i=d;return n|0}else if((e|0)==17){i=d;return b|0}return 0}function Sb(a){a=a|0;var b=0;b=i;c[(c[a+16>>2]|0)+24>>2]=0;c[(c[a+20>>2]|0)+24>>2]=0;c[(c[a+24>>2]|0)+24>>2]=0;c[(c[a+28>>2]|0)+24>>2]=0;c[(c[a+32>>2]|0)+24>>2]=0;c[(c[a+36>>2]|0)+24>>2]=0;c[(c[a+40>>2]|0)+24>>2]=0;c[(c[a+44>>2]|0)+24>>2]=0;c[(c[a+48>>2]|0)+24>>2]=0;c[(c[a+52>>2]|0)+24>>2]=0;c[(c[a+56>>2]|0)+24>>2]=0;c[(c[a+60>>2]|0)+24>>2]=0;c[(c[a+64>>2]|0)+24>>2]=0;c[(c[a+68>>2]|0)+24>>2]=0;c[(c[a+72>>2]|0)+24>>2]=0;c[(c[a+76>>2]|0)+24>>2]=0;c[(c[a+80>>2]|0)+24>>2]=0;c[(c[a+84>>2]|0)+24>>2]=0;c[(c[a+88>>2]|0)+24>>2]=0;c[(c[a+92>>2]|0)+24>>2]=0;c[(c[a+96>>2]|0)+24>>2]=0;c[(c[a+100>>2]|0)+24>>2]=0;c[(c[a+104>>2]|0)+24>>2]=0;ac(a)|0;ec(a)|0;$c(a);_c(a);i=b;return}function Tb(a){a=a|0;var b=0,d=0,e=0,f=0,g=0,h=0,j=0;d=i;b=c[a+34312>>2]|0;if((b|0)<=0){i=d;return}a=a+34308|0;f=c[a>>2]|0;if((f|0)==0){i=d;return}else{e=0}do{h=f+(e*592|0)+16|0;g=c[h>>2]|0;if((g|0)!=0){le(g)}c[h>>2]=0;g=f+(e*592|0)+20|0;h=c[g>>2]|0;if((h|0)!=0){le(h)}c[g>>2]=0;j=0;do{h=f+(e*592|0)+(j<<2)+24|0;g=c[h>>2]|0;if((g|0)!=0){le(g)}c[h>>2]=0;j=j+1|0}while((j|0)!=21);e=e+1|0;f=c[a>>2]|0}while((e|0)!=(b|0));le(f);i=d;return}function Ub(b,d,e){b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0;f=i;if((e|0)==0){n=0;i=f;return n|0}g=(d*88|0)+176|0;if((((d+2|0)>>>16)*88&8323072|0)==0){h=0}else{n=-1;i=f;return n|0}a:while(1){j=0;do{l=ke(g)|0;k=b+(h<<3)+(j<<2)|0;c[k>>2]=l;if((l|0)==0){e=5;break a}n=l+88|0;c[k>>2]=n;a[l+4>>0]=3;n=n+(d*88|0)|0;k=l+72|0;m=k+16|0;do{a[k>>0]=3;k=k+1|0}while((k|0)<(m|0));k=n+0|0;l=l+0|0;m=k+88|0;do{c[k>>2]=c[l>>2];k=k+4|0;l=l+4|0}while((k|0)<(m|0));j=j+1|0}while(j>>>0<2);h=h+1|0;if(!(h>>>0<e>>>0)){d=0;e=8;break}}if((e|0)==5){pa(2088,2120,65,2152)}else if((e|0)==8){i=f;return d|0}return 0}function Vb(a,b,d,e,f){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,j=0,k=0,l=0,m=0,n=0;g=i;if((b|0)==0){i=g;return}h=(f|0)==0;f=(d|0)!=0;if((e|0)==0){if(h){d=0;do{l=a+(d<<3)|0;m=c[l>>2]|0;n=a+(d<<3)+4|0;c[l>>2]=c[n>>2];c[n>>2]=m;d=d+1|0}while((d|0)!=(b|0));i=g;return}else{l=0}do{n=a+(l<<3)|0;h=c[n>>2]|0;k=a+(l<<3)+4|0;c[n>>2]=c[k>>2];c[k>>2]=h;a:do{if(f){m=0;while(1){e=h+(m*88|0)+0|0;j=h+ -88+0|0;h=e+88|0;do{c[e>>2]=c[j>>2];e=e+4|0;j=j+4|0}while((e|0)<(h|0));m=m+1|0;if((m|0)==(d|0)){break a}h=c[k>>2]|0}}}while(0);l=l+1|0}while((l|0)!=(b|0));i=g;return}if(h){k=0;do{l=a+(k<<3)|0;m=c[l>>2]|0;n=a+(k<<3)+4|0;h=c[n>>2]|0;c[l>>2]=h;c[n>>2]=m;b:do{if(f){m=0;while(1){e=h+(m*88|0)+0|0;j=h+ -88+0|0;h=e+88|0;do{c[e>>2]=c[j>>2];e=e+4|0;j=j+4|0}while((e|0)<(h|0));m=m+1|0;if((m|0)==(d|0)){break b}h=c[l>>2]|0}}}while(0);k=k+1|0}while((k|0)!=(b|0));i=g;return}else{l=0}do{m=a+(l<<3)|0;n=c[m>>2]|0;k=a+(l<<3)+4|0;h=c[k>>2]|0;c[m>>2]=h;c[k>>2]=n;if(f){n=0;while(1){e=h+(n*88|0)+0|0;j=h+ -88+0|0;h=e+88|0;do{c[e>>2]=c[j>>2];e=e+4|0;j=j+4|0}while((e|0)<(h|0));n=n+1|0;if((n|0)==(d|0)){break}h=c[m>>2]|0}if(f){m=0;do{j=c[k>>2]|0;e=j+(m*88|0)+0|0;j=j+ -88+0|0;h=e+88|0;do{c[e>>2]=c[j>>2];e=e+4|0;j=j+4|0}while((e|0)<(h|0));m=m+1|0}while((m|0)!=(d|0))}}l=l+1|0}while((l|0)!=(b|0));i=g;return}function Wb(b,d,e,f){b=b|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,j=0,k=0,l=0,m=0,n=0;g=i;b=c[b+(f<<3)+4>>2]|0;c[b+(e*88|0)>>2]=c[d>>2];f=b+(e*88|0)+4|0;a[f>>0]=0;j=16;while(1){k=j+16|0;if((c[d+(j<<2)>>2]|0)!=0){h=4;break}if(k>>>0<256){j=k}else{f=0;break}}if((h|0)==4){a[f>>0]=3;f=0}do{j=f<<4;l=b+(e*88|0)+(f<<2)+72|0;a[l>>0]=0;k=1;do{m=k;k=k+1|0;if((c[d+(j+m<<2)>>2]|0)!=0){h=8;break}}while(k>>>0<16);if((h|0)==8){h=0;a[l>>0]=3}k=j+64|0;l=b+(e*88|0)+(f<<2)+73|0;a[l>>0]=0;m=1;do{n=m;m=m+1|0;if((c[d+(k+n<<2)>>2]|0)!=0){h=12;break}}while(m>>>0<16);if((h|0)==12){h=0;a[l>>0]=3}l=j+128|0;m=b+(e*88|0)+(f<<2)+74|0;a[m>>0]=0;k=1;do{n=k;k=k+1|0;if((c[d+(l+n<<2)>>2]|0)!=0){h=16;break}}while(k>>>0<16);if((h|0)==16){h=0;a[m>>0]=3}j=j+192|0;k=b+(e*88|0)+(f<<2)+75|0;a[k>>0]=0;l=1;do{n=l;l=l+1|0;if((c[d+(j+n<<2)>>2]|0)!=0){h=20;break}}while(l>>>0<16);if((h|0)==20){h=0;a[k>>0]=3}f=f+1|0}while((f|0)!=4);i=g;return}function Xb(b,e,f,g,h,j){b=b|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;var k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0;k=i;m=c[b+(h<<3)>>2]|0;n=m+(g*88|0)|0;l=g+ -1|0;o=m+(l*88|0)|0;b=c[b+(h<<3)+4>>2]|0;p=b+(g*88|0)|0;q=b+(l*88|0)|0;r=m+(l*88|0)+4|0;h=b+(l*88|0)+4|0;if((d[r>>0]|0|0)==(0-(d[h>>0]|0)|0)?(s=(c[o>>2]|0)-(c[q>>2]|0)|0,(((s|0)>-1?s:0-s|0)|0)<=(j|0)):0){v=e+ -320|0;s=f+ -512|0;u=c[s>>2]|0;t=(c[e+ -384>>2]|0)-(c[f+ -448>>2]|0)+(u-(c[v>>2]|0)<<2)>>3;c[s>>2]=u-t;c[v>>2]=t+(c[v>>2]|0);v=e+ -64|0;t=f+ -256|0;s=c[t>>2]|0;u=(c[e+ -128>>2]|0)-(c[f+ -192>>2]|0)+(s-(c[v>>2]|0)<<2)>>3;c[t>>2]=s-u;c[v>>2]=u+(c[v>>2]|0)}t=m+(g*88|0)+4|0;u=a[t>>0]|0;s=b+(g*88|0)+4|0;if((u&255|0)==(0-(d[s>>0]|0)|0)?(v=(c[n>>2]|0)-(c[p>>2]|0)|0,(((v|0)>-1?v:0-v|0)|0)<=(j|0)):0){u=e+192|0;x=c[f>>2]|0;w=(c[e+128>>2]|0)-(c[f+64>>2]|0)+(x-(c[u>>2]|0)<<2)>>3;c[f>>2]=x-w;c[u>>2]=w+(c[u>>2]|0);u=e+448|0;w=f+256|0;x=c[w>>2]|0;v=(c[e+384>>2]|0)-(c[f+320>>2]|0)+(x-(c[u>>2]|0)<<2)>>3;c[w>>2]=x-v;c[u>>2]=v+(c[u>>2]|0);u=a[t>>0]|0}if((d[r>>0]|0|0)==(0-(u&255)|0)?(n=(c[o>>2]|0)-(c[n>>2]|0)|0,(((n|0)>-1?n:0-n|0)|0)<=(j|0)):0){x=e+ -128|0;w=e+128|0;v=c[w>>2]|0;u=c[x>>2]|0;t=(c[e+ -384>>2]|0)-(c[e+384>>2]|0)+(v-u<<2)>>3;c[w>>2]=v-t;c[x>>2]=t+u;x=e+ -64|0;u=e+192|0;t=c[u>>2]|0;w=c[x>>2]|0;v=(c[e+ -320>>2]|0)-(c[e+448>>2]|0)+(t-w<<2)>>3;c[u>>2]=t-v;c[x>>2]=v+w}if((d[h>>0]|0|0)==(0-(d[s>>0]|0)|0)?(n=(c[q>>2]|0)-(c[p>>2]|0)|0,(((n|0)>-1?n:0-n|0)|0)<=(j|0)):0){x=f+ -256|0;w=c[f>>2]|0;u=c[x>>2]|0;t=(c[f+ -512>>2]|0)-(c[f+256>>2]|0)+(w-u<<2)>>3;c[f>>2]=w-t;c[x>>2]=t+u;x=f+ -192|0;u=f+64|0;t=c[u>>2]|0;w=c[x>>2]|0;v=(c[f+ -448>>2]|0)-(c[f+320>>2]|0)+(t-w<<2)>>3;c[u>>2]=t-v;c[x>>2]=v+w}c[b+(g*88|0)+8>>2]=c[f>>2];c[b+(g*88|0)+12>>2]=c[f+256>>2];c[b+(g*88|0)+24>>2]=c[f+64>>2];c[b+(g*88|0)+28>>2]=c[f+320>>2];c[m+(g*88|0)+40>>2]=c[e+128>>2];c[m+(g*88|0)+44>>2]=c[e+384>>2];c[m+(g*88|0)+56>>2]=c[e+192>>2];c[m+(g*88|0)+60>>2]=c[e+448>>2];c[b+(l*88|0)+16>>2]=c[f+ -512>>2];c[b+(l*88|0)+20>>2]=c[f+ -256>>2];c[b+(l*88|0)+32>>2]=c[f+ -448>>2];c[b+(l*88|0)+36>>2]=c[f+ -192>>2];c[m+(l*88|0)+48>>2]=c[e+ -384>>2];c[m+(l*88|0)+52>>2]=c[e+ -128>>2];c[m+(l*88|0)+64>>2]=c[e+ -320>>2];c[m+(l*88|0)+68>>2]=c[e+ -64>>2];i=k;return}function Yb(e,f,g,h,j,k){e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;k=k|0;var l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0;m=i;i=i+128|0;n=m+28|0;l=m;t=c[e+(j<<3)>>2]|0;s=h+ -1|0;j=c[e+(j<<3)+4>>2]|0;o=t+(h*88|0)|0;r=o+ -16|0;q=o+ -80|0;c[n+0>>2]=c[q+0>>2];c[n+4>>2]=c[q+4>>2];c[n+8>>2]=c[q+8>>2];c[n+12>>2]=c[q+12>>2];c[l>>2]=d[r>>0]|d[r+1>>0]<<8|d[r+2>>0]<<16|d[r+3>>0]<<24;c[n+80>>2]=c[j+(s*88|0)+8>>2];a[l+20>>0]=a[j+(s*88|0)+72>>0]|0;c[n+16>>2]=c[t+(h*88|0)+8>>2];a[l+4>>0]=a[t+(h*88|0)+72>>0]|0;r=l+5|0;q=o+ -12|0;p=n+20|0;e=o+ -64|0;c[p+0>>2]=c[e+0>>2];c[p+4>>2]=c[e+4>>2];c[p+8>>2]=c[e+8>>2];c[p+12>>2]=c[e+12>>2];q=d[q>>0]|d[q+1>>0]<<8|d[q+2>>0]<<16|d[q+3>>0]<<24;a[r>>0]=q;a[r+1>>0]=q>>8;a[r+2>>0]=q>>16;a[r+3>>0]=q>>24;c[n+84>>2]=c[j+(s*88|0)+12>>2];a[l+21>>0]=a[j+(s*88|0)+73>>0]|0;c[n+36>>2]=c[t+(h*88|0)+24>>2];a[l+9>>0]=a[t+(h*88|0)+76>>0]|0;r=l+10|0;q=o+ -8|0;p=n+40|0;e=o+ -48|0;c[p+0>>2]=c[e+0>>2];c[p+4>>2]=c[e+4>>2];c[p+8>>2]=c[e+8>>2];c[p+12>>2]=c[e+12>>2];q=d[q>>0]|d[q+1>>0]<<8|d[q+2>>0]<<16|d[q+3>>0]<<24;b[r>>1]=q;b[r+2>>1]=q>>>16;c[n+88>>2]=c[j+(s*88|0)+16>>2];a[l+22>>0]=a[j+(s*88|0)+74>>0]|0;c[n+56>>2]=c[t+(h*88|0)+40>>2];a[l+14>>0]=a[t+(h*88|0)+80>>0]|0;r=l+15|0;q=o+ -4|0;p=n+60|0;o=o+ -32|0;c[p+0>>2]=c[o+0>>2];c[p+4>>2]=c[o+4>>2];c[p+8>>2]=c[o+8>>2];c[p+12>>2]=c[o+12>>2];q=d[q>>0]|d[q+1>>0]<<8|d[q+2>>0]<<16|d[q+3>>0]<<24;a[r>>0]=q;a[r+1>>0]=q>>8;a[r+2>>0]=q>>16;a[r+3>>0]=q>>24;c[n+92>>2]=c[j+(s*88|0)+20>>2];a[l+23>>0]=a[j+(s*88|0)+75>>0]|0;c[n+76>>2]=c[t+(h*88|0)+56>>2];a[l+19>>0]=a[t+(h*88|0)+84>>0]|0;c[n+96>>2]=c[j+(h*88|0)+8>>2];a[l+24>>0]=a[j+(h*88|0)+72>>0]|0;j=0;do{o=j<<4;p=j;j=j+1|0;q=p>>>0<3;r=a[l+(p*5|0)>>0]|0;e=0;do{t=(e<<6)+ -256|0;h=t+o|0;s=r&255;if(((d[l+(j*5|0)+e>>0]|0)+s|0)<3?(r=(c[n+(p*20|0)+(e<<2)>>2]|0)-(c[n+(j*20|0)+(e<<2)>>2]|0)|0,(((r|0)>-1?r:0-r|0)|0)<=(k|0)):0){if(q){t=f+(h+16<<2)|0}else{t=g+(t<<2)|0}r=0;do{y=d[8416+r>>0]|0;w=d[8432+r>>0]|0;x=f+(w+h<<2)|0;A=f+((d[8448+r>>0]|0)+h<<2)|0;v=t+((d[8400+r>>0]|0)<<2)|0;u=t+(y<<2)|0;B=c[v>>2]|0;z=(c[x>>2]|0)-(c[u>>2]|0)+(B-(c[A>>2]|0)<<2)>>3;c[v>>2]=B-z;z=z+(c[A>>2]|0)|0;c[A>>2]=z;c[x>>2]=(z+(c[f+(y+h<<2)>>2]|0)>>2)+(c[x>>2]>>1);c[u>>2]=((c[t+(w<<2)>>2]|0)+(c[v>>2]|0)>>2)+(c[u>>2]>>1);r=r+1|0}while((r|0)!=4)}t=e;e=e+1|0;r=a[l+(p*5|0)+e>>0]|0;if(((r&255)+s|0)<3?(s=(c[n+(p*20|0)+(t<<2)>>2]|0)-(c[n+(p*20|0)+(e<<2)>>2]|0)|0,(((s|0)>-1?s:0-s|0)|0)<=(k|0)):0){t=h+64|0;s=0;do{x=d[8401+(s<<4)>>0]|0;z=d[8402+(s<<4)>>0]|0;y=f+(z+h<<2)|0;v=f+((d[8403+(s<<4)>>0]|0)+h<<2)|0;A=f+((d[8400+(s<<4)>>0]|0)+t<<2)|0;B=f+(x+t<<2)|0;u=c[A>>2]|0;w=(c[y>>2]|0)-(c[B>>2]|0)+(u-(c[v>>2]|0)<<2)>>3;c[A>>2]=u-w;w=w+(c[v>>2]|0)|0;c[v>>2]=w;c[y>>2]=(w+(c[f+(x+h<<2)>>2]|0)>>2)+(c[y>>2]>>1);c[B>>2]=((c[f+(z+t<<2)>>2]|0)+(c[A>>2]|0)>>2)+(c[B>>2]>>1);s=s+1|0}while((s|0)!=4)}}while((e|0)!=4)}while((j|0)!=4);i=m;return}function Zb(a,e,f){a=a|0;e=e|0;f=f|0;var g=0,h=0,j=0,k=0,l=0,m=0;g=i;if((a|0)>=5){l=b[(c[e+20>>2]|0)+((c[f+4>>2]|0)>>>27<<1)>>1]|0;e=l<<16>>16;if(!(l<<16>>16>-1)){pa(2880,2392,169,2896)}a=c[7736+(a<<2)>>2]|0;Md(f,e&7)|0;a=(a*5|0)+(e>>3)|0;e=c[2168+(a<<2)>>2]|0;a=c[7800+(a<<2)>>2]|0;if((a|0)==0){l=e;i=g;return l|0}l=(_b(f,a)|0)+e|0;i=g;return l|0}if((a|0)==1){l=1;i=g;return l|0}h=f+4|0;k=c[h>>2]|0;e=f+8|0;l=(c[e>>2]|0)+1|0;c[e>>2]=l;if(l>>>0<16){j=k<<1}else{j=f+20|0;m=(c[j>>2]|0)+(l>>>3)&c[f+12>>2];c[j>>2]=m;j=l&15;c[e>>2]=j;l=j;j=((d[m+2>>0]|0)<<8|(d[m+3>>0]|0)|(d[m+1>>0]|0)<<16|(d[m>>0]|0)<<24)<<j}c[h>>2]=j;if(!((k|0)>-1)){m=1;i=g;return m|0}if((a|0)==2){m=2;i=g;return m|0}l=l+1|0;c[e>>2]=l;if(l>>>0<16){k=j<<1}else{k=f+20|0;m=(c[k>>2]|0)+(l>>>3)&c[f+12>>2];c[k>>2]=m;k=l&15;c[e>>2]=k;l=k;k=((d[m+2>>0]|0)<<8|(d[m+3>>0]|0)|(d[m+1>>0]|0)<<16|(d[m>>0]|0)<<24)<<k}c[h>>2]=k;if(!((j|0)>-1)){m=2;i=g;return m|0}if((a|0)==3){m=3;i=g;return m|0}a=l+1|0;c[e>>2]=a;if(a>>>0<16){m=k<<1;c[h>>2]=m;m=k>>31;m=m+4|0;i=g;return m|0}else{m=f+20|0;l=(c[m>>2]|0)+(a>>>3)&c[f+12>>2];c[m>>2]=l;m=a&15;c[e>>2]=m;m=((d[l+2>>0]|0)<<8|(d[l+3>>0]|0)|(d[l+1>>0]|0)<<16|(d[l>>0]|0)<<24)<<m;c[h>>2]=m;m=k>>31;m=m+4|0;i=g;return m|0}return 0}function _b(a,b){a=a|0;b=b|0;var e=0,f=0,g=0,h=0,j=0,k=0;e=i;if(!((b|0)>-1&b>>>0<17)){pa(2776,2392,78,2848)}f=a+4|0;g=c[a+12>>2]|0;if((g&1|0)==0){h=(c[f>>2]|0)>>>(32-b|0);j=a+8|0;b=(c[j>>2]|0)+b|0;k=a+20|0;a=(c[k>>2]|0)+(b>>>3)&g;c[k>>2]=a;g=b&15;c[j>>2]=g;c[f>>2]=((d[a+2>>0]|0)<<8|(d[a+3>>0]|0)|(d[a+1>>0]|0)<<16|(d[a>>0]|0)<<24)<<g;i=e;return h|0}else{pa(2824,2392,85,2808)}return 0}



function Pc(b){b=b|0;var e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0,Y=0,Z=0,_=0,$=0,ba=0,ca=0,da=0,ea=0,fa=0,ga=0,ha=0,ia=0,ja=0,ka=0,la=0,ma=0,na=0,oa=0,pa=0,qa=0,ra=0,sa=0,ta=0,ua=0,va=0,wa=0,xa=0,ya=0,za=0,Aa=0,Ba=0,Ca=0,Da=0,Ea=0,Fa=0,Ga=0,Ha=0,Ia=0,Ja=0,Ka=0,La=0,Ma=0,Na=0,Oa=0,Pa=0,Qa=0,Ra=0;k=i;i=i+128|0;y=k+64|0;z=k;q=c[b+100>>2]|0;Ha=c[b+34128>>2]|0;A=c[b+34328>>2]|0;p=(A|0)==0;F=c[b+34332>>2]|0;o=(A|0)==(F|0);l=c[b+34324>>2]|0;f=(l|0)==0;E=f&1;l=(l|0)==(c[b+34336>>2]|0);u=l&1;g=f|l;h=p|o;t=f|p;e=l|o;v=(Ha|0)==1;s=(Ha|0)==2;if((Ha+ -1|0)>>>0<2){x=1}else{x=c[b+34156>>2]|0}j=b+34232|0;r=c[(c[j>>2]|0)+12>>2]|0;w=b+68|0;Ha=a[w>>0]|0;C=Ha&255;m=(c[b+116>>2]&-2|0)==2;n=m&1;if(!(Ha<<24>>24==0)){if((x|0)!=0){I=d[b+34116>>0]|0;H=c[b+34272>>2]|0;B=c[b+34292>>2]|0;G=(q|0)==0?2:1;D=0;do{c[y+(D<<2)>>2]=aa(c[(c[B+(H*240|0)+(D<<2)+64>>2]|0)+(I*20|0)+4>>2]<<C,G)|0;c[z+(D<<2)>>2]=c[(c[B+(H*240|0)+(D<<2)>>2]|0)+4>>2]<<C;D=D+1|0}while((D|0)!=(x|0))}if(p){Vb(b+34936|0,c[b+34156>>2]|0,F,E,u)}}u=r>>>0<16;if((x|0)!=0&u){F=(q|0)==2;E=g|h^1;H=p?0:-128;K=H|32;L=H|48;M=H|16;N=H|96;O=H|112;P=H|64;Q=H|80;I=r>>>0>3;G=(q|0)==0;J=b+34936|0;Z=p?10:-50;R=Z|48;S=R+ -2|0;T=Z+ -10|0;U=Z+ -8|0;V=Z|49;W=R+ -1|0;X=Z+ -9|0;Y=Z+ -7|0;_=Z+ -2|0;$=Z+6|0;ba=Z+8|0;ca=Z|1;da=Z+ -1|0;ea=Z+7|0;fa=Z+9|0;ga=Z|16;ha=ga+ -2|0;ia=ga+6|0;ja=ga+8|0;ka=Z|17;la=ga+ -1|0;ma=ga+7|0;na=ga+9|0;oa=ga+16|0;pa=ga+14|0;qa=ga+22|0;ra=ga+24|0;sa=ga+17|0;ta=ga+15|0;ua=ga+23|0;va=ga+25|0;wa=p?0:-192;ya=o?-64:64;xa=(wa|0)<(ya|0);za=o?0:128;Aa=p?32:-96;Ba=o?32:160;Ca=b+34140|0;Fa=b+34117|0;Ea=b+34272|0;Da=b+34292|0;Ga=0;do{D=c[b+(Ga<<2)+34652>>2]|0;C=c[b+(Ga<<2)+34716>>2]|0;if(m){B=255}else{B=c[(c[(c[Da>>2]|0)+((c[Ea>>2]|0)*240|0)+(Ga<<2)+128>>2]|0)+((d[Fa>>0]|0)*20|0)+4>>2]|0}if(!e){if((a[w>>0]|0)!=0){Wb(J,C,A,Ga)}Kc(C);if(!((c[Ca>>2]|0)==0|(Ga|0)==0)){c[C>>2]=c[C>>2]<<1;Ha=C+64|0;c[Ha>>2]=c[Ha>>2]<<1;Ha=C+128|0;c[Ha>>2]=c[Ha>>2]<<1;Ha=C+192|0;c[Ha>>2]=c[Ha>>2]<<1;Ha=C+256|0;c[Ha>>2]=c[Ha>>2]<<1;Ha=C+320|0;c[Ha>>2]=c[Ha>>2]<<1;Ha=C+384|0;c[Ha>>2]=c[Ha>>2]<<1;Ha=C+448|0;c[Ha>>2]=c[Ha>>2]<<1;Ha=C+512|0;c[Ha>>2]=c[Ha>>2]<<1;Ha=C+576|0;c[Ha>>2]=c[Ha>>2]<<1;Ha=C+640|0;c[Ha>>2]=c[Ha>>2]<<1;Ha=C+704|0;c[Ha>>2]=c[Ha>>2]<<1;Ha=C+768|0;c[Ha>>2]=c[Ha>>2]<<1;Ha=C+832|0;c[Ha>>2]=c[Ha>>2]<<1;Ha=C+896|0;c[Ha>>2]=c[Ha>>2]<<1;Ha=C+960|0;c[Ha>>2]=c[Ha>>2]<<1}}do{if(F){if(!E){Oa=D+(K<<2)|0;Pa=D+(L<<2)|0;Ja=C+(H<<2)|0;Na=C+(M<<2)|0;Ka=c[Ja>>2]|0;La=c[Na>>2]|0;Ma=La+(c[Oa>>2]|0)|0;Ia=Ka+(c[Pa>>2]|0)|0;Ha=Ia+1>>1;Ka=Ka-Ha-(La+1-(Ma+1>>1)>>1)|0;La=(Ka+1>>1)+La|0;Ha=Ka+Ha|0;Ma=((La*3|0)+16>>5)-La+Ma|0;Ia=((Ha*3|0)+16>>5)-Ha+Ia|0;La=((Ma*3|0)+8>>4)+La|0;Ha=((Ia*3|0)+8>>4)+Ha|0;c[Oa>>2]=((La*3|0)+16>>5)+Ma;c[Pa>>2]=((Ha*3|0)+16>>5)+Ia;c[Ja>>2]=Ha;c[Na>>2]=La;Na=D+(N<<2)|0;La=D+(O<<2)|0;Ja=C+(P<<2)|0;Ha=C+(Q<<2)|0;Pa=c[Ja>>2]|0;Ia=c[Ha>>2]|0;Oa=Ia+(c[Na>>2]|0)|0;Ma=Pa+(c[La>>2]|0)|0;Ka=Ma+1>>1;Pa=Pa-Ka-(Ia+1-(Oa+1>>1)>>1)|0;Ia=(Pa+1>>1)+Ia|0;Ka=Pa+Ka|0;Oa=((Ia*3|0)+16>>5)-Ia+Oa|0;Ma=((Ka*3|0)+16>>5)-Ka+Ma|0;Ia=((Oa*3|0)+8>>4)+Ia|0;Ka=((Ma*3|0)+8>>4)+Ka|0;c[Na>>2]=((Ia*3|0)+16>>5)+Oa;c[La>>2]=((Ka*3|0)+16>>5)+Ma;c[Ja>>2]=Ka;c[Ha>>2]=Ia}if(!h){if(g){Pa=f?C:D+128|0;Ia=Pa+ -512|0;Ha=Pa+ -256|0;Ja=Pa+256|0;Ma=c[Pa>>2]|0;La=c[Ja>>2]|0;Ka=La+(c[Ia>>2]|0)|0;Oa=Ma+(c[Ha>>2]|0)|0;Na=Oa+1>>1;Ma=Ma-Na-(La+1-(Ka+1>>1)>>1)|0;La=(Ma+1>>1)+La|0;Na=Ma+Na|0;Ka=((La*3|0)+16>>5)-La+Ka|0;Oa=((Na*3|0)+16>>5)-Na+Oa|0;La=((Ka*3|0)+8>>4)+La|0;Na=((Oa*3|0)+8>>4)+Na|0;c[Ia>>2]=((La*3|0)+16>>5)+Ka;c[Ha>>2]=((Na*3|0)+16>>5)+Oa;c[Pa>>2]=Na;c[Ja>>2]=La;Ja=Pa+ -448|0;La=Pa+ -192|0;Na=Pa+64|0;Pa=Pa+320|0;Ha=c[Na>>2]|0;Oa=c[Pa>>2]|0;Ia=Oa+(c[Ja>>2]|0)|0;Ka=Ha+(c[La>>2]|0)|0;Ma=Ka+1>>1;Ha=Ha-Ma-(Oa+1-(Ia+1>>1)>>1)|0;Oa=(Ha+1>>1)+Oa|0;Ma=Ha+Ma|0;Ia=((Oa*3|0)+16>>5)-Oa+Ia|0;Ka=((Ma*3|0)+16>>5)-Ma+Ka|0;Oa=((Ia*3|0)+8>>4)+Oa|0;Ma=((Ka*3|0)+8>>4)+Ma|0;c[Ja>>2]=((Oa*3|0)+16>>5)+Ia;c[La>>2]=((Ma*3|0)+16>>5)+Ka;c[Na>>2]=Ma;c[Pa>>2]=Oa;break}else{Nc(D,C);break}}}}while(0);if((a[w>>0]|0)!=0){Xb(J,D,C,A,Ga,c[z+(Ga<<2)>>2]|0)}if(!I){if(!(f|(Aa|0)>=(Ba|0))){Ha=Aa;do{Jc(D+(Ha<<2)|0);Jc(D+((Ha|16)<<2)|0);Ha=Ha+64|0}while((Ha|0)<(Ba|0))}if(!(l|(H|0)>=(za|0))){Ha=H;do{Jc(C+(Ha<<2)|0);Jc(C+((Ha|16)<<2)|0);Ha=Ha+64|0}while((Ha|0)<(za|0))}a:do{if(!G){if(h){if(!f){Oa=D+(ga<<2)|0;Pa=D+(ha<<2)|0;Na=D+(ia<<2)|0;Ka=D+(ja<<2)|0;Ja=c[Na>>2]|0;Ma=c[Ka>>2]|0;La=Ma+(c[Oa>>2]|0)|0;Ha=Ja+(c[Pa>>2]|0)|0;Ia=Ha+1>>1;Ja=Ja-Ia-(Ma+1-(La+1>>1)>>1)|0;Ma=(Ja+1>>1)+Ma|0;Ia=Ja+Ia|0;La=((Ma*3|0)+16>>5)-Ma+La|0;Ha=((Ia*3|0)+16>>5)-Ia+Ha|0;Ma=((La*3|0)+8>>4)+Ma|0;Ia=((Ha*3|0)+8>>4)+Ia|0;c[Oa>>2]=((Ma*3|0)+16>>5)+La;c[Pa>>2]=((Ia*3|0)+16>>5)+Ha;c[Na>>2]=Ia;c[Ka>>2]=Ma;Ka=D+(ka<<2)|0;Ma=D+(la<<2)|0;Na=D+(ma<<2)|0;Ia=D+(na<<2)|0;Pa=c[Na>>2]|0;Ha=c[Ia>>2]|0;Oa=Ha+(c[Ka>>2]|0)|0;La=Pa+(c[Ma>>2]|0)|0;Ja=La+1>>1;Pa=Pa-Ja-(Ha+1-(Oa+1>>1)>>1)|0;Ha=(Pa+1>>1)+Ha|0;Ja=Pa+Ja|0;Oa=((Ha*3|0)+16>>5)-Ha+Oa|0;La=((Ja*3|0)+16>>5)-Ja+La|0;Ha=((Oa*3|0)+8>>4)+Ha|0;Ja=((La*3|0)+8>>4)+Ja|0;c[Ka>>2]=((Ha*3|0)+16>>5)+Oa;c[Ma>>2]=((Ja*3|0)+16>>5)+La;c[Na>>2]=Ja;c[Ia>>2]=Ha;Ia=D+(oa<<2)|0;Ha=D+(pa<<2)|0;Na=D+(qa<<2)|0;Ja=D+(ra<<2)|0;Ma=c[Na>>2]|0;La=c[Ja>>2]|0;Ka=La+(c[Ia>>2]|0)|0;Oa=Ma+(c[Ha>>2]|0)|0;Pa=Oa+1>>1;Ma=Ma-Pa-(La+1-(Ka+1>>1)>>1)|0;La=(Ma+1>>1)+La|0;Pa=Ma+Pa|0;Ka=((La*3|0)+16>>5)-La+Ka|0;Oa=((Pa*3|0)+16>>5)-Pa+Oa|0;La=((Ka*3|0)+8>>4)+La|0;Pa=((Oa*3|0)+8>>4)+Pa|0;c[Ia>>2]=((La*3|0)+16>>5)+Ka;c[Ha>>2]=((Pa*3|0)+16>>5)+Oa;c[Na>>2]=Pa;c[Ja>>2]=La;Ja=D+(sa<<2)|0;La=D+(ta<<2)|0;Na=D+(ua<<2)|0;Pa=D+(va<<2)|0;Ha=c[Na>>2]|0;Oa=c[Pa>>2]|0;Ia=Oa+(c[Ja>>2]|0)|0;Ka=Ha+(c[La>>2]|0)|0;Ma=Ka+1>>1;Ha=Ha-Ma-(Oa+1-(Ia+1>>1)>>1)|0;Oa=(Ha+1>>1)+Oa|0;Ma=Ha+Ma|0;Ia=((Oa*3|0)+16>>5)-Oa+Ia|0;Ka=((Ma*3|0)+16>>5)-Ma+Ka|0;Oa=((Ia*3|0)+8>>4)+Oa|0;Ma=((Ka*3|0)+8>>4)+Ma|0;c[Ja>>2]=((Oa*3|0)+16>>5)+Ia;c[La>>2]=((Ma*3|0)+16>>5)+Ka;c[Na>>2]=Ma;c[Pa>>2]=Oa}if(!l){Ia=C+(Z<<2)|0;Ha=C+(_<<2)|0;Na=C+($<<2)|0;Ja=C+(ba<<2)|0;Ma=c[Na>>2]|0;La=c[Ja>>2]|0;Ka=La+(c[Ia>>2]|0)|0;Oa=Ma+(c[Ha>>2]|0)|0;Pa=Oa+1>>1;Ma=Ma-Pa-(La+1-(Ka+1>>1)>>1)|0;La=(Ma+1>>1)+La|0;Pa=Ma+Pa|0;Ka=((La*3|0)+16>>5)-La+Ka|0;Oa=((Pa*3|0)+16>>5)-Pa+Oa|0;La=((Ka*3|0)+8>>4)+La|0;Pa=((Oa*3|0)+8>>4)+Pa|0;c[Ia>>2]=((La*3|0)+16>>5)+Ka;c[Ha>>2]=((Pa*3|0)+16>>5)+Oa;c[Na>>2]=Pa;c[Ja>>2]=La;Ja=C+(ca<<2)|0;La=C+(da<<2)|0;Na=C+(ea<<2)|0;Pa=C+(fa<<2)|0;Ha=c[Na>>2]|0;Oa=c[Pa>>2]|0;Ia=Oa+(c[Ja>>2]|0)|0;Ka=Ha+(c[La>>2]|0)|0;Ma=Ka+1>>1;Ha=Ha-Ma-(Oa+1-(Ia+1>>1)>>1)|0;Oa=(Ha+1>>1)+Oa|0;Ma=Ha+Ma|0;Ia=((Oa*3|0)+16>>5)-Oa+Ia|0;Ka=((Ma*3|0)+16>>5)-Ma+Ka|0;Oa=((Ia*3|0)+8>>4)+Oa|0;Ma=((Ka*3|0)+8>>4)+Ma|0;c[Ja>>2]=((Oa*3|0)+16>>5)+Ia;c[La>>2]=((Ma*3|0)+16>>5)+Ka;c[Na>>2]=Ma;c[Pa>>2]=Oa}if(!g){Ia=D+(R<<2)|0;Ha=D+(S<<2)|0;Na=C+(T<<2)|0;Ja=C+(U<<2)|0;Ma=c[Na>>2]|0;La=c[Ja>>2]|0;Ka=La+(c[Ia>>2]|0)|0;Oa=Ma+(c[Ha>>2]|0)|0;Pa=Oa+1>>1;Ma=Ma-Pa-(La+1-(Ka+1>>1)>>1)|0;La=(Ma+1>>1)+La|0;Pa=Ma+Pa|0;Ka=((La*3|0)+16>>5)-La+Ka|0;Oa=((Pa*3|0)+16>>5)-Pa+Oa|0;La=((Ka*3|0)+8>>4)+La|0;Pa=((Oa*3|0)+8>>4)+Pa|0;c[Ia>>2]=((La*3|0)+16>>5)+Ka;c[Ha>>2]=((Pa*3|0)+16>>5)+Oa;c[Na>>2]=Pa;c[Ja>>2]=La;Ja=D+(V<<2)|0;La=D+(W<<2)|0;Na=C+(X<<2)|0;Pa=C+(Y<<2)|0;Ha=c[Na>>2]|0;Oa=c[Pa>>2]|0;Ia=Oa+(c[Ja>>2]|0)|0;Ka=Ha+(c[La>>2]|0)|0;Ma=Ka+1>>1;Ha=Ha-Ma-(Oa+1-(Ia+1>>1)>>1)|0;Oa=(Ha+1>>1)+Oa|0;Ma=Ha+Ma|0;Ia=((Oa*3|0)+16>>5)-Oa+Ia|0;Ka=((Ma*3|0)+16>>5)-Ma+Ka|0;Oa=((Ia*3|0)+8>>4)+Oa|0;Ma=((Ka*3|0)+8>>4)+Ma|0;c[Ja>>2]=((Oa*3|0)+16>>5)+Ia;c[La>>2]=((Ma*3|0)+16>>5)+Ka;c[Na>>2]=Ma;c[Pa>>2]=Oa}}if(f){if(xa){Ha=wa}else{break}while(1){Qa=C+((Ha|5)<<2)|0;Ra=C+((Ha|4)<<2)|0;Pa=Ha;Ha=Ha+64|0;Ma=C+(Ha<<2)|0;Ia=C+(Pa+65<<2)|0;La=c[Ma>>2]|0;Ka=c[Ia>>2]|0;Ja=Ka+(c[Qa>>2]|0)|0;Na=La+(c[Ra>>2]|0)|0;Oa=Na+1>>1;La=La-Oa-(Ka+1-(Ja+1>>1)>>1)|0;Ka=(La+1>>1)+Ka|0;Oa=La+Oa|0;Ja=((Ka*3|0)+16>>5)-Ka+Ja|0;Na=((Oa*3|0)+16>>5)-Oa+Na|0;Ka=((Ja*3|0)+8>>4)+Ka|0;Oa=((Na*3|0)+8>>4)+Oa|0;c[Qa>>2]=((Ka*3|0)+16>>5)+Ja;c[Ra>>2]=((Oa*3|0)+16>>5)+Na;c[Ma>>2]=Oa;c[Ia>>2]=Ka;Ia=C+((Pa|7)<<2)|0;Ka=C+((Pa|6)<<2)|0;Ma=C+(Pa+66<<2)|0;Oa=C+(Pa+67<<2)|0;Ra=c[Ma>>2]|0;Na=c[Oa>>2]|0;Qa=Na+(c[Ia>>2]|0)|0;Ja=Ra+(c[Ka>>2]|0)|0;La=Ja+1>>1;Ra=Ra-La-(Na+1-(Qa+1>>1)>>1)|0;Na=(Ra+1>>1)+Na|0;La=Ra+La|0;Qa=((Na*3|0)+16>>5)-Na+Qa|0;Ja=((La*3|0)+16>>5)-La+Ja|0;Na=((Qa*3|0)+8>>4)+Na|0;La=((Ja*3|0)+8>>4)+La|0;c[Ia>>2]=((Na*3|0)+16>>5)+Qa;c[Ka>>2]=((La*3|0)+16>>5)+Ja;c[Ma>>2]=La;c[Oa>>2]=Na;Lc(C+(Pa<<2)|0,C+((Pa|16)<<2)|0,0,B,n);if((Ha|0)>=(ya|0)){break a}}}if(l){if(xa){Ha=wa}else{break}do{Ra=Ha|16;Lc(D+(Ra<<2)|0,D+(Ra+16<<2)|0,0,B,n);Ra=Ha|48;Lc(D+((Ha|32)<<2)|0,D+(Ra<<2)|0,0,B,n);Ma=D+((Ha|63)<<2)|0;Qa=D+((Ha|62)<<2)|0;Pa=D+(Ra+74<<2)|0;La=D+(Ra+75<<2)|0;Ia=c[Pa>>2]|0;Na=c[La>>2]|0;Oa=Na+(c[Ma>>2]|0)|0;Ka=Ia+(c[Qa>>2]|0)|0;Ja=Ka+1>>1;Ia=Ia-Ja-(Na+1-(Oa+1>>1)>>1)|0;Na=(Ia+1>>1)+Na|0;Ja=Ia+Ja|0;Oa=((Na*3|0)+16>>5)-Na+Oa|0;Ka=((Ja*3|0)+16>>5)-Ja+Ka|0;Na=((Oa*3|0)+8>>4)+Na|0;Ja=((Ka*3|0)+8>>4)+Ja|0;c[Ma>>2]=((Na*3|0)+16>>5)+Oa;c[Qa>>2]=((Ja*3|0)+16>>5)+Ka;c[Pa>>2]=Ja;c[La>>2]=Na;La=D+((Ha|61)<<2)|0;Na=D+((Ha|60)<<2)|0;Pa=D+(Ra+72<<2)|0;Ra=D+(Ra+73<<2)|0;Ja=c[Pa>>2]|0;Qa=c[Ra>>2]|0;Ka=Qa+(c[La>>2]|0)|0;Ma=Ja+(c[Na>>2]|0)|0;Oa=Ma+1>>1;Ja=Ja-Oa-(Qa+1-(Ka+1>>1)>>1)|0;Qa=(Ja+1>>1)+Qa|0;Oa=Ja+Oa|0;Ka=((Qa*3|0)+16>>5)-Qa+Ka|0;Ma=((Oa*3|0)+16>>5)-Oa+Ma|0;Qa=((Ka*3|0)+8>>4)+Qa|0;Oa=((Ma*3|0)+8>>4)+Oa|0;c[La>>2]=((Qa*3|0)+16>>5)+Ka;c[Na>>2]=((Oa*3|0)+16>>5)+Ma;c[Pa>>2]=Oa;c[Ra>>2]=Qa;Ha=Ha+64|0}while((Ha|0)<(ya|0))}else{if(xa){Ha=wa}else{break}do{Ra=Ha|16;Lc(D+(Ra<<2)|0,D+(Ra+16<<2)|0,0,B,n);Pa=D+((Ha|48)<<2)|0;Lc(D+((Ha|32)<<2)|0,Pa,0,B,n);Qa=C+(Ha<<2)|0;Lc(Pa,Qa,0,B,n);Lc(Qa,C+(Ra<<2)|0,0,B,n);Ha=Ha+64|0}while((Ha|0)<(ya|0))}}}while(0);if(!((a[w>>0]|0)==0|t)){Yb(J,D,C,A,Ga,c[y+(Ga<<2)>>2]|0)}}Ga=Ga+1|0}while(Ga>>>0<x>>>0)}w=v?2:0;if(v&u){z=(q|0)==2;x=g|h^1;A=p?0:-32;C=A|16;B=r>>>0>3;y=(q|0)==0;N=p?0:-64;O=o?-32:0;v=(N|0)<(O|0);M=o?-16:16;L=o?0:32;K=p?16:-16;J=o?16:48;I=b+34140|0;F=b+34117|0;G=b+34272|0;H=b+34292|0;E=0;do{Q=E;E=E+1|0;P=c[b+(E<<2)+34652>>2]|0;D=c[b+(E<<2)+34716>>2]|0;if(m){Q=255}else{Q=c[(c[(c[H>>2]|0)+((c[G>>2]|0)*240|0)+(Q<<2)+128>>2]|0)+((d[F>>0]|0)*20|0)+4>>2]|0}do{if(!e){T=D+128|0;S=D+64|0;R=D+192|0;if((c[I>>2]|0)==0){de(D,T,S,R);break}else{Ra=c[S>>2]|0;Qa=c[R>>2]|0;Oa=Qa+(c[D>>2]|0)|0;Pa=(c[T>>2]|0)-Ra|0;Na=Oa-Pa>>1;Qa=Na-Qa|0;Ra=Na-Ra|0;c[D>>2]=Oa-Ra<<1;c[T>>2]=Qa+Pa<<1;c[S>>2]=Qa<<1;c[R>>2]=Ra<<1;break}}}while(0);do{if(z){if(!x){Pa=P+(C<<2)|0;Ra=D+(A<<2)|0;Qa=((c[Pa>>2]|0)+4>>3)+(c[Ra>>2]|0)|0;c[Ra>>2]=Qa;Qa=(Qa+2>>2)+(c[Pa>>2]|0)|0;c[Pa>>2]=Qa;c[Ra>>2]=(Qa+4>>3)+(c[Ra>>2]|0)}if(!h){if(g){Ra=f?D:P+64|0;Oa=Ra+ -128|0;Pa=c[Oa>>2]|0;Qa=(Pa+4>>3)+(c[Ra>>2]|0)|0;Pa=(Qa+2>>2)+Pa|0;c[Oa>>2]=Pa;c[Ra>>2]=(Pa+4>>3)+Qa;break}else{Ma=P+ -64|0;Oa=P+64|0;Qa=D+ -128|0;Ka=c[Qa>>2]|0;Ja=c[D>>2]|0;Ra=Ja+(c[Ma>>2]|0)|0;Pa=Ka+(c[Oa>>2]|0)|0;Na=(Ra+2>>2)+Pa|0;La=(Na+1>>1)+Ra|0;Na=(La+2>>2)+Na|0;Ra=(La+1>>1)+(Ja-(Ra+1>>1))|0;Pa=(Na+1>>1)+(Ka-(Pa+1>>1))|0;c[Ma>>2]=La-Ra;c[Oa>>2]=Na-Pa;c[Qa>>2]=Pa;c[D>>2]=Ra;break}}}}while(0);b:do{if(!B){if(!(f|(K|0)>=(J|0))){R=K;do{Jc(P+(R<<2)|0);R=R+32|0}while((R|0)<(J|0))}if(!(l|(A|0)>=(L|0))){R=A;do{Jc(D+(R<<2)|0);R=R+32|0}while((R|0)<(L|0))}if(!y){if(t){if(f){if(v){P=N}else{break}while(1){Ra=P|4;Qa=D+(Ra<<2)|0;Ma=D+((P|5)<<2)|0;Pa=D+(Ra+28<<2)|0;La=D+(Ra+29<<2)|0;Ia=c[Pa>>2]|0;Na=c[La>>2]|0;Oa=Na+(c[Ma>>2]|0)|0;Ka=Ia+(c[Qa>>2]|0)|0;Ja=Ka+1>>1;Ia=Ia-Ja-(Na+1-(Oa+1>>1)>>1)|0;Na=(Ia+1>>1)+Na|0;Ja=Ia+Ja|0;Oa=((Na*3|0)+16>>5)-Na+Oa|0;Ka=((Ja*3|0)+16>>5)-Ja+Ka|0;Na=((Oa*3|0)+8>>4)+Na|0;Ja=((Ka*3|0)+8>>4)+Ja|0;c[Ma>>2]=((Na*3|0)+16>>5)+Oa;c[Qa>>2]=((Ja*3|0)+16>>5)+Ka;c[Pa>>2]=Ja;c[La>>2]=Na;La=D+((P|7)<<2)|0;Na=D+((P|6)<<2)|0;Pa=D+(Ra+30<<2)|0;Ra=D+(Ra+31<<2)|0;Ja=c[Pa>>2]|0;Qa=c[Ra>>2]|0;Ka=Qa+(c[La>>2]|0)|0;Ma=Ja+(c[Na>>2]|0)|0;Oa=Ma+1>>1;Ja=Ja-Oa-(Qa+1-(Ka+1>>1)>>1)|0;Qa=(Ja+1>>1)+Qa|0;Oa=Ja+Oa|0;Ka=((Qa*3|0)+16>>5)-Qa+Ka|0;Ma=((Oa*3|0)+16>>5)-Oa+Ma|0;Qa=((Ka*3|0)+8>>4)+Qa|0;Oa=((Ma*3|0)+8>>4)+Oa|0;c[La>>2]=((Qa*3|0)+16>>5)+Ka;c[Na>>2]=((Oa*3|0)+16>>5)+Ma;c[Pa>>2]=Oa;c[Ra>>2]=Qa;P=P+32|0;if((P|0)>=(O|0)){break b}}}if(!p){break}if(!l){Qa=P+104|0;Ra=P+96|0;La=D+8|0;Ma=c[D>>2]|0;Na=c[La>>2]|0;Ka=Na+(c[Qa>>2]|0)|0;Ja=Ma+(c[Ra>>2]|0)|0;Pa=Ja+1>>1;Ma=Ma-Pa-(Na+1-(Ka+1>>1)>>1)|0;Na=(Ma+1>>1)+Na|0;Pa=Ma+Pa|0;Ka=((Na*3|0)+16>>5)-Na+Ka|0;Ja=((Pa*3|0)+16>>5)-Pa+Ja|0;Na=((Ka*3|0)+8>>4)+Na|0;Pa=((Ja*3|0)+8>>4)+Pa|0;c[Qa>>2]=((Na*3|0)+16>>5)+Ka;c[Ra>>2]=((Pa*3|0)+16>>5)+Ja;c[D>>2]=Pa;c[La>>2]=Na;La=P+108|0;Na=P+100|0;Pa=D+4|0;Ra=D+12|0;Ja=c[Pa>>2]|0;Qa=c[Ra>>2]|0;Ka=Qa+(c[La>>2]|0)|0;Ma=Ja+(c[Na>>2]|0)|0;Oa=Ma+1>>1;Ja=Ja-Oa-(Qa+1-(Ka+1>>1)>>1)|0;Qa=(Ja+1>>1)+Qa|0;Oa=Ja+Oa|0;Ka=((Qa*3|0)+16>>5)-Qa+Ka|0;Ma=((Oa*3|0)+16>>5)-Oa+Ma|0;Qa=((Ka*3|0)+8>>4)+Qa|0;Oa=((Ma*3|0)+8>>4)+Oa|0;c[La>>2]=((Qa*3|0)+16>>5)+Ka;c[Na>>2]=((Oa*3|0)+16>>5)+Ma;c[Pa>>2]=Oa;c[Ra>>2]=Qa}Ka=P+40|0;Ja=P+32|0;Pa=P+64|0;La=P+72|0;Oa=c[Pa>>2]|0;Na=c[La>>2]|0;Ma=Na+(c[Ka>>2]|0)|0;Qa=Oa+(c[Ja>>2]|0)|0;Ra=Qa+1>>1;Oa=Oa-Ra-(Na+1-(Ma+1>>1)>>1)|0;Na=(Oa+1>>1)+Na|0;Ra=Oa+Ra|0;Ma=((Na*3|0)+16>>5)-Na+Ma|0;Qa=((Ra*3|0)+16>>5)-Ra+Qa|0;Na=((Ma*3|0)+8>>4)+Na|0;Ra=((Qa*3|0)+8>>4)+Ra|0;c[Ka>>2]=((Na*3|0)+16>>5)+Ma;c[Ja>>2]=((Ra*3|0)+16>>5)+Qa;c[Pa>>2]=Ra;c[La>>2]=Na;La=P+44|0;Na=P+36|0;Pa=P+68|0;Ra=P+76|0;Ja=c[Pa>>2]|0;Qa=c[Ra>>2]|0;Ka=Qa+(c[La>>2]|0)|0;Ma=Ja+(c[Na>>2]|0)|0;Oa=Ma+1>>1;Ja=Ja-Oa-(Qa+1-(Ka+1>>1)>>1)|0;Qa=(Ja+1>>1)+Qa|0;Oa=Ja+Oa|0;Ka=((Qa*3|0)+16>>5)-Qa+Ka|0;Ma=((Oa*3|0)+16>>5)-Oa+Ma|0;Qa=((Ka*3|0)+8>>4)+Qa|0;Oa=((Ma*3|0)+8>>4)+Oa|0;c[La>>2]=((Qa*3|0)+16>>5)+Ka;c[Na>>2]=((Oa*3|0)+16>>5)+Ma;c[Pa>>2]=Oa;c[Ra>>2]=Qa;break}if(l){R=-48;do{Ka=P+((R|15)<<2)|0;Ja=P+((R|14)<<2)|0;Pa=P+(R+42<<2)|0;La=P+(R+43<<2)|0;Oa=c[Pa>>2]|0;Na=c[La>>2]|0;Ma=Na+(c[Ka>>2]|0)|0;Qa=Oa+(c[Ja>>2]|0)|0;Ra=Qa+1>>1;Oa=Oa-Ra-(Na+1-(Ma+1>>1)>>1)|0;Na=(Oa+1>>1)+Na|0;Ra=Oa+Ra|0;Ma=((Na*3|0)+16>>5)-Na+Ma|0;Qa=((Ra*3|0)+16>>5)-Ra+Qa|0;Na=((Ma*3|0)+8>>4)+Na|0;Ra=((Qa*3|0)+8>>4)+Ra|0;c[Ka>>2]=((Na*3|0)+16>>5)+Ma;c[Ja>>2]=((Ra*3|0)+16>>5)+Qa;c[Pa>>2]=Ra;c[La>>2]=Na;La=P+((R|13)<<2)|0;Na=P+((R|12)<<2)|0;Pa=P+(R+40<<2)|0;Ra=P+(R+41<<2)|0;Ja=c[Pa>>2]|0;Qa=c[Ra>>2]|0;Ka=Qa+(c[La>>2]|0)|0;Ma=Ja+(c[Na>>2]|0)|0;Oa=Ma+1>>1;Ja=Ja-Oa-(Qa+1-(Ka+1>>1)>>1)|0;Qa=(Ja+1>>1)+Qa|0;Oa=Ja+Oa|0;Ka=((Qa*3|0)+16>>5)-Qa+Ka|0;Ma=((Oa*3|0)+16>>5)-Oa+Ma|0;Qa=((Ka*3|0)+8>>4)+Qa|0;Oa=((Ma*3|0)+8>>4)+Oa|0;c[La>>2]=((Qa*3|0)+16>>5)+Ka;c[Na>>2]=((Oa*3|0)+16>>5)+Ma;c[Pa>>2]=Oa;c[Ra>>2]=Qa;R=R+32|0}while((R|0)<(M|0))}else{R=-48;do{Lc(P+(R<<2)|0,D+(R+ -16<<2)|0,32,Q,n);R=R+32|0}while((R|0)<(M|0))}if(o){if(!l){Ka=P+ -8|0;Ja=P+ -16|0;Pa=D+ -112|0;La=D+ -104|0;Oa=c[Pa>>2]|0;Na=c[La>>2]|0;Ma=Na+(c[Ka>>2]|0)|0;Qa=Oa+(c[Ja>>2]|0)|0;Ra=Qa+1>>1;Oa=Oa-Ra-(Na+1-(Ma+1>>1)>>1)|0;Na=(Oa+1>>1)+Na|0;Ra=Oa+Ra|0;Ma=((Na*3|0)+16>>5)-Na+Ma|0;Qa=((Ra*3|0)+16>>5)-Ra+Qa|0;Na=((Ma*3|0)+8>>4)+Na|0;Ra=((Qa*3|0)+8>>4)+Ra|0;c[Ka>>2]=((Na*3|0)+16>>5)+Ma;c[Ja>>2]=((Ra*3|0)+16>>5)+Qa;c[Pa>>2]=Ra;c[La>>2]=Na;La=P+ -4|0;Na=P+ -12|0;Pa=D+ -108|0;Ra=D+ -100|0;Ja=c[Pa>>2]|0;Qa=c[Ra>>2]|0;Ka=Qa+(c[La>>2]|0)|0;Ma=Ja+(c[Na>>2]|0)|0;Oa=Ma+1>>1;Ja=Ja-Oa-(Qa+1-(Ka+1>>1)>>1)|0;Qa=(Ja+1>>1)+Qa|0;Oa=Ja+Oa|0;Ka=((Qa*3|0)+16>>5)-Qa+Ka|0;Ma=((Oa*3|0)+16>>5)-Oa+Ma|0;Qa=((Ka*3|0)+8>>4)+Qa|0;Oa=((Ma*3|0)+8>>4)+Oa|0;c[La>>2]=((Qa*3|0)+16>>5)+Ka;c[Na>>2]=((Oa*3|0)+16>>5)+Ma;c[Pa>>2]=Oa;c[Ra>>2]=Qa}Ka=P+ -72|0;Ja=P+ -80|0;Pa=P+ -48|0;La=P+ -40|0;Oa=c[Pa>>2]|0;Na=c[La>>2]|0;Ma=Na+(c[Ka>>2]|0)|0;Qa=Oa+(c[Ja>>2]|0)|0;Ra=Qa+1>>1;Oa=Oa-Ra-(Na+1-(Ma+1>>1)>>1)|0;Na=(Oa+1>>1)+Na|0;Ra=Oa+Ra|0;Ma=((Na*3|0)+16>>5)-Na+Ma|0;Qa=((Ra*3|0)+16>>5)-Ra+Qa|0;Na=((Ma*3|0)+8>>4)+Na|0;Ra=((Qa*3|0)+8>>4)+Ra|0;c[Ka>>2]=((Na*3|0)+16>>5)+Ma;c[Ja>>2]=((Ra*3|0)+16>>5)+Qa;c[Pa>>2]=Ra;c[La>>2]=Na;La=P+ -68|0;Na=P+ -76|0;Pa=P+ -44|0;Ra=P+ -36|0;Ja=c[Pa>>2]|0;Qa=c[Ra>>2]|0;Ka=Qa+(c[La>>2]|0)|0;Ma=Ja+(c[Na>>2]|0)|0;Oa=Ma+1>>1;Ja=Ja-Oa-(Qa+1-(Ka+1>>1)>>1)|0;Qa=(Ja+1>>1)+Qa|0;Oa=Ja+Oa|0;Ka=((Qa*3|0)+16>>5)-Qa+Ka|0;Ma=((Oa*3|0)+16>>5)-Oa+Ma|0;Qa=((Ka*3|0)+8>>4)+Qa|0;Oa=((Ma*3|0)+8>>4)+Oa|0;c[La>>2]=((Qa*3|0)+16>>5)+Ka;c[Na>>2]=((Oa*3|0)+16>>5)+Ma;c[Pa>>2]=Oa;c[Ra>>2]=Qa}else{Lc(P+ -128|0,P+ -64|0,32,Q,n)}Lc(P+ -256|0,P+ -192|0,32,Q,n)}}}while(0)}while(E>>>0<w>>>0)}t=s?2:0;if(!(s&u)){i=k;return 0}u=(q|0)==2;v=p?16:-48;w=v+16|0;x=p?0:-64;s=x|48;r=r>>>0>3;ba=(q|0)==0;fa=p?0:-128;ga=o?-64:0;R=(fa|0)<(ga|0);D=p?0:-60;F=D|58;E=F+ -2|0;C=D|2;B=D|59;A=F+ -1|0;z=D|1;y=D|3;ea=p?10:-50;da=ea+ -2|0;ca=ea+6|0;$=ea+8|0;_=ea|1;Y=ea+ -1|0;Z=ea+7|0;X=ea+9|0;W=ea|16;V=W+ -2|0;U=W+6|0;T=W+8|0;S=ea|17;q=W+ -1|0;Q=W+7|0;P=W+9|0;O=p?42:-18;N=O+ -2|0;M=O+6|0;L=O+8|0;K=O|1;J=O+ -1|0;I=O+7|0;H=O+9|0;G=o?0:64;p=p?48:-16;o=o?48:112;ha=b+34140|0;ka=b+34117|0;ja=b+34272|0;ia=b+34292|0;la=0;do{oa=la;la=la+1|0;ma=c[b+(la<<2)+34652>>2]|0;na=c[b+(la<<2)+34716>>2]|0;if(m){oa=255}else{oa=c[(c[(c[ia>>2]|0)+((c[ja>>2]|0)*240|0)+(oa<<2)+128>>2]|0)+((d[ka>>0]|0)*20|0)+4>>2]|0}do{if(!e?(c[(c[j>>2]|0)+12>>2]|0)>>>0<16:0){ra=na+128|0;ta=c[ra>>2]|0;qa=(c[na>>2]|0)-(ta+1>>1)|0;c[na>>2]=qa;ta=qa+ta|0;c[ra>>2]=ta;pa=na+256|0;ua=na+64|0;sa=na+320|0;if((c[ha>>2]|0)==0){de(na,pa,ua,sa);de(ra,na+384|0,na+192|0,na+448|0);break}else{Na=c[ua>>2]|0;Pa=c[sa>>2]|0;Qa=Pa+qa|0;Ra=(c[pa>>2]|0)-Na|0;Oa=Qa-Ra>>1;Pa=Oa-Pa|0;Na=Oa-Na|0;c[na>>2]=Qa-Na<<1;c[pa>>2]=Pa+Ra<<1;c[ua>>2]=Pa<<1;c[sa>>2]=Na<<1;Na=na+384|0;Pa=na+192|0;Ra=na+448|0;Qa=c[Pa>>2]|0;Oa=c[Ra>>2]|0;La=Oa+ta|0;Ma=(c[Na>>2]|0)-Qa|0;Ka=La-Ma>>1;Oa=Ka-Oa|0;Qa=Ka-Qa|0;c[ra>>2]=La-Qa<<1;c[Na>>2]=Oa+Ma<<1;c[Pa>>2]=Oa<<1;c[Ra>>2]=Qa<<1;break}}}while(0);do{if(u){if(l){if(h){break}Oa=ma+ -64|0;Ra=ma+192|0;Pa=c[Oa>>2]|0;Qa=(Pa+4>>3)+(c[Ra>>2]|0)|0;Pa=(Qa+2>>2)+Pa|0;c[Oa>>2]=Pa;c[Ra>>2]=(Pa+4>>3)+Qa;break}if(h){if(!f){Pa=ma+(s<<2)|0;Ra=na+(x<<2)|0;Qa=((c[Pa>>2]|0)+4>>3)+(c[Ra>>2]|0)|0;c[Ra>>2]=Qa;Qa=(Qa+2>>2)+(c[Pa>>2]|0)|0;c[Pa>>2]=Qa;c[Ra>>2]=(Qa+4>>3)+(c[Ra>>2]|0)}Oa=na+(v<<2)|0;Ra=na+(w<<2)|0;Pa=c[Oa>>2]|0;Qa=(Pa+4>>3)+(c[Ra>>2]|0)|0;Pa=(Qa+2>>2)+Pa|0;c[Oa>>2]=Pa;c[Ra>>2]=(Pa+4>>3)+Qa;break}if(f){Qa=na+ -256|0;Ra=c[Qa>>2]|0;pa=(Ra+4>>3)+(c[na>>2]|0)|0;c[na>>2]=pa;Ra=(pa+2>>2)+Ra|0;c[Qa>>2]=Ra;pa=(Ra+4>>3)+pa|0}else{Na=ma+ -64|0;Pa=ma+192|0;Ra=na+ -256|0;La=c[Ra>>2]|0;Ka=c[na>>2]|0;pa=Ka+(c[Na>>2]|0)|0;Qa=La+(c[Pa>>2]|0)|0;Oa=(pa+2>>2)+Qa|0;Ma=(Oa+1>>1)+pa|0;Oa=(Ma+2>>2)+Oa|0;pa=(Ma+1>>1)+(Ka-(pa+1>>1))|0;Qa=(Oa+1>>1)+(La-(Qa+1>>1))|0;c[Na>>2]=Ma-pa;c[Pa>>2]=Oa-Qa;c[Ra>>2]=Qa}c[na>>2]=pa;La=na+ -192|0;Na=na+64|0;Pa=na+ -128|0;Ra=na+128|0;Ja=c[Pa>>2]|0;Ia=c[Ra>>2]|0;Qa=Ia+(c[La>>2]|0)|0;Oa=Ja+(c[Na>>2]|0)|0;Ma=(Qa+2>>2)+Oa|0;Ka=(Ma+1>>1)+Qa|0;Ma=(Ka+2>>2)+Ma|0;Qa=(Ka+1>>1)+(Ia-(Qa+1>>1))|0;Oa=(Ma+1>>1)+(Ja-(Oa+1>>1))|0;c[La>>2]=Ka-Qa;c[Na>>2]=Ma-Oa;c[Pa>>2]=Oa;c[Ra>>2]=Qa}}while(0);c:do{if(!r){if(!(f|(p|0)>=(o|0))){pa=p;do{Jc(ma+(pa<<2)|0);pa=pa+64|0}while((pa|0)<(o|0))}if(!(l|(x|0)>=(G|0))){pa=x;do{Jc(na+(pa<<2)|0);Jc(na+((pa|16)<<2)|0);Jc(na+((pa|32)<<2)|0);pa=pa+64|0}while((pa|0)<(G|0))}if(!ba){if(!f){if(h){Ka=ma+(O<<2)|0;Ja=ma+(N<<2)|0;Pa=ma+(M<<2)|0;La=ma+(L<<2)|0;Oa=c[Pa>>2]|0;Na=c[La>>2]|0;Ma=Na+(c[Ka>>2]|0)|0;Qa=Oa+(c[Ja>>2]|0)|0;Ra=Qa+1>>1;Oa=Oa-Ra-(Na+1-(Ma+1>>1)>>1)|0;Na=(Oa+1>>1)+Na|0;Ra=Oa+Ra|0;Ma=((Na*3|0)+16>>5)-Na+Ma|0;Qa=((Ra*3|0)+16>>5)-Ra+Qa|0;Na=((Ma*3|0)+8>>4)+Na|0;Ra=((Qa*3|0)+8>>4)+Ra|0;c[Ka>>2]=((Na*3|0)+16>>5)+Ma;c[Ja>>2]=((Ra*3|0)+16>>5)+Qa;c[Pa>>2]=Ra;c[La>>2]=Na;La=ma+(K<<2)|0;Na=ma+(J<<2)|0;Pa=ma+(I<<2)|0;Ra=ma+(H<<2)|0;Ja=c[Pa>>2]|0;Qa=c[Ra>>2]|0;Ka=Qa+(c[La>>2]|0)|0;Ma=Ja+(c[Na>>2]|0)|0;Oa=Ma+1>>1;Ja=Ja-Oa-(Qa+1-(Ka+1>>1)>>1)|0;Qa=(Ja+1>>1)+Qa|0;Oa=Ja+Oa|0;Ka=((Qa*3|0)+16>>5)-Qa+Ka|0;Ma=((Oa*3|0)+16>>5)-Oa+Ma|0;Qa=((Ka*3|0)+8>>4)+Qa|0;Oa=((Ma*3|0)+8>>4)+Oa|0;c[La>>2]=((Qa*3|0)+16>>5)+Ka;c[Na>>2]=((Oa*3|0)+16>>5)+Ma;c[Pa>>2]=Oa;c[Ra>>2]=Qa}if(R){pa=fa;do{Lc(ma+((pa|32)<<2)|0,ma+((pa|48)<<2)|0,0,oa,n);pa=pa+64|0}while((pa|0)<(ga|0))}}if(!l){if(h){Qa=na+(ea<<2)|0;Ra=na+(da<<2)|0;Pa=na+(ca<<2)|0;Ma=na+($<<2)|0;La=c[Pa>>2]|0;Oa=c[Ma>>2]|0;Na=Oa+(c[Qa>>2]|0)|0;Ja=La+(c[Ra>>2]|0)|0;Ka=Ja+1>>1;La=La-Ka-(Oa+1-(Na+1>>1)>>1)|0;Oa=(La+1>>1)+Oa|0;Ka=La+Ka|0;Na=((Oa*3|0)+16>>5)-Oa+Na|0;Ja=((Ka*3|0)+16>>5)-Ka+Ja|0;Oa=((Na*3|0)+8>>4)+Oa|0;Ka=((Ja*3|0)+8>>4)+Ka|0;c[Qa>>2]=((Oa*3|0)+16>>5)+Na;c[Ra>>2]=((Ka*3|0)+16>>5)+Ja;c[Pa>>2]=Ka;c[Ma>>2]=Oa;Ma=na+(_<<2)|0;Oa=na+(Y<<2)|0;Pa=na+(Z<<2)|0;Ka=na+(X<<2)|0;Ra=c[Pa>>2]|0;Ja=c[Ka>>2]|0;Qa=Ja+(c[Ma>>2]|0)|0;Na=Ra+(c[Oa>>2]|0)|0;La=Na+1>>1;Ra=Ra-La-(Ja+1-(Qa+1>>1)>>1)|0;Ja=(Ra+1>>1)+Ja|0;La=Ra+La|0;Qa=((Ja*3|0)+16>>5)-Ja+Qa|0;Na=((La*3|0)+16>>5)-La+Na|0;Ja=((Qa*3|0)+8>>4)+Ja|0;La=((Na*3|0)+8>>4)+La|0;c[Ma>>2]=((Ja*3|0)+16>>5)+Qa;c[Oa>>2]=((La*3|0)+16>>5)+Na;c[Pa>>2]=La;c[Ka>>2]=Ja;Ka=na+(W<<2)|0;Ja=na+(V<<2)|0;Pa=na+(U<<2)|0;La=na+(T<<2)|0;Oa=c[Pa>>2]|0;Na=c[La>>2]|0;Ma=Na+(c[Ka>>2]|0)|0;Qa=Oa+(c[Ja>>2]|0)|0;Ra=Qa+1>>1;Oa=Oa-Ra-(Na+1-(Ma+1>>1)>>1)|0;Na=(Oa+1>>1)+Na|0;Ra=Oa+Ra|0;Ma=((Na*3|0)+16>>5)-Na+Ma|0;Qa=((Ra*3|0)+16>>5)-Ra+Qa|0;Na=((Ma*3|0)+8>>4)+Na|0;Ra=((Qa*3|0)+8>>4)+Ra|0;c[Ka>>2]=((Na*3|0)+16>>5)+Ma;c[Ja>>2]=((Ra*3|0)+16>>5)+Qa;c[Pa>>2]=Ra;c[La>>2]=Na;La=na+(S<<2)|0;Na=na+(q<<2)|0;Pa=na+(Q<<2)|0;Ra=na+(P<<2)|0;Ja=c[Pa>>2]|0;Qa=c[Ra>>2]|0;Ka=Qa+(c[La>>2]|0)|0;Ma=Ja+(c[Na>>2]|0)|0;Oa=Ma+1>>1;Ja=Ja-Oa-(Qa+1-(Ka+1>>1)>>1)|0;Qa=(Ja+1>>1)+Qa|0;Oa=Ja+Oa|0;Ka=((Qa*3|0)+16>>5)-Qa+Ka|0;Ma=((Oa*3|0)+16>>5)-Oa+Ma|0;Qa=((Ka*3|0)+8>>4)+Qa|0;Oa=((Ma*3|0)+8>>4)+Oa|0;c[La>>2]=((Qa*3|0)+16>>5)+Ka;c[Na>>2]=((Oa*3|0)+16>>5)+Ma;c[Pa>>2]=Oa;c[Ra>>2]=Qa}if(R){pa=fa;do{Ra=pa|16;Qa=na+(Ra<<2)|0;Lc(na+(pa<<2)|0,Qa,0,oa,n);Lc(Qa,na+(Ra+16<<2)|0,0,oa,n);pa=pa+64|0}while((pa|0)<(ga|0))}}if(g){na=f?na+20|0:ma+244|0;if(R){ma=fa}else{break}while(1){Ka=na+(ma<<2)|0;Ja=na+(ma+ -1<<2)|0;Pa=na+((ma|59)<<2)|0;La=na+((ma|60)<<2)|0;Oa=c[Pa>>2]|0;Na=c[La>>2]|0;Ma=Na+(c[Ka>>2]|0)|0;Qa=Oa+(c[Ja>>2]|0)|0;Ra=Qa+1>>1;Oa=Oa-Ra-(Na+1-(Ma+1>>1)>>1)|0;Na=(Oa+1>>1)+Na|0;Ra=Oa+Ra|0;Ma=((Na*3|0)+16>>5)-Na+Ma|0;Qa=((Ra*3|0)+16>>5)-Ra+Qa|0;Na=((Ma*3|0)+8>>4)+Na|0;Ra=((Qa*3|0)+8>>4)+Ra|0;c[Ka>>2]=((Na*3|0)+16>>5)+Ma;c[Ja>>2]=((Ra*3|0)+16>>5)+Qa;c[Pa>>2]=Ra;c[La>>2]=Na;La=na+((ma|2)<<2)|0;Na=na+((ma|1)<<2)|0;Pa=na+((ma|61)<<2)|0;Ra=na+((ma|62)<<2)|0;Ja=c[Pa>>2]|0;Qa=c[Ra>>2]|0;Ka=Qa+(c[La>>2]|0)|0;Ma=Ja+(c[Na>>2]|0)|0;Oa=Ma+1>>1;Ja=Ja-Oa-(Qa+1-(Ka+1>>1)>>1)|0;Qa=(Ja+1>>1)+Qa|0;Oa=Ja+Oa|0;Ka=((Qa*3|0)+16>>5)-Qa+Ka|0;Ma=((Oa*3|0)+16>>5)-Oa+Ma|0;Qa=((Ka*3|0)+8>>4)+Qa|0;Oa=((Ma*3|0)+8>>4)+Oa|0;c[La>>2]=((Qa*3|0)+16>>5)+Ka;c[Na>>2]=((Oa*3|0)+16>>5)+Ma;c[Pa>>2]=Oa;c[Ra>>2]=Qa;ma=ma+64|0;if((ma|0)>=(ga|0)){break c}}}if(h){Ka=ma+(F<<2)|0;Ja=ma+(E<<2)|0;Pa=na+(D<<2)|0;La=na+(C<<2)|0;Oa=c[Pa>>2]|0;Na=c[La>>2]|0;Ma=Na+(c[Ka>>2]|0)|0;Qa=Oa+(c[Ja>>2]|0)|0;Ra=Qa+1>>1;Oa=Oa-Ra-(Na+1-(Ma+1>>1)>>1)|0;Na=(Oa+1>>1)+Na|0;Ra=Oa+Ra|0;Ma=((Na*3|0)+16>>5)-Na+Ma|0;Qa=((Ra*3|0)+16>>5)-Ra+Qa|0;Na=((Ma*3|0)+8>>4)+Na|0;Ra=((Qa*3|0)+8>>4)+Ra|0;c[Ka>>2]=((Na*3|0)+16>>5)+Ma;c[Ja>>2]=((Ra*3|0)+16>>5)+Qa;c[Pa>>2]=Ra;c[La>>2]=Na;La=ma+(B<<2)|0;Na=ma+(A<<2)|0;Pa=na+(z<<2)|0;Ra=na+(y<<2)|0;Ja=c[Pa>>2]|0;Qa=c[Ra>>2]|0;Ka=Qa+(c[La>>2]|0)|0;Ma=Ja+(c[Na>>2]|0)|0;Oa=Ma+1>>1;Ja=Ja-Oa-(Qa+1-(Ka+1>>1)>>1)|0;Qa=(Ja+1>>1)+Qa|0;Oa=Ja+Oa|0;Ka=((Qa*3|0)+16>>5)-Qa+Ka|0;Ma=((Oa*3|0)+16>>5)-Oa+Ma|0;Qa=((Ka*3|0)+8>>4)+Qa|0;Oa=((Ma*3|0)+8>>4)+Oa|0;c[La>>2]=((Qa*3|0)+16>>5)+Ka;c[Na>>2]=((Oa*3|0)+16>>5)+Ma;c[Pa>>2]=Oa;c[Ra>>2]=Qa}if(R){pa=fa;do{Lc(ma+((pa|48)<<2)|0,na+(pa<<2)|0,0,oa,n);pa=pa+64|0}while((pa|0)<(ga|0))}}}}while(0)}while(la>>>0<t>>>0);i=k;return 0}function Qc(b){b=b|0;var e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0,Y=0,Z=0,_=0,$=0,ba=0,ca=0;l=i;i=i+128|0;x=l+64|0;y=l;s=c[b+100>>2]|0;U=c[b+34128>>2]|0;r=c[b+34328>>2]|0;m=(r|0)==0;D=c[b+34332>>2]|0;q=(r|0)==(D|0);C=c[b+34324>>2]|0;g=(C|0)==0;F=g&1;p=(C|0)==(c[b+34336>>2]|0);E=p&1;n=g|p;h=m|q;v=g|m;k=p|q;e=(r|0)==1;o=(r|0)==(D+ -1|0);w=(U|0)==1;u=(U|0)==2;if((U+ -1|0)>>>0<2){z=1}else{z=c[b+34156>>2]|0}j=b+34232|0;t=c[(c[j>>2]|0)+12>>2]|0;A=b+68|0;G=a[A>>0]|0;B=G&255;a:do{if((c[b+32916>>2]|0)!=0){if(!m){K=c[b+34356>>2]|0;M=b+34372|0;c[M>>2]=0;I=b+34368|0;c[I>>2]=0;L=b+34356|0;if((K|0)!=0){J=c[b+16520>>2]|0;if(!(K>>>0>J>>>0)?(r+ -1|0)==(c[b+(K<<2)+16524>>2]|0):0){c[M>>2]=1}}else{f=7}}else{c[b+34360>>2]=0;c[b+34356>>2]=0;c[b+34372>>2]=0;I=b+34368|0;c[I>>2]=0;L=b+34356|0;f=7}if((f|0)==7){K=0;J=c[b+16520>>2]|0}if(K>>>0<J>>>0?(H=K+1|0,(r|0)==(c[b+(H<<2)+16524>>2]|0)):0){c[b+34360>>2]=1;c[L>>2]=H}else{c[b+34360>>2]=0;H=K}if(H>>>0<J>>>0?(r+1|0)==(c[b+(H+1<<2)+16524>>2]|0):0){c[I>>2]=1}if(g){c[b+34364>>2]=0;c[b+34352>>2]=0;H=0;break}I=(c[b+34348>>2]|0)==(C|0);if(I){H=C}else{H=b+34352|0;J=c[H>>2]|0;do{if(J>>>0<(c[b+132>>2]|0)>>>0){J=J+1|0;if((C|0)!=(c[b+(J<<2)+136>>2]|0)){if(I){H=C;break a}else{break}}else{c[b+34364>>2]=1;c[H>>2]=J;H=C;break a}}}while(0);c[b+34364>>2]=0;H=C}}else{H=b+34360|0;c[H+0>>2]=0;c[H+4>>2]=0;c[H+8>>2]=0;c[H+12>>2]=0;H=C}}while(0);C=b+34344|0;c[C>>2]=r;c[b+34348>>2]=H;if(!(G<<24>>24==0)){if((z|0)!=0){I=d[b+34116>>0]|0;J=c[b+34272>>2]|0;G=c[b+34292>>2]|0;K=(s|0)==0?2:1;H=0;do{c[x+(H<<2)>>2]=aa(c[(c[G+(J*240|0)+(H<<2)+64>>2]|0)+(I*20|0)+4>>2]<<B,K)|0;c[y+(H<<2)>>2]=c[(c[G+(J*240|0)+(H<<2)>>2]|0)+4>>2]<<B;H=H+1|0}while((H|0)!=(z|0))}if(m){Vb(b+34936|0,c[b+34156>>2]|0,D,F,E)}}B=t>>>0<16;if((z|0)!=0&B){P=(s|0)==2;M=b+34360|0;L=b+34364|0;O=t>>>0>3;N=(s|0)==0;F=b+34936|0;Q=m?0:-192;S=q?-64:64;R=(Q|0)<(S|0);T=m?0:-128;K=q?0:128;J=m?32:-96;I=q?32:160;H=b+34140|0;G=0;do{E=c[b+(G<<2)+34652>>2]|0;D=c[b+(G<<2)+34716>>2]|0;if(!k){if((a[A>>0]|0)!=0){Wb(F,D,c[C>>2]|0,G)}Kc(D);if(!((c[H>>2]|0)==0|(G|0)==0)){c[D>>2]=c[D>>2]<<1;U=D+64|0;c[U>>2]=c[U>>2]<<1;U=D+128|0;c[U>>2]=c[U>>2]<<1;U=D+192|0;c[U>>2]=c[U>>2]<<1;U=D+256|0;c[U>>2]=c[U>>2]<<1;U=D+320|0;c[U>>2]=c[U>>2]<<1;U=D+384|0;c[U>>2]=c[U>>2]<<1;U=D+448|0;c[U>>2]=c[U>>2]<<1;U=D+512|0;c[U>>2]=c[U>>2]<<1;U=D+576|0;c[U>>2]=c[U>>2]<<1;U=D+640|0;c[U>>2]=c[U>>2]<<1;U=D+704|0;c[U>>2]=c[U>>2]<<1;U=D+768|0;c[U>>2]=c[U>>2]<<1;U=D+832|0;c[U>>2]=c[U>>2]<<1;U=D+896|0;c[U>>2]=c[U>>2]<<1;U=D+960|0;c[U>>2]=c[U>>2]<<1}}do{if(P){if(!(!g?(c[L>>2]|0)==0:0)){f=45}do{if((f|0)==45){f=0;if(!(!m?(c[M>>2]|0)==0:0)){Y=D+256|0;W=D+64|0;U=D+320|0;X=c[W>>2]|0;V=c[U>>2]|0;_=V+(c[D>>2]|0)|0;Z=X+(c[Y>>2]|0)|0;V=V-(_+1>>1)|0;X=X-(Z+1>>1)|0;_=V+_|0;V=(_>>1)-V|0;_=(V*3>>3)+_|0;V=(_>>7)+V-(_>>10)+(_*3>>4)|0;_=((V*3|0)+4>>3)+_|0;V=V-(_>>1)|0;_=V+_|0;Z=X+Z|0;X=(Z>>1)-X|0;Z=(X*3>>3)+Z|0;X=(Z>>7)+X-(Z>>10)+(Z*3>>4)|0;Z=((X*3|0)+4>>3)+Z|0;X=X-(Z>>1)|0;Z=X+Z|0;X=(1-V>>1)+X|0;V=(1-X>>1)-V+(_+1>>1)|0;X=(Z+1>>1)-X|0;c[D>>2]=_-V;c[Y>>2]=Z-X;c[W>>2]=X;c[U>>2]=V}if(!g?(c[L>>2]|0)==0:0){break}if(!q?(c[M>>2]|0)==0:0){break}U=D+ -512|0;W=D+ -256|0;Y=D+ -448|0;_=D+ -192|0;X=c[Y>>2]|0;Z=c[_>>2]|0;$=Z+(c[U>>2]|0)|0;V=X+(c[W>>2]|0)|0;Z=Z-($+1>>1)|0;X=X-(V+1>>1)|0;$=Z+$|0;Z=($>>1)-Z|0;$=(Z*3>>3)+$|0;Z=($>>7)+Z-($>>10)+($*3>>4)|0;$=((Z*3|0)+4>>3)+$|0;Z=Z-($>>1)|0;$=Z+$|0;V=X+V|0;X=(V>>1)-X|0;V=(X*3>>3)+V|0;X=(V>>7)+X-(V>>10)+(V*3>>4)|0;V=((X*3|0)+4>>3)+V|0;X=X-(V>>1)|0;V=X+V|0;X=(1-Z>>1)+X|0;Z=(1-X>>1)-Z+($+1>>1)|0;X=(V+1>>1)-X|0;c[U>>2]=$-Z;c[W>>2]=V-X;c[Y>>2]=X;c[_>>2]=Z}}while(0);if(!(!p?(c[L>>2]|0)==0:0)){f=55}do{if((f|0)==55){f=0;if(!(!m?(c[M>>2]|0)==0:0)){V=E+128|0;X=E+384|0;Z=E+192|0;$=E+448|0;Y=c[Z>>2]|0;_=c[$>>2]|0;U=_+(c[V>>2]|0)|0;W=Y+(c[X>>2]|0)|0;_=_-(U+1>>1)|0;Y=Y-(W+1>>1)|0;U=_+U|0;_=(U>>1)-_|0;U=(_*3>>3)+U|0;_=(U>>7)+_-(U>>10)+(U*3>>4)|0;U=((_*3|0)+4>>3)+U|0;_=_-(U>>1)|0;U=_+U|0;W=Y+W|0;Y=(W>>1)-Y|0;W=(Y*3>>3)+W|0;Y=(W>>7)+Y-(W>>10)+(W*3>>4)|0;W=((Y*3|0)+4>>3)+W|0;Y=Y-(W>>1)|0;W=Y+W|0;Y=(1-_>>1)+Y|0;_=(1-Y>>1)-_+(U+1>>1)|0;Y=(W+1>>1)-Y|0;c[V>>2]=U-_;c[X>>2]=W-Y;c[Z>>2]=Y;c[$>>2]=_}if(!p?(c[L>>2]|0)==0:0){break}if(!q?(c[M>>2]|0)==0:0){break}V=E+ -384|0;X=E+ -128|0;Z=E+ -320|0;$=E+ -64|0;Y=c[Z>>2]|0;_=c[$>>2]|0;U=_+(c[V>>2]|0)|0;W=Y+(c[X>>2]|0)|0;_=_-(U+1>>1)|0;Y=Y-(W+1>>1)|0;U=_+U|0;_=(U>>1)-_|0;U=(_*3>>3)+U|0;_=(U>>7)+_-(U>>10)+(U*3>>4)|0;U=((_*3|0)+4>>3)+U|0;_=_-(U>>1)|0;U=_+U|0;W=Y+W|0;Y=(W>>1)-Y|0;W=(Y*3>>3)+W|0;Y=(W>>7)+Y-(W>>10)+(W*3>>4)|0;W=((Y*3|0)+4>>3)+W|0;Y=Y-(W>>1)|0;W=Y+W|0;Y=(1-_>>1)+Y|0;_=(1-Y>>1)-_+(U+1>>1)|0;Y=(W+1>>1)-Y|0;c[V>>2]=U-_;c[X>>2]=W-Y;c[Z>>2]=Y;c[$>>2]=_}}while(0);if(h){if(n){break}else{f=66}}else{if(!((c[M>>2]|0)==0|n)){f=66}}if((f|0)==66){f=0;do{if((c[L>>2]|0)==0){if(!(!m?(c[M>>2]|0)==0:0)){_=E+128|0;$=E+192|0;V=D+64|0;Z=c[D>>2]|0;X=c[V>>2]|0;U=X+(c[_>>2]|0)|0;Y=Z+(c[$>>2]|0)|0;X=X-(U+1>>1)|0;Z=Z-(Y+1>>1)|0;U=X+U|0;X=(U>>1)-X|0;U=(X*3>>3)+U|0;X=(U>>7)+X-(U>>10)+(U*3>>4)|0;U=((X*3|0)+4>>3)+U|0;X=X-(U>>1)|0;U=X+U|0;Y=Z+Y|0;Z=(Y>>1)-Z|0;Y=(Z*3>>3)+Y|0;Z=(Y>>7)+Z-(Y>>10)+(Y*3>>4)|0;Y=((Z*3|0)+4>>3)+Y|0;Z=Z-(Y>>1)|0;Y=Z+Y|0;Z=(1-X>>1)+Z|0;X=(1-Z>>1)-X+(U+1>>1)|0;Z=(Y+1>>1)-Z|0;c[_>>2]=U-X;c[$>>2]=Y-Z;c[D>>2]=Z;c[V>>2]=X;V=E+384|0;X=E+448|0;Z=D+256|0;$=D+320|0;Y=c[Z>>2]|0;_=c[$>>2]|0;U=_+(c[V>>2]|0)|0;W=Y+(c[X>>2]|0)|0;_=_-(U+1>>1)|0;Y=Y-(W+1>>1)|0;U=_+U|0;_=(U>>1)-_|0;U=(_*3>>3)+U|0;_=(U>>7)+_-(U>>10)+(U*3>>4)|0;U=((_*3|0)+4>>3)+U|0;_=_-(U>>1)|0;U=_+U|0;W=Y+W|0;Y=(W>>1)-Y|0;W=(Y*3>>3)+W|0;Y=(W>>7)+Y-(W>>10)+(W*3>>4)|0;W=((Y*3|0)+4>>3)+W|0;Y=Y-(W>>1)|0;W=Y+W|0;Y=(1-_>>1)+Y|0;_=(1-Y>>1)-_+(U+1>>1)|0;Y=(W+1>>1)-Y|0;c[V>>2]=U-_;c[X>>2]=W-Y;c[Z>>2]=Y;c[$>>2]=_}if(!q?(c[M>>2]|0)==0:0){break}U=E+ -384|0;Y=E+ -320|0;Z=D+ -512|0;V=D+ -448|0;$=c[Z>>2]|0;X=c[V>>2]|0;W=X+(c[U>>2]|0)|0;_=$+(c[Y>>2]|0)|0;X=X-(W+1>>1)|0;$=$-(_+1>>1)|0;W=X+W|0;X=(W>>1)-X|0;W=(X*3>>3)+W|0;X=(W>>7)+X-(W>>10)+(W*3>>4)|0;W=((X*3|0)+4>>3)+W|0;X=X-(W>>1)|0;W=X+W|0;_=$+_|0;$=(_>>1)-$|0;_=($*3>>3)+_|0;$=(_>>7)+$-(_>>10)+(_*3>>4)|0;_=(($*3|0)+4>>3)+_|0;$=$-(_>>1)|0;_=$+_|0;$=(1-X>>1)+$|0;X=(1-$>>1)-X+(W+1>>1)|0;$=(_+1>>1)-$|0;c[U>>2]=W-X;c[Y>>2]=_-$;c[Z>>2]=$;c[V>>2]=X;V=E+ -128|0;X=E+ -64|0;Z=D+ -256|0;$=D+ -192|0;Y=c[Z>>2]|0;_=c[$>>2]|0;U=_+(c[V>>2]|0)|0;W=Y+(c[X>>2]|0)|0;_=_-(U+1>>1)|0;Y=Y-(W+1>>1)|0;U=_+U|0;_=(U>>1)-_|0;U=(_*3>>3)+U|0;_=(U>>7)+_-(U>>10)+(U*3>>4)|0;U=((_*3|0)+4>>3)+U|0;_=_-(U>>1)|0;U=_+U|0;W=Y+W|0;Y=(W>>1)-Y|0;W=(Y*3>>3)+W|0;Y=(W>>7)+Y-(W>>10)+(W*3>>4)|0;W=((Y*3|0)+4>>3)+W|0;Y=Y-(W>>1)|0;W=Y+W|0;Y=(1-_>>1)+Y|0;_=(1-Y>>1)-_+(U+1>>1)|0;Y=(W+1>>1)-Y|0;c[V>>2]=U-_;c[X>>2]=W-Y;c[Z>>2]=Y;c[$>>2]=_}}while(0);if(h){break}}if(!(!n?(c[L>>2]|0)==0:0)){do{if((c[M>>2]|0)==0){if(!(!g?(c[L>>2]|0)==0:0)){_=D+ -512|0;$=D+ -256|0;V=D+256|0;Z=c[D>>2]|0;X=c[V>>2]|0;U=X+(c[_>>2]|0)|0;Y=Z+(c[$>>2]|0)|0;X=X-(U+1>>1)|0;Z=Z-(Y+1>>1)|0;U=X+U|0;X=(U>>1)-X|0;U=(X*3>>3)+U|0;X=(U>>7)+X-(U>>10)+(U*3>>4)|0;U=((X*3|0)+4>>3)+U|0;X=X-(U>>1)|0;U=X+U|0;Y=Z+Y|0;Z=(Y>>1)-Z|0;Y=(Z*3>>3)+Y|0;Z=(Y>>7)+Z-(Y>>10)+(Y*3>>4)|0;Y=((Z*3|0)+4>>3)+Y|0;Z=Z-(Y>>1)|0;Y=Z+Y|0;Z=(1-X>>1)+Z|0;X=(1-Z>>1)-X+(U+1>>1)|0;Z=(Y+1>>1)-Z|0;c[_>>2]=U-X;c[$>>2]=Y-Z;c[D>>2]=Z;c[V>>2]=X;V=D+ -448|0;X=D+ -192|0;Z=D+64|0;$=D+320|0;Y=c[Z>>2]|0;_=c[$>>2]|0;U=_+(c[V>>2]|0)|0;W=Y+(c[X>>2]|0)|0;_=_-(U+1>>1)|0;Y=Y-(W+1>>1)|0;U=_+U|0;_=(U>>1)-_|0;U=(_*3>>3)+U|0;_=(U>>7)+_-(U>>10)+(U*3>>4)|0;U=((_*3|0)+4>>3)+U|0;_=_-(U>>1)|0;U=_+U|0;W=Y+W|0;Y=(W>>1)-Y|0;W=(Y*3>>3)+W|0;Y=(W>>7)+Y-(W>>10)+(W*3>>4)|0;W=((Y*3|0)+4>>3)+W|0;Y=Y-(W>>1)|0;W=Y+W|0;Y=(1-_>>1)+Y|0;_=(1-Y>>1)-_+(U+1>>1)|0;Y=(W+1>>1)-Y|0;c[V>>2]=U-_;c[X>>2]=W-Y;c[Z>>2]=Y;c[$>>2]=_}if(!p?(c[L>>2]|0)==0:0){break}Z=E+128|0;U=E+ -384|0;Y=E+ -128|0;V=E+384|0;$=c[Z>>2]|0;X=c[V>>2]|0;W=X+(c[U>>2]|0)|0;_=$+(c[Y>>2]|0)|0;X=X-(W+1>>1)|0;$=$-(_+1>>1)|0;W=X+W|0;X=(W>>1)-X|0;W=(X*3>>3)+W|0;X=(W>>7)+X-(W>>10)+(W*3>>4)|0;W=((X*3|0)+4>>3)+W|0;X=X-(W>>1)|0;W=X+W|0;_=$+_|0;$=(_>>1)-$|0;_=($*3>>3)+_|0;$=(_>>7)+$-(_>>10)+(_*3>>4)|0;_=(($*3|0)+4>>3)+_|0;$=$-(_>>1)|0;_=$+_|0;$=(1-X>>1)+$|0;X=(1-$>>1)-X+(W+1>>1)|0;$=(_+1>>1)-$|0;c[U>>2]=W-X;c[Y>>2]=_-$;c[Z>>2]=$;c[V>>2]=X;V=E+ -320|0;X=E+ -64|0;Z=E+192|0;$=E+448|0;Y=c[Z>>2]|0;_=c[$>>2]|0;U=_+(c[V>>2]|0)|0;W=Y+(c[X>>2]|0)|0;_=_-(U+1>>1)|0;Y=Y-(W+1>>1)|0;U=_+U|0;_=(U>>1)-_|0;U=(_*3>>3)+U|0;_=(U>>7)+_-(U>>10)+(U*3>>4)|0;U=((_*3|0)+4>>3)+U|0;_=_-(U>>1)|0;U=_+U|0;W=Y+W|0;Y=(W>>1)-Y|0;W=(Y*3>>3)+W|0;Y=(W>>7)+Y-(W>>10)+(W*3>>4)|0;W=((Y*3|0)+4>>3)+W|0;Y=Y-(W>>1)|0;W=Y+W|0;Y=(1-_>>1)+Y|0;_=(1-Y>>1)-_+(U+1>>1)|0;Y=(W+1>>1)-Y|0;c[V>>2]=U-_;c[X>>2]=W-Y;c[Z>>2]=Y;c[$>>2]=_}}while(0);if(n){break}if((c[L>>2]|0)!=0){break}}if((c[M>>2]|0)==0){Oc(E,D)}}}while(0);if((a[A>>0]|0)!=0){Xb(F,E,D,c[C>>2]|0,G,c[y+(G<<2)>>2]|0)}if(!O){if(!(g|(J|0)>=(I|0))){U=J;do{Jc(E+(U<<2)|0);Jc(E+((U|16)<<2)|0);U=U+64|0}while((U|0)<(I|0))}if(!(p|(T|0)>=(K|0))){U=T;do{Jc(D+(U<<2)|0);Jc(D+((U|16)<<2)|0);U=U+64|0}while((U|0)<(K|0))}if(!N){if(!(!h?(c[M>>2]|0)==0:0)){f=97}do{if((f|0)==97){f=0;if(!(!g?(c[L>>2]|0)==0:0)){f=99}do{if((f|0)==99){f=0;if(!(!m?(c[M>>2]|0)==0:0)){X=D+4|0;Z=D+8|0;$=D+12|0;Y=c[Z>>2]|0;_=c[$>>2]|0;V=_+(c[D>>2]|0)|0;W=Y+(c[X>>2]|0)|0;_=_-(V+1>>1)|0;Y=Y-(W+1>>1)|0;V=_+V|0;_=(V>>1)-_|0;V=(_*3>>3)+V|0;_=(V>>7)+_-(V>>10)+(V*3>>4)|0;V=((_*3|0)+4>>3)+V|0;_=_-(V>>1)|0;V=_+V|0;W=Y+W|0;Y=(W>>1)-Y|0;W=(Y*3>>3)+W|0;Y=(W>>7)+Y-(W>>10)+(W*3>>4)|0;W=((Y*3|0)+4>>3)+W|0;Y=Y-(W>>1)|0;W=Y+W|0;Y=(1-_>>1)+Y|0;_=(1-Y>>1)-_+(V+1>>1)|0;Y=(W+1>>1)-Y|0;c[D>>2]=V-_;c[X>>2]=W-Y;c[Z>>2]=Y;c[$>>2]=_}if(!g?(c[L>>2]|0)==0:0){break}if(!q?(c[M>>2]|0)==0:0){break}V=D+ -236|0;X=D+ -240|0;Z=D+ -228|0;$=D+ -232|0;Y=c[Z>>2]|0;_=c[$>>2]|0;U=_+(c[V>>2]|0)|0;W=Y+(c[X>>2]|0)|0;_=_-(U+1>>1)|0;Y=Y-(W+1>>1)|0;U=_+U|0;_=(U>>1)-_|0;U=(_*3>>3)+U|0;_=(U>>7)+_-(U>>10)+(U*3>>4)|0;U=((_*3|0)+4>>3)+U|0;_=_-(U>>1)|0;U=_+U|0;W=Y+W|0;Y=(W>>1)-Y|0;W=(Y*3>>3)+W|0;Y=(W>>7)+Y-(W>>10)+(W*3>>4)|0;W=((Y*3|0)+4>>3)+W|0;Y=Y-(W>>1)|0;W=Y+W|0;Y=(1-_>>1)+Y|0;_=(1-Y>>1)-_+(U+1>>1)|0;Y=(W+1>>1)-Y|0;c[V>>2]=U-_;c[X>>2]=W-Y;c[Z>>2]=Y;c[$>>2]=_}}while(0);if(!(!p?(c[L>>2]|0)==0:0)){f=109}do{if((f|0)==109){f=0;if(!(!m?(c[M>>2]|0)==0:0)){V=E+232|0;X=E+236|0;Z=E+224|0;$=E+228|0;Y=c[Z>>2]|0;_=c[$>>2]|0;U=_+(c[V>>2]|0)|0;W=Y+(c[X>>2]|0)|0;_=_-(U+1>>1)|0;Y=Y-(W+1>>1)|0;U=_+U|0;_=(U>>1)-_|0;U=(_*3>>3)+U|0;_=(U>>7)+_-(U>>10)+(U*3>>4)|0;U=((_*3|0)+4>>3)+U|0;_=_-(U>>1)|0;U=_+U|0;W=Y+W|0;Y=(W>>1)-Y|0;W=(Y*3>>3)+W|0;Y=(W>>7)+Y-(W>>10)+(W*3>>4)|0;W=((Y*3|0)+4>>3)+W|0;Y=Y-(W>>1)|0;W=Y+W|0;Y=(1-_>>1)+Y|0;_=(1-Y>>1)-_+(U+1>>1)|0;Y=(W+1>>1)-Y|0;c[V>>2]=U-_;c[X>>2]=W-Y;c[Z>>2]=Y;c[$>>2]=_}if(!p?(c[L>>2]|0)==0:0){break}if(!q?(c[M>>2]|0)==0:0){break}V=E+ -4|0;X=E+ -8|0;Z=E+ -12|0;$=E+ -16|0;Y=c[Z>>2]|0;_=c[$>>2]|0;U=_+(c[V>>2]|0)|0;W=Y+(c[X>>2]|0)|0;_=_-(U+1>>1)|0;Y=Y-(W+1>>1)|0;U=_+U|0;_=(U>>1)-_|0;U=(_*3>>3)+U|0;_=(U>>7)+_-(U>>10)+(U*3>>4)|0;U=((_*3|0)+4>>3)+U|0;_=_-(U>>1)|0;U=_+U|0;W=Y+W|0;Y=(W>>1)-Y|0;W=(Y*3>>3)+W|0;Y=(W>>7)+Y-(W>>10)+(W*3>>4)|0;W=((Y*3|0)+4>>3)+W|0;Y=Y-(W>>1)|0;W=Y+W|0;Y=(1-_>>1)+Y|0;_=(1-Y>>1)-_+(U+1>>1)|0;Y=(W+1>>1)-Y|0;c[V>>2]=U-_;c[X>>2]=W-Y;c[Z>>2]=Y;c[$>>2]=_}}while(0);if(!(!m?(c[M>>2]|0)==0:0)){if(!g){_=E+104|0;V=E+96|0;Z=E+128|0;W=E+136|0;U=c[Z>>2]|0;$=c[W>>2]|0;X=$+(c[_>>2]|0)|0;Y=U+(c[V>>2]|0)|0;$=$-(X+1>>1)|0;U=U-(Y+1>>1)|0;X=$+X|0;$=(X>>1)-$|0;X=($*3>>3)+X|0;$=(X>>7)+$-(X>>10)+(X*3>>4)|0;X=(($*3|0)+4>>3)+X|0;$=$-(X>>1)|0;X=$+X|0;Y=U+Y|0;U=(Y>>1)-U|0;Y=(U*3>>3)+Y|0;U=(Y>>7)+U-(Y>>10)+(Y*3>>4)|0;Y=((U*3|0)+4>>3)+Y|0;U=U-(Y>>1)|0;Y=U+Y|0;U=(1-$>>1)+U|0;$=(1-U>>1)-$+(X+1>>1)|0;U=(Y+1>>1)-U|0;c[_>>2]=X-$;c[V>>2]=Y-U;c[Z>>2]=U;c[W>>2]=$;W=E+108|0;$=E+100|0;Z=E+132|0;U=E+140|0;V=c[Z>>2]|0;Y=c[U>>2]|0;_=Y+(c[W>>2]|0)|0;X=V+(c[$>>2]|0)|0;Y=Y-(_+1>>1)|0;V=V-(X+1>>1)|0;_=Y+_|0;Y=(_>>1)-Y|0;_=(Y*3>>3)+_|0;Y=(_>>7)+Y-(_>>10)+(_*3>>4)|0;_=((Y*3|0)+4>>3)+_|0;Y=Y-(_>>1)|0;_=Y+_|0;X=V+X|0;V=(X>>1)-V|0;X=(V*3>>3)+X|0;V=(X>>7)+V-(X>>10)+(X*3>>4)|0;X=((V*3|0)+4>>3)+X|0;V=V-(X>>1)|0;X=V+X|0;V=(1-Y>>1)+V|0;Y=(1-V>>1)-Y+(_+1>>1)|0;V=(X+1>>1)-V|0;c[W>>2]=_-Y;c[$>>2]=X-V;c[Z>>2]=V;c[U>>2]=Y;U=E+168|0;Y=E+160|0;Z=E+192|0;V=E+200|0;$=c[Z>>2]|0;X=c[V>>2]|0;W=X+(c[U>>2]|0)|0;_=$+(c[Y>>2]|0)|0;X=X-(W+1>>1)|0;$=$-(_+1>>1)|0;W=X+W|0;X=(W>>1)-X|0;W=(X*3>>3)+W|0;X=(W>>7)+X-(W>>10)+(W*3>>4)|0;W=((X*3|0)+4>>3)+W|0;X=X-(W>>1)|0;W=X+W|0;_=$+_|0;$=(_>>1)-$|0;_=($*3>>3)+_|0;$=(_>>7)+$-(_>>10)+(_*3>>4)|0;_=(($*3|0)+4>>3)+_|0;$=$-(_>>1)|0;_=$+_|0;$=(1-X>>1)+$|0;X=(1-$>>1)-X+(W+1>>1)|0;$=(_+1>>1)-$|0;c[U>>2]=W-X;c[Y>>2]=_-$;c[Z>>2]=$;c[V>>2]=X;V=E+172|0;X=E+164|0;Z=E+196|0;$=E+204|0;Y=c[Z>>2]|0;_=c[$>>2]|0;U=_+(c[V>>2]|0)|0;W=Y+(c[X>>2]|0)|0;_=_-(U+1>>1)|0;Y=Y-(W+1>>1)|0;U=_+U|0;_=(U>>1)-_|0;U=(_*3>>3)+U|0;_=(U>>7)+_-(U>>10)+(U*3>>4)|0;U=((_*3|0)+4>>3)+U|0;_=_-(U>>1)|0;U=_+U|0;W=Y+W|0;Y=(W>>1)-Y|0;W=(Y*3>>3)+W|0;Y=(W>>7)+Y-(W>>10)+(W*3>>4)|0;W=((Y*3|0)+4>>3)+W|0;Y=Y-(W>>1)|0;W=Y+W|0;Y=(1-_>>1)+Y|0;_=(1-Y>>1)-_+(U+1>>1)|0;Y=(W+1>>1)-Y|0;c[V>>2]=U-_;c[X>>2]=W-Y;c[Z>>2]=Y;c[$>>2]=_}if(!p){U=D+40|0;Y=D+32|0;Z=D+64|0;V=D+72|0;$=c[Z>>2]|0;X=c[V>>2]|0;W=X+(c[U>>2]|0)|0;_=$+(c[Y>>2]|0)|0;X=X-(W+1>>1)|0;$=$-(_+1>>1)|0;W=X+W|0;X=(W>>1)-X|0;W=(X*3>>3)+W|0;X=(W>>7)+X-(W>>10)+(W*3>>4)|0;W=((X*3|0)+4>>3)+W|0;X=X-(W>>1)|0;W=X+W|0;_=$+_|0;$=(_>>1)-$|0;_=($*3>>3)+_|0;$=(_>>7)+$-(_>>10)+(_*3>>4)|0;_=(($*3|0)+4>>3)+_|0;$=$-(_>>1)|0;_=$+_|0;$=(1-X>>1)+$|0;X=(1-$>>1)-X+(W+1>>1)|0;$=(_+1>>1)-$|0;c[U>>2]=W-X;c[Y>>2]=_-$;c[Z>>2]=$;c[V>>2]=X;V=D+44|0;X=D+36|0;Z=D+68|0;$=D+76|0;Y=c[Z>>2]|0;_=c[$>>2]|0;U=_+(c[V>>2]|0)|0;W=Y+(c[X>>2]|0)|0;_=_-(U+1>>1)|0;Y=Y-(W+1>>1)|0;U=_+U|0;_=(U>>1)-_|0;U=(_*3>>3)+U|0;_=(U>>7)+_-(U>>10)+(U*3>>4)|0;U=((_*3|0)+4>>3)+U|0;_=_-(U>>1)|0;U=_+U|0;W=Y+W|0;Y=(W>>1)-Y|0;W=(Y*3>>3)+W|0;Y=(W>>7)+Y-(W>>10)+(W*3>>4)|0;W=((Y*3|0)+4>>3)+W|0;Y=Y-(W>>1)|0;W=Y+W|0;Y=(1-_>>1)+Y|0;_=(1-Y>>1)-_+(U+1>>1)|0;Y=(W+1>>1)-Y|0;c[V>>2]=U-_;c[X>>2]=W-Y;c[Z>>2]=Y;c[$>>2]=_}if(!n?(c[L>>2]|0)==0:0){_=E+232|0;$=E+224|0;V=D+8|0;Z=c[D>>2]|0;X=c[V>>2]|0;U=X+(c[_>>2]|0)|0;Y=Z+(c[$>>2]|0)|0;X=X-(U+1>>1)|0;Z=Z-(Y+1>>1)|0;U=X+U|0;X=(U>>1)-X|0;U=(X*3>>3)+U|0;X=(U>>7)+X-(U>>10)+(U*3>>4)|0;U=((X*3|0)+4>>3)+U|0;X=X-(U>>1)|0;U=X+U|0;Y=Z+Y|0;Z=(Y>>1)-Z|0;Y=(Z*3>>3)+Y|0;Z=(Y>>7)+Z-(Y>>10)+(Y*3>>4)|0;Y=((Z*3|0)+4>>3)+Y|0;Z=Z-(Y>>1)|0;Y=Z+Y|0;Z=(1-X>>1)+Z|0;X=(1-Z>>1)-X+(U+1>>1)|0;Z=(Y+1>>1)-Z|0;c[_>>2]=U-X;c[$>>2]=Y-Z;c[D>>2]=Z;c[V>>2]=X;V=E+236|0;X=E+228|0;Z=D+4|0;$=D+12|0;Y=c[Z>>2]|0;_=c[$>>2]|0;U=_+(c[V>>2]|0)|0;W=Y+(c[X>>2]|0)|0;_=_-(U+1>>1)|0;Y=Y-(W+1>>1)|0;U=_+U|0;_=(U>>1)-_|0;U=(_*3>>3)+U|0;_=(U>>7)+_-(U>>10)+(U*3>>4)|0;U=((_*3|0)+4>>3)+U|0;_=_-(U>>1)|0;U=_+U|0;W=Y+W|0;Y=(W>>1)-Y|0;W=(Y*3>>3)+W|0;Y=(W>>7)+Y-(W>>10)+(W*3>>4)|0;W=((Y*3|0)+4>>3)+W|0;Y=Y-(W>>1)|0;W=Y+W|0;Y=(1-_>>1)+Y|0;_=(1-Y>>1)-_+(U+1>>1)|0;Y=(W+1>>1)-Y|0;c[V>>2]=U-_;c[X>>2]=W-Y;c[Z>>2]=Y;c[$>>2]=_}}if(!q?(c[M>>2]|0)==0:0){break}if(!g){_=E+ -136|0;V=E+ -144|0;Z=E+ -112|0;W=E+ -104|0;U=c[Z>>2]|0;$=c[W>>2]|0;X=$+(c[_>>2]|0)|0;Y=U+(c[V>>2]|0)|0;$=$-(X+1>>1)|0;U=U-(Y+1>>1)|0;X=$+X|0;$=(X>>1)-$|0;X=($*3>>3)+X|0;$=(X>>7)+$-(X>>10)+(X*3>>4)|0;X=(($*3|0)+4>>3)+X|0;$=$-(X>>1)|0;X=$+X|0;Y=U+Y|0;U=(Y>>1)-U|0;Y=(U*3>>3)+Y|0;U=(Y>>7)+U-(Y>>10)+(Y*3>>4)|0;Y=((U*3|0)+4>>3)+Y|0;U=U-(Y>>1)|0;Y=U+Y|0;U=(1-$>>1)+U|0;$=(1-U>>1)-$+(X+1>>1)|0;U=(Y+1>>1)-U|0;c[_>>2]=X-$;c[V>>2]=Y-U;c[Z>>2]=U;c[W>>2]=$;W=E+ -132|0;$=E+ -140|0;Z=E+ -108|0;U=E+ -100|0;V=c[Z>>2]|0;Y=c[U>>2]|0;_=Y+(c[W>>2]|0)|0;X=V+(c[$>>2]|0)|0;Y=Y-(_+1>>1)|0;V=V-(X+1>>1)|0;_=Y+_|0;Y=(_>>1)-Y|0;_=(Y*3>>3)+_|0;Y=(_>>7)+Y-(_>>10)+(_*3>>4)|0;_=((Y*3|0)+4>>3)+_|0;Y=Y-(_>>1)|0;_=Y+_|0;X=V+X|0;V=(X>>1)-V|0;X=(V*3>>3)+X|0;V=(X>>7)+V-(X>>10)+(X*3>>4)|0;X=((V*3|0)+4>>3)+X|0;V=V-(X>>1)|0;X=V+X|0;V=(1-Y>>1)+V|0;Y=(1-V>>1)-Y+(_+1>>1)|0;V=(X+1>>1)-V|0;c[W>>2]=_-Y;c[$>>2]=X-V;c[Z>>2]=V;c[U>>2]=Y;U=E+ -72|0;Y=E+ -80|0;Z=E+ -48|0;V=E+ -40|0;$=c[Z>>2]|0;X=c[V>>2]|0;W=X+(c[U>>2]|0)|0;_=$+(c[Y>>2]|0)|0;X=X-(W+1>>1)|0;$=$-(_+1>>1)|0;W=X+W|0;X=(W>>1)-X|0;W=(X*3>>3)+W|0;X=(W>>7)+X-(W>>10)+(W*3>>4)|0;W=((X*3|0)+4>>3)+W|0;X=X-(W>>1)|0;W=X+W|0;_=$+_|0;$=(_>>1)-$|0;_=($*3>>3)+_|0;$=(_>>7)+$-(_>>10)+(_*3>>4)|0;_=(($*3|0)+4>>3)+_|0;$=$-(_>>1)|0;_=$+_|0;$=(1-X>>1)+$|0;X=(1-$>>1)-X+(W+1>>1)|0;$=(_+1>>1)-$|0;c[U>>2]=W-X;c[Y>>2]=_-$;c[Z>>2]=$;c[V>>2]=X;V=E+ -68|0;X=E+ -76|0;Z=E+ -44|0;$=E+ -36|0;Y=c[Z>>2]|0;_=c[$>>2]|0;U=_+(c[V>>2]|0)|0;W=Y+(c[X>>2]|0)|0;_=_-(U+1>>1)|0;Y=Y-(W+1>>1)|0;U=_+U|0;_=(U>>1)-_|0;U=(_*3>>3)+U|0;_=(U>>7)+_-(U>>10)+(U*3>>4)|0;U=((_*3|0)+4>>3)+U|0;_=_-(U>>1)|0;U=_+U|0;W=Y+W|0;Y=(W>>1)-Y|0;W=(Y*3>>3)+W|0;Y=(W>>7)+Y-(W>>10)+(W*3>>4)|0;W=((Y*3|0)+4>>3)+W|0;Y=Y-(W>>1)|0;W=Y+W|0;Y=(1-_>>1)+Y|0;_=(1-Y>>1)-_+(U+1>>1)|0;Y=(W+1>>1)-Y|0;c[V>>2]=U-_;c[X>>2]=W-Y;c[Z>>2]=Y;c[$>>2]=_}if(!p){U=D+ -200|0;Y=D+ -208|0;Z=D+ -176|0;V=D+ -168|0;$=c[Z>>2]|0;X=c[V>>2]|0;W=X+(c[U>>2]|0)|0;_=$+(c[Y>>2]|0)|0;X=X-(W+1>>1)|0;$=$-(_+1>>1)|0;W=X+W|0;X=(W>>1)-X|0;W=(X*3>>3)+W|0;X=(W>>7)+X-(W>>10)+(W*3>>4)|0;W=((X*3|0)+4>>3)+W|0;X=X-(W>>1)|0;W=X+W|0;_=$+_|0;$=(_>>1)-$|0;_=($*3>>3)+_|0;$=(_>>7)+$-(_>>10)+(_*3>>4)|0;_=(($*3|0)+4>>3)+_|0;$=$-(_>>1)|0;_=$+_|0;$=(1-X>>1)+$|0;X=(1-$>>1)-X+(W+1>>1)|0;$=(_+1>>1)-$|0;c[U>>2]=W-X;c[Y>>2]=_-$;c[Z>>2]=$;c[V>>2]=X;V=D+ -196|0;X=D+ -204|0;Z=D+ -172|0;$=D+ -164|0;Y=c[Z>>2]|0;_=c[$>>2]|0;U=_+(c[V>>2]|0)|0;W=Y+(c[X>>2]|0)|0;_=_-(U+1>>1)|0;Y=Y-(W+1>>1)|0;U=_+U|0;_=(U>>1)-_|0;U=(_*3>>3)+U|0;_=(U>>7)+_-(U>>10)+(U*3>>4)|0;U=((_*3|0)+4>>3)+U|0;_=_-(U>>1)|0;U=_+U|0;W=Y+W|0;Y=(W>>1)-Y|0;W=(Y*3>>3)+W|0;Y=(W>>7)+Y-(W>>10)+(W*3>>4)|0;W=((Y*3|0)+4>>3)+W|0;Y=Y-(W>>1)|0;W=Y+W|0;Y=(1-_>>1)+Y|0;_=(1-Y>>1)-_+(U+1>>1)|0;Y=(W+1>>1)-Y|0;c[V>>2]=U-_;c[X>>2]=W-Y;c[Z>>2]=Y;c[$>>2]=_}if(!n?(c[L>>2]|0)==0:0){U=E+ -8|0;Y=E+ -16|0;Z=D+ -240|0;V=D+ -232|0;$=c[Z>>2]|0;X=c[V>>2]|0;W=X+(c[U>>2]|0)|0;_=$+(c[Y>>2]|0)|0;X=X-(W+1>>1)|0;$=$-(_+1>>1)|0;W=X+W|0;X=(W>>1)-X|0;W=(X*3>>3)+W|0;X=(W>>7)+X-(W>>10)+(W*3>>4)|0;W=((X*3|0)+4>>3)+W|0;X=X-(W>>1)|0;W=X+W|0;_=$+_|0;$=(_>>1)-$|0;_=($*3>>3)+_|0;$=(_>>7)+$-(_>>10)+(_*3>>4)|0;_=(($*3|0)+4>>3)+_|0;$=$-(_>>1)|0;_=$+_|0;$=(1-X>>1)+$|0;X=(1-$>>1)-X+(W+1>>1)|0;$=(_+1>>1)-$|0;c[U>>2]=W-X;c[Y>>2]=_-$;c[Z>>2]=$;c[V>>2]=X;V=E+ -4|0;X=E+ -12|0;Z=D+ -236|0;$=D+ -228|0;Y=c[Z>>2]|0;_=c[$>>2]|0;U=_+(c[V>>2]|0)|0;W=Y+(c[X>>2]|0)|0;_=_-(U+1>>1)|0;Y=Y-(W+1>>1)|0;U=_+U|0;_=(U>>1)-_|0;U=(_*3>>3)+U|0;_=(U>>7)+_-(U>>10)+(U*3>>4)|0;U=((_*3|0)+4>>3)+U|0;_=_-(U>>1)|0;U=_+U|0;W=Y+W|0;Y=(W>>1)-Y|0;W=(Y*3>>3)+W|0;Y=(W>>7)+Y-(W>>10)+(W*3>>4)|0;W=((Y*3|0)+4>>3)+W|0;Y=Y-(W>>1)|0;W=Y+W|0;Y=(1-_>>1)+Y|0;_=(1-Y>>1)-_+(U+1>>1)|0;Y=(W+1>>1)-Y|0;c[V>>2]=U-_;c[X>>2]=W-Y;c[Z>>2]=Y;c[$>>2]=_}}}while(0);if(g){if(R){U=Q;f=138}}else{if(!((c[L>>2]|0)==0|R^1)){U=Q;f=138}}if((f|0)==138){while(1){f=0;if((c[M>>2]|0)!=0&(U|0)==-64){U=U+64|0}else{ca=D+((U|5)<<2)|0;X=D+((U|4)<<2)|0;$=U+64|0;Y=D+($<<2)|0;ba=D+(U+65<<2)|0;_=c[Y>>2]|0;W=c[ba>>2]|0;V=W+(c[ca>>2]|0)|0;Z=_+(c[X>>2]|0)|0;W=W-(V+1>>1)|0;_=_-(Z+1>>1)|0;V=W+V|0;W=(V>>1)-W|0;V=(W*3>>3)+V|0;W=(V>>7)+W-(V>>10)+(V*3>>4)|0;V=((W*3|0)+4>>3)+V|0;W=W-(V>>1)|0;V=W+V|0;Z=_+Z|0;_=(Z>>1)-_|0;Z=(_*3>>3)+Z|0;_=(Z>>7)+_-(Z>>10)+(Z*3>>4)|0;Z=((_*3|0)+4>>3)+Z|0;_=_-(Z>>1)|0;Z=_+Z|0;_=(1-W>>1)+_|0;W=(1-_>>1)-W+(V+1>>1)|0;_=(Z+1>>1)-_|0;c[ca>>2]=V-W;c[X>>2]=Z-_;c[Y>>2]=_;c[ba>>2]=W;ba=D+((U|7)<<2)|0;W=D+((U|6)<<2)|0;Y=D+(U+66<<2)|0;_=D+(U+67<<2)|0;X=c[Y>>2]|0;Z=c[_>>2]|0;ca=Z+(c[ba>>2]|0)|0;V=X+(c[W>>2]|0)|0;Z=Z-(ca+1>>1)|0;X=X-(V+1>>1)|0;ca=Z+ca|0;Z=(ca>>1)-Z|0;ca=(Z*3>>3)+ca|0;Z=(ca>>7)+Z-(ca>>10)+(ca*3>>4)|0;ca=((Z*3|0)+4>>3)+ca|0;Z=Z-(ca>>1)|0;ca=Z+ca|0;V=X+V|0;X=(V>>1)-X|0;V=(X*3>>3)+V|0;X=(V>>7)+X-(V>>10)+(V*3>>4)|0;V=((X*3|0)+4>>3)+V|0;X=X-(V>>1)|0;V=X+V|0;X=(1-Z>>1)+X|0;Z=(1-X>>1)-Z+(ca+1>>1)|0;X=(V+1>>1)-X|0;c[ba>>2]=ca-Z;c[W>>2]=V-X;c[Y>>2]=X;c[_>>2]=Z;Mc(D+(U<<2)|0,D+((U|16)<<2)|0,0);U=$}if((U|0)<(S|0)){f=138}else{break}}}if(p){if(R){U=Q;f=145}}else{if(!((c[L>>2]|0)==0|R^1)){U=Q;f=145}}if((f|0)==145){while(1){f=0;if(!((c[M>>2]|0)!=0&(U|0)==-64)){ca=U|16;Mc(E+(ca<<2)|0,E+(ca+16<<2)|0,0);ca=U|48;Mc(E+((U|32)<<2)|0,E+(ca<<2)|0,0);Y=E+((U|63)<<2)|0;ba=E+((U|62)<<2)|0;$=E+(ca+74<<2)|0;X=E+(ca+75<<2)|0;_=c[$>>2]|0;Z=c[X>>2]|0;V=Z+(c[Y>>2]|0)|0;W=_+(c[ba>>2]|0)|0;Z=Z-(V+1>>1)|0;_=_-(W+1>>1)|0;V=Z+V|0;Z=(V>>1)-Z|0;V=(Z*3>>3)+V|0;Z=(V>>7)+Z-(V>>10)+(V*3>>4)|0;V=((Z*3|0)+4>>3)+V|0;Z=Z-(V>>1)|0;V=Z+V|0;W=_+W|0;_=(W>>1)-_|0;W=(_*3>>3)+W|0;_=(W>>7)+_-(W>>10)+(W*3>>4)|0;W=((_*3|0)+4>>3)+W|0;_=_-(W>>1)|0;W=_+W|0;_=(1-Z>>1)+_|0;Z=(1-_>>1)-Z+(V+1>>1)|0;_=(W+1>>1)-_|0;c[Y>>2]=V-Z;c[ba>>2]=W-_;c[$>>2]=_;c[X>>2]=Z;X=E+((U|61)<<2)|0;Z=E+((U|60)<<2)|0;$=E+(ca+72<<2)|0;ca=E+(ca+73<<2)|0;_=c[$>>2]|0;ba=c[ca>>2]|0;W=ba+(c[X>>2]|0)|0;Y=_+(c[Z>>2]|0)|0;ba=ba-(W+1>>1)|0;_=_-(Y+1>>1)|0;W=ba+W|0;ba=(W>>1)-ba|0;W=(ba*3>>3)+W|0;ba=(W>>7)+ba-(W>>10)+(W*3>>4)|0;W=((ba*3|0)+4>>3)+W|0;ba=ba-(W>>1)|0;W=ba+W|0;Y=_+Y|0;_=(Y>>1)-_|0;Y=(_*3>>3)+Y|0;_=(Y>>7)+_-(Y>>10)+(Y*3>>4)|0;Y=((_*3|0)+4>>3)+Y|0;_=_-(Y>>1)|0;Y=_+Y|0;_=(1-ba>>1)+_|0;ba=(1-_>>1)-ba+(W+1>>1)|0;_=(Y+1>>1)-_|0;c[X>>2]=W-ba;c[Z>>2]=Y-_;c[$>>2]=_;c[ca>>2]=ba}U=U+64|0;if((U|0)>=(S|0)){break}else{f=145}}}if(!n?!((c[L>>2]|0)!=0|R^1):0){U=Q;do{if(!((c[M>>2]|0)!=0&(U|0)==-64)){ca=U|16;Mc(E+(ca<<2)|0,E+(ca+16<<2)|0,0);$=E+((U|48)<<2)|0;Mc(E+((U|32)<<2)|0,$,0);ba=D+(U<<2)|0;Mc($,ba,0);Mc(ba,D+(ca<<2)|0,0)}U=U+64|0}while((U|0)<(S|0))}}if(!((a[A>>0]|0)==0|v)){Yb(F,E,D,c[C>>2]|0,G,c[x+(G<<2)>>2]|0)}}G=G+1|0}while(G>>>0<z>>>0)}x=w?2:0;if(w&B){L=(s|0)==2;G=b+34364|0;F=b+34360|0;K=b+34368|0;H=b+34372|0;J=t>>>0>3;I=(s|0)==0;D=m?32:-64;E=r>>>0<2;w=m?48:-48;y=b+34140|0;z=0;do{M=z;z=z+1|0;A=c[b+(z<<2)+34652>>2]|0;C=c[b+(z<<2)+34716>>2]|0;do{if(!k){P=C+128|0;O=C+64|0;N=C+192|0;if((c[y>>2]|0)==0){de(C,P,O,N);break}else{ca=c[O>>2]|0;ba=c[N>>2]|0;_=ba+(c[C>>2]|0)|0;$=(c[P>>2]|0)-ca|0;Z=_-$>>1;ba=Z-ba|0;ca=Z-ca|0;c[C>>2]=_-ca<<1;c[P>>2]=ba+$<<1;c[O>>2]=ba<<1;c[N>>2]=ca<<1;break}}}while(0);do{if(L){if(!(!e?(c[H>>2]|0)==0:0)){f=165}do{if((f|0)==165){f=0;if(!g?(c[G>>2]|0)==0:0){break}ca=C+ -256|0;c[ca>>2]=(c[ca>>2]|0)-(c[C+ -128>>2]|0)}}while(0);if(!(!o?(c[K>>2]|0)==0:0)){f=170}do{if((f|0)==170){f=0;if(!g?(c[G>>2]|0)==0:0){break}c[b+(M<<3)+34376>>2]=c[C>>2]}}while(0);if(!(!q?(c[F>>2]|0)==0:0)){f=175}do{if((f|0)==175){f=0;if(!g?(c[G>>2]|0)==0:0){break}ca=C+ -128|0;c[ca>>2]=(c[ca>>2]|0)-(c[b+(M<<3)+34376>>2]|0)}}while(0);if(!(!e?(c[H>>2]|0)==0:0)){f=180}do{if((f|0)==180){f=0;if(!p?(c[G>>2]|0)==0:0){break}ca=A+ -192|0;c[ca>>2]=(c[ca>>2]|0)-(c[A+ -64>>2]|0)}}while(0);if(!(!o?(c[K>>2]|0)==0:0)){f=185}do{if((f|0)==185){f=0;if(!p?(c[G>>2]|0)==0:0){break}c[b+(M<<3)+34380>>2]=c[A+64>>2]}}while(0);if(!(!q?(c[F>>2]|0)==0:0)){f=190}do{if((f|0)==190){f=0;if(!p?(c[G>>2]|0)==0:0){break}ca=A+ -64|0;c[ca>>2]=(c[ca>>2]|0)-(c[b+(M<<3)+34380>>2]|0)}}while(0);if(h){if(!n){f=196}}else{if((c[F>>2]|0)==0|n){f=204}else{f=196}}if((f|0)==196){f=0;do{if((c[G>>2]|0)==0){if(!(!m?(c[F>>2]|0)==0:0)){$=A+64|0;ba=c[$>>2]|0;ca=(ba+2>>2)+(c[C>>2]|0)|0;ba=(ca>>5)+ba+(ca>>9)+(ca>>13)+(ca+1>>1)|0;c[$>>2]=ba;c[C>>2]=(ba+2>>2)+ca}if(!q?(c[F>>2]|0)==0:0){break}_=A+ -64|0;ca=C+ -128|0;$=c[_>>2]|0;ba=($+2>>2)+(c[ca>>2]|0)|0;$=(ba>>5)+$+(ba>>9)+(ba>>13)+(ba+1>>1)|0;c[_>>2]=$;c[ca>>2]=($+2>>2)+ba}}while(0);if(!h){f=204}}b:do{if((f|0)==204){f=0;if(!(!n?(c[G>>2]|0)==0:0)){f=206}do{if((f|0)==206){f=0;if((c[F>>2]|0)!=0){if(n){break b}if((c[G>>2]|0)==0){break}else{break b}}if(!(!g?(c[G>>2]|0)==0:0)){$=C+ -128|0;ba=c[$>>2]|0;ca=(ba+2>>2)+(c[C>>2]|0)|0;ba=(ca>>5)+ba+(ca>>9)+(ca>>13)+(ca+1>>1)|0;c[$>>2]=ba;c[C>>2]=(ba+2>>2)+ca}if(!p?(c[G>>2]|0)==0:0){break b}ca=A+64|0;_=A+ -64|0;$=c[_>>2]|0;ba=($+2>>2)+(c[ca>>2]|0)|0;$=(ba>>5)+$+(ba>>9)+(ba>>13)+(ba+1>>1)|0;c[_>>2]=$;c[ca>>2]=($+2>>2)+ba;break b}}while(0);if((c[F>>2]|0)==0){Y=A+ -64|0;_=A+64|0;ba=C+ -128|0;W=c[ba>>2]|0;V=c[C>>2]|0;ca=V+(c[Y>>2]|0)|0;$=W+(c[_>>2]|0)|0;Z=(ca+2>>2)+$|0;X=(Z>>5)+ca+(Z>>9)+(Z>>13)+(Z+1>>1)|0;Z=(X+2>>2)+Z|0;ca=(X+1>>1)+(V-(ca+1>>1))|0;$=(Z+1>>1)+(W-($+1>>1))|0;c[Y>>2]=X-ca;c[_>>2]=Z-$;c[ba>>2]=$;c[C>>2]=ca}}}while(0);if(!(!e?(c[H>>2]|0)==0:0)){f=219}do{if((f|0)==219){f=0;if(!g?(c[G>>2]|0)==0:0){break}ca=C+ -256|0;c[ca>>2]=(c[ca>>2]|0)+(c[C+ -128>>2]|0)}}while(0);if(!(!o?(c[K>>2]|0)==0:0)){f=224}do{if((f|0)==224){f=0;if(!g?(c[G>>2]|0)==0:0){break}c[b+(M<<3)+34392>>2]=c[C>>2]}}while(0);if(!(!q?(c[F>>2]|0)==0:0)){f=229}do{if((f|0)==229){f=0;if(!g?(c[G>>2]|0)==0:0){break}ca=C+ -128|0;c[ca>>2]=(c[ca>>2]|0)+(c[b+(M<<3)+34392>>2]|0)}}while(0);if(!(!e?(c[H>>2]|0)==0:0)){f=234}do{if((f|0)==234){f=0;if(!p?(c[G>>2]|0)==0:0){break}ca=A+ -192|0;c[ca>>2]=(c[ca>>2]|0)+(c[A+ -64>>2]|0)}}while(0);if(!(!o?(c[K>>2]|0)==0:0)){f=239}do{if((f|0)==239){f=0;if(!p?(c[G>>2]|0)==0:0){break}c[b+(M<<3)+34396>>2]=c[A+64>>2]}}while(0);if(!q?(c[F>>2]|0)==0:0){break}if(!p?(c[G>>2]|0)==0:0){break}ca=A+ -64|0;c[ca>>2]=(c[ca>>2]|0)+(c[b+(M<<3)+34396>>2]|0)}}while(0);do{if(!J){c:do{if(!g){if(E){M=w}else{M=(c[H>>2]|0)!=0?-48:-16}if(q){while(1){if((M|0)>=16){break c}Jc(A+(M<<2)|0);M=M+32|0}}else{while(1){if((M|0)>=(((c[F>>2]|0)!=0?16:48)|0)){break c}Jc(A+(M<<2)|0);M=M+32|0}}}}while(0);d:do{if(!p){if(E){M=D}else{M=(c[H>>2]|0)!=0?-64:-32}if(q){while(1){if((M|0)>=0){break d}Jc(C+(M<<2)|0);M=M+32|0}}else{while(1){if((M|0)>=(((c[F>>2]|0)!=0?0:32)|0)){break d}Jc(C+(M<<2)|0);M=M+32|0}}}}while(0);if(!I){if(!(!g?(c[G>>2]|0)==0:0)){f=267}do{if((f|0)==267){f=0;if(!(!e?(c[H>>2]|0)==0:0)){X=C+ -256|0;Z=C+ -252|0;$=C+ -248|0;ca=C+ -244|0;_=c[$>>2]|0;ba=c[ca>>2]|0;W=ba+(c[X>>2]|0)|0;Y=_+(c[Z>>2]|0)|0;ba=ba-(W+1>>1)|0;_=_-(Y+1>>1)|0;W=ba+W|0;ba=(W>>1)-ba|0;W=(ba*3>>3)+W|0;ba=(W>>7)+ba-(W>>10)+(W*3>>4)|0;W=((ba*3|0)+4>>3)+W|0;ba=ba-(W>>1)|0;W=ba+W|0;Y=_+Y|0;_=(Y>>1)-_|0;Y=(_*3>>3)+Y|0;_=(Y>>7)+_-(Y>>10)+(Y*3>>4)|0;Y=((_*3|0)+4>>3)+Y|0;_=_-(Y>>1)|0;Y=_+Y|0;_=(1-ba>>1)+_|0;ba=(1-_>>1)-ba+(W+1>>1)|0;_=(Y+1>>1)-_|0;c[X>>2]=W-ba;c[Z>>2]=Y-_;c[$>>2]=_;c[ca>>2]=ba}if(!g?(c[G>>2]|0)==0:0){break}if(!q?(c[F>>2]|0)==0:0){break}X=C+ -108|0;Z=C+ -112|0;$=C+ -100|0;ca=C+ -104|0;_=c[$>>2]|0;ba=c[ca>>2]|0;W=ba+(c[X>>2]|0)|0;Y=_+(c[Z>>2]|0)|0;ba=ba-(W+1>>1)|0;_=_-(Y+1>>1)|0;W=ba+W|0;ba=(W>>1)-ba|0;W=(ba*3>>3)+W|0;ba=(W>>7)+ba-(W>>10)+(W*3>>4)|0;W=((ba*3|0)+4>>3)+W|0;ba=ba-(W>>1)|0;W=ba+W|0;Y=_+Y|0;_=(Y>>1)-_|0;Y=(_*3>>3)+Y|0;_=(Y>>7)+_-(Y>>10)+(Y*3>>4)|0;Y=((_*3|0)+4>>3)+Y|0;_=_-(Y>>1)|0;Y=_+Y|0;_=(1-ba>>1)+_|0;ba=(1-_>>1)-ba+(W+1>>1)|0;_=(Y+1>>1)-_|0;c[X>>2]=W-ba;c[Z>>2]=Y-_;c[$>>2]=_;c[ca>>2]=ba}}while(0);if(!(!p?(c[G>>2]|0)==0:0)){f=277}do{if((f|0)==277){f=0;if(!(!e?(c[H>>2]|0)==0:0)){X=A+ -152|0;Z=A+ -148|0;$=A+ -160|0;ca=A+ -156|0;_=c[$>>2]|0;ba=c[ca>>2]|0;W=ba+(c[X>>2]|0)|0;Y=_+(c[Z>>2]|0)|0;ba=ba-(W+1>>1)|0;_=_-(Y+1>>1)|0;W=ba+W|0;ba=(W>>1)-ba|0;W=(ba*3>>3)+W|0;ba=(W>>7)+ba-(W>>10)+(W*3>>4)|0;W=((ba*3|0)+4>>3)+W|0;ba=ba-(W>>1)|0;W=ba+W|0;Y=_+Y|0;_=(Y>>1)-_|0;Y=(_*3>>3)+Y|0;_=(Y>>7)+_-(Y>>10)+(Y*3>>4)|0;Y=((_*3|0)+4>>3)+Y|0;_=_-(Y>>1)|0;Y=_+Y|0;_=(1-ba>>1)+_|0;ba=(1-_>>1)-ba+(W+1>>1)|0;_=(Y+1>>1)-_|0;c[X>>2]=W-ba;c[Z>>2]=Y-_;c[$>>2]=_;c[ca>>2]=ba}if(!p?(c[G>>2]|0)==0:0){break}if(!q?(c[F>>2]|0)==0:0){break}X=A+ -4|0;Z=A+ -8|0;$=A+ -12|0;ca=A+ -16|0;_=c[$>>2]|0;ba=c[ca>>2]|0;W=ba+(c[X>>2]|0)|0;Y=_+(c[Z>>2]|0)|0;ba=ba-(W+1>>1)|0;_=_-(Y+1>>1)|0;W=ba+W|0;ba=(W>>1)-ba|0;W=(ba*3>>3)+W|0;ba=(W>>7)+ba-(W>>10)+(W*3>>4)|0;W=((ba*3|0)+4>>3)+W|0;ba=ba-(W>>1)|0;W=ba+W|0;Y=_+Y|0;_=(Y>>1)-_|0;Y=(_*3>>3)+Y|0;_=(Y>>7)+_-(Y>>10)+(Y*3>>4)|0;Y=((_*3|0)+4>>3)+Y|0;_=_-(Y>>1)|0;Y=_+Y|0;_=(1-ba>>1)+_|0;ba=(1-_>>1)-ba+(W+1>>1)|0;_=(Y+1>>1)-_|0;c[X>>2]=W-ba;c[Z>>2]=Y-_;c[$>>2]=_;c[ca>>2]=ba}}while(0);if(!v){if(!(!e?(c[H>>2]|0)==0:0)){if(!p?(c[G>>2]|0)==0:0){W=A+ -152|0;_=A+ -160|0;$=C+ -256|0;X=C+ -248|0;ca=c[$>>2]|0;Z=c[X>>2]|0;Y=Z+(c[W>>2]|0)|0;ba=ca+(c[_>>2]|0)|0;Z=Z-(Y+1>>1)|0;ca=ca-(ba+1>>1)|0;Y=Z+Y|0;Z=(Y>>1)-Z|0;Y=(Z*3>>3)+Y|0;Z=(Y>>7)+Z-(Y>>10)+(Y*3>>4)|0;Y=((Z*3|0)+4>>3)+Y|0;Z=Z-(Y>>1)|0;Y=Z+Y|0;ba=ca+ba|0;ca=(ba>>1)-ca|0;ba=(ca*3>>3)+ba|0;ca=(ba>>7)+ca-(ba>>10)+(ba*3>>4)|0;ba=((ca*3|0)+4>>3)+ba|0;ca=ca-(ba>>1)|0;ba=ca+ba|0;ca=(1-Z>>1)+ca|0;Z=(1-ca>>1)-Z+(Y+1>>1)|0;ca=(ba+1>>1)-ca|0;c[W>>2]=Y-Z;c[_>>2]=ba-ca;c[$>>2]=ca;c[X>>2]=Z;X=A+ -148|0;Z=A+ -156|0;$=C+ -252|0;ca=C+ -244|0;_=c[$>>2]|0;ba=c[ca>>2]|0;W=ba+(c[X>>2]|0)|0;Y=_+(c[Z>>2]|0)|0;ba=ba-(W+1>>1)|0;_=_-(Y+1>>1)|0;W=ba+W|0;ba=(W>>1)-ba|0;W=(ba*3>>3)+W|0;ba=(W>>7)+ba-(W>>10)+(W*3>>4)|0;W=((ba*3|0)+4>>3)+W|0;ba=ba-(W>>1)|0;W=ba+W|0;Y=_+Y|0;_=(Y>>1)-_|0;Y=(_*3>>3)+Y|0;_=(Y>>7)+_-(Y>>10)+(Y*3>>4)|0;Y=((_*3|0)+4>>3)+Y|0;_=_-(Y>>1)|0;Y=_+Y|0;_=(1-ba>>1)+_|0;ba=(1-_>>1)-ba+(W+1>>1)|0;_=(Y+1>>1)-_|0;c[X>>2]=W-ba;c[Z>>2]=Y-_;c[$>>2]=_;c[ca>>2]=ba}W=A+ -216|0;_=A+ -224|0;$=A+ -192|0;X=A+ -184|0;ca=c[$>>2]|0;Z=c[X>>2]|0;Y=Z+(c[W>>2]|0)|0;ba=ca+(c[_>>2]|0)|0;Z=Z-(Y+1>>1)|0;ca=ca-(ba+1>>1)|0;Y=Z+Y|0;Z=(Y>>1)-Z|0;Y=(Z*3>>3)+Y|0;Z=(Y>>7)+Z-(Y>>10)+(Y*3>>4)|0;Y=((Z*3|0)+4>>3)+Y|0;Z=Z-(Y>>1)|0;Y=Z+Y|0;ba=ca+ba|0;ca=(ba>>1)-ca|0;ba=(ca*3>>3)+ba|0;ca=(ba>>7)+ca-(ba>>10)+(ba*3>>4)|0;ba=((ca*3|0)+4>>3)+ba|0;ca=ca-(ba>>1)|0;ba=ca+ba|0;ca=(1-Z>>1)+ca|0;Z=(1-ca>>1)-Z+(Y+1>>1)|0;ca=(ba+1>>1)-ca|0;c[W>>2]=Y-Z;c[_>>2]=ba-ca;c[$>>2]=ca;c[X>>2]=Z;X=A+ -212|0;Z=A+ -220|0;$=A+ -188|0;ca=A+ -180|0;_=c[$>>2]|0;ba=c[ca>>2]|0;W=ba+(c[X>>2]|0)|0;Y=_+(c[Z>>2]|0)|0;ba=ba-(W+1>>1)|0;_=_-(Y+1>>1)|0;W=ba+W|0;ba=(W>>1)-ba|0;W=(ba*3>>3)+W|0;ba=(W>>7)+ba-(W>>10)+(W*3>>4)|0;W=((ba*3|0)+4>>3)+W|0;ba=ba-(W>>1)|0;W=ba+W|0;Y=_+Y|0;_=(Y>>1)-_|0;Y=(_*3>>3)+Y|0;_=(Y>>7)+_-(Y>>10)+(Y*3>>4)|0;Y=((_*3|0)+4>>3)+Y|0;_=_-(Y>>1)|0;Y=_+Y|0;_=(1-ba>>1)+_|0;ba=(1-_>>1)-ba+(W+1>>1)|0;_=(Y+1>>1)-_|0;c[X>>2]=W-ba;c[Z>>2]=Y-_;c[$>>2]=_;c[ca>>2]=ba}if(!p?(c[G>>2]|0)==0:0){Mc(A+ -192|0,C+ -256|0,32);if(!q){if((c[F>>2]|0)==0){Mc(A+ -64|0,C+ -128|0,32);f=300}else{f=301}}else{f=302}}else{W=A+ -132|0;_=A+ -136|0;$=A+ -24|0;X=A+ -20|0;ca=c[$>>2]|0;Z=c[X>>2]|0;Y=Z+(c[W>>2]|0)|0;ba=ca+(c[_>>2]|0)|0;Z=Z-(Y+1>>1)|0;ca=ca-(ba+1>>1)|0;Y=Z+Y|0;Z=(Y>>1)-Z|0;Y=(Z*3>>3)+Y|0;Z=(Y>>7)+Z-(Y>>10)+(Y*3>>4)|0;Y=((Z*3|0)+4>>3)+Y|0;Z=Z-(Y>>1)|0;Y=Z+Y|0;ba=ca+ba|0;ca=(ba>>1)-ca|0;ba=(ca*3>>3)+ba|0;ca=(ba>>7)+ca-(ba>>10)+(ba*3>>4)|0;ba=((ca*3|0)+4>>3)+ba|0;ca=ca-(ba>>1)|0;ba=ca+ba|0;ca=(1-Z>>1)+ca|0;Z=(1-ca>>1)-Z+(Y+1>>1)|0;ca=(ba+1>>1)-ca|0;c[W>>2]=Y-Z;c[_>>2]=ba-ca;c[$>>2]=ca;c[X>>2]=Z;X=A+ -140|0;Z=A+ -144|0;$=A+ -32|0;ca=A+ -28|0;_=c[$>>2]|0;ba=c[ca>>2]|0;W=ba+(c[X>>2]|0)|0;Y=_+(c[Z>>2]|0)|0;ba=ba-(W+1>>1)|0;_=_-(Y+1>>1)|0;W=ba+W|0;ba=(W>>1)-ba|0;W=(ba*3>>3)+W|0;ba=(W>>7)+ba-(W>>10)+(W*3>>4)|0;W=((ba*3|0)+4>>3)+W|0;ba=ba-(W>>1)|0;W=ba+W|0;Y=_+Y|0;_=(Y>>1)-_|0;Y=(_*3>>3)+Y|0;_=(Y>>7)+_-(Y>>10)+(Y*3>>4)|0;Y=((_*3|0)+4>>3)+Y|0;_=_-(Y>>1)|0;Y=_+Y|0;_=(1-ba>>1)+_|0;ba=(1-_>>1)-ba+(W+1>>1)|0;_=(Y+1>>1)-_|0;c[X>>2]=W-ba;c[Z>>2]=Y-_;c[$>>2]=_;c[ca>>2]=ba;if(!q?(c[F>>2]|0)==0:0){X=A+ -4|0;$=A+ -8|0;ba=A+104|0;Y=A+108|0;f=c[ba>>2]|0;_=c[Y>>2]|0;Z=_+(c[X>>2]|0)|0;ca=f+(c[$>>2]|0)|0;_=_-(Z+1>>1)|0;f=f-(ca+1>>1)|0;Z=_+Z|0;_=(Z>>1)-_|0;Z=(_*3>>3)+Z|0;_=(Z>>7)+_-(Z>>10)+(Z*3>>4)|0;Z=((_*3|0)+4>>3)+Z|0;_=_-(Z>>1)|0;Z=_+Z|0;ca=f+ca|0;f=(ca>>1)-f|0;ca=(f*3>>3)+ca|0;f=(ca>>7)+f-(ca>>10)+(ca*3>>4)|0;ca=((f*3|0)+4>>3)+ca|0;f=f-(ca>>1)|0;ca=f+ca|0;f=(1-_>>1)+f|0;_=(1-f>>1)-_+(Z+1>>1)|0;f=(ca+1>>1)-f|0;c[X>>2]=Z-_;c[$>>2]=ca-f;c[ba>>2]=f;c[Y>>2]=_;Y=A+ -12|0;_=A+ -16|0;ba=A+96|0;f=A+100|0;$=c[ba>>2]|0;ca=c[f>>2]|0;X=ca+(c[Y>>2]|0)|0;Z=$+(c[_>>2]|0)|0;ca=ca-(X+1>>1)|0;$=$-(Z+1>>1)|0;X=ca+X|0;ca=(X>>1)-ca|0;X=(ca*3>>3)+X|0;ca=(X>>7)+ca-(X>>10)+(X*3>>4)|0;X=((ca*3|0)+4>>3)+X|0;ca=ca-(X>>1)|0;X=ca+X|0;Z=$+Z|0;$=(Z>>1)-$|0;Z=($*3>>3)+Z|0;$=(Z>>7)+$-(Z>>10)+(Z*3>>4)|0;Z=(($*3|0)+4>>3)+Z|0;$=$-(Z>>1)|0;Z=$+Z|0;$=(1-ca>>1)+$|0;ca=(1-$>>1)-ca+(X+1>>1)|0;$=(Z+1>>1)-$|0;c[Y>>2]=X-ca;c[_>>2]=Z-$;c[ba>>2]=$;c[f>>2]=ca;f=300}else{f=301}}if((f|0)==300){f=0;if((c[F>>2]|0)==0){Mc(A+ -128|0,A+ -64|0,32)}else{f=301}}if((f|0)==301){if(p){f=304}else{f=302}}if((f|0)==302){if((c[G>>2]|0)==0){X=A+ -8|0;$=A+ -16|0;ba=C+ -112|0;Y=C+ -104|0;f=c[ba>>2]|0;_=c[Y>>2]|0;Z=_+(c[X>>2]|0)|0;ca=f+(c[$>>2]|0)|0;_=_-(Z+1>>1)|0;f=f-(ca+1>>1)|0;Z=_+Z|0;_=(Z>>1)-_|0;Z=(_*3>>3)+Z|0;_=(Z>>7)+_-(Z>>10)+(Z*3>>4)|0;Z=((_*3|0)+4>>3)+Z|0;_=_-(Z>>1)|0;Z=_+Z|0;ca=f+ca|0;f=(ca>>1)-f|0;ca=(f*3>>3)+ca|0;f=(ca>>7)+f-(ca>>10)+(ca*3>>4)|0;ca=((f*3|0)+4>>3)+ca|0;f=f-(ca>>1)|0;ca=f+ca|0;f=(1-_>>1)+f|0;_=(1-f>>1)-_+(Z+1>>1)|0;f=(ca+1>>1)-f|0;c[X>>2]=Z-_;c[$>>2]=ca-f;c[ba>>2]=f;c[Y>>2]=_;Y=A+ -4|0;_=A+ -12|0;ba=C+ -108|0;f=C+ -100|0;$=c[ba>>2]|0;ca=c[f>>2]|0;X=ca+(c[Y>>2]|0)|0;Z=$+(c[_>>2]|0)|0;ca=ca-(X+1>>1)|0;$=$-(Z+1>>1)|0;X=ca+X|0;ca=(X>>1)-ca|0;X=(ca*3>>3)+X|0;ca=(X>>7)+ca-(X>>10)+(X*3>>4)|0;X=((ca*3|0)+4>>3)+X|0;ca=ca-(X>>1)|0;X=ca+X|0;Z=$+Z|0;$=(Z>>1)-$|0;Z=($*3>>3)+Z|0;$=(Z>>7)+$-(Z>>10)+(Z*3>>4)|0;Z=(($*3|0)+4>>3)+Z|0;$=$-(Z>>1)|0;Z=$+Z|0;$=(1-ca>>1)+$|0;ca=(1-$>>1)-ca+(X+1>>1)|0;$=(Z+1>>1)-$|0;c[Y>>2]=X-ca;c[_>>2]=Z-$;c[ba>>2]=$;c[f>>2]=ca;f=304}else{f=304}}if((f|0)==304){f=0;W=A+ -72|0;_=A+ -80|0;$=A+ -48|0;X=A+ -40|0;ca=c[$>>2]|0;Z=c[X>>2]|0;Y=Z+(c[W>>2]|0)|0;ba=ca+(c[_>>2]|0)|0;Z=Z-(Y+1>>1)|0;ca=ca-(ba+1>>1)|0;Y=Z+Y|0;Z=(Y>>1)-Z|0;Y=(Z*3>>3)+Y|0;Z=(Y>>7)+Z-(Y>>10)+(Y*3>>4)|0;Y=((Z*3|0)+4>>3)+Y|0;Z=Z-(Y>>1)|0;Y=Z+Y|0;ba=ca+ba|0;ca=(ba>>1)-ca|0;ba=(ca*3>>3)+ba|0;ca=(ba>>7)+ca-(ba>>10)+(ba*3>>4)|0;ba=((ca*3|0)+4>>3)+ba|0;ca=ca-(ba>>1)|0;ba=ca+ba|0;ca=(1-Z>>1)+ca|0;Z=(1-ca>>1)-Z+(Y+1>>1)|0;ca=(ba+1>>1)-ca|0;c[W>>2]=Y-Z;c[_>>2]=ba-ca;c[$>>2]=ca;c[X>>2]=Z;X=A+ -68|0;Z=A+ -76|0;$=A+ -44|0;ca=A+ -36|0;_=c[$>>2]|0;ba=c[ca>>2]|0;W=ba+(c[X>>2]|0)|0;Y=_+(c[Z>>2]|0)|0;ba=ba-(W+1>>1)|0;_=_-(Y+1>>1)|0;W=ba+W|0;ba=(W>>1)-ba|0;W=(ba*3>>3)+W|0;ba=(W>>7)+ba-(W>>10)+(W*3>>4)|0;W=((ba*3|0)+4>>3)+W|0;ba=ba-(W>>1)|0;W=ba+W|0;Y=_+Y|0;_=(Y>>1)-_|0;Y=(_*3>>3)+Y|0;_=(Y>>7)+_-(Y>>10)+(Y*3>>4)|0;Y=((_*3|0)+4>>3)+Y|0;_=_-(Y>>1)|0;Y=_+Y|0;_=(1-ba>>1)+_|0;ba=(1-_>>1)-ba+(W+1>>1)|0;_=(Y+1>>1)-_|0;c[X>>2]=W-ba;c[Z>>2]=Y-_;c[$>>2]=_;c[ca>>2]=ba}Mc(A+ -256|0,A+ -192|0,32)}if(g){if(m){break}}else{if((c[G>>2]|0)==0|m){break}}_=C+ -240|0;W=C+ -236|0;$=C+ -128|0;X=C+ -124|0;ca=c[$>>2]|0;Z=c[X>>2]|0;Y=Z+(c[W>>2]|0)|0;ba=ca+(c[_>>2]|0)|0;Z=Z-(Y+1>>1)|0;ca=ca-(ba+1>>1)|0;Y=Z+Y|0;Z=(Y>>1)-Z|0;Y=(Z*3>>3)+Y|0;Z=(Y>>7)+Z-(Y>>10)+(Y*3>>4)|0;Y=((Z*3|0)+4>>3)+Y|0;Z=Z-(Y>>1)|0;Y=Z+Y|0;ba=ca+ba|0;ca=(ba>>1)-ca|0;ba=(ca*3>>3)+ba|0;ca=(ba>>7)+ca-(ba>>10)+(ba*3>>4)|0;ba=((ca*3|0)+4>>3)+ba|0;ca=ca-(ba>>1)|0;ba=ca+ba|0;ca=(1-Z>>1)+ca|0;Z=(1-ca>>1)-Z+(Y+1>>1)|0;ca=(ba+1>>1)-ca|0;c[W>>2]=Y-Z;c[_>>2]=ba-ca;c[$>>2]=ca;c[X>>2]=Z;X=C+ -228|0;Z=C+ -232|0;$=C+ -120|0;ca=C+ -116|0;_=c[$>>2]|0;ba=c[ca>>2]|0;W=ba+(c[X>>2]|0)|0;Y=_+(c[Z>>2]|0)|0;ba=ba-(W+1>>1)|0;_=_-(Y+1>>1)|0;W=ba+W|0;ba=(W>>1)-ba|0;W=(ba*3>>3)+W|0;ba=(W>>7)+ba-(W>>10)+(W*3>>4)|0;W=((ba*3|0)+4>>3)+W|0;ba=ba-(W>>1)|0;W=ba+W|0;Y=_+Y|0;_=(Y>>1)-_|0;Y=(_*3>>3)+Y|0;_=(Y>>7)+_-(Y>>10)+(Y*3>>4)|0;Y=((_*3|0)+4>>3)+Y|0;_=_-(Y>>1)|0;Y=_+Y|0;_=(1-ba>>1)+_|0;ba=(1-_>>1)-ba+(W+1>>1)|0;_=(Y+1>>1)-_|0;c[X>>2]=W-ba;c[Z>>2]=Y-_;c[$>>2]=_;c[ca>>2]=ba;if(!h?(c[F>>2]|0)==0:0){ca=C+ -112|0;ba=C+ -108|0;X=C+4|0;$=c[C>>2]|0;Z=c[X>>2]|0;W=Z+(c[ba>>2]|0)|0;_=$+(c[ca>>2]|0)|0;Z=Z-(W+1>>1)|0;$=$-(_+1>>1)|0;W=Z+W|0;Z=(W>>1)-Z|0;W=(Z*3>>3)+W|0;Z=(W>>7)+Z-(W>>10)+(W*3>>4)|0;W=((Z*3|0)+4>>3)+W|0;Z=Z-(W>>1)|0;W=Z+W|0;_=$+_|0;$=(_>>1)-$|0;_=($*3>>3)+_|0;$=(_>>7)+$-(_>>10)+(_*3>>4)|0;_=(($*3|0)+4>>3)+_|0;$=$-(_>>1)|0;_=$+_|0;$=(1-Z>>1)+$|0;Z=(1-$>>1)-Z+(W+1>>1)|0;$=(_+1>>1)-$|0;c[ba>>2]=W-Z;c[ca>>2]=_-$;c[C>>2]=$;c[X>>2]=Z;X=C+ -100|0;Z=C+ -104|0;$=C+8|0;ca=C+12|0;_=c[$>>2]|0;ba=c[ca>>2]|0;W=ba+(c[X>>2]|0)|0;Y=_+(c[Z>>2]|0)|0;ba=ba-(W+1>>1)|0;_=_-(Y+1>>1)|0;W=ba+W|0;ba=(W>>1)-ba|0;W=(ba*3>>3)+W|0;ba=(W>>7)+ba-(W>>10)+(W*3>>4)|0;W=((ba*3|0)+4>>3)+W|0;ba=ba-(W>>1)|0;W=ba+W|0;Y=_+Y|0;_=(Y>>1)-_|0;Y=(_*3>>3)+Y|0;_=(Y>>7)+_-(Y>>10)+(Y*3>>4)|0;Y=((_*3|0)+4>>3)+Y|0;_=_-(Y>>1)|0;Y=_+Y|0;_=(1-ba>>1)+_|0;ba=(1-_>>1)-ba+(W+1>>1)|0;_=(Y+1>>1)-_|0;c[X>>2]=W-ba;c[Z>>2]=Y-_;c[$>>2]=_;c[ca>>2]=ba}}}}while(0)}while(z>>>0<x>>>0)}v=u?2:0;if(!(u&B)){i=l;return 0}z=(s|0)==2;y=b+34364|0;w=b+34360|0;x=b+34368|0;u=b+34372|0;t=t>>>0>3;s=(s|0)==0;A=m?0:-128;B=m?64:-128;r=r>>>0<2;F=m?112:-80;C=b+34140|0;D=0;do{H=D;D=D+1|0;E=c[b+(D<<2)+34652>>2]|0;G=c[b+(D<<2)+34716>>2]|0;do{if(!k?(c[(c[j>>2]|0)+12>>2]|0)>>>0<16:0){J=G+128|0;N=c[J>>2]|0;M=(c[G>>2]|0)-(N+1>>1)|0;c[G>>2]=M;N=M+N|0;c[J>>2]=N;L=G+256|0;I=G+64|0;K=G+320|0;if((c[C>>2]|0)==0){de(G,L,I,K);de(J,G+384|0,G+192|0,G+448|0);break}else{Z=c[I>>2]|0;$=c[K>>2]|0;ba=$+M|0;ca=(c[L>>2]|0)-Z|0;_=ba-ca>>1;$=_-$|0;Z=_-Z|0;c[G>>2]=ba-Z<<1;c[L>>2]=$+ca<<1;c[I>>2]=$<<1;c[K>>2]=Z<<1;Z=G+384|0;$=G+192|0;ca=G+448|0;ba=c[$>>2]|0;_=c[ca>>2]|0;X=_+N|0;Y=(c[Z>>2]|0)-ba|0;W=X-Y>>1;_=W-_|0;ba=W-ba|0;c[J>>2]=X-ba<<1;c[Z>>2]=_+Y<<1;c[$>>2]=_<<1;c[ca>>2]=ba<<1;break}}}while(0);do{if(z){if(!(!e?(c[u>>2]|0)==0:0)){f=322}do{if((f|0)==322){f=0;if(!g?(c[y>>2]|0)==0:0){break}ca=G+ -512|0;c[ca>>2]=(c[ca>>2]|0)-(c[G+ -256>>2]|0)}}while(0);if(!(!o?(c[x>>2]|0)==0:0)){f=327}do{if((f|0)==327){f=0;if(!g?(c[y>>2]|0)==0:0){break}c[b+(H<<3)+34376>>2]=c[G>>2]}}while(0);if(!(!q?(c[w>>2]|0)==0:0)){f=332}do{if((f|0)==332){f=0;if(!g?(c[y>>2]|0)==0:0){break}ca=G+ -256|0;c[ca>>2]=(c[ca>>2]|0)-(c[b+(H<<3)+34376>>2]|0)}}while(0);if(!(!e?(c[u>>2]|0)==0:0)){f=337}do{if((f|0)==337){f=0;if(!p?(c[y>>2]|0)==0:0){break}ca=E+ -320|0;c[ca>>2]=(c[ca>>2]|0)-(c[E+ -64>>2]|0)}}while(0);if(!(!o?(c[x>>2]|0)==0:0)){f=342}do{if((f|0)==342){if(!p?(c[y>>2]|0)==0:0){break}c[b+(H<<3)+34380>>2]=c[E+192>>2]}}while(0);if(!q?(c[w>>2]|0)==0:0){f=350}else{f=347}do{if((f|0)==347){if(!p?(c[y>>2]|0)==0:0){f=351;break}f=E+ -64|0;c[f>>2]=(c[f>>2]|0)-(c[b+(H<<3)+34380>>2]|0);f=350}}while(0);if((f|0)==350){if(p){f=376}else{f=351}}if((f|0)==351){f=0;if(!h?(c[w>>2]|0)==0:0){f=369}else{do{if(!g?(c[y>>2]|0)==0:0){if(!(!m?(c[w>>2]|0)==0:0)){$=E+192|0;ba=c[$>>2]|0;ca=(ba+2>>2)+(c[G>>2]|0)|0;ba=(ca>>5)+ba+(ca>>9)+(ca>>13)+(ca+1>>1)|0;c[$>>2]=ba;c[G>>2]=(ba+2>>2)+ca}if(!q?(c[w>>2]|0)==0:0){break}_=E+ -64|0;ca=G+ -256|0;$=c[_>>2]|0;ba=($+2>>2)+(c[ca>>2]|0)|0;$=(ba>>5)+$+(ba>>9)+(ba>>13)+(ba+1>>1)|0;c[_>>2]=$;c[ca>>2]=($+2>>2)+ba}}while(0);if(!(!m?(c[w>>2]|0)==0:0)){_=G+64|0;ca=G+128|0;$=c[_>>2]|0;ba=($+2>>2)+(c[ca>>2]|0)|0;$=(ba>>5)+$+(ba>>9)+(ba>>13)+(ba+1>>1)|0;c[_>>2]=$;c[ca>>2]=($+2>>2)+ba}if(!(!q?(c[w>>2]|0)==0:0)){_=G+ -192|0;ca=G+ -128|0;$=c[_>>2]|0;ba=($+2>>2)+(c[ca>>2]|0)|0;$=(ba>>5)+$+(ba>>9)+(ba>>13)+(ba+1>>1)|0;c[_>>2]=$;c[ca>>2]=($+2>>2)+ba}if(!h?(c[w>>2]|0)==0:0){f=369}}if((f|0)==369){f=0;if(!g?(c[y>>2]|0)==0:0){Z=E+ -64|0;$=E+192|0;ca=G+ -256|0;X=c[ca>>2]|0;W=c[G>>2]|0;I=W+(c[Z>>2]|0)|0;ba=X+(c[$>>2]|0)|0;_=(I+2>>2)+ba|0;Y=(_>>5)+I+(_>>9)+(_>>13)+(_+1>>1)|0;_=(Y+2>>2)+_|0;I=(Y+1>>1)+(W-(I+1>>1))|0;ba=(_+1>>1)+(X-(ba+1>>1))|0;c[Z>>2]=Y-I;c[$>>2]=_-ba;c[ca>>2]=ba}else{ba=G+ -256|0;ca=c[ba>>2]|0;I=(ca+2>>2)+(c[G>>2]|0)|0;ca=(I>>5)+ca+(I>>9)+(I>>13)+(I+1>>1)|0;c[ba>>2]=ca;I=(ca+2>>2)+I|0}c[G>>2]=I;X=G+ -192|0;Z=G+64|0;$=G+ -128|0;ca=G+128|0;V=c[$>>2]|0;U=c[ca>>2]|0;ba=U+(c[X>>2]|0)|0;_=V+(c[Z>>2]|0)|0;Y=(ba+2>>2)+_|0;W=(Y>>5)+ba+(Y>>9)+(Y>>13)+(Y+1>>1)|0;Y=(W+2>>2)+Y|0;ba=(W+1>>1)+(U-(ba+1>>1))|0;_=(Y+1>>1)+(V-(_+1>>1))|0;c[X>>2]=W-ba;c[Z>>2]=Y-_;c[$>>2]=_;c[ca>>2]=ba}if(!p){if(!((c[y>>2]|0)==0|h)){f=377}}else{f=376}}if((f|0)==376?(f=0,!h):0){f=377}if((f|0)==377?(f=0,(c[w>>2]|0)==0):0){_=E+ -64|0;ca=E+192|0;$=c[_>>2]|0;ba=($+2>>2)+(c[ca>>2]|0)|0;$=(ba>>5)+$+(ba>>9)+(ba>>13)+(ba+1>>1)|0;c[_>>2]=$;c[ca>>2]=($+2>>2)+ba}if(!(!e?(c[u>>2]|0)==0:0)){f=381}do{if((f|0)==381){f=0;if(!g?(c[y>>2]|0)==0:0){break}ca=G+ -512|0;c[ca>>2]=(c[ca>>2]|0)+(c[G+ -256>>2]|0)}}while(0);if(!(!o?(c[x>>2]|0)==0:0)){f=386}do{if((f|0)==386){f=0;if(!g?(c[y>>2]|0)==0:0){break}c[b+(H<<3)+34392>>2]=c[G>>2]}}while(0);if(!(!q?(c[w>>2]|0)==0:0)){f=391}do{if((f|0)==391){f=0;if(!g?(c[y>>2]|0)==0:0){break}ca=G+ -256|0;c[ca>>2]=(c[ca>>2]|0)+(c[b+(H<<3)+34392>>2]|0)}}while(0);if(!(!e?(c[u>>2]|0)==0:0)){f=396}do{if((f|0)==396){f=0;if(!p?(c[y>>2]|0)==0:0){break}ca=E+ -320|0;c[ca>>2]=(c[ca>>2]|0)+(c[E+ -64>>2]|0)}}while(0);if(!(!o?(c[x>>2]|0)==0:0)){f=401}do{if((f|0)==401){f=0;if(!p?(c[y>>2]|0)==0:0){break}c[b+(H<<3)+34396>>2]=c[E+192>>2]}}while(0);if(!q?(c[w>>2]|0)==0:0){break}if(!p?(c[y>>2]|0)==0:0){break}ca=E+ -64|0;c[ca>>2]=(c[ca>>2]|0)+(c[b+(H<<3)+34396>>2]|0)}}while(0);e:do{if(!t){f:do{if(!g){if(r){H=F}else{H=(c[u>>2]|0)!=0?-80:-16}if(q){while(1){if((H|0)>=48){break f}Jc(E+(H<<2)|0);H=H+64|0}}else{while(1){if((H|0)>=(((c[w>>2]|0)!=0?48:112)|0)){break f}Jc(E+(H<<2)|0);H=H+64|0}}}}while(0);g:do{if(!p){if(r){H=B}else{H=(c[u>>2]|0)!=0?-128:-64}if(q){while(1){if((H|0)>=0){break g}Jc(G+(H<<2)|0);Jc(G+((H|16)<<2)|0);Jc(G+((H|32)<<2)|0);H=H+64|0}}else{while(1){if((H|0)>=(((c[w>>2]|0)!=0?0:64)|0)){break g}Jc(G+(H<<2)|0);Jc(G+((H|16)<<2)|0);Jc(G+((H|32)<<2)|0);H=H+64|0}}}}while(0);if(!s){if(!(!g?(c[y>>2]|0)==0:0)){f=429}do{if((f|0)==429){f=0;if(!(!e?(c[u>>2]|0)==0:0)){X=G+ -512|0;Z=G+ -508|0;$=G+ -504|0;ca=G+ -500|0;_=c[$>>2]|0;ba=c[ca>>2]|0;W=ba+(c[X>>2]|0)|0;Y=_+(c[Z>>2]|0)|0;ba=ba-(W+1>>1)|0;_=_-(Y+1>>1)|0;W=ba+W|0;ba=(W>>1)-ba|0;W=(ba*3>>3)+W|0;ba=(W>>7)+ba-(W>>10)+(W*3>>4)|0;W=((ba*3|0)+4>>3)+W|0;ba=ba-(W>>1)|0;W=ba+W|0;Y=_+Y|0;_=(Y>>1)-_|0;Y=(_*3>>3)+Y|0;_=(Y>>7)+_-(Y>>10)+(Y*3>>4)|0;Y=((_*3|0)+4>>3)+Y|0;_=_-(Y>>1)|0;Y=_+Y|0;_=(1-ba>>1)+_|0;ba=(1-_>>1)-ba+(W+1>>1)|0;_=(Y+1>>1)-_|0;c[X>>2]=W-ba;c[Z>>2]=Y-_;c[$>>2]=_;c[ca>>2]=ba}if(!g?(c[y>>2]|0)==0:0){break}if(!q?(c[w>>2]|0)==0:0){break}X=G+ -236|0;Z=G+ -240|0;$=G+ -228|0;ca=G+ -232|0;_=c[$>>2]|0;ba=c[ca>>2]|0;W=ba+(c[X>>2]|0)|0;Y=_+(c[Z>>2]|0)|0;ba=ba-(W+1>>1)|0;_=_-(Y+1>>1)|0;W=ba+W|0;ba=(W>>1)-ba|0;W=(ba*3>>3)+W|0;ba=(W>>7)+ba-(W>>10)+(W*3>>4)|0;W=((ba*3|0)+4>>3)+W|0;ba=ba-(W>>1)|0;W=ba+W|0;Y=_+Y|0;_=(Y>>1)-_|0;Y=(_*3>>3)+Y|0;_=(Y>>7)+_-(Y>>10)+(Y*3>>4)|0;Y=((_*3|0)+4>>3)+Y|0;_=_-(Y>>1)|0;Y=_+Y|0;_=(1-ba>>1)+_|0;ba=(1-_>>1)-ba+(W+1>>1)|0;_=(Y+1>>1)-_|0;c[X>>2]=W-ba;c[Z>>2]=Y-_;c[$>>2]=_;c[ca>>2]=ba}}while(0);if(!(!p?(c[y>>2]|0)==0:0)){f=439}do{if((f|0)==439){f=0;if(!(!e?(c[u>>2]|0)==0:0)){X=E+ -280|0;Z=E+ -276|0;$=E+ -288|0;ca=E+ -284|0;_=c[$>>2]|0;ba=c[ca>>2]|0;W=ba+(c[X>>2]|0)|0;Y=_+(c[Z>>2]|0)|0;ba=ba-(W+1>>1)|0;_=_-(Y+1>>1)|0;W=ba+W|0;ba=(W>>1)-ba|0;W=(ba*3>>3)+W|0;ba=(W>>7)+ba-(W>>10)+(W*3>>4)|0;W=((ba*3|0)+4>>3)+W|0;ba=ba-(W>>1)|0;W=ba+W|0;Y=_+Y|0;_=(Y>>1)-_|0;Y=(_*3>>3)+Y|0;_=(Y>>7)+_-(Y>>10)+(Y*3>>4)|0;Y=((_*3|0)+4>>3)+Y|0;_=_-(Y>>1)|0;Y=_+Y|0;_=(1-ba>>1)+_|0;ba=(1-_>>1)-ba+(W+1>>1)|0;_=(Y+1>>1)-_|0;c[X>>2]=W-ba;c[Z>>2]=Y-_;c[$>>2]=_;c[ca>>2]=ba}if(!p?(c[y>>2]|0)==0:0){break}if(!q?(c[w>>2]|0)==0:0){break}X=E+ -4|0;Z=E+ -8|0;$=E+ -12|0;ca=E+ -16|0;_=c[$>>2]|0;ba=c[ca>>2]|0;W=ba+(c[X>>2]|0)|0;Y=_+(c[Z>>2]|0)|0;ba=ba-(W+1>>1)|0;_=_-(Y+1>>1)|0;W=ba+W|0;ba=(W>>1)-ba|0;W=(ba*3>>3)+W|0;ba=(W>>7)+ba-(W>>10)+(W*3>>4)|0;W=((ba*3|0)+4>>3)+W|0;ba=ba-(W>>1)|0;W=ba+W|0;Y=_+Y|0;_=(Y>>1)-_|0;Y=(_*3>>3)+Y|0;_=(Y>>7)+_-(Y>>10)+(Y*3>>4)|0;Y=((_*3|0)+4>>3)+Y|0;_=_-(Y>>1)|0;Y=_+Y|0;_=(1-ba>>1)+_|0;ba=(1-_>>1)-ba+(W+1>>1)|0;_=(Y+1>>1)-_|0;c[X>>2]=W-ba;c[Z>>2]=Y-_;c[$>>2]=_;c[ca>>2]=ba}}while(0);h:do{if(!g){if(!(!e?(c[u>>2]|0)==0:0)){W=E+ -344|0;_=E+ -352|0;$=E+ -320|0;X=E+ -312|0;ca=c[$>>2]|0;Z=c[X>>2]|0;Y=Z+(c[W>>2]|0)|0;ba=ca+(c[_>>2]|0)|0;Z=Z-(Y+1>>1)|0;ca=ca-(ba+1>>1)|0;Y=Z+Y|0;Z=(Y>>1)-Z|0;Y=(Z*3>>3)+Y|0;Z=(Y>>7)+Z-(Y>>10)+(Y*3>>4)|0;Y=((Z*3|0)+4>>3)+Y|0;Z=Z-(Y>>1)|0;Y=Z+Y|0;ba=ca+ba|0;ca=(ba>>1)-ca|0;ba=(ca*3>>3)+ba|0;ca=(ba>>7)+ca-(ba>>10)+(ba*3>>4)|0;ba=((ca*3|0)+4>>3)+ba|0;ca=ca-(ba>>1)|0;ba=ca+ba|0;ca=(1-Z>>1)+ca|0;Z=(1-ca>>1)-Z+(Y+1>>1)|0;ca=(ba+1>>1)-ca|0;c[W>>2]=Y-Z;c[_>>2]=ba-ca;c[$>>2]=ca;c[X>>2]=Z;X=E+ -340|0;Z=E+ -348|0;$=E+ -316|0;ca=E+ -308|0;_=c[$>>2]|0;ba=c[ca>>2]|0;W=ba+(c[X>>2]|0)|0;Y=_+(c[Z>>2]|0)|0;ba=ba-(W+1>>1)|0;_=_-(Y+1>>1)|0;W=ba+W|0;ba=(W>>1)-ba|0;W=(ba*3>>3)+W|0;ba=(W>>7)+ba-(W>>10)+(W*3>>4)|0;W=((ba*3|0)+4>>3)+W|0;ba=ba-(W>>1)|0;W=ba+W|0;Y=_+Y|0;_=(Y>>1)-_|0;Y=(_*3>>3)+Y|0;_=(Y>>7)+_-(Y>>10)+(Y*3>>4)|0;Y=((_*3|0)+4>>3)+Y|0;_=_-(Y>>1)|0;Y=_+Y|0;_=(1-ba>>1)+_|0;ba=(1-_>>1)-ba+(W+1>>1)|0;_=(Y+1>>1)-_|0;c[X>>2]=W-ba;c[Z>>2]=Y-_;c[$>>2]=_;c[ca>>2]=ba}if(!q?(c[w>>2]|0)==0:0){H=A}else{W=E+ -72|0;_=E+ -80|0;$=E+ -48|0;X=E+ -40|0;ca=c[$>>2]|0;Z=c[X>>2]|0;Y=Z+(c[W>>2]|0)|0;ba=ca+(c[_>>2]|0)|0;Z=Z-(Y+1>>1)|0;ca=ca-(ba+1>>1)|0;Y=Z+Y|0;Z=(Y>>1)-Z|0;Y=(Z*3>>3)+Y|0;Z=(Y>>7)+Z-(Y>>10)+(Y*3>>4)|0;Y=((Z*3|0)+4>>3)+Y|0;Z=Z-(Y>>1)|0;Y=Z+Y|0;ba=ca+ba|0;ca=(ba>>1)-ca|0;ba=(ca*3>>3)+ba|0;ca=(ba>>7)+ca-(ba>>10)+(ba*3>>4)|0;ba=((ca*3|0)+4>>3)+ba|0;ca=ca-(ba>>1)|0;ba=ca+ba|0;ca=(1-Z>>1)+ca|0;Z=(1-ca>>1)-Z+(Y+1>>1)|0;ca=(ba+1>>1)-ca|0;c[W>>2]=Y-Z;c[_>>2]=ba-ca;c[$>>2]=ca;c[X>>2]=Z;X=E+ -68|0;Z=E+ -76|0;$=E+ -44|0;ca=E+ -36|0;_=c[$>>2]|0;ba=c[ca>>2]|0;W=ba+(c[X>>2]|0)|0;Y=_+(c[Z>>2]|0)|0;ba=ba-(W+1>>1)|0;_=_-(Y+1>>1)|0;W=ba+W|0;ba=(W>>1)-ba|0;W=(ba*3>>3)+W|0;ba=(W>>7)+ba-(W>>10)+(W*3>>4)|0;W=((ba*3|0)+4>>3)+W|0;ba=ba-(W>>1)|0;W=ba+W|0;Y=_+Y|0;_=(Y>>1)-_|0;Y=(_*3>>3)+Y|0;_=(Y>>7)+_-(Y>>10)+(Y*3>>4)|0;Y=((_*3|0)+4>>3)+Y|0;_=_-(Y>>1)|0;Y=_+Y|0;_=(1-ba>>1)+_|0;ba=(1-_>>1)-ba+(W+1>>1)|0;_=(Y+1>>1)-_|0;c[X>>2]=W-ba;c[Z>>2]=Y-_;c[$>>2]=_;c[ca>>2]=ba;if(q){H=A;while(1){if(!((H|0)<-64)){break h}Mc(E+((H|32)<<2)|0,E+((H|48)<<2)|0,0);H=H+64|0}}else{H=A}}while(1){if((H|0)>=(((c[w>>2]|0)!=0?-64:0)|0)){break h}Mc(E+((H|32)<<2)|0,E+((H|48)<<2)|0,0);H=H+64|0}}}while(0);i:do{if(!p){if(!(!e?(c[u>>2]|0)==0:0)){ba=G+ -472|0;X=G+ -480|0;$=G+ -448|0;Y=G+ -440|0;W=c[$>>2]|0;ca=c[Y>>2]|0;Z=ca+(c[ba>>2]|0)|0;_=W+(c[X>>2]|0)|0;ca=ca-(Z+1>>1)|0;W=W-(_+1>>1)|0;Z=ca+Z|0;ca=(Z>>1)-ca|0;Z=(ca*3>>3)+Z|0;ca=(Z>>7)+ca-(Z>>10)+(Z*3>>4)|0;Z=((ca*3|0)+4>>3)+Z|0;ca=ca-(Z>>1)|0;Z=ca+Z|0;_=W+_|0;W=(_>>1)-W|0;_=(W*3>>3)+_|0;W=(_>>7)+W-(_>>10)+(_*3>>4)|0;_=((W*3|0)+4>>3)+_|0;W=W-(_>>1)|0;_=W+_|0;W=(1-ca>>1)+W|0;ca=(1-W>>1)-ca+(Z+1>>1)|0;W=(_+1>>1)-W|0;c[ba>>2]=Z-ca;c[X>>2]=_-W;c[$>>2]=W;c[Y>>2]=ca;Y=G+ -468|0;ca=G+ -476|0;$=G+ -444|0;W=G+ -436|0;X=c[$>>2]|0;_=c[W>>2]|0;ba=_+(c[Y>>2]|0)|0;Z=X+(c[ca>>2]|0)|0;_=_-(ba+1>>1)|0;X=X-(Z+1>>1)|0;ba=_+ba|0;_=(ba>>1)-_|0;ba=(_*3>>3)+ba|0;_=(ba>>7)+_-(ba>>10)+(ba*3>>4)|0;ba=((_*3|0)+4>>3)+ba|0;_=_-(ba>>1)|0;ba=_+ba|0;Z=X+Z|0;X=(Z>>1)-X|0;Z=(X*3>>3)+Z|0;X=(Z>>7)+X-(Z>>10)+(Z*3>>4)|0;Z=((X*3|0)+4>>3)+Z|0;X=X-(Z>>1)|0;Z=X+Z|0;X=(1-_>>1)+X|0;_=(1-X>>1)-_+(ba+1>>1)|0;X=(Z+1>>1)-X|0;c[Y>>2]=ba-_;c[ca>>2]=Z-X;c[$>>2]=X;c[W>>2]=_;W=G+ -408|0;_=G+ -416|0;$=G+ -384|0;X=G+ -376|0;ca=c[$>>2]|0;Z=c[X>>2]|0;Y=Z+(c[W>>2]|0)|0;ba=ca+(c[_>>2]|0)|0;Z=Z-(Y+1>>1)|0;ca=ca-(ba+1>>1)|0;Y=Z+Y|0;Z=(Y>>1)-Z|0;Y=(Z*3>>3)+Y|0;Z=(Y>>7)+Z-(Y>>10)+(Y*3>>4)|0;Y=((Z*3|0)+4>>3)+Y|0;Z=Z-(Y>>1)|0;Y=Z+Y|0;ba=ca+ba|0;ca=(ba>>1)-ca|0;ba=(ca*3>>3)+ba|0;ca=(ba>>7)+ca-(ba>>10)+(ba*3>>4)|0;ba=((ca*3|0)+4>>3)+ba|0;ca=ca-(ba>>1)|0;ba=ca+ba|0;ca=(1-Z>>1)+ca|0;Z=(1-ca>>1)-Z+(Y+1>>1)|0;ca=(ba+1>>1)-ca|0;c[W>>2]=Y-Z;c[_>>2]=ba-ca;c[$>>2]=ca;c[X>>2]=Z;X=G+ -404|0;Z=G+ -412|0;$=G+ -380|0;ca=G+ -372|0;_=c[$>>2]|0;ba=c[ca>>2]|0;W=ba+(c[X>>2]|0)|0;Y=_+(c[Z>>2]|0)|0;ba=ba-(W+1>>1)|0;_=_-(Y+1>>1)|0;W=ba+W|0;ba=(W>>1)-ba|0;W=(ba*3>>3)+W|0;ba=(W>>7)+ba-(W>>10)+(W*3>>4)|0;W=((ba*3|0)+4>>3)+W|0;ba=ba-(W>>1)|0;W=ba+W|0;Y=_+Y|0;_=(Y>>1)-_|0;Y=(_*3>>3)+Y|0;_=(Y>>7)+_-(Y>>10)+(Y*3>>4)|0;Y=((_*3|0)+4>>3)+Y|0;_=_-(Y>>1)|0;Y=_+Y|0;_=(1-ba>>1)+_|0;ba=(1-_>>1)-ba+(W+1>>1)|0;_=(Y+1>>1)-_|0;c[X>>2]=W-ba;c[Z>>2]=Y-_;c[$>>2]=_;c[ca>>2]=ba}if(!q?(c[w>>2]|0)==0:0){H=A}else{ca=G+ -200|0;Y=G+ -208|0;ba=G+ -176|0;Z=G+ -168|0;X=c[ba>>2]|0;H=c[Z>>2]|0;_=H+(c[ca>>2]|0)|0;$=X+(c[Y>>2]|0)|0;H=H-(_+1>>1)|0;X=X-($+1>>1)|0;_=H+_|0;H=(_>>1)-H|0;_=(H*3>>3)+_|0;H=(_>>7)+H-(_>>10)+(_*3>>4)|0;_=((H*3|0)+4>>3)+_|0;H=H-(_>>1)|0;_=H+_|0;$=X+$|0;X=($>>1)-X|0;$=(X*3>>3)+$|0;X=($>>7)+X-($>>10)+($*3>>4)|0;$=((X*3|0)+4>>3)+$|0;X=X-($>>1)|0;$=X+$|0;X=(1-H>>1)+X|0;H=(1-X>>1)-H+(_+1>>1)|0;X=($+1>>1)-X|0;c[ca>>2]=_-H;c[Y>>2]=$-X;c[ba>>2]=X;c[Z>>2]=H;Z=G+ -196|0;H=G+ -204|0;ba=G+ -172|0;X=G+ -164|0;Y=c[ba>>2]|0;$=c[X>>2]|0;ca=$+(c[Z>>2]|0)|0;_=Y+(c[H>>2]|0)|0;$=$-(ca+1>>1)|0;Y=Y-(_+1>>1)|0;ca=$+ca|0;$=(ca>>1)-$|0;ca=($*3>>3)+ca|0;$=(ca>>7)+$-(ca>>10)+(ca*3>>4)|0;ca=(($*3|0)+4>>3)+ca|0;$=$-(ca>>1)|0;ca=$+ca|0;_=Y+_|0;Y=(_>>1)-Y|0;_=(Y*3>>3)+_|0;Y=(_>>7)+Y-(_>>10)+(_*3>>4)|0;_=((Y*3|0)+4>>3)+_|0;Y=Y-(_>>1)|0;_=Y+_|0;Y=(1-$>>1)+Y|0;$=(1-Y>>1)-$+(ca+1>>1)|0;Y=(_+1>>1)-Y|0;c[Z>>2]=ca-$;c[H>>2]=_-Y;c[ba>>2]=Y;c[X>>2]=$;X=G+ -136|0;$=G+ -144|0;ba=G+ -112|0;Y=G+ -104|0;H=c[ba>>2]|0;_=c[Y>>2]|0;Z=_+(c[X>>2]|0)|0;ca=H+(c[$>>2]|0)|0;_=_-(Z+1>>1)|0;H=H-(ca+1>>1)|0;Z=_+Z|0;_=(Z>>1)-_|0;Z=(_*3>>3)+Z|0;_=(Z>>7)+_-(Z>>10)+(Z*3>>4)|0;Z=((_*3|0)+4>>3)+Z|0;_=_-(Z>>1)|0;Z=_+Z|0;ca=H+ca|0;H=(ca>>1)-H|0;ca=(H*3>>3)+ca|0;H=(ca>>7)+H-(ca>>10)+(ca*3>>4)|0;ca=((H*3|0)+4>>3)+ca|0;H=H-(ca>>1)|0;ca=H+ca|0;H=(1-_>>1)+H|0;_=(1-H>>1)-_+(Z+1>>1)|0;H=(ca+1>>1)-H|0;c[X>>2]=Z-_;c[$>>2]=ca-H;c[ba>>2]=H;c[Y>>2]=_;Y=G+ -132|0;_=G+ -140|0;ba=G+ -108|0;H=G+ -100|0;$=c[ba>>2]|0;ca=c[H>>2]|0;X=ca+(c[Y>>2]|0)|0;Z=$+(c[_>>2]|0)|0;ca=ca-(X+1>>1)|0;$=$-(Z+1>>1)|0;X=ca+X|0;ca=(X>>1)-ca|0;X=(ca*3>>3)+X|0;ca=(X>>7)+ca-(X>>10)+(X*3>>4)|0;X=((ca*3|0)+4>>3)+X|0;ca=ca-(X>>1)|0;X=ca+X|0;Z=$+Z|0;$=(Z>>1)-$|0;Z=($*3>>3)+Z|0;$=(Z>>7)+$-(Z>>10)+(Z*3>>4)|0;Z=(($*3|0)+4>>3)+Z|0;$=$-(Z>>1)|0;Z=$+Z|0;$=(1-ca>>1)+$|0;ca=(1-$>>1)-ca+(X+1>>1)|0;$=(Z+1>>1)-$|0;c[Y>>2]=X-ca;c[_>>2]=Z-$;c[ba>>2]=$;c[H>>2]=ca;H=A}while(1){if(q){I=-64}else{I=(c[w>>2]|0)!=0?-64:0}if((H|0)>=(I|0)){break i}ca=H|16;ba=G+(ca<<2)|0;Mc(G+(H<<2)|0,ba,0);Mc(ba,G+(ca+16<<2)|0,0);H=H+64|0}}}while(0);if(!n?(c[y>>2]|0)==0:0){if(!(!e?(c[u>>2]|0)==0:0)){W=E+ -280|0;_=E+ -288|0;$=G+ -512|0;X=G+ -504|0;ca=c[$>>2]|0;Z=c[X>>2]|0;Y=Z+(c[W>>2]|0)|0;ba=ca+(c[_>>2]|0)|0;Z=Z-(Y+1>>1)|0;ca=ca-(ba+1>>1)|0;Y=Z+Y|0;Z=(Y>>1)-Z|0;Y=(Z*3>>3)+Y|0;Z=(Y>>7)+Z-(Y>>10)+(Y*3>>4)|0;Y=((Z*3|0)+4>>3)+Y|0;Z=Z-(Y>>1)|0;Y=Z+Y|0;ba=ca+ba|0;ca=(ba>>1)-ca|0;ba=(ca*3>>3)+ba|0;ca=(ba>>7)+ca-(ba>>10)+(ba*3>>4)|0;ba=((ca*3|0)+4>>3)+ba|0;ca=ca-(ba>>1)|0;ba=ca+ba|0;ca=(1-Z>>1)+ca|0;Z=(1-ca>>1)-Z+(Y+1>>1)|0;ca=(ba+1>>1)-ca|0;c[W>>2]=Y-Z;c[_>>2]=ba-ca;c[$>>2]=ca;c[X>>2]=Z;X=E+ -276|0;Z=E+ -284|0;$=G+ -508|0;ca=G+ -500|0;_=c[$>>2]|0;ba=c[ca>>2]|0;W=ba+(c[X>>2]|0)|0;Y=_+(c[Z>>2]|0)|0;ba=ba-(W+1>>1)|0;_=_-(Y+1>>1)|0;W=ba+W|0;ba=(W>>1)-ba|0;W=(ba*3>>3)+W|0;ba=(W>>7)+ba-(W>>10)+(W*3>>4)|0;W=((ba*3|0)+4>>3)+W|0;ba=ba-(W>>1)|0;W=ba+W|0;Y=_+Y|0;_=(Y>>1)-_|0;Y=(_*3>>3)+Y|0;_=(Y>>7)+_-(Y>>10)+(Y*3>>4)|0;Y=((_*3|0)+4>>3)+Y|0;_=_-(Y>>1)|0;Y=_+Y|0;_=(1-ba>>1)+_|0;ba=(1-_>>1)-ba+(W+1>>1)|0;_=(Y+1>>1)-_|0;c[X>>2]=W-ba;c[Z>>2]=Y-_;c[$>>2]=_;c[ca>>2]=ba}if(!q?(c[w>>2]|0)==0:0){H=A}else{X=E+ -8|0;$=E+ -16|0;ba=G+ -240|0;Y=G+ -232|0;H=c[ba>>2]|0;_=c[Y>>2]|0;Z=_+(c[X>>2]|0)|0;ca=H+(c[$>>2]|0)|0;_=_-(Z+1>>1)|0;H=H-(ca+1>>1)|0;Z=_+Z|0;_=(Z>>1)-_|0;Z=(_*3>>3)+Z|0;_=(Z>>7)+_-(Z>>10)+(Z*3>>4)|0;Z=((_*3|0)+4>>3)+Z|0;_=_-(Z>>1)|0;Z=_+Z|0;ca=H+ca|0;H=(ca>>1)-H|0;ca=(H*3>>3)+ca|0;H=(ca>>7)+H-(ca>>10)+(ca*3>>4)|0;ca=((H*3|0)+4>>3)+ca|0;H=H-(ca>>1)|0;ca=H+ca|0;H=(1-_>>1)+H|0;_=(1-H>>1)-_+(Z+1>>1)|0;H=(ca+1>>1)-H|0;c[X>>2]=Z-_;c[$>>2]=ca-H;c[ba>>2]=H;c[Y>>2]=_;Y=E+ -4|0;_=E+ -12|0;ba=G+ -236|0;H=G+ -228|0;$=c[ba>>2]|0;ca=c[H>>2]|0;X=ca+(c[Y>>2]|0)|0;Z=$+(c[_>>2]|0)|0;ca=ca-(X+1>>1)|0;$=$-(Z+1>>1)|0;X=ca+X|0;ca=(X>>1)-ca|0;X=(ca*3>>3)+X|0;ca=(X>>7)+ca-(X>>10)+(X*3>>4)|0;X=((ca*3|0)+4>>3)+X|0;ca=ca-(X>>1)|0;X=ca+X|0;Z=$+Z|0;$=(Z>>1)-$|0;Z=($*3>>3)+Z|0;$=(Z>>7)+$-(Z>>10)+(Z*3>>4)|0;Z=(($*3|0)+4>>3)+Z|0;$=$-(Z>>1)|0;Z=$+Z|0;$=(1-ca>>1)+$|0;ca=(1-$>>1)-ca+(X+1>>1)|0;$=(Z+1>>1)-$|0;c[Y>>2]=X-ca;c[_>>2]=Z-$;c[ba>>2]=$;c[H>>2]=ca;H=A}while(1){if(q){I=-64}else{I=(c[w>>2]|0)!=0?-64:0}if((H|0)>=(I|0)){break e}Mc(E+((H|48)<<2)|0,G+(H<<2)|0,0);H=H+64|0}}if(!(!g?(c[y>>2]|0)==0:0)){I=A;f=473}j:do{if((f|0)==473){while(1){f=0;if(q){H=-64}else{H=(c[w>>2]|0)!=0?-64:0}if((I|0)>=(H|0)){break j}f=I|5;Z=G+(f<<2)|0;ca=G+(f+ -1<<2)|0;ba=G+(f+59<<2)|0;Y=G+(f+60<<2)|0;$=c[ba>>2]|0;_=c[Y>>2]|0;W=_+(c[Z>>2]|0)|0;X=$+(c[ca>>2]|0)|0;_=_-(W+1>>1)|0;$=$-(X+1>>1)|0;W=_+W|0;_=(W>>1)-_|0;W=(_*3>>3)+W|0;_=(W>>7)+_-(W>>10)+(W*3>>4)|0;W=((_*3|0)+4>>3)+W|0;_=_-(W>>1)|0;W=_+W|0;X=$+X|0;$=(X>>1)-$|0;X=($*3>>3)+X|0;$=(X>>7)+$-(X>>10)+(X*3>>4)|0;X=(($*3|0)+4>>3)+X|0;$=$-(X>>1)|0;X=$+X|0;$=(1-_>>1)+$|0;_=(1-$>>1)-_+(W+1>>1)|0;$=(X+1>>1)-$|0;c[Z>>2]=W-_;c[ca>>2]=X-$;c[ba>>2]=$;c[Y>>2]=_;Y=G+((I|7)<<2)|0;_=G+(f+1<<2)|0;ba=G+(f+61<<2)|0;f=G+(f+62<<2)|0;$=c[ba>>2]|0;ca=c[f>>2]|0;X=ca+(c[Y>>2]|0)|0;Z=$+(c[_>>2]|0)|0;ca=ca-(X+1>>1)|0;$=$-(Z+1>>1)|0;X=ca+X|0;ca=(X>>1)-ca|0;X=(ca*3>>3)+X|0;ca=(X>>7)+ca-(X>>10)+(X*3>>4)|0;X=((ca*3|0)+4>>3)+X|0;ca=ca-(X>>1)|0;X=ca+X|0;Z=$+Z|0;$=(Z>>1)-$|0;Z=($*3>>3)+Z|0;$=(Z>>7)+$-(Z>>10)+(Z*3>>4)|0;Z=(($*3|0)+4>>3)+Z|0;$=$-(Z>>1)|0;Z=$+Z|0;$=(1-ca>>1)+$|0;ca=(1-$>>1)-ca+(X+1>>1)|0;$=(Z+1>>1)-$|0;c[Y>>2]=X-ca;c[_>>2]=Z-$;c[ba>>2]=$;c[f>>2]=ca;I=I+64|0;f=473}}}while(0);if(!p?(c[y>>2]|0)==0:0){break}else{G=A}while(1){if(q){H=-64}else{H=(c[w>>2]|0)!=0?-64:0}if((G|0)>=(H|0)){break e}ca=G|61;Y=E+(ca<<2)|0;ba=E+(ca+ -1<<2)|0;$=E+(ca+59<<2)|0;X=E+(ca+60<<2)|0;_=c[$>>2]|0;Z=c[X>>2]|0;V=Z+(c[Y>>2]|0)|0;W=_+(c[ba>>2]|0)|0;Z=Z-(V+1>>1)|0;_=_-(W+1>>1)|0;V=Z+V|0;Z=(V>>1)-Z|0;V=(Z*3>>3)+V|0;Z=(V>>7)+Z-(V>>10)+(V*3>>4)|0;V=((Z*3|0)+4>>3)+V|0;Z=Z-(V>>1)|0;V=Z+V|0;W=_+W|0;_=(W>>1)-_|0;W=(_*3>>3)+W|0;_=(W>>7)+_-(W>>10)+(W*3>>4)|0;W=((_*3|0)+4>>3)+W|0;_=_-(W>>1)|0;W=_+W|0;_=(1-Z>>1)+_|0;Z=(1-_>>1)-Z+(V+1>>1)|0;_=(W+1>>1)-_|0;c[Y>>2]=V-Z;c[ba>>2]=W-_;c[$>>2]=_;c[X>>2]=Z;X=E+((G|63)<<2)|0;Z=E+(ca+1<<2)|0;$=E+(ca+61<<2)|0;ca=E+(ca+62<<2)|0;_=c[$>>2]|0;ba=c[ca>>2]|0;W=ba+(c[X>>2]|0)|0;Y=_+(c[Z>>2]|0)|0;ba=ba-(W+1>>1)|0;_=_-(Y+1>>1)|0;W=ba+W|0;ba=(W>>1)-ba|0;W=(ba*3>>3)+W|0;ba=(W>>7)+ba-(W>>10)+(W*3>>4)|0;W=((ba*3|0)+4>>3)+W|0;ba=ba-(W>>1)|0;W=ba+W|0;Y=_+Y|0;_=(Y>>1)-_|0;Y=(_*3>>3)+Y|0;_=(Y>>7)+_-(Y>>10)+(Y*3>>4)|0;Y=((_*3|0)+4>>3)+Y|0;_=_-(Y>>1)|0;Y=_+Y|0;_=(1-ba>>1)+_|0;ba=(1-_>>1)-ba+(W+1>>1)|0;_=(Y+1>>1)-_|0;c[X>>2]=W-ba;c[Z>>2]=Y-_;c[$>>2]=_;c[ca>>2]=ba;G=G+64|0}}}}while(0)}while(D>>>0<v>>>0);i=l;return 0}function Rc(a){a=a|0;var b=0,e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0;t=i;z=c[a+34128>>2]|0;f=c[a+34292>>2]|0;g=c[a+34272>>2]|0;e=c[a+34156>>2]|0;if((e|0)==0){i=t;return 0}j=a+34116|0;k=c[8340>>2]|0;l=c[8344>>2]|0;m=c[8348>>2]|0;n=c[8352>>2]|0;o=c[8356>>2]|0;p=c[8360>>2]|0;q=c[8364>>2]|0;r=c[8368>>2]|0;s=c[8372>>2]|0;b=c[8376>>2]|0;u=c[8380>>2]|0;v=c[8384>>2]|0;w=c[8388>>2]|0;x=c[8392>>2]|0;y=c[8396>>2]|0;h=c[a+116>>2]|0;if((z+ -1|0)>>>0>1){z=0;do{C=aa(c[(c[f+(g*240|0)+(z<<2)>>2]|0)+4>>2]|0,c[a+(z<<6)+32960>>2]|0)|0;A=c[a+(z<<2)+34716>>2]|0;c[A>>2]=C;if((h|0)!=3){C=c[(c[f+(g*240|0)+(z<<2)+64>>2]|0)+((d[j>>0]|0)*20|0)+4>>2]|0;c[A+(k<<2)>>2]=aa(c[a+(z<<6)+32964>>2]|0,C)|0;c[A+(l<<2)>>2]=aa(c[a+(z<<6)+32968>>2]|0,C)|0;c[A+(m<<2)>>2]=aa(c[a+(z<<6)+32972>>2]|0,C)|0;c[A+(n<<2)>>2]=aa(c[a+(z<<6)+32976>>2]|0,C)|0;c[A+(o<<2)>>2]=aa(c[a+(z<<6)+32980>>2]|0,C)|0;c[A+(p<<2)>>2]=aa(c[a+(z<<6)+32984>>2]|0,C)|0;c[A+(q<<2)>>2]=aa(c[a+(z<<6)+32988>>2]|0,C)|0;c[A+(r<<2)>>2]=aa(c[a+(z<<6)+32992>>2]|0,C)|0;c[A+(s<<2)>>2]=aa(c[a+(z<<6)+32996>>2]|0,C)|0;c[A+(b<<2)>>2]=aa(c[a+(z<<6)+33e3>>2]|0,C)|0;c[A+(u<<2)>>2]=aa(c[a+(z<<6)+33004>>2]|0,C)|0;c[A+(v<<2)>>2]=aa(c[a+(z<<6)+33008>>2]|0,C)|0;c[A+(w<<2)>>2]=aa(c[a+(z<<6)+33012>>2]|0,C)|0;c[A+(x<<2)>>2]=aa(c[a+(z<<6)+33016>>2]|0,C)|0;c[A+(y<<2)>>2]=aa(c[a+(z<<6)+33020>>2]|0,C)|0}z=z+1|0}while((z|0)!=(e|0));i=t;return 0}if((z|0)==2){A=0;do{C=aa(c[(c[f+(g*240|0)+(A<<2)>>2]|0)+4>>2]|0,c[a+(A<<6)+32960>>2]|0)|0;z=c[a+(A<<2)+34716>>2]|0;c[z>>2]=C;do{if((h|0)!=3){B=c[(c[f+(g*240|0)+(A<<2)+64>>2]|0)+((d[j>>0]|0)*20|0)+4>>2]|0;C=aa(c[a+(A<<6)+32964>>2]|0,B)|0;if((A|0)==0){c[z+(k<<2)>>2]=C;c[z+(l<<2)>>2]=aa(c[a+(A<<6)+32968>>2]|0,B)|0;c[z+(m<<2)>>2]=aa(c[a+(A<<6)+32972>>2]|0,B)|0;c[z+(n<<2)>>2]=aa(c[a+(A<<6)+32976>>2]|0,B)|0;c[z+(o<<2)>>2]=aa(c[a+(A<<6)+32980>>2]|0,B)|0;c[z+(p<<2)>>2]=aa(c[a+(A<<6)+32984>>2]|0,B)|0;c[z+(q<<2)>>2]=aa(c[a+(A<<6)+32988>>2]|0,B)|0;c[z+(r<<2)>>2]=aa(c[a+(A<<6)+32992>>2]|0,B)|0;c[z+(s<<2)>>2]=aa(c[a+(A<<6)+32996>>2]|0,B)|0;c[z+(b<<2)>>2]=aa(c[a+(A<<6)+33e3>>2]|0,B)|0;c[z+(u<<2)>>2]=aa(c[a+(A<<6)+33004>>2]|0,B)|0;c[z+(v<<2)>>2]=aa(c[a+(A<<6)+33008>>2]|0,B)|0;c[z+(w<<2)>>2]=aa(c[a+(A<<6)+33012>>2]|0,B)|0;c[z+(x<<2)>>2]=aa(c[a+(A<<6)+33016>>2]|0,B)|0;c[z+(y<<2)>>2]=aa(c[a+(A<<6)+33020>>2]|0,B)|0;break}else{c[z+256>>2]=C;c[z+64>>2]=aa(c[a+(A<<6)+32968>>2]|0,B)|0;c[z+320>>2]=aa(c[a+(A<<6)+32972>>2]|0,B)|0;c[z+128>>2]=aa(c[a+(A<<6)+32976>>2]|0,B)|0;c[z+384>>2]=aa(c[a+(A<<6)+32980>>2]|0,B)|0;c[z+192>>2]=aa(c[a+(A<<6)+32984>>2]|0,B)|0;c[z+448>>2]=aa(c[a+(A<<6)+32988>>2]|0,B)|0;break}}}while(0);A=A+1|0}while((A|0)!=(e|0));i=t;return 0}else{B=0;do{A=aa(c[(c[f+(g*240|0)+(B<<2)>>2]|0)+4>>2]|0,c[a+(B<<6)+32960>>2]|0)|0;C=c[a+(B<<2)+34716>>2]|0;c[C>>2]=A;do{if((h|0)!=3){A=c[(c[f+(g*240|0)+(B<<2)+64>>2]|0)+((d[j>>0]|0)*20|0)+4>>2]|0;z=aa(c[a+(B<<6)+32964>>2]|0,A)|0;if((B|0)==0){c[C+(k<<2)>>2]=z;c[C+(l<<2)>>2]=aa(c[a+(B<<6)+32968>>2]|0,A)|0;c[C+(m<<2)>>2]=aa(c[a+(B<<6)+32972>>2]|0,A)|0;c[C+(n<<2)>>2]=aa(c[a+(B<<6)+32976>>2]|0,A)|0;c[C+(o<<2)>>2]=aa(c[a+(B<<6)+32980>>2]|0,A)|0;c[C+(p<<2)>>2]=aa(c[a+(B<<6)+32984>>2]|0,A)|0;c[C+(q<<2)>>2]=aa(c[a+(B<<6)+32988>>2]|0,A)|0;c[C+(r<<2)>>2]=aa(c[a+(B<<6)+32992>>2]|0,A)|0;c[C+(s<<2)>>2]=aa(c[a+(B<<6)+32996>>2]|0,A)|0;c[C+(b<<2)>>2]=aa(c[a+(B<<6)+33e3>>2]|0,A)|0;c[C+(u<<2)>>2]=aa(c[a+(B<<6)+33004>>2]|0,A)|0;c[C+(v<<2)>>2]=aa(c[a+(B<<6)+33008>>2]|0,A)|0;c[C+(w<<2)>>2]=aa(c[a+(B<<6)+33012>>2]|0,A)|0;c[C+(x<<2)>>2]=aa(c[a+(B<<6)+33016>>2]|0,A)|0;c[C+(y<<2)>>2]=aa(c[a+(B<<6)+33020>>2]|0,A)|0;break}else{c[C+128>>2]=z;c[C+64>>2]=aa(c[a+(B<<6)+32968>>2]|0,A)|0;c[C+192>>2]=aa(c[a+(B<<6)+32972>>2]|0,A)|0;break}}}while(0);B=B+1|0}while((B|0)!=(e|0));i=t;return 0}return 0}function Sc(a){a=a|0;var b=0,d=0,e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0;b=i;h=c[a+34128>>2]|0;if((h+ -1|0)>>>0<2){j=1}else{j=c[a+34156>>2]|0}g=a+32960|0;d=c[a+34328>>2]|0;e=be(a,d)|0;f=e&3;e=e&12;if((j|0)>0){k=d+ -1|0;l=0;do{m=a+(l<<6)+32960|0;if((f|0)==0){c[m>>2]=(c[m>>2]|0)+(c[(c[a+(l<<2)+34788>>2]|0)+(k*40|0)+8>>2]|0)}else if((f|0)==1){c[m>>2]=(c[m>>2]|0)+(c[(c[a+(l<<2)+34852>>2]|0)+(d*40|0)+8>>2]|0)}else if((f|0)==2){c[m>>2]=((c[(c[a+(l<<2)+34852>>2]|0)+(d*40|0)+8>>2]|0)+(c[(c[a+(l<<2)+34788>>2]|0)+(k*40|0)+8>>2]|0)>>1)+(c[m>>2]|0)}if((e|0)==0){n=c[(c[a+(l<<2)+34788>>2]|0)+(k*40|0)+36>>2]|0;m=a+(l<<6)+32964|0;c[m>>2]=(c[m>>2]|0)+(c[n>>2]|0);m=a+(l<<6)+32968|0;c[m>>2]=(c[m>>2]|0)+(c[n+4>>2]|0);m=a+(l<<6)+32972|0;c[m>>2]=(c[m>>2]|0)+(c[n+8>>2]|0)}else if((e|0)==4){m=c[(c[a+(l<<2)+34852>>2]|0)+(d*40|0)+36>>2]|0;n=a+(l<<6)+32976|0;c[n>>2]=(c[n>>2]|0)+(c[m+12>>2]|0);n=a+(l<<6)+32992|0;c[n>>2]=(c[n>>2]|0)+(c[m+16>>2]|0);n=a+(l<<6)+33008|0;c[n>>2]=(c[n>>2]|0)+(c[m+20>>2]|0)}l=l+1|0}while((l|0)!=(j|0))}if((h|0)==2){l=(f|0)==1;k=d+ -1|0;j=1;do{m=a+(j<<6)+32960|0;do{if(!l){if((f|0)==0){c[m>>2]=(c[m>>2]|0)+(c[(c[a+(j<<2)+34788>>2]|0)+(k*40|0)+8>>2]|0);break}else if((f|0)==2){c[m>>2]=((c[(c[a+(j<<2)+34788>>2]|0)+(k*40|0)+8>>2]|0)+1+(c[(c[a+(j<<2)+34852>>2]|0)+(d*40|0)+8>>2]|0)>>1)+(c[m>>2]|0);break}else{break}}else{c[m>>2]=(c[m>>2]|0)+(c[(c[a+(j<<2)+34852>>2]|0)+(d*40|0)+8>>2]|0)}}while(0);if((e|0)==0){m=a+(j<<2)+34788|0;n=a+(j<<6)+32976|0;c[n>>2]=(c[n>>2]|0)+(c[(c[(c[m>>2]|0)+(k*40|0)+36>>2]|0)+16>>2]|0);n=a+(j<<6)+32964|0;c[n>>2]=(c[n>>2]|0)+(c[c[(c[m>>2]|0)+(k*40|0)+36>>2]>>2]|0);n=a+(j<<6)+32980|0;c[n>>2]=(c[n>>2]|0)+(c[(c[(c[m>>2]|0)+(k*40|0)+36>>2]|0)+8>>2]|0)}else if((e|0)!=4){if(l){n=a+(j<<6)+32984|0;c[n>>2]=(c[n>>2]|0)+(c[a+(j<<6)+32968>>2]|0)}}else{m=a+(j<<2)+34852|0;n=a+(j<<6)+32976|0;c[n>>2]=(c[n>>2]|0)+(c[(c[(c[m>>2]|0)+(d*40|0)+36>>2]|0)+16>>2]|0);n=a+(j<<6)+32968|0;m=(c[n>>2]|0)+(c[(c[(c[m>>2]|0)+(d*40|0)+36>>2]|0)+12>>2]|0)|0;c[n>>2]=m;n=a+(j<<6)+32984|0;c[n>>2]=(c[n>>2]|0)+m}j=j+1|0}while((j|0)!=3);m=ae(g,h)|0;m=2-m|0;n=a+33984|0;c[n>>2]=m;i=b;return}else if((h|0)==1){j=d+ -1|0;k=a+33024|0;if((f|0)==0){c[k>>2]=(c[k>>2]|0)+(c[(c[a+34792>>2]|0)+(j*40|0)+8>>2]|0)}else if((f|0)==2){c[k>>2]=((c[(c[a+34792>>2]|0)+(j*40|0)+8>>2]|0)+1+(c[(c[a+34856>>2]|0)+(d*40|0)+8>>2]|0)>>1)+(c[k>>2]|0)}else if((f|0)==1){c[k>>2]=(c[k>>2]|0)+(c[(c[a+34856>>2]|0)+(d*40|0)+8>>2]|0)}if((e|0)==0){n=a+33028|0;c[n>>2]=(c[n>>2]|0)+(c[c[(c[a+34792>>2]|0)+(j*40|0)+36>>2]>>2]|0)}else if((e|0)==4){n=a+33032|0;c[n>>2]=(c[n>>2]|0)+(c[(c[(c[a+34856>>2]|0)+(d*40|0)+36>>2]|0)+4>>2]|0)}k=a+33088|0;if((f|0)==1){c[k>>2]=(c[k>>2]|0)+(c[(c[a+34860>>2]|0)+(d*40|0)+8>>2]|0)}else if((f|0)==0){c[k>>2]=(c[k>>2]|0)+(c[(c[a+34796>>2]|0)+(j*40|0)+8>>2]|0)}else if((f|0)==2){c[k>>2]=((c[(c[a+34796>>2]|0)+(j*40|0)+8>>2]|0)+1+(c[(c[a+34860>>2]|0)+(d*40|0)+8>>2]|0)>>1)+(c[k>>2]|0)}if((e|0)==0){m=a+33092|0;c[m>>2]=(c[m>>2]|0)+(c[c[(c[a+34796>>2]|0)+(j*40|0)+36>>2]>>2]|0);m=ae(g,h)|0;m=2-m|0;n=a+33984|0;c[n>>2]=m;i=b;return}else if((e|0)==4){m=a+33096|0;c[m>>2]=(c[m>>2]|0)+(c[(c[(c[a+34860>>2]|0)+(d*40|0)+36>>2]|0)+4>>2]|0);m=ae(g,h)|0;m=2-m|0;n=a+33984|0;c[n>>2]=m;i=b;return}else{m=ae(g,h)|0;m=2-m|0;n=a+33984|0;c[n>>2]=m;i=b;return}}else{m=ae(g,h)|0;m=2-m|0;n=a+33984|0;c[n>>2]=m;i=b;return}}function Tc(a){a=a|0;var b=0,e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0;b=i;e=c[a+34128>>2]|0;if(!((e+ -1|0)>>>0<2)){g=c[a+34156>>2]|0;f=2-(c[a+33984>>2]|0)|0;if((g|0)>0){h=4}}else{f=2-(c[a+33984>>2]|0)|0;g=1;h=4}if((h|0)==4){j=0;do{h=c[a+(j<<2)+34716>>2]|0;if((f|0)==0){k=64;do{l=h+((k|1)<<2)|0;c[l>>2]=(c[l>>2]|0)+(c[h+(k+ -63<<2)>>2]|0);l=h+((k|5)<<2)|0;c[l>>2]=(c[l>>2]|0)+(c[h+(k+ -59<<2)>>2]|0);l=h+((k|6)<<2)|0;c[l>>2]=(c[l>>2]|0)+(c[h+(k+ -58<<2)>>2]|0);k=k+16|0}while((k|0)<256)}else if((f|0)==1){k=0;do{m=(d[3920+k>>0]|0)<<4;l=h+((m|2)<<2)|0;c[l>>2]=(c[l>>2]|0)+(c[h+(m+ -14<<2)>>2]|0);l=h+((m|10)<<2)|0;c[l>>2]=(c[l>>2]|0)+(c[h+(m+ -6<<2)>>2]|0);l=h+((m|9)<<2)|0;c[l>>2]=(c[l>>2]|0)+(c[h+(m+ -7<<2)>>2]|0);k=k+1|0}while((k|0)!=12)}j=j+1|0}while((j|0)!=(g|0))}if((e|0)==1){e=c[a+34720>>2]|0;if((f|0)==1){l=e+72|0;c[l>>2]=(c[l>>2]|0)+(c[e+8>>2]|0);l=e+104|0;c[l>>2]=(c[l>>2]|0)+(c[e+40>>2]|0);l=e+100|0;c[l>>2]=(c[l>>2]|0)+(c[e+36>>2]|0);l=e+200|0;c[l>>2]=(c[l>>2]|0)+(c[e+136>>2]|0);l=e+232|0;c[l>>2]=(c[l>>2]|0)+(c[e+168>>2]|0);l=e+228|0;c[l>>2]=(c[l>>2]|0)+(c[e+164>>2]|0);l=c[a+34724>>2]|0;m=l+72|0;c[m>>2]=(c[m>>2]|0)+(c[l+8>>2]|0);m=l+104|0;c[m>>2]=(c[m>>2]|0)+(c[l+40>>2]|0);m=l+100|0;c[m>>2]=(c[m>>2]|0)+(c[l+36>>2]|0);m=l+200|0;c[m>>2]=(c[m>>2]|0)+(c[l+136>>2]|0);m=l+232|0;c[m>>2]=(c[m>>2]|0)+(c[l+168>>2]|0);m=l+228|0;c[m>>2]=(c[m>>2]|0)+(c[l+164>>2]|0);i=b;return}else if((f|0)==0){l=e+132|0;c[l>>2]=(c[l>>2]|0)+(c[e+4>>2]|0);l=e+148|0;c[l>>2]=(c[l>>2]|0)+(c[e+20>>2]|0);l=e+152|0;c[l>>2]=(c[l>>2]|0)+(c[e+24>>2]|0);l=e+196|0;c[l>>2]=(c[l>>2]|0)+(c[e+68>>2]|0);l=e+212|0;c[l>>2]=(c[l>>2]|0)+(c[e+84>>2]|0);l=e+216|0;c[l>>2]=(c[l>>2]|0)+(c[e+88>>2]|0);l=c[a+34724>>2]|0;m=l+132|0;c[m>>2]=(c[m>>2]|0)+(c[l+4>>2]|0);m=l+148|0;c[m>>2]=(c[m>>2]|0)+(c[l+20>>2]|0);m=l+152|0;c[m>>2]=(c[m>>2]|0)+(c[l+24>>2]|0);m=l+196|0;c[m>>2]=(c[m>>2]|0)+(c[l+68>>2]|0);m=l+212|0;c[m>>2]=(c[m>>2]|0)+(c[l+84>>2]|0);m=l+216|0;c[m>>2]=(c[m>>2]|0)+(c[l+88>>2]|0);i=b;return}else{i=b;return}}else if((e|0)==2){e=16;do{g=c[a+((e>>3)+ -1<<2)+34716>>2]|0;if((f|0)==0){m=c[8180>>2]|0;l=g+(m+1<<2)|0;c[l>>2]=(c[l>>2]|0)+(c[g+(m+ -63<<2)>>2]|0);l=g+(m+5<<2)|0;c[l>>2]=(c[l>>2]|0)+(c[g+(m+ -59<<2)>>2]|0);l=g+(m+6<<2)|0;c[l>>2]=(c[l>>2]|0)+(c[g+(m+ -58<<2)>>2]|0);l=c[8188>>2]|0;m=g+(l+1<<2)|0;c[m>>2]=(c[m>>2]|0)+(c[g+(l+ -63<<2)>>2]|0);m=g+(l+5<<2)|0;c[m>>2]=(c[m>>2]|0)+(c[g+(l+ -59<<2)>>2]|0);m=g+(l+6<<2)|0;c[m>>2]=(c[m>>2]|0)+(c[g+(l+ -58<<2)>>2]|0);m=c[8196>>2]|0;l=g+(m+1<<2)|0;c[l>>2]=(c[l>>2]|0)+(c[g+(m+ -63<<2)>>2]|0);l=g+(m+5<<2)|0;c[l>>2]=(c[l>>2]|0)+(c[g+(m+ -59<<2)>>2]|0);l=g+(m+6<<2)|0;c[l>>2]=(c[l>>2]|0)+(c[g+(m+ -58<<2)>>2]|0);l=c[8204>>2]|0;m=g+(l+1<<2)|0;c[m>>2]=(c[m>>2]|0)+(c[g+(l+ -63<<2)>>2]|0);m=g+(l+5<<2)|0;c[m>>2]=(c[m>>2]|0)+(c[g+(l+ -59<<2)>>2]|0);m=g+(l+6<<2)|0;c[m>>2]=(c[m>>2]|0)+(c[g+(l+ -58<<2)>>2]|0)}else if((f|0)==1){h=2;do{l=c[8176+(h<<2)>>2]|0;m=g+(l+10<<2)|0;c[m>>2]=(c[m>>2]|0)+(c[g+(l+ -6<<2)>>2]|0);m=g+(l+2<<2)|0;c[m>>2]=(c[m>>2]|0)+(c[g+(l+ -14<<2)>>2]|0);m=g+(l+9<<2)|0;c[m>>2]=(c[m>>2]|0)+(c[g+(l+ -7<<2)>>2]|0);h=h+1|0}while((h|0)!=8)}e=e+8|0}while((e|0)<32);i=b;return}else{i=b;return}}function Uc(a,b){a=a|0;b=b|0;var d=0,e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0;e=i;d=c[a+34128>>2]|0;if(!((d+ -1|0)>>>0<2)){g=c[a+34156>>2]|0;f=c[a+34328>>2]|0;if((g|0)!=0){h=4}}else{f=c[a+34328>>2]|0;g=1;h=4}if((h|0)==4){j=a+34276|0;k=f+ -1|0;h=a+34280|0;l=0;do{n=c[a+(l<<2)+34052>>2]|0;o=(l|0)!=0|0;m=b+(o<<2)+576|0;p=c[m>>2]|0;if((p|0)==0){do{if((c[j>>2]|0)!=0){if((c[h>>2]|0)==0){n=(c[(c[a+(l<<2)+34852>>2]|0)+(f*40|0)+4>>2]|0)>>>10&1^n;break}else{n=n^1;break}}else{n=(c[(c[a+(l<<2)+34788>>2]|0)+(k*40|0)+4>>2]|0)>>>5&1^n}}while(0);n=n<<1&2^n;n=n<<3&16^n;n=n<<1&32^n;n=n<<2&204^n;n=n<<6&13056^n;n=n<<2&52224^n}else if((p|0)==2){n=n^65535}p=n&65535;if((p|0)==0){q=0}else{q=0;do{q=(c[3936+((p&15)<<2)>>2]|0)+q|0;p=p>>4}while((p|0)!=0)}r=b+(o<<2)+560|0;p=q+ -3+(c[r>>2]|0)|0;c[r>>2]=p;do{if((p+16|0)>>>0>31){if((p|0)<0){c[r>>2]=-16;p=-16;break}else{c[r>>2]=15;p=15;break}}}while(0);o=b+(o<<2)+568|0;q=(c[o>>2]|0)+(13-q)|0;c[o>>2]=q;do{if((q+16|0)>>>0>31){if((q|0)<0){c[o>>2]=-16;q=-16;break}else{c[o>>2]=15;q=15;break}}}while(0);do{if((p|0)<0){if((p|0)<(q|0)){c[m>>2]=1;break}else{c[m>>2]=2;break}}else{if((q|0)<0){c[m>>2]=2;break}else{c[m>>2]=0;break}}}while(0);c[a+(l<<2)+33988>>2]=n;c[(c[a+(l<<2)+34788>>2]|0)+(f*40|0)+4>>2]=n;l=l+1|0}while((l|0)!=(g|0))}if((d|0)==2){r=b+560|0;q=Vc(a,c[a+34056>>2]|0,f,1,r)|0;c[a+33992>>2]=q;c[(c[a+34792>>2]|0)+(f*40|0)+4>>2]=q;r=Vc(a,c[a+34060>>2]|0,f,2,r)|0;c[a+33996>>2]=r;c[(c[a+34796>>2]|0)+(f*40|0)+4>>2]=r;i=e;return}else if((d|0)==1){r=b+560|0;q=Wc(a,c[a+34056>>2]|0,f,1,r)|0;c[a+33992>>2]=q;c[(c[a+34792>>2]|0)+(f*40|0)+4>>2]=q;r=Wc(a,c[a+34060>>2]|0,f,2,r)|0;c[a+33996>>2]=r;c[(c[a+34796>>2]|0)+(f*40|0)+4>>2]=r;i=e;return}else{i=e;return}}function Vc(a,b,d,e,f){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,j=0;h=i;g=f+20|0;j=c[g>>2]|0;if((j|0)==2){b=b^255}else if((j|0)==0){do{if((c[a+34276>>2]|0)!=0){if((c[a+34280>>2]|0)==0){b=(c[(c[a+(e<<2)+34852>>2]|0)+(d*40|0)+4>>2]|0)>>>6&1^b;break}else{b=b^1;break}}else{b=(c[(c[a+(e<<2)+34788>>2]|0)+((d+ -1|0)*40|0)+4>>2]|0)>>>1&1^b}}while(0);b=b<<1&2^b;b=b<<2&12^b;b=b<<2&48^b;b=b<<2&192^b}j=b&65535;if((j|0)==0){j=0}else{d=0;do{d=(c[3936+((j&15)<<2)>>2]|0)+d|0;j=j>>4}while((j|0)!=0);j=d<<1}e=f+4|0;d=j+ -3+(c[e>>2]|0)|0;c[e>>2]=d;do{if((d+16|0)>>>0>31){if((d|0)<0){c[e>>2]=-16;d=-16;break}else{c[e>>2]=15;d=15;break}}}while(0);f=f+12|0;j=13-j+(c[f>>2]|0)|0;c[f>>2]=j;do{if((j+16|0)>>>0>31){if((j|0)<0){c[f>>2]=-16;j=-16;break}else{c[f>>2]=15;j=15;break}}}while(0);if((d|0)<0){if((d|0)<(j|0)){c[g>>2]=1;i=h;return b|0}else{c[g>>2]=2;i=h;return b|0}}else{if((j|0)<0){c[g>>2]=2;i=h;return b|0}else{c[g>>2]=0;i=h;return b|0}}return 0}function Wc(a,b,d,e,f){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,j=0;h=i;g=f+20|0;j=c[g>>2]|0;if((j|0)==2){b=b^15}else if((j|0)==0){do{if((c[a+34276>>2]|0)!=0){if((c[a+34280>>2]|0)==0){b=(c[(c[a+(e<<2)+34852>>2]|0)+(d*40|0)+4>>2]|0)>>>2&1^b;break}else{b=b^1;break}}else{b=(c[(c[a+(e<<2)+34788>>2]|0)+((d+ -1|0)*40|0)+4>>2]|0)>>>1&1^b}}while(0);b=b<<1&2^b;b=b<<2&12^b}j=b&65535;if((j|0)==0){j=0}else{d=0;do{d=(c[3936+((j&15)<<2)>>2]|0)+d|0;j=j>>4}while((j|0)!=0);j=d<<2}e=f+4|0;d=j+ -3+(c[e>>2]|0)|0;c[e>>2]=d;do{if((d+16|0)>>>0>31){if((d|0)<0){c[e>>2]=-16;d=-16;break}else{c[e>>2]=15;d=15;break}}}while(0);f=f+12|0;j=13-j+(c[f>>2]|0)|0;c[f>>2]=j;do{if((j+16|0)>>>0>31){if((j|0)<0){c[f>>2]=-16;j=-16;break}else{c[f>>2]=15;j=15;break}}}while(0);if((d|0)<0){if((d|0)<(j|0)){c[g>>2]=1;i=h;return b|0}else{c[g>>2]=2;i=h;return b|0}}else{if((j|0)<0){c[g>>2]=2;i=h;return b|0}else{c[g>>2]=0;i=h;return b|0}}return 0}function Xc(a,b){a=a|0;b=b|0;var d=0,e=0,f=0;b=i;d=ke(44)|0;if((d|0)==0){f=0;i=b;return f|0}if((a+ -1|0)>>>0>254){le(d);f=0;i=b;return f|0}else{f=d+0|0;e=f+36|0;do{c[f>>2]=0;f=f+4|0}while((f|0)<(e|0));c[d>>2]=a;c[d+8>>2]=0;c[d+40>>2]=0;c[d+36>>2]=0;c[d+28>>2]=0;f=d;i=b;return f|0}return 0}function Yc(a){a=a|0;var b=0,d=0,e=0,f=0,g=0,h=0,j=0,k=0,l=0;b=i;f=c[a>>2]|0;g=a+24|0;if((c[g>>2]|0)==0){c[g>>2]=1;c[a+32>>2]=0;c[a+28>>2]=0;c[a+16>>2]=c[4056+(f<<2)>>2];j=0}else{j=c[a+28>>2]|0}k=a+28|0;if((f|0)==6|(f|0)==12){l=c[a+32>>2]|0}else{l=j}g=a+40|0;do{if((j|0)>=(c[g>>2]|0)){if((l|0)>(c[a+36>>2]|0)){h=a+16|0;c[h>>2]=(c[h>>2]|0)+1;h=10;break}if((j|0)<-64){c[k>>2]=-64;break}if((j|0)>64){c[k>>2]=64}}else{h=a+16|0;c[h>>2]=(c[h>>2]|0)+ -1;h=10}}while(0);if((h|0)==10){c[k>>2]=0;c[a+32>>2]=0}h=a+32|0;j=c[h>>2]|0;if(!((j|0)<-64)){if((j|0)>64){c[h>>2]=64}}else{c[h>>2]=-64}h=c[a+16>>2]|0;if(!((h|0)>-1)){pa(4112,4120,458,4144)}j=c[4e3+(f<<2)>>2]|0;if((h|0)>=(j|0)){pa(4168,4120,459,4144)}k=(h|0)==0;c[g>>2]=k?-2147483648:-8;c[a+36>>2]=(h|0)==(j+ -1|0)?1073741824:8;switch(f|0){case 4:{c[a+20>>2]=4232;d=4192;e=0;break};case 8:{c[a+20>>2]=5696;d=5560;e=0;break};case 12:{c[a+12>>2]=6784+((h-((h+1|0)==(j|0))|0)*12<<2);c[a+20>>2]=6976+(h*112|0);d=6280+(h*25<<2)|0;e=6784+((h+ -1+(k&1)|0)*12<<2)|0;break};case 6:{c[a+12>>2]=4800+((h-((h+1|0)==(j|0))|0)*6<<2);c[a+20>>2]=4872+(h*88|0);d=4592+(h*13<<2)|0;e=4800+((h+ -1+(k&1)|0)*6<<2)|0;break};case 9:{c[a+20>>2]=6080+(h*100|0);d=5888+(h*19<<2)|0;e=6040;break};case 7:{c[a+20>>2]=5376+(h*92|0);d=5224+(h*15<<2)|0;e=5344;break};case 5:{c[a+20>>2]=4424+(h*84|0);d=4312+(h*11<<2)|0;e=4400;break};default:{pa(7536,4120,504,4144)}}c[a+4>>2]=d;c[a+8>>2]=e;i=b;return}function Zc(a,b,d,e){a=a|0;b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0,j=0,k=0;f=i;h=c[e+16>>2]|0;k=h+ -1|0;g=aa(c[d>>2]|0,c[7864+(k<<2)>>2]|0)|0;c[d>>2]=g;if((a|0)==1){j=d+4|0;c[j>>2]=aa(c[j>>2]|0,c[8072+(k<<2)>>2]|0)|0;j=g;h=0}else if((a|0)!=2){j=d+4|0;b=aa(c[j>>2]|0,c[7880+(k<<6)+(b+ -1<<2)>>2]|0)|0;c[j>>2]=(h|0)==3?b>>4:b;if((a|0)==0){d=c[e>>2]|0;g=g+ -70>>2;do{if((g|0)<-7){g=g+4|0;d=((g|0)<-16?-16:g)+d|0;if((d|0)<-8){d=e+8|0;g=c[d>>2]|0;if((g|0)==0){d=-8}else{c[d>>2]=g+ -1;d=0}}}else{if((g|0)>7){g=g+ -4|0;d=((g|0)>15?15:g)+d|0;if((d|0)>8){g=e+8|0;d=c[g>>2]|0;if((d|0)>14){c[g>>2]=15;d=8;break}else{c[g>>2]=d+1;d=0;break}}}}}while(0);c[e>>2]=d;i=f;return}else{j=g;h=0}}else{j=d+4|0;c[j>>2]=aa(c[j>>2]|0,c[8072+(h+2<<2)>>2]|0)|0;j=g;h=0}while(1){g=e+(h<<2)|0;a=c[g>>2]|0;j=j+ -70>>2;do{if((j|0)<-7){j=j+4|0;a=((j|0)<-16?-16:j)+a|0;if((a|0)<-8){j=e+(h<<2)+8|0;a=c[j>>2]|0;if((a|0)==0){a=-8}else{c[j>>2]=a+ -1;a=0}}}else{if((j|0)>7){j=j+ -4|0;a=((j|0)>15?15:j)+a|0;if((a|0)>8){j=e+(h<<2)+8|0;a=c[j>>2]|0;if((a|0)>14){c[j>>2]=15;a=8;break}else{c[j>>2]=a+1;a=0;break}}}}}while(0);c[g>>2]=a;h=h+1|0;if((h|0)>=2){break}j=c[d+(h<<2)>>2]|0}i=f;return}function _c(a){a=a|0;var b=0,d=0;b=i;d=a+492|0;c[d+0>>2]=0;c[d+4>>2]=0;c[d+8>>2]=0;c[d+12>>2]=0;c[a+508>>2]=3;d=a+512|0;c[d>>2]=0;c[d+4>>2]=0;c[a+528>>2]=2;c[a+524>>2]=4;c[a+520>>2]=4;d=a+532|0;c[d>>2]=0;c[d+4>>2]=0;c[a+548>>2]=1;c[a+544>>2]=8;c[a+540>>2]=8;c[a+552>>2]=1;c[a+556>>2]=1;c[a+564>>2]=-4;c[a+560>>2]=-4;c[a+572>>2]=4;c[a+568>>2]=4;c[a+580>>2]=0;c[a+576>>2]=0;i=b;return}function $c(a){a=a|0;var b=0,d=0;b=i;if((a|0)==0){i=b;return}else{d=0}do{c[a+(d<<3)+112>>2]=c[7544+(d<<2)>>2];c[a+(d<<3)+240>>2]=c[8208+(c[7608+(d<<2)>>2]<<2)>>2];c[a+(d<<3)+368>>2]=c[8208+(c[7672+(d<<2)>>2]<<2)>>2];d=d+1|0}while((d|0)!=16);i=b;return}function ad(a,b,d){a=a|0;b=b|0;d=d|0;var e=0,f=0,g=0,h=0,j=0;e=i;h=(c[a+32924>>2]|0)==0;if(h){f=c[a+16>>2]|0}else{f=6}g=c[(h?a+12|0:a+34128|0)>>2]|0;j=(g|0)==1;if(j){d=(d+1|0)>>>1}if(d>>>0>(c[a+32940>>2]|0)>>>0){d=-1;i=e;return d|0}d=(g+ -1|0)>>>0<2;if(d){b=(b+1|0)>>>1}if(b>>>0>134217727){d=-1;i=e;return d|0}do{if(h){g=c[a+20>>2]|0;if((f|0)==0){f=((aa(g,b)|0)+7|0)>>>3;break}else{f=aa((g+7|0)>>>3,b)|0;break}}else{if(d){f=j?6:4}else{f=(g|0)==3?3:1}f=aa(b<<2,f)|0}}while(0);d=(f>>>0>(c[a+32944>>2]|0)>>>0)<<31>>31;i=e;return d|0}function bd(a,d,e){a=a|0;d=d|0;e=e|0;var f=0,g=0,h=0,j=0;f=i;if(!(e>>>0<17)){pa(9184,8800,994,9240)}d=~(-1<<e)&d;if((d>>>e|0)==0){g=a+4|0;j=c[g>>2]<<e|d;c[g>>2]=j;d=a+8|0;e=(c[d>>2]|0)+e|0;j=(ve(j<<32-e|0)|0)&65535;g=a+20|0;h=c[g>>2]|0;b[h>>1]=j;c[g>>2]=h+(e>>>3&2)&c[a+12>>2];c[d>>2]=e&15;i=f;return}else{pa(9216,8800,981,9200)}}function cd(a,b){a=a|0;b=b|0;var e=0,f=0,g=0,h=0,j=0,k=0;e=i;if(!((b|0)>-1&b>>>0<17)){pa(9048,8800,901,9080)}f=a+4|0;g=c[a+12>>2]|0;if((g&1|0)==0){h=(c[f>>2]|0)>>>(32-b|0);j=a+8|0;b=(c[j>>2]|0)+b|0;k=a+20|0;a=(b>>>3)+(c[k>>2]|0)&g;c[k>>2]=a;g=b&15;c[j>>2]=g;c[f>>2]=((d[a+2>>0]|0)<<8|(d[a+3>>0]|0)|(d[a+1>>0]|0)<<16|(d[a>>0]|0)<<24)<<g;i=e;return h|0}else{pa(9112,8800,908,9096)}return 0}function dd(a,b,d){a=a|0;b=b|0;d=d|0;var e=0,f=0,g=0,h=0,j=0;e=i;h=a+34272|0;if((b|0)!=0){j=c[h>>2]|0;if(j>>>0<(c[a+132>>2]|0)>>>0?(g=j+1|0,(c[a+(g<<2)+136>>2]|0)==(b|0)):0){c[h>>2]=g}else{g=j}}else{c[h>>2]=0;g=0}h=a+34268|0;if((d|0)!=0){j=c[h>>2]|0;if(j>>>0<(c[a+16520>>2]|0)>>>0?(f=j+1|0,(c[a+(f<<2)+16524>>2]|0)==(d|0)):0){c[h>>2]=f}else{f=j}}else{c[h>>2]=0;f=0}j=a+(g<<2)+136|0;c[a+34276>>2]=(c[j>>2]|0)==(b|0);c[a+34280>>2]=(c[a+(f<<2)+16524>>2]|0)==(d|0);j=(b-(c[j>>2]|0)&15|0)==0|0;c[a+34284>>2]=j;d=a+34288|0;c[d>>2]=j;b=b+1|0;if((g|0)==(c[a+132>>2]|0)){if((b|0)!=(c[a+34332>>2]|0)){i=e;return}c[d>>2]=1;i=e;return}else{if((b|0)!=(c[a+(g+1<<2)+136>>2]|0)){i=e;return}c[d>>2]=1;i=e;return}}function ed(a){a=a|0;var b=0,d=0,e=0,f=0,g=0,h=0,j=0;b=i;f=c[a+34928>>2]|0;d=(f|0)!=0?2:1;e=1;while(1){j=a+34652|0;h=a+34524|0;g=j+64|0;do{c[j>>2]=c[h>>2];j=j+4|0;h=h+4|0}while((j|0)<(g|0));j=a+34716|0;h=a+34588|0;g=j+64|0;do{c[j>>2]=c[h>>2];j=j+4|0;h=h+4|0}while((j|0)<(g|0));if((e|0)==(d|0)){break}a=f;f=c[f+34928>>2]|0;e=e+1|0}i=b;return}function fd(a){a=a|0;var b=0,d=0,e=0,f=0,g=0,h=0,j=0,k=0,l=0;d=i;b=c[8728+(c[a+34128>>2]<<2)>>2]<<4;if((c[a+34932>>2]|0)!=0){pa(8768,8800,193,8824)}e=(c[a+34928>>2]|0)!=0?2:1;f=0;while(1){g=a+34156|0;if((c[g>>2]|0)!=0){h=256;j=0;while(1){k=a+(j<<2)+34652|0;l=c[k>>2]|0;c[a+(j<<2)+34460>>2]=l;c[k>>2]=l+(h<<2);k=a+(j<<2)+34716|0;c[k>>2]=(c[k>>2]|0)+(h<<2);j=j+1|0;if(j>>>0<(c[g>>2]|0)>>>0){h=b}else{break}}}f=f+1|0;if((f|0)==(e|0)){break}else{a=c[a+34928>>2]|0}}i=d;return}function gd(a){a=a|0;var b=0,d=0,e=0,f=0,g=0,h=0,j=0,k=0;b=i;d=(c[a+34928>>2]|0)!=0?2:1;e=0;while(1){f=a+34156|0;if((c[f>>2]|0)!=0){g=0;do{k=a+(g<<2)+34788|0;j=c[k>>2]|0;h=a+(g<<2)+34852|0;c[k>>2]=c[h>>2];c[h>>2]=j;g=g+1|0}while(g>>>0<(c[f>>2]|0)>>>0)}e=e+1|0;if((e|0)==(d|0)){break}else{a=c[a+34928>>2]|0}}i=b;return}function hd(a){a=a|0;var b=0,d=0,e=0,f=0,g=0,h=0,j=0,k=0,l=0;d=i;i=i+64|0;e=d;h=c[a+34928>>2]|0;b=(h|0)!=0?2:1;g=1;while(1){k=a+34524|0;f=e+0|0;l=k+0|0;j=f+64|0;do{c[f>>2]=c[l>>2];f=f+4|0;l=l+4|0}while((f|0)<(j|0));a=a+34588|0;f=k+0|0;l=a+0|0;j=f+64|0;do{c[f>>2]=c[l>>2];f=f+4|0;l=l+4|0}while((f|0)<(j|0));f=a+0|0;l=e+0|0;j=f+64|0;do{c[f>>2]=c[l>>2];f=f+4|0;l=l+4|0}while((f|0)<(j|0));if((g|0)==(b|0)){break}a=h;h=c[h+34928>>2]|0;g=g+1|0}i=d;return}function id(a,b,d){a=a|0;b=b|0;d=d|0;var e=0,f=0,g=0;e=i;i=i+16|0;f=e;g=me(1,44)|0;c[a>>2]=g;a=(g|0)!=0;if(!a){a=a?0:-101;i=e;return a|0}c[g+20>>2]=25;c[g+24>>2]=26;c[g+28>>2]=18;c[g+32>>2]=19;c[g+36>>2]=20;c[g+40>>2]=21;b=Ta(b|0,d|0)|0;c[g>>2]=b;if((b|0)==0){c[f>>2]=0;Da(8840,f|0)|0;b=c[g>>2]|0}a=(b|0)==0?-102:0;i=e;return a|0}function jd(a){a=a|0;var b=0,d=0;b=i;Ea(c[c[a>>2]>>2]|0)|0;d=c[a>>2]|0;if((d|0)==0){i=b;return 0}le(d);c[a>>2]=0;i=b;return 0}function kd(a){a=a|0;var b=0;b=i;a=Fa(c[a>>2]|0)|0;i=b;return a|0}function ld(a,b,d){a=a|0;b=b|0;d=d|0;var e=0;e=i;b=(ma(b|0,d|0,1,c[a>>2]|0)|0)==1;i=e;return(b?0:-102)|0}function md(a,b,d){a=a|0;b=b|0;d=d|0;var e=0;e=i;if((d|0)==0){b=0}else{a=(Oa(b|0,d|0,1,c[a>>2]|0)|0)!=1;b=a?-102:0}i=e;return b|0}function nd(a,b){a=a|0;b=b|0;var d=0;d=i;a=(Aa(c[a>>2]|0,b|0,0)|0)!=0;i=d;return(a?-102:0)|0}function od(a,b){a=a|0;b=b|0;var d=0,e=0;d=i;e=Ha(c[a>>2]|0)|0;a=(e|0)==-1;if(!a){c[b>>2]=e}i=d;return(a?-102:0)|0}function pd(b){b=b|0;var d=0,e=0,f=0;d=i;e=me(1,4144)|0;c[b>>2]=e;b=(e|0)!=0;f=b?0:-101;if(!b){i=d;return f|0}c[e>>2]=e+48;b=e+44|0;a[b>>0]=0;a[b+1>>0]=0;a[b+2>>0]=0;a[b+3>>0]=0;c[e+4>>2]=4096;c[e+8>>2]=0;c[e+12>>2]=0;c[e+20>>2]=27;c[e+24>>2]=0;c[e+28>>2]=20;c[e+32>>2]=21;c[e+36>>2]=22;c[e+40>>2]=23;i=d;return f|0}function qd(a){a=a|0;var b=0,d=0,e=0,f=0;b=i;if((a|0)!=0){e=c[a>>2]|0;f=c[e+44>>2]|0;if((f|0)!=0){while(1){d=c[f>>2]|0;le(f);if((d|0)==0){d=4;break}else{f=d}}}}else{d=4}if((d|0)==4){e=c[a>>2]|0}if((e|0)==0){i=b;return 0}le(e);c[a>>2]=0;i=b;return 0}function rd(a,b,d){a=a|0;b=b|0;d=d|0;var e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0;e=i;f=a+8|0;j=c[f>>2]|0;k=j+d|0;h=k>>>0<j>>>0;g=h?-103:0;if(h){i=e;return g|0}l=c[a+4>>2]|0;h=a+12|0;m=c[h>>2]<<12;if(l>>>0<(k+m|0)>>>0){d=l-j-m|0}if((d|0)==0){i=e;return g|0}while(1){k=4096-j|0;k=k>>>0>d>>>0?d:k;te(b|0,(c[a>>2]|0)+j|0,k|0)|0;j=(c[f>>2]|0)+k|0;c[f>>2]=j;if((j|0)==4096){c[a>>2]=(c[(c[a>>2]|0)+ -4>>2]|0)+4;c[f>>2]=0;c[h>>2]=(c[h>>2]|0)+1;j=0}if((d|0)==(k|0)){break}else{b=b+k|0;d=d-k|0}}i=e;return g|0}function sd(b,d,e){b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0,j=0,k=0,l=0,m=0;f=i;g=b+8|0;k=c[g>>2]|0;j=k+e|0;h=j>>>0<k>>>0;if(h){m=h?-103:0;i=f;return m|0}h=b+4|0;j=(c[h>>2]|0)>>>0<j>>>0;l=j?-103:0;if(j){m=l;i=f;return m|0}j=b+12|0;a:while(1){do{if((e|0)==0){b=9;break a}m=4096-k|0;m=m>>>0>e>>>0?e:m;te((c[b>>2]|0)+k|0,d|0,m|0)|0;k=(c[g>>2]|0)+m|0;c[g>>2]=k;d=d+m|0;e=e-m|0}while((k|0)!=4096);l=c[b>>2]|0;k=me(1,4100)|0;if((k|0)==0){l=-101;b=9;break}c[l+ -4>>2]=k;c[b>>2]=k+4;c[h>>2]=(c[h>>2]|0)+4096;a[k>>0]=0;a[k+1>>0]=0;a[k+2>>0]=0;a[k+3>>0]=0;c[g>>2]=0;c[j>>2]=(c[j>>2]|0)+1;k=0;l=0}if((b|0)==9){i=f;return l|0}return 0}function td(a,b){a=a|0;b=b|0;var d=0,e=0,f=0,g=0,h=0;e=i;g=a+44|0;d=a+8|0;c[d>>2]=0;f=a+12|0;c[f>>2]=0;if(b>>>0>4095){h=0;do{g=c[g>>2]|0;b=b+ -4096|0;h=h+1|0}while(b>>>0>4095&(g|0)!=0);c[f>>2]=h}if((g|0)==0){i=e;return 0}c[d>>2]=b;c[a>>2]=g+4;i=e;return 0}function ud(a,b){a=a|0;b=b|0;c[b>>2]=(c[a+12>>2]<<12)+(c[a+8>>2]|0);return 0}function vd(b,d){b=b|0;d=d|0;c[b>>2]=d;c[b+4>>2]=0;a[b+8>>0]=0;c[b+12>>2]=0;return 0}function wd(b,e){b=b|0;e=e|0;var f=0,g=0,h=0,j=0,k=0,l=0;g=i;f=b+12|0;l=c[f>>2]|0;h=b+8|0;if(l>>>0<e>>>0){j=b+4|0;k=0;while(1){k=(d[h>>0]|0)>>>(8-l|0)|k<<l;e=e-l|0;l=c[b>>2]|0;Xa[c[l+28>>2]&31](l,h,1)|0;c[j>>2]=(c[j>>2]|0)+1;c[f>>2]=8;if(e>>>0>8){l=8}else{l=8;break}}}else{k=0}j=d[h>>0]|0;a[h>>0]=j<<e;c[f>>2]=l-e;i=g;return j>>>(8-e|0)|k<<e|0}function xd(b){b=b|0;a[b+8>>0]=0;c[b+12>>2]=0;return}function yd(a){a=a|0;return c[a+4>>2]|0}function zd(a){a=a|0;if((c[a+12>>2]|0)==0){c[a>>2]=0;return 0}else{pa(8864,8800,656,8888)}return 0}function Ad(b){b=b|0;var d=0,e=0,f=0,g=0,h=0,j=0,k=0;d=i;f=c[b+116>>2]|0;if((f|0)!=3){if((f|0)==2){g=2}else{g=(f|0)==1?3:4}}else{g=1}a[b+34236>>0]=g;f=(c[b+104>>2]|0)==0;if((c[b+34144>>2]|0)==0){if(!f){pa(8904,8800,708,9024)}if((c[b+16520>>2]|0)==(0-(c[b+132>>2]|0)|0)){e=0}else{pa(8904,8800,708,9024)}}else{e=(c[b+132>>2]|0)+1|0;if(!f){e=aa(e,g&255)|0}if(e>>>0>16384){k=-1;i=d;return k|0}if((e|0)!=0){h=e<<5;f=h+16383+(e<<14)|0;k=ke(f)|0;if((k|0)==0){k=-1;i=d;return k|0}re(k|0,0,f|0)|0;g=b+34296|0;c[g>>2]=k;f=k;j=0;h=(k+h+16383&-16384)+8192|0;while(1){c[f+(j<<2)>>2]=h;j=j+1|0;if((j|0)==(e|0)){break}f=c[g>>2]|0;h=h+16384|0}f=c[b+16520>>2]|0;if(f>>>0>4095){k=-1;i=d;return k|0}k=ke(aa(e<<2,f+1|0)|0)|0;c[b+34264>>2]=k;if((k|0)==0){k=-1;i=d;return k|0}}else{e=0}}c[b+34300>>2]=e;k=0;i=d;return k|0}function Bd(b){b=b|0;var d=0,e=0,f=0,g=0,h=0,j=0,k=0,l=0;g=i;if((c[b+34300>>2]|0)==0){l=c[b+34308>>2]|0;k=c[b+34248>>2]|0;c[l+12>>2]=k;c[l+8>>2]=k;c[l+4>>2]=k;c[l>>2]=k;i=g;return 0}d=b+34308|0;e=b+34296|0;f=c[b+132>>2]|0;h=b+34236|0;if((c[b+104>>2]|0)==0){h=0;do{l=c[d>>2]|0;k=c[(c[e>>2]|0)+(h<<2)>>2]|0;c[l+(h*592|0)+12>>2]=k;c[l+(h*592|0)+8>>2]=k;c[l+(h*592|0)+4>>2]=k;c[l+(h*592|0)>>2]=k;h=h+1|0}while(!(h>>>0>f>>>0));i=g;return 0}else{k=0}do{j=c[d>>2]|0;b=a[h>>0]|0;l=aa(b&255,k)|0;c[j+(k*592|0)>>2]=c[(c[e>>2]|0)+(l<<2)>>2];if(((b&255)>1?(c[j+(k*592|0)+4>>2]=c[(c[e>>2]|0)+(l+1<<2)>>2],(b&255)>2):0)?(c[j+(k*592|0)+8>>2]=c[(c[e>>2]|0)+(l+2<<2)>>2],(b&255)>3):0){c[j+(k*592|0)+12>>2]=c[(c[e>>2]|0)+(l+3<<2)>>2]}k=k+1|0}while(!(k>>>0>f>>>0));i=g;return 0}function Cd(b){b=b|0;var d=0,e=0,f=0,g=0;d=i;e=b+132|0;f=c[e>>2]|0;if(f>>>0>4095){g=-1;i=d;return g|0}g=(f*240|0)+240|0;f=ke(g)|0;b=b+34292|0;c[b>>2]=f;if((f|0)==0){g=-1;i=d;return g|0}re(f|0,0,g|0)|0;g=0;while(1){a[f+(g*240|0)+192>>0]=1;a[(c[b>>2]|0)+(g*240|0)+193>>0]=1;a[(c[b>>2]|0)+(g*240|0)+194>>0]=0;a[(c[b>>2]|0)+(g*240|0)+195>>0]=0;g=g+1|0;if(g>>>0>(c[e>>2]|0)>>>0){e=0;break}f=c[b>>2]|0}i=d;return e|0}function Dd(a){a=a|0;var b=0,d=0,e=0,f=0,g=0,h=0,j=0;b=i;d=a+34180|0;if((c[d>>2]&1|0)==0){e=c[c[a+34292>>2]>>2]|0;if((e|0)!=0){le(e)}}else{h=a+132|0;g=a+34292|0;f=0;do{e=c[(c[g>>2]|0)+(f*240|0)>>2]|0;if((e|0)!=0){le(e)}f=f+1|0}while(!(f>>>0>(c[h>>2]|0)>>>0))}e=a+116|0;do{if((c[e>>2]|0)!=3){if((c[d>>2]&2|0)==0){f=c[(c[a+34292>>2]|0)+64>>2]|0;if((f|0)==0){break}le(f);break}h=a+132|0;g=a+34292|0;f=0;do{j=c[(c[g>>2]|0)+(f*240|0)+64>>2]|0;if((j|0)!=0){le(j)}f=f+1|0}while(!(f>>>0>(c[h>>2]|0)>>>0))}}while(0);do{if(((c[e>>2]|0)+ -2|0)>>>0<2){a=a+34292|0}else{if((c[d>>2]&4|0)==0){a=a+34292|0;d=c[(c[a>>2]|0)+128>>2]|0;if((d|0)==0){break}le(d);break}d=a+132|0;a=a+34292|0;f=0;do{e=c[(c[a>>2]|0)+(f*240|0)+128>>2]|0;if((e|0)!=0){le(e)}f=f+1|0}while(!(f>>>0>(c[d>>2]|0)>>>0))}}while(0);a=c[a>>2]|0;if((a|0)==0){i=b;return}le(a);i=b;return}function Ed(a){a=a|0;var b=0;b=i;a=c[a>>2]|0;if((a|0)!=0){le(a)}i=b;return}function Fd(a,b,d){a=a|0;b=b|0;d=d|0;var e=0,f=0,g=0;e=i;if(!(d>>>0>16|b>>>0>16)?(g=ke(aa(b*20|0,d)|0)|0,c[a>>2]=g,(g|0)!=0):0){if(b>>>0>1){f=1;while(1){g=g+(d*20|0)|0;c[a+(f<<2)>>2]=g;f=f+1|0;if((f|0)==(b|0)){d=0;break}}}else{d=0}}else{d=-1}i=e;return d|0}function Gd(a,b,d,e,f,g){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;g=g|0;var h=0,j=0,k=0,l=0,m=0;h=i;if((d|0)==0){i=h;return}j=a+4|0;f=(f|0)==1;if(b<<24>>24==1){b=0;do{k=(b|0)!=0;if(k){l=(c[a+(b<<2)>>2]|0)+(e*20|0)|0;m=(c[j>>2]|0)+(e*20|0)|0;c[l+0>>2]=c[m+0>>2];c[l+4>>2]=c[m+4>>2];c[l+8>>2]=c[m+8>>2];c[l+12>>2]=c[m+12>>2];c[l+16>>2]=c[m+16>>2]}Zd((c[a+(b<<2)>>2]|0)+(e*20|0)|0,k&f&1^1,g);b=b+1|0}while(b>>>0<d>>>0);i=h;return}else if(b<<24>>24==0){j=0;do{b=(j|0)!=0;if(b){m=(c[a+(j<<2)>>2]|0)+(e*20|0)|0;l=(c[a>>2]|0)+(e*20|0)|0;c[m+0>>2]=c[l+0>>2];c[m+4>>2]=c[l+4>>2];c[m+8>>2]=c[l+8>>2];c[m+12>>2]=c[l+12>>2];c[m+16>>2]=c[l+16>>2]}Zd((c[a+(j<<2)>>2]|0)+(e*20|0)|0,b&f&1^1,g);j=j+1|0}while((j|0)!=(d|0));i=h;return}else{j=0;do{Zd((c[a+(j<<2)>>2]|0)+(e*20|0)|0,(j|0)!=0&f&1^1,g);j=j+1|0}while(j>>>0<d>>>0);i=h;return}}function Hd(a,b){a=a|0;b=b|0;var d=0,e=0,f=0,g=0,h=0,j=0,k=0;d=i;e=c[a+34156>>2]|0;if((e|0)==0){i=d;return}f=c[a+132>>2]|0;g=(f|0)==0;a=a+34292|0;h=0;do{if(!g){j=1;do{k=c[a>>2]|0;if((b|0)==1){c[k+(j*240|0)+(h<<2)+64>>2]=c[k+(h<<2)+64>>2]}else if((b|0)==0){c[k+(j*240|0)+(h<<2)>>2]=c[k+(h<<2)>>2]}else{c[k+(j*240|0)+(h<<2)+128>>2]=c[k+(h<<2)+128>>2]}j=j+1|0}while(!(j>>>0>f>>>0))}h=h+1|0}while(h>>>0<e>>>0);i=d;return}function Id(a,b){a=a|0;b=b|0;var d=0,e=0,f=0,g=0,h=0;d=i;e=a+34156|0;if((c[e>>2]|0)==0){i=d;return}a=a+34292|0;f=0;do{h=c[a>>2]|0;g=c[h+(b*240|0)+(f<<2)+64>>2]|0;h=c[h+(b*240|0)+(f<<2)>>2]|0;c[g+0>>2]=c[h+0>>2];c[g+4>>2]=c[h+4>>2];c[g+8>>2]=c[h+8>>2];c[g+12>>2]=c[h+12>>2];c[g+16>>2]=c[h+16>>2];f=f+1|0}while(f>>>0<(c[e>>2]|0)>>>0);i=d;return}function Jd(a,b,d){a=a|0;b=b|0;d=d|0;var e=0,f=0,g=0,h=0,j=0,k=0,l=0;g=i;e=a+34156|0;h=c[e>>2]|0;if((h|0)==0){i=g;return}f=(b|0)==0;a=a+34292|0;j=h;h=0;do{if(!f){j=0;do{l=c[a>>2]|0;k=(c[l+(d*240|0)+(h<<2)+128>>2]|0)+(j*20|0)|0;l=(c[l+(d*240|0)+(h<<2)+64>>2]|0)+(j*20|0)|0;c[k+0>>2]=c[l+0>>2];c[k+4>>2]=c[l+4>>2];c[k+8>>2]=c[l+8>>2];c[k+12>>2]=c[l+12>>2];c[k+16>>2]=c[l+16>>2];j=j+1|0}while((j|0)!=(b|0));j=c[e>>2]|0}h=h+1|0}while(h>>>0<j>>>0);i=g;return}function Kd(a){a=a|0;var b=0;b=i;if(!((a&255)<2)){if(!((a&255)<4)){if((a&255)<6){a=2}else{a=(a&255)<10?3:4}}else{a=1}}else{a=0}i=b;return a|0}function Ld(a,b){a=a|0;b=b|0;if((b|0)>-1&b>>>0<17){return(c[a+4>>2]|0)>>>(32-b|0)|0}else{pa(9048,8800,901,9080)}return 0}function Md(a,b){a=a|0;b=b|0;var e=0,f=0,g=0,h=0,j=0;e=i;if(!((b|0)>-1&b>>>0<17)){pa(9048,8800,907,9096)}f=c[a+12>>2]|0;if((f&1|0)==0){g=a+8|0;h=(c[g>>2]|0)+b|0;j=a+20|0;b=(h>>>3)+(c[j>>2]|0)&f;c[j>>2]=b;f=h&15;c[g>>2]=f;c[a+4>>2]=((d[b+2>>0]|0)<<8|(d[b+3>>0]|0)|(d[b+1>>0]|0)<<16|(d[b>>0]|0)<<24)<<f;i=e;return 0}else{pa(9112,8800,908,9096)}return 0}function Nd(a,b){a=a|0;b=b|0;var e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0;e=i;if(!((b|0)>-1&b>>>0<33)){pa(9136,8800,957,9168)}if(!(b>>>0>16)){f=b;g=0;f=cd(a,f)|0;g=f|g;i=e;return g|0}f=a+4|0;g=c[a+12>>2]|0;if((g&1|0)!=0){pa(9112,8800,908,9096)}h=(c[f>>2]|0)>>>16;k=a+8|0;l=(c[k>>2]|0)+16|0;m=a+20|0;j=(l>>>3)+(c[m>>2]|0)&g;c[m>>2]=j;g=l&15;c[k>>2]=g;c[f>>2]=((d[j+2>>0]|0)<<8|(d[j+3>>0]|0)|(d[j+1>>0]|0)<<16|(d[j>>0]|0)<<24)<<g;g=b+ -16|0;f=g;g=h<<g;f=cd(a,f)|0;g=f|g;i=e;return g|0}function Od(a){a=a|0;var b=0,e=0,f=0,g=0;b=a+8|0;f=c[b>>2]|0;e=c[a+12>>2]|0;if((e&1|0)==0){f=(0-f&7)+f|0;g=a+20|0;e=(c[g>>2]|0)+(f>>>3)&e;c[g>>2]=e;f=f&15;c[b>>2]=f;c[a+4>>2]=((d[e+2>>0]|0)<<8|(d[e+3>>0]|0)|(d[e+1>>0]|0)<<16|(d[e>>0]|0)<<24)<<f;return 0}else{pa(9112,8800,908,9096)}return 0}function Pd(a,d,e){a=a|0;d=d|0;e=e|0;var f=0,g=0,h=0,j=0;f=i;if(!(e>>>0<17)){pa(9184,8800,980,9200)}if((d>>>e|0)==0){g=a+4|0;j=c[g>>2]<<e|d;c[g>>2]=j;d=a+8|0;e=(c[d>>2]|0)+e|0;j=(ve(j<<32-e|0)|0)&65535;g=a+20|0;h=c[g>>2]|0;b[h>>1]=j;c[g>>2]=h+(e>>>3&2)&c[a+12>>2];c[d>>2]=e&15;i=f;return}else{pa(9216,8800,981,9200)}}function Qd(a,d,e){a=a|0;d=d|0;e=e|0;var f=0,g=0,h=0,j=0,k=0,l=0,m=0;f=i;if(!((e|0)>-1&e>>>0<33)){pa(9136,8800,1002,9256)}if(e>>>0>16){e=e+ -16|0;h=a+4|0;k=c[h>>2]<<16|d>>>e&65535;c[h>>2]=k;h=a+8|0;m=c[h>>2]|0;j=m+16|0;m=(ve(k<<16-m|0)|0)&65535;k=a+20|0;l=c[k>>2]|0;b[l>>1]=m;c[k>>2]=l+(j>>>3&2)&c[a+12>>2];c[h>>2]=j&15;if(e>>>0<17){g=e}else{pa(9184,8800,994,9240)}}else{g=e}d=~(-1<<g)&d;if((d>>>g|0)==0){m=a+4|0;h=c[m>>2]<<g|d;c[m>>2]=h;m=a+8|0;l=(c[m>>2]|0)+g|0;h=(ve(h<<32-l|0)|0)&65535;k=a+20|0;j=c[k>>2]|0;b[j>>1]=h;c[k>>2]=j+(l>>>3&2)&c[a+12>>2];c[m>>2]=l&15;i=f;return}else{pa(9216,8800,981,9200)}}function Rd(a){a=a|0;var d=0,e=0,f=0,g=0,h=0,j=0;d=i;e=a+8|0;g=c[e>>2]|0;f=0-g&7;h=a+4|0;j=c[h>>2]<<f;c[h>>2]=j;f=g+f|0;j=(ve(j<<32-f|0)|0)&65535;g=a+20|0;h=c[g>>2]|0;b[h>>1]=j;c[g>>2]=h+(f>>>3&2)&c[a+12>>2];c[e>>2]=f&15;i=d;return}function Sd(a,b){a=a|0;b=b|0;var d=0,e=0,f=0;a=i;d=b+16|0;if(((c[b+20>>2]^c[d>>2])&4096|0)==0){i=a;return 0}f=c[b+24>>2]|0;e=b+28|0;Ya[c[f+36>>2]&31](f,c[e>>2]|0)|0;Xa[c[f+28>>2]&31](f,c[d>>2]|0,4096)|0;c[e>>2]=(c[e>>2]|0)+4096;e=c[d>>2]|0;c[b>>2]=c[e>>2];c[d>>2]=e+4096&c[b+12>>2];i=a;return 0}function Td(a,b){a=a|0;b=b|0;var d=0,e=0,f=0;a=i;d=b+16|0;e=c[d>>2]|0;if(((c[b+20>>2]^e)&4096|0)==0){e=0;i=a;return e|0}f=c[b+24>>2]|0;e=Xa[c[f+32>>2]&31](f,e,4096)|0;if((e|0)<0){f=e;i=a;return f|0}c[d>>2]=(c[d>>2]|0)+4096&c[b+12>>2];f=e;i=a;return f|0}function Ud(a){a=a|0;var b=0,d=0;b=c[a+20>>2]|0;d=c[a+16>>2]|0;return((c[a+8>>2]|0)>>>3)-d+(b+(d>>>0<=b>>>0?0:8192))|0}function Vd(a){a=a|0;return(c[a+20>>2]|0)-((c[a+16>>2]|0)+8192)+((c[a+8>>2]|0)>>>3)+(c[a+28>>2]|0)|0}function Wd(a,b,e){a=a|0;b=b|0;e=e|0;var f=0,g=0,h=0;e=i;g=a+28|0;Ya[c[b+40>>2]&31](b,g)|0;h=a+ -8192|0;f=a+16|0;c[f>>2]=h;c[a+20>>2]=h;Ya[c[b+36>>2]&31](b,c[g>>2]|0)|0;Xa[c[b+28>>2]&31](b,c[f>>2]|0,8192)|0;c[g>>2]=(c[g>>2]|0)+8192;f=c[f>>2]|0;c[a+4>>2]=(d[f+2>>0]|0)<<8|(d[f+3>>0]|0)|(d[f+1>>0]|0)<<16|(d[f>>0]|0)<<24;c[a+8>>2]=0;c[a+12>>2]=-8194;c[a+24>>2]=b;i=e;return 0}function Xd(a,b){a=a|0;b=b|0;var e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0;e=i;a=b+24|0;h=c[a>>2]|0;g=b+8|0;j=c[g>>2]|0;f=b+12|0;k=c[f>>2]|0;if((k&1|0)!=0){pa(9112,8800,908,9096)}n=(0-j&7)+j|0;j=b+20|0;o=(c[j>>2]|0)+(n>>>3)&k;m=o;c[j>>2]=m;n=n&15;c[g>>2]=n;c[b+4>>2]=((d[m+2>>0]|0)<<8|(d[m+3>>0]|0)|(d[m+1>>0]|0)<<16|(d[m>>0]|0)<<24)<<n;k=b+16|0;l=c[k>>2]|0;if(((o^l)&4096|0)==0){k=h+36|0;j=l;o=n;n=m;l=c[b+28>>2]|0;m=j+8192|0;o=o>>>3;o=n+o|0;n=c[k>>2]|0;m=l-m|0;o=m+o|0;Ya[n&31](h,o)|0;c[a>>2]=0;i=e;return 0}else{n=h+36|0;m=b+28|0;Ya[c[n>>2]&31](h,c[m>>2]|0)|0;Xa[c[h+28>>2]&31](h,c[k>>2]|0,4096)|0;l=(c[m>>2]|0)+4096|0;c[m>>2]=l;m=c[k>>2]|0;c[b>>2]=c[m>>2];m=m+4096&c[f>>2];c[k>>2]=m;o=c[g>>2]|0;k=c[j>>2]|0;m=m+8192|0;o=o>>>3;o=k+o|0;n=c[n>>2]|0;m=l-m|0;o=m+o|0;Ya[n&31](h,o)|0;c[a>>2]=0;i=e;return 0}return 0}function Yd(a,b){a=a|0;b=b|0;var d=0,e=0;d=i;Ya[c[b+40>>2]&31](b,a+28|0)|0;e=a+ -8192|0;c[a+16>>2]=e;c[a+20>>2]=e;c[a+4>>2]=0;c[a+8>>2]=0;c[a+12>>2]=-8193;c[a+24>>2]=b;i=d;return 0}function Zd(b,d,e){b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0;f=i;h=a[b>>0]|0;g=h&255;if(h<<24>>24==0){c[b+4>>2]=1;c[b+8>>2]=0;c[b+16>>2]=0;c[b+12>>2]=0;i=f;return}if((e|0)!=0){if(!((h&255)<16)){d=d+ -1+(g>>>4)|0;g=g&15|16}e=g<<d;c[b+4>>2]=e;c[b+12>>2]=c[9272+(g<<3)>>2];c[b+16>>2]=(c[9276+(g<<3)>>2]|0)+d;c[b+8>>2]=(e*3|0)+1>>3;i=f;return}do{if(!((h&255)<32)){d=g&15|16;if((h&255)<48){h=(g>>>4)+ -2|0;d=(d+1|0)>>>1;break}else{h=(g>>>4)+ -3|0;break}}else{h=0;d=(g+3|0)>>>2}}while(0);e=d<<h;c[b+4>>2]=e;c[b+12>>2]=c[9272+(d<<3)>>2];c[b+16>>2]=(c[9276+(d<<3)>>2]|0)+h;c[b+8>>2]=(e*3|0)+1>>3;i=f;return}function _d(a){a=a|0;var b=0,d=0,e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0;b=i;f=c[a+34332>>2]|0;e=c[a+34156>>2]|0;if((aa(e*80|0,f>>>16)|0)>>>0>65535){m=-1;i=b;return m|0}l=ke(aa(f*80|0,e)|0)|0;if((l|0)==0){m=-1;i=b;return m|0}c[a+34916>>2]=l;if((e|0)==0){m=0;i=b;return m|0}d=f<<1;if((f|0)==0){f=0;while(1){c[a+(f<<2)+34788>>2]=l;c[a+(f<<2)+34852>>2]=l;f=f+1|0;if((f|0)==(e|0)){a=0;break}else{l=l+(d*40|0)|0}}i=b;return a|0}else{g=0}while(1){j=a+(g<<2)+34788|0;c[j>>2]=l;k=a+(g<<2)+34852|0;c[k>>2]=l+(f*40|0);h=l+(d*40|0)|0;m=0;while(1){c[l+(m*40|0)+36>>2]=l+(m*40|0)+12;l=c[k>>2]|0;c[l+(m*40|0)+36>>2]=l+(m*40|0)+12;m=m+1|0;if((m|0)==(f|0)){break}l=c[j>>2]|0}g=g+1|0;if((g|0)==(e|0)){a=0;break}else{l=h}}i=b;return a|0}function $d(a){a=a|0;var b=0,d=0;b=i;a=a+34916|0;d=c[a>>2]|0;if((d|0)!=0){le(d)}c[a>>2]=0;i=b;return}function ae(a,b){a=a|0;b=b|0;var d=0,e=0,f=0,g=0,h=0,j=0,k=0;d=i;f=c[a+4>>2]|0;e=c[a+8>>2]|0;g=c[a+12>>2]|0;f=((e|0)>-1?e:0-e|0)+((f|0)>-1?f:0-f|0)+((g|0)>-1?g:0-g|0)|0;h=c[a+16>>2]|0;e=c[a+32>>2]|0;g=c[a+48>>2]|0;e=((e|0)>-1?e:0-e|0)+((h|0)>-1?h:0-h|0)+((g|0)>-1?g:0-g|0)|0;do{if(!((b|0)==0|(b|0)==6)){g=c[a+68>>2]|0;h=c[a+132>>2]|0;f=((g|0)>-1?g:0-g|0)+f+((h|0)>-1?h:0-h|0)|0;if((b|0)==1){b=c[a+72>>2]|0;a=c[a+136>>2]|0;e=((b|0)>-1?b:0-b|0)+e+((a|0)>-1?a:0-a|0)|0;break}else if((b|0)==2){b=c[a+72>>2]|0;g=c[a+136>>2]|0;h=c[a+88>>2]|0;j=c[a+152>>2]|0;k=c[a+84>>2]|0;a=c[a+148>>2]|0;f=((k|0)>-1?k:0-k|0)+f+((a|0)>-1?a:0-a|0)|0;e=((b|0)>-1?b:0-b|0)+e+((g|0)>-1?g:0-g|0)+((h|0)>-1?h:0-h|0)+((j|0)>-1?j:0-j|0)|0;break}else{b=c[a+80>>2]|0;a=c[a+144>>2]|0;e=((b|0)>-1?b:0-b|0)+e+((a|0)>-1?a:0-a|0)|0;break}}}while(0);if((f<<2|0)<(e|0)){k=1;i=d;return k|0}k=(e<<2|0)<(f|0)?0:2;i=d;return k|0}function be(a,b){a=a|0;b=b|0;var e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0;e=i;f=(c[a+34280>>2]|0)==0;do{if((c[a+34276>>2]|0)!=0){if(f){h=c[a+34852>>2]|0;break}else{r=8;s=3;s=r|s;i=e;return s|0}}else{if(f){n=c[a+34128>>2]|0;g=b+ -1|0;f=c[a+34788>>2]|0;m=c[f+(g*40|0)+8>>2]|0;h=c[a+34852>>2]|0;l=c[h+(b*40|0)+8>>2]|0;p=c[h+(g*40|0)+8>>2]|0;if((n|0)==0|(n|0)==6){j=p-m|0;k=p-l|0;m=(j|0)>-1?j:0-j|0;j=(k|0)>-1?k:0-k|0}else{k=c[a+34856>>2]|0;j=c[a+34860>>2]|0;if((n|0)==1){q=8}else{q=(n|0)==2?4:2}m=p-m|0;n=aa(q,(m|0)>-1?m:0-m|0)|0;s=c[k+(g*40|0)+8>>2]|0;m=s-(c[(c[a+34792>>2]|0)+(g*40|0)+8>>2]|0)|0;r=c[j+(g*40|0)+8>>2]|0;o=r-(c[(c[a+34796>>2]|0)+(g*40|0)+8>>2]|0)|0;l=p-l|0;l=aa(q,(l|0)>-1?l:0-l|0)|0;k=s-(c[k+(b*40|0)+8>>2]|0)|0;j=r-(c[j+(b*40|0)+8>>2]|0)|0;m=((m|0)>-1?m:0-m|0)+n+((o|0)>-1?o:0-o|0)|0;j=((k|0)>-1?k:0-k|0)+l+((j|0)>-1?j:0-j|0)|0}if((m<<2|0)<(j|0)){break}b=(j<<2|0)<(m|0);if(!b){a=8;s=b?0:2;s=a|s;i=e;return s|0}}else{g=b+ -1|0;f=c[a+34788>>2]|0}r=(d[a+34116>>0]|0|0)==(c[f+(g*40|0)>>2]|0)?0:8;s=0;s=r|s;i=e;return s|0}}while(0);r=(d[a+34116>>0]|0|0)==(c[h+(b*40|0)>>2]|0)?4:8;s=1;s=r|s;i=e;return s|0}function ce(a,b,e,f){a=a|0;b=b|0;e=e|0;f=f|0;var g=0,h=0,j=0,k=0,l=0;g=i;if(!((f+ -1|0)>>>0<2)){h=c[a+34156>>2]|0;if((h|0)>0){j=3}}else{h=1;j=3}if((j|0)==3){j=b+1156|0;k=0;do{l=c[a+(k<<2)+34788>>2]|0;c[l+(e*40|0)+8>>2]=c[b+(k<<6)>>2];c[l+(e*40|0)>>2]=d[j>>0]|0;l=c[l+(e*40|0)+36>>2]|0;c[l>>2]=c[b+(k<<6)+4>>2];c[l+4>>2]=c[b+(k<<6)+8>>2];c[l+8>>2]=c[b+(k<<6)+12>>2];c[l+12>>2]=c[b+(k<<6)+16>>2];c[l+16>>2]=c[b+(k<<6)+32>>2];c[l+20>>2]=c[b+(k<<6)+48>>2];k=k+1|0}while((k|0)!=(h|0))}if((f|0)==2){k=b+1156|0;l=c[a+34792>>2]|0;c[l+(e*40|0)>>2]=d[k>>0]|0;c[l+(e*40|0)+8>>2]=c[b+64>>2];l=c[l+(e*40|0)+36>>2]|0;c[l>>2]=c[b+68>>2];c[l+4>>2]=c[b+72>>2];c[l+8>>2]=c[b+84>>2];c[l+12>>2]=c[b+88>>2];c[l+16>>2]=c[b+80>>2];l=c[a+34796>>2]|0;c[l+(e*40|0)>>2]=d[k>>0]|0;c[l+(e*40|0)+8>>2]=c[b+128>>2];l=c[l+(e*40|0)+36>>2]|0;c[l>>2]=c[b+132>>2];c[l+4>>2]=c[b+136>>2];c[l+8>>2]=c[b+148>>2];c[l+12>>2]=c[b+152>>2];c[l+16>>2]=c[b+144>>2];i=g;return}else if((f|0)==1){k=b+1156|0;l=c[a+34792>>2]|0;c[l+(e*40|0)+8>>2]=c[b+64>>2];c[l+(e*40|0)>>2]=d[k>>0]|0;l=c[l+(e*40|0)+36>>2]|0;c[l>>2]=c[b+68>>2];c[l+4>>2]=c[b+72>>2];l=c[a+34796>>2]|0;c[l+(e*40|0)+8>>2]=c[b+128>>2];c[l+(e*40|0)>>2]=d[k>>0]|0;l=c[l+(e*40|0)+36>>2]|0;c[l>>2]=c[b+132>>2];c[l+4>>2]=c[b+136>>2];i=g;return}else{i=g;return}}function de(a,b,d,e){a=a|0;b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0,i=0,j=0;f=c[d>>2]|0;g=c[e>>2]|0;i=g+(c[a>>2]|0)|0;h=(c[b>>2]|0)-f|0;j=i-h>>1;g=j-g|0;f=j-f|0;c[a>>2]=i-f;c[b>>2]=g+h;c[d>>2]=g;c[e>>2]=f;return}function ee(a,b,d,e){a=a|0;b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0,i=0,j=0;f=c[d>>2]|0;g=c[e>>2]|0;i=g+(c[a>>2]|0)|0;h=(c[b>>2]|0)-f|0;j=1-h+i>>1;g=j-g|0;f=j-f|0;c[a>>2]=i-f;c[b>>2]=g+h;c[d>>2]=g;c[e>>2]=f;return}function fe(a){a=a|0;var b=0,d=0,e=0,f=0,g=0,h=0,i=0,j=0,k=0;e=a+16|0;d=a+32|0;j=a+48|0;f=c[d>>2]|0;g=c[j>>2]|0;i=g+(c[a>>2]|0)|0;h=(c[e>>2]|0)-f|0;b=i-h>>1;g=b-g|0;f=b-f|0;c[a>>2]=i-f;c[e>>2]=g+h;c[d>>2]=g;c[j>>2]=f;j=a+4|0;f=a+20|0;d=a+36|0;g=a+52|0;e=c[d>>2]|0;h=c[g>>2]|0;i=h+(c[j>>2]|0)|0;b=(c[f>>2]|0)-e|0;k=i-b>>1;h=k-h|0;e=k-e|0;c[j>>2]=i-e;c[f>>2]=h+b;c[d>>2]=h;c[g>>2]=e;g=a+8|0;e=a+24|0;d=a+40|0;h=a+56|0;f=c[d>>2]|0;b=c[h>>2]|0;j=b+(c[g>>2]|0)|0;i=(c[e>>2]|0)-f|0;k=j-i>>1;b=k-b|0;f=k-f|0;c[g>>2]=j-f;c[e>>2]=b+i;c[d>>2]=b;c[h>>2]=f;h=a+12|0;f=a+28|0;d=a+44|0;a=a+60|0;b=c[d>>2]|0;e=c[a>>2]|0;i=e+(c[h>>2]|0)|0;g=(c[f>>2]|0)-b|0;j=i-g>>1;e=j-e|0;b=j-b|0;c[h>>2]=i-b;c[f>>2]=e+g;c[d>>2]=e;c[a>>2]=b;return}function ge(b,d,e){b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0,j=0;f=i;g=d&255;j=(e|0)==0;a:do{if((b&3|0)==0|j){h=e;e=5}else{h=d&255;while(1){if((a[b>>0]|0)==h<<24>>24){h=e;e=6;break a}b=b+1|0;e=e+ -1|0;j=(e|0)==0;if((b&3|0)==0|j){h=e;e=5;break}}}}while(0);if((e|0)==5){if(j){h=0}else{e=6}}b:do{if((e|0)==6){d=d&255;if(!((a[b>>0]|0)==d<<24>>24)){g=aa(g,16843009)|0;c:do{if(h>>>0>3){do{j=c[b>>2]^g;if(((j&-2139062144^-2139062144)&j+ -16843009|0)!=0){break c}b=b+4|0;h=h+ -4|0}while(h>>>0>3)}}while(0);if((h|0)==0){h=0}else{while(1){if((a[b>>0]|0)==d<<24>>24){break b}b=b+1|0;h=h+ -1|0;if((h|0)==0){h=0;break}}}}}}while(0);i=f;return((h|0)!=0?b:0)|0}function he(b,c){b=b|0;c=c|0;var d=0;d=i;b=ie(b,c)|0;i=d;return((a[b>>0]|0)==(c&255)<<24>>24?b:0)|0}function ie(b,d){b=b|0;d=d|0;var e=0,f=0,g=0,h=0,j=0;e=i;f=d&255;if((f|0)==0){j=b+(se(b|0)|0)|0;i=e;return j|0}a:do{if((b&3|0)!=0){g=d&255;while(1){h=a[b>>0]|0;if(h<<24>>24==0){g=b;h=13;break}j=b+1|0;if(h<<24>>24==g<<24>>24){g=b;h=13;break}if((j&3|0)==0){g=j;break a}else{b=j}}if((h|0)==13){i=e;return g|0}}else{g=b}}while(0);f=aa(f,16843009)|0;b=c[g>>2]|0;b:do{if(((b&-2139062144^-2139062144)&b+ -16843009|0)==0){while(1){j=b^f;h=g+4|0;if(((j&-2139062144^-2139062144)&j+ -16843009|0)!=0){break b}b=c[h>>2]|0;if(((b&-2139062144^-2139062144)&b+ -16843009|0)==0){g=h}else{g=h;break}}}}while(0);d=d&255;while(1){j=a[g>>0]|0;if(j<<24>>24==0|j<<24>>24==d<<24>>24){break}else{g=g+1|0}}i=e;return g|0}function je(b,e){b=b|0;e=e|0;var f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0;f=i;i=i+1056|0;k=f+1024|0;g=f;n=a[e>>0]|0;if(n<<24>>24==0){x=b;i=f;return x|0}b=he(b,n<<24>>24)|0;if((b|0)==0){x=0;i=f;return x|0}q=a[e+1>>0]|0;if(q<<24>>24==0){x=b;i=f;return x|0}o=b+1|0;m=a[o>>0]|0;if(m<<24>>24==0){x=0;i=f;return x|0}s=a[e+2>>0]|0;if(s<<24>>24==0){g=q&255|(n&255)<<8;h=m;e=d[b>>0]<<8|m&255;while(1){j=e&65535;if((j|0)==(g|0)){break}o=o+1|0;e=a[o>>0]|0;if(e<<24>>24==0){h=0;break}else{h=e;e=e&255|j<<8}}x=h<<24>>24==0?0:o+ -1|0;i=f;return x|0}t=b+2|0;o=a[t>>0]|0;if(o<<24>>24==0){x=0;i=f;return x|0}v=a[e+3>>0]|0;if(v<<24>>24==0){g=(q&255)<<16|(n&255)<<24|(s&255)<<8;e=(o&255)<<8|(m&255)<<16|d[b>>0]<<24;if((e|0)==(g|0)){h=0}else{do{t=t+1|0;h=a[t>>0]|0;e=(h&255|e)<<8;h=h<<24>>24==0}while(!(h|(e|0)==(g|0)))}x=h?0:t+ -2|0;i=f;return x|0}t=b+3|0;u=a[t>>0]|0;if(u<<24>>24==0){x=0;i=f;return x|0}if((a[e+4>>0]|0)==0){g=(q&255)<<16|(n&255)<<24|(s&255)<<8|v&255;h=(o&255)<<8|(m&255)<<16|u&255|d[b>>0]<<24;if((h|0)==(g|0)){e=0}else{do{t=t+1|0;e=a[t>>0]|0;h=e&255|h<<8;e=e<<24>>24==0}while(!(e|(h|0)==(g|0)))}x=e?0:t+ -3|0;i=f;return x|0}c[k+0>>2]=0;c[k+4>>2]=0;c[k+8>>2]=0;c[k+12>>2]=0;c[k+16>>2]=0;c[k+20>>2]=0;c[k+24>>2]=0;c[k+28>>2]=0;m=0;while(1){if((a[b+m>>0]|0)==0){r=0;p=80;break}x=n&255;h=k+(x>>>5<<2)|0;c[h>>2]=c[h>>2]|1<<(x&31);h=m+1|0;c[g+(x<<2)>>2]=h;n=a[e+h>>0]|0;if(n<<24>>24==0){break}else{m=h}}if((p|0)==80){i=f;return r|0}a:do{if(h>>>0>1){r=1;o=-1;p=0;b:while(1){n=1;while(1){q=r;c:while(1){r=1;while(1){s=a[e+(r+o)>>0]|0;t=a[e+q>>0]|0;if(!(s<<24>>24==t<<24>>24)){break c}s=r+1|0;if((r|0)==(n|0)){break}q=s+p|0;if(q>>>0<h>>>0){r=s}else{break b}}p=p+n|0;q=p+1|0;if(!(q>>>0<h>>>0)){break b}}n=q-o|0;if(!((s&255)>(t&255))){break}r=q+1|0;if(r>>>0<h>>>0){p=q}else{break b}}r=p+2|0;if(r>>>0<h>>>0){o=p;p=p+1|0}else{o=p;n=1;break}}t=1;p=-1;r=0;while(1){q=1;while(1){s=t;d:while(1){v=1;while(1){u=a[e+(v+p)>>0]|0;t=a[e+s>>0]|0;if(!(u<<24>>24==t<<24>>24)){break d}t=v+1|0;if((v|0)==(q|0)){break}s=t+r|0;if(s>>>0<h>>>0){v=t}else{r=p;break a}}r=r+q|0;s=r+1|0;if(!(s>>>0<h>>>0)){r=p;break a}}q=s-p|0;if(!((u&255)<(t&255))){break}t=s+1|0;if(t>>>0<h>>>0){r=s}else{r=p;break a}}t=r+2|0;if(t>>>0<h>>>0){p=r;r=r+1|0}else{q=1;break}}}else{o=-1;r=-1;n=1;q=1}}while(0);s=(r+1|0)>>>0>(o+1|0)>>>0;p=s?q:n;o=s?r:o;n=o+1|0;if((ne(e,e+p|0,n)|0)==0){s=h-p|0;t=h|63;if((h|0)!=(p|0)){r=b;q=0;u=b;e:while(1){v=r;do{if((u-v|0)>>>0<h>>>0){w=ge(u,0,t)|0;if((w|0)!=0){if((w-v|0)>>>0<h>>>0){r=0;p=80;break e}else{u=w;break}}else{u=u+t|0;break}}}while(0);v=d[r+m>>0]|0;if((1<<(v&31)&c[k+(v>>>5<<2)>>2]|0)==0){r=r+h|0;q=0;continue}x=c[g+(v<<2)>>2]|0;v=h-x|0;if((h|0)!=(x|0)){r=r+((q|0)!=0&v>>>0<p>>>0?s:v)|0;q=0;continue}x=n>>>0>q>>>0?n:q;v=a[e+x>>0]|0;f:do{if(v<<24>>24==0){w=n}else{while(1){w=x+1|0;if(!(v<<24>>24==(a[r+x>>0]|0))){break}v=a[e+w>>0]|0;if(v<<24>>24==0){w=n;break f}else{x=w}}r=r+(x-o)|0;q=0;continue e}}while(0);while(1){if(!(w>>>0>q>>>0)){break}v=w+ -1|0;if((a[e+v>>0]|0)==(a[r+v>>0]|0)){w=v}else{break}}if((w|0)==(q|0)){p=80;break}r=r+p|0;q=s}if((p|0)==80){i=f;return r|0}}else{j=t;l=h}}else{l=h-o+ -1|0;j=h|63;l=(o>>>0>l>>>0?o:l)+1|0}p=e+n|0;r=b;g:while(1){q=r;do{if((b-q|0)>>>0<h>>>0){s=ge(b,0,j)|0;if((s|0)!=0){if((s-q|0)>>>0<h>>>0){r=0;p=80;break g}else{b=s;break}}else{b=b+j|0;break}}}while(0);q=d[r+m>>0]|0;if((1<<(q&31)&c[k+(q>>>5<<2)>>2]|0)==0){r=r+h|0;continue}q=c[g+(q<<2)>>2]|0;if((h|0)!=(q|0)){r=r+(h-q)|0;continue}s=a[p>>0]|0;h:do{if(s<<24>>24==0){q=n}else{t=n;while(1){q=t+1|0;if(!(s<<24>>24==(a[r+t>>0]|0))){break}s=a[e+q>>0]|0;if(s<<24>>24==0){q=n;break h}else{t=q}}r=r+(t-o)|0;continue g}}while(0);do{if((q|0)==0){p=80;break g}q=q+ -1|0}while((a[e+q>>0]|0)==(a[r+q>>0]|0));r=r+l|0}if((p|0)==80){i=f;return r|0}return 0}



function $b(e,f,g,h){e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0,Y=0;k=i;i=i+272|0;p=k+264|0;o=k+136|0;g=k;s=k+72|0;m=k+8|0;h=c[e+34128>>2]|0;z=e+34156|0;j=c[z>>2]|0;t=(h|0)==1;r=(h|0)==2;w=h+ -1|0;x=w>>>0<2;q=x?2:j;n=c[f+4>>2]|0;l=f+512|0;u=f+520|0;L=c[u>>2]|0;X=g;c[X>>2]=0;c[X+4>>2]=0;Sd(e,n)|0;if((c[e+104>>2]|0)!=0?(y=a[(c[e+34292>>2]|0)+((c[e+34272>>2]|0)*240|0)+194>>0]|0,!(y<<24>>24==0)):0){B=n+4|0;A=c[n+12>>2]|0;if((A&1|0)!=0){pa(2824,2392,85,2808)}X=c[B>>2]|0;U=n+8|0;W=(c[U>>2]|0)+1|0;T=n+20|0;V=(c[T>>2]|0)+(W>>>3)&A;c[T>>2]=V;W=W&15;c[U>>2]=W;c[B>>2]=((d[V+2>>0]|0)<<8|(d[V+3>>0]|0)|(d[V+1>>0]|0)<<16|(d[V>>0]|0)<<24)<<W;if((X|0)>-1){y=0}else{y=(_b(n,y&255)|0)+1&255}a[e+34116>>0]=y}y=c[z>>2]|0;if((y|0)>0){z=0;do{c[s+((z&15)<<2)>>2]=e+(z<<6)+32960;z=z+1|0}while((z|0)<(y|0))}if((c[e+34284>>2]|0)!=0){c[f+108>>2]=32767;c[f+116>>2]=32;c[f+124>>2]=30;c[f+132>>2]=28;c[f+140>>2]=26;c[f+148>>2]=24;c[f+156>>2]=22;c[f+164>>2]=20;c[f+172>>2]=18;c[f+180>>2]=16;c[f+188>>2]=14;c[f+196>>2]=12;c[f+204>>2]=10;c[f+212>>2]=8;c[f+220>>2]=6;c[f+228>>2]=4}a:do{if(!(w>>>0<3)){if((j|0)>0){w=n+4|0;v=n+8|0;y=n+20|0;z=c[n+12>>2]|0;A=(z&1|0)==0;B=0;C=0;while(1){if(!A){break}X=(c[w>>2]|0)>>>31;W=(c[v>>2]|0)+1|0;V=(c[y>>2]|0)+(W>>>3)&z;c[y>>2]=V;W=W&15;c[v>>2]=W;c[w>>2]=((d[V+2>>0]|0)<<8|(d[V+3>>0]|0)|(d[V+1>>0]|0)<<16|(d[V>>0]|0)<<24)<<W;B=X<<C|B;C=C+1|0;if((C|0)<(j|0)){A=1}else{J=B;break a}}pa(2824,2392,85,2808)}else{J=0}}else{y=f+556|0;B=c[y>>2]|0;w=f+552|0;z=c[w>>2]|0;A=(q<<2)+ -5|0;if((z|0)<1|(B|0)<0){C=n+4|0;E=c[C>>2]|0;D=n+8|0;F=(c[D>>2]|0)+1|0;c[D>>2]=F;if(F>>>0<16){F=E<<1}else{W=n+20|0;X=(c[W>>2]|0)+(F>>>3)&c[n+12>>2];c[W>>2]=X;F=F&15;c[D>>2]=F;F=((d[X+2>>0]|0)<<8|(d[X+3>>0]|0)|(d[X+1>>0]|0)<<16|(d[X>>0]|0)<<24)<<F}c[C>>2]=F;do{if(!((E|0)>-1)){F=_b(n,q+ -1|0)|0;if((F|0)!=0){E=c[n+12>>2]|0;if((E&1|0)==0){v=(c[C>>2]|0)>>>31;X=(c[D>>2]|0)+1|0;V=n+20|0;W=(c[V>>2]|0)+(X>>>3)&E;c[V>>2]=W;X=X&15;c[D>>2]=X;c[C>>2]=((d[W+2>>0]|0)<<8|(d[W+3>>0]|0)|(d[W+1>>0]|0)<<16|(d[W>>0]|0)<<24)<<X;v=v|F<<1;break}else{pa(2824,2392,85,2808)}}else{v=1}}else{v=0}}while(0);J=(B|0)<(z|0)?A-v|0:v}else{J=_b(n,q)|0}A=1-(((J|0)==(A|0))<<2)+B|0;v=1-(((J|0)==0)<<2)+z|0;if((A|0)<-8){z=-8}else{z=(A|0)>7?7:A}c[y>>2]=z;if((v|0)<-8){v=-8}else{v=(v|0)>7?7:v}c[w>>2]=v}}while(0);if((c[u>>2]|0)<=14?(c[f+524>>2]|0)<=14:0){w=18}else{w=19}b:do{if((q|0)>0){u=g+4|0;v=f+524|0;I=x^1;D=n+4|0;B=n+12|0;z=n+8|0;A=n+20|0;C=t?4:8;E=s+4|0;x=s+8|0;y=f+24|0;H=o+4|0;F=t&1;G=t?6:14;K=0;N=g;c:while(1){M=c[s+(K<<2)>>2]|0;d:do{if((J&1|0)!=0){if(t){O=(K|0)==1?10:1}else{O=1}P=O+(r&(K|0)==1&1)|0;O=(K|0)>0?3:0;Q=c[f+(O+5<<2)+24>>2]|0;R=c[Q+20>>2]|0;X=b[R+((Ld(n,5)|0)<<1)>>1]|0;S=X<<16>>16;Md(n,X<<16>>16<0?5:S&7)|0;T=S>>3;if((T|0)<0){T=S;do{X=b[R+(T+32768+(cd(n,1)|0)<<1)>>1]|0;T=X<<16>>16}while(X<<16>>16<0)}S=Q+28|0;c[S>>2]=(c[S>>2]|0)+(c[(c[Q+8>>2]|0)+(T<<2)>>2]|0);S=Q+32|0;c[S>>2]=(c[S>>2]|0)+(c[(c[Q+12>>2]|0)+(T<<2)>>2]|0);c[p>>2]=T;S=T&1;Q=T>>2;R=S&Q;W=c[D>>2]|0;U=W>>31;V=(c[z>>2]|0)+1|0;c[z>>2]=V;if(V>>>0<16){V=W<<1}else{X=(c[A>>2]|0)+(V>>>3)&c[B>>2];c[A>>2]=X;V=V&15;c[z>>2]=V;V=((d[X+2>>0]|0)<<8|(d[X+3>>0]|0)|(d[X+1>>0]|0)<<16|(d[X>>0]|0)<<24)<<V}c[D>>2]=V;if((T&2|0)==0){T=U|1}else{T=((cc(c[f+(R+11<<2)+24>>2]|0,n)|0)^U)-U|0}c[H>>2]=T;c[o>>2]=0;if((S|0)==0){S=Zb(15-P|0,c[y>>2]|0,n)|0;c[o>>2]=S}else{S=0}if((Q|0)==0){P=1}else{O=O+6|0;T=P+1+S|0;P=1;S=Q;do{Q=P<<1;U=o+(Q<<2)|0;c[U>>2]=0;if((S&1|0)==0){S=Zb(15-T|0,c[y>>2]|0,n)|0;c[U>>2]=S}else{S=0}T=T+1+S|0;gc(p,T,c[f+(O+R<<2)+24>>2]|0,n);U=c[p>>2]|0;S=U>>1;if(!(S>>>0<3)){m=57;break c}R=S&R;W=c[D>>2]|0;V=W>>31;X=(c[z>>2]|0)+1|0;c[z>>2]=X;if(X>>>0<16){W=W<<1}else{Y=(c[A>>2]|0)+(X>>>3)&c[B>>2];c[A>>2]=Y;W=X&15;c[z>>2]=W;W=((d[Y+2>>0]|0)<<8|(d[Y+3>>0]|0)|(d[Y+1>>0]|0)<<16|(d[Y>>0]|0)<<24)<<W}c[D>>2]=W;if((U&1|0)==0){c[o+((Q|1)<<2)>>2]=V|1}else{c[o+((Q|1)<<2)>>2]=((cc(c[f+(R+11<<2)+24>>2]|0,n)|0)^V)-V}P=P+1|0}while((S|0)!=0)}if(!((K|0)==0|I)){c[N>>2]=(c[N>>2]|0)+P;O=m+0|0;N=O+64|0;do{c[O>>2]=0;O=O+4|0}while((O|0)<(N|0));if((P|0)>0){N=0;O=0;while(1){Y=O<<1;N=(c[o+(Y<<2)>>2]|0)+N|0;c[m+((N&15)<<2)>>2]=c[o+((Y|1)<<2)>>2];O=O+1|0;if((O|0)==(P|0)){N=0;break}else{N=N+1|0}}}else{N=0}while(1){c[(c[s+((N&1)+1<<2)>>2]|0)+(c[2232+((N>>1)+F<<2)>>2]<<2)>>2]=c[m+(N<<2)>>2];N=N+1|0;if((N|0)>=(G|0)){break d}}}c[N>>2]=(c[N>>2]|0)+P;if((P|0)>0){O=1;N=0;while(1){Y=N<<1;Q=(c[o+(Y<<2)>>2]|0)+O|0;R=f+(Q<<3)+108|0;c[M+(c[f+(Q<<3)+112>>2]<<2)>>2]=c[o+((Y|1)<<2)>>2];Y=(c[R>>2]|0)+1|0;c[R>>2]=Y;O=f+(Q+ -1<<3)+108|0;if(Y>>>0>(c[O>>2]|0)>>>0){X=R;W=c[X>>2]|0;X=c[X+4>>2]|0;U=O;V=c[U+4>>2]|0;Y=R;c[Y>>2]=c[U>>2];c[Y+4>>2]=V;Y=O;c[Y>>2]=W;c[Y+4>>2]=X}N=N+1|0;if((N|0)==(P|0)){break}else{O=Q+1|0}}}}}while(0);e:do{if((L|0)!=0){if((K|0)==0|I){O=31-L|0;if((L|0)>-2&(L+1|0)>>>0<17){N=1}else{O=1;while(1){N=M+(O<<2)|0;P=c[N>>2]|0;if((P|0)>0){c[N>>2]=P<<L;P=Ya[w&31](n,L)|0;P=(c[N>>2]|0)+P|0}else{if((P|0)>=0){m=110;break c}c[N>>2]=P<<L;P=Ya[w&31](n,L)|0;P=(c[N>>2]|0)-P|0}c[N>>2]=P;O=O+1|0;if((O|0)>=16){break e}}}while(1){Q=M+(N<<2)|0;P=c[Q>>2]|0;do{if((P|0)>0){c[Q>>2]=P<<L;Y=Ya[w&31](n,L)|0;c[Q>>2]=(c[Q>>2]|0)+Y}else{if((P|0)<0){c[Q>>2]=P<<L;Y=Ya[w&31](n,L)|0;c[Q>>2]=(c[Q>>2]|0)-Y;break}X=(c[D>>2]|0)>>>O;Y=X&1;Y=(X>>1^0-Y)+Y|0;c[Q>>2]=Y;Q=((Y|0)!=0)+L|0;if(!((Q|0)>-1&Q>>>0<17)){m=111;break c}P=c[B>>2]|0;if((P&1|0)!=0){m=112;break c}Y=(c[z>>2]|0)+Q|0;X=(c[A>>2]|0)+(Y>>>3)&P;c[A>>2]=X;Y=Y&15;c[z>>2]=Y;c[D>>2]=((d[X+2>>0]|0)<<8|(d[X+3>>0]|0)|(d[X+1>>0]|0)<<16|(d[X>>0]|0)<<24)<<Y}}while(0);N=N+1|0;if((N|0)>=16){break e}}}N=c[E>>2]|0;M=1;do{O=N+(M<<2)|0;P=c[O>>2]|0;do{if((P|0)<=0){if((P|0)<0){c[O>>2]=P<<L;Y=Ya[w&31](n,L)|0;c[O>>2]=(c[O>>2]|0)-Y;break}Y=Ya[w&31](n,L)|0;c[O>>2]=Y;if((Y|0)!=0){P=c[D>>2]|0;Q=(c[z>>2]|0)+1|0;c[z>>2]=Q;if(Q>>>0<16){Q=P<<1}else{Y=(c[A>>2]|0)+(Q>>>3)&c[B>>2];c[A>>2]=Y;Q=Q&15;c[z>>2]=Q;Q=((d[Y+2>>0]|0)<<8|(d[Y+3>>0]|0)|(d[Y+1>>0]|0)<<16|(d[Y>>0]|0)<<24)<<Q}c[D>>2]=Q;if(!((P|0)>-1)){c[O>>2]=0-(c[O>>2]|0)}}}else{c[O>>2]=P<<L;Y=Ya[w&31](n,L)|0;c[O>>2]=(c[O>>2]|0)+Y}}while(0);O=(c[x>>2]|0)+(M<<2)|0;P=c[O>>2]|0;do{if((P|0)<=0){if((P|0)<0){c[O>>2]=P<<L;Y=Ya[w&31](n,L)|0;c[O>>2]=(c[O>>2]|0)-Y;break}Y=Ya[w&31](n,L)|0;c[O>>2]=Y;if((Y|0)!=0){P=c[D>>2]|0;Q=(c[z>>2]|0)+1|0;c[z>>2]=Q;if(Q>>>0<16){Q=P<<1}else{Y=(c[A>>2]|0)+(Q>>>3)&c[B>>2];c[A>>2]=Y;Q=Q&15;c[z>>2]=Q;Q=((d[Y+2>>0]|0)<<8|(d[Y+3>>0]|0)|(d[Y+1>>0]|0)<<16|(d[Y>>0]|0)<<24)<<Q}c[D>>2]=Q;if(!((P|0)>-1)){c[O>>2]=0-(c[O>>2]|0)}}}else{c[O>>2]=P<<L;Y=Ya[w&31](n,L)|0;c[O>>2]=(c[O>>2]|0)+Y}}while(0);M=M+1|0}while((M|0)<(C|0))}}while(0);K=K+1|0;if((K|0)<(q|0)){J=J>>1;L=c[v>>2]|0;N=u}else{break b}}if((m|0)==57){pa(2488,2392,477,2864)}else if((m|0)==110){pa(2776,2392,78,2848)}else if((m|0)==111){pa(2776,2392,85,2808)}else if((m|0)==112){pa(2824,2392,85,2808)}}}while(0);Zc(h,j,g,l);if((c[e+34288>>2]|0)==0){i=k;return 0}ac(f)|0;i=k;return 0}function ac(a){a=a|0;var b=0;b=i;Yc(c[a+24>>2]|0);Yc(c[a+28>>2]|0);Yc(c[a+32>>2]|0);Yc(c[a+36>>2]|0);Yc(c[a+40>>2]|0);Yc(c[a+44>>2]|0);Yc(c[a+48>>2]|0);Yc(c[a+52>>2]|0);Yc(c[a+56>>2]|0);Yc(c[a+60>>2]|0);Yc(c[a+64>>2]|0);Yc(c[a+68>>2]|0);Yc(c[a+72>>2]|0);i=b;return 0}function bc(e,f,g,h){e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0;g=i;i=i+16|0;n=g;v=c[e+34292>>2]|0;t=c[e+34272>>2]|0;h=c[e+34128>>2]|0;k=c[e+34156>>2]|0;o=c[f>>2]|0;l=n;c[l>>2]=0;c[l+4>>2]=0;l=f+532|0;p=c[f+540>>2]|0;s=(k|0)>0;if(s){re(e+32960|0,0,k<<6|0)|0}Sd(e,o)|0;r=e+34117|0;a[r>>0]=0;u=e+34116|0;a[u>>0]=0;m=e+104|0;if((c[m>>2]|0)==0?(w=e+116|0,x=c[w>>2]|0,(x|0)!=3):0){z=a[v+(t*240|0)+194>>0]|0;if(!(z<<24>>24==0)){A=o+4|0;y=c[o+12>>2]|0;if((y&1|0)!=0){pa(2824,2392,85,2808)}B=c[A>>2]|0;D=o+8|0;E=(c[D>>2]|0)+1|0;F=o+20|0;C=(c[F>>2]|0)+(E>>>3)&y;c[F>>2]=C;y=E&15;c[D>>2]=y;c[A>>2]=(d[C+2>>0]<<8|d[C+3>>0]|d[C+1>>0]<<16|d[C>>0]<<24)<<y;if((B|0)>-1){y=0}else{y=(_b(o,z&255)|0)+1&255;x=c[w>>2]|0}a[u>>0]=y}if((x|0)!=2?(q=a[v+(t*240|0)+195>>0]|0,!(q<<24>>24==0)):0){x=o+4|0;w=c[o+12>>2]|0;if((w&1|0)!=0){pa(2824,2392,85,2808)}F=c[x>>2]|0;C=o+8|0;E=(c[C>>2]|0)+1|0;B=o+20|0;D=(c[B>>2]|0)+(E>>>3)&w;c[B>>2]=D;E=E&15;c[C>>2]=E;c[x>>2]=(d[D+2>>0]<<8|d[D+3>>0]|d[D+1>>0]<<16|d[D>>0]<<24)<<E;if((F|0)>-1){q=0}else{q=(_b(o,q&255)|0)+1&255}a[r>>0]=q}}if((a[v+(t*240|0)+195>>0]|0)==0?(d[v+(t*240|0)+193>>0]|0)>1:0){a[r>>0]=a[u>>0]|0}if((d[u>>0]|0)>=(d[v+(t*240|0)+192>>0]|0)){F=-1;i=g;return F|0}if((d[r>>0]|0)>=(d[v+(t*240|0)+193>>0]|0)){F=-1;i=g;return F|0}if((h&-5|0)==0|(h|0)==6){if(s){t=o+4|0;u=o+8|0;w=n+4|0;q=f+544|0;r=o+20|0;s=o+12|0;v=f+36|0;x=0;z=n;while(1){y=c[t>>2]|0;A=(c[u>>2]|0)+1|0;c[u>>2]=A;if(A>>>0<16){A=y<<1}else{F=(c[r>>2]|0)+(A>>>3)&c[s>>2];c[r>>2]=F;A=A&15;c[u>>2]=A;A=(d[F+2>>0]<<8|d[F+3>>0]|d[F+1>>0]<<16|d[F>>0]<<24)<<A}c[t>>2]=A;if((y|0)>-1){y=0}else{y=(cc(c[v>>2]|0,o)|0)+ -1|0;c[z>>2]=(c[z>>2]|0)+1}if((p|0)==0){p=y}else{p=_b(o,p)|0|y<<p}if((p|0)==0){p=0}else{y=c[t>>2]|0;z=(c[u>>2]|0)+1|0;c[u>>2]=z;if(z>>>0<16){z=y<<1}else{F=(c[r>>2]|0)+(z>>>3)&c[s>>2];c[r>>2]=F;z=z&15;c[u>>2]=z;z=(d[F+2>>0]<<8|d[F+3>>0]|d[F+1>>0]<<16|d[F>>0]<<24)<<z}c[t>>2]=z;p=(y|0)>-1?p:0-p|0}c[e+(x<<6)+32960>>2]=p;x=x+1|0;if((x|0)==(k|0)){break}else{p=c[q>>2]|0;z=w}}}}else{q=c[(c[f+32>>2]|0)+20>>2]|0;F=b[q+((Ld(o,5)|0)<<1)>>1]|0;r=F<<16>>16;Md(o,F<<16>>16<0?5:r&7)|0;s=r>>3;if((s|0)<0){s=r;do{F=b[q+(s+32768+(cd(o,1)|0)<<1)>>1]|0;s=F<<16>>16}while(F<<16>>16<0)}r=s&2;q=s&1;if(s>>>0<4){s=0}else{s=(cc(c[f+36>>2]|0,o)|0)+ -1|0;c[n>>2]=(c[n>>2]|0)+1}if((p|0)!=0){s=_b(o,p)|0|s<<p}if((s|0)==0){p=0}else{t=o+4|0;p=c[t>>2]|0;v=o+8|0;u=(c[v>>2]|0)+1|0;c[v>>2]=u;if(u>>>0<16){u=p<<1}else{E=o+20|0;F=(c[E>>2]|0)+(u>>>3)&c[o+12>>2];c[E>>2]=F;u=u&15;c[v>>2]=u;u=(d[F+2>>0]<<8|d[F+3>>0]|d[F+1>>0]<<16|d[F>>0]<<24)<<u}c[t>>2]=u;p=(p|0)>-1?s:0-s|0}c[e+32960>>2]=p;s=n+4|0;p=c[f+544>>2]|0;if((r|0)==0){v=0}else{v=(cc(c[f+40>>2]|0,o)|0)+ -1|0;c[s>>2]=(c[s>>2]|0)+1}r=(p|0)!=0;if(r){v=_b(o,p)|0|v<<p}if((v|0)==0){t=0}else{t=o+4|0;u=c[t>>2]|0;x=o+8|0;w=(c[x>>2]|0)+1|0;c[x>>2]=w;if(w>>>0<16){w=u<<1}else{E=o+20|0;F=(c[E>>2]|0)+(w>>>3)&c[o+12>>2];c[E>>2]=F;w=w&15;c[x>>2]=w;w=(d[F+2>>0]<<8|d[F+3>>0]|d[F+1>>0]<<16|d[F>>0]<<24)<<w}c[t>>2]=w;t=(u|0)>-1?v:0-v|0}c[e+33024>>2]=t;if((q|0)==0){q=0}else{q=(cc(c[f+40>>2]|0,o)|0)+ -1|0;c[s>>2]=(c[s>>2]|0)+1}if(r){r=_b(o,p)|0|q<<p}else{r=q}if((r|0)==0){o=0}else{p=o+4|0;q=c[p>>2]|0;s=o+8|0;t=(c[s>>2]|0)+1|0;c[s>>2]=t;if(t>>>0<16){o=q<<1}else{E=o+20|0;F=(c[E>>2]|0)+(t>>>3)&c[o+12>>2];c[E>>2]=F;o=t&15;c[s>>2]=o;o=(d[F+2>>0]<<8|d[F+3>>0]|d[F+1>>0]<<16|d[F>>0]<<24)<<o}c[p>>2]=o;o=(q|0)>-1?r:0-r|0}c[e+33088>>2]=o}Zc(h,k,n,l);if(!((c[m>>2]|0)==1?!((c[(c[e+34232>>2]|0)+12>>2]|0)>>>0<16):0)){j=71}if((j|0)==71?(c[e+116>>2]|0)!=3:0){F=0;i=g;return F|0}if((c[e+34288>>2]|0)==0){F=0;i=g;return F|0}Yc(c[f+32>>2]|0);Yc(c[f+36>>2]|0);Yc(c[f+40>>2]|0);F=0;i=g;return F|0}function cc(a,e){a=a|0;e=e|0;var f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0;f=i;g=c[a+20>>2]|0;o=b[g+((Ld(e,5)|0)<<1)>>1]|0;h=o<<16>>16;Md(e,o<<16>>16<0?5:h&7)|0;j=h>>3;if((j|0)<0){do{o=b[g+(h+32768+(cd(e,1)|0)<<1)>>1]|0;h=o<<16>>16}while(o<<16>>16<0)}else{h=j}if(!(h>>>0<7)){pa(2728,2392,752,2744)}o=a+28|0;c[o>>2]=(c[o>>2]|0)+(c[(c[a+8>>2]|0)+(h<<2)>>2]|0);if(h>>>0<2){o=h+2|0;i=f;return o|0}if(h>>>0<6){o=c[2680+(h<<2)>>2]|0;o=(_b(e,c[2704+(h<<2)>>2]|0)|0)+o|0;i=f;return o|0}j=e+4|0;k=c[e+12>>2]|0;if((k&1|0)!=0){pa(2824,2392,85,2808)}n=(c[j>>2]|0)>>>28;a=e+8|0;h=c[a>>2]|0;m=h+4|0;g=e+20|0;o=(c[g>>2]|0)+(m>>>3)&k;c[g>>2]=o;m=m&15;c[a>>2]=m;l=((d[o+2>>0]|0)<<8|(d[o+3>>0]|0)|(d[o+1>>0]|0)<<16|(d[o>>0]|0)<<24)<<m;c[j>>2]=l;n=n+4|0;if((n|0)==19){m=o+((m+2|0)>>>3)&k;c[g>>2]=m;n=h+6&15;c[a>>2]=n;o=((d[m+2>>0]|0)<<8|(d[m+3>>0]|0)|(d[m+1>>0]|0)<<16|(d[m>>0]|0)<<24)<<n;c[j>>2]=o;l=(l>>>30)+19|0;if((l|0)==22){m=m+((n+3|0)>>>3)&k;c[g>>2]=m;n=h+9&15;c[a>>2]=n;c[j>>2]=((d[m+2>>0]|0)<<8|(d[m+3>>0]|0)|(d[m+1>>0]|0)<<16|(d[m>>0]|0)<<24)<<n;n=(o>>>29)+22|0}else{n=l}}o=(1<<n)+2+(Nd(e,n)|0)|0;i=f;return o|0}function dc(e,f,g,h){e=e|0;f=f|0;g=g|0;h=h|0;var j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0,Y=0,Z=0,_=0,$=0,ba=0,ca=0,da=0,ea=0,fa=0,ga=0,ha=0,ia=0,ja=0,ka=0,la=0,ma=0,na=0,oa=0,qa=0,ra=0;g=i;i=i+16|0;m=g+8|0;h=g;if((c[e+34284>>2]|0)!=0){c[f+364>>2]=32767;c[f+236>>2]=32767;c[f+372>>2]=32;c[f+244>>2]=32;c[f+380>>2]=30;c[f+252>>2]=30;c[f+388>>2]=28;c[f+260>>2]=28;c[f+396>>2]=26;c[f+268>>2]=26;c[f+404>>2]=24;c[f+276>>2]=24;c[f+412>>2]=22;c[f+284>>2]=22;c[f+420>>2]=20;c[f+292>>2]=20;c[f+428>>2]=18;c[f+300>>2]=18;c[f+436>>2]=16;c[f+308>>2]=16;c[f+444>>2]=14;c[f+316>>2]=14;c[f+452>>2]=12;c[f+324>>2]=12;c[f+460>>2]=10;c[f+332>>2]=10;c[f+468>>2]=8;c[f+340>>2]=8;c[f+476>>2]=6;c[f+348>>2]=6;c[f+484>>2]=4;c[f+356>>2]=4}l=e+104|0;n=e+34272|0;p=c[n>>2]|0;o=e+34292|0;q=c[o>>2]|0;s=a[q+(p*240|0)+195>>0]|0;t=s<<24>>24==0;if((c[l>>2]|0)==0){if(t){r=11}}else{if(!t){u=c[f+8>>2]|0;t=u+4|0;v=c[u+12>>2]|0;if((v&1|0)!=0){pa(2824,2392,85,2808)}qa=c[t>>2]|0;ma=u+8|0;oa=(c[ma>>2]|0)+1|0;la=u+20|0;na=(c[la>>2]|0)+(oa>>>3)&v;c[la>>2]=na;oa=oa&15;c[ma>>2]=oa;c[t>>2]=((d[na+2>>0]|0)<<8|(d[na+3>>0]|0)|(d[na+1>>0]|0)<<16|(d[na>>0]|0)<<24)<<oa;if((qa|0)>-1){s=0}else{s=(_b(u,s&255)|0)+1&255}a[e+34117>>0]=s;if(!((s&255)<(d[(c[o>>2]|0)+((c[n>>2]|0)*240|0)+193>>0]|0))){qa=-1;i=g;return qa|0}}else{r=11}}if((r|0)==11?(d[q+(p*240|0)+193>>0]|0)>1:0){a[e+34117>>0]=a[e+34116>>0]|0}q=f+8|0;u=c[q>>2]|0;p=e+34128|0;t=c[p>>2]|0;F=t&-3;if((F|0)==4){s=c[e+34156>>2]|0}else{s=1}o=f+16|0;A=c[o>>2]|0;n=f+20|0;x=c[n>>2]|0;E=c[f+28>>2]|0;Sd(e,u)|0;a:do{if((s|0)>0){v=x+20|0;w=u+4|0;r=x+8|0;x=x+28|0;z=A+20|0;y=A+8|0;B=A+28|0;A=u+8|0;D=u+20|0;C=u+12|0;E=E+20|0;I=(F|0)==1|(t|0)==2;G=e+34056|0;H=e+34060|0;F=0;b:while(1){qa=b[(c[v>>2]|0)+((c[w>>2]|0)>>>27<<1)>>1]|0;J=qa<<16>>16;if(!(qa<<16>>16>-1)){r=18;break}Md(u,J&7)|0;J=J>>3;c[x>>2]=(c[x>>2]|0)+(c[(c[r>>2]|0)+(J<<2)>>2]|0);do{if((J|0)==2){M=c[C>>2]|0;if((M&1|0)!=0){r=21;break b}K=(c[w>>2]|0)>>>30;N=c[A>>2]|0;O=N+2|0;L=(c[D>>2]|0)+(O>>>3)&M;c[D>>2]=L;O=O&15;c[A>>2]=O;J=((d[L+2>>0]|0)<<8|(d[L+3>>0]|0)|(d[L+1>>0]|0)<<16|(d[L>>0]|0)<<24)<<O;c[w>>2]=J;if((K|0)==1){J=5;break}else if((K|0)==0){J=3;break}O=O+1|0;c[A>>2]=O;if(O>>>0<16){L=J<<1}else{qa=L+(O>>>3)&M;c[D>>2]=qa;L=N+3&15;c[A>>2]=L;L=((d[qa+2>>0]|0)<<8|(d[qa+3>>0]|0)|(d[qa+1>>0]|0)<<16|(d[qa>>0]|0)<<24)<<L}c[w>>2]=L;J=c[2648+((J>>>31|K<<1)+ -4<<2)>>2]|0}else if((J|0)==1){J=c[C>>2]|0;if((J&1|0)!=0){r=29;break b}qa=(c[w>>2]|0)>>>30;na=(c[A>>2]|0)+2|0;oa=(c[D>>2]|0)+(na>>>3)&J;c[D>>2]=oa;J=na&15;c[A>>2]=J;c[w>>2]=((d[oa+2>>0]|0)<<8|(d[oa+3>>0]|0)|(d[oa+1>>0]|0)<<16|(d[oa>>0]|0)<<24)<<J;J=1<<qa}else if((J|0)==3){J=c[C>>2]|0;if((J&1|0)!=0){r=32;break b}qa=(c[w>>2]|0)>>>30;na=(c[A>>2]|0)+2|0;oa=(c[D>>2]|0)+(na>>>3)&J;c[D>>2]=oa;J=na&15;c[A>>2]=J;c[w>>2]=((d[oa+2>>0]|0)<<8|(d[oa+3>>0]|0)|(d[oa+1>>0]|0)<<16|(d[oa>>0]|0)<<24)<<J;J=1<<qa^15}else if((J|0)==4){J=15}}while(0);K=0;O=0;N=0;L=0;do{c:do{if((1<<K&J|0)!=0){M=c[z>>2]|0;qa=b[M+((Ld(u,5)|0)<<1)>>1]|0;Q=qa<<16>>16;Md(u,qa<<16>>16<0?5:Q&7)|0;P=Q>>3;if((P|0)<0){P=Q;do{qa=b[M+(P+32768+(cd(u,1)|0)<<1)>>1]|0;P=qa<<16>>16}while(qa<<16>>16<0)}M=P+1|0;c[B>>2]=(c[B>>2]|0)+(c[(c[y>>2]|0)+(P<<2)>>2]|0);if(M>>>0>5){Q=c[w>>2]|0;R=(c[A>>2]|0)+1|0;c[A>>2]=R;if(R>>>0<16){P=Q<<1}else{qa=(c[D>>2]|0)+(R>>>3)&c[C>>2];c[D>>2]=qa;P=R&15;c[A>>2]=P;R=P;P=((d[qa+2>>0]|0)<<8|(d[qa+3>>0]|0)|(d[qa+1>>0]|0)<<16|(d[qa>>0]|0)<<24)<<P}c[w>>2]=P;if((Q|0)>-1){R=R+1|0;c[A>>2]=R;if(R>>>0<16){Q=P<<1}else{qa=(c[D>>2]|0)+(R>>>3)&c[C>>2];c[D>>2]=qa;Q=R&15;c[A>>2]=Q;R=Q;Q=((d[qa+2>>0]|0)<<8|(d[qa+3>>0]|0)|(d[qa+1>>0]|0)<<16|(d[qa>>0]|0)<<24)<<Q}c[w>>2]=Q;P=(P>>31)+48&-16}else{Q=P;P=16}if((M|0)==9){R=R+1|0;c[A>>2]=R;if(R>>>0<16){M=Q<<1}else{qa=(c[D>>2]|0)+(R>>>3)&c[C>>2];c[D>>2]=qa;M=R&15;c[A>>2]=M;R=M;M=((d[qa+2>>0]|0)<<8|(d[qa+3>>0]|0)|(d[qa+1>>0]|0)<<16|(d[qa>>0]|0)<<24)<<M}c[w>>2]=M;if((Q|0)>-1){Q=R+1|0;c[A>>2]=Q;if(Q>>>0<16){Q=M<<1}else{qa=(c[D>>2]|0)+(Q>>>3)&c[C>>2];c[D>>2]=qa;Q=Q&15;c[A>>2]=Q;Q=((d[qa+2>>0]|0)<<8|(d[qa+3>>0]|0)|(d[qa+1>>0]|0)<<16|(d[qa>>0]|0)<<24)<<Q}c[w>>2]=Q;M=(M>>31)+11|0}else{M=9}}M=M+ -6|0}else{P=0}Q=c[2560+(M<<2)>>2]|0;if(!((M|0)==0|(M|0)==5)){Q=(_b(u,c[2536+(M<<2)>>2]|0)|0)+Q|0}M=(c[2584+(Q<<2)>>2]|0)+P|0;if((t|0)==3){P=K<<2;L=(M&15)<<P|L;Q=0;while(1){while(1){if((1<<Q+4&M|0)!=0){qa=b[(c[E>>2]|0)+((c[w>>2]|0)>>>27<<1)>>1]|0;R=qa<<16>>16;if(!(qa<<16>>16>-1)){r=65;break b}Md(u,R&7)|0;R=R>>3;do{if((R|0)==0){R=c[C>>2]|0;if((R&1|0)!=0){r=76;break b}qa=(c[w>>2]|0)>>>30;na=(c[A>>2]|0)+2|0;oa=(c[D>>2]|0)+(na>>>3)&R;c[D>>2]=oa;R=na&15;c[A>>2]=R;c[w>>2]=((d[oa+2>>0]|0)<<8|(d[oa+3>>0]|0)|(d[oa+1>>0]|0)<<16|(d[oa>>0]|0)<<24)<<R;R=1<<qa}else if((R|0)==1){U=c[C>>2]|0;if((U&1|0)!=0){r=68;break b}S=(c[w>>2]|0)>>>30;V=c[A>>2]|0;W=V+2|0;T=(c[D>>2]|0)+(W>>>3)&U;c[D>>2]=T;W=W&15;c[A>>2]=W;R=((d[T+2>>0]|0)<<8|(d[T+3>>0]|0)|(d[T+1>>0]|0)<<16|(d[T>>0]|0)<<24)<<W;c[w>>2]=R;if((S|0)==1){R=5;break}else if((S|0)==0){R=3;break}W=W+1|0;c[A>>2]=W;if(W>>>0<16){T=R<<1}else{qa=T+(W>>>3)&U;c[D>>2]=qa;T=V+3&15;c[A>>2]=T;T=((d[qa+2>>0]|0)<<8|(d[qa+3>>0]|0)|(d[qa+1>>0]|0)<<16|(d[qa>>0]|0)<<24)<<T}c[w>>2]=T;R=c[2648+((R>>>31|S<<1)+ -4<<2)>>2]|0}else if((R|0)==2){R=c[C>>2]|0;if((R&1|0)!=0){r=79;break b}qa=(c[w>>2]|0)>>>30;na=(c[A>>2]|0)+2|0;oa=(c[D>>2]|0)+(na>>>3)&R;c[D>>2]=oa;R=na&15;c[A>>2]=R;c[w>>2]=((d[oa+2>>0]|0)<<8|(d[oa+3>>0]|0)|(d[oa+1>>0]|0)<<16|(d[oa>>0]|0)<<24)<<R;R=1<<qa^15}else if((R|0)==3){R=15}}while(0);R=R<<P;if((Q|0)==0){break}N=R|N}Q=Q+1|0;if((Q|0)>=2){break c}}O=R|O;Q=1}}else if((t|0)==1){O=(M>>>4&1)<<K|O;N=(M>>>5&1)<<K|N;L=(M&15)<<(K<<2)|L;break}else if((t|0)==2){P=K<<2;Q=2664+(K<<2)|0;R=0;d:while(1){while(1){if((1<<R+4&M|0)!=0){T=c[w>>2]|0;U=(c[A>>2]|0)+1|0;c[A>>2]=U;if(U>>>0<16){S=T<<1}else{qa=(c[D>>2]|0)+(U>>>3)&c[C>>2];c[D>>2]=qa;S=U&15;c[A>>2]=S;U=S;S=((d[qa+2>>0]|0)<<8|(d[qa+3>>0]|0)|(d[qa+1>>0]|0)<<16|(d[qa>>0]|0)<<24)<<S}c[w>>2]=S;if((T|0)>-1){T=U+1|0;c[A>>2]=T;if(T>>>0<16){T=S<<1}else{qa=(c[D>>2]|0)+(T>>>3)&c[C>>2];c[D>>2]=qa;T=T&15;c[A>>2]=T;T=((d[qa+2>>0]|0)<<8|(d[qa+3>>0]|0)|(d[qa+1>>0]|0)<<16|(d[qa>>0]|0)<<24)<<T}c[w>>2]=T;S=(S>>31)+5|0}else{S=1}S=S<<c[Q>>2];if((R|0)==0){break}N=S|N}R=R+1|0;if((R|0)==2){break d}}O=S|O;R=1}L=(M&15)<<P|L;break}else{L=M<<(K<<2)|L;break}}}while(0);K=K+1|0}while((K|0)<4);c[e+(F<<2)+34052>>2]=L;if(I){c[G>>2]=O;c[H>>2]=N}F=F+1|0;if((F|0)>=(s|0)){break a}}if((r|0)==18){pa(2880,2392,169,2896)}else if((r|0)==21){pa(2824,2392,85,2808)}else if((r|0)==29){pa(2824,2392,85,2808)}else if((r|0)==32){pa(2824,2392,85,2808)}else if((r|0)==65){pa(2880,2392,169,2896)}else if((r|0)==68){pa(2824,2392,85,2808)}else if((r|0)==76){pa(2824,2392,85,2808)}else if((r|0)==79){pa(2824,2392,85,2808)}}}while(0);Uc(e,f);t=c[q>>2]|0;s=c[f+12>>2]|0;r=c[e+34292>>2]|0;q=c[e+34272>>2]|0;p=c[p>>2]|0;v=c[e+34156>>2]|0;w=(p|0)==1;x=(p+ -1|0)>>>0<2?1:v;u=f+492|0;R=c[f+500>>2]|0;z=h;c[z>>2]=0;c[z+4>>2]=0;z=c[e+33992>>2]|0;A=c[e+33996>>2]|0;U=c[e+33988>>2]|0;y=(c[e+33984>>2]|0)==1?f+364|0:f+236|0;if(!w){if((p|0)==2){U=(A<<24)+(z<<16)+U|0;A=8}else{A=4}}else{U=(A<<20)+(z<<16)+U|0;A=6}e:do{if((x|0)>0){z=e+116|0;D=e+34176|0;M=e+34117|0;L=(x|0)>1;I=e+34232|0;F=f+24|0;G=f+584|0;H=t+4|0;B=t+8|0;C=t+20|0;E=t+12|0;J=f+504|0;K=h+4|0;Q=0;N=0;S=h;f:while(1){if((c[z>>2]|0)!=1){Sd(e,s)|0}O=e+(N<<2)+34716|0;P=0;T=0;do{if((c[D>>2]|0)==0){W=d[M>>0]|0;do{if(!L){if((P|0)>3){if(w){V=P+ -3|0;break}else{V=((P|0)/2|0)+ -1|0;break}}else{V=0}}else{V=N}}while(0);$=c[(c[r+(q*240|0)+(V<<2)+128>>2]|0)+(W*20|0)+4>>2]|0}else{$=1}Z=(P|0)>3;V=e+(P+ -3<<2)+34716|0;X=e+((P>>>1&1)+1<<2)+34716|0;W=P<<5&32;Y=($|0)==1;ba=$<<R;ca=Q*3|0;_=f+(ca+13<<2)+24|0;ca=ca+14|0;ea=0;do{do{if(Z){if(w){da=(c[V>>2]|0)+(c[8160+(ea<<2)>>2]<<2)|0;break}else{da=(c[X>>2]|0)+((c[8176+(ea<<2)>>2]|0)+W<<2)|0;break}}else{da=(c[O>>2]|0)+(c[8096+((T&15)<<2)>>2]<<2)|0}}while(0);ha=(c[(c[I>>2]|0)+8>>2]|0)==0;if((!ha?(c[l>>2]|0)!=1:0)?(c[z>>2]|0)!=1:0){r=133;break f}ga=c[G>>2]|0;fa=R-ga|0;fa=(fa|0)>-1&ha?fa:0;g:do{if((U&1|0)!=0){ha=c[_>>2]|0;ia=c[ha+20>>2]|0;qa=b[ia+((Ld(t,5)|0)<<1)>>1]|0;ja=qa<<16>>16;Md(t,qa<<16>>16<0?5:ja&7)|0;la=ja>>3;if((la|0)<0){la=ja;do{qa=b[ia+(la+32768+(cd(t,1)|0)<<1)>>1]|0;la=qa<<16>>16}while(qa<<16>>16<0)}ja=ha+28|0;c[ja>>2]=(c[ja>>2]|0)+(c[(c[ha+8>>2]|0)+(la<<2)>>2]|0);ja=ha+32|0;c[ja>>2]=(c[ja>>2]|0)+(c[(c[ha+12>>2]|0)+(la<<2)>>2]|0);c[m>>2]=la;ja=la&1;ia=la>>2;ha=ja&ia;ma=c[H>>2]|0;ka=ma>>31;na=(c[B>>2]|0)+1|0;c[B>>2]=na;if(na>>>0<16){ma=ma<<1}else{qa=(c[C>>2]|0)+(na>>>3)&c[E>>2];c[C>>2]=qa;ma=na&15;c[B>>2]=ma;ma=((d[qa+2>>0]|0)<<8|(d[qa+3>>0]|0)|(d[qa+1>>0]|0)<<16|(d[qa>>0]|0)<<24)<<ma}c[H>>2]=ma;ka=(ka^ba)-ka|0;if((la&2|0)!=0){ka=aa(cc(c[f+(ha+19<<2)+24>>2]|0,t)|0,ka)|0}if((ja|0)==0){qa=b[(c[(c[F>>2]|0)+20>>2]|0)+((c[H>>2]|0)>>>27<<1)>>1]|0;ja=qa<<16>>16;if(!(qa<<16>>16>-1)){r=144;break f}la=c[7792>>2]|0;Md(t,ja&7)|0;la=(la*5|0)+(ja>>3)|0;ja=c[2168+(la<<2)>>2]|0;la=c[7800+(la<<2)>>2]|0;if((la|0)!=0){ja=(_b(t,la)|0)+ja|0}ja=ja+1&15}else{ja=1}la=y+(ja<<3)|0;c[da+(c[y+(ja<<3)+4>>2]<<2)>>2]=ka;ka=(c[la>>2]|0)+1|0;c[la>>2]=ka;if((ja|0)!=0?(j=y+(ja+ -1<<3)|0,ka>>>0>(c[j>>2]|0)>>>0):0){oa=la;na=c[oa>>2]|0;oa=c[oa+4>>2]|0;ka=j;ma=c[ka+4>>2]|0;qa=la;c[qa>>2]=c[ka>>2];c[qa+4>>2]=ma;qa=j;c[qa>>2]=na;c[qa+4>>2]=oa}if((ia|0)==0){ha=1}else{la=ha;ja=ja+1|0;ha=1;do{ka=ja&15;if((ia&1|0)==0){ka=(Zb(ka^15,c[F>>2]|0,t)|0)+ka|0;if(ka>>>0>15){ha=16;break g}}ja=ka+1|0;gc(m,ja,c[f+(ca+la<<2)+24>>2]|0,t);ma=c[m>>2]|0;ia=ma>>1;if(!(ia>>>0<3)){r=156;break f}la=ia&la;oa=c[H>>2]|0;na=oa>>31;qa=(c[B>>2]|0)+1|0;c[B>>2]=qa;if(qa>>>0<16){oa=oa<<1}else{ra=(c[C>>2]|0)+(qa>>>3)&c[E>>2];c[C>>2]=ra;oa=qa&15;c[B>>2]=oa;oa=((d[ra+2>>0]|0)<<8|(d[ra+3>>0]|0)|(d[ra+1>>0]|0)<<16|(d[ra>>0]|0)<<24)<<oa}c[H>>2]=oa;na=(na^ba)-na|0;if((ma&1|0)!=0){na=aa(cc(c[f+(la+19<<2)+24>>2]|0,t)|0,na)|0}ma=y+(ka<<3)|0;c[da+(c[y+(ka<<3)+4>>2]<<2)>>2]=na;na=(c[ma>>2]|0)+1|0;c[ma>>2]=na;if((ka|0)!=0?(k=y+(ka+ -1<<3)|0,na>>>0>(c[k>>2]|0)>>>0):0){qa=ma;oa=c[qa>>2]|0;qa=c[qa+4>>2]|0;ka=k;na=c[ka+4>>2]|0;ra=ma;c[ra>>2]=c[ka>>2];c[ra+4>>2]=na;ra=k;c[ra>>2]=oa;c[ra+4>>2]=qa}ha=ha+1|0}while((ia|0)!=0)}}else{ha=0}}while(0);h:do{if((fa|0)!=0){if((ga+$|0)!=1){ia=$<<ga;ja=1;while(1){ka=da+(c[8208+(ja<<2)>>2]<<2)|0;ga=c[ka>>2]|0;do{if((ga|0)>=0){if((ga|0)>0){ra=aa(_b(s,fa)|0,ia)|0;c[ka>>2]=(c[ka>>2]|0)+ra;break}else{c[ka>>2]=aa(fc(s,fa)|0,ia)|0;break}}else{ra=aa(_b(s,fa)|0,ia)|0;c[ka>>2]=(c[ka>>2]|0)-ra}}while(0);ja=ja+1|0;if((ja|0)==16){break h}}}if((ga|0)!=0){r=169;break f}if(Y){ia=1}else{r=171;break f}do{ja=da+(c[8208+(ia<<2)>>2]<<2)|0;ga=c[ja>>2]|0;do{if((ga|0)>=0){if((ga|0)>0){ra=_b(s,fa)|0;c[ja>>2]=(c[ja>>2]|0)+ra;break}else{c[ja>>2]=fc(s,fa)|0;break}}else{ra=_b(s,fa)|0;c[ja>>2]=(c[ja>>2]|0)-ra}}while(0);ia=ia+1|0}while((ia|0)!=16)}}while(0);if((ha|0)>16){e=-1;r=194;break f}c[S>>2]=(c[S>>2]|0)+ha;ea=ea+1|0;T=T+1|0;U=U>>1}while((ea|0)<4);if((P|0)==3){Q=1;R=c[J>>2]|0;S=K}P=P+1|0}while((P|0)<(A|0));O=N+1|0;if((O|0)>=(x|0)){break e}N=O;U=c[e+((O&15)<<2)+33988>>2]|0}if((r|0)==133){pa(2264,2392,712,2416)}else if((r|0)==144){pa(2880,2392,169,2896)}else if((r|0)==156){pa(2488,2392,544,2512)}else if((r|0)==169){pa(2432,2392,594,2448)}else if((r|0)==171){pa(2472,2392,595,2448)}else if((r|0)==194){i=g;return e|0}}}while(0);Zc(p,v,h,u);if((c[e+34288>>2]|0)==0){ra=0;i=g;return ra|0}Yc(c[o>>2]|0);Yc(c[n>>2]|0);Yc(c[f+76>>2]|0);Yc(c[f+80>>2]|0);Yc(c[f+84>>2]|0);Yc(c[f+88>>2]|0);Yc(c[f+92>>2]|0);Yc(c[f+96>>2]|0);Yc(c[f+100>>2]|0);Yc(c[f+104>>2]|0);ra=0;i=g;return ra|0}function ec(a){a=a|0;var b=0;b=i;Yc(c[a+16>>2]|0);Yc(c[a+20>>2]|0);Yc(c[a+76>>2]|0);Yc(c[a+80>>2]|0);Yc(c[a+84>>2]|0);Yc(c[a+88>>2]|0);Yc(c[a+92>>2]|0);Yc(c[a+96>>2]|0);Yc(c[a+100>>2]|0);Yc(c[a+104>>2]|0);i=b;return 0}function fc(a,b){a=a|0;b=b|0;var e=0,f=0,g=0,h=0,j=0,k=0;e=i;h=b+1|0;if(!((h|0)>-1&h>>>0<17)){pa(2776,2392,78,2848)}f=a+4|0;h=(c[f>>2]|0)>>>(31-b|0);g=h&1;g=(h>>1^0-g)+g|0;b=((g|0)!=0)+b|0;if(!((b|0)>-1&b>>>0<17)){pa(2776,2392,85,2808)}h=c[a+12>>2]|0;if((h&1|0)==0){j=a+8|0;k=(c[j>>2]|0)+b|0;a=a+20|0;b=(c[a>>2]|0)+(k>>>3)&h;c[a>>2]=b;h=k&15;c[j>>2]=h;c[f>>2]=((d[b+2>>0]|0)<<8|(d[b+3>>0]|0)|(d[b+1>>0]|0)<<16|(d[b>>0]|0)<<24)<<h;i=e;return g|0}else{pa(2824,2392,85,2808)}return 0}function gc(a,e,f,g){a=a|0;e=e|0;f=f|0;g=g|0;var h=0,j=0,k=0,l=0,m=0;h=i;if((e|0)<15){l=b[(c[f+20>>2]|0)+((c[g+4>>2]|0)>>>27<<1)>>1]|0;e=l<<16>>16;if(!(l<<16>>16>-1)){pa(2880,2392,169,2896)}Md(g,e&7)|0;l=e>>3;k=f+28|0;c[k>>2]=(c[k>>2]|0)+(c[(c[f+8>>2]|0)+(l<<2)>>2]|0);k=f+32|0;c[k>>2]=(c[k>>2]|0)+(c[(c[f+12>>2]|0)+(l<<2)>>2]|0);c[a>>2]=l;i=h;return}f=g+4|0;if((e|0)!=15){e=c[g+12>>2]|0;if((e&1|0)!=0){pa(2824,2392,85,2808)}l=(c[f>>2]|0)>>>31;m=g+8|0;k=(c[m>>2]|0)+1|0;g=g+20|0;j=(c[g>>2]|0)+(k>>>3)&e;c[g>>2]=j;k=k&15;c[m>>2]=k;c[f>>2]=((d[j+2>>0]|0)<<8|(d[j+3>>0]|0)|(d[j+1>>0]|0)<<16|(d[j>>0]|0)<<24)<<k;c[a>>2]=l;i=h;return}k=c[f>>2]|0;e=g+8|0;l=(c[e>>2]|0)+1|0;c[e>>2]=l;if(l>>>0<16){j=k<<1}else{j=g+20|0;m=(c[j>>2]|0)+(l>>>3)&c[g+12>>2];c[j>>2]=m;j=l&15;c[e>>2]=j;l=j;j=((d[m+2>>0]|0)<<8|(d[m+3>>0]|0)|(d[m+1>>0]|0)<<16|(d[m>>0]|0)<<24)<<j}c[f>>2]=j;if(!((k|0)>-1)){l=l+1|0;c[e>>2]=l;if(l>>>0<16){k=j<<1}else{k=g+20|0;m=(c[k>>2]|0)+(l>>>3)&c[g+12>>2];c[k>>2]=m;k=l&15;c[e>>2]=k;l=k;k=((d[m+2>>0]|0)<<8|(d[m+3>>0]|0)|(d[m+1>>0]|0)<<16|(d[m>>0]|0)<<24)<<k}c[f>>2]=k;if((j|0)>-1){g=2}else{j=l+1|0;c[e>>2]=j;if(j>>>0<16){g=k<<1}else{l=g+20|0;m=(c[l>>2]|0)+(j>>>3)&c[g+12>>2];c[l>>2]=m;g=j&15;c[e>>2]=g;g=((d[m+2>>0]|0)<<8|(d[m+3>>0]|0)|(d[m+1>>0]|0)<<16|(d[m>>0]|0)<<24)<<g}c[f>>2]=g;g=k>>>30|1}}else{g=0}c[a>>2]=g;i=h;return}function hc(b,c,d){b=b|0;c=c|0;d=d|0;var e=0,f=0,g=0;e=i;do{if(!(d>>>0>15)){if(!(d>>>0>1)){a[b>>0]=wd(c,8)|0;f=0;break}f=(wd(c,2)|0)&255;a[b>>0]=wd(c,8)|0;if(f<<24>>24==0){f=0;break}else if(f<<24>>24==1){a[b+1>>0]=wd(c,8)|0;f=1;break}else{g=1;do{a[b+g>>0]=wd(c,8)|0;g=g+1|0}while((g|0)!=(d|0))}}else{f=0}}while(0);i=e;return f|0}function ic(b,d){b=b|0;d=d|0;var e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0;e=i;if((c[b+34180>>2]&1|0)==0){n=0;i=e;return n|0}h=b+34292|0;f=c[h>>2]|0;g=c[b+34272>>2]|0;a:do{if((c[b+34268>>2]|0)==(0-g|0)){j=b+132|0;l=b+34156|0;m=f;k=0;while(1){n=k+1|0;if((Fd(m+(k*240|0)|0,c[l>>2]|0,1)|0)!=0){b=-1;break}if(n>>>0>(c[j>>2]|0)>>>0){break a}m=c[h>>2]|0;k=n}i=e;return b|0}else{l=b+34156|0}}while(0);h=f+(g*240|0)|0;j=c[l>>2]|0;m=j>>>0>1;if(m){k=(cd(d,2)|0)&255}else{k=0}n=(cd(d,8)|0)&255;a[c[h>>2]>>0]=n;if(k<<24>>24==1){n=(cd(d,8)|0)&255;a[c[f+(g*240|0)+4>>2]>>0]=n}else if(!(k<<24>>24==0)?m:0){m=1;do{n=(cd(d,8)|0)&255;a[c[f+(g*240|0)+(m<<2)>>2]>>0]=n;m=m+1|0}while((m|0)!=(j|0))}a[f+(g*240|0)+204>>0]=k;Gd(h,k,c[l>>2]|0,0,1,c[b+34140>>2]|0);n=0;i=e;return n|0}function jc(b,e){b=b|0;e=e|0;var f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0;f=i;if((c[b+116>>2]|0)==3){r=0;i=f;return r|0}if((c[b+34180>>2]&2|0)==0){r=0;i=f;return r|0}g=c[b+34292>>2]|0;l=b+34272|0;h=c[l>>2]|0;n=(cd(e,1)|0)==1|0;m=g+(h*240|0)+196|0;c[m>>2]=n;k=g+(h*240|0)+194|0;a[k>>0]=0;j=g+(h*240|0)+192|0;a[j>>0]=1;if((c[b+34268>>2]|0)!=0){Ed(g+(h*240|0)+64|0);n=c[m>>2]|0}if((n|0)==1){if((Fd(g+(h*240|0)+64|0,c[b+34156>>2]|0,d[j>>0]|0)|0)!=0){r=-1;i=f;return r|0}Id(b,c[l>>2]|0);r=0;i=f;return r|0}l=(cd(e,4)|0)+1&255;a[j>>0]=l;a[k>>0]=Kd(l)|0;k=g+(h*240|0)+64|0;l=b+34156|0;if((Fd(k,c[l>>2]|0,d[j>>0]|0)|0)!=0){r=-1;i=f;return r|0}if((a[j>>0]|0)==0){r=0;i=f;return r|0}m=b+34140|0;b=g+(h*240|0)+68|0;n=0;o=0;while(1){q=c[l>>2]|0;r=q>>>0>1;if(r){p=(cd(e,2)|0)&255}else{p=0}s=(cd(e,8)|0)&255;a[(c[k>>2]|0)+(n*20|0)>>0]=s;if(p<<24>>24==1){s=(cd(e,8)|0)&255;a[(c[b>>2]|0)+(n*20|0)>>0]=s}else if(!(p<<24>>24==0)?r:0){r=1;do{s=(cd(e,8)|0)&255;a[(c[g+(h*240|0)+(r<<2)+64>>2]|0)+(n*20|0)>>0]=s;r=r+1|0}while((r|0)!=(q|0))}a[g+(h*240|0)+n+205>>0]=p;Gd(k,p,c[l>>2]|0,n,1,c[m>>2]|0);o=o+1<<24>>24;if((o&255)<(d[j>>0]|0)){n=o&255}else{g=0;break}}i=f;return g|0}function kc(b,e){b=b|0;e=e|0;var f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0;f=i;if(((c[b+116>>2]|0)+ -2|0)>>>0<2){r=0;i=f;return r|0}if((c[b+34180>>2]&4|0)==0){r=0;i=f;return r|0}h=c[b+34292>>2]|0;l=b+34272|0;j=c[l>>2]|0;n=(cd(e,1)|0)==1|0;m=h+(j*240|0)+200|0;c[m>>2]=n;k=h+(j*240|0)+195|0;a[k>>0]=0;g=h+(j*240|0)+193|0;a[g>>0]=1;if((c[b+34268>>2]|0)!=0){Ed(h+(j*240|0)+128|0);n=c[m>>2]|0}if((n|0)==1){r=a[h+(j*240|0)+192>>0]|0;a[g>>0]=r;if((Fd(h+(j*240|0)+128|0,c[b+34156>>2]|0,r&255)|0)!=0){r=-1;i=f;return r|0}Jd(b,d[g>>0]|0,c[l>>2]|0);r=0;i=f;return r|0}l=(cd(e,4)|0)+1&255;a[g>>0]=l;a[k>>0]=Kd(l)|0;k=h+(j*240|0)+128|0;l=b+34156|0;if((Fd(k,c[l>>2]|0,d[g>>0]|0)|0)!=0){r=-1;i=f;return r|0}if((a[g>>0]|0)==0){r=0;i=f;return r|0}n=b+34140|0;b=h+(j*240|0)+132|0;m=0;o=0;while(1){q=c[l>>2]|0;r=q>>>0>1;if(r){p=(cd(e,2)|0)&255}else{p=0}s=(cd(e,8)|0)&255;a[(c[k>>2]|0)+(m*20|0)>>0]=s;if(p<<24>>24==1){s=(cd(e,8)|0)&255;a[(c[b>>2]|0)+(m*20|0)>>0]=s}else if(!(p<<24>>24==0)?r:0){r=1;do{s=(cd(e,8)|0)&255;a[(c[h+(j*240|0)+(r<<2)+128>>2]|0)+(m*20|0)>>0]=s;r=r+1|0}while((r|0)!=(q|0))}a[h+(j*240|0)+m+221>>0]=p;Gd(k,p,c[l>>2]|0,m,0,c[n>>2]|0);o=o+1<<24>>24;if((o&255)<(d[g>>0]|0)){m=o&255}else{g=0;break}}i=f;return g|0}function lc(b){b=b|0;var e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0;e=i;a:do{if((c[b+34328>>2]|0)==0?(h=b+34268|0,(c[b+34324>>2]|0)==(c[b+(c[h>>2]<<2)+16524>>2]|0)):0){f=b+34300|0;u=c[f>>2]|0;k=(u|0)==0;if((c[b+34932>>2]|0)!=0){if(k){Sb(c[b+34308>>2]|0);break}g=b+132|0;f=b+34308|0;h=0;while(1){Sb((c[f>>2]|0)+(h*592|0)|0);h=h+1|0;if(h>>>0>(c[g>>2]|0)>>>0){break a}}}if(!k){p=b+34920|0;m=b+124|0;o=b+34264|0;n=b+34304|0;q=b+34296|0;l=b+104|0;k=b+132|0;r=b+34236|0;s=0;do{t=c[p>>2]|0;if((t|0)!=0){if((c[l>>2]|0)==0){u=1}else{u=d[r>>0]|0}v=c[h>>2]|0;t=t+((aa(aa(v,u)|0,(c[k>>2]|0)+1|0)|0)+s<<2)|0;if((v|0)!=0?(j=c[(c[q>>2]|0)+(s<<2)>>2]|0,(c[j+24>>2]|0)!=0):0){Xd(b,j)|0}t=c[t>>2]|0;if((t|0)!=0){Wd(c[(c[q>>2]|0)+(s<<2)>>2]|0,t,b)|0}}else{if((c[h>>2]|0)==0){t=u;u=0}else{Xd(b,c[(c[q>>2]|0)+(s<<2)>>2]|0)|0;t=c[f>>2]|0;u=c[h>>2]|0}w=c[m>>2]|0;v=(aa(u,t)|0)+s|0;Ya[c[w+36>>2]&31](w,(c[n>>2]|0)+(c[(c[o>>2]|0)+(v<<2)>>2]|0)|0)|0;Wd(c[(c[q>>2]|0)+(s<<2)>>2]|0,c[m>>2]|0,b)|0}s=s+1|0;u=c[f>>2]|0}while(s>>>0<u>>>0);if((u|0)!=0){h=b+34248|0}else{g=24}}else{g=24}do{if((g|0)==24){h=b+34248|0;Xd(b,c[h>>2]|0)|0;g=c[b+34920>>2]|0;if((g|0)==0){w=b+124|0;v=c[w>>2]|0;Ya[c[v+36>>2]&31](v,c[b+34304>>2]|0)|0;Wd(c[h>>2]|0,c[w>>2]|0,b)|0;break}else{Wd(c[h>>2]|0,c[g>>2]|0,b)|0;break}}}while(0);j=b+132|0;k=b+104|0;m=b+34148|0;o=b+34308|0;n=b+34296|0;g=b+34236|0;l=0;while(1){if((c[k>>2]|0)!=0){p=aa(d[g>>0]|0,l)|0;p=c[(c[n>>2]|0)+(p<<2)>>2]|0;if((p|0)==0){b=-1;g=73;break}if((cd(p,8)|0)!=0){b=-1;g=73;break}if((cd(p,8)|0)!=0){b=-1;g=73;break}if((cd(p,8)|0)!=1){b=-1;g=73;break}cd(p,8)|0;p=a[g>>0]|0;if((p&255)>1){p=(aa(p&255,l)|0)+1|0;p=c[(c[n>>2]|0)+(p<<2)>>2]|0;if((p|0)==0){b=-1;g=73;break}if((cd(p,8)|0)!=0){b=-1;g=73;break}if((cd(p,8)|0)!=0){b=-1;g=73;break}if((cd(p,8)|0)!=1){b=-1;g=73;break}cd(p,8)|0;p=a[g>>0]|0;if((p&255)>2){p=(aa(p&255,l)|0)+2|0;p=c[(c[n>>2]|0)+(p<<2)>>2]|0;if((p|0)==0){b=-1;g=73;break}if((cd(p,8)|0)!=0){b=-1;g=73;break}if((cd(p,8)|0)!=0){b=-1;g=73;break}if((cd(p,8)|0)!=1){b=-1;g=73;break}cd(p,8)|0;p=a[g>>0]|0;if((p&255)>3){p=(aa(p&255,l)|0)+3|0;p=c[(c[n>>2]|0)+(p<<2)>>2]|0;if((p|0)==0){b=-1;g=73;break}if(((cd(p,8)|0)==0?(cd(p,8)|0)==0:0)?(cd(p,8)|0)==1:0){cd(p,8)|0}if((c[m>>2]|0)==0){p=0}else{p=(aa(d[g>>0]|0,l)|0)+3|0;p=cd(c[(c[n>>2]|0)+(p<<2)>>2]|0,4)|0}c[(c[o>>2]|0)+(l*592|0)+584>>2]=p}}}}else{if((c[f>>2]|0)==0){p=h}else{p=(c[n>>2]|0)+(l<<2)|0}p=c[p>>2]|0;if((c[p+24>>2]|0)==0){b=-1;g=73;break}if((cd(p,8)|0)!=0){b=-1;g=73;break}if((cd(p,8)|0)!=0){b=-1;g=73;break}if((cd(p,8)|0)!=1){b=-1;g=73;break}cd(p,8)|0;if((c[m>>2]|0)==0){p=0}else{p=cd(p,4)|0}c[(c[o>>2]|0)+(l*592|0)+584>>2]=p}Sb((c[o>>2]|0)+(l*592|0)|0);l=l+1|0;if(l>>>0>(c[j>>2]|0)>>>0){break a}}if((g|0)==73){i=e;return b|0}}}while(0);if((c[b+34276>>2]|0)==0){w=0;i=e;return w|0}if((c[b+34280>>2]|0)==0){w=0;i=e;return w|0}if((c[b+34932>>2]|0)!=0){w=0;i=e;return w|0}g=c[b+34272>>2]|0;h=c[b+34308>>2]|0;j=h+(g*592|0)|0;ic(b,c[j>>2]|0)|0;f=b+34928|0;k=c[f>>2]|0;if((k|0)!=0){ic(k,c[j>>2]|0)|0}j=b+34236|0;if((d[j>>0]|0)<=1){w=0;i=e;return w|0}k=h+(g*592|0)+4|0;jc(b,c[k>>2]|0)|0;l=c[f>>2]|0;if((l|0)!=0){jc(l,c[k>>2]|0)|0}if((d[j>>0]|0)<=2){w=0;i=e;return w|0}g=h+(g*592|0)+8|0;kc(b,c[g>>2]|0)|0;b=c[f>>2]|0;if((b|0)==0){w=0;i=e;return w|0}kc(b,c[g>>2]|0)|0;w=0;i=e;return w|0}function mc(a){a=a|0;var b=0,d=0,e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0;d=i;m=c[a+100>>2]|0;if((c[a+34324>>2]|0)==(c[a+34336>>2]|0)){h=1}else{h=(c[a+34328>>2]|0)==(c[a+34332>>2]|0)}k=(c[a+34928>>2]|0)!=0;j=k&1;e=(m|0)==0;m=(m|0)==1?2:10;n=0;a:while(1){b:do{if(h){o=a+34232|0}else{p=a+34328|0;q=a+34324|0;dd(a,c[p>>2]|0,c[q>>2]|0);v=a+34272|0;if(k){B=c[a+34928>>2]|0;c[B+34272>>2]=c[v>>2];c[B+34268>>2]=c[a+34268>>2]}t=c[v>>2]|0;s=c[a+34308>>2]|0;r=s+(t*592|0)|0;if((lc(a)|0)!=0){g=-1;b=40;break a}o=a+34232|0;u=c[o>>2]|0;do{if((c[u>>2]|0)==0){y=c[p>>2]|0;A=c[v>>2]|0;if((y|0)==(c[a+(A<<2)+136>>2]|0)){x=c[u+28>>2]|0;w=c[u+36>>2]|0;v=e?0:m;y=y<<4;B=c[a+34268>>2]|0;z=c[a+(B<<2)+16524>>2]<<4;if((A|0)==(c[a+132>>2]|0)){A=a+34332|0}else{A=a+(A+1<<2)+136|0}if((B|0)==(c[a+16520>>2]|0)){B=a+34336|0}else{B=a+(B+1<<2)+16524|0}if((((c[u+24>>2]|0)>>>0<(c[A>>2]<<4|v)>>>0?(c[u+32>>2]|0)>>>0<(c[B>>2]<<4|v)>>>0:0)?!(y>>>0>(v+x|0)>>>0):0)?(l=v+w|0,!(z>>>0>l>>>0)):0){v=c[q>>2]<<4>>>0>l>>>0}else{v=1}c[s+(t*592|0)+588>>2]=v&1^1;if((c[u>>2]|0)!=0){break}}if((c[s+(t*592|0)+588>>2]|0)==0){break b}}}while(0);s=bc(a,r,c[p>>2]|0,c[q>>2]|0)|0;if((s|0)!=0){g=s;b=40;break a}if((c[(c[o>>2]|0)+20>>2]|0)!=0?(g=$b(a,r,c[p>>2]|0,c[q>>2]|0)|0,(g|0)!=0):0){b=40;break a}Sc(a);Rc(a)|0;if((c[(c[o>>2]|0)+16>>2]|0)!=0){q=dc(a,r,c[p>>2]|0,c[q>>2]|0)|0;if((q|0)!=0){g=q;b=40;break a}Tc(a)}ce(a,a+32960|0,c[p>>2]|0,c[a+34128>>2]|0)}}while(0);o=c[o>>2]|0;if((c[o>>2]|0)==0){p=c[a+34328>>2]<<4;if(((!(p>>>0>((c[o+28>>2]|0)+25|0)>>>0)?!((p+25|0)>>>0<(c[o+24>>2]|0)>>>0):0)?(f=c[a+34324>>2]<<4,!(f>>>0>((c[o+36>>2]|0)+25|0)>>>0)):0)?!((f+25|0)>>>0<(c[o+32>>2]|0)>>>0):0){b=36}}else{b=36}if((b|0)==36){b=0;Wa[c[a+34412>>2]&31](a)|0}if(k){B=c[a+34928>>2]|0;c[B+34324>>2]=c[a+34324>>2];c[B+34328>>2]=c[a+34328>>2];a=B}n=n+1|0;if(n>>>0>j>>>0){g=0;b=40;break}}if((b|0)==40){i=d;return g|0}return 0}function nc(a){a=a|0;var b=0,e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0;b=i;g=c[a+34332>>2]<<4;h=c[a+34528>>2]|0;j=c[a+34532>>2]|0;e=c[a+34780>>2]|0;f=c[a+34784>>2]|0;if((c[a+34128>>2]|0)==2){if((g|0)==0){g=0;do{y=d[8415+(g<<4)>>0]|0|-256;c[e+(y<<2)>>2]=c[e>>2];c[f+(y<<2)>>2]=c[f>>2];g=g+1|0}while((g|0)!=16);i=b;return}else{k=0}do{m=0;while(1){l=m>>>4;y=(d[(m>>>1&7)+(8400+(k<<4))>>0]|0)+(l<<7)|0;l=d[(m&14)+(8400+(k<<4))>>0]|0|l<<8;o=e+(l<<2)|0;c[o>>2]=c[h+(y<<2)>>2];n=f+(l<<2)|0;c[n>>2]=c[j+(y<<2)>>2];if((m|0)!=0){x=m+ -2|0;x=d[(x&14)+(8400+(k<<4))>>0]|0|x>>>4<<8;y=m+ -1|0;y=d[(y&15)+(8400+(k<<4))>>0]|0|y>>>4<<8;c[e+(y<<2)>>2]=(c[e+(x<<2)>>2]|0)+1+(c[o>>2]|0)>>1;c[f+(y<<2)>>2]=(c[f+(x<<2)>>2]|0)+1+(c[n>>2]|0)>>1}n=m+2|0;if(n>>>0<g>>>0){m=n}else{break}}y=d[(m&14|1)+(8400+(k<<4))>>0]|0|m>>>4<<8;c[e+(y<<2)>>2]=c[e+(l<<2)>>2];c[f+(y<<2)>>2]=c[f+(l<<2)>>2];k=k+1|0}while((k|0)!=16);i=b;return}l=(c[a+12>>2]|0)==2;n=l?3:4;if((g|0)!=0){m=n+4|0;k=4-n|0;n=(1<<n)+ -1|0;o=a+34324|0;p=a+34336|0;q=a+34592|0;t=a+34596|0;r=0;do{a=r>>>4;w=a<<m;x=r>>>k&n;a=a<<6;v=r>>>1&7;y=0;while(1){z=(d[8656+(y>>>1<<3)+v>>0]|0)+a|0;u=(d[8400+(y<<4)+x>>0]|0)+w|0;s=e+(u<<2)|0;c[s>>2]=c[h+(z<<2)>>2];u=f+(u<<2)|0;c[u>>2]=c[j+(z<<2)>>2];if((y|0)!=0){A=(d[8400+(y+ -2<<4)+x>>0]|0)+w|0;z=(d[8400+(y+ -1<<4)+x>>0]|0)+w|0;c[e+(z<<2)>>2]=(c[e+(A<<2)>>2]|0)+1+(c[s>>2]|0)>>1;c[f+(z<<2)>>2]=(c[f+(A<<2)>>2]|0)+1+(c[u>>2]|0)>>1;y=y+2|0;if(y>>>0<16){continue}else{break}}else{y=y+2|0;continue}}w=(d[8640+x>>0]|0)+w|0;if((c[o>>2]|0)==(c[p>>2]|0)){c[e+(w<<2)>>2]=c[s>>2];c[f+(w<<2)>>2]=c[u>>2]}else{A=(d[8656+v>>0]|0)+a|0;c[e+(w<<2)>>2]=(c[(c[q>>2]|0)+(A<<2)>>2]|0)+1+(c[s>>2]|0)>>1;c[f+(w<<2)>>2]=(c[(c[t>>2]|0)+(A<<2)>>2]|0)+1+(c[u>>2]|0)>>1}r=r+2|0}while(r>>>0<g>>>0)}if(l){i=b;return}h=g+ -2|0;j=(g+268435455|0)>>>4<<8;g=0;do{k=1;do{z=k+ -1|0;z=d[(z&15)+(8400+(g<<4))>>0]|0|z>>>4<<8;A=d[(k&15)+(8400+(g<<4))>>0]|0|k>>>4<<8;l=k+1|0;l=d[(l&15)+(8400+(g<<4))>>0]|0|l>>>4<<8;c[e+(A<<2)>>2]=(c[e+(l<<2)>>2]|0)+1+(c[e+(z<<2)>>2]|0)>>1;c[f+(A<<2)>>2]=(c[f+(l<<2)>>2]|0)+1+(c[f+(z<<2)>>2]|0)>>1;k=k+2|0}while(k>>>0<h>>>0);A=d[8415+(g<<4)>>0]|0|j;c[e+(A<<2)>>2]=c[e+(l<<2)>>2];c[f+(A<<2)>>2]=c[f+(l<<2)>>2];g=g+1|0}while((g|0)!=16);i=b;return}function oc(e,f,h,j,l,m,n){e=e|0;f=f|0;h=h|0;j=j|0;l=l|0;m=m|0;n=n|0;var o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0;p=i;i=i+64|0;t=p;if((c[e+12>>2]|0)==0){o=1}else{o=c[e+108>>2]|0}w=a[e+32908>>0]|0;z=a[e+32909>>0]|0;r=c[e+34232>>2]|0;q=c[r+40>>2]|0;r=c[r+44>>2]|0;s=(c[e+34324>>2]<<4)+ -16|0;if(!(o>>>0<17)){pa(2912,2928,620,2952)}u=(o|0)==0;if(!u){v=0;do{c[t+((v&15)<<2)>>2]=c[e+(v<<2)+34524>>2];v=v+1|0}while((v|0)!=(o|0))}if((c[e+34240>>2]|0)!=0){c[t+4>>2]=c[e+34780>>2];c[t+8>>2]=c[e+34784>>2]}switch(c[e+16>>2]|0){case 6:{if(!(f>>>0<l>>>0)){i=p;return}v=h>>>0<j>>>0;e=e+32936|0;w=w&255;do{x=c[r+(f+s<<2)>>2]|0;if(v){y=c[e>>2]|0;z=h;do{A=(c[q+(z<<2)>>2]|0)+x|0;if(!u){B=d[(z&15)+(8400+(f<<4))>>0]|0|z>>>4<<8;C=0;do{c[y+(A+C<<2)>>2]=(c[(c[t+((C&15)<<2)>>2]|0)+(B<<2)>>2]|0)+n>>m<<w;C=C+1|0}while((C|0)!=(o|0))}z=z+1|0}while((z|0)!=(j|0))}f=f+1|0}while((f|0)!=(l|0));i=p;return};case 5:{if(!(f>>>0<l>>>0)){i=p;return}v=h>>>0<j>>>0;e=e+32936|0;w=w&255;do{C=c[r+(f+s<<2)>>2]|0;if(v){B=c[e>>2]|0;A=h;do{z=(c[q+(A<<2)>>2]|0)+C|0;if(!u){y=d[(A&15)+(8400+(f<<4))>>0]|0|A>>>4<<8;x=0;do{c[B+(z+x<<2)>>2]=(c[(c[t+((x&15)<<2)>>2]|0)+(y<<2)>>2]|0)+n>>m<<w;x=x+1|0}while((x|0)!=(o|0))}A=A+1|0}while((A|0)!=(j|0))}f=f+1|0}while((f|0)!=(l|0));i=p;return};case 2:{if(!(f>>>0<l>>>0)){i=p;return}v=h>>>0<j>>>0;e=e+32936|0;w=w&255;do{x=c[r+(f+s<<2)>>2]|0;if(v){y=c[e>>2]|0;z=h;do{A=(c[q+(z<<2)>>2]|0)+x|0;if(!u){C=d[(z&15)+(8400+(f<<4))>>0]|0|z>>>4<<8;B=0;do{D=(c[(c[t+((B&15)<<2)>>2]|0)+(C<<2)>>2]|0)+n>>m<<w;if((D|0)<0){D=0}else{D=(D|0)>65535?-1:D&65535}b[y+(A+B<<1)>>1]=D;B=B+1|0}while((B|0)!=(o|0))}z=z+1|0}while((z|0)!=(j|0))}f=f+1|0}while((f|0)!=(l|0));i=p;return};case 1:{if(!(f>>>0<l>>>0)){i=p;return}v=e+32936|0;do{A=c[r+(f+s<<2)>>2]|0;if(!(h>>>0>=j>>>0|u)){z=h;do{y=c[v>>2]|0;e=(c[q+(z<<2)>>2]|0)+A|0;x=d[(z&15)+(8400+(f<<4))>>0]|0|z>>>4<<8;w=0;do{B=(c[(c[t+((w&15)<<2)>>2]|0)+(x<<2)>>2]|0)+n>>m;if((B|0)<0){B=0}else{B=(B|0)>255?-1:B&255}a[y+(e+w)>>0]=B;w=w+1|0}while((w|0)!=(o|0));z=z+1|0}while((z|0)!=(j|0))}f=f+1|0}while((f|0)!=(l|0));i=p;return};case 4:{if(!(f>>>0<l>>>0)){i=p;return}v=h>>>0<j>>>0;e=e+32936|0;do{x=c[r+(f+s<<2)>>2]|0;if(v){y=c[e>>2]|0;z=h;do{A=(c[q+(z<<2)>>2]|0)+x|0;if(!u){w=d[(z&15)+(8400+(f<<4))>>0]|0|z>>>4<<8;B=0;do{J=(c[(c[t+((B&15)<<2)>>2]|0)+(w<<2)>>2]|0)+n>>m;K=J>>31;b[y+(A+B<<1)>>1]=(J&32767^K)-K;B=B+1|0}while((B|0)!=(o|0))}z=z+1|0}while((z|0)!=(j|0))}f=f+1|0}while((f|0)!=(l|0));i=p;return};case 7:{if(!(f>>>0<l>>>0)){i=p;return}v=h>>>0<j>>>0;e=e+32936|0;w=w&255;y=1<<w;x=y+ -1|0;z=127-(z<<24>>24)|0;A=23-w|0;do{H=c[r+(f+s<<2)>>2]|0;if(v){F=c[e>>2]|0;G=h;do{D=(c[q+(G<<2)>>2]|0)+H|0;if(!u){B=d[(G&15)+(8400+(f<<4))>>0]|0|G>>>4<<8;C=0;do{I=(c[(c[t+((C&15)<<2)>>2]|0)+(B<<2)>>2]|0)+n>>m;E=I>>31;I=(E^I)-E|0;K=I>>>w;J=(K|0)==0;I=(J?y:0)^(I&x|y);K=(J?1:K)+z|0;J=(I|0)<(y|0);if(J&(K|0)>1&(I|0)>0){do{K=K+ -1|0;I=I<<1;J=(I|0)<(y|0)}while(J&(K|0)>1&(I|0)>0)}g[F+(D+C<<2)>>2]=(c[k>>2]=((J?0:y)^I)<<A|E&-2147483648|(J?0:K<<23),+g[k>>2]);C=C+1|0}while((C|0)!=(o|0))}G=G+1|0}while((G|0)!=(j|0))}f=f+1|0}while((f|0)!=(l|0));i=p;return};case 3:{if(!(f>>>0<l>>>0)){i=p;return}v=h>>>0<j>>>0;e=e+32936|0;w=w&255;do{z=c[r+(f+s<<2)>>2]|0;if(v){y=c[e>>2]|0;C=h;do{B=(c[q+(C<<2)>>2]|0)+z|0;if(!u){x=d[(C&15)+(8400+(f<<4))>>0]|0|C>>>4<<8;A=0;do{D=(c[(c[t+((A&15)<<2)>>2]|0)+(x<<2)>>2]|0)+n>>m<<w;if((D|0)<-32768){D=-32768}else{D=(D|0)>32767?32767:D&65535}b[y+(B+A<<1)>>1]=D;A=A+1|0}while((A|0)!=(o|0))}C=C+1|0}while((C|0)!=(j|0))}f=f+1|0}while((f|0)!=(l|0));i=p;return};default:{pa(2968,2928,732,2952)}}}function pc(e){e=e|0;var f=0,h=0,j=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0;f=i;u=c[e+16>>2]|0;if((u|0)==1?(c[e+92>>2]|0)==7:0){F=0;i=f;return F|0}if((c[e+34932>>2]|0)!=0){F=0;i=f;return F|0}o=c[e+34928>>2]|0;if((o|0)==0){F=0;i=f;return F|0}x=(c[e+34140>>2]|0)!=0;h=x?3:0;q=c[e+34232>>2]|0;s=c[q+36>>2]|0;t=c[e+34324>>2]|0;l=t<<4;j=l+ -16|0;l=s+1+(16-l)|0;v=l>>>0<16?l:16;l=(c[q+28>>2]|0)+1|0;m=c[q+32>>2]|0;r=j>>>0>m>>>0?0:m&15;m=c[q+24>>2]|0;y=c[e+12>>2]|0;n=((y|0)==4?4:3)+(c[e+24>>2]|0)|0;o=c[o+34524>>2]|0;w=a[e+32908>>0]|0;z=a[e+32909>>0]|0;p=c[q+40>>2]|0;q=c[q+44>>2]|0;if(!((y|0)==4|(y|0)==7)){F=-1;i=f;return F|0}switch(u|0){case 6:{if(x){u=1<<h+ -1}else{u=0}if(!(r>>>0<v>>>0)){F=0;i=f;return F|0}v=m>>>0<l>>>0;w=w&255;e=e+32936|0;s=(t<<4)+ -18-s|0;s=s>>>0>4294967279?~s:16;while(1){if(v){x=(c[q+(r+j<<2)>>2]|0)+n|0;y=c[e>>2]|0;t=m;do{c[y+(x+(c[p+(t<<2)>>2]|0)<<2)>>2]=(c[o+((d[(t&15)+(8400+(r<<4))>>0]|0|t>>>4<<8)<<2)>>2]|0)+u>>h<<w;t=t+1|0}while((t|0)!=(l|0))}r=r+1|0;if((r|0)==(s|0)){h=0;break}}i=f;return h|0};case 7:{if(x){u=1<<h+ -1}else{u=0}if(!(r>>>0<v>>>0)){F=0;i=f;return F|0}v=m>>>0<l>>>0;y=w&255;x=1<<y;w=x+ -1|0;A=127-(z<<24>>24)|0;z=23-y|0;e=e+32936|0;s=(t<<4)+ -18-s|0;s=s>>>0>4294967279?~s:16;while(1){if(v){t=(c[q+(r+j<<2)>>2]|0)+n|0;B=m;do{D=(c[o+((d[(B&15)+(8400+(r<<4))>>0]|0|B>>>4<<8)<<2)>>2]|0)+u>>h;C=D>>31;D=(C^D)-C|0;F=D>>>y;E=(F|0)==0;D=(E?x:0)^(D&w|x);F=(E?1:F)+A|0;E=(D|0)<(x|0);if(E&(F|0)>1&(D|0)>0){do{F=F+ -1|0;D=D<<1;E=(D|0)<(x|0)}while(E&(F|0)>1&(D|0)>0)}g[(c[e>>2]|0)+(t+(c[p+(B<<2)>>2]|0)<<2)>>2]=(c[k>>2]=((E?0:x)^D)<<z|C&-2147483648|(E?0:F<<23),+g[k>>2]);B=B+1|0}while((B|0)!=(l|0))}r=r+1|0;if((r|0)==(s|0)){h=0;break}}i=f;return h|0};case 1:{if(x){u=1<<h+ -1}else{u=0}u=u+(1<<h+7)|0;if(!(r>>>0<v>>>0)){F=0;i=f;return F|0}v=m>>>0<l>>>0;e=e+32936|0;s=(t<<4)+ -18-s|0;s=s>>>0>4294967279?~s:16;while(1){if(v){t=(c[q+(r+j<<2)>>2]|0)+n|0;w=m;do{x=u+(c[o+((d[(w&15)+(8400+(r<<4))>>0]|0|w>>>4<<8)<<2)>>2]|0)>>h;if((x|0)<0){x=0}else{x=(x|0)>255?-1:x&255}a[(c[e>>2]|0)+(t+(c[p+(w<<2)>>2]|0))>>0]=x;w=w+1|0}while((w|0)!=(l|0))}r=r+1|0;if((r|0)==(s|0)){h=0;break}}i=f;return h|0};case 3:{if(x){u=1<<h+ -1}else{u=0}if(!(r>>>0<v>>>0)){F=0;i=f;return F|0}v=m>>>0<l>>>0;w=w&255;e=e+32936|0;s=(t<<4)+ -18-s|0;s=s>>>0>4294967279?~s:16;while(1){if(v){x=(c[q+(r+j<<2)>>2]|0)+n|0;t=m;do{y=(c[o+((d[(t&15)+(8400+(r<<4))>>0]|0|t>>>4<<8)<<2)>>2]|0)+u>>h<<w;if((y|0)<-32768){y=-32768}else{y=(y|0)>32767?32767:y&65535}b[(c[e>>2]|0)+(x+(c[p+(t<<2)>>2]|0)<<1)>>1]=y;t=t+1|0}while((t|0)!=(l|0))}r=r+1|0;if((r|0)==(s|0)){h=0;break}}i=f;return h|0};case 2:{if(x){u=1<<h+ -1}else{u=0}u=u+(1<<h+15)|0;if(!(r>>>0<v>>>0)){F=0;i=f;return F|0}v=m>>>0<l>>>0;w=w&255;e=e+32936|0;s=(t<<4)+ -18-s|0;s=s>>>0>4294967279?~s:16;while(1){if(v){t=(c[q+(r+j<<2)>>2]|0)+n|0;x=m;do{y=u+(c[o+((d[(x&15)+(8400+(r<<4))>>0]|0|x>>>4<<8)<<2)>>2]|0)>>h<<w;if((y|0)<0){y=0}else{y=(y|0)>65535?-1:y&65535}b[(c[e>>2]|0)+(t+(c[p+(x<<2)>>2]|0)<<1)>>1]=y;x=x+1|0}while((x|0)!=(l|0))}r=r+1|0;if((r|0)==(s|0)){h=0;break}}i=f;return h|0};case 4:{if(x){u=1<<h+ -1}else{u=0}if(!(r>>>0<v>>>0)){F=0;i=f;return F|0}v=m>>>0<l>>>0;e=e+32936|0;s=(t<<4)+ -18-s|0;s=s>>>0>4294967279?~s:16;while(1){if(v){w=(c[q+(r+j<<2)>>2]|0)+n|0;x=c[e>>2]|0;t=m;do{E=(c[o+((d[(t&15)+(8400+(r<<4))>>0]|0|t>>>4<<8)<<2)>>2]|0)+u>>h;F=E>>31;b[x+(w+(c[p+(t<<2)>>2]|0)<<1)>>1]=(E&32767^F)-F;t=t+1|0}while((t|0)!=(l|0))}r=r+1|0;if((r|0)==(s|0)){h=0;break}}i=f;return h|0};default:{F=-1;i=f;return F|0}}return 0}function qc(e){e=e|0;var f=0,h=0,j=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0;f=i;i=i+64|0;y=f;L=e+34128|0;if((c[L>>2]|0)==0){B=0}else{B=c[e+12>>2]|0}r=e+16|0;M=c[r>>2]|0;G=e+34140|0;t=c[G>>2]|0;H=(t|0)!=0;x=H?3:0;m=c[e+34232>>2]|0;q=c[m+36>>2]|0;J=e+34324|0;p=c[J>>2]|0;j=p<<4;h=q+1+(16-j)|0;s=h>>>0<16?h:16;h=(c[m+28>>2]|0)+1|0;l=c[m+32>>2]|0;o=(j+ -16|0)>>>0>l>>>0?0:l&15;j=c[m+24>>2]|0;u=c[e+34524>>2]|0;O=e+34240|0;l=(c[O>>2]|0)==0;v=c[(l?e+34528|0:e+34780|0)>>2]|0;w=c[(l?e+34532|0:e+34784|0)>>2]|0;A=(c[e+28>>2]|0)!=0?2:0;z=2-A|0;F=a[e+32908>>0]|0;K=a[e+32909>>0]|0;l=c[m+40>>2]|0;n=c[m+44>>2]|0;m=aa(p+ -1|0,(B|0)==1?8:16)|0;N=e+34928|0;I=c[N>>2]|0;if((I|0)!=0?(t|0)!=(c[I+34140>>2]|0):0){pa(2976,2928,894,3048)}I=e+64|0;t=s-o|0;if((ad(e,c[((c[I>>2]|0)>>>0>3?e+48|0:e+40|0)>>2]|0,t)|0)!=0){S=-1;i=f;return S|0}if((c[O>>2]|0)!=0){nc(e)}a:do{if((c[e+32924>>2]|0)==0){do{switch(M|0){case 2:{y=F&255;A=32768>>>y<<x;if(H){C=1<<x+ -1}else{C=0}z=C+A|0;switch(B|0){case 7:{A=o>>>0<s>>>0;if((c[G>>2]|0)==0){if(!A){break a}A=j>>>0<h>>>0;x=e+32936|0;B=(p<<4)+ -18-q|0;B=B>>>0>4294967279?~B:16;C=o;do{D=c[n+(C+m<<2)>>2]|0;if(A){E=c[x>>2]|0;F=j;do{J=d[(F&15)+(8400+(C<<4))>>0]|F>>>4<<8;K=c[v+(J<<2)>>2]|0;H=c[w+(J<<2)>>2]|0;J=(c[u+(J<<2)>>2]|0)+z-(0-K>>1)|0;K=J-(H+1>>1)-K|0;J=J<<y;H=K+H<<y;K=K<<y;G=(c[l+(F<<2)>>2]|0)+D|0;I=E+(G<<1)|0;if((K|J|H)>>>0>65535){if((K|0)<0){K=0}else{K=(K|0)>65535?-1:K&65535}b[I>>1]=K;if((J|0)<0){I=0}else{I=(J|0)>65535?-1:J&65535}b[E+(G+1<<1)>>1]=I;if((H|0)<0){H=0}else{H=(H|0)>65535?-1:H&65535}b[E+(G+2<<1)>>1]=H}else{b[I>>1]=K;b[E+(G+1<<1)>>1]=J;b[E+(G+2<<1)>>1]=H}F=F+1|0}while((F|0)!=(h|0))}C=C+1|0}while((C|0)!=(B|0))}else{if(!A){break a}B=j>>>0<h>>>0;A=e+32936|0;C=(p<<4)+ -18-q|0;C=C>>>0>4294967279?~C:16;D=o;do{E=c[n+(D+m<<2)>>2]|0;if(B){F=c[A>>2]|0;G=j;do{J=d[(G&15)+(8400+(D<<4))>>0]|G>>>4<<8;L=c[v+(J<<2)>>2]|0;I=c[w+(J<<2)>>2]|0;J=(c[u+(J<<2)>>2]|0)+z-(0-L>>1)|0;L=J-(I+1>>1)-L|0;J=J>>x<<y;I=L+I>>x<<y;L=L>>x<<y;H=(c[l+(G<<2)>>2]|0)+E|0;K=F+(H<<1)|0;if((L|J|I)>>>0>65535){if((L|0)<0){L=0}else{L=(L|0)>65535?-1:L&65535}b[K>>1]=L;if((J|0)<0){J=0}else{J=(J|0)>65535?-1:J&65535}b[F+(H+1<<1)>>1]=J;if((I|0)<0){I=0}else{I=(I|0)>65535?-1:I&65535}b[F+(H+2<<1)>>1]=I}else{b[K>>1]=L;b[F+(H+1<<1)>>1]=J;b[F+(H+2<<1)>>1]=I}G=G+1|0}while((G|0)!=(h|0))}D=D+1|0}while((D|0)!=(C|0))}break};case 6:case 3:case 0:{oc(e,o,j,h,s,x,z);break a};case 2:{C=c[I>>2]|0;C=((C&-2|0)==2|(C|0)==5|(C|0)==7)&1;D=C^1;if(!(o>>>0<s>>>0)){break a}A=j>>>0<h>>>0;B=e+32936|0;E=(p<<4)+ -18-q|0;E=E>>>0>4294967279?~E:16;F=o;do{G=c[n+(F+m<<2)>>2]|0;if(A){H=c[B>>2]|0;I=j;do{M=I>>>4;J=I>>>1;L=(d[(J&7)+(8400+(F<<4))>>0]|0)+(M<<7)|0;N=(c[v+(L<<2)>>2]|0)+z>>x<<y;L=(c[w+(L<<2)>>2]|0)+z>>x<<y;M=(c[u+((d[(I&15)+(8400+(F<<4))>>0]|M<<8)<<2)>>2]|0)+z>>x<<y;K=I+1|0;K=(c[u+((d[(K&15)+(8400+(F<<4))>>0]|K>>>4<<8)<<2)>>2]|0)+z>>x<<y;J=(c[l+(J<<2)>>2]|0)+G|0;if((L|N|M|K)>>>0>65535){if((N|0)<0){N=0}else{N=(N|0)>65535?-1:N&65535}b[H+(J+C<<1)>>1]=N;if((M|0)<0){M=0}else{M=(M|0)>65535?-1:M&65535}b[H+(J+D<<1)>>1]=M;if((L|0)<0){L=0}else{L=(L|0)>65535?-1:L&65535}b[H+(J+2<<1)>>1]=L;if((K|0)<0){K=0}else{K=(K|0)>65535?-1:K&65535}b[H+(J+3<<1)>>1]=K}else{b[H+(J+C<<1)>>1]=N;b[H+(J+D<<1)>>1]=M;b[H+(J+2<<1)>>1]=L;b[H+(J+3<<1)>>1]=K}I=I+2|0}while(I>>>0<h>>>0)}F=F+1|0}while((F|0)!=(E|0));break};case 1:{A=c[I>>2]|0;D=c[3064+(A<<4)>>2]|0;C=c[3068+(A<<4)>>2]|0;B=c[3072+(A<<4)>>2]|0;A=c[3076+(A<<4)>>2]|0;if(!(o>>>0<s>>>0)){break a}K=j>>>0<h>>>0;L=e+32936|0;J=o;do{I=c[n+((J>>>1)+m<<2)>>2]|0;if(K){G=J+1|0;H=c[L>>2]|0;F=j;do{E=F>>>1;M=(d[(E&7)+(8400+(J<<4))>>0]|0)+(F>>>3<<6)|0;N=(c[v+(M<<2)>>2]|0)+z>>x<<y;M=(c[w+(M<<2)>>2]|0)+z>>x<<y;P=F>>>4<<8;T=F&15;R=(c[u+((d[8400+(J<<4)+T>>0]|P)<<2)>>2]|0)+z>>x<<y;S=F+1|0;O=S>>>4<<8;S=S&15;Q=(c[u+((d[8400+(J<<4)+S>>0]|O)<<2)>>2]|0)+z>>x<<y;P=(c[u+((d[8400+(G<<4)+T>>0]|P)<<2)>>2]|0)+z>>x<<y;O=(c[u+((d[8400+(G<<4)+S>>0]|O)<<2)>>2]|0)+z>>x<<y;E=(c[l+(E<<2)>>2]|0)+I|0;if((M|N|R|Q|P|O)>>>0>65535){if((R|0)<0){R=0}else{R=(R|0)>65535?-1:R&65535}b[H+(E+D<<1)>>1]=R;if((Q|0)<0){Q=0}else{Q=(Q|0)>65535?-1:Q&65535}b[H+(E+C<<1)>>1]=Q;if((P|0)<0){P=0}else{P=(P|0)>65535?-1:P&65535}b[H+(E+B<<1)>>1]=P;if((O|0)<0){O=0}else{O=(O|0)>65535?-1:O&65535}b[H+(E+A<<1)>>1]=O;if((N|0)<0){N=0}else{N=(N|0)>65535?-1:N&65535}b[H+(E+4<<1)>>1]=N;if((M|0)<0){M=0}else{M=(M|0)>65535?-1:M&65535}b[H+(E+5<<1)>>1]=M}else{b[H+(E+D<<1)>>1]=R;b[H+(E+C<<1)>>1]=Q;b[H+(E+B<<1)>>1]=P;b[H+(E+A<<1)>>1]=O;b[H+(E+4<<1)>>1]=N;b[H+(E+5<<1)>>1]=M}F=F+2|0}while(F>>>0<h>>>0)}J=J+2|0}while(J>>>0<s>>>0);break};case 4:{D=c[e+34536>>2]|0;if(!(o>>>0<s>>>0)){break a}B=j>>>0<h>>>0;z=e+32936|0;E=(p<<4)+ -18-q|0;E=E>>>0>4294967279?~E:16;F=o;do{G=c[n+(F+m<<2)>>2]|0;if(B){H=c[z>>2]|0;I=j;do{K=d[(I&15)+(8400+(F<<4))>>0]|I>>>4<<8;M=A-(c[u+(K<<2)>>2]|0)|0;J=c[v+(K<<2)>>2]|0;L=c[w+(K<<2)>>2]|0;K=(c[D+(K<<2)>>2]|0)+C-(M+1>>1)|0;M=K-(J>>1)+M|0;J=M-(1-L>>1)+J|0;O=J>>x<<y;M=M>>x<<y;L=J-L>>x<<y;K=K>>x<<y;J=(c[l+(I<<2)>>2]|0)+G|0;N=H+(J<<1)|0;if((M|K|O|L)>>>0>65535){if((O|0)<0){O=0}else{O=(O|0)>65535?-1:O&65535}b[N>>1]=O;if((M|0)<0){M=0}else{M=(M|0)>65535?-1:M&65535}b[H+(J+1<<1)>>1]=M;if((L|0)<0){L=0}else{L=(L|0)>65535?-1:L&65535}b[H+(J+2<<1)>>1]=L;if((K|0)<0){K=0}else{K=(K|0)>65535?-1:K&65535}b[H+(J+3<<1)>>1]=K}else{b[N>>1]=O;b[H+(J+1<<1)>>1]=M;b[H+(J+2<<1)>>1]=L;b[H+(J+3<<1)>>1]=K}I=I+1|0}while((I|0)!=(h|0))}F=F+1|0}while((F|0)!=(E|0));break};default:{pa(2968,2928,1337,3048)}}break};case 1:{D=128<<x;E=c[G>>2]|0;C=(E|0)!=0?3:0;y=C+D|0;switch(B|0){case 7:{B=c[N>>2]|0;if((B|0)!=0?(a[e+112>>0]|0)!=0:0){B=c[B+34524>>2]|0;C=o>>>0<s>>>0;if((E|0)==0){if(!C){break a}C=j>>>0<h>>>0;x=e+32936|0;D=(p<<4)+ -18-q|0;E=D>>>0>4294967279?~D:16;F=o;while(1){G=c[n+(F+m<<2)>>2]|0;if(C){H=j;do{L=d[(H&15)+(8400+(F<<4))>>0]|H>>>4<<8;M=c[v+(L<<2)>>2]|0;K=c[w+(L<<2)>>2]|0;J=(c[B+(L<<2)>>2]|0)+y|0;L=(c[u+(L<<2)>>2]|0)+y-(0-M>>1)|0;M=L-(K+1>>1)-M|0;K=M+K|0;I=c[x>>2]|0;D=(c[l+(H<<2)>>2]|0)+G|0;if((J|L|M|K)>>>0>255){if((M|0)<0){M=0}else{M=(M|0)>255?-1:M&255}a[I+(D+z)>>0]=M;if((L|0)<0){L=0}else{L=(L|0)>255?-1:L&255}a[I+(D+1)>>0]=L;if((K|0)<0){K=0}else{K=(K|0)>255?-1:K&255}a[I+(D+A)>>0]=K;if((J|0)<0){J=0}else{J=(J|0)>255?-1:J&255}a[I+(D+3)>>0]=J}else{a[I+(D+z)>>0]=M;a[I+(D+1)>>0]=L;a[I+(D+A)>>0]=K;a[I+(D+3)>>0]=J}H=H+1|0}while((H|0)!=(h|0))}F=F+1|0;if((F|0)==(E|0)){break a}}}else{if(!C){break a}D=j>>>0<h>>>0;C=e+32936|0;E=(p<<4)+ -18-q|0;E=E>>>0>4294967279?~E:16;F=o;while(1){G=c[n+(F+m<<2)>>2]|0;if(D){H=j;do{K=d[(H&15)+(8400+(F<<4))>>0]|H>>>4<<8;N=c[v+(K<<2)>>2]|0;L=c[w+(K<<2)>>2]|0;M=(c[u+(K<<2)>>2]|0)+y-(0-N>>1)|0;N=M-(L+1>>1)-N|0;M=M>>x;L=N+L>>x;N=N>>x;K=(c[B+(K<<2)>>2]|0)+y>>x;I=c[C>>2]|0;J=(c[l+(H<<2)>>2]|0)+G|0;if((K|M|N|L)>>>0>255){if((N|0)<0){N=0}else{N=(N|0)>255?-1:N&255}a[I+(J+z)>>0]=N;if((M|0)<0){M=0}else{M=(M|0)>255?-1:M&255}a[I+(J+1)>>0]=M;if((L|0)<0){L=0}else{L=(L|0)>255?-1:L&255}a[I+(J+A)>>0]=L;if((K|0)<0){K=0}else{K=(K|0)>255?-1:K&255}a[I+(J+3)>>0]=K}else{a[I+(J+z)>>0]=N;a[I+(J+1)>>0]=M;a[I+(J+A)>>0]=L;a[I+(J+3)>>0]=K}H=H+1|0}while((H|0)!=(h|0))}F=F+1|0;if((F|0)==(E|0)){break a}}}}B=o>>>0<s>>>0;if((E|0)==0){if(!B){break a}x=j>>>0<h>>>0;B=e+32936|0;C=(p<<4)+ -18-q|0;H=C>>>0>4294967279?~C:16;G=o;do{F=c[n+(G+m<<2)>>2]|0;if(x){E=j;do{J=d[(E&15)+(8400+(G<<4))>>0]|E>>>4<<8;K=c[v+(J<<2)>>2]|0;I=c[w+(J<<2)>>2]|0;J=(c[u+(J<<2)>>2]|0)+y-(0-K>>1)|0;K=J-(I+1>>1)-K|0;I=K+I|0;D=c[B>>2]|0;C=(c[l+(E<<2)>>2]|0)+F|0;if((K|J|I)>>>0>255){if((K|0)<0){K=0}else{K=(K|0)>255?-1:K&255}a[D+(C+z)>>0]=K;if((J|0)<0){J=0}else{J=(J|0)>255?-1:J&255}a[D+(C+1)>>0]=J;if((I|0)<0){I=0}else{I=(I|0)>255?-1:I&255}a[D+(C+A)>>0]=I}else{a[D+(C+z)>>0]=K;a[D+(C+1)>>0]=J;a[D+(C+A)>>0]=I}E=E+1|0}while((E|0)!=(h|0))}G=G+1|0}while((G|0)!=(H|0))}else{if(!B){break a}B=j>>>0<h>>>0;C=e+32936|0;D=(p<<4)+ -18-q|0;E=D>>>0>4294967279?~D:16;F=o;do{G=c[n+(F+m<<2)>>2]|0;if(B){H=j;do{K=d[(H&15)+(8400+(F<<4))>>0]|H>>>4<<8;L=c[v+(K<<2)>>2]|0;J=c[w+(K<<2)>>2]|0;K=(c[u+(K<<2)>>2]|0)+y-(0-L>>1)|0;L=K-(J+1>>1)-L|0;K=K>>x;J=L+J>>x;L=L>>x;I=c[C>>2]|0;D=(c[l+(H<<2)>>2]|0)+G|0;if((L|K|J)>>>0>255){if((L|0)<0){L=0}else{L=(L|0)>255?-1:L&255}a[I+(D+z)>>0]=L;if((K|0)<0){K=0}else{K=(K|0)>255?-1:K&255}a[I+(D+1)>>0]=K;if((J|0)<0){J=0}else{J=(J|0)>255?-1:J&255}a[I+(D+A)>>0]=J}else{a[I+(D+z)>>0]=L;a[I+(D+1)>>0]=K;a[I+(D+A)>>0]=J}H=H+1|0}while((H|0)!=(h|0))}F=F+1|0}while((F|0)!=(E|0))}break};case 6:case 3:case 0:{oc(e,o,j,h,s,x,y);break a};case 1:{J=c[I>>2]|0;G=c[3064+(J<<4)>>2]|0;H=c[3068+(J<<4)>>2]|0;I=c[3072+(J<<4)>>2]|0;J=c[3076+(J<<4)>>2]|0;if(!(o>>>0<s>>>0)){break a}K=j>>>0<h>>>0;D=e+32936|0;E=o;do{C=E>>>1;z=c[n+(C+m<<2)>>2]|0;if(K){F=E+1|0;B=j;do{P=B>>>4;A=B>>>1;M=(d[(A&7)+(8656+(C<<3))>>0]|0)+(P<<6)|0;N=(c[v+(M<<2)>>2]|0)+y>>x;M=(c[w+(M<<2)>>2]|0)+y>>x;P=P<<8;T=B&15;R=(c[u+((d[8400+(E<<4)+T>>0]|P)<<2)>>2]|0)+y>>x;L=B+1|0;O=L>>>4<<8;L=L&15;Q=(c[u+((d[8400+(E<<4)+L>>0]|O)<<2)>>2]|0)+y>>x;P=(c[u+((d[8400+(F<<4)+T>>0]|P)<<2)>>2]|0)+y>>x;O=(c[u+((d[8400+(F<<4)+L>>0]|O)<<2)>>2]|0)+y>>x;L=c[D>>2]|0;A=(c[l+(A<<2)>>2]|0)+z|0;if((M|N|R|Q|P|O)>>>0>255){if((R|0)<0){R=0}else{R=(R|0)>255?-1:R&255}a[L+(A+G)>>0]=R;if((Q|0)<0){Q=0}else{Q=(Q|0)>255?-1:Q&255}a[L+(A+H)>>0]=Q;if((P|0)<0){P=0}else{P=(P|0)>255?-1:P&255}a[L+(A+I)>>0]=P;if((O|0)<0){O=0}else{O=(O|0)>255?-1:O&255}a[L+(A+J)>>0]=O;if((N|0)<0){N=0}else{N=(N|0)>255?-1:N&255}a[L+(A+4)>>0]=N;if((M|0)<0){M=0}else{M=(M|0)>255?-1:M&255}a[L+(A+5)>>0]=M}else{a[L+(A+G)>>0]=R;a[L+(A+H)>>0]=Q;a[L+(A+I)>>0]=P;a[L+(A+J)>>0]=O;a[L+(A+4)>>0]=N;a[L+(A+5)>>0]=M}B=B+2|0}while(B>>>0<h>>>0)}E=E+2|0}while(E>>>0<s>>>0);break};case 8:{if(!(o>>>0<s>>>0)){break a}y=j>>>0<h>>>0;z=e+32936|0;A=(p<<4)+ -18-q|0;E=A>>>0>4294967279?~A:16;D=o;do{B=c[n+(D+m<<2)>>2]|0;if(y){A=j;do{P=d[(A&15)+(8400+(D<<4))>>0]|A>>>4<<8;Q=c[v+(P<<2)>>2]|0;R=c[w+(P<<2)>>2]|0;P=(c[u+(P<<2)>>2]|0)+C-(0-Q>>1)|0;Q=P-(R+1>>1)-Q|0;S=c[z>>2]|0;T=(c[l+(A<<2)>>2]|0)+B|0;rc(Q>>x,P>>x,Q+R>>x,S+T|0,S+(T+1)|0,S+(T+2)|0,S+(T+3)|0);A=A+1|0}while((A|0)!=(h|0))}D=D+1|0}while((D|0)!=(E|0));break};case 2:{if(!(o>>>0<s>>>0)){break a}A=j>>>0<h>>>0;z=e+32936|0;B=(p<<4)+ -18-q|0;D=B>>>0>4294967279?~B:16;E=o;do{F=c[n+(E+m<<2)>>2]|0;if(A){G=j;do{K=G>>>4;C=G>>>1;I=(d[(C&7)+(8400+(E<<4))>>0]|0)+(K<<7)|0;L=(c[v+(I<<2)>>2]|0)+y>>x;I=(c[w+(I<<2)>>2]|0)+y>>x;K=(c[u+((d[(G&15)+(8400+(E<<4))>>0]|K<<8)<<2)>>2]|0)+y>>x;H=G+1|0;H=(c[u+((d[(H&15)+(8400+(E<<4))>>0]|H>>>4<<8)<<2)>>2]|0)+y>>x;B=c[z>>2]|0;C=(c[l+(C<<2)>>2]|0)+F|0;J=B+C|0;if((I|L|K|H)>>>0>255){if((L|0)<0){L=0}else{L=(L|0)>255?-1:L&255}a[J>>0]=L;if((K|0)<0){J=0}else{J=(K|0)>255?-1:K&255}a[B+(C+1)>>0]=J;if((I|0)<0){I=0}else{I=(I|0)>255?-1:I&255}a[B+(C+2)>>0]=I;if((H|0)<0){H=0}else{H=(H|0)>255?-1:H&255}a[B+(C+3)>>0]=H}else{a[J>>0]=L;a[B+(C+1)>>0]=K;a[B+(C+2)>>0]=I;a[B+(C+3)>>0]=H}G=G+2|0}while(G>>>0<h>>>0)}E=E+1|0}while((E|0)!=(D|0));break};case 4:{A=c[e+34536>>2]|0;if(!(o>>>0<s>>>0)){break a}y=j>>>0<h>>>0;z=e+32936|0;B=(p<<4)+ -18-q|0;H=B>>>0>4294967279?~B:16;G=o;do{F=c[n+(G+m<<2)>>2]|0;if(y){E=j;do{J=d[(E&15)+(8400+(G<<4))>>0]|E>>>4<<8;L=D-(c[u+(J<<2)>>2]|0)|0;B=c[v+(J<<2)>>2]|0;K=c[w+(J<<2)>>2]|0;J=(c[A+(J<<2)>>2]|0)+C-(L+1>>1)|0;L=J-(B>>1)+L|0;B=L-(1-K>>1)+B|0;N=B>>x;L=L>>x;K=B-K>>x;J=J>>x;B=c[z>>2]|0;I=(c[l+(E<<2)>>2]|0)+F|0;M=B+I|0;if((L|J|N|K)>>>0>255){if((N|0)<0){N=0}else{N=(N|0)>255?-1:N&255}a[M>>0]=N;if((L|0)<0){L=0}else{L=(L|0)>255?-1:L&255}a[B+(I+1)>>0]=L;if((K|0)<0){K=0}else{K=(K|0)>255?-1:K&255}a[B+(I+2)>>0]=K;if((J|0)<0){J=0}else{J=(J|0)>255?-1:J&255}a[B+(I+3)>>0]=J}else{a[M>>0]=N;a[B+(I+1)>>0]=L;a[B+(I+2)>>0]=K;a[B+(I+3)>>0]=J}E=E+1|0}while((E|0)!=(h|0))}G=G+1|0}while((G|0)!=(H|0));break};default:{pa(2968,2928,1178,3048)}}break};case 4:{y=(c[G>>2]|0)!=0?3:0;if((B|0)==7){if(!(o>>>0<s>>>0)){break a}z=j>>>0<h>>>0;A=e+32936|0;B=(p<<4)+ -18-q|0;C=B>>>0>4294967279?~B:16;E=o;do{D=c[n+(E+m<<2)>>2]|0;if(z){F=c[A>>2]|0;B=j;do{P=d[(B&15)+(8400+(E<<4))>>0]|B>>>4<<8;S=c[v+(P<<2)>>2]|0;R=c[w+(P<<2)>>2]|0;P=(c[u+(P<<2)>>2]|0)+y-(0-S>>1)|0;S=P-(R+1>>1)-S|0;T=(c[l+(B<<2)>>2]|0)+D|0;O=S>>x;Q=O>>31;b[F+(T<<1)>>1]=(O&32767^Q)-Q;P=P>>x;Q=P>>31;b[F+(T+1<<1)>>1]=(P&32767^Q)-Q;R=S+R>>x;S=R>>31;b[F+(T+2<<1)>>1]=(R&32767^S)-S;B=B+1|0}while((B|0)!=(h|0))}E=E+1|0}while((E|0)!=(C|0))}else if((B|0)==6|(B|0)==3|(B|0)==0){oc(e,o,j,h,s,x,y);break a}else{pa(2968,2928,1433,3048)}break};case 5:{y=F&255;if(H){z=1<<x+ -1}else{z=0}z=z+(-2147483648>>y<<x)|0;if((B|0)==7){if(!(o>>>0<s>>>0)){break a}A=j>>>0<h>>>0;B=e+32936|0;C=(p<<4)+ -18-q|0;F=C>>>0>4294967279?~C:16;G=o;do{D=c[n+(G+m<<2)>>2]|0;if(A){C=c[B>>2]|0;E=j;do{Q=d[(E&15)+(8400+(G<<4))>>0]|E>>>4<<8;R=c[v+(Q<<2)>>2]|0;S=c[w+(Q<<2)>>2]|0;Q=(c[u+(Q<<2)>>2]|0)+z-(0-R>>1)|0;R=Q-(S+1>>1)-R|0;T=(c[l+(E<<2)>>2]|0)+D|0;c[C+(T<<2)>>2]=R>>x<<y;c[C+(T+1<<2)>>2]=Q>>x<<y;c[C+(T+2<<2)>>2]=R+S>>x<<y;E=E+1|0}while((E|0)!=(h|0))}G=G+1|0}while((G|0)!=(F|0))}else if((B|0)==6|(B|0)==3|(B|0)==0){oc(e,o,j,h,s,x,z);break a}else{pa(2968,2928,1472,3048)}break};case 6:{y=(c[G>>2]|0)!=0?3:0;if((B|0)==7){if(!(o>>>0<s>>>0)){break a}z=j>>>0<h>>>0;A=e+32936|0;B=F&255;C=(p<<4)+ -18-q|0;G=C>>>0>4294967279?~C:16;C=o;do{D=c[n+(C+m<<2)>>2]|0;if(z){E=c[A>>2]|0;F=j;do{Q=d[(F&15)+(8400+(C<<4))>>0]|F>>>4<<8;R=c[v+(Q<<2)>>2]|0;S=c[w+(Q<<2)>>2]|0;Q=(c[u+(Q<<2)>>2]|0)+y-(0-R>>1)|0;R=Q-(S+1>>1)-R|0;T=(c[l+(F<<2)>>2]|0)+D|0;c[E+(T<<2)>>2]=R>>x<<B;c[E+(T+1<<2)>>2]=Q>>x<<B;c[E+(T+2<<2)>>2]=R+S>>x<<B;F=F+1|0}while((F|0)!=(h|0))}C=C+1|0}while((C|0)!=(G|0))}else if((B|0)==6|(B|0)==3|(B|0)==0){oc(e,o,j,h,s,x,y);break a}else{pa(2968,2928,1510,3048)}break};case 7:{y=(c[G>>2]|0)!=0?3:0;if((B|0)==7){if(!(o>>>0<s>>>0)){break a}z=j>>>0<h>>>0;A=e+32936|0;C=F&255;D=1<<C;B=D+ -1|0;E=127-(K<<24>>24)|0;F=23-C|0;G=(p<<4)+ -18-q|0;G=G>>>0>4294967279?~G:16;K=o;do{J=c[n+(K+m<<2)>>2]|0;if(z){I=c[A>>2]|0;L=j;do{O=d[(L&15)+(8400+(K<<4))>>0]|L>>>4<<8;Q=c[v+(O<<2)>>2]|0;M=c[w+(O<<2)>>2]|0;O=(c[u+(O<<2)>>2]|0)+y-(0-Q>>1)|0;Q=O-(M+1>>1)-Q|0;M=Q+M|0;H=(c[l+(L<<2)>>2]|0)+J|0;N=I+(H<<2)|0;Q=Q>>x;P=Q>>31;Q=(P^Q)-P|0;S=Q>>>C;R=(S|0)==0;Q=(R?D:0)^(Q&B|D);S=(R?1:S)+E|0;R=(Q|0)<(D|0);if(R&(S|0)>1&(Q|0)>0){do{S=S+ -1|0;Q=Q<<1;R=(Q|0)<(D|0)}while(R&(S|0)>1&(Q|0)>0)}g[N>>2]=(c[k>>2]=((R?0:D)^Q)<<F|P&-2147483648|(R?0:S<<23),+g[k>>2]);O=O>>x;N=O>>31;O=(N^O)-N|0;Q=O>>>C;P=(Q|0)==0;O=(P?D:0)^(O&B|D);Q=(P?1:Q)+E|0;P=(O|0)<(D|0);if(P&(Q|0)>1&(O|0)>0){do{Q=Q+ -1|0;O=O<<1;P=(O|0)<(D|0)}while(P&(Q|0)>1&(O|0)>0)}g[I+(H+1<<2)>>2]=(c[k>>2]=((P?0:D)^O)<<F|N&-2147483648|(P?0:Q<<23),+g[k>>2]);N=M>>x;M=N>>31;N=(M^N)-M|0;P=N>>>C;O=(P|0)==0;N=(O?D:0)^(N&B|D);P=(O?1:P)+E|0;O=(N|0)<(D|0);if(O&(P|0)>1&(N|0)>0){do{P=P+ -1|0;N=N<<1;O=(N|0)<(D|0)}while(O&(P|0)>1&(N|0)>0)}g[I+(H+2<<2)>>2]=(c[k>>2]=((O?0:D)^N)<<F|M&-2147483648|(O?0:P<<23),+g[k>>2]);L=L+1|0}while((L|0)!=(h|0))}K=K+1|0}while((K|0)!=(G|0))}else if((B|0)==6|(B|0)==3|(B|0)==0){oc(e,o,j,h,s,x,y);break a}else{pa(2968,2928,1547,3048)}break};case 8:{y=((c[G>>2]|0)!=0?3:0)+(16<<x)|0;if((B|0)!=7){pa(3192,2928,1556,3048)}if(!(o>>>0<s>>>0)){break a}B=j>>>0<h>>>0;z=e+32936|0;A=e+34132|0;C=(p<<4)+ -18-q|0;D=C>>>0>4294967279?~C:16;E=o;do{I=c[n+(E+m<<2)>>2]|0;if(B){F=c[z>>2]|0;G=c[A>>2]|0;H=j;do{L=d[(H&15)+(8400+(E<<4))>>0]|H>>>4<<8;K=c[v+(L<<2)>>2]|0;M=c[w+(L<<2)>>2]|0;L=y+(c[u+(L<<2)>>2]|0)-(0-K>>1)|0;K=L-(M+1>>1)-K|0;L=L>>x;M=K+M>>x;K=K>>x;C=F+((c[l+(H<<2)>>2]|0)+I<<1)|0;if((G|0)==0){if((K|0)<0){J=0}else{J=(K|0)>31?31:K&65535}if((L|0)<0){K=0}else{K=(L|0)>31?992:L<<5&2097120}if((M|0)<0){L=0}else{L=(M|0)>31?31744:M<<10&67107840}J=K+J+L|0}else{if((M|0)<0){J=0}else{J=(M|0)>31?31:M&65535}if((L|0)<0){L=0}else{L=(L|0)>31?992:L<<5&2097120}if((K|0)<0){K=0}else{K=(K|0)>31?31744:K<<10&67107840}J=L+J+K|0}b[C>>1]=J;H=H+1|0}while((H|0)!=(h|0))}E=E+1|0}while((E|0)!=(D|0));break};case 10:{y=((c[G>>2]|0)!=0?3:0)+(32<<x)|0;if((B|0)!=7){pa(3192,2928,1579,3048)}if(!(o>>>0<s>>>0)){break a}C=j>>>0<h>>>0;A=x+1|0;B=e+32936|0;z=e+34132|0;D=(p<<4)+ -18-q|0;J=D>>>0>4294967279?~D:16;D=o;do{E=c[n+(D+m<<2)>>2]|0;if(C){F=c[B>>2]|0;G=c[z>>2]|0;H=j;do{M=d[(H&15)+(8400+(D<<4))>>0]|H>>>4<<8;L=c[v+(M<<2)>>2]|0;N=c[w+(M<<2)>>2]|0;M=y+(c[u+(M<<2)>>2]|0)-(0-L>>1)|0;L=M-(N+1>>1)-L|0;M=M>>x;N=L+N>>A;L=L>>A;I=F+((c[l+(H<<2)>>2]|0)+E<<1)|0;if((G|0)==0){if((L|0)<0){K=0}else{K=(L|0)>31?31:L&65535}if((M|0)<0){L=0}else{L=(M|0)>63?2016:M<<5&2097120}if((N|0)<0){M=0}else{M=(N|0)>31?63488:N<<11&134215680}K=L+K+M|0}else{if((N|0)<0){K=0}else{K=(N|0)>31?31:N&65535}if((M|0)<0){M=0}else{M=(M|0)>63?2016:M<<5&2097120}if((L|0)<0){L=0}else{L=(L|0)>31?63488:L<<11&134215680}K=M+K+L|0}b[I>>1]=K;H=H+1|0}while((H|0)!=(h|0))}D=D+1|0}while((D|0)!=(J|0));break};case 9:{y=((c[G>>2]|0)!=0?3:0)+(512<<x)|0;if((B|0)!=7){pa(3192,2928,1602,3048)}if(!(o>>>0<s>>>0)){break a}B=j>>>0<h>>>0;A=e+32936|0;z=e+34132|0;C=(p<<4)+ -18-q|0;E=C>>>0>4294967279?~C:16;C=o;do{G=c[n+(C+m<<2)>>2]|0;if(B){F=c[A>>2]|0;H=j;do{J=d[(H&15)+(8400+(C<<4))>>0]|H>>>4<<8;K=c[v+(J<<2)>>2]|0;I=c[w+(J<<2)>>2]|0;J=y+(c[u+(J<<2)>>2]|0)-(0-K>>1)|0;K=J-(I+1>>1)-K|0;J=J>>x;I=K+I>>x;K=K>>x;D=F+((c[l+(H<<2)>>2]|0)+G<<2)|0;if((c[z>>2]|0)==0){if((K|0)<0){K=0}else{K=(K|0)>1023?1023:K}if((J|0)<0){J=0}else{J=(J|0)>1023?1047552:J<<10}if((I|0)<0){I=0}else{I=(I|0)>1023?1072693248:I<<20}I=J+K+I|0}else{if((I|0)<0){I=0}else{I=(I|0)>1023?1023:I}if((J|0)<0){J=0}else{J=(J|0)>1023?1047552:J<<10}if((K|0)<0){K=0}else{K=(K|0)>1023?1072693248:K<<20}I=J+I+K|0}c[D>>2]=I;H=H+1|0}while((H|0)!=(h|0))}C=C+1|0}while((C|0)!=(E|0));break};case 0:{v=c[e+24>>2]|0;if(H){w=1<<x+ -1}else{w=1}if((B|0)!=0){pa(3208,2928,1628,3048)}if((c[L>>2]|0)!=0){pa(3208,2928,1628,3048)}x=o>>>0<s>>>0;if((c[I>>2]|0)>>>0<4){if(!x){break a}x=j>>>0<h>>>0;z=e+32936|0;y=e+32912|0;A=(p<<4)+ -18-q|0;A=A>>>0>4294967279?~A:16;B=o;while(1){C=(c[n+(B+m<<2)>>2]|0)+v|0;if(x){D=j;do{R=c[l+(D<<2)>>2]|0;T=(c[z>>2]|0)+(C+(R>>>3))|0;R=R&7^7;S=d[T>>0]|0;a[T>>0]=((S>>>R)+(c[y>>2]|0)+((c[u+((d[(D&15)+(8400+(B<<4))>>0]|D>>>4<<8)<<2)>>2]|0)>=(w|0))&1)<<R^S;D=D+1|0}while((D|0)!=(h|0))}B=B+1|0;if((B|0)==(A|0)){break a}}}if(!x){break a}z=j>>>0<h>>>0;x=e+32936|0;y=e+32912|0;A=(p<<4)+ -18-q|0;A=A>>>0>4294967279?~A:16;B=o;do{D=(c[n+(B+m<<2)>>2]|0)+v|0;if(z){C=D>>>3;D=D&7^7;E=j;do{T=(c[x>>2]|0)+((c[l+(E<<2)>>2]|0)+C)|0;S=d[T>>0]|0;a[T>>0]=((S>>>D)+(c[y>>2]|0)+((c[u+((d[(E&15)+(8400+(B<<4))>>0]|E>>>4<<8)<<2)>>2]|0)>=(w|0))&1)<<D^S;E=E+1|0}while((E|0)!=(h|0))}B=B+1|0}while((B|0)!=(A|0));break};case 3:{y=(c[G>>2]|0)!=0?3:0;switch(B|0){case 6:case 3:case 0:{oc(e,o,j,h,s,x,y);break a};case 4:{A=c[e+34536>>2]|0;if(!(o>>>0<s>>>0)){break a}z=j>>>0<h>>>0;B=F&255;C=e+32936|0;D=(p<<4)+ -18-q|0;H=D>>>0>4294967279?~D:16;G=o;do{F=c[n+(G+m<<2)>>2]|0;if(z){E=c[C>>2]|0;D=j;do{S=d[(D&15)+(8400+(G<<4))>>0]|D>>>4<<8;P=c[u+(S<<2)>>2]|0;Q=c[v+(S<<2)>>2]|0;R=c[w+(S<<2)>>2]|0;S=y-(1-P>>1)+(c[A+(S<<2)>>2]|0)|0;P=S-(Q>>1)-P|0;Q=P-(1-R>>1)+Q|0;T=(c[l+(D<<2)>>2]|0)+F|0;b[E+(T<<1)>>1]=Q>>x<<B;b[E+(T+1<<1)>>1]=P>>x<<B;b[E+(T+2<<1)>>1]=Q-R>>x<<B;b[E+(T+3<<1)>>1]=S>>x<<B;D=D+1|0}while((D|0)!=(h|0))}G=G+1|0}while((G|0)!=(H|0));break};case 7:{if(!(o>>>0<s>>>0)){break a}z=j>>>0<h>>>0;B=F&255;A=e+32936|0;C=(p<<4)+ -18-q|0;C=C>>>0>4294967279?~C:16;D=o;do{E=c[n+(D+m<<2)>>2]|0;if(z){F=c[A>>2]|0;G=j;do{J=d[(G&15)+(8400+(D<<4))>>0]|G>>>4<<8;H=c[v+(J<<2)>>2]|0;I=c[w+(J<<2)>>2]|0;J=(c[u+(J<<2)>>2]|0)+y-(0-H>>1)|0;H=J-(I+1>>1)-H|0;K=H>>x<<B;J=J>>x<<B;I=H+I>>x<<B;H=(c[l+(G<<2)>>2]|0)+E|0;if((K|0)<-32768){K=-32768}else{K=(K|0)>32767?32767:K&65535}b[F+(H<<1)>>1]=K;if((J|0)<-32768){J=-32768}else{J=(J|0)>32767?32767:J&65535}b[F+(H+1<<1)>>1]=J;if((I|0)<-32768){I=-32768}else{I=(I|0)>32767?32767:I&65535}b[F+(H+2<<1)>>1]=I;G=G+1|0}while((G|0)!=(h|0))}D=D+1|0}while((D|0)!=(C|0));break};default:{pa(2968,2928,1395,3048)}}break};default:{break a}}}while(0)}else{B=c[L>>2]|0;x=e+32944|0;A=c[x>>2]|0;z=(c[e+32936>>2]|0)+((aa(aa(A,(c[J>>2]|0)+ -1|0)|0,(B|0)==1?8:16)|0)>>>2<<2)|0;switch(B|0){case 6:case 3:case 0:{do{if((c[e+12>>2]|0)!=0){u=c[e+108>>2]|0;if(u>>>0<17){if((u|0)==0){C=0;D=1;break}else{C=u;E=17;break}}else{pa(2912,2928,917,3048)}}else{C=1;E=17}}while(0);if((E|0)==17){u=0;while(1){c[y+((u&15)<<2)>>2]=c[e+(u<<2)+34524>>2];u=u+1|0;if((u|0)==(C|0)){D=0;break}}}if(!(o>>>0<s>>>0)){break a}u=j>>>0<h>>>0;v=(p<<4)+ -18-q|0;v=v>>>0>4294967279?~v:16;w=o;while(1){if(u){A=j;B=z;do{if(!D){E=d[(A&15)+(8400+(w<<4))>>0]|A>>>4<<8;F=0;G=B;while(1){c[G>>2]=c[(c[y+((F&15)<<2)>>2]|0)+(E<<2)>>2];F=F+1|0;if((F|0)==(C|0)){break}else{G=G+4|0}}B=B+(C<<2)|0}A=A+1|0}while((A|0)!=(h|0));B=c[x>>2]|0}else{B=A}w=w+1|0;if((w|0)==(v|0)){break}else{A=B;z=z+(B>>>2<<2)|0}}break};case 2:{if(!(o>>>0<s>>>0)){break a}y=j>>>0<h>>>0;B=(p<<4)+ -18-q|0;B=B>>>0>4294967279?~B:16;C=o;while(1){if(y){A=j;D=z;while(1){R=A>>>4;Q=(d[(A>>>1&7)+(8400+(C<<4))>>0]|0)+(R<<7)|0;S=c[w+(Q<<2)>>2]|0;R=c[u+((d[(A&15)+(8400+(C<<4))>>0]|R<<8)<<2)>>2]|0;T=A+1|0;T=c[u+((d[(T&15)+(8400+(C<<4))>>0]|T>>>4<<8)<<2)>>2]|0;c[D>>2]=c[v+(Q<<2)>>2];c[D+4>>2]=R;c[D+8>>2]=S;c[D+12>>2]=T;A=A+2|0;if(!(A>>>0<h>>>0)){break}else{D=D+16|0}}D=c[x>>2]|0}else{D=A}C=C+1|0;if((C|0)==(B|0)){break}else{A=D;z=z+(D>>>2<<2)|0}}break};case 1:{if(!(o>>>0<s>>>0)){break a}y=j>>>0<h>>>0;B=o;while(1){if(y){D=B>>>1;C=B+1|0;E=j;A=z;while(1){O=E>>>4;T=(d[(E>>>1&7)+(8656+(D<<3))>>0]|0)+(O<<6)|0;S=c[v+(T<<2)>>2]|0;T=c[w+(T<<2)>>2]|0;O=O<<8;N=E&15;M=E+1|0;R=M>>>4<<8;M=M&15;P=c[u+((d[8400+(B<<4)+M>>0]|R)<<2)>>2]|0;Q=c[u+((d[8400+(C<<4)+N>>0]|O)<<2)>>2]|0;R=c[u+((d[8400+(C<<4)+M>>0]|R)<<2)>>2]|0;c[A>>2]=c[u+((d[8400+(B<<4)+N>>0]|O)<<2)>>2];c[A+4>>2]=P;c[A+8>>2]=Q;c[A+12>>2]=R;c[A+16>>2]=S;c[A+20>>2]=T;E=E+2|0;if(!(E>>>0<h>>>0)){break}else{A=A+24|0}}C=c[x>>2]|0}else{C=A}B=B+2|0;if(!(B>>>0<s>>>0)){break}else{A=C;z=z+(C>>>2<<2)|0}}break};default:{pa(2968,2928,982,3048)}}}}while(0);if((a[e+112>>0]|0)!=0?(pc(e)|0)!=0:0){T=-1;i=f;return T|0}u=e+32936|0;c[e+32956>>2]=t;if((c[e+12>>2]|0)!=7){T=0;i=f;return T|0}if((c[e+92>>2]|0)!=0){T=0;i=f;return T|0}switch(c[r>>2]|0){case 7:case 6:case 5:{if(!(o>>>0<s>>>0)){T=0;i=f;return T|0}e=j>>>0<h>>>0;p=(p<<4)+ -18-q|0;p=p>>>0>4294967279?~p:16;while(1){s=c[n+(o+m<<2)>>2]|0;if(e){r=c[u>>2]|0;q=j;do{T=(c[l+(q<<2)>>2]|0)+s|0;S=c[r+(T<<2)>>2]|0;c[r+(T+1<<2)>>2]=S;c[r+(T+2<<2)>>2]=S;q=q+1|0}while((q|0)!=(h|0))}o=o+1|0;if((o|0)==(p|0)){h=0;break}}i=f;return h|0};case 1:{if(!(o>>>0<s>>>0)){T=0;i=f;return T|0}e=j>>>0<h>>>0;p=(p<<4)+ -18-q|0;p=p>>>0>4294967279?~p:16;while(1){q=c[n+(o+m<<2)>>2]|0;if(e){r=j;do{S=c[u>>2]|0;T=(c[l+(r<<2)>>2]|0)+q|0;R=a[S+T>>0]|0;a[S+(T+1)>>0]=R;a[S+(T+2)>>0]=R;r=r+1|0}while((r|0)!=(h|0))}o=o+1|0;if((o|0)==(p|0)){h=0;break}}i=f;return h|0};case 4:case 3:case 2:{if(!(o>>>0<s>>>0)){T=0;i=f;return T|0}e=j>>>0<h>>>0;p=(p<<4)+ -18-q|0;p=p>>>0>4294967279?~p:16;while(1){s=c[n+(o+m<<2)>>2]|0;if(e){q=c[u>>2]|0;r=j;do{T=(c[l+(r<<2)>>2]|0)+s|0;S=b[q+(T<<1)>>1]|0;b[q+(T+1<<1)>>1]=S;b[q+(T+2<<1)>>1]=S;r=r+1|0}while((r|0)!=(h|0))}o=o+1|0;if((o|0)==(p|0)){h=0;break}}i=f;return h|0};default:{T=0;i=f;return T|0}}return 0}function rc(b,c,e,f,g,h,j){b=b|0;c=c|0;e=e|0;f=f|0;g=g|0;h=h|0;j=j|0;var k=0,l=0;k=i;do{if((b|0)>=1){l=b>>7;if((l|0)>1){a[f>>0]=b|128;l=l&255;break}else{a[f>>0]=b;l=1;break}}else{a[f>>0]=0;l=0}}while(0);do{if((c|0)>=1){b=c>>7;if((b|0)>1){a[g>>0]=c|128;c=b&255;break}else{a[g>>0]=c;c=1;break}}else{a[g>>0]=0;c=0}}while(0);do{if((e|0)>=1){b=e>>7;if((b|0)>1){a[h>>0]=e|128;e=b&255;break}else{a[h>>0]=e;e=1;break}}else{a[h>>0]=0;e=0}}while(0);b=(l&255)>(c&255)?l:c;b=(b&255)>(e&255)?b:e;a[j>>0]=b;if((b&255)>(l&255)){a[f>>0]=((d[f>>0]|0)<<1|1)>>>(((b&255)-(l&255)&255)+1|0);b=a[j>>0]|0}if((b&255)>(c&255)){a[g>>0]=((d[g>>0]|0)<<1|1)>>>(((b&255)-(c&255)&255)+1|0);b=a[j>>0]|0}if(!((b&255)>(e&255))){i=k;return}a[h>>0]=((d[h>>0]|0)<<1|1)>>>(((b&255)-(e&255)&255)+1|0);i=k;return}function sc(e,f,h,j,l){e=e|0;f=f|0;h=h|0;j=j|0;l=l|0;var m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0;o=i;i=i+64|0;r=o;p=c[e+34232>>2]|0;n=c[p+12>>2]|0;t=c[p+28>>2]|0;v=c[e+34324>>2]|0;w=(c[p+36>>2]|0)+17+(aa(v,-16)|0)|0;u=w>>>0<16;s=c[e+108>>2]|0;y=a[e+32908>>0]|0;B=a[e+32909>>0]|0;q=c[p+40>>2]|0;p=c[p+44>>2]|0;v=(v<<4)+ -16|0;m=0;while(1){if(1<<m>>>0<n>>>0){m=m+1|0}else{break}}t=t+1|0;u=u?w:16;v=(v>>>0)/(n>>>0)|0;if(!(s>>>0<17)){pa(2912,2928,1724,3264)}w=(s|0)==0;if(!w){x=0;do{c[r+((x&15)<<2)>>2]=c[e+(x<<2)+34524>>2];x=x+1|0}while((x|0)!=(s|0))}if((c[e+34240>>2]|0)!=0){c[r+4>>2]=c[e+34780>>2];c[r+8>>2]=c[e+34784>>2]}switch(c[e+16>>2]|0){case 1:{y=(128<<h|0)/(f|0)|0;if(!(u>>>0>j>>>0)){i=o;return}x=t>>>0>l>>>0;e=e+32936|0;do{z=c[p+((j>>>m)+v<<2)>>2]|0;if(x){A=l;do{E=c[e>>2]|0;D=(c[q+(A>>>m<<2)>>2]|0)+z|0;if(!w){C=d[(A&15)+(8400+(j<<4))>>0]|0|A>>>4<<8;B=0;do{F=(aa((c[(c[r+((B&15)<<2)>>2]|0)+(C<<2)>>2]|0)+y|0,f)|0)>>h;if((F|0)<0){F=0}else{F=(F|0)>255?-1:F&255}a[E+(D+B)>>0]=F;B=B+1|0}while((B|0)!=(s|0))}A=A+n|0}while(A>>>0<t>>>0)}j=j+n|0}while(j>>>0<u>>>0);i=o;return};case 3:{if(!(u>>>0>j>>>0)){i=o;return}x=t>>>0>l>>>0;e=e+32936|0;y=y&255;do{E=c[p+((j>>>m)+v<<2)>>2]|0;if(x){D=c[e>>2]|0;C=l;do{B=(c[q+(C>>>m<<2)>>2]|0)+E|0;if(!w){A=d[(C&15)+(8400+(j<<4))>>0]|0|C>>>4<<8;z=0;do{F=(aa(c[(c[r+((z&15)<<2)>>2]|0)+(A<<2)>>2]|0,f)|0)>>h<<y;if((F|0)<-32768){F=-32768}else{F=(F|0)>32767?32767:F&65535}b[D+(B+z<<1)>>1]=F;z=z+1|0}while((z|0)!=(s|0))}C=C+n|0}while(C>>>0<t>>>0)}j=j+n|0}while(j>>>0<u>>>0);i=o;return};case 2:{z=(32768<<h|0)/(f|0)|0;if(!(u>>>0>j>>>0)){i=o;return}x=t>>>0>l>>>0;e=e+32936|0;y=y&255;do{F=c[p+((j>>>m)+v<<2)>>2]|0;if(x){A=c[e>>2]|0;B=l;do{C=(c[q+(B>>>m<<2)>>2]|0)+F|0;if(!w){D=d[(B&15)+(8400+(j<<4))>>0]|0|B>>>4<<8;E=0;do{G=(aa((c[(c[r+((E&15)<<2)>>2]|0)+(D<<2)>>2]|0)+z|0,f)|0)>>h<<y;if((G|0)<0){G=0}else{G=(G|0)>65535?-1:G&65535}b[A+(C+E<<1)>>1]=G;E=E+1|0}while((E|0)!=(s|0))}B=B+n|0}while(B>>>0<t>>>0)}j=j+n|0}while(j>>>0<u>>>0);i=o;return};case 7:{if(!(u>>>0>j>>>0)){i=o;return}x=t>>>0>l>>>0;e=e+32936|0;y=y&255;A=1<<y;z=A+ -1|0;B=127-(B<<24>>24)|0;C=23-y|0;do{F=c[p+((j>>>m)+v<<2)>>2]|0;if(x){G=c[e>>2]|0;H=l;do{I=(c[q+(H>>>m<<2)>>2]|0)+F|0;if(!w){E=d[(H&15)+(8400+(j<<4))>>0]|0|H>>>4<<8;D=0;do{K=(aa(c[(c[r+((D&15)<<2)>>2]|0)+(E<<2)>>2]|0,f)|0)>>h;J=K>>31;K=(J^K)-J|0;L=K>>>y;M=(L|0)==0;K=(M?A:0)^(K&z|A);M=(M?1:L)+B|0;L=(K|0)<(A|0);if(L&(M|0)>1&(K|0)>0){do{M=M+ -1|0;K=K<<1;L=(K|0)<(A|0)}while(L&(M|0)>1&(K|0)>0)}g[G+(I+D<<2)>>2]=(c[k>>2]=((L?0:A)^K)<<C|J&-2147483648|(L?0:M<<23),+g[k>>2]);D=D+1|0}while((D|0)!=(s|0))}H=H+n|0}while(H>>>0<t>>>0)}j=j+n|0}while(j>>>0<u>>>0);i=o;return};case 6:{if(!(u>>>0>j>>>0)){i=o;return}x=t>>>0>l>>>0;e=e+32936|0;y=y&255;do{D=c[p+((j>>>m)+v<<2)>>2]|0;if(x){C=c[e>>2]|0;B=l;do{A=(c[q+(B>>>m<<2)>>2]|0)+D|0;if(!w){z=d[(B&15)+(8400+(j<<4))>>0]|0|B>>>4<<8;E=0;do{c[C+(A+E<<2)>>2]=(aa(c[(c[r+((E&15)<<2)>>2]|0)+(z<<2)>>2]|0,f)|0)>>h<<y;E=E+1|0}while((E|0)!=(s|0))}B=B+n|0}while(B>>>0<t>>>0)}j=j+n|0}while(j>>>0<u>>>0);i=o;return};case 5:{z=y&255;y=(-2147483648>>z<<h|0)/(f|0)|0;if(!(u>>>0>j>>>0)){i=o;return}x=t>>>0>l>>>0;e=e+32936|0;do{F=c[p+((j>>>m)+v<<2)>>2]|0;if(x){E=c[e>>2]|0;D=l;do{C=(c[q+(D>>>m<<2)>>2]|0)+F|0;if(!w){B=d[(D&15)+(8400+(j<<4))>>0]|0|D>>>4<<8;A=0;do{c[E+(C+A<<2)>>2]=(aa((c[(c[r+((A&15)<<2)>>2]|0)+(B<<2)>>2]|0)+y|0,f)|0)>>h<<z;A=A+1|0}while((A|0)!=(s|0))}D=D+n|0}while(D>>>0<t>>>0)}j=j+n|0}while(j>>>0<u>>>0);i=o;return};case 4:{if(!(u>>>0>j>>>0)){i=o;return}x=t>>>0>l>>>0;e=e+32936|0;do{D=c[p+((j>>>m)+v<<2)>>2]|0;if(x){z=c[e>>2]|0;y=l;do{C=(c[q+(y>>>m<<2)>>2]|0)+D|0;if(!w){B=d[(y&15)+(8400+(j<<4))>>0]|0|y>>>4<<8;A=0;do{L=(aa(c[(c[r+((A&15)<<2)>>2]|0)+(B<<2)>>2]|0,f)|0)>>h;M=L>>31;b[z+(C+A<<1)>>1]=(L&32767^M)-M;A=A+1|0}while((A|0)!=(s|0))}y=y+n|0}while(y>>>0<t>>>0)}j=j+n|0}while(j>>>0<u>>>0);i=o;return};default:{pa(2968,2928,1829,3264)}}}function tc(e,f,h,j){e=e|0;f=f|0;h=h|0;j=j|0;var l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0;l=i;if((c[e+34932>>2]|0)!=0){H=0;i=l;return H|0}s=c[e+34928>>2]|0;if((s|0)==0){H=0;i=l;return H|0}m=1<<f;t=c[e+34232>>2]|0;n=c[e+34324>>2]<<4;u=n+ -16|0;n=(c[t+36>>2]|0)+1+(16-n)|0;o=n>>>0<16?n:16;n=(c[t+28>>2]|0)+1|0;q=c[t+32>>2]|0;p=m+ -1|0;v=(p+(u>>>0>q>>>0?0:q&15)|0)>>>f<<f;p=(p+(c[t+24>>2]|0)|0)>>>f<<f;w=c[e+12>>2]|0;r=((w|0)==4?4:3)+(c[e+24>>2]|0)|0;q=c[s+34524>>2]|0;y=a[s+32908>>0]|0;A=a[s+32909>>0]|0;s=c[t+40>>2]|0;t=c[t+44>>2]|0;u=u>>>f;if(!((w|0)==4|(w|0)==7)){H=-1;i=l;return H|0}switch(c[e+16>>2]|0){case 1:{x=(128<<j|0)/(h|0)|0;if(!(v>>>0<o>>>0)){H=0;i=l;return H|0}w=p>>>0<n>>>0;y=e+32936|0;while(1){if(w){A=(c[t+((v>>>f)+u<<2)>>2]|0)+r|0;z=p;do{B=(aa((c[q+((d[(z&15)+(8400+(v<<4))>>0]|0|z>>>4<<8)<<2)>>2]|0)+x|0,h)|0)>>j;if((B|0)<0){B=0}else{B=(B|0)>255?-1:B&255}a[(c[y>>2]|0)+(A+(c[s+(z>>>f<<2)>>2]|0))>>0]=B;z=z+m|0}while(z>>>0<n>>>0)}v=v+m|0;if(!(v>>>0<o>>>0)){j=0;break}}i=l;return j|0};case 7:{if(!(v>>>0<o>>>0)){H=0;i=l;return H|0}w=p>>>0<n>>>0;y=y&255;x=1<<y;z=x+ -1|0;A=127-(A<<24>>24)|0;B=23-y|0;e=e+32936|0;while(1){if(w){C=(c[t+((v>>>f)+u<<2)>>2]|0)+r|0;E=p;do{F=(aa(c[q+((d[(E&15)+(8400+(v<<4))>>0]|0|E>>>4<<8)<<2)>>2]|0,h)|0)>>j;D=F>>31;F=(D^F)-D|0;G=F>>>y;H=(G|0)==0;F=(H?x:0)^(F&z|x);H=(H?1:G)+A|0;G=(F|0)<(x|0);if(G&(H|0)>1&(F|0)>0){do{H=H+ -1|0;F=F<<1;G=(F|0)<(x|0)}while(G&(H|0)>1&(F|0)>0)}g[(c[e>>2]|0)+(C+(c[s+(E>>>f<<2)>>2]|0)<<2)>>2]=(c[k>>2]=((G?0:x)^F)<<B|D&-2147483648|(G?0:H<<23),+g[k>>2]);E=E+m|0}while(E>>>0<n>>>0)}v=v+m|0;if(!(v>>>0<o>>>0)){j=0;break}}i=l;return j|0};case 4:{if(!(v>>>0<o>>>0)){H=0;i=l;return H|0}w=p>>>0<n>>>0;x=e+32936|0;while(1){if(w){A=(c[t+((v>>>f)+u<<2)>>2]|0)+r|0;y=c[x>>2]|0;z=p;do{G=(aa(c[q+((d[(z&15)+(8400+(v<<4))>>0]|0|z>>>4<<8)<<2)>>2]|0,h)|0)>>j;H=G>>31;b[y+(A+(c[s+(z>>>f<<2)>>2]|0)<<1)>>1]=(G&32767^H)-H;z=z+m|0}while(z>>>0<n>>>0)}v=v+m|0;if(!(v>>>0<o>>>0)){j=0;break}}i=l;return j|0};case 6:{if(!(v>>>0<o>>>0)){H=0;i=l;return H|0}w=p>>>0<n>>>0;x=y&255;y=e+32936|0;while(1){if(w){z=(c[t+((v>>>f)+u<<2)>>2]|0)+r|0;B=c[y>>2]|0;A=p;do{H=(aa(c[q+((d[(A&15)+(8400+(v<<4))>>0]|0|A>>>4<<8)<<2)>>2]|0,h)|0)>>j<<x;c[B+(z+(c[s+(A>>>f<<2)>>2]|0)<<2)>>2]=H;A=A+m|0}while(A>>>0<n>>>0)}v=v+m|0;if(!(v>>>0<o>>>0)){j=0;break}}i=l;return j|0};case 2:{w=(32768<<j|0)/(h|0)|0;if(!(v>>>0<o>>>0)){H=0;i=l;return H|0}x=p>>>0<n>>>0;y=y&255;z=e+32936|0;while(1){if(x){B=(c[t+((v>>>f)+u<<2)>>2]|0)+r|0;A=p;do{e=(aa((c[q+((d[(A&15)+(8400+(v<<4))>>0]|0|A>>>4<<8)<<2)>>2]|0)+w|0,h)|0)>>j<<y;if((e|0)<0){e=0}else{e=(e|0)>65535?-1:e&65535}b[(c[z>>2]|0)+(B+(c[s+(A>>>f<<2)>>2]|0)<<1)>>1]=e;A=A+m|0}while(A>>>0<n>>>0)}v=v+m|0;if(!(v>>>0<o>>>0)){j=0;break}}i=l;return j|0};case 3:{if(!(v>>>0<o>>>0)){H=0;i=l;return H|0}w=p>>>0<n>>>0;x=y&255;y=e+32936|0;while(1){if(w){z=(c[t+((v>>>f)+u<<2)>>2]|0)+r|0;A=p;do{B=(aa(c[q+((d[(A&15)+(8400+(v<<4))>>0]|0|A>>>4<<8)<<2)>>2]|0,h)|0)>>j<<x;if((B|0)<-32768){B=-32768}else{B=(B|0)>32767?32767:B&65535}b[(c[y>>2]|0)+(z+(c[s+(A>>>f<<2)>>2]|0)<<1)>>1]=B;A=A+m|0}while(A>>>0<n>>>0)}v=v+m|0;if(!(v>>>0<o>>>0)){j=0;break}}i=l;return j|0};default:{H=-1;i=l;return H|0}}return 0}function uc(e){e=e|0;var f=0,h=0,j=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0,M=0,N=0,O=0,P=0,Q=0,R=0,S=0,T=0,U=0,V=0,W=0,X=0,Y=0,Z=0,_=0,$=0,ba=0;f=i;L=e+34232|0;q=c[L>>2]|0;h=c[q+12>>2]|0;l=(c[q>>2]|0)==0;if(l){j=(c[q+36>>2]|0)+1|0}else{j=c[e+8>>2]|0}M=e+34324|0;m=c[M>>2]<<4;if((j+16-m|0)>>>0<16){if(l){j=(c[q+36>>2]|0)+1|0}else{j=c[e+8>>2]|0}j=j+16-m|0}else{j=16}if(l){l=(c[q+28>>2]|0)+1|0}else{l=c[e+4>>2]|0}r=m+ -16|0;m=c[q+32>>2]|0;u=h+ -1|0;t=u+(r>>>0>m>>>0?0:m&15)|0;t=t-((t>>>0)%(h>>>0)|0)|0;m=u+(c[q+24>>2]|0)|0;m=m-((m>>>0)%(h>>>0)|0)|0;I=e+34128|0;O=c[I>>2]|0;if((O|0)==0){H=0}else{H=c[e+12>>2]|0}v=e+16|0;J=c[v>>2]|0;x=c[e+100>>2]|0;n=(c[e+28>>2]|0)!=0?2:0;o=2-n|0;F=a[e+32908>>0]|0;G=a[e+32909>>0]|0;w=c[e+34524>>2]|0;B=c[e+34528>>2]|0;C=c[e+34532>>2]|0;p=c[q+40>>2]|0;q=c[q+44>>2]|0;r=(r>>>0)/(h>>>0)|0;s=h>>>0>15;do{if(s){if((x|0)==0){x=16;E=20}else{x=(x|0)==1?23:34;E=20}}else{if(h>>>0>3){x=(x|0)==0?64:93;E=20;break}else{x=258;z=0;y=(c[e+34140>>2]|0)!=0?11:8;break}}}while(0);if((E|0)==20){z=(c[e+34140>>2]|0)!=0;y=z?11:8;if(z&s){if((O|0)==1){z=2}else{z=(O|0)==2?2:1}}else{z=0}}s=0;while(1){K=1<<s;if(K>>>0<h>>>0){s=s+1|0}else{break}}A=y-z|0;if((h|0)!=(K|0)){pa(3288,2928,1941,3320)}K=e+64|0;z=j-t|0;if((ad(e,c[((c[K>>2]|0)>>>0<4?e+40|0:e+48|0)>>2]|0,(z>>>0)/(h>>>0)|0)|0)!=0){$=-1;i=f;return $|0}M=c[M>>2]<<4;if((((M+ -16|0)>>>0)%(h>>>0)|0|0)!=0){$=0;i=f;return $|0}L=c[L>>2]|0;if(!(M>>>0>(c[L+32>>2]|0)>>>0)){$=0;i=f;return $|0}if(M>>>0>((c[L+36>>2]|0)+16|0)>>>0){$=0;i=f;return $|0}Q=(O|0)==2;L=(H|0)==0;if(!((O+ -1|0)>>>0>1|L)){N=c[e+34780>>2]|0;M=c[e+34784>>2]|0;U=(l|0)==0;V=0;do{if(!U){P=V>>>1;T=0;do{R=T>>>4;if(Q){S=(d[(T>>>1&7)+(8400+(V<<4))>>0]|0)+(R<<7)|0}else{S=(d[(T>>>1&7)+(8656+(P<<3))>>0]|0)+(R<<6)|0}$=d[(T&15)+(8400+(V<<4))>>0]|R<<8;c[N+($<<2)>>2]=c[B+(S<<2)>>2];c[M+($<<2)>>2]=c[C+(S<<2)>>2];T=T+h|0}while(T>>>0<l>>>0)}V=V+h|0}while(V>>>0<16);if((h|0)==4){if((O|0)!=1|U){P=0}else{B=0;do{$=B>>>4<<8;Y=B&8;W=d[8400+Y>>0]|$;X=d[8464+Y>>0]|$;P=d[8528+Y>>0]|$;Z=N+(P<<2)|0;c[N+(X<<2)>>2]=(c[N+(W<<2)>>2]|0)+1+(c[Z>>2]|0)>>1;_=M+(P<<2)|0;c[M+(X<<2)>>2]=(c[M+(W<<2)>>2]|0)+1+(c[_>>2]|0)>>1;$=d[8592+Y>>0]|$;c[N+($<<2)>>2]=c[Z>>2];c[M+($<<2)>>2]=c[_>>2];B=B+8|0}while(B>>>0<l>>>0)}C=l+ -8|0;B=(C|0)==0;O=0;while(1){if(B){R=0;Q=4}else{T=a[8400+(O<<4)>>0]|0;R=0;Q=0;S=0;do{_=T&255|R;$=d[(Q|4)+(8400+(O<<4))>>0]|R;S=S+8|0;Q=S&8;T=a[8400+(O<<4)+Q>>0]|0;P=T&255|R;c[N+($<<2)>>2]=(c[N+(_<<2)>>2]|0)+1+(c[N+(P<<2)>>2]|0)>>1;c[M+($<<2)>>2]=(c[M+(_<<2)>>2]|0)+1+(c[M+(P<<2)>>2]|0)>>1;R=S>>>4<<8}while(S>>>0<C>>>0);Q=Q|4}$=d[8400+(O<<4)+Q>>0]|R;c[N+($<<2)>>2]=c[N+(P<<2)>>2];c[M+($<<2)>>2]=c[M+(P<<2)>>2];O=O+4|0;if(!(O>>>0<16)){B=N;C=M;break}}}else{B=N;C=M}}a:do{switch(J|0){case 2:{M=F&255;O=(32768>>>M<<y|0)/(x|0)|0;switch(H|0){case 6:case 3:case 0:{sc(e,x,y,t,m);break a};case 4:{P=c[e+34536>>2]|0;O=(32768<<y|0)/(x|0)|0;Q=((32768<<A|0)/(x|0)|0)>>1;if(t>>>0<j>>>0){T=m>>>0<l>>>0;R=0-x|0;S=e+32936|0;U=t;do{N=c[q+((U>>>s)+r<<2)>>2]|0;if(T){V=c[S>>2]|0;W=m;do{Y=d[(W&15)+(8400+(U<<4))>>0]|W>>>4<<8;_=(aa(O-(c[w+(Y<<2)>>2]|0)|0,x)|0)>>y;ba=(aa(c[B+(Y<<2)>>2]|0,x)|0)>>A;Z=(aa(c[C+(Y<<2)>>2]|0,R)|0)>>A;Y=((aa((c[P+(Y<<2)>>2]|0)+Q|0,x)|0)>>A)-(_+1>>1)|0;_=Y-(ba>>1)+_|0;ba=_-(Z+1>>1)+ba|0;X=(c[p+(W>>>s<<2)>>2]|0)+N|0;$=ba<<M;_=_<<M;Z=ba+Z<<M;Y=Y<<M;if(($|0)<0){$=0}else{$=($|0)>65535?-1:$&65535}b[V+(X<<1)>>1]=$;if((_|0)<0){_=0}else{_=(_|0)>65535?-1:_&65535}b[V+(X+1<<1)>>1]=_;if((Z|0)<0){Z=0}else{Z=(Z|0)>65535?-1:Z&65535}b[V+(X+2<<1)>>1]=Z;if((Y|0)<0){Y=0}else{Y=(Y|0)>65535?-1:Y&65535}b[V+(X+3<<1)>>1]=Y;W=W+h|0}while(W>>>0<l>>>0)}U=U+h|0}while(U>>>0<j>>>0)}break};case 7:{if(!(t>>>0<j>>>0)){break a}Q=m>>>0<l>>>0;N=0-x|0;U=e+32936|0;T=t;do{V=c[q+((T>>>s)+r<<2)>>2]|0;if(Q){S=c[U>>2]|0;R=m;do{W=d[(R&15)+(8400+(T<<4))>>0]|R>>>4<<8;X=(aa((c[w+(W<<2)>>2]|0)+O|0,x)|0)>>y;ba=(aa(c[B+(W<<2)>>2]|0,N)|0)>>A;W=(aa(c[C+(W<<2)>>2]|0,x)|0)>>A;X=X-(ba>>1)|0;ba=X-(W+1>>1)+ba|0;P=(c[p+(R>>>s<<2)>>2]|0)+V|0;Y=ba<<M;X=X<<M;W=ba+W<<M;if((Y|0)<0){Y=0}else{Y=(Y|0)>65535?-1:Y&65535}b[S+(P<<1)>>1]=Y;if((X|0)<0){X=0}else{X=(X|0)>65535?-1:X&65535}b[S+(P+1<<1)>>1]=X;if((W|0)<0){W=0}else{W=(W|0)>65535?-1:W&65535}b[S+(P+2<<1)>>1]=W;R=R+h|0}while(R>>>0<l>>>0)}T=T+h|0}while(T>>>0<j>>>0);break};default:{pa(2968,2928,2117,3320)}}switch(J|0){case 4:{E=141;break};case 7:{E=168;break};case 5:{E=150;break};case 6:{E=159;break};case 0:{E=183;break};case 8:{E=198;break};case 10:{E=210;break};case 3:{E=112;break};case 9:{E=222;break};default:{}}break};case 4:{E=141;break};case 7:{E=168;break};case 5:{E=150;break};case 6:{E=159;break};case 0:{E=183;break};case 8:{E=198;break};case 10:{E=210;break};case 3:{E=112;break};case 1:{F=(128<<y|0)/(x|0)|0;switch(H|0){case 6:case 3:case 0:{sc(e,x,y,t,m);break a};case 4:{H=c[e+34536>>2]|0;G=((128<<A|0)/(x|0)|0)>>1;if(!(t>>>0<j>>>0)){break a}L=m>>>0<l>>>0;J=0-x|0;K=e+32936|0;M=t;do{N=c[q+((M>>>s)+r<<2)>>2]|0;if(L){O=m;do{Q=d[(O&15)+(8400+(M<<4))>>0]|O>>>4<<8;S=(aa(F-(c[w+(Q<<2)>>2]|0)|0,x)|0)>>y;T=(aa(c[B+(Q<<2)>>2]|0,x)|0)>>A;R=(aa(c[C+(Q<<2)>>2]|0,J)|0)>>A;Q=((aa((c[H+(Q<<2)>>2]|0)+G|0,x)|0)>>A)-(S+1>>1)|0;S=Q-(T>>1)+S|0;T=S-(R+1>>1)+T|0;R=T+R|0;P=c[K>>2]|0;I=(c[p+(O>>>s<<2)>>2]|0)+N|0;if((T|0)<0){T=0}else{T=(T|0)>255?-1:T&255}a[P+I>>0]=T;if((S|0)<0){S=0}else{S=(S|0)>255?-1:S&255}a[P+(I+1)>>0]=S;if((R|0)<0){R=0}else{R=(R|0)>255?-1:R&255}a[P+(I+2)>>0]=R;if((Q|0)<0){Q=0}else{Q=(Q|0)>255?-1:Q&255}a[P+(I+3)>>0]=Q;O=O+h|0}while(O>>>0<l>>>0)}M=M+h|0}while(M>>>0<j>>>0);break};case 7:{if(!(t>>>0<j>>>0)){break a}L=m>>>0<l>>>0;N=0-x|0;M=e+32936|0;G=t;do{K=c[q+((G>>>s)+r<<2)>>2]|0;if(L){J=m;do{Q=d[(J&15)+(8400+(G<<4))>>0]|J>>>4<<8;P=(aa((c[w+(Q<<2)>>2]|0)+F|0,x)|0)>>y;O=(aa(c[B+(Q<<2)>>2]|0,N)|0)>>A;Q=(aa(c[C+(Q<<2)>>2]|0,x)|0)>>A;P=P-(O>>1)|0;O=P-(Q+1>>1)+O|0;Q=O+Q|0;I=c[M>>2]|0;H=(c[p+(J>>>s<<2)>>2]|0)+K|0;if((Q|0)<0){Q=0}else{Q=(Q|0)>255?-1:Q&255}a[I+(H+n)>>0]=Q;if((P|0)<0){P=0}else{P=(P|0)>255?-1:P&255}a[I+(H+1)>>0]=P;if((O|0)<0){O=0}else{O=(O|0)>255?-1:O&255}a[I+(H+o)>>0]=O;J=J+h|0}while(J>>>0<l>>>0)}G=G+h|0}while(G>>>0<j>>>0);break};case 8:{if(!(t>>>0<j>>>0)){break a}F=m>>>0<l>>>0;G=e+32936|0;I=t;do{J=c[q+((I>>>s)+r<<2)>>2]|0;if(F){H=m;do{_=d[(H&15)+(8400+(I<<4))>>0]|H>>>4<<8;Y=(aa(c[w+(_<<2)>>2]|0,x)|0)>>y;Z=(aa(c[B+(_<<2)>>2]|0,x)|0)>>A;_=(aa(c[C+(_<<2)>>2]|0,x)|0)>>A;Y=Y-(0-Z>>1)|0;Z=Y-(_+1>>1)-Z|0;$=c[G>>2]|0;ba=(c[p+(H>>>s<<2)>>2]|0)+J|0;rc(Z,Y,Z+_|0,$+ba|0,$+(ba+1)|0,$+(ba+2)|0,$+(ba+3)|0);H=H+h|0}while(H>>>0<l>>>0)}I=I+h|0}while(I>>>0<j>>>0);break};default:{pa(2968,2928,2061,3320)}}break};case 9:{E=222;break};default:{}}}while(0);b:do{if((E|0)==112){switch(H|0){case 6:case 3:case 0:{sc(e,x,y,t,m);break b};case 4:{D=c[e+34536>>2]|0;if(!(t>>>0<j>>>0)){break b}H=m>>>0<l>>>0;E=0-x|0;G=e+32936|0;M=F&255;I=t;do{J=c[q+((I>>>s)+r<<2)>>2]|0;if(H){F=c[G>>2]|0;K=m;do{N=d[(K&15)+(8400+(I<<4))>>0]|K>>>4<<8;P=(aa(c[w+(N<<2)>>2]|0,E)|0)>>y;ba=(aa(c[B+(N<<2)>>2]|0,x)|0)>>A;O=(aa(c[C+(N<<2)>>2]|0,E)|0)>>A;N=((aa(c[D+(N<<2)>>2]|0,x)|0)>>A)-(P+1>>1)|0;P=N-(ba>>1)+P|0;ba=P-(O+1>>1)+ba|0;L=(c[p+(K>>>s<<2)>>2]|0)+J|0;Q=ba<<M;P=P<<M;O=ba+O<<M;N=N<<M;if((Q|0)<-32768){Q=-32768}else{Q=(Q|0)>32767?32767:Q&65535}b[F+(L<<1)>>1]=Q;if((P|0)<-32768){P=-32768}else{P=(P|0)>32767?32767:P&65535}b[F+(L+1<<1)>>1]=P;if((O|0)<-32768){O=-32768}else{O=(O|0)>32767?32767:O&65535}b[F+(L+2<<1)>>1]=O;if((N|0)<-32768){N=-32768}else{N=(N|0)>32767?32767:N&65535}b[F+(L+3<<1)>>1]=N;K=K+h|0}while(K>>>0<l>>>0)}I=I+h|0}while(I>>>0<j>>>0);break};case 7:{if(!(t>>>0<j>>>0)){break b}G=m>>>0<l>>>0;D=0-x|0;E=e+32936|0;L=F&255;K=t;do{J=c[q+((K>>>s)+r<<2)>>2]|0;if(G){I=c[E>>2]|0;H=m;do{M=d[(H&15)+(8400+(K<<4))>>0]|H>>>4<<8;N=(aa(c[w+(M<<2)>>2]|0,x)|0)>>y;ba=(aa(c[B+(M<<2)>>2]|0,D)|0)>>A;M=(aa(c[C+(M<<2)>>2]|0,x)|0)>>A;N=N-(ba>>1)|0;ba=N-(M+1>>1)+ba|0;F=(c[p+(H>>>s<<2)>>2]|0)+J|0;O=ba<<L;N=N<<L;M=ba+M<<L;if((O|0)<-32768){O=-32768}else{O=(O|0)>32767?32767:O&65535}b[I+(F<<1)>>1]=O;if((N|0)<-32768){N=-32768}else{N=(N|0)>32767?32767:N&65535}b[I+(F+1<<1)>>1]=N;if((M|0)<-32768){M=-32768}else{M=(M|0)>32767?32767:M&65535}b[I+(F+2<<1)>>1]=M;H=H+h|0}while(H>>>0<l>>>0)}K=K+h|0}while(K>>>0<j>>>0);break};default:{pa(2968,2928,2171,3320)}}}else if((E|0)==141){if((H|0)==7){if(!(t>>>0<j>>>0)){break}H=m>>>0<l>>>0;J=0-x|0;I=e+32936|0;G=t;do{F=c[q+((G>>>s)+r<<2)>>2]|0;if(H){E=c[I>>2]|0;D=m;do{_=d[(D&15)+(8400+(G<<4))>>0]|D>>>4<<8;Z=(aa(c[w+(_<<2)>>2]|0,x)|0)>>y;Y=(aa(c[B+(_<<2)>>2]|0,J)|0)>>A;_=(aa(c[C+(_<<2)>>2]|0,x)|0)>>A;Z=Z-(Y>>1)|0;Y=Z-(_+1>>1)+Y|0;_=Y+_|0;ba=(c[p+(D>>>s<<2)>>2]|0)+F|0;$=Y>>31;b[E+(ba<<1)>>1]=(Y&32767^$)-$;$=Z>>31;b[E+(ba+1<<1)>>1]=(Z&32767^$)-$;$=_>>31;b[E+(ba+2<<1)>>1]=(_&32767^$)-$;D=D+h|0}while(D>>>0<l>>>0)}G=G+h|0}while(G>>>0<j>>>0)}else if((H|0)==6|(H|0)==3|(H|0)==0){sc(e,x,y,t,m);break}else{pa(2968,2928,2202,3320)}}else if((E|0)==150){E=F&255;D=(-2147483648>>E<<y|0)/(x|0)|0;if((H|0)==7){if(!(t>>>0<j>>>0)){break}F=m>>>0<l>>>0;H=0-x|0;G=e+32936|0;I=t;do{J=c[q+((I>>>s)+r<<2)>>2]|0;if(F){K=c[G>>2]|0;L=m;do{$=d[(L&15)+(8400+(I<<4))>>0]|L>>>4<<8;Z=(aa((c[w+($<<2)>>2]|0)+D|0,x)|0)>>y;_=(aa(c[B+($<<2)>>2]|0,H)|0)>>A;$=(aa(c[C+($<<2)>>2]|0,x)|0)>>A;Z=Z-(_>>1)|0;_=Z-($+1>>1)+_|0;ba=(c[p+(L>>>s<<2)>>2]|0)+J|0;c[K+(ba<<2)>>2]=_<<E;c[K+(ba+1<<2)>>2]=Z<<E;c[K+(ba+2<<2)>>2]=_+$<<E;L=L+h|0}while(L>>>0<l>>>0)}I=I+h|0}while(I>>>0<j>>>0)}else if((H|0)==6|(H|0)==3|(H|0)==0){sc(e,x,y,t,m);break}else{pa(2968,2928,2236,3320)}}else if((E|0)==159){if((H|0)==6|(H|0)==3|(H|0)==0){sc(e,x,y,t,m);break}else if((H|0)==7){if(!(t>>>0<j>>>0)){break}G=m>>>0<l>>>0;D=0-x|0;E=e+32936|0;F=F&255;H=t;do{I=c[q+((H>>>s)+r<<2)>>2]|0;if(G){J=c[E>>2]|0;K=m;do{$=d[(K&15)+(8400+(H<<4))>>0]|K>>>4<<8;Z=(aa(c[w+($<<2)>>2]|0,x)|0)>>y;_=(aa(c[B+($<<2)>>2]|0,D)|0)>>A;$=(aa(c[C+($<<2)>>2]|0,x)|0)>>A;Z=Z-(_>>1)|0;_=Z-($+1>>1)+_|0;ba=(c[p+(K>>>s<<2)>>2]|0)+I|0;c[J+(ba<<2)>>2]=_<<F;c[J+(ba+1<<2)>>2]=Z<<F;c[J+(ba+2<<2)>>2]=_+$<<F;K=K+h|0}while(K>>>0<l>>>0)}H=H+h|0}while(H>>>0<j>>>0)}else{pa(2968,2928,2267,3320)}}else if((E|0)==168){if((H|0)==7){if(!(t>>>0<j>>>0)){break}D=m>>>0<l>>>0;H=0-x|0;E=e+32936|0;I=F&255;F=1<<I;J=F+ -1|0;O=127-(G<<24>>24)|0;K=23-I|0;N=t;do{P=c[q+((N>>>s)+r<<2)>>2]|0;if(D){G=c[E>>2]|0;M=m;do{Q=d[(M&15)+(8400+(N<<4))>>0]|M>>>4<<8;R=(aa(c[w+(Q<<2)>>2]|0,x)|0)>>y;U=(aa(c[B+(Q<<2)>>2]|0,H)|0)>>A;Q=(aa(c[C+(Q<<2)>>2]|0,x)|0)>>A;R=R-(U>>1)|0;U=R-(Q+1>>1)+U|0;Q=U+Q|0;L=(c[p+(M>>>s<<2)>>2]|0)+P|0;S=G+(L<<2)|0;T=U>>31;U=(T^U)-T|0;V=U>>>I;W=(V|0)==0;U=(W?F:0)^(U&J|F);W=(W?1:V)+O|0;V=(U|0)<(F|0);if(V&(W|0)>1&(U|0)>0){do{W=W+ -1|0;U=U<<1;V=(U|0)<(F|0)}while(V&(W|0)>1&(U|0)>0)}g[S>>2]=(c[k>>2]=((V?0:F)^U)<<K|T&-2147483648|(V?0:W<<23),+g[k>>2]);S=R>>31;R=(S^R)-S|0;U=R>>>I;T=(U|0)==0;R=(T?F:0)^(R&J|F);U=(T?1:U)+O|0;T=(R|0)<(F|0);if(T&(U|0)>1&(R|0)>0){do{U=U+ -1|0;R=R<<1;T=(R|0)<(F|0)}while(T&(U|0)>1&(R|0)>0)}g[G+(L+1<<2)>>2]=(c[k>>2]=((T?0:F)^R)<<K|S&-2147483648|(T?0:U<<23),+g[k>>2]);R=Q>>31;Q=(R^Q)-R|0;S=Q>>>I;T=(S|0)==0;Q=(T?F:0)^(Q&J|F);T=(T?1:S)+O|0;S=(Q|0)<(F|0);if(S&(T|0)>1&(Q|0)>0){do{T=T+ -1|0;Q=Q<<1;S=(Q|0)<(F|0)}while(S&(T|0)>1&(Q|0)>0)}g[G+(L+2<<2)>>2]=(c[k>>2]=((S?0:F)^Q)<<K|R&-2147483648|(S?0:T<<23),+g[k>>2]);M=M+h|0}while(M>>>0<l>>>0)}N=N+h|0}while(N>>>0<j>>>0)}else if((H|0)==6|(H|0)==3|(H|0)==0){sc(e,x,y,t,m);break}else{pa(2968,2928,2299,3320)}}else if((E|0)==183){A=c[e+24>>2]|0;if(!L){pa(3208,2928,2307,3320)}if((c[I>>2]|0)!=0){pa(3208,2928,2307,3320)}B=t>>>0<j>>>0;if((c[K>>2]|0)>>>0<4){if(!B){break}F=m>>>0<l>>>0;G=e+32912|0;C=e+32936|0;E=t;while(1){D=(c[q+((E>>>s)+r<<2)>>2]|0)+A|0;if(F){B=m;do{_=c[p+(B>>>s<<2)>>2]|0;ba=(c[C>>2]|0)+(D+(_>>>3))|0;_=_&7^7;$=d[ba>>0]|0;a[ba>>0]=(($>>>_)+((c[w+((d[(B&15)+(8400+(E<<4))>>0]|B>>>4<<8)<<2)>>2]|0)>0^c[G>>2])&1)<<_^$;B=B+h|0}while(B>>>0<l>>>0)}E=E+h|0;if(!(E>>>0<j>>>0)){break b}}}if(B){E=m>>>0<l>>>0;F=e+32912|0;B=e+32936|0;D=t;do{G=(c[q+((D>>>s)+r<<2)>>2]|0)+A|0;if(E){C=G>>>3;G=G&7^7;H=m;do{ba=(c[B>>2]|0)+((c[p+(H>>>s<<2)>>2]|0)+C)|0;$=d[ba>>0]|0;a[ba>>0]=(($>>>G)+((c[w+((d[(H&15)+(8400+(D<<4))>>0]|H>>>4<<8)<<2)>>2]|0)>0^c[F>>2])&1)<<G^$;H=H+h|0}while(H>>>0<l>>>0)}D=D+h|0}while(D>>>0<j>>>0)}}else if((E|0)==198){E=(16<<y|0)/(x|0)|0;if(t>>>0<j>>>0){J=m>>>0<l>>>0;D=0-x|0;K=e+32936|0;I=t;do{F=c[q+((I>>>s)+r<<2)>>2]|0;if(J){G=c[K>>2]|0;H=m;do{L=d[(H&15)+(8400+(I<<4))>>0]|H>>>4<<8;N=(aa((c[w+(L<<2)>>2]|0)+E|0,x)|0)>>y;M=(aa(c[B+(L<<2)>>2]|0,D)|0)>>A;L=(aa(c[C+(L<<2)>>2]|0,x)|0)>>A;N=N-(M>>1)|0;M=N-(L+1>>1)+M|0;L=M+L|0;if((M|0)<0){M=0}else{M=(M|0)>31?31:M&65535}if((N|0)<0){N=0}else{N=(N|0)>31?992:N<<5&2097120}if((L|0)<0){L=0}else{L=(L|0)>31?31744:L<<10&67107840}b[G+((c[p+(H>>>s<<2)>>2]|0)+F<<1)>>1]=N+M+L;H=H+h|0}while(H>>>0<l>>>0)}I=I+h|0}while(I>>>0<j>>>0)}}else if((E|0)==210){H=(32<<y|0)/(x|0)|0;if(t>>>0<j>>>0){F=m>>>0<l>>>0;D=0-x|0;E=e+32936|0;G=t;do{K=c[q+((G>>>s)+r<<2)>>2]|0;if(F){J=c[E>>2]|0;I=m;do{M=d[(I&15)+(8400+(G<<4))>>0]|I>>>4<<8;N=(aa((c[w+(M<<2)>>2]|0)+H|0,x)|0)>>y;L=(aa(c[B+(M<<2)>>2]|0,D)|0)>>A;M=(aa(c[C+(M<<2)>>2]|0,x)|0)>>A;N=N-(L>>1)|0;L=N-(M+1>>1)+L|0;M=L+M|0;if((L|0)<-1){L=0}else{L=(L|0)>63?31:((L|0)/2|0)&65535}if((N|0)<0){N=0}else{N=(N|0)>63?2016:N<<5&2097120}if((M|0)<-1){M=0}else{M=(M|0)>63?63488:((M|0)/2|0)<<11&134215680}b[J+((c[p+(I>>>s<<2)>>2]|0)+K<<1)>>1]=N+L+M;I=I+h|0}while(I>>>0<l>>>0)}G=G+h|0}while(G>>>0<j>>>0)}}else if((E|0)==222?(D=(512<<y|0)/(x|0)|0,t>>>0<j>>>0):0){J=m>>>0<l>>>0;E=0-x|0;K=e+32936|0;I=t;do{H=c[q+((I>>>s)+r<<2)>>2]|0;if(J){G=c[K>>2]|0;F=m;do{M=d[(F&15)+(8400+(I<<4))>>0]|F>>>4<<8;N=(aa((c[w+(M<<2)>>2]|0)+D|0,x)|0)>>y;L=(aa(c[B+(M<<2)>>2]|0,E)|0)>>A;M=(aa(c[C+(M<<2)>>2]|0,x)|0)>>A;N=N-(L>>1)|0;L=N-(M+1>>1)+L|0;M=L+M|0;if((L|0)<0){L=0}else{L=(L|0)>1023?1023:L}if((N|0)<0){N=0}else{N=(N|0)>1023?1047552:N<<10}if((M|0)<0){M=0}else{M=(M|0)>1023?1072693248:M<<20}c[G+((c[p+(F>>>s<<2)>>2]|0)+H<<2)>>2]=N+L+M;F=F+h|0}while(F>>>0<l>>>0)}I=I+h|0}while(I>>>0<j>>>0)}}while(0);if((a[e+112>>0]|0)!=0?(tc(e,s,x,y)|0)!=0:0){ba=-1;i=f;return ba|0}w=e+32936|0;c[e+32956>>2]=((u+z|0)>>>0)/(h>>>0)|0;if((c[e+12>>2]|0)!=7){ba=0;i=f;return ba|0}if((c[e+92>>2]|0)!=0){ba=0;i=f;return ba|0}switch(c[v>>2]|0){case 7:case 6:case 5:{if(!(t>>>0<j>>>0)){ba=0;i=f;return ba|0}e=m>>>0<l>>>0;while(1){v=c[q+((t>>>s)+r<<2)>>2]|0;if(e){u=c[w>>2]|0;x=m;do{_=(c[p+(x>>>s<<2)>>2]|0)+v|0;ba=u+_|0;$=c[ba+(o<<2)>>2]|0;c[u+(_+4)>>2]=$;c[ba+(n<<2)>>2]=$;x=x+h|0}while(x>>>0<l>>>0)}t=t+h|0;if(!(t>>>0<j>>>0)){h=0;break}}i=f;return h|0};case 4:case 3:case 2:{if(!(t>>>0<j>>>0)){ba=0;i=f;return ba|0}e=m>>>0<l>>>0;while(1){u=c[q+((t>>>s)+r<<2)>>2]|0;if(e){v=c[w>>2]|0;x=m;do{_=(c[p+(x>>>s<<2)>>2]|0)+u|0;ba=v+_|0;$=b[ba+(o<<1)>>1]|0;b[v+(_+2)>>1]=$;b[ba+(n<<1)>>1]=$;x=x+h|0}while(x>>>0<l>>>0)}t=t+h|0;if(!(t>>>0<j>>>0)){h=0;break}}i=f;return h|0};case 1:{if(!(t>>>0<j>>>0)){ba=0;i=f;return ba|0}e=m>>>0<l>>>0;while(1){v=c[q+((t>>>s)+r<<2)>>2]|0;if(e){u=m;do{$=c[w>>2]|0;ba=(c[p+(u>>>s<<2)>>2]|0)+v|0;_=a[$+(ba+o)>>0]|0;a[$+(ba+1)>>0]=_;a[$+(ba+n)>>0]=_;u=u+h|0}while(u>>>0<l>>>0)}t=t+h|0;if(!(t>>>0<j>>>0)){h=0;break}}i=f;return h|0};default:{ba=0;i=f;return ba|0}}return 0}function vc(a){a=a|0;var b=0,d=0,e=0,f=0,g=0,h=0;b=i;e=a+34248|0;d=c[e>>2]|0;Sd(a,d)|0;g=c[a+34300>>2]|0;if((g|0)!=0){f=c[a+34264>>2]|0;g=aa((c[a+16520>>2]|0)+1|0,g)|0;if((Nd(d,16)|0)!=1){h=-1;i=b;return h|0}if((g|0)!=0){h=0;do{Sd(a,d)|0;c[f+(h<<2)>>2]=wc(d)|0;h=h+1|0}while((h|0)!=(g|0))}}h=a+34304|0;c[h>>2]=wc(d)|0;Od(d)|0;g=Vd(c[e>>2]|0)|0;c[h>>2]=(c[h>>2]|0)+g;h=0;i=b;return h|0}function wc(a){a=a|0;var b=0,c=0;b=i;c=Nd(a,8)|0;if((c+ -253|0)>>>0<3){c=0;i=b;return c|0}if(c>>>0<251){c=Nd(a,8)|0|c<<8;i=b;return c|0}if((c|0)!=251){Nd(a,16)|0;Nd(a,16)|0}c=(Nd(a,16)|0)<<16;c=c|(Nd(a,16)|0);i=b;return c|0}function xc(a){a=a|0;var b=0,d=0,e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0;b=i;i=i+32|0;e=b;if((Ad(a)|0)!=0){p=-1;i=b;return p|0}Wd(c[a+34248>>2]|0,c[a+124>>2]|0,a)|0;if((vc(a)|0)!=0){p=-1;i=b;return p|0}if((c[a+76>>2]|0)==0){p=0;i=b;return p|0}d=a+16520|0;c[e>>2]=(c[d>>2]|0)+1;Da(3336,e|0)|0;f=0;do{p=c[a+(f<<2)+16524>>2]|0;c[e>>2]=f;c[e+4>>2]=p;Da(3360,e|0)|0;f=f+1|0}while(!(f>>>0>(c[d>>2]|0)>>>0));f=a+132|0;c[e>>2]=(c[f>>2]|0)+1;Da(3400,e|0)|0;g=0;do{p=c[a+(g<<2)+136>>2]|0;c[e>>2]=g;c[e+4>>2]=p;Da(3360,e|0)|0;g=g+1|0}while(!(g>>>0>(c[f>>2]|0)>>>0));g=a+104|0;if((c[g>>2]|0)==0){Ca(3888)|0}else{Ca(3816)|0}if((c[a+34144>>2]|0)==0){Ca(3848)|0;p=0;i=b;return p|0}a=a+34264|0;h=c[f>>2]|0;if((c[g>>2]|0)==0){g=0;while(1){l=0;do{k=(aa(h+1|0,g)|0)+l|0;j=c[a>>2]|0;if((l+g|0)==((c[d>>2]|0)+h|0)){c[e>>2]=g;c[e+4>>2]=l;Da(3464,e|0)|0}else{p=(c[j+(k+1<<2)>>2]|0)-(c[j+(k<<2)>>2]|0)|0;c[e>>2]=g;c[e+4>>2]=l;c[e+8>>2]=p;Da(3424,e|0)|0}l=l+1|0;h=c[f>>2]|0}while(!(l>>>0>h>>>0));g=g+1|0;if(g>>>0>(c[d>>2]|0)>>>0){d=0;break}}i=b;return d|0}else{g=0;while(1){p=0;do{m=(aa(h+1|0,g)|0)+p<<2;n=c[a>>2]|0;l=c[n+((m|1)<<2)>>2]|0;o=l-(c[n+(m<<2)>>2]|0)|0;k=c[n+((m|2)<<2)>>2]|0;l=k-l|0;j=c[n+((m|3)<<2)>>2]|0;k=j-k|0;if((p+g|0)==((c[d>>2]|0)+h|0)){c[e>>2]=g;c[e+4>>2]=p;c[e+8>>2]=o;c[e+12>>2]=l;c[e+16>>2]=k;Da(3584,e|0)|0}else{n=(c[n+(m+4<<2)>>2]|0)-j|0;c[e>>2]=g;c[e+4>>2]=p;c[e+8>>2]=o;c[e+12>>2]=l;c[e+16>>2]=k;c[e+20>>2]=n;Da(3512,e|0)|0}p=p+1|0;h=c[f>>2]|0}while(!(p>>>0>h>>>0));g=g+1|0;if(g>>>0>(c[d>>2]|0)>>>0){d=0;break}}i=b;return d|0}return 0}function yc(a){a=a|0;var b=0,e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0;b=i;g=a+34232|0;m=c[g>>2]|0;f=c[m+12>>2]|0;if(f>>>0>1){e=a+60|0;h=a+56|0}else{e=a+8|0;h=a+4|0}j=((((c[m+24>>2]|0)+ -1+f|0)>>>0)/(f>>>0)|0)+(c[h>>2]|0)|0;f=(((f+ -1+(c[m+32>>2]|0)|0)>>>0)/(f>>>0)|0)+(c[e>>2]|0)|0;l=c[a+16>>2]|0;switch(l|0){case 4:case 10:case 8:case 3:case 2:{h=(c[a+32944>>2]|0)>>>1;break};case 9:case 7:case 6:case 5:{h=(c[a+32944>>2]|0)>>>2;break};default:{h=c[a+32944>>2]|0}}e=c[a+12>>2]|0;if((e|0)==1){n=6;f=f>>>1;k=11}else if((e|0)==2){n=4;k=11}else{n=((c[a+20>>2]|0)>>>3>>>0)/((d[3664+l>>0]|0)>>>0)|0}if((k|0)==11){j=j>>>1}if((l|0)==10|(l|0)==9|(l|0)==8|(l|0)==0){n=1}k=c[a+64>>2]|0;o=k>>>0>3;l=o?h:n;h=o?n:h;v=j<<2;c[m+40>>2]=ke(v)|0;o=c[g>>2]|0;n=c[o+40>>2]|0;if((n|0)==0|v>>>0<j>>>0){v=-1;i=b;return v|0}m=k+ -2|0;if(m>>>0<6){p=(43>>>(m&63)&1)!=0}else{p=0}if((c[o>>2]|0)==0){m=c[o+12>>2]|0;m=(((c[o+24>>2]|0)+ -1+m|0)>>>0)/(m>>>0)|0}else{m=0}if(m>>>0<j>>>0){q=a+24|0;a=j-m|0;r=m;s=0;while(1){t=c[q>>2]|0;if(p){if((c[o>>2]|0)==0){u=c[o+12>>2]|0;if((e|0)==1){v=2}else{v=(e|0)==2?2:1}u=(((((c[o+28>>2]|0)-(c[o+24>>2]|0)+u|0)>>>0)/(u>>>0)|0)>>>0)/(v>>>0)|0}else{u=j}u=u+~s|0}else{u=s}c[n+(r<<2)>>2]=(aa(u,l)|0)+t;s=s+1|0;if((s|0)==(a|0)){break}else{r=s+m|0}}}v=f<<2;c[o+44>>2]=ke(v)|0;j=c[g>>2]|0;g=c[j+44>>2]|0;if((g|0)==0|v>>>0<f>>>0){v=-1;i=b;return v|0}k=k+ -1|0;if(k>>>0<5){k=(29>>>(k&31)&1)!=0}else{k=0}if((c[j>>2]|0)==0){m=c[j+12>>2]|0;m=(((c[j+32>>2]|0)+ -1+m|0)>>>0)/(m>>>0)|0}if(!(m>>>0<f>>>0)){v=0;i=b;return v|0}l=f-m|0;a=m;n=0;while(1){if(k){if((c[j>>2]|0)==0){o=c[j+12>>2]|0;o=((((c[j+36>>2]|0)-(c[j+32>>2]|0)+o|0)>>>0)/(o>>>0)|0)>>>((e|0)==1|0)}else{o=f}o=o+~n|0}else{o=n}c[g+(a<<2)>>2]=aa(o,h)|0;n=n+1|0;if((n|0)==(l|0)){e=0;break}else{a=n+m|0}}i=b;return e|0}function zc(a){a=a|0;var b=0,d=0,e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0;l=i;e=a+34164|0;f=a+34172|0;d=a+4|0;c[d>>2]=(c[d>>2]|0)-((c[f>>2]|0)+(c[e>>2]|0));h=a+34160|0;g=a+34168|0;j=a+8|0;c[j>>2]=(c[j>>2]|0)-((c[g>>2]|0)+(c[h>>2]|0));p=c[a+116>>2]|0;k=c[a+34232>>2]|0;b=k+8|0;c[b>>2]=(p|0)==1;o=k+16|0;c[o>>2]=p>>>0<2;n=k+20|0;c[n>>2]=(p|0)!=3;p=k+12|0;c[p>>2]=1;m=a+56|0;if((c[m>>2]|0)>>>0<(c[d>>2]|0)>>>0){q=1;do{q=q<<1;c[p>>2]=q;t=aa(c[m>>2]|0,q)|0}while(t>>>0<(c[d>>2]|0)>>>0)}else{q=1}m=(c[a+104>>2]|0)==1;if(m){if(q>>>0>3){c[o>>2]=0}if(q>>>0>15){c[n>>2]=0}}n=c[e>>2]|0;c[d>>2]=(c[f>>2]|0)+n+(c[d>>2]|0);c[j>>2]=(c[g>>2]|0)+(c[h>>2]|0)+(c[j>>2]|0);p=c[p>>2]|0;n=(aa(p,c[a+36>>2]|0)|0)+n|0;c[k+24>>2]=n;r=n+ -1+(aa(p,c[a+40>>2]|0)|0)|0;s=k+28|0;c[s>>2]=r;o=aa(p,c[a+44>>2]|0)|0;o=o+(c[h>>2]|0)|0;c[k+32>>2]=o;p=o+ -1+(aa(p,c[a+48>>2]|0)|0)|0;q=k+36|0;c[q>>2]=p;t=c[d>>2]|0;if(!(r>>>0<t>>>0)){r=t+ -1|0;c[s>>2]=r}s=c[j>>2]|0;if(!(p>>>0<s>>>0)){p=s+ -1|0;c[q>>2]=p}if((n|0)==(0-o|0)?!((r+15|0)>>>4>>>0<((c[d>>2]|0)+14|0)>>>4>>>0):0){o=(p+15|0)>>>4>>>0>=((c[j>>2]|0)+14|0)>>>4>>>0}else{o=0}c[k>>2]=o&1;if((n|0)==0){n=(r+15|0)>>>4>>>0>=((c[d>>2]|0)+14|0)>>>4>>>0}else{n=0}c[k+4>>2]=n&1;c[d>>2]=(c[d>>2]|0)-((c[f>>2]|0)+(c[e>>2]|0));c[j>>2]=(c[j>>2]|0)-((c[g>>2]|0)+(c[h>>2]|0));if(!m){t=a+34268|0;c[t>>2]=0;t=a+34272|0;c[t>>2]=0;i=l;return}if((c[a+52>>2]|0)!=1){t=a+34268|0;c[t>>2]=0;t=a+34272|0;c[t>>2]=0;i=l;return}c[b>>2]=1;t=a+34268|0;c[t>>2]=0;t=a+34272|0;c[t>>2]=0;i=l;return}function Ac(b){b=b|0;var d=0,e=0,f=0,g=0,h=0,j=0,k=0;d=i;g=c[b+34128>>2]|0;f=c[b+12>>2]|0;do{if((f|0)!=0){if((g|0)!=1|(f|0)==1?(g|0)!=2|(f|0)==2:0){e=4;break}k=(c[b+32924>>2]|0)==0;c[b+34240>>2]=k&1;if(k?(g=c[b+34332>>2]|0,k=aa((f|0)==2?512:1024,g)|0,h=ke(k)|0,c[b+34780>>2]=h,j=ke(k)|0,c[b+34784>>2]=j,(h|0)==0|(j|0)==0|k>>>0<g>>>0):0){k=-1;i=d;return k|0}}else{e=4}}while(0);if((e|0)==4){c[b+34240>>2]=0}if((_d(b)|0)!=0){k=-1;i=d;return k|0}if((Cd(b)|0)!=0){k=-1;i=d;return k|0}e=b+34180|0;if((c[e>>2]&1|0)==0){f=b+34292|0;g=b+34156|0;if((Fd(c[f>>2]|0,c[g>>2]|0,1)|0)!=0){k=-1;i=d;return k|0}Hd(b,0);if((c[g>>2]|0)==0){j=0}else{h=0;do{a[c[(c[f>>2]|0)+(h<<2)>>2]>>0]=a[b+h+34184>>0]|0;h=h+1|0;j=c[g>>2]|0}while(h>>>0<j>>>0)}Gd(c[f>>2]|0,(c[e>>2]|0)>>>3&3,j,0,1,c[b+34140>>2]|0)}f=b+116|0;do{if((c[f>>2]|0)!=3){do{if((c[e>>2]&2|0)==0){g=b+34292|0;h=b+34156|0;if((Fd((c[g>>2]|0)+64|0,c[h>>2]|0,1)|0)!=0){k=-1;i=d;return k|0}Hd(b,1);j=c[e>>2]|0;if((j&512|0)==0){Id(b,0);break}if((c[h>>2]|0)==0){h=0}else{j=0;do{a[c[(c[g>>2]|0)+(j<<2)+64>>2]>>0]=a[b+j+34200>>0]|0;j=j+1|0;k=c[h>>2]|0}while(j>>>0<k>>>0);h=k;j=c[e>>2]|0}Gd((c[g>>2]|0)+64|0,j>>>5&3,h,0,1,c[b+34140>>2]|0)}}while(0);if((c[f>>2]|0)!=2?(c[e>>2]&4|0)==0:0){f=b+34292|0;g=b+34156|0;if((Fd((c[f>>2]|0)+128|0,c[g>>2]|0,1)|0)!=0){k=-1;i=d;return k|0}Hd(b,2);h=c[e>>2]|0;if((h&1024|0)==0){Jd(b,1,0);break}if((c[g>>2]|0)==0){g=0}else{j=0;do{a[c[(c[f>>2]|0)+(j<<2)+128>>2]>>0]=a[b+j+34216>>0]|0;j=j+1|0;h=c[g>>2]|0}while(j>>>0<h>>>0);g=h;h=c[e>>2]|0}Gd((c[f>>2]|0)+128|0,h>>>7&3,g,0,0,c[b+34140>>2]|0)}}}while(0);e=c[b+132>>2]|0;if(e>>>0>4095){k=-1;i=d;return k|0}if((Rb(b,e+1|0)|0)!=0){k=-1;i=d;return k|0}if((c[b+34932>>2]|0)!=0){k=c[b+34928>>2]|0;c[b+34248>>2]=c[k+34248>>2];c[b+34296>>2]=c[k+34296>>2];c[b+34300>>2]=c[k+34300>>2];a[b+34236>>0]=a[k+34236>>0]|0}Bd(b)|0;k=0;i=d;return k|0}function Bc(a){a=a|0;var b=0,d=0,e=0,f=0,g=0,h=0;d=i;b=(c[a+34928>>2]|0)!=0?2:1;e=0;while(1){if((c[a+34240>>2]|0)!=0){f=c[a+34780>>2]|0;if((f|0)!=0){le(f)}f=c[a+34784>>2]|0;if((f|0)!=0){le(f)}}$d(a);Dd(a);Tb(a);if((e|0)==0){Xd(a,c[a+34248>>2]|0)|0;le(c[a+34296>>2]|0);le(c[a+34264>>2]|0);g=a+34232|0;f=c[g>>2]|0;h=c[f+40>>2]|0;if((h|0)!=0){le(h);f=c[g>>2]|0}f=c[f+44>>2]|0;if((f|0)!=0){le(f)}}e=e+1|0;if((e|0)==(b|0)){break}else{a=c[a+34928>>2]|0}}i=d;return 0}function Cc(b,d,e,f){b=b|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,j=0,k=0,l=0;g=i;j=wd(f,3)|0;k=e+8|0;c[k>>2]=j;l=j>>>0>6;if(l){l=l?-106:0;l=(l|0)!=0;l=l<<31>>31;i=g;return l|0}c[d+16>>2]=j;c[e+20>>2]=wd(f,1)|0;j=d+40|0;c[j>>2]=wd(f,4)|0;switch(c[k>>2]|0){case 1:{c[e+36>>2]=3;wd(f,1)|0;a[b+28>>0]=wd(f,3)|0;wd(f,1)|0;a[b+29>>0]=wd(f,3)|0;break};case 4:{c[e+36>>2]=4;break};case 3:{c[e+36>>2]=3;wd(f,4)|0;wd(f,4)|0;break};case 6:{c[e+36>>2]=(wd(f,4)|0)+1;wd(f,4)|0;break};case 2:{c[e+36>>2]=3;wd(f,1)|0;a[b+28>>0]=wd(f,3)|0;wd(f,4)|0;break};case 0:{c[e+36>>2]=1;break};default:{}}switch(c[b+12>>2]|0){case 7:{a[d+32832>>0]=wd(f,8)|0;a[d+32833>>0]=wd(f,8)|0;break};case 6:case 5:case 3:case 2:{a[d+32832>>0]=wd(f,8)|0;break};default:{}}k=e+60|0;c[k>>2]=0;if((wd(f,1)|0)==1){d=((hc(e+64|0,f,c[e+36>>2]|0)|0)&255)<<3;d=d+(c[k>>2]|0)|0}else{d=(c[k>>2]|0)+1|0}c[k>>2]=d;do{if((c[j>>2]|0)==3){h=28}else{l=(wd(f,1)|0)==0;d=c[k>>2]|0;do{if(l){c[k>>2]=d+512;if((wd(f,1)|0)==1){d=((hc(e+80|0,f,c[e+36>>2]|0)|0)&255)<<5;d=d+(c[k>>2]|0)|0;c[k>>2]=d;break}else{d=(c[k>>2]|0)+2|0;c[k>>2]=d;break}}else{d=(d<<1&2|d<<2&96)+d|0;c[k>>2]=d}}while(0);if((c[j>>2]|0)!=2){l=(wd(f,1)|0)==0;d=c[k>>2]|0;do{if(l){c[k>>2]=d+1024;if((wd(f,1)|0)==1){d=((hc(e+96|0,f,c[e+36>>2]|0)|0)&255)<<7;d=d+(c[k>>2]|0)|0;c[k>>2]=d;break}else{d=(c[k>>2]|0)+4|0;c[k>>2]=d;break}}else{d=(d<<1&4|d<<2&384)+d|0;c[k>>2]=d}}while(0);e=c[j>>2]|0;if((e|0)==3){h=28;break}else if((e|0)!=2){break}}d=d|1024;c[k>>2]=d}}while(0);if((h|0)==28){d=d|512;c[k>>2]=d}h=(d&1536|0)==0;e=h?-104:0;if(h){l=e;l=(l|0)!=0;l=l<<31>>31;i=g;return l|0}xd(f);l=e;l=(l|0)!=0;l=l<<31>>31;i=g;return l|0}function Dc(b,d,e){b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0;f=i;i=i+32|0;g=f+8|0;j=f;h=c[d+48>>2]|0;c[g+0>>2]=0;c[g+4>>2]=0;c[g+8>>2]=0;c[g+12>>2]=0;w=j;c[w>>2]=0;c[w+4>>2]=0;if((e|0)==0){pa(3680,2928,2960,3696)}k=Xa[c[h+28>>2]&31](h,j,8)|0;do{if((k|0)>=0){j=(j|0)!=(je(j,3712)|0);if(!j){h=vd(g,h)|0;if((h|0)>=0){h=(wd(g,4)|0)!=1;if(!h){c[e>>2]=1;h=wd(g,4)|0;j=h>>>0>1&(h|0)!=9?-114:0;if((h|0)==0|(h|0)==1|(h|0)==9){c[e+4>>2]=h;w=(h|0)==9|0;c[e+32>>2]=w;c[d+32840>>2]=w;w=wd(g,1)|0;v=d+28|0;c[v>>2]=wd(g,1)|0;c[b+60>>2]=wd(g,3)|0;u=e+24|0;c[u>>2]=wd(g,1)|0;j=wd(g,2)|0;h=(j|0)==3;if(!h){c[d+24>>2]=j;k=wd(g,1)|0;wd(g,1)|0;c[d+20>>2]=1;q=wd(g,1)|0;c[e+28>>2]=wd(g,1)|0;p=wd(g,1)|0;c[e+12>>2]=wd(g,1)|0;wd(g,1)|0;j=e+16|0;c[j>>2]=wd(g,1)|0;c[b+8>>2]=wd(g,4)|0;t=wd(g,4)|0;h=b+12|0;c[h>>2]=t;if((t|0)==15){c[h>>2]=0;c[d+32836>>2]=1}t=(k|0)!=0;o=t?16:32;c[b>>2]=(wd(g,o)|0)+1;l=b+4|0;c[l>>2]=(wd(g,o)|0)+1;o=e+52|0;k=e+48|0;n=e+44|0;m=e+40|0;q=(q|0)==0;c[m+0>>2]=0;c[m+4>>2]=0;c[m+8>>2]=0;c[m+12>>2]=0;if(q){r=c[b>>2]&15;if((r|0)!=0){c[o>>2]=16-r}r=c[l>>2]&15;if((r|0)!=0){c[k>>2]=16-r}}r=d+16444|0;c[r>>2]=0;s=d+56|0;c[s>>2]=0;if((w|0)==0){w=0}else{c[s>>2]=wd(g,12)|0;w=wd(g,12)|0;c[r>>2]=w}if((c[u>>2]|0)==0){if((c[v>>2]|0)==1){b=-106;break}u=c[s>>2]|0;v=(u|0)!=(0-w|0);if(v){b=v?-106:0;break}}else{u=c[s>>2]|0}c[d+16448>>2]=0;c[d+60>>2]=0;if((u|0)!=0){v=t?8:16;u=0;do{w=wd(g,v)|0;x=u;u=u+1|0;c[d+(u<<2)+60>>2]=(c[d+(x<<2)+60>>2]|0)+w}while(u>>>0<(c[s>>2]|0)>>>0);w=c[r>>2]|0}if((w|0)==0){t=1}else{v=t?8:16;u=0;do{t=wd(g,v)|0;x=u;u=u+1|0;c[d+(u<<2)+16448>>2]=(c[d+(x<<2)+16448>>2]|0)+t;t=c[r>>2]|0}while(u>>>0<t>>>0);t=t+1|0}if((p|0)!=0?(aa(t,(c[s>>2]|0)+1|0)|0)!=0:0){p=0;do{wd(g,8)|0;p=p+1|0}while(p>>>0<(aa((c[r>>2]|0)+1|0,(c[s>>2]|0)+1|0)|0)>>>0)}if(q){p=c[o>>2]|0}else{c[m>>2]=(wd(g,6)|0)&255;c[n>>2]=(wd(g,6)|0)&255;c[k>>2]=(wd(g,6)|0)&255;p=(wd(g,6)|0)&255;c[o>>2]=p}o=c[b>>2]|0;q=c[n>>2]|0;n=c[l>>2]|0;r=c[m>>2]|0;m=c[k>>2]|0;if((q+o+p&15|0)!=(0-(r+n+m&15)|0)){q=(q+(o&15)+(n&15)|0)!=(0-r|0);if(q){b=q?-104:0;break}m=n>>>0<=m>>>0|o>>>0<=p>>>0;if(m){b=m?-104:0;break}c[b>>2]=o-p;c[l>>2]=n-(c[k>>2]|0)}xd(g);k=(Cc(b,d,e,g)|0)!=0;b=k?-106:0;if(!k){zd(g)|0;c[d+52>>2]=0-(yd(g)|0);g=d+36|0;if((c[j>>2]|0)==0){j=0}else{j=a[g>>0]|0}a[g>>0]=j;c[d+32>>2]=c[e+36>>2];if(((c[h>>2]|0)+ -8|0)>>>0<3?!((c[d+16>>2]|0)>>>0<4):0){x=-1;i=f;return x|0}}}else{b=h?-104:0}}else{b=j}}else{b=h?-107:0}}else{b=h}}else{b=j?-106:0}}else{b=k}}while(0);x=((b|0)!=0)<<31>>31;i=f;return x|0}function Ec(a,b){a=a|0;b=b|0;var d=0,e=0,f=0,g=0;d=i;i=i+128|0;f=d+112|0;e=b+48|0;g=c[e>>2]|0;if(((Ya[c[g+40>>2]&31](g,f)|0)>=0?(Dc(a,b,d)|0)>=0:0)?(g=c[e>>2]|0,(Ya[c[g+36>>2]&31](g,c[f>>2]|0)|0)>=0):0){g=0;i=d;return g|0}g=-1;i=d;return g|0}function Fc(b,e){b=b|0;e=e|0;var f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0;f=i;i=i+192|0;h=f+184|0;k=f;p=c[e>>2]|0;o=e+36|0;n=a[o>>0]|0;t=e+48|0;s=c[t>>2]|0;r=e+52|0;q=c[r>>2]|0;if((Ya[c[s+40>>2]&31](s,h)|0)<0){t=-1;i=f;return t|0}if((Dc(k,e,f+72|0)|0)<0){t=-1;i=f;return t|0}u=c[t>>2]|0;if((Ya[c[u+36>>2]&31](u,c[h>>2]|0)|0)<0){u=-1;i=f;return u|0}c[b+12>>2]=c[k+12>>2];v=c[k>>2]|0;c[b>>2]=v;u=c[k+4>>2]|0;h=b+4|0;c[h>>2]=u;if((v|0)==0|(u|0)==0){v=-1;i=f;return v|0}c[e>>2]=p;c[r>>2]=q;c[t>>2]=s;if((d[o>>0]|0)>1){a[o>>0]=n}n=e+16|0;o=c[n>>2]|0;if((o|0)==6){c[b+8>>2]=6;o=c[n>>2]|0}if((o|0)==4){o=b+8|0;v=c[o>>2]|0;if(!((v|0)==7|(v|0)==0)){c[o>>2]=4;o=c[n>>2]|0;m=12}}else{m=12}do{if((m|0)==12){if((o|0)==2){m=b+8|0;if((c[m>>2]|0)!=1){break}c[m>>2]=2;o=c[n>>2]|0}if((o|0)==3?(l=b+8|0,((c[l>>2]|0)+ -1|0)>>>0<2):0){c[l>>2]=3}}}while(0);k=c[k+8>>2]|0;if((k|0)==8){c[b+8>>2]=8}else if((k|0)==7?(j=b+8|0,v=c[j>>2]|0,!((v|0)==6|(v|0)==0)):0){c[j>>2]=7}k=b+52|0;n=c[k>>2]|0;l=c[b>>2]|0;if((n|0)==0|n>>>0>l>>>0){c[k>>2]=l;n=l}j=b+56|0;o=c[j>>2]|0;m=c[h>>2]|0;if((o|0)==0|o>>>0>m>>>0){c[j>>2]=m;h=m}else{h=o}o=((l+ -1+n|0)>>>0)/(n>>>0)|0;a:do{if((o|0)==(((m+ -1+h|0)>>>0)/(h>>>0)|0|0)){o=(o|0)==0?1:o}else{o=1;while(1){p=o+ -1|0;if(!((((p+l|0)>>>0)/(o>>>0)|0)>>>0>n>>>0)){break a}if(!((((p+m|0)>>>0)/(o>>>0)|0)>>>0>h>>>0)){break a}p=o<<1;if((p|0)==0){break}else{o=p}}}}while(0);v=o+ -1|0;h=((v+l|0)>>>0)/(o>>>0)|0;c[k>>2]=h;k=((v+m|0)>>>0)/(o>>>0)|0;c[j>>2]=k;j=b+44|0;l=c[j>>2]|0;m=b+36|0;if((l|0)!=0?(g=c[m>>2]|0,(g|0)!=0):0){m=c[b+32>>2]|0}else{c[b+40>>2]=0;c[b+32>>2]=0;c[m>>2]=h;c[j>>2]=k;l=k;m=0;g=h}if(!(m>>>0<h>>>0)){c[b+32>>2]=0;m=0}n=b+40|0;o=c[n>>2]|0;if(!(o>>>0<k>>>0)){c[n>>2]=0;o=0}if((g+m|0)>>>0>h>>>0){c[b+36>>2]=h-m}if(!((l+o|0)>>>0>k>>>0)){v=0;i=f;return v|0}c[j>>2]=k-o;v=0;i=f;return v|0}function Gc(b,d,e){b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0;f=i;i=i+35088|0;p=f+16|0;r=f;re(p|0,0,35064)|0;c[e>>2]=0;if((Fc(b,d)|0)!=0){H=-1;i=f;return H|0}if((c[d+40>>2]|0)==4){H=-1;i=f;return H|0}A=d+48|0;u=p+76|0;c[p+124>>2]=c[A>>2];v=p+4|0;x=p+34120|0;if((Dc(v,u,x)|0)!=0){H=-1;i=f;return H|0}m=c[p+32916>>2]|0;if((c[d+16>>2]|0)!=4?(c[b+8>>2]|0)==4:0){H=-1;i=f;return H|0}te(u|0,d|0,32860)|0;E=v+0|0;F=b+0|0;D=E+72|0;do{c[E>>2]=c[F>>2];E=E+4|0;F=F+4|0}while((E|0)<(D|0));j=p+34164|0;h=c[j>>2]|0;H=(c[p+34172>>2]|0)+h+(c[v>>2]|0)|0;c[v>>2]=H;g=p+34160|0;s=c[g>>2]|0;k=p+8|0;c[k>>2]=(c[p+34168>>2]|0)+s+(c[k>>2]|0);k=b+32|0;c[k>>2]=(c[k>>2]|0)+h;h=b+40|0;c[h>>2]=(c[h>>2]|0)+s;s=c[3720+(c[p+96>>2]<<2)>>2]|0;t=s<<8;C=aa(c[3728+(c[p+34128>>2]<<2)>>2]|0,s<<4)|0;H=H+15|0;B=H>>>4;l=(aa((c[p+34156>>2]|0)+ -1|0,C)|0)+t<<1;if((aa(l,H>>>20)|0)>>>0>16383){H=-1;i=f;return H|0}n=(aa(l,B)|0)+59846|0;l=ke(n)|0;if((l|0)==0){H=-101;i=f;return H|0}re(l|0,0,n|0)|0;w=l+34232|0;c[w>>2]=l+35064;q=l+35112|0;c[l+34340>>2]=s;c[l+34252>>2]=m;E=l+34120|0;F=x+0|0;D=E+112|0;do{c[E>>2]=c[F>>2];E=E+4|0;F=F+4|0}while((E|0)<(D|0));c[l>>2]=35064;n=l+4|0;E=n+0|0;F=v+0|0;D=E+72|0;do{c[E>>2]=c[F>>2];E=E+4|0;F=F+4|0}while((E|0)<(D|0));m=l+76|0;te(m|0,u|0,32860)|0;c[l+34324>>2]=0;c[l+34328>>2]=0;o=l+34332|0;c[o>>2]=((c[n>>2]|0)+15|0)>>>4;c[l+34336>>2]=((c[l+8>>2]|0)+15|0)>>>4;c[l+34408>>2]=21;z=p+34124|0;p=(c[z>>2]|0)==0?23:22;c[l+34412>>2]=p;c[l+34416>>2]=p;c[l+34424>>2]=24;c[l+34428>>2]=24;c[l+34432>>2]=24;c[l+34436>>2]=24;c[l+34440>>2]=24;c[l+34444>>2]=24;c[l+34448>>2]=24;c[l+34452>>2]=24;c[l+34456>>2]=24;p=l+34928|0;c[p>>2]=0;c[l+34932>>2]=0;H=q+127&-128;q=l+34156|0;if((c[q>>2]|0)!=0){D=l+34524|0;F=l+34588|0;E=t;G=0;while(1){c[D+(G<<2)>>2]=H;I=c[o>>2]|0;c[F+(G<<2)>>2]=H+(aa(I,E)|0);H=H+(aa((c[o>>2]|0)+I|0,E)|0)|0;G=G+1|0;if(G>>>0<(c[q>>2]|0)>>>0){E=C}else{break}}}G=l+34248|0;c[G>>2]=(H+16383&-16384)+8192;C=l+34136|0;if((c[C>>2]|0)!=0){c[r+0>>2]=0;c[r+4>>2]=0;c[r+8>>2]=0;c[r+12>>2]=0;D=(aa(s<<9,B)|0)+35191|0;B=ke(D)|0;if((B|0)==0){I=-101;i=f;return I|0}re(B|0,0,D|0)|0;A=vd(r,c[A>>2]|0)|0;if((A|0)>=0){y=B+35064|0;A=B+34120|0;E=A+0|0;F=x+0|0;D=E+112|0;do{c[E>>2]=c[F>>2];E=E+4|0;F=F+4|0}while((E|0)<(D|0));c[B>>2]=35064;x=B+4|0;E=x+0|0;F=v+0|0;D=E+72|0;do{c[E>>2]=c[F>>2];E=E+4|0;F=F+4|0}while((E|0)<(D|0));v=B+76|0;te(v|0,u|0,32860)|0;c[B+34324>>2]=0;c[B+34328>>2]=0;u=B+34332|0;c[u>>2]=((c[x>>2]|0)+15|0)>>>4;c[B+34336>>2]=((c[B+8>>2]|0)+15|0)>>>4;c[B+34408>>2]=21;H=(c[z>>2]|0)==0?23:22;c[B+34412>>2]=H;c[B+34416>>2]=H;c[B+34424>>2]=24;c[B+34428>>2]=24;c[B+34432>>2]=24;c[B+34436>>2]=24;c[B+34440>>2]=24;c[B+34444>>2]=24;c[B+34448>>2]=24;c[B+34452>>2]=24;c[B+34456>>2]=24;H=B+34928|0;c[H>>2]=0;I=B+34932|0;c[I>>2]=0;Cc(x,v,A,r)|0;zd(r)|0;c[B+34232>>2]=c[w>>2];c[B+34340>>2]=s;c[B+34128>>2]=0;c[B+34156>>2]=1;c[B+34136>>2]=1;y=y+127&-128;c[B+34524>>2]=y;c[B+34588>>2]=y+(aa(c[u>>2]|0,t)|0);c[B+34248>>2]=c[G>>2];c[H>>2]=l;c[I>>2]=1;y=16}}else{a[l+112>>0]=0;B=0;y=16}do{if((y|0)==16){r=(xc(l)|0)!=0;if(!r){r=(Ac(l)|0)!=0;A=r?-101:0;if(!r){if((B|0)!=0){r=(Ac(B)|0)!=0;A=r?-101:0;if(r){break}}c[p>>2]=B;E=b+0|0;F=n+0|0;D=E+72|0;do{c[E>>2]=c[F>>2];E=E+4|0;F=F+4|0}while((E|0)<(D|0));te(d|0,m|0,32860)|0;c[e>>2]=l;c[k>>2]=(c[k>>2]|0)+(c[j>>2]|0);c[h>>2]=(c[h>>2]|0)+(c[g>>2]|0);if((a[l+68>>0]|0)!=0?(Ub(l+34936|0,c[o>>2]|0,c[q>>2]|0)|0,(c[C>>2]|0)!=0):0){Ub(B+34936|0,c[B+34332>>2]|0,c[B+34156>>2]|0)|0}}}else{A=r?-101:0}}}while(0);I=((A|0)!=0)<<31>>31;i=f;return I|0}function Hc(a,b,e){a=a|0;b=b|0;e=e|0;var f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0,I=0,J=0,K=0,L=0;f=i;i=i+16|0;h=f;j=a+34928|0;n=c[j>>2]|0;k=c[a+34128>>2]|0;if((k|0)==1){l=256}else{l=(k|0)==2?512:1024}if((c[a>>2]|0)!=35064){L=-1;i=f;return L|0}k=a+32936|0;c[k+0>>2]=c[b+0>>2];c[k+4>>2]=c[b+4>>2];c[k+8>>2]=c[b+8>>2];c[k+12>>2]=c[b+12>>2];c[k+16>>2]=c[b+16>>2];c[k+20>>2]=c[b+20>>2];o=a+32948|0;if((c[o>>2]|0)==0?(zc(a),(n|0)!=0):0){L=n+32936|0;c[L+0>>2]=c[k+0>>2];c[L+4>>2]=c[k+4>>2];c[L+8>>2]=c[k+8>>2];c[L+12>>2]=c[k+12>>2];c[L+16>>2]=c[k+16>>2];c[L+20>>2]=c[k+20>>2];zc(n)}b=a+34232|0;m=c[b>>2]|0;if((c[m>>2]|0)==0){m=((c[m+36>>2]|0)+16|0)>>>4}else{m=c[a+34336>>2]|0}if((c[o>>2]|0)==0){if((yc(a)|0)!=0){L=-1;i=f;return L|0}if((n|0)!=0?(yc(n)|0)!=0:0){L=-1;i=f;return L|0}}n=a+32952|0;H=c[n>>2]|0;c[h>>2]=c[o>>2];c[h+4>>2]=H;Da(3768,h|0)|0;H=c[o>>2]|0;s=a+34324|0;c[s>>2]=H;a:do{if(!(H>>>0>(c[n>>2]|0)>>>0)){r=a+34424|0;o=a+34428|0;t=a+34432|0;x=a+34124|0;y=a+34328|0;A=a+34716|0;z=a+34332|0;B=a+34156|0;w=a+34412|0;v=a+32956|0;p=a+34408|0;D=a+34448|0;C=a+34452|0;E=a+34456|0;G=a+34436|0;F=a+34440|0;u=a+34444|0;q=a+34416|0;b:while(1){do{if((H|0)!=0){if((m|0)==(H|0)){J=C;K=D;I=E;H=(c[x>>2]|0)==0?23:22;break}else{J=F;K=G;I=u;H=c[q>>2]|0;break}}else{J=o;K=r;I=t;H=(c[x>>2]|0)==0?23:22}}while(0);I=c[I>>2]|0;J=c[J>>2]|0;K=c[K>>2]|0;c[y>>2]=0;ed(a);re(c[A>>2]|0,0,c[z>>2]<<10|0)|0;if((c[B>>2]|0)>>>0>1){L=1;do{re(c[A+(L<<2)>>2]|0,0,aa(c[z>>2]|0,l)|0)|0;L=L+1|0}while(L>>>0<(c[B>>2]|0)>>>0)}L=c[j>>2]|0;if((L|0)!=0){re(c[L+34716>>2]|0,0,c[L+34332>>2]<<10|0)|0}if((Wa[K&31](a)|0)!=0){h=-1;g=40;break}fd(a);c[w>>2]=H;c[y>>2]=1;if((c[z>>2]|0)>>>0>1){do{if((Wa[J&31](a)|0)!=0){h=-1;g=40;break b}fd(a);L=(c[y>>2]|0)+1|0;c[y>>2]=L}while(L>>>0<(c[z>>2]|0)>>>0)}c[w>>2]=(c[x>>2]|0)==0?23:22;if((Wa[I&31](a)|0)!=0){h=-1;g=40;break}J=c[s>>2]|0;do{if((J|0)!=0){I=c[b>>2]|0;H=c[I+12>>2]|0;if(H>>>0<2){if((c[I>>2]|0)==0){J=J<<4;if(J>>>0>(c[I+32>>2]|0)>>>0?!(J>>>0>((c[I+36>>2]|0)+16|0)>>>0):0){g=35}}else{g=35}if((g|0)==35){g=0;Wa[c[p>>2]&31](a)|0;H=c[(c[b>>2]|0)+12>>2]|0}if(!(H>>>0>1)){break}}uc(a)|0}}while(0);gd(a);hd(a);c[e>>2]=c[v>>2];H=(c[s>>2]|0)+1|0;c[s>>2]=H;if(H>>>0>(c[n>>2]|0)>>>0){break a}}if((g|0)==40){i=f;return h|0}}}while(0);c[h>>2]=d[(c[k>>2]|0)+10>>0]|0;Da(3800,h|0)|0;L=0;i=f;return L|0}function Ic(a){a=a|0;var b=0;b=i;if((a|0)!=0){if((c[a>>2]|0)==35064){Bc(a)|0;le(a);a=0}else{a=-1}}else{a=0}i=b;return a|0}function Jc(a){a=a|0;var b=0,d=0,e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0;b=i;ee(a,a+4|0,a+8|0,a+12|0);m=a+20|0;k=a+16|0;g=a+28|0;f=a+24|0;n=c[g>>2]|0;j=c[f>>2]|0;e=j+(c[k>>2]|0)|0;l=(c[m>>2]|0)-n|0;j=j-(e>>1)|0;d=l-((e*3|0)+4>>3)|0;e=((d*3|0)+4>>3)+e|0;n=(l+1>>1)+n-((j*3|0)+4>>3)|0;l=n-(e+1>>1)|0;n=(d+1>>1)-j-((n*3|0)+4>>3)|0;c[m>>2]=d-n;c[k>>2]=l+e;c[g>>2]=l;c[f>>2]=n;f=a+40|0;n=a+32|0;g=a+44|0;l=a+36|0;k=c[g>>2]|0;e=c[l>>2]|0;m=e+(c[n>>2]|0)|0;d=(c[f>>2]|0)-k|0;e=e-(m>>1)|0;j=d-((m*3|0)+4>>3)|0;m=((j*3|0)+4>>3)+m|0;k=(d+1>>1)+k-((e*3|0)+4>>3)|0;d=k-(m+1>>1)|0;k=(j+1>>1)-e-((k*3|0)+4>>3)|0;c[f>>2]=j-k;c[n>>2]=d+m;c[g>>2]=d;c[l>>2]=k;l=a+60|0;k=a+56|0;g=a+52|0;d=a+48|0;n=c[l>>2]|0;m=c[k>>2]|0;f=(c[d>>2]|0)+n|0;j=(c[g>>2]|0)-m|0;e=f>>1;h=j>>1;m=h+m|0;n=n-e-((m*3|0)+3>>3)|0;m=((n*3|0)+3>>2)+m|0;h=m-h|0;e=n-((m*3|0)+4>>3)+e|0;c[l>>2]=e;c[k>>2]=0-h;c[g>>2]=0-(j+h);c[d>>2]=f-e;fe(a);i=b;return}function Kc(a){a=a|0;var b=0,d=0,e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0;b=i;j=a+128|0;r=a+192|0;e=a+384|0;n=a+448|0;o=c[e>>2]|0;d=c[n>>2]|0;f=d+(c[r>>2]|0)|0;s=(c[j>>2]|0)-o|0;d=d-(f>>1)|0;k=s-((f*3|0)+4>>3)|0;f=((k*3|0)+4>>3)+f|0;o=(s+1>>1)+o-((d*3|0)+4>>3)|0;s=o-(f+1>>1)|0;o=(k+1>>1)-d-((o*3|0)+4>>3)|0;c[j>>2]=k-o;c[r>>2]=s+f;c[e>>2]=s;c[n>>2]=o;o=a+512|0;s=a+768|0;f=a+576|0;k=a+832|0;d=c[f>>2]|0;v=c[k>>2]|0;m=v+(c[s>>2]|0)|0;h=(c[o>>2]|0)-d|0;v=v-(m>>1)|0;q=h-((m*3|0)+4>>3)|0;m=((q*3|0)+4>>3)+m|0;d=(h+1>>1)+d-((v*3|0)+4>>3)|0;h=d-(m+1>>1)|0;d=(q+1>>1)-v-((d*3|0)+4>>3)|0;c[o>>2]=q-d;c[s>>2]=h+m;c[f>>2]=h;c[k>>2]=d;d=a+640|0;h=a+896|0;m=a+704|0;q=a+960|0;v=c[d>>2]|0;u=c[h>>2]|0;l=(c[q>>2]|0)+v|0;t=(c[m>>2]|0)-u|0;p=l>>1;g=t>>1;u=g+u|0;v=v-p-((u*3|0)+3>>3)|0;u=((v*3|0)+3>>2)+u|0;g=u-g|0;p=v-((u*3|0)+4>>3)+p|0;c[d>>2]=p;c[h>>2]=0-g;c[m>>2]=0-(t+g);c[q>>2]=l-p;p=a+256|0;l=a+64|0;g=a+320|0;ee(a,p,l,g);de(a,s,r,q);de(p,o,n,m);de(l,k,j,h);de(g,f,e,d);i=b;return}function Lc(a,b,d,e,f){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0;g=i;h=72-d|0;u=a+(h<<2)|0;A=64-d|0;y=b+(A<<2)|0;x=a+48|0;v=b+16|0;de(x,u,v,y);t=a+52|0;r=a+(h+1<<2)|0;q=b+20|0;s=b+(A+1<<2)|0;de(t,r,q,s);j=a+56|0;l=a+(h+2<<2)|0;n=b+24|0;m=b+(A+2<<2)|0;de(j,l,n,m);d=a+60|0;a=a+(h+3<<2)|0;h=b+28|0;b=b+(A+3<<2)|0;de(d,a,h,b);A=c[y>>2]|0;B=c[s>>2]|0;z=(c[b>>2]|0)+A|0;C=(c[m>>2]|0)-B|0;k=z>>1;D=C>>1;B=D+B|0;A=A-k-((B*3|0)+6>>3)|0;B=((A*3|0)+2>>2)+B|0;D=B-D|0;k=A-((B*3|0)+4>>3)+k|0;c[y>>2]=k;c[s>>2]=D;c[m>>2]=D+C;c[b>>2]=z-k;k=c[h>>2]|0;z=(c[n>>2]|0)-(k+1>>1)|0;c[n>>2]=z;c[h>>2]=(z+1>>1)+k;k=c[q>>2]|0;z=(c[v>>2]|0)-(k+1>>1)|0;c[v>>2]=z;c[q>>2]=(z+1>>1)+k;k=c[a>>2]|0;z=(c[r>>2]|0)-(k+1>>1)|0;c[r>>2]=z;c[a>>2]=(z+1>>1)+k;k=c[l>>2]|0;z=(c[u>>2]|0)-(k+1>>1)|0;c[u>>2]=z;c[l>>2]=(z+1>>1)+k;k=c[y>>2]|0;z=k+(c[x>>2]|0)|0;k=(z>>1)-k|0;z=(k*3>>3)+z|0;c[x>>2]=z;c[y>>2]=(z*3>>4)+k;k=c[s>>2]|0;z=k+(c[t>>2]|0)|0;k=(z>>1)-k|0;z=(k*3>>3)+z|0;c[t>>2]=z;c[s>>2]=(z*3>>4)+k;k=c[m>>2]|0;z=k+(c[j>>2]|0)|0;k=(z>>1)-k|0;z=(k*3>>3)+z|0;c[j>>2]=z;c[m>>2]=(z*3>>4)+k;k=c[b>>2]|0;z=k+(c[d>>2]|0)|0;k=(z>>1)-k|0;z=(k*3>>3)+z|0;c[d>>2]=z;c[b>>2]=(z*3>>4)+k;k=c[v>>2]|0;z=c[y>>2]|0;C=(c[u>>2]|0)-k|0;D=((z*3|0)+4>>3)+(c[x>>2]|0)|0;z=z-(C>>1)|0;k=(D-C>>1)-k|0;c[v>>2]=z;c[y>>2]=k;c[x>>2]=D-k;c[u>>2]=z+C;C=c[q>>2]|0;z=c[s>>2]|0;k=(c[r>>2]|0)-C|0;D=((z*3|0)+4>>3)+(c[t>>2]|0)|0;z=z-(k>>1)|0;C=(D-k>>1)-C|0;c[q>>2]=z;c[s>>2]=C;c[t>>2]=D-C;c[r>>2]=z+k;k=c[n>>2]|0;z=c[m>>2]|0;C=(c[l>>2]|0)-k|0;D=((z*3|0)+4>>3)+(c[j>>2]|0)|0;z=z-(C>>1)|0;k=(D-C>>1)-k|0;c[n>>2]=z;c[m>>2]=k;c[j>>2]=D-k;c[l>>2]=z+C;C=c[h>>2]|0;z=c[b>>2]|0;k=(c[a>>2]|0)-C|0;D=((z*3|0)+4>>3)+(c[d>>2]|0)|0;z=z-(k>>1)|0;C=(D-k>>1)-C|0;c[h>>2]=z;c[b>>2]=C;c[d>>2]=D-C;k=z+k|0;c[a>>2]=k;z=c[x>>2]|0;C=c[v>>2]|0;D=c[u>>2]|0;B=c[y>>2]|0;A=((C+z+D+B>>1)*595|0)+65536>>17;w=(((c[q>>2]|0)+(c[t>>2]|0)+(c[r>>2]|0)+(c[s>>2]|0)>>1)*595|0)+65536>>17;p=(((c[n>>2]|0)+(c[j>>2]|0)+(c[l>>2]|0)+(c[m>>2]|0)>>1)*595|0)+65536>>17;k=(((c[d>>2]|0)+k+(c[h>>2]|0)+(c[b>>2]|0)>>1)*595|0)+65536>>17;o=(e|0)>20;f=(f|0)==0;if(!(f&((((A|0)>-1?A:0-A|0)|0)<(e|0)&o^1))){B=z-C-D+B>>1;if((A|0)>0){if((B|0)>0){A=(A|0)<(B|0)?A:B}else{A=0}}else{if((A&B|0)<0){A=(A|0)>(B|0)?A:B}else{A=0}}D=A>>1;c[x>>2]=z-D;c[y>>2]=(c[y>>2]|0)-D;c[u>>2]=(c[u>>2]|0)+D;c[v>>2]=(c[v>>2]|0)+D}if(!(f&((((w|0)>-1?w:0-w|0)|0)<(e|0)&o^1))){u=c[t>>2]|0;v=u-(c[q>>2]|0)-(c[r>>2]|0)+(c[s>>2]|0)>>1;if((w|0)>0){if((v|0)>0){v=(w|0)<(v|0)?w:v}else{v=0}}else{if((v&w|0)<0){v=(w|0)>(v|0)?w:v}else{v=0}}D=v>>1;c[t>>2]=u-D;c[s>>2]=(c[s>>2]|0)-D;c[r>>2]=(c[r>>2]|0)+D;c[q>>2]=(c[q>>2]|0)+D}if(!(f&((((p|0)>-1?p:0-p|0)|0)<(e|0)&o^1))){q=c[j>>2]|0;r=q-(c[n>>2]|0)-(c[l>>2]|0)+(c[m>>2]|0)>>1;if((p|0)>0){if((r|0)>0){p=(p|0)<(r|0)?p:r}else{p=0}}else{if((r&p|0)<0){p=(p|0)>(r|0)?p:r}else{p=0}}D=p>>1;c[j>>2]=q-D;c[m>>2]=(c[m>>2]|0)-D;c[l>>2]=(c[l>>2]|0)+D;c[n>>2]=(c[n>>2]|0)+D}if(f&((((k|0)>-1?k:0-k|0)|0)<(e|0)&o^1)){i=g;return}e=c[d>>2]|0;j=e-(c[h>>2]|0)-(c[a>>2]|0)+(c[b>>2]|0)>>1;if((k|0)>0){if((j|0)>0){j=(k|0)<(j|0)?k:j}else{j=0}}else{if((j&k|0)<0){j=(k|0)>(j|0)?k:j}else{j=0}}D=j>>1;c[d>>2]=e-D;c[b>>2]=(c[b>>2]|0)-D;c[a>>2]=(c[a>>2]|0)+D;c[h>>2]=(c[h>>2]|0)+D;i=g;return}function Mc(a,b,d){a=a|0;b=b|0;d=d|0;var e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0;e=i;k=72-d|0;p=a+(k<<2)|0;j=64-d|0;v=b+(j<<2)|0;q=a+48|0;w=b+16|0;de(q,p,w,v);m=a+52|0;l=a+(k+1<<2)|0;s=b+20|0;r=b+(j+1<<2)|0;de(m,l,s,r);h=a+56|0;g=a+(k+2<<2)|0;o=b+24|0;n=b+(j+2<<2)|0;de(h,g,o,n);f=a+60|0;d=a+(k+3<<2)|0;k=b+28|0;j=b+(j+3<<2)|0;de(f,d,k,j);y=c[v>>2]|0;x=c[r>>2]|0;a=(c[j>>2]|0)+y|0;b=(c[n>>2]|0)-x|0;t=a>>1;u=b>>1;x=u+x|0;y=y-t-((x*3|0)+6>>3)|0;x=((y*3|0)+2>>2)+x|0;u=x-u|0;t=y-((x*3|0)+4>>3)+t|0;c[v>>2]=t;c[r>>2]=u;c[n>>2]=u+b;c[j>>2]=a-t;t=c[k>>2]|0;a=(c[o>>2]|0)-(t+1>>1)|0;c[o>>2]=a;c[k>>2]=(a+1>>1)+t;t=c[s>>2]|0;a=(c[w>>2]|0)-(t+1>>1)|0;c[w>>2]=a;c[s>>2]=(a+1>>1)+t;t=c[d>>2]|0;a=(c[l>>2]|0)-(t+1>>1)|0;c[l>>2]=a;c[d>>2]=(a+1>>1)+t;t=c[g>>2]|0;a=(c[p>>2]|0)-(t+1>>1)|0;c[p>>2]=a;c[g>>2]=(a+1>>1)+t;t=c[v>>2]|0;a=t+(c[q>>2]|0)|0;t=(a>>1)-t|0;a=(t*3>>3)+a|0;c[q>>2]=a;c[v>>2]=(a>>7)+t-(a>>10)+(a*3>>4);a=c[r>>2]|0;t=a+(c[m>>2]|0)|0;a=(t>>1)-a|0;t=(a*3>>3)+t|0;c[m>>2]=t;c[r>>2]=(t>>7)+a-(t>>10)+(t*3>>4);t=c[n>>2]|0;a=t+(c[h>>2]|0)|0;t=(a>>1)-t|0;a=(t*3>>3)+a|0;c[h>>2]=a;c[n>>2]=(a>>7)+t-(a>>10)+(a*3>>4);a=c[j>>2]|0;t=a+(c[f>>2]|0)|0;a=(t>>1)-a|0;t=(a*3>>3)+t|0;c[f>>2]=t;c[j>>2]=(t>>7)+a-(t>>10)+(t*3>>4);t=c[w>>2]|0;a=c[v>>2]|0;b=(c[p>>2]|0)-t|0;u=((a*3|0)+4>>3)+(c[q>>2]|0)|0;a=a-(b>>1)|0;t=(u-b>>1)-t|0;c[w>>2]=a;c[v>>2]=t;c[q>>2]=u-t;c[p>>2]=a+b;p=c[s>>2]|0;b=c[r>>2]|0;a=(c[l>>2]|0)-p|0;q=((b*3|0)+4>>3)+(c[m>>2]|0)|0;b=b-(a>>1)|0;p=(q-a>>1)-p|0;c[s>>2]=b;c[r>>2]=p;c[m>>2]=q-p;c[l>>2]=b+a;l=c[o>>2]|0;a=c[n>>2]|0;b=(c[g>>2]|0)-l|0;m=((a*3|0)+4>>3)+(c[h>>2]|0)|0;a=a-(b>>1)|0;l=(m-b>>1)-l|0;c[o>>2]=a;c[n>>2]=l;c[h>>2]=m-l;c[g>>2]=a+b;g=c[k>>2]|0;b=c[j>>2]|0;a=(c[d>>2]|0)-g|0;h=((b*3|0)+4>>3)+(c[f>>2]|0)|0;b=b-(a>>1)|0;g=(h-a>>1)-g|0;c[k>>2]=b;c[j>>2]=g;c[f>>2]=h-g;c[d>>2]=b+a;i=e;return}function Nc(a,b){a=a|0;b=b|0;var d=0,e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0;d=i;q=a+ -384|0;w=a+384|0;p=b+ -448|0;v=b+320|0;de(q,w,p,v);m=a+ -128|0;s=a+128|0;l=b+ -192|0;r=b+64|0;de(m,s,l,r);j=a+ -320|0;o=a+448|0;h=b+ -512|0;n=b+256|0;de(j,o,h,n);g=a+ -64|0;k=a+192|0;a=b+ -256|0;de(g,k,a,b);y=c[b>>2]|0;x=c[n>>2]|0;e=(c[v>>2]|0)+y|0;f=(c[r>>2]|0)-x|0;t=e>>1;u=f>>1;x=u+x|0;y=y-t-((x*3|0)+6>>3)|0;x=((y*3|0)+2>>2)+x|0;u=x-u|0;t=y-((x*3|0)+4>>3)+t|0;c[b>>2]=t;c[n>>2]=u;c[r>>2]=u+f;c[v>>2]=e-t;t=c[s>>2]|0;e=(c[k>>2]|0)-(t+1>>1)|0;c[k>>2]=e;c[s>>2]=(e+1>>1)+t;t=c[w>>2]|0;e=(c[o>>2]|0)-(t+1>>1)|0;c[o>>2]=e;c[w>>2]=(e+1>>1)+t;t=c[h>>2]|0;e=(c[a>>2]|0)-(t+1>>1)|0;c[a>>2]=e;c[h>>2]=(e+1>>1)+t;t=c[p>>2]|0;e=(c[l>>2]|0)-(t+1>>1)|0;c[l>>2]=e;c[p>>2]=(e+1>>1)+t;t=c[v>>2]|0;e=t+(c[q>>2]|0)|0;t=(e>>1)-t|0;e=(t*3>>3)+e|0;c[q>>2]=e;c[v>>2]=(e*3>>4)+t;t=c[r>>2]|0;e=t+(c[m>>2]|0)|0;t=(e>>1)-t|0;e=(t*3>>3)+e|0;c[m>>2]=e;c[r>>2]=(e*3>>4)+t;t=c[n>>2]|0;e=t+(c[j>>2]|0)|0;t=(e>>1)-t|0;e=(t*3>>3)+e|0;c[j>>2]=e;c[n>>2]=(e*3>>4)+t;t=c[b>>2]|0;e=t+(c[g>>2]|0)|0;t=(e>>1)-t|0;e=(t*3>>3)+e|0;c[g>>2]=e;c[b>>2]=(e*3>>4)+t;t=c[w>>2]|0;e=c[v>>2]|0;f=(c[p>>2]|0)-t|0;u=((e*3|0)+4>>3)+(c[q>>2]|0)|0;e=e-(f>>1)|0;t=(u-f>>1)-t|0;c[w>>2]=e;c[v>>2]=t;c[q>>2]=u-t;c[p>>2]=e+f;p=c[s>>2]|0;f=c[r>>2]|0;e=(c[l>>2]|0)-p|0;q=((f*3|0)+4>>3)+(c[m>>2]|0)|0;f=f-(e>>1)|0;p=(q-e>>1)-p|0;c[s>>2]=f;c[r>>2]=p;c[m>>2]=q-p;c[l>>2]=f+e;l=c[o>>2]|0;e=c[n>>2]|0;f=(c[h>>2]|0)-l|0;m=((e*3|0)+4>>3)+(c[j>>2]|0)|0;e=e-(f>>1)|0;l=(m-f>>1)-l|0;c[o>>2]=e;c[n>>2]=l;c[j>>2]=m-l;c[h>>2]=e+f;h=c[k>>2]|0;f=c[b>>2]|0;e=(c[a>>2]|0)-h|0;j=((f*3|0)+4>>3)+(c[g>>2]|0)|0;f=f-(e>>1)|0;h=(j-e>>1)-h|0;c[k>>2]=f;c[b>>2]=h;c[g>>2]=j-h;c[a>>2]=f+e;i=d;return}function Oc(a,b){a=a|0;b=b|0;var d=0,e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0;d=i;q=a+ -384|0;w=a+384|0;p=b+ -448|0;v=b+320|0;de(q,w,p,v);m=a+ -128|0;s=a+128|0;l=b+ -192|0;r=b+64|0;de(m,s,l,r);j=a+ -320|0;o=a+448|0;h=b+ -512|0;n=b+256|0;de(j,o,h,n);g=a+ -64|0;k=a+192|0;a=b+ -256|0;de(g,k,a,b);y=c[b>>2]|0;x=c[n>>2]|0;e=(c[v>>2]|0)+y|0;f=(c[r>>2]|0)-x|0;t=e>>1;u=f>>1;x=u+x|0;y=y-t-((x*3|0)+6>>3)|0;x=((y*3|0)+2>>2)+x|0;u=x-u|0;t=y-((x*3|0)+4>>3)+t|0;c[b>>2]=t;c[n>>2]=u;c[r>>2]=u+f;c[v>>2]=e-t;t=c[s>>2]|0;e=(c[k>>2]|0)-(t+1>>1)|0;c[k>>2]=e;c[s>>2]=(e+1>>1)+t;t=c[w>>2]|0;e=(c[o>>2]|0)-(t+1>>1)|0;c[o>>2]=e;c[w>>2]=(e+1>>1)+t;t=c[h>>2]|0;e=(c[a>>2]|0)-(t+1>>1)|0;c[a>>2]=e;c[h>>2]=(e+1>>1)+t;t=c[p>>2]|0;e=(c[l>>2]|0)-(t+1>>1)|0;c[l>>2]=e;c[p>>2]=(e+1>>1)+t;t=c[v>>2]|0;e=t+(c[q>>2]|0)|0;t=(e>>1)-t|0;e=(t*3>>3)+e|0;c[q>>2]=e;c[v>>2]=(e>>7)+t-(e>>10)+(e*3>>4);e=c[r>>2]|0;t=e+(c[m>>2]|0)|0;e=(t>>1)-e|0;t=(e*3>>3)+t|0;c[m>>2]=t;c[r>>2]=(t>>7)+e-(t>>10)+(t*3>>4);t=c[n>>2]|0;e=t+(c[j>>2]|0)|0;t=(e>>1)-t|0;e=(t*3>>3)+e|0;c[j>>2]=e;c[n>>2]=(e>>7)+t-(e>>10)+(e*3>>4);e=c[b>>2]|0;t=e+(c[g>>2]|0)|0;e=(t>>1)-e|0;t=(e*3>>3)+t|0;c[g>>2]=t;c[b>>2]=(t>>7)+e-(t>>10)+(t*3>>4);t=c[w>>2]|0;e=c[v>>2]|0;f=(c[p>>2]|0)-t|0;u=((e*3|0)+4>>3)+(c[q>>2]|0)|0;e=e-(f>>1)|0;t=(u-f>>1)-t|0;c[w>>2]=e;c[v>>2]=t;c[q>>2]=u-t;c[p>>2]=e+f;p=c[s>>2]|0;f=c[r>>2]|0;e=(c[l>>2]|0)-p|0;q=((f*3|0)+4>>3)+(c[m>>2]|0)|0;f=f-(e>>1)|0;p=(q-e>>1)-p|0;c[s>>2]=f;c[r>>2]=p;c[m>>2]=q-p;c[l>>2]=f+e;l=c[o>>2]|0;e=c[n>>2]|0;f=(c[h>>2]|0)-l|0;m=((e*3|0)+4>>3)+(c[j>>2]|0)|0;e=e-(f>>1)|0;l=(m-f>>1)-l|0;c[o>>2]=e;c[n>>2]=l;c[j>>2]=m-l;c[h>>2]=e+f;h=c[k>>2]|0;f=c[b>>2]|0;e=(c[a>>2]|0)-h|0;j=((f*3|0)+4>>3)+(c[g>>2]|0)|0;f=f-(e>>1)|0;h=(j-e>>1)-h|0;c[k>>2]=f;c[b>>2]=h;c[g>>2]=j-h;c[a>>2]=f+e;i=d;return}



function ke(a){a=a|0;var b=0,d=0,e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0,x=0,y=0,z=0,A=0,B=0,C=0,D=0,E=0,F=0,G=0,H=0;b=i;do{if(a>>>0<245){if(a>>>0<11){a=16}else{a=a+11&-8}v=a>>>3;r=c[2382]|0;w=r>>>v;if((w&3|0)!=0){j=(w&1^1)+v|0;h=j<<1;f=9568+(h<<2)|0;h=9568+(h+2<<2)|0;d=c[h>>2]|0;e=d+8|0;g=c[e>>2]|0;do{if((f|0)!=(g|0)){if(g>>>0<(c[9544>>2]|0)>>>0){Na()}k=g+12|0;if((c[k>>2]|0)==(d|0)){c[k>>2]=f;c[h>>2]=g;break}else{Na()}}else{c[2382]=r&~(1<<j)}}while(0);H=j<<3;c[d+4>>2]=H|3;H=d+(H|4)|0;c[H>>2]=c[H>>2]|1;H=e;i=b;return H|0}if(a>>>0>(c[9536>>2]|0)>>>0){if((w|0)!=0){f=2<<v;f=w<<v&(f|0-f);f=(f&0-f)+ -1|0;d=f>>>12&16;f=f>>>d;e=f>>>5&8;f=f>>>e;g=f>>>2&4;f=f>>>g;j=f>>>1&2;f=f>>>j;h=f>>>1&1;h=(e|d|g|j|h)+(f>>>h)|0;f=h<<1;j=9568+(f<<2)|0;f=9568+(f+2<<2)|0;g=c[f>>2]|0;d=g+8|0;e=c[d>>2]|0;do{if((j|0)!=(e|0)){if(e>>>0<(c[9544>>2]|0)>>>0){Na()}k=e+12|0;if((c[k>>2]|0)==(g|0)){c[k>>2]=j;c[f>>2]=e;break}else{Na()}}else{c[2382]=r&~(1<<h)}}while(0);h=h<<3;f=h-a|0;c[g+4>>2]=a|3;e=g+a|0;c[g+(a|4)>>2]=f|1;c[g+h>>2]=f;h=c[9536>>2]|0;if((h|0)!=0){g=c[9548>>2]|0;l=h>>>3;j=l<<1;h=9568+(j<<2)|0;k=c[2382]|0;l=1<<l;if((k&l|0)!=0){j=9568+(j+2<<2)|0;k=c[j>>2]|0;if(k>>>0<(c[9544>>2]|0)>>>0){Na()}else{D=j;C=k}}else{c[2382]=k|l;D=9568+(j+2<<2)|0;C=h}c[D>>2]=g;c[C+12>>2]=g;c[g+8>>2]=C;c[g+12>>2]=h}c[9536>>2]=f;c[9548>>2]=e;H=d;i=b;return H|0}r=c[9532>>2]|0;if((r|0)!=0){d=(r&0-r)+ -1|0;G=d>>>12&16;d=d>>>G;F=d>>>5&8;d=d>>>F;H=d>>>2&4;d=d>>>H;h=d>>>1&2;d=d>>>h;e=d>>>1&1;e=c[9832+((F|G|H|h|e)+(d>>>e)<<2)>>2]|0;d=(c[e+4>>2]&-8)-a|0;h=e;while(1){g=c[h+16>>2]|0;if((g|0)==0){g=c[h+20>>2]|0;if((g|0)==0){break}}h=(c[g+4>>2]&-8)-a|0;f=h>>>0<d>>>0;d=f?h:d;h=g;e=f?g:e}h=c[9544>>2]|0;if(e>>>0<h>>>0){Na()}f=e+a|0;if(!(e>>>0<f>>>0)){Na()}g=c[e+24>>2]|0;k=c[e+12>>2]|0;do{if((k|0)==(e|0)){k=e+20|0;j=c[k>>2]|0;if((j|0)==0){k=e+16|0;j=c[k>>2]|0;if((j|0)==0){B=0;break}}while(1){l=j+20|0;m=c[l>>2]|0;if((m|0)!=0){j=m;k=l;continue}l=j+16|0;m=c[l>>2]|0;if((m|0)==0){break}else{j=m;k=l}}if(k>>>0<h>>>0){Na()}else{c[k>>2]=0;B=j;break}}else{j=c[e+8>>2]|0;if(j>>>0<h>>>0){Na()}h=j+12|0;if((c[h>>2]|0)!=(e|0)){Na()}l=k+8|0;if((c[l>>2]|0)==(e|0)){c[h>>2]=k;c[l>>2]=j;B=k;break}else{Na()}}}while(0);do{if((g|0)!=0){j=c[e+28>>2]|0;h=9832+(j<<2)|0;if((e|0)==(c[h>>2]|0)){c[h>>2]=B;if((B|0)==0){c[9532>>2]=c[9532>>2]&~(1<<j);break}}else{if(g>>>0<(c[9544>>2]|0)>>>0){Na()}h=g+16|0;if((c[h>>2]|0)==(e|0)){c[h>>2]=B}else{c[g+20>>2]=B}if((B|0)==0){break}}if(B>>>0<(c[9544>>2]|0)>>>0){Na()}c[B+24>>2]=g;g=c[e+16>>2]|0;do{if((g|0)!=0){if(g>>>0<(c[9544>>2]|0)>>>0){Na()}else{c[B+16>>2]=g;c[g+24>>2]=B;break}}}while(0);g=c[e+20>>2]|0;if((g|0)!=0){if(g>>>0<(c[9544>>2]|0)>>>0){Na()}else{c[B+20>>2]=g;c[g+24>>2]=B;break}}}}while(0);if(d>>>0<16){H=d+a|0;c[e+4>>2]=H|3;H=e+(H+4)|0;c[H>>2]=c[H>>2]|1}else{c[e+4>>2]=a|3;c[e+(a|4)>>2]=d|1;c[e+(d+a)>>2]=d;h=c[9536>>2]|0;if((h|0)!=0){g=c[9548>>2]|0;j=h>>>3;k=j<<1;h=9568+(k<<2)|0;l=c[2382]|0;j=1<<j;if((l&j|0)!=0){k=9568+(k+2<<2)|0;j=c[k>>2]|0;if(j>>>0<(c[9544>>2]|0)>>>0){Na()}else{z=k;A=j}}else{c[2382]=l|j;z=9568+(k+2<<2)|0;A=h}c[z>>2]=g;c[A+12>>2]=g;c[g+8>>2]=A;c[g+12>>2]=h}c[9536>>2]=d;c[9548>>2]=f}H=e+8|0;i=b;return H|0}}}else{if(!(a>>>0>4294967231)){z=a+11|0;a=z&-8;B=c[9532>>2]|0;if((B|0)!=0){A=0-a|0;z=z>>>8;if((z|0)!=0){if(a>>>0>16777215){C=31}else{G=(z+1048320|0)>>>16&8;H=z<<G;F=(H+520192|0)>>>16&4;H=H<<F;C=(H+245760|0)>>>16&2;C=14-(F|G|C)+(H<<C>>>15)|0;C=a>>>(C+7|0)&1|C<<1}}else{C=0}F=c[9832+(C<<2)>>2]|0;a:do{if((F|0)==0){D=0;z=0}else{if((C|0)==31){z=0}else{z=25-(C>>>1)|0}D=0;E=a<<z;z=0;while(1){H=c[F+4>>2]&-8;G=H-a|0;if(G>>>0<A>>>0){if((H|0)==(a|0)){A=G;D=F;z=F;break a}else{A=G;z=F}}G=c[F+20>>2]|0;F=c[F+(E>>>31<<2)+16>>2]|0;D=(G|0)==0|(G|0)==(F|0)?D:G;if((F|0)==0){break}else{E=E<<1}}}}while(0);if((D|0)==0&(z|0)==0){H=2<<C;B=B&(H|0-H);if((B|0)==0){break}H=(B&0-B)+ -1|0;E=H>>>12&16;H=H>>>E;C=H>>>5&8;H=H>>>C;F=H>>>2&4;H=H>>>F;G=H>>>1&2;H=H>>>G;D=H>>>1&1;D=c[9832+((C|E|F|G|D)+(H>>>D)<<2)>>2]|0}if((D|0)!=0){while(1){C=(c[D+4>>2]&-8)-a|0;B=C>>>0<A>>>0;A=B?C:A;z=B?D:z;B=c[D+16>>2]|0;if((B|0)!=0){D=B;continue}D=c[D+20>>2]|0;if((D|0)==0){break}}}if((z|0)!=0?A>>>0<((c[9536>>2]|0)-a|0)>>>0:0){f=c[9544>>2]|0;if(z>>>0<f>>>0){Na()}d=z+a|0;if(!(z>>>0<d>>>0)){Na()}e=c[z+24>>2]|0;g=c[z+12>>2]|0;do{if((g|0)==(z|0)){h=z+20|0;g=c[h>>2]|0;if((g|0)==0){h=z+16|0;g=c[h>>2]|0;if((g|0)==0){x=0;break}}while(1){j=g+20|0;k=c[j>>2]|0;if((k|0)!=0){g=k;h=j;continue}j=g+16|0;k=c[j>>2]|0;if((k|0)==0){break}else{g=k;h=j}}if(h>>>0<f>>>0){Na()}else{c[h>>2]=0;x=g;break}}else{h=c[z+8>>2]|0;if(h>>>0<f>>>0){Na()}j=h+12|0;if((c[j>>2]|0)!=(z|0)){Na()}f=g+8|0;if((c[f>>2]|0)==(z|0)){c[j>>2]=g;c[f>>2]=h;x=g;break}else{Na()}}}while(0);do{if((e|0)!=0){g=c[z+28>>2]|0;f=9832+(g<<2)|0;if((z|0)==(c[f>>2]|0)){c[f>>2]=x;if((x|0)==0){c[9532>>2]=c[9532>>2]&~(1<<g);break}}else{if(e>>>0<(c[9544>>2]|0)>>>0){Na()}f=e+16|0;if((c[f>>2]|0)==(z|0)){c[f>>2]=x}else{c[e+20>>2]=x}if((x|0)==0){break}}if(x>>>0<(c[9544>>2]|0)>>>0){Na()}c[x+24>>2]=e;e=c[z+16>>2]|0;do{if((e|0)!=0){if(e>>>0<(c[9544>>2]|0)>>>0){Na()}else{c[x+16>>2]=e;c[e+24>>2]=x;break}}}while(0);e=c[z+20>>2]|0;if((e|0)!=0){if(e>>>0<(c[9544>>2]|0)>>>0){Na()}else{c[x+20>>2]=e;c[e+24>>2]=x;break}}}}while(0);b:do{if(!(A>>>0<16)){c[z+4>>2]=a|3;c[z+(a|4)>>2]=A|1;c[z+(A+a)>>2]=A;f=A>>>3;if(A>>>0<256){g=f<<1;e=9568+(g<<2)|0;h=c[2382]|0;f=1<<f;if((h&f|0)!=0){f=9568+(g+2<<2)|0;g=c[f>>2]|0;if(g>>>0<(c[9544>>2]|0)>>>0){Na()}else{w=f;v=g}}else{c[2382]=h|f;w=9568+(g+2<<2)|0;v=e}c[w>>2]=d;c[v+12>>2]=d;c[z+(a+8)>>2]=v;c[z+(a+12)>>2]=e;break}e=A>>>8;if((e|0)!=0){if(A>>>0>16777215){e=31}else{G=(e+1048320|0)>>>16&8;H=e<<G;F=(H+520192|0)>>>16&4;H=H<<F;e=(H+245760|0)>>>16&2;e=14-(F|G|e)+(H<<e>>>15)|0;e=A>>>(e+7|0)&1|e<<1}}else{e=0}f=9832+(e<<2)|0;c[z+(a+28)>>2]=e;c[z+(a+20)>>2]=0;c[z+(a+16)>>2]=0;h=c[9532>>2]|0;g=1<<e;if((h&g|0)==0){c[9532>>2]=h|g;c[f>>2]=d;c[z+(a+24)>>2]=f;c[z+(a+12)>>2]=d;c[z+(a+8)>>2]=d;break}f=c[f>>2]|0;if((e|0)==31){e=0}else{e=25-(e>>>1)|0}c:do{if((c[f+4>>2]&-8|0)!=(A|0)){e=A<<e;g=f;while(1){h=g+(e>>>31<<2)+16|0;f=c[h>>2]|0;if((f|0)==0){break}if((c[f+4>>2]&-8|0)==(A|0)){r=f;break c}else{e=e<<1;g=f}}if(h>>>0<(c[9544>>2]|0)>>>0){Na()}else{c[h>>2]=d;c[z+(a+24)>>2]=g;c[z+(a+12)>>2]=d;c[z+(a+8)>>2]=d;break b}}else{r=f}}while(0);f=r+8|0;e=c[f>>2]|0;g=c[9544>>2]|0;if(r>>>0<g>>>0){Na()}if(e>>>0<g>>>0){Na()}else{c[e+12>>2]=d;c[f>>2]=d;c[z+(a+8)>>2]=e;c[z+(a+12)>>2]=r;c[z+(a+24)>>2]=0;break}}else{H=A+a|0;c[z+4>>2]=H|3;H=z+(H+4)|0;c[H>>2]=c[H>>2]|1}}while(0);H=z+8|0;i=b;return H|0}}}else{a=-1}}}while(0);r=c[9536>>2]|0;if(!(a>>>0>r>>>0)){e=r-a|0;d=c[9548>>2]|0;if(e>>>0>15){c[9548>>2]=d+a;c[9536>>2]=e;c[d+(a+4)>>2]=e|1;c[d+r>>2]=e;c[d+4>>2]=a|3}else{c[9536>>2]=0;c[9548>>2]=0;c[d+4>>2]=r|3;H=d+(r+4)|0;c[H>>2]=c[H>>2]|1}H=d+8|0;i=b;return H|0}r=c[9540>>2]|0;if(a>>>0<r>>>0){G=r-a|0;c[9540>>2]=G;H=c[9552>>2]|0;c[9552>>2]=H+a;c[H+(a+4)>>2]=G|1;c[H+4>>2]=a|3;H=H+8|0;i=b;return H|0}do{if((c[2500]|0)==0){r=ya(30)|0;if((r+ -1&r|0)==0){c[10008>>2]=r;c[10004>>2]=r;c[10012>>2]=-1;c[10016>>2]=-1;c[10020>>2]=0;c[9972>>2]=0;c[2500]=(sa(0)|0)&-16^1431655768;break}else{Na()}}}while(0);w=a+48|0;A=c[10008>>2]|0;x=a+47|0;z=A+x|0;A=0-A|0;v=z&A;if(!(v>>>0>a>>>0)){H=0;i=b;return H|0}r=c[9968>>2]|0;if((r|0)!=0?(G=c[9960>>2]|0,H=G+v|0,H>>>0<=G>>>0|H>>>0>r>>>0):0){H=0;i=b;return H|0}d:do{if((c[9972>>2]&4|0)==0){B=c[9552>>2]|0;e:do{if((B|0)!=0){r=9976|0;while(1){C=c[r>>2]|0;if(!(C>>>0>B>>>0)?(y=r+4|0,(C+(c[y>>2]|0)|0)>>>0>B>>>0):0){break}r=c[r+8>>2]|0;if((r|0)==0){o=182;break e}}if((r|0)!=0){A=z-(c[9540>>2]|0)&A;if(A>>>0<2147483647){o=va(A|0)|0;B=(o|0)==((c[r>>2]|0)+(c[y>>2]|0)|0);y=o;z=A;r=B?o:-1;A=B?A:0;o=191}else{A=0}}else{o=182}}else{o=182}}while(0);do{if((o|0)==182){r=va(0)|0;if((r|0)!=(-1|0)){A=r;y=c[10004>>2]|0;z=y+ -1|0;if((z&A|0)==0){A=v}else{A=v-A+(z+A&0-y)|0}B=c[9960>>2]|0;z=B+A|0;if(A>>>0>a>>>0&A>>>0<2147483647){y=c[9968>>2]|0;if((y|0)!=0?z>>>0<=B>>>0|z>>>0>y>>>0:0){A=0;break}y=va(A|0)|0;o=(y|0)==(r|0);z=A;r=o?r:-1;A=o?A:0;o=191}else{A=0}}else{A=0}}}while(0);f:do{if((o|0)==191){o=0-z|0;if((r|0)!=(-1|0)){p=A;o=202;break d}do{if((y|0)!=(-1|0)&z>>>0<2147483647&z>>>0<w>>>0?(u=c[10008>>2]|0,u=x-z+u&0-u,u>>>0<2147483647):0){if((va(u|0)|0)==(-1|0)){va(o|0)|0;break f}else{z=u+z|0;break}}}while(0);if((y|0)!=(-1|0)){r=y;p=z;o=202;break d}}}while(0);c[9972>>2]=c[9972>>2]|4;o=199}else{A=0;o=199}}while(0);if((((o|0)==199?v>>>0<2147483647:0)?(s=va(v|0)|0,t=va(0)|0,(t|0)!=(-1|0)&(s|0)!=(-1|0)&s>>>0<t>>>0):0)?(p=t-s|0,q=p>>>0>(a+40|0)>>>0,q):0){r=s;p=q?p:A;o=202}if((o|0)==202){q=(c[9960>>2]|0)+p|0;c[9960>>2]=q;if(q>>>0>(c[9964>>2]|0)>>>0){c[9964>>2]=q}q=c[9552>>2]|0;g:do{if((q|0)!=0){v=9976|0;while(1){w=c[v>>2]|0;t=v+4|0;u=c[t>>2]|0;if((r|0)==(w+u|0)){o=214;break}s=c[v+8>>2]|0;if((s|0)==0){break}else{v=s}}if(((o|0)==214?(c[v+12>>2]&8|0)==0:0)?q>>>0>=w>>>0&q>>>0<r>>>0:0){c[t>>2]=u+p;d=(c[9540>>2]|0)+p|0;e=q+8|0;if((e&7|0)==0){e=0}else{e=0-e&7}H=d-e|0;c[9552>>2]=q+e;c[9540>>2]=H;c[q+(e+4)>>2]=H|1;c[q+(d+4)>>2]=40;c[9556>>2]=c[10016>>2];break}if(r>>>0<(c[9544>>2]|0)>>>0){c[9544>>2]=r}t=r+p|0;s=9976|0;while(1){if((c[s>>2]|0)==(t|0)){o=224;break}u=c[s+8>>2]|0;if((u|0)==0){break}else{s=u}}if((o|0)==224?(c[s+12>>2]&8|0)==0:0){c[s>>2]=r;h=s+4|0;c[h>>2]=(c[h>>2]|0)+p;h=r+8|0;if((h&7|0)==0){h=0}else{h=0-h&7}j=r+(p+8)|0;if((j&7|0)==0){n=0}else{n=0-j&7}o=r+(n+p)|0;j=h+a|0;k=r+j|0;m=o-(r+h)-a|0;c[r+(h+4)>>2]=a|3;h:do{if((o|0)!=(c[9552>>2]|0)){if((o|0)==(c[9548>>2]|0)){H=(c[9536>>2]|0)+m|0;c[9536>>2]=H;c[9548>>2]=k;c[r+(j+4)>>2]=H|1;c[r+(H+j)>>2]=H;break}q=p+4|0;t=c[r+(q+n)>>2]|0;if((t&3|0)==1){a=t&-8;s=t>>>3;do{if(!(t>>>0<256)){l=c[r+((n|24)+p)>>2]|0;v=c[r+(p+12+n)>>2]|0;do{if((v|0)==(o|0)){u=n|16;t=r+(q+u)|0;s=c[t>>2]|0;if((s|0)==0){t=r+(u+p)|0;s=c[t>>2]|0;if((s|0)==0){g=0;break}}while(1){v=s+20|0;u=c[v>>2]|0;if((u|0)!=0){s=u;t=v;continue}v=s+16|0;u=c[v>>2]|0;if((u|0)==0){break}else{s=u;t=v}}if(t>>>0<(c[9544>>2]|0)>>>0){Na()}else{c[t>>2]=0;g=s;break}}else{s=c[r+((n|8)+p)>>2]|0;if(s>>>0<(c[9544>>2]|0)>>>0){Na()}t=s+12|0;if((c[t>>2]|0)!=(o|0)){Na()}u=v+8|0;if((c[u>>2]|0)==(o|0)){c[t>>2]=v;c[u>>2]=s;g=v;break}else{Na()}}}while(0);if((l|0)!=0){s=c[r+(p+28+n)>>2]|0;t=9832+(s<<2)|0;if((o|0)==(c[t>>2]|0)){c[t>>2]=g;if((g|0)==0){c[9532>>2]=c[9532>>2]&~(1<<s);break}}else{if(l>>>0<(c[9544>>2]|0)>>>0){Na()}s=l+16|0;if((c[s>>2]|0)==(o|0)){c[s>>2]=g}else{c[l+20>>2]=g}if((g|0)==0){break}}if(g>>>0<(c[9544>>2]|0)>>>0){Na()}c[g+24>>2]=l;l=n|16;o=c[r+(l+p)>>2]|0;do{if((o|0)!=0){if(o>>>0<(c[9544>>2]|0)>>>0){Na()}else{c[g+16>>2]=o;c[o+24>>2]=g;break}}}while(0);l=c[r+(q+l)>>2]|0;if((l|0)!=0){if(l>>>0<(c[9544>>2]|0)>>>0){Na()}else{c[g+20>>2]=l;c[l+24>>2]=g;break}}}}else{q=c[r+((n|8)+p)>>2]|0;g=c[r+(p+12+n)>>2]|0;t=9568+(s<<1<<2)|0;if((q|0)!=(t|0)){if(q>>>0<(c[9544>>2]|0)>>>0){Na()}if((c[q+12>>2]|0)!=(o|0)){Na()}}if((g|0)==(q|0)){c[2382]=c[2382]&~(1<<s);break}if((g|0)!=(t|0)){if(g>>>0<(c[9544>>2]|0)>>>0){Na()}s=g+8|0;if((c[s>>2]|0)==(o|0)){l=s}else{Na()}}else{l=g+8|0}c[q+12>>2]=g;c[l>>2]=q}}while(0);o=r+((a|n)+p)|0;m=a+m|0}g=o+4|0;c[g>>2]=c[g>>2]&-2;c[r+(j+4)>>2]=m|1;c[r+(m+j)>>2]=m;g=m>>>3;if(m>>>0<256){l=g<<1;d=9568+(l<<2)|0;m=c[2382]|0;g=1<<g;if((m&g|0)!=0){l=9568+(l+2<<2)|0;g=c[l>>2]|0;if(g>>>0<(c[9544>>2]|0)>>>0){Na()}else{f=l;e=g}}else{c[2382]=m|g;f=9568+(l+2<<2)|0;e=d}c[f>>2]=k;c[e+12>>2]=k;c[r+(j+8)>>2]=e;c[r+(j+12)>>2]=d;break}e=m>>>8;if((e|0)!=0){if(m>>>0>16777215){e=31}else{G=(e+1048320|0)>>>16&8;H=e<<G;F=(H+520192|0)>>>16&4;H=H<<F;e=(H+245760|0)>>>16&2;e=14-(F|G|e)+(H<<e>>>15)|0;e=m>>>(e+7|0)&1|e<<1}}else{e=0}f=9832+(e<<2)|0;c[r+(j+28)>>2]=e;c[r+(j+20)>>2]=0;c[r+(j+16)>>2]=0;l=c[9532>>2]|0;g=1<<e;if((l&g|0)==0){c[9532>>2]=l|g;c[f>>2]=k;c[r+(j+24)>>2]=f;c[r+(j+12)>>2]=k;c[r+(j+8)>>2]=k;break}f=c[f>>2]|0;if((e|0)==31){e=0}else{e=25-(e>>>1)|0}i:do{if((c[f+4>>2]&-8|0)!=(m|0)){e=m<<e;while(1){g=f+(e>>>31<<2)+16|0;l=c[g>>2]|0;if((l|0)==0){break}if((c[l+4>>2]&-8|0)==(m|0)){d=l;break i}else{e=e<<1;f=l}}if(g>>>0<(c[9544>>2]|0)>>>0){Na()}else{c[g>>2]=k;c[r+(j+24)>>2]=f;c[r+(j+12)>>2]=k;c[r+(j+8)>>2]=k;break h}}else{d=f}}while(0);f=d+8|0;e=c[f>>2]|0;g=c[9544>>2]|0;if(d>>>0<g>>>0){Na()}if(e>>>0<g>>>0){Na()}else{c[e+12>>2]=k;c[f>>2]=k;c[r+(j+8)>>2]=e;c[r+(j+12)>>2]=d;c[r+(j+24)>>2]=0;break}}else{H=(c[9540>>2]|0)+m|0;c[9540>>2]=H;c[9552>>2]=k;c[r+(j+4)>>2]=H|1}}while(0);H=r+(h|8)|0;i=b;return H|0}e=9976|0;while(1){d=c[e>>2]|0;if(!(d>>>0>q>>>0)?(n=c[e+4>>2]|0,m=d+n|0,m>>>0>q>>>0):0){break}e=c[e+8>>2]|0}e=d+(n+ -39)|0;if((e&7|0)==0){e=0}else{e=0-e&7}d=d+(n+ -47+e)|0;d=d>>>0<(q+16|0)>>>0?q:d;e=d+8|0;f=r+8|0;if((f&7|0)==0){f=0}else{f=0-f&7}H=p+ -40-f|0;c[9552>>2]=r+f;c[9540>>2]=H;c[r+(f+4)>>2]=H|1;c[r+(p+ -36)>>2]=40;c[9556>>2]=c[10016>>2];c[d+4>>2]=27;c[e+0>>2]=c[9976>>2];c[e+4>>2]=c[9980>>2];c[e+8>>2]=c[9984>>2];c[e+12>>2]=c[9988>>2];c[9976>>2]=r;c[9980>>2]=p;c[9988>>2]=0;c[9984>>2]=e;e=d+28|0;c[e>>2]=7;if((d+32|0)>>>0<m>>>0){do{H=e;e=e+4|0;c[e>>2]=7}while((H+8|0)>>>0<m>>>0)}if((d|0)!=(q|0)){d=d-q|0;e=q+(d+4)|0;c[e>>2]=c[e>>2]&-2;c[q+4>>2]=d|1;c[q+d>>2]=d;e=d>>>3;if(d>>>0<256){f=e<<1;d=9568+(f<<2)|0;g=c[2382]|0;e=1<<e;if((g&e|0)!=0){f=9568+(f+2<<2)|0;e=c[f>>2]|0;if(e>>>0<(c[9544>>2]|0)>>>0){Na()}else{k=f;j=e}}else{c[2382]=g|e;k=9568+(f+2<<2)|0;j=d}c[k>>2]=q;c[j+12>>2]=q;c[q+8>>2]=j;c[q+12>>2]=d;break}e=d>>>8;if((e|0)!=0){if(d>>>0>16777215){e=31}else{G=(e+1048320|0)>>>16&8;H=e<<G;F=(H+520192|0)>>>16&4;H=H<<F;e=(H+245760|0)>>>16&2;e=14-(F|G|e)+(H<<e>>>15)|0;e=d>>>(e+7|0)&1|e<<1}}else{e=0}j=9832+(e<<2)|0;c[q+28>>2]=e;c[q+20>>2]=0;c[q+16>>2]=0;f=c[9532>>2]|0;g=1<<e;if((f&g|0)==0){c[9532>>2]=f|g;c[j>>2]=q;c[q+24>>2]=j;c[q+12>>2]=q;c[q+8>>2]=q;break}f=c[j>>2]|0;if((e|0)==31){e=0}else{e=25-(e>>>1)|0}j:do{if((c[f+4>>2]&-8|0)!=(d|0)){e=d<<e;while(1){j=f+(e>>>31<<2)+16|0;g=c[j>>2]|0;if((g|0)==0){break}if((c[g+4>>2]&-8|0)==(d|0)){h=g;break j}else{e=e<<1;f=g}}if(j>>>0<(c[9544>>2]|0)>>>0){Na()}else{c[j>>2]=q;c[q+24>>2]=f;c[q+12>>2]=q;c[q+8>>2]=q;break g}}else{h=f}}while(0);f=h+8|0;e=c[f>>2]|0;d=c[9544>>2]|0;if(h>>>0<d>>>0){Na()}if(e>>>0<d>>>0){Na()}else{c[e+12>>2]=q;c[f>>2]=q;c[q+8>>2]=e;c[q+12>>2]=h;c[q+24>>2]=0;break}}}else{H=c[9544>>2]|0;if((H|0)==0|r>>>0<H>>>0){c[9544>>2]=r}c[9976>>2]=r;c[9980>>2]=p;c[9988>>2]=0;c[9564>>2]=c[2500];c[9560>>2]=-1;d=0;do{H=d<<1;G=9568+(H<<2)|0;c[9568+(H+3<<2)>>2]=G;c[9568+(H+2<<2)>>2]=G;d=d+1|0}while((d|0)!=32);d=r+8|0;if((d&7|0)==0){d=0}else{d=0-d&7}H=p+ -40-d|0;c[9552>>2]=r+d;c[9540>>2]=H;c[r+(d+4)>>2]=H|1;c[r+(p+ -36)>>2]=40;c[9556>>2]=c[10016>>2]}}while(0);d=c[9540>>2]|0;if(d>>>0>a>>>0){G=d-a|0;c[9540>>2]=G;H=c[9552>>2]|0;c[9552>>2]=H+a;c[H+(a+4)>>2]=G|1;c[H+4>>2]=a|3;H=H+8|0;i=b;return H|0}}c[(Ia()|0)>>2]=12;H=0;i=b;return H|0}function le(a){a=a|0;var b=0,d=0,e=0,f=0,g=0,h=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0,q=0,r=0,s=0,t=0,u=0,v=0,w=0;b=i;if((a|0)==0){i=b;return}q=a+ -8|0;r=c[9544>>2]|0;if(q>>>0<r>>>0){Na()}o=c[a+ -4>>2]|0;n=o&3;if((n|0)==1){Na()}j=o&-8;h=a+(j+ -8)|0;do{if((o&1|0)==0){u=c[q>>2]|0;if((n|0)==0){i=b;return}q=-8-u|0;o=a+q|0;n=u+j|0;if(o>>>0<r>>>0){Na()}if((o|0)==(c[9548>>2]|0)){d=a+(j+ -4)|0;if((c[d>>2]&3|0)!=3){d=o;m=n;break}c[9536>>2]=n;c[d>>2]=c[d>>2]&-2;c[a+(q+4)>>2]=n|1;c[h>>2]=n;i=b;return}t=u>>>3;if(u>>>0<256){d=c[a+(q+8)>>2]|0;m=c[a+(q+12)>>2]|0;p=9568+(t<<1<<2)|0;if((d|0)!=(p|0)){if(d>>>0<r>>>0){Na()}if((c[d+12>>2]|0)!=(o|0)){Na()}}if((m|0)==(d|0)){c[2382]=c[2382]&~(1<<t);d=o;m=n;break}if((m|0)!=(p|0)){if(m>>>0<r>>>0){Na()}p=m+8|0;if((c[p>>2]|0)==(o|0)){s=p}else{Na()}}else{s=m+8|0}c[d+12>>2]=m;c[s>>2]=d;d=o;m=n;break}s=c[a+(q+24)>>2]|0;t=c[a+(q+12)>>2]|0;do{if((t|0)==(o|0)){u=a+(q+20)|0;t=c[u>>2]|0;if((t|0)==0){u=a+(q+16)|0;t=c[u>>2]|0;if((t|0)==0){p=0;break}}while(1){w=t+20|0;v=c[w>>2]|0;if((v|0)!=0){t=v;u=w;continue}v=t+16|0;w=c[v>>2]|0;if((w|0)==0){break}else{t=w;u=v}}if(u>>>0<r>>>0){Na()}else{c[u>>2]=0;p=t;break}}else{u=c[a+(q+8)>>2]|0;if(u>>>0<r>>>0){Na()}r=u+12|0;if((c[r>>2]|0)!=(o|0)){Na()}v=t+8|0;if((c[v>>2]|0)==(o|0)){c[r>>2]=t;c[v>>2]=u;p=t;break}else{Na()}}}while(0);if((s|0)!=0){t=c[a+(q+28)>>2]|0;r=9832+(t<<2)|0;if((o|0)==(c[r>>2]|0)){c[r>>2]=p;if((p|0)==0){c[9532>>2]=c[9532>>2]&~(1<<t);d=o;m=n;break}}else{if(s>>>0<(c[9544>>2]|0)>>>0){Na()}r=s+16|0;if((c[r>>2]|0)==(o|0)){c[r>>2]=p}else{c[s+20>>2]=p}if((p|0)==0){d=o;m=n;break}}if(p>>>0<(c[9544>>2]|0)>>>0){Na()}c[p+24>>2]=s;r=c[a+(q+16)>>2]|0;do{if((r|0)!=0){if(r>>>0<(c[9544>>2]|0)>>>0){Na()}else{c[p+16>>2]=r;c[r+24>>2]=p;break}}}while(0);q=c[a+(q+20)>>2]|0;if((q|0)!=0){if(q>>>0<(c[9544>>2]|0)>>>0){Na()}else{c[p+20>>2]=q;c[q+24>>2]=p;d=o;m=n;break}}else{d=o;m=n}}else{d=o;m=n}}else{d=q;m=j}}while(0);if(!(d>>>0<h>>>0)){Na()}n=a+(j+ -4)|0;o=c[n>>2]|0;if((o&1|0)==0){Na()}if((o&2|0)==0){if((h|0)==(c[9552>>2]|0)){w=(c[9540>>2]|0)+m|0;c[9540>>2]=w;c[9552>>2]=d;c[d+4>>2]=w|1;if((d|0)!=(c[9548>>2]|0)){i=b;return}c[9548>>2]=0;c[9536>>2]=0;i=b;return}if((h|0)==(c[9548>>2]|0)){w=(c[9536>>2]|0)+m|0;c[9536>>2]=w;c[9548>>2]=d;c[d+4>>2]=w|1;c[d+w>>2]=w;i=b;return}m=(o&-8)+m|0;n=o>>>3;do{if(!(o>>>0<256)){l=c[a+(j+16)>>2]|0;q=c[a+(j|4)>>2]|0;do{if((q|0)==(h|0)){o=a+(j+12)|0;n=c[o>>2]|0;if((n|0)==0){o=a+(j+8)|0;n=c[o>>2]|0;if((n|0)==0){k=0;break}}while(1){p=n+20|0;q=c[p>>2]|0;if((q|0)!=0){n=q;o=p;continue}p=n+16|0;q=c[p>>2]|0;if((q|0)==0){break}else{n=q;o=p}}if(o>>>0<(c[9544>>2]|0)>>>0){Na()}else{c[o>>2]=0;k=n;break}}else{o=c[a+j>>2]|0;if(o>>>0<(c[9544>>2]|0)>>>0){Na()}p=o+12|0;if((c[p>>2]|0)!=(h|0)){Na()}n=q+8|0;if((c[n>>2]|0)==(h|0)){c[p>>2]=q;c[n>>2]=o;k=q;break}else{Na()}}}while(0);if((l|0)!=0){n=c[a+(j+20)>>2]|0;o=9832+(n<<2)|0;if((h|0)==(c[o>>2]|0)){c[o>>2]=k;if((k|0)==0){c[9532>>2]=c[9532>>2]&~(1<<n);break}}else{if(l>>>0<(c[9544>>2]|0)>>>0){Na()}n=l+16|0;if((c[n>>2]|0)==(h|0)){c[n>>2]=k}else{c[l+20>>2]=k}if((k|0)==0){break}}if(k>>>0<(c[9544>>2]|0)>>>0){Na()}c[k+24>>2]=l;h=c[a+(j+8)>>2]|0;do{if((h|0)!=0){if(h>>>0<(c[9544>>2]|0)>>>0){Na()}else{c[k+16>>2]=h;c[h+24>>2]=k;break}}}while(0);h=c[a+(j+12)>>2]|0;if((h|0)!=0){if(h>>>0<(c[9544>>2]|0)>>>0){Na()}else{c[k+20>>2]=h;c[h+24>>2]=k;break}}}}else{k=c[a+j>>2]|0;a=c[a+(j|4)>>2]|0;j=9568+(n<<1<<2)|0;if((k|0)!=(j|0)){if(k>>>0<(c[9544>>2]|0)>>>0){Na()}if((c[k+12>>2]|0)!=(h|0)){Na()}}if((a|0)==(k|0)){c[2382]=c[2382]&~(1<<n);break}if((a|0)!=(j|0)){if(a>>>0<(c[9544>>2]|0)>>>0){Na()}j=a+8|0;if((c[j>>2]|0)==(h|0)){l=j}else{Na()}}else{l=a+8|0}c[k+12>>2]=a;c[l>>2]=k}}while(0);c[d+4>>2]=m|1;c[d+m>>2]=m;if((d|0)==(c[9548>>2]|0)){c[9536>>2]=m;i=b;return}}else{c[n>>2]=o&-2;c[d+4>>2]=m|1;c[d+m>>2]=m}h=m>>>3;if(m>>>0<256){a=h<<1;e=9568+(a<<2)|0;j=c[2382]|0;h=1<<h;if((j&h|0)!=0){h=9568+(a+2<<2)|0;a=c[h>>2]|0;if(a>>>0<(c[9544>>2]|0)>>>0){Na()}else{f=h;g=a}}else{c[2382]=j|h;f=9568+(a+2<<2)|0;g=e}c[f>>2]=d;c[g+12>>2]=d;c[d+8>>2]=g;c[d+12>>2]=e;i=b;return}f=m>>>8;if((f|0)!=0){if(m>>>0>16777215){f=31}else{v=(f+1048320|0)>>>16&8;w=f<<v;u=(w+520192|0)>>>16&4;w=w<<u;f=(w+245760|0)>>>16&2;f=14-(u|v|f)+(w<<f>>>15)|0;f=m>>>(f+7|0)&1|f<<1}}else{f=0}g=9832+(f<<2)|0;c[d+28>>2]=f;c[d+20>>2]=0;c[d+16>>2]=0;a=c[9532>>2]|0;h=1<<f;a:do{if((a&h|0)!=0){g=c[g>>2]|0;if((f|0)==31){f=0}else{f=25-(f>>>1)|0}b:do{if((c[g+4>>2]&-8|0)!=(m|0)){f=m<<f;a=g;while(1){h=a+(f>>>31<<2)+16|0;g=c[h>>2]|0;if((g|0)==0){break}if((c[g+4>>2]&-8|0)==(m|0)){e=g;break b}else{f=f<<1;a=g}}if(h>>>0<(c[9544>>2]|0)>>>0){Na()}else{c[h>>2]=d;c[d+24>>2]=a;c[d+12>>2]=d;c[d+8>>2]=d;break a}}else{e=g}}while(0);g=e+8|0;f=c[g>>2]|0;h=c[9544>>2]|0;if(e>>>0<h>>>0){Na()}if(f>>>0<h>>>0){Na()}else{c[f+12>>2]=d;c[g>>2]=d;c[d+8>>2]=f;c[d+12>>2]=e;c[d+24>>2]=0;break}}else{c[9532>>2]=a|h;c[g>>2]=d;c[d+24>>2]=g;c[d+12>>2]=d;c[d+8>>2]=d}}while(0);w=(c[9560>>2]|0)+ -1|0;c[9560>>2]=w;if((w|0)==0){d=9984|0}else{i=b;return}while(1){d=c[d>>2]|0;if((d|0)==0){break}else{d=d+8|0}}c[9560>>2]=-1;i=b;return}function me(a,b){a=a|0;b=b|0;var d=0,e=0;d=i;if((a|0)!=0){e=aa(b,a)|0;if((b|a)>>>0>65535){e=((e>>>0)/(a>>>0)|0|0)==(b|0)?e:-1}}else{e=0}a=ke(e)|0;if((a|0)==0){i=d;return a|0}if((c[a+ -4>>2]&3|0)==0){i=d;return a|0}re(a|0,0,e|0)|0;i=d;return a|0}function ne(b,c,d){b=b|0;c=c|0;d=d|0;var e=0,f=0,g=0;e=i;a:do{if((d|0)==0){b=0}else{while(1){g=a[b>>0]|0;f=a[c>>0]|0;if(!(g<<24>>24==f<<24>>24)){break}d=d+ -1|0;if((d|0)==0){b=0;break a}else{b=b+1|0;c=c+1|0}}b=(g&255)-(f&255)|0}}while(0);i=e;return b|0}function oe(){}function pe(a){a=a|0;var b=0;b=(aa(c[a>>2]|0,31010991)|0)+1735287159&2147483647;c[a>>2]=b;return b|0}function qe(){return pe(o)|0}function re(b,d,e){b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0,i=0;f=b+e|0;if((e|0)>=20){d=d&255;i=b&3;h=d|d<<8|d<<16|d<<24;g=f&~3;if(i){i=b+4-i|0;while((b|0)<(i|0)){a[b>>0]=d;b=b+1|0}}while((b|0)<(g|0)){c[b>>2]=h;b=b+4|0}}while((b|0)<(f|0)){a[b>>0]=d;b=b+1|0}return b-e|0}function se(b){b=b|0;var c=0;c=b;while(a[c>>0]|0){c=c+1|0}return c-b|0}function te(b,d,e){b=b|0;d=d|0;e=e|0;var f=0;if((e|0)>=4096)return wa(b|0,d|0,e|0)|0;f=b|0;if((b&3)==(d&3)){while(b&3){if((e|0)==0)return f|0;a[b>>0]=a[d>>0]|0;b=b+1|0;d=d+1|0;e=e-1|0}while((e|0)>=4){c[b>>2]=c[d>>2];b=b+4|0;d=d+4|0;e=e-4|0}}while((e|0)>0){a[b>>0]=a[d>>0]|0;b=b+1|0;d=d+1|0;e=e-1|0}return f|0}function ue(b,c){b=b|0;c=c|0;var d=0;do{a[b+d>>0]=a[c+d>>0];d=d+1|0}while(a[c+(d-1)>>0]|0);return b|0}function ve(a){a=a|0;return(a&255)<<24|(a>>8&255)<<16|(a>>16&255)<<8|a>>>24|0}function we(a,b,c,d){a=a|0;b=b|0;c=c|0;d=d|0;c=a+c>>>0;return(E=b+d+(c>>>0<a>>>0|0)>>>0,c|0)|0}function xe(a,b,c,d){a=a|0;b=b|0;c=c|0;d=d|0;b=b-d-(c>>>0>a>>>0|0)>>>0;return(E=b,a-c>>>0|0)|0}function ye(a,b,c){a=a|0;b=b|0;c=c|0;if((c|0)<32){E=b<<c|(a&(1<<c)-1<<32-c)>>>32-c;return a<<c}E=a<<c-32;return 0}function ze(a,b,c){a=a|0;b=b|0;c=c|0;if((c|0)<32){E=b>>>c;return a>>>c|(b&(1<<c)-1)<<32-c}E=0;return b>>>c-32|0}function Ae(a,b,c){a=a|0;b=b|0;c=c|0;if((c|0)<32){E=b>>c;return a>>>c|(b&(1<<c)-1)<<32-c}E=(b|0)<0?-1:0;return b>>c-32|0}function Be(b){b=b|0;var c=0;c=a[n+(b>>>24)>>0]|0;if((c|0)<8)return c|0;c=a[n+(b>>16&255)>>0]|0;if((c|0)<8)return c+8|0;c=a[n+(b>>8&255)>>0]|0;if((c|0)<8)return c+16|0;return(a[n+(b&255)>>0]|0)+24|0}function Ce(b){b=b|0;var c=0;c=a[m+(b&255)>>0]|0;if((c|0)<8)return c|0;c=a[m+(b>>8&255)>>0]|0;if((c|0)<8)return c+8|0;c=a[m+(b>>16&255)>>0]|0;if((c|0)<8)return c+16|0;return(a[m+(b>>>24)>>0]|0)+24|0}function De(a,b){a=a|0;b=b|0;var c=0,d=0,e=0,f=0;f=a&65535;d=b&65535;c=aa(d,f)|0;e=a>>>16;d=(c>>>16)+(aa(d,e)|0)|0;b=b>>>16;a=aa(b,f)|0;return(E=(d>>>16)+(aa(b,e)|0)+(((d&65535)+a|0)>>>16)|0,d+a<<16|c&65535|0)|0}function Ee(a,b,c,d){a=a|0;b=b|0;c=c|0;d=d|0;var e=0,f=0,g=0,h=0;e=b>>31|((b|0)<0?-1:0)<<1;f=((b|0)<0?-1:0)>>31|((b|0)<0?-1:0)<<1;g=d>>31|((d|0)<0?-1:0)<<1;h=((d|0)<0?-1:0)>>31|((d|0)<0?-1:0)<<1;a=xe(e^a,f^b,e,f)|0;b=E;e=g^e;f=h^f;g=xe((Je(a,b,xe(g^c,h^d,g,h)|0,E,0)|0)^e,E^f,e,f)|0;return g|0}function Fe(a,b,d,e){a=a|0;b=b|0;d=d|0;e=e|0;var f=0,g=0,h=0,j=0,k=0,l=0;g=i;i=i+8|0;f=g|0;h=b>>31|((b|0)<0?-1:0)<<1;j=((b|0)<0?-1:0)>>31|((b|0)<0?-1:0)<<1;k=e>>31|((e|0)<0?-1:0)<<1;l=((e|0)<0?-1:0)>>31|((e|0)<0?-1:0)<<1;a=xe(h^a,j^b,h,j)|0;b=E;Je(a,b,xe(k^d,l^e,k,l)|0,E,f)|0;k=xe(c[f>>2]^h,c[f+4>>2]^j,h,j)|0;j=E;i=g;return(E=j,k)|0}function Ge(a,b,c,d){a=a|0;b=b|0;c=c|0;d=d|0;var e=0,f=0;e=a;f=c;a=De(e,f)|0;c=E;return(E=(aa(b,f)|0)+(aa(d,e)|0)+c|c&0,a|0|0)|0}function He(a,b,c,d){a=a|0;b=b|0;c=c|0;d=d|0;a=Je(a,b,c,d,0)|0;return a|0}function Ie(a,b,d,e){a=a|0;b=b|0;d=d|0;e=e|0;var f=0,g=0;g=i;i=i+8|0;f=g|0;Je(a,b,d,e,f)|0;i=g;return(E=c[f+4>>2]|0,c[f>>2]|0)|0}function Je(a,b,d,e,f){a=a|0;b=b|0;d=d|0;e=e|0;f=f|0;var g=0,h=0,i=0,j=0,k=0,l=0,m=0,n=0,o=0,p=0;h=a;j=b;i=j;k=d;g=e;l=g;if((i|0)==0){e=(f|0)!=0;if((l|0)==0){if(e){c[f>>2]=(h>>>0)%(k>>>0);c[f+4>>2]=0}l=0;m=(h>>>0)/(k>>>0)>>>0;return(E=l,m)|0}else{if(!e){l=0;m=0;return(E=l,m)|0}c[f>>2]=a|0;c[f+4>>2]=b&0;l=0;m=0;return(E=l,m)|0}}m=(l|0)==0;do{if((k|0)!=0){if(!m){k=(Be(l|0)|0)-(Be(i|0)|0)|0;if(k>>>0<=31){m=k+1|0;l=31-k|0;b=k-31>>31;j=m;a=h>>>(m>>>0)&b|i<<l;b=i>>>(m>>>0)&b;k=0;l=h<<l;break}if((f|0)==0){l=0;m=0;return(E=l,m)|0}c[f>>2]=a|0;c[f+4>>2]=j|b&0;l=0;m=0;return(E=l,m)|0}l=k-1|0;if((l&k|0)!=0){l=(Be(k|0)|0)+33-(Be(i|0)|0)|0;p=64-l|0;m=32-l|0;n=m>>31;o=l-32|0;b=o>>31;j=l;a=m-1>>31&i>>>(o>>>0)|(i<<m|h>>>(l>>>0))&b;b=b&i>>>(l>>>0);k=h<<p&n;l=(i<<p|h>>>(o>>>0))&n|h<<m&l-33>>31;break}if((f|0)!=0){c[f>>2]=l&h;c[f+4>>2]=0}if((k|0)==1){o=j|b&0;p=a|0|0;return(E=o,p)|0}else{p=Ce(k|0)|0;o=i>>>(p>>>0)|0;p=i<<32-p|h>>>(p>>>0)|0;return(E=o,p)|0}}else{if(m){if((f|0)!=0){c[f>>2]=(i>>>0)%(k>>>0);c[f+4>>2]=0}o=0;p=(i>>>0)/(k>>>0)>>>0;return(E=o,p)|0}if((h|0)==0){if((f|0)!=0){c[f>>2]=0;c[f+4>>2]=(i>>>0)%(l>>>0)}o=0;p=(i>>>0)/(l>>>0)>>>0;return(E=o,p)|0}k=l-1|0;if((k&l|0)==0){if((f|0)!=0){c[f>>2]=a|0;c[f+4>>2]=k&i|b&0}o=0;p=i>>>((Ce(l|0)|0)>>>0);return(E=o,p)|0}k=(Be(l|0)|0)-(Be(i|0)|0)|0;if(k>>>0<=30){b=k+1|0;l=31-k|0;j=b;a=i<<l|h>>>(b>>>0);b=i>>>(b>>>0);k=0;l=h<<l;break}if((f|0)==0){o=0;p=0;return(E=o,p)|0}c[f>>2]=a|0;c[f+4>>2]=j|b&0;o=0;p=0;return(E=o,p)|0}}while(0);if((j|0)==0){g=0;e=0}else{h=d|0|0;i=g|e&0;d=we(h,i,-1,-1)|0;g=E;e=0;do{m=l;l=k>>>31|l<<1;k=e|k<<1;m=a<<1|m>>>31|0;a=a>>>31|b<<1|0;xe(d,g,m,a)|0;b=E;p=b>>31|((b|0)<0?-1:0)<<1;e=p&1;a=xe(m,a,p&h,(((b|0)<0?-1:0)>>31|((b|0)<0?-1:0)<<1)&i)|0;b=E;j=j-1|0}while((j|0)!=0);g=0}h=0;if((f|0)!=0){c[f>>2]=a;c[f+4>>2]=b}o=(k|0)>>>31|(l|h)<<1|(h<<1|k>>>31)&0|g;p=(k<<1|0>>>31)&-2|e;return(E=o,p)|0}function Ke(a,b){a=a|0;b=b|0;return Wa[a&31](b|0)|0}function Le(a){a=a|0;return ga(0,a|0)|0}function Me(a){a=a|0;return ga(1,a|0)|0}function Ne(a){a=a|0;return ga(2,a|0)|0}function Oe(a){a=a|0;return ga(3,a|0)|0}function Pe(a){a=a|0;return ga(4,a|0)|0}function Qe(a){a=a|0;return ga(5,a|0)|0}function Re(a){a=a|0;return ga(6,a|0)|0}function Se(a){a=a|0;return ga(7,a|0)|0}function Te(a,b,c,d){a=a|0;b=b|0;c=c|0;d=d|0;return Xa[a&31](b|0,c|0,d|0)|0}function Ue(a,b,c){a=a|0;b=b|0;c=c|0;return ga(0,a|0,b|0,c|0)|0}function Ve(a,b,c){a=a|0;b=b|0;c=c|0;return ga(1,a|0,b|0,c|0)|0}function We(a,b,c){a=a|0;b=b|0;c=c|0;return ga(2,a|0,b|0,c|0)|0}function Xe(a,b,c){a=a|0;b=b|0;c=c|0;return ga(3,a|0,b|0,c|0)|0}function Ye(a,b,c){a=a|0;b=b|0;c=c|0;return ga(4,a|0,b|0,c|0)|0}function Ze(a,b,c){a=a|0;b=b|0;c=c|0;return ga(5,a|0,b|0,c|0)|0}function _e(a,b,c){a=a|0;b=b|0;c=c|0;return ga(6,a|0,b|0,c|0)|0}function $e(a,b,c){a=a|0;b=b|0;c=c|0;return ga(7,a|0,b|0,c|0)|0}function af(a,b,c){a=a|0;b=b|0;c=c|0;return Ya[a&31](b|0,c|0)|0}function bf(a,b){a=a|0;b=b|0;return ga(0,a|0,b|0)|0}function cf(a,b){a=a|0;b=b|0;return ga(1,a|0,b|0)|0}function df(a,b){a=a|0;b=b|0;return ga(2,a|0,b|0)|0}function ef(a,b){a=a|0;b=b|0;return ga(3,a|0,b|0)|0}function ff(a,b){a=a|0;b=b|0;return ga(4,a|0,b|0)|0}function gf(a,b){a=a|0;b=b|0;return ga(5,a|0,b|0)|0}function hf(a,b){a=a|0;b=b|0;return ga(6,a|0,b|0)|0}function jf(a,b){a=a|0;b=b|0;return ga(7,a|0,b|0)|0}function kf(a,b,c,d){a=a|0;b=b|0;c=c|0;d=d|0;Za[a&31](b|0,c|0,d|0)}function lf(a,b,c){a=a|0;b=b|0;c=c|0;ga(0,a|0,b|0,c|0)}function mf(a,b,c){a=a|0;b=b|0;c=c|0;ga(1,a|0,b|0,c|0)}function nf(a,b,c){a=a|0;b=b|0;c=c|0;ga(2,a|0,b|0,c|0)}function of(a,b,c){a=a|0;b=b|0;c=c|0;ga(3,a|0,b|0,c|0)}function pf(a,b,c){a=a|0;b=b|0;c=c|0;ga(4,a|0,b|0,c|0)}function qf(a,b,c){a=a|0;b=b|0;c=c|0;ga(5,a|0,b|0,c|0)}function rf(a,b,c){a=a|0;b=b|0;c=c|0;ga(6,a|0,b|0,c|0)}function sf(a,b,c){a=a|0;b=b|0;c=c|0;ga(7,a|0,b|0,c|0)}function tf(a){a=a|0;ba(0);return 0}function uf(a,b,c){a=a|0;b=b|0;c=c|0;ba(1);return 0}function vf(a,b){a=a|0;b=b|0;ba(2);return 0}function wf(a,b,c){a=a|0;b=b|0;c=c|0;ba(3)}




// EMSCRIPTEN_END_FUNCS
var Wa=[tf,tf,Le,tf,Me,tf,Ne,tf,Oe,tf,Pe,tf,Qe,tf,Re,tf,Se,tf,Ib,Ob,wb,qc,Qc,Pc,mc,jd,kd,qd,tf,tf,tf,tf];var Xa=[uf,uf,Ue,uf,Ve,uf,We,uf,Xe,uf,Ye,uf,Ze,uf,_e,uf,$e,uf,ld,md,rd,sd,uf,uf,uf,uf,uf,uf,uf,uf,uf,uf];var Ya=[vf,vf,bf,vf,cf,vf,df,vf,ef,vf,ff,vf,gf,vf,hf,vf,jf,vf,_b,Nd,nd,od,td,ud,vf,vf,vf,vf,vf,vf,vf,vf];var Za=[wf,wf,lf,wf,mf,wf,nf,wf,of,wf,pf,wf,qf,wf,rf,wf,sf,wf,bd,Qd,wf,wf,wf,wf,wf,wf,wf,wf,wf,wf,wf,wf];return{_strlen:se,_free:le,_ImageStrDecDecode:Hc,_rand_r:pe,_ImageStrDecTerm:Ic,_memset:re,_ImageStrDecInit:Gc,_malloc:ke,_ImageStrEncEncode:Eb,_WMPhotoValidate:Fc,_llvm_bswap_i32:ve,_ImageStrEncInit:Db,_memcpy:te,_ImageStrDecGetInfo:Ec,_strcpy:ue,_calloc:me,_rand:qe,runPostSets:oe,stackAlloc:_a,stackSave:$a,stackRestore:ab,setThrew:bb,setTempRet0:eb,getTempRet0:fb,dynCall_ii:Ke,dynCall_iiii:Te,dynCall_iii:af,dynCall_viii:kf}})


// EMSCRIPTEN_END_ASM
({ "Math": Math, "Int8Array": Int8Array, "Int16Array": Int16Array, "Int32Array": Int32Array, "Uint8Array": Uint8Array, "Uint16Array": Uint16Array, "Uint32Array": Uint32Array, "Float32Array": Float32Array, "Float64Array": Float64Array }, { "abort": abort, "assert": assert, "asmPrintInt": asmPrintInt, "asmPrintFloat": asmPrintFloat, "min": Math_min, "jsCall": jsCall, "invoke_ii": invoke_ii, "invoke_iiii": invoke_iiii, "invoke_iii": invoke_iii, "invoke_viii": invoke_viii, "_send": _send, "_fread": _fread, "_lseek": _lseek, "_open": _open, "___assert_fail": ___assert_fail, "_write": _write, "_fflush": _fflush, "_time": _time, "_pwrite": _pwrite, "__reallyNegative": __reallyNegative, "_sbrk": _sbrk, "_emscripten_memcpy_big": _emscripten_memcpy_big, "_fileno": _fileno, "_sysconf": _sysconf, "___setErrNo": ___setErrNo, "_fseek": _fseek, "_pread": _pread, "_puts": _puts, "_printf": _printf, "_fclose": _fclose, "_feof": _feof, "_fsync": _fsync, "_ftell": _ftell, "___errno_location": ___errno_location, "_recv": _recv, "_fputc": _fputc, "_mkport": _mkport, "_read": _read, "_abort": _abort, "_fwrite": _fwrite, "_tmpnam": _tmpnam, "_fprintf": _fprintf, "__formatString": __formatString, "_fputs": _fputs, "_fopen": _fopen, "_close": _close, "STACKTOP": STACKTOP, "STACK_MAX": STACK_MAX, "tempDoublePtr": tempDoublePtr, "ABORT": ABORT, "cttz_i8": cttz_i8, "ctlz_i8": ctlz_i8, "___rand_seed": ___rand_seed, "NaN": NaN, "Infinity": Infinity }, buffer);
var _strlen = Module["_strlen"] = asm["_strlen"];
var _free = Module["_free"] = asm["_free"];
var _ImageStrDecDecode = Module["_ImageStrDecDecode"] = asm["_ImageStrDecDecode"];
var _rand_r = Module["_rand_r"] = asm["_rand_r"];
var _ImageStrDecTerm = Module["_ImageStrDecTerm"] = asm["_ImageStrDecTerm"];
var _memset = Module["_memset"] = asm["_memset"];
var _ImageStrDecInit = Module["_ImageStrDecInit"] = asm["_ImageStrDecInit"];
var _malloc = Module["_malloc"] = asm["_malloc"];
var _ImageStrEncEncode = Module["_ImageStrEncEncode"] = asm["_ImageStrEncEncode"];
var _WMPhotoValidate = Module["_WMPhotoValidate"] = asm["_WMPhotoValidate"];
var _llvm_bswap_i32 = Module["_llvm_bswap_i32"] = asm["_llvm_bswap_i32"];
var _ImageStrEncInit = Module["_ImageStrEncInit"] = asm["_ImageStrEncInit"];
var _memcpy = Module["_memcpy"] = asm["_memcpy"];
var _ImageStrDecGetInfo = Module["_ImageStrDecGetInfo"] = asm["_ImageStrDecGetInfo"];
var _strcpy = Module["_strcpy"] = asm["_strcpy"];
var _calloc = Module["_calloc"] = asm["_calloc"];
var _rand = Module["_rand"] = asm["_rand"];
var runPostSets = Module["runPostSets"] = asm["runPostSets"];
var dynCall_ii = Module["dynCall_ii"] = asm["dynCall_ii"];
var dynCall_iiii = Module["dynCall_iiii"] = asm["dynCall_iiii"];
var dynCall_iii = Module["dynCall_iii"] = asm["dynCall_iii"];
var dynCall_viii = Module["dynCall_viii"] = asm["dynCall_viii"];

Runtime.stackAlloc = asm['stackAlloc'];
Runtime.stackSave = asm['stackSave'];
Runtime.stackRestore = asm['stackRestore'];
Runtime.setTempRet0 = asm['setTempRet0'];
Runtime.getTempRet0 = asm['getTempRet0'];


// TODO: strip out parts of this we do not need

//======= begin closure i64 code =======

// Copyright 2009 The Closure Library Authors. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS-IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @fileoverview Defines a Long class for representing a 64-bit two's-complement
 * integer value, which faithfully simulates the behavior of a Java "long". This
 * implementation is derived from LongLib in GWT.
 *
 */

var i64Math = (function() { // Emscripten wrapper
  var goog = { math: {} };


  /**
   * Constructs a 64-bit two's-complement integer, given its low and high 32-bit
   * values as *signed* integers.  See the from* functions below for more
   * convenient ways of constructing Longs.
   *
   * The internal representation of a long is the two given signed, 32-bit values.
   * We use 32-bit pieces because these are the size of integers on which
   * Javascript performs bit-operations.  For operations like addition and
   * multiplication, we split each number into 16-bit pieces, which can easily be
   * multiplied within Javascript's floating-point representation without overflow
   * or change in sign.
   *
   * In the algorithms below, we frequently reduce the negative case to the
   * positive case by negating the input(s) and then post-processing the result.
   * Note that we must ALWAYS check specially whether those values are MIN_VALUE
   * (-2^63) because -MIN_VALUE == MIN_VALUE (since 2^63 cannot be represented as
   * a positive number, it overflows back into a negative).  Not handling this
   * case would often result in infinite recursion.
   *
   * @param {number} low  The low (signed) 32 bits of the long.
   * @param {number} high  The high (signed) 32 bits of the long.
   * @constructor
   */
  goog.math.Long = function(low, high) {
    /**
     * @type {number}
     * @private
     */
    this.low_ = low | 0;  // force into 32 signed bits.

    /**
     * @type {number}
     * @private
     */
    this.high_ = high | 0;  // force into 32 signed bits.
  };


  // NOTE: Common constant values ZERO, ONE, NEG_ONE, etc. are defined below the
  // from* methods on which they depend.


  /**
   * A cache of the Long representations of small integer values.
   * @type {!Object}
   * @private
   */
  goog.math.Long.IntCache_ = {};


  /**
   * Returns a Long representing the given (32-bit) integer value.
   * @param {number} value The 32-bit integer in question.
   * @return {!goog.math.Long} The corresponding Long value.
   */
  goog.math.Long.fromInt = function(value) {
    if (-128 <= value && value < 128) {
      var cachedObj = goog.math.Long.IntCache_[value];
      if (cachedObj) {
        return cachedObj;
      }
    }

    var obj = new goog.math.Long(value | 0, value < 0 ? -1 : 0);
    if (-128 <= value && value < 128) {
      goog.math.Long.IntCache_[value] = obj;
    }
    return obj;
  };


  /**
   * Returns a Long representing the given value, provided that it is a finite
   * number.  Otherwise, zero is returned.
   * @param {number} value The number in question.
   * @return {!goog.math.Long} The corresponding Long value.
   */
  goog.math.Long.fromNumber = function(value) {
    if (isNaN(value) || !isFinite(value)) {
      return goog.math.Long.ZERO;
    } else if (value <= -goog.math.Long.TWO_PWR_63_DBL_) {
      return goog.math.Long.MIN_VALUE;
    } else if (value + 1 >= goog.math.Long.TWO_PWR_63_DBL_) {
      return goog.math.Long.MAX_VALUE;
    } else if (value < 0) {
      return goog.math.Long.fromNumber(-value).negate();
    } else {
      return new goog.math.Long(
          (value % goog.math.Long.TWO_PWR_32_DBL_) | 0,
          (value / goog.math.Long.TWO_PWR_32_DBL_) | 0);
    }
  };


  /**
   * Returns a Long representing the 64-bit integer that comes by concatenating
   * the given high and low bits.  Each is assumed to use 32 bits.
   * @param {number} lowBits The low 32-bits.
   * @param {number} highBits The high 32-bits.
   * @return {!goog.math.Long} The corresponding Long value.
   */
  goog.math.Long.fromBits = function(lowBits, highBits) {
    return new goog.math.Long(lowBits, highBits);
  };


  /**
   * Returns a Long representation of the given string, written using the given
   * radix.
   * @param {string} str The textual representation of the Long.
   * @param {number=} opt_radix The radix in which the text is written.
   * @return {!goog.math.Long} The corresponding Long value.
   */
  goog.math.Long.fromString = function(str, opt_radix) {
    if (str.length == 0) {
      throw Error('number format error: empty string');
    }

    var radix = opt_radix || 10;
    if (radix < 2 || 36 < radix) {
      throw Error('radix out of range: ' + radix);
    }

    if (str.charAt(0) == '-') {
      return goog.math.Long.fromString(str.substring(1), radix).negate();
    } else if (str.indexOf('-') >= 0) {
      throw Error('number format error: interior "-" character: ' + str);
    }

    // Do several (8) digits each time through the loop, so as to
    // minimize the calls to the very expensive emulated div.
    var radixToPower = goog.math.Long.fromNumber(Math.pow(radix, 8));

    var result = goog.math.Long.ZERO;
    for (var i = 0; i < str.length; i += 8) {
      var size = Math.min(8, str.length - i);
      var value = parseInt(str.substring(i, i + size), radix);
      if (size < 8) {
        var power = goog.math.Long.fromNumber(Math.pow(radix, size));
        result = result.multiply(power).add(goog.math.Long.fromNumber(value));
      } else {
        result = result.multiply(radixToPower);
        result = result.add(goog.math.Long.fromNumber(value));
      }
    }
    return result;
  };


  // NOTE: the compiler should inline these constant values below and then remove
  // these variables, so there should be no runtime penalty for these.


  /**
   * Number used repeated below in calculations.  This must appear before the
   * first call to any from* function below.
   * @type {number}
   * @private
   */
  goog.math.Long.TWO_PWR_16_DBL_ = 1 << 16;


  /**
   * @type {number}
   * @private
   */
  goog.math.Long.TWO_PWR_24_DBL_ = 1 << 24;


  /**
   * @type {number}
   * @private
   */
  goog.math.Long.TWO_PWR_32_DBL_ =
      goog.math.Long.TWO_PWR_16_DBL_ * goog.math.Long.TWO_PWR_16_DBL_;


  /**
   * @type {number}
   * @private
   */
  goog.math.Long.TWO_PWR_31_DBL_ =
      goog.math.Long.TWO_PWR_32_DBL_ / 2;


  /**
   * @type {number}
   * @private
   */
  goog.math.Long.TWO_PWR_48_DBL_ =
      goog.math.Long.TWO_PWR_32_DBL_ * goog.math.Long.TWO_PWR_16_DBL_;


  /**
   * @type {number}
   * @private
   */
  goog.math.Long.TWO_PWR_64_DBL_ =
      goog.math.Long.TWO_PWR_32_DBL_ * goog.math.Long.TWO_PWR_32_DBL_;


  /**
   * @type {number}
   * @private
   */
  goog.math.Long.TWO_PWR_63_DBL_ =
      goog.math.Long.TWO_PWR_64_DBL_ / 2;


  /** @type {!goog.math.Long} */
  goog.math.Long.ZERO = goog.math.Long.fromInt(0);


  /** @type {!goog.math.Long} */
  goog.math.Long.ONE = goog.math.Long.fromInt(1);


  /** @type {!goog.math.Long} */
  goog.math.Long.NEG_ONE = goog.math.Long.fromInt(-1);


  /** @type {!goog.math.Long} */
  goog.math.Long.MAX_VALUE =
      goog.math.Long.fromBits(0xFFFFFFFF | 0, 0x7FFFFFFF | 0);


  /** @type {!goog.math.Long} */
  goog.math.Long.MIN_VALUE = goog.math.Long.fromBits(0, 0x80000000 | 0);


  /**
   * @type {!goog.math.Long}
   * @private
   */
  goog.math.Long.TWO_PWR_24_ = goog.math.Long.fromInt(1 << 24);


  /** @return {number} The value, assuming it is a 32-bit integer. */
  goog.math.Long.prototype.toInt = function() {
    return this.low_;
  };


  /** @return {number} The closest floating-point representation to this value. */
  goog.math.Long.prototype.toNumber = function() {
    return this.high_ * goog.math.Long.TWO_PWR_32_DBL_ +
           this.getLowBitsUnsigned();
  };


  /**
   * @param {number=} opt_radix The radix in which the text should be written.
   * @return {string} The textual representation of this value.
   */
  goog.math.Long.prototype.toString = function(opt_radix) {
    var radix = opt_radix || 10;
    if (radix < 2 || 36 < radix) {
      throw Error('radix out of range: ' + radix);
    }

    if (this.isZero()) {
      return '0';
    }

    if (this.isNegative()) {
      if (this.equals(goog.math.Long.MIN_VALUE)) {
        // We need to change the Long value before it can be negated, so we remove
        // the bottom-most digit in this base and then recurse to do the rest.
        var radixLong = goog.math.Long.fromNumber(radix);
        var div = this.div(radixLong);
        var rem = div.multiply(radixLong).subtract(this);
        return div.toString(radix) + rem.toInt().toString(radix);
      } else {
        return '-' + this.negate().toString(radix);
      }
    }

    // Do several (6) digits each time through the loop, so as to
    // minimize the calls to the very expensive emulated div.
    var radixToPower = goog.math.Long.fromNumber(Math.pow(radix, 6));

    var rem = this;
    var result = '';
    while (true) {
      var remDiv = rem.div(radixToPower);
      var intval = rem.subtract(remDiv.multiply(radixToPower)).toInt();
      var digits = intval.toString(radix);

      rem = remDiv;
      if (rem.isZero()) {
        return digits + result;
      } else {
        while (digits.length < 6) {
          digits = '0' + digits;
        }
        result = '' + digits + result;
      }
    }
  };


  /** @return {number} The high 32-bits as a signed value. */
  goog.math.Long.prototype.getHighBits = function() {
    return this.high_;
  };


  /** @return {number} The low 32-bits as a signed value. */
  goog.math.Long.prototype.getLowBits = function() {
    return this.low_;
  };


  /** @return {number} The low 32-bits as an unsigned value. */
  goog.math.Long.prototype.getLowBitsUnsigned = function() {
    return (this.low_ >= 0) ?
        this.low_ : goog.math.Long.TWO_PWR_32_DBL_ + this.low_;
  };


  /**
   * @return {number} Returns the number of bits needed to represent the absolute
   *     value of this Long.
   */
  goog.math.Long.prototype.getNumBitsAbs = function() {
    if (this.isNegative()) {
      if (this.equals(goog.math.Long.MIN_VALUE)) {
        return 64;
      } else {
        return this.negate().getNumBitsAbs();
      }
    } else {
      var val = this.high_ != 0 ? this.high_ : this.low_;
      for (var bit = 31; bit > 0; bit--) {
        if ((val & (1 << bit)) != 0) {
          break;
        }
      }
      return this.high_ != 0 ? bit + 33 : bit + 1;
    }
  };


  /** @return {boolean} Whether this value is zero. */
  goog.math.Long.prototype.isZero = function() {
    return this.high_ == 0 && this.low_ == 0;
  };


  /** @return {boolean} Whether this value is negative. */
  goog.math.Long.prototype.isNegative = function() {
    return this.high_ < 0;
  };


  /** @return {boolean} Whether this value is odd. */
  goog.math.Long.prototype.isOdd = function() {
    return (this.low_ & 1) == 1;
  };


  /**
   * @param {goog.math.Long} other Long to compare against.
   * @return {boolean} Whether this Long equals the other.
   */
  goog.math.Long.prototype.equals = function(other) {
    return (this.high_ == other.high_) && (this.low_ == other.low_);
  };


  /**
   * @param {goog.math.Long} other Long to compare against.
   * @return {boolean} Whether this Long does not equal the other.
   */
  goog.math.Long.prototype.notEquals = function(other) {
    return (this.high_ != other.high_) || (this.low_ != other.low_);
  };


  /**
   * @param {goog.math.Long} other Long to compare against.
   * @return {boolean} Whether this Long is less than the other.
   */
  goog.math.Long.prototype.lessThan = function(other) {
    return this.compare(other) < 0;
  };


  /**
   * @param {goog.math.Long} other Long to compare against.
   * @return {boolean} Whether this Long is less than or equal to the other.
   */
  goog.math.Long.prototype.lessThanOrEqual = function(other) {
    return this.compare(other) <= 0;
  };


  /**
   * @param {goog.math.Long} other Long to compare against.
   * @return {boolean} Whether this Long is greater than the other.
   */
  goog.math.Long.prototype.greaterThan = function(other) {
    return this.compare(other) > 0;
  };


  /**
   * @param {goog.math.Long} other Long to compare against.
   * @return {boolean} Whether this Long is greater than or equal to the other.
   */
  goog.math.Long.prototype.greaterThanOrEqual = function(other) {
    return this.compare(other) >= 0;
  };


  /**
   * Compares this Long with the given one.
   * @param {goog.math.Long} other Long to compare against.
   * @return {number} 0 if they are the same, 1 if the this is greater, and -1
   *     if the given one is greater.
   */
  goog.math.Long.prototype.compare = function(other) {
    if (this.equals(other)) {
      return 0;
    }

    var thisNeg = this.isNegative();
    var otherNeg = other.isNegative();
    if (thisNeg && !otherNeg) {
      return -1;
    }
    if (!thisNeg && otherNeg) {
      return 1;
    }

    // at this point, the signs are the same, so subtraction will not overflow
    if (this.subtract(other).isNegative()) {
      return -1;
    } else {
      return 1;
    }
  };


  /** @return {!goog.math.Long} The negation of this value. */
  goog.math.Long.prototype.negate = function() {
    if (this.equals(goog.math.Long.MIN_VALUE)) {
      return goog.math.Long.MIN_VALUE;
    } else {
      return this.not().add(goog.math.Long.ONE);
    }
  };


  /**
   * Returns the sum of this and the given Long.
   * @param {goog.math.Long} other Long to add to this one.
   * @return {!goog.math.Long} The sum of this and the given Long.
   */
  goog.math.Long.prototype.add = function(other) {
    // Divide each number into 4 chunks of 16 bits, and then sum the chunks.

    var a48 = this.high_ >>> 16;
    var a32 = this.high_ & 0xFFFF;
    var a16 = this.low_ >>> 16;
    var a00 = this.low_ & 0xFFFF;

    var b48 = other.high_ >>> 16;
    var b32 = other.high_ & 0xFFFF;
    var b16 = other.low_ >>> 16;
    var b00 = other.low_ & 0xFFFF;

    var c48 = 0, c32 = 0, c16 = 0, c00 = 0;
    c00 += a00 + b00;
    c16 += c00 >>> 16;
    c00 &= 0xFFFF;
    c16 += a16 + b16;
    c32 += c16 >>> 16;
    c16 &= 0xFFFF;
    c32 += a32 + b32;
    c48 += c32 >>> 16;
    c32 &= 0xFFFF;
    c48 += a48 + b48;
    c48 &= 0xFFFF;
    return goog.math.Long.fromBits((c16 << 16) | c00, (c48 << 16) | c32);
  };


  /**
   * Returns the difference of this and the given Long.
   * @param {goog.math.Long} other Long to subtract from this.
   * @return {!goog.math.Long} The difference of this and the given Long.
   */
  goog.math.Long.prototype.subtract = function(other) {
    return this.add(other.negate());
  };


  /**
   * Returns the product of this and the given long.
   * @param {goog.math.Long} other Long to multiply with this.
   * @return {!goog.math.Long} The product of this and the other.
   */
  goog.math.Long.prototype.multiply = function(other) {
    if (this.isZero()) {
      return goog.math.Long.ZERO;
    } else if (other.isZero()) {
      return goog.math.Long.ZERO;
    }

    if (this.equals(goog.math.Long.MIN_VALUE)) {
      return other.isOdd() ? goog.math.Long.MIN_VALUE : goog.math.Long.ZERO;
    } else if (other.equals(goog.math.Long.MIN_VALUE)) {
      return this.isOdd() ? goog.math.Long.MIN_VALUE : goog.math.Long.ZERO;
    }

    if (this.isNegative()) {
      if (other.isNegative()) {
        return this.negate().multiply(other.negate());
      } else {
        return this.negate().multiply(other).negate();
      }
    } else if (other.isNegative()) {
      return this.multiply(other.negate()).negate();
    }

    // If both longs are small, use float multiplication
    if (this.lessThan(goog.math.Long.TWO_PWR_24_) &&
        other.lessThan(goog.math.Long.TWO_PWR_24_)) {
      return goog.math.Long.fromNumber(this.toNumber() * other.toNumber());
    }

    // Divide each long into 4 chunks of 16 bits, and then add up 4x4 products.
    // We can skip products that would overflow.

    var a48 = this.high_ >>> 16;
    var a32 = this.high_ & 0xFFFF;
    var a16 = this.low_ >>> 16;
    var a00 = this.low_ & 0xFFFF;

    var b48 = other.high_ >>> 16;
    var b32 = other.high_ & 0xFFFF;
    var b16 = other.low_ >>> 16;
    var b00 = other.low_ & 0xFFFF;

    var c48 = 0, c32 = 0, c16 = 0, c00 = 0;
    c00 += a00 * b00;
    c16 += c00 >>> 16;
    c00 &= 0xFFFF;
    c16 += a16 * b00;
    c32 += c16 >>> 16;
    c16 &= 0xFFFF;
    c16 += a00 * b16;
    c32 += c16 >>> 16;
    c16 &= 0xFFFF;
    c32 += a32 * b00;
    c48 += c32 >>> 16;
    c32 &= 0xFFFF;
    c32 += a16 * b16;
    c48 += c32 >>> 16;
    c32 &= 0xFFFF;
    c32 += a00 * b32;
    c48 += c32 >>> 16;
    c32 &= 0xFFFF;
    c48 += a48 * b00 + a32 * b16 + a16 * b32 + a00 * b48;
    c48 &= 0xFFFF;
    return goog.math.Long.fromBits((c16 << 16) | c00, (c48 << 16) | c32);
  };


  /**
   * Returns this Long divided by the given one.
   * @param {goog.math.Long} other Long by which to divide.
   * @return {!goog.math.Long} This Long divided by the given one.
   */
  goog.math.Long.prototype.div = function(other) {
    if (other.isZero()) {
      throw Error('division by zero');
    } else if (this.isZero()) {
      return goog.math.Long.ZERO;
    }

    if (this.equals(goog.math.Long.MIN_VALUE)) {
      if (other.equals(goog.math.Long.ONE) ||
          other.equals(goog.math.Long.NEG_ONE)) {
        return goog.math.Long.MIN_VALUE;  // recall that -MIN_VALUE == MIN_VALUE
      } else if (other.equals(goog.math.Long.MIN_VALUE)) {
        return goog.math.Long.ONE;
      } else {
        // At this point, we have |other| >= 2, so |this/other| < |MIN_VALUE|.
        var halfThis = this.shiftRight(1);
        var approx = halfThis.div(other).shiftLeft(1);
        if (approx.equals(goog.math.Long.ZERO)) {
          return other.isNegative() ? goog.math.Long.ONE : goog.math.Long.NEG_ONE;
        } else {
          var rem = this.subtract(other.multiply(approx));
          var result = approx.add(rem.div(other));
          return result;
        }
      }
    } else if (other.equals(goog.math.Long.MIN_VALUE)) {
      return goog.math.Long.ZERO;
    }

    if (this.isNegative()) {
      if (other.isNegative()) {
        return this.negate().div(other.negate());
      } else {
        return this.negate().div(other).negate();
      }
    } else if (other.isNegative()) {
      return this.div(other.negate()).negate();
    }

    // Repeat the following until the remainder is less than other:  find a
    // floating-point that approximates remainder / other *from below*, add this
    // into the result, and subtract it from the remainder.  It is critical that
    // the approximate value is less than or equal to the real value so that the
    // remainder never becomes negative.
    var res = goog.math.Long.ZERO;
    var rem = this;
    while (rem.greaterThanOrEqual(other)) {
      // Approximate the result of division. This may be a little greater or
      // smaller than the actual value.
      var approx = Math.max(1, Math.floor(rem.toNumber() / other.toNumber()));

      // We will tweak the approximate result by changing it in the 48-th digit or
      // the smallest non-fractional digit, whichever is larger.
      var log2 = Math.ceil(Math.log(approx) / Math.LN2);
      var delta = (log2 <= 48) ? 1 : Math.pow(2, log2 - 48);

      // Decrease the approximation until it is smaller than the remainder.  Note
      // that if it is too large, the product overflows and is negative.
      var approxRes = goog.math.Long.fromNumber(approx);
      var approxRem = approxRes.multiply(other);
      while (approxRem.isNegative() || approxRem.greaterThan(rem)) {
        approx -= delta;
        approxRes = goog.math.Long.fromNumber(approx);
        approxRem = approxRes.multiply(other);
      }

      // We know the answer can't be zero... and actually, zero would cause
      // infinite recursion since we would make no progress.
      if (approxRes.isZero()) {
        approxRes = goog.math.Long.ONE;
      }

      res = res.add(approxRes);
      rem = rem.subtract(approxRem);
    }
    return res;
  };


  /**
   * Returns this Long modulo the given one.
   * @param {goog.math.Long} other Long by which to mod.
   * @return {!goog.math.Long} This Long modulo the given one.
   */
  goog.math.Long.prototype.modulo = function(other) {
    return this.subtract(this.div(other).multiply(other));
  };


  /** @return {!goog.math.Long} The bitwise-NOT of this value. */
  goog.math.Long.prototype.not = function() {
    return goog.math.Long.fromBits(~this.low_, ~this.high_);
  };


  /**
   * Returns the bitwise-AND of this Long and the given one.
   * @param {goog.math.Long} other The Long with which to AND.
   * @return {!goog.math.Long} The bitwise-AND of this and the other.
   */
  goog.math.Long.prototype.and = function(other) {
    return goog.math.Long.fromBits(this.low_ & other.low_,
                                   this.high_ & other.high_);
  };


  /**
   * Returns the bitwise-OR of this Long and the given one.
   * @param {goog.math.Long} other The Long with which to OR.
   * @return {!goog.math.Long} The bitwise-OR of this and the other.
   */
  goog.math.Long.prototype.or = function(other) {
    return goog.math.Long.fromBits(this.low_ | other.low_,
                                   this.high_ | other.high_);
  };


  /**
   * Returns the bitwise-XOR of this Long and the given one.
   * @param {goog.math.Long} other The Long with which to XOR.
   * @return {!goog.math.Long} The bitwise-XOR of this and the other.
   */
  goog.math.Long.prototype.xor = function(other) {
    return goog.math.Long.fromBits(this.low_ ^ other.low_,
                                   this.high_ ^ other.high_);
  };


  /**
   * Returns this Long with bits shifted to the left by the given amount.
   * @param {number} numBits The number of bits by which to shift.
   * @return {!goog.math.Long} This shifted to the left by the given amount.
   */
  goog.math.Long.prototype.shiftLeft = function(numBits) {
    numBits &= 63;
    if (numBits == 0) {
      return this;
    } else {
      var low = this.low_;
      if (numBits < 32) {
        var high = this.high_;
        return goog.math.Long.fromBits(
            low << numBits,
            (high << numBits) | (low >>> (32 - numBits)));
      } else {
        return goog.math.Long.fromBits(0, low << (numBits - 32));
      }
    }
  };


  /**
   * Returns this Long with bits shifted to the right by the given amount.
   * @param {number} numBits The number of bits by which to shift.
   * @return {!goog.math.Long} This shifted to the right by the given amount.
   */
  goog.math.Long.prototype.shiftRight = function(numBits) {
    numBits &= 63;
    if (numBits == 0) {
      return this;
    } else {
      var high = this.high_;
      if (numBits < 32) {
        var low = this.low_;
        return goog.math.Long.fromBits(
            (low >>> numBits) | (high << (32 - numBits)),
            high >> numBits);
      } else {
        return goog.math.Long.fromBits(
            high >> (numBits - 32),
            high >= 0 ? 0 : -1);
      }
    }
  };


  /**
   * Returns this Long with bits shifted to the right by the given amount, with
   * the new top bits matching the current sign bit.
   * @param {number} numBits The number of bits by which to shift.
   * @return {!goog.math.Long} This shifted to the right by the given amount, with
   *     zeros placed into the new leading bits.
   */
  goog.math.Long.prototype.shiftRightUnsigned = function(numBits) {
    numBits &= 63;
    if (numBits == 0) {
      return this;
    } else {
      var high = this.high_;
      if (numBits < 32) {
        var low = this.low_;
        return goog.math.Long.fromBits(
            (low >>> numBits) | (high << (32 - numBits)),
            high >>> numBits);
      } else if (numBits == 32) {
        return goog.math.Long.fromBits(high, 0);
      } else {
        return goog.math.Long.fromBits(high >>> (numBits - 32), 0);
      }
    }
  };

  //======= begin jsbn =======

  var navigator = { appName: 'Modern Browser' }; // polyfill a little

  // Copyright (c) 2005  Tom Wu
  // All Rights Reserved.
  // http://www-cs-students.stanford.edu/~tjw/jsbn/

  /*
   * Copyright (c) 2003-2005  Tom Wu
   * All Rights Reserved.
   *
   * Permission is hereby granted, free of charge, to any person obtaining
   * a copy of this software and associated documentation files (the
   * "Software"), to deal in the Software without restriction, including
   * without limitation the rights to use, copy, modify, merge, publish,
   * distribute, sublicense, and/or sell copies of the Software, and to
   * permit persons to whom the Software is furnished to do so, subject to
   * the following conditions:
   *
   * The above copyright notice and this permission notice shall be
   * included in all copies or substantial portions of the Software.
   *
   * THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND, 
   * EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY 
   * WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  
   *
   * IN NO EVENT SHALL TOM WU BE LIABLE FOR ANY SPECIAL, INCIDENTAL,
   * INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND, OR ANY DAMAGES WHATSOEVER
   * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER OR NOT ADVISED OF
   * THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF LIABILITY, ARISING OUT
   * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
   *
   * In addition, the following condition applies:
   *
   * All redistributions must retain an intact copy of this copyright notice
   * and disclaimer.
   */

  // Basic JavaScript BN library - subset useful for RSA encryption.

  // Bits per digit
  var dbits;

  // JavaScript engine analysis
  var canary = 0xdeadbeefcafe;
  var j_lm = ((canary&0xffffff)==0xefcafe);

  // (public) Constructor
  function BigInteger(a,b,c) {
    if(a != null)
      if("number" == typeof a) this.fromNumber(a,b,c);
      else if(b == null && "string" != typeof a) this.fromString(a,256);
      else this.fromString(a,b);
  }

  // return new, unset BigInteger
  function nbi() { return new BigInteger(null); }

  // am: Compute w_j += (x*this_i), propagate carries,
  // c is initial carry, returns final carry.
  // c < 3*dvalue, x < 2*dvalue, this_i < dvalue
  // We need to select the fastest one that works in this environment.

  // am1: use a single mult and divide to get the high bits,
  // max digit bits should be 26 because
  // max internal value = 2*dvalue^2-2*dvalue (< 2^53)
  function am1(i,x,w,j,c,n) {
    while(--n >= 0) {
      var v = x*this[i++]+w[j]+c;
      c = Math.floor(v/0x4000000);
      w[j++] = v&0x3ffffff;
    }
    return c;
  }
  // am2 avoids a big mult-and-extract completely.
  // Max digit bits should be <= 30 because we do bitwise ops
  // on values up to 2*hdvalue^2-hdvalue-1 (< 2^31)
  function am2(i,x,w,j,c,n) {
    var xl = x&0x7fff, xh = x>>15;
    while(--n >= 0) {
      var l = this[i]&0x7fff;
      var h = this[i++]>>15;
      var m = xh*l+h*xl;
      l = xl*l+((m&0x7fff)<<15)+w[j]+(c&0x3fffffff);
      c = (l>>>30)+(m>>>15)+xh*h+(c>>>30);
      w[j++] = l&0x3fffffff;
    }
    return c;
  }
  // Alternately, set max digit bits to 28 since some
  // browsers slow down when dealing with 32-bit numbers.
  function am3(i,x,w,j,c,n) {
    var xl = x&0x3fff, xh = x>>14;
    while(--n >= 0) {
      var l = this[i]&0x3fff;
      var h = this[i++]>>14;
      var m = xh*l+h*xl;
      l = xl*l+((m&0x3fff)<<14)+w[j]+c;
      c = (l>>28)+(m>>14)+xh*h;
      w[j++] = l&0xfffffff;
    }
    return c;
  }
  if(j_lm && (navigator.appName == "Microsoft Internet Explorer")) {
    BigInteger.prototype.am = am2;
    dbits = 30;
  }
  else if(j_lm && (navigator.appName != "Netscape")) {
    BigInteger.prototype.am = am1;
    dbits = 26;
  }
  else { // Mozilla/Netscape seems to prefer am3
    BigInteger.prototype.am = am3;
    dbits = 28;
  }

  BigInteger.prototype.DB = dbits;
  BigInteger.prototype.DM = ((1<<dbits)-1);
  BigInteger.prototype.DV = (1<<dbits);

  var BI_FP = 52;
  BigInteger.prototype.FV = Math.pow(2,BI_FP);
  BigInteger.prototype.F1 = BI_FP-dbits;
  BigInteger.prototype.F2 = 2*dbits-BI_FP;

  // Digit conversions
  var BI_RM = "0123456789abcdefghijklmnopqrstuvwxyz";
  var BI_RC = new Array();
  var rr,vv;
  rr = "0".charCodeAt(0);
  for(vv = 0; vv <= 9; ++vv) BI_RC[rr++] = vv;
  rr = "a".charCodeAt(0);
  for(vv = 10; vv < 36; ++vv) BI_RC[rr++] = vv;
  rr = "A".charCodeAt(0);
  for(vv = 10; vv < 36; ++vv) BI_RC[rr++] = vv;

  function int2char(n) { return BI_RM.charAt(n); }
  function intAt(s,i) {
    var c = BI_RC[s.charCodeAt(i)];
    return (c==null)?-1:c;
  }

  // (protected) copy this to r
  function bnpCopyTo(r) {
    for(var i = this.t-1; i >= 0; --i) r[i] = this[i];
    r.t = this.t;
    r.s = this.s;
  }

  // (protected) set from integer value x, -DV <= x < DV
  function bnpFromInt(x) {
    this.t = 1;
    this.s = (x<0)?-1:0;
    if(x > 0) this[0] = x;
    else if(x < -1) this[0] = x+DV;
    else this.t = 0;
  }

  // return bigint initialized to value
  function nbv(i) { var r = nbi(); r.fromInt(i); return r; }

  // (protected) set from string and radix
  function bnpFromString(s,b) {
    var k;
    if(b == 16) k = 4;
    else if(b == 8) k = 3;
    else if(b == 256) k = 8; // byte array
    else if(b == 2) k = 1;
    else if(b == 32) k = 5;
    else if(b == 4) k = 2;
    else { this.fromRadix(s,b); return; }
    this.t = 0;
    this.s = 0;
    var i = s.length, mi = false, sh = 0;
    while(--i >= 0) {
      var x = (k==8)?s[i]&0xff:intAt(s,i);
      if(x < 0) {
        if(s.charAt(i) == "-") mi = true;
        continue;
      }
      mi = false;
      if(sh == 0)
        this[this.t++] = x;
      else if(sh+k > this.DB) {
        this[this.t-1] |= (x&((1<<(this.DB-sh))-1))<<sh;
        this[this.t++] = (x>>(this.DB-sh));
      }
      else
        this[this.t-1] |= x<<sh;
      sh += k;
      if(sh >= this.DB) sh -= this.DB;
    }
    if(k == 8 && (s[0]&0x80) != 0) {
      this.s = -1;
      if(sh > 0) this[this.t-1] |= ((1<<(this.DB-sh))-1)<<sh;
    }
    this.clamp();
    if(mi) BigInteger.ZERO.subTo(this,this);
  }

  // (protected) clamp off excess high words
  function bnpClamp() {
    var c = this.s&this.DM;
    while(this.t > 0 && this[this.t-1] == c) --this.t;
  }

  // (public) return string representation in given radix
  function bnToString(b) {
    if(this.s < 0) return "-"+this.negate().toString(b);
    var k;
    if(b == 16) k = 4;
    else if(b == 8) k = 3;
    else if(b == 2) k = 1;
    else if(b == 32) k = 5;
    else if(b == 4) k = 2;
    else return this.toRadix(b);
    var km = (1<<k)-1, d, m = false, r = "", i = this.t;
    var p = this.DB-(i*this.DB)%k;
    if(i-- > 0) {
      if(p < this.DB && (d = this[i]>>p) > 0) { m = true; r = int2char(d); }
      while(i >= 0) {
        if(p < k) {
          d = (this[i]&((1<<p)-1))<<(k-p);
          d |= this[--i]>>(p+=this.DB-k);
        }
        else {
          d = (this[i]>>(p-=k))&km;
          if(p <= 0) { p += this.DB; --i; }
        }
        if(d > 0) m = true;
        if(m) r += int2char(d);
      }
    }
    return m?r:"0";
  }

  // (public) -this
  function bnNegate() { var r = nbi(); BigInteger.ZERO.subTo(this,r); return r; }

  // (public) |this|
  function bnAbs() { return (this.s<0)?this.negate():this; }

  // (public) return + if this > a, - if this < a, 0 if equal
  function bnCompareTo(a) {
    var r = this.s-a.s;
    if(r != 0) return r;
    var i = this.t;
    r = i-a.t;
    if(r != 0) return (this.s<0)?-r:r;
    while(--i >= 0) if((r=this[i]-a[i]) != 0) return r;
    return 0;
  }

  // returns bit length of the integer x
  function nbits(x) {
    var r = 1, t;
    if((t=x>>>16) != 0) { x = t; r += 16; }
    if((t=x>>8) != 0) { x = t; r += 8; }
    if((t=x>>4) != 0) { x = t; r += 4; }
    if((t=x>>2) != 0) { x = t; r += 2; }
    if((t=x>>1) != 0) { x = t; r += 1; }
    return r;
  }

  // (public) return the number of bits in "this"
  function bnBitLength() {
    if(this.t <= 0) return 0;
    return this.DB*(this.t-1)+nbits(this[this.t-1]^(this.s&this.DM));
  }

  // (protected) r = this << n*DB
  function bnpDLShiftTo(n,r) {
    var i;
    for(i = this.t-1; i >= 0; --i) r[i+n] = this[i];
    for(i = n-1; i >= 0; --i) r[i] = 0;
    r.t = this.t+n;
    r.s = this.s;
  }

  // (protected) r = this >> n*DB
  function bnpDRShiftTo(n,r) {
    for(var i = n; i < this.t; ++i) r[i-n] = this[i];
    r.t = Math.max(this.t-n,0);
    r.s = this.s;
  }

  // (protected) r = this << n
  function bnpLShiftTo(n,r) {
    var bs = n%this.DB;
    var cbs = this.DB-bs;
    var bm = (1<<cbs)-1;
    var ds = Math.floor(n/this.DB), c = (this.s<<bs)&this.DM, i;
    for(i = this.t-1; i >= 0; --i) {
      r[i+ds+1] = (this[i]>>cbs)|c;
      c = (this[i]&bm)<<bs;
    }
    for(i = ds-1; i >= 0; --i) r[i] = 0;
    r[ds] = c;
    r.t = this.t+ds+1;
    r.s = this.s;
    r.clamp();
  }

  // (protected) r = this >> n
  function bnpRShiftTo(n,r) {
    r.s = this.s;
    var ds = Math.floor(n/this.DB);
    if(ds >= this.t) { r.t = 0; return; }
    var bs = n%this.DB;
    var cbs = this.DB-bs;
    var bm = (1<<bs)-1;
    r[0] = this[ds]>>bs;
    for(var i = ds+1; i < this.t; ++i) {
      r[i-ds-1] |= (this[i]&bm)<<cbs;
      r[i-ds] = this[i]>>bs;
    }
    if(bs > 0) r[this.t-ds-1] |= (this.s&bm)<<cbs;
    r.t = this.t-ds;
    r.clamp();
  }

  // (protected) r = this - a
  function bnpSubTo(a,r) {
    var i = 0, c = 0, m = Math.min(a.t,this.t);
    while(i < m) {
      c += this[i]-a[i];
      r[i++] = c&this.DM;
      c >>= this.DB;
    }
    if(a.t < this.t) {
      c -= a.s;
      while(i < this.t) {
        c += this[i];
        r[i++] = c&this.DM;
        c >>= this.DB;
      }
      c += this.s;
    }
    else {
      c += this.s;
      while(i < a.t) {
        c -= a[i];
        r[i++] = c&this.DM;
        c >>= this.DB;
      }
      c -= a.s;
    }
    r.s = (c<0)?-1:0;
    if(c < -1) r[i++] = this.DV+c;
    else if(c > 0) r[i++] = c;
    r.t = i;
    r.clamp();
  }

  // (protected) r = this * a, r != this,a (HAC 14.12)
  // "this" should be the larger one if appropriate.
  function bnpMultiplyTo(a,r) {
    var x = this.abs(), y = a.abs();
    var i = x.t;
    r.t = i+y.t;
    while(--i >= 0) r[i] = 0;
    for(i = 0; i < y.t; ++i) r[i+x.t] = x.am(0,y[i],r,i,0,x.t);
    r.s = 0;
    r.clamp();
    if(this.s != a.s) BigInteger.ZERO.subTo(r,r);
  }

  // (protected) r = this^2, r != this (HAC 14.16)
  function bnpSquareTo(r) {
    var x = this.abs();
    var i = r.t = 2*x.t;
    while(--i >= 0) r[i] = 0;
    for(i = 0; i < x.t-1; ++i) {
      var c = x.am(i,x[i],r,2*i,0,1);
      if((r[i+x.t]+=x.am(i+1,2*x[i],r,2*i+1,c,x.t-i-1)) >= x.DV) {
        r[i+x.t] -= x.DV;
        r[i+x.t+1] = 1;
      }
    }
    if(r.t > 0) r[r.t-1] += x.am(i,x[i],r,2*i,0,1);
    r.s = 0;
    r.clamp();
  }

  // (protected) divide this by m, quotient and remainder to q, r (HAC 14.20)
  // r != q, this != m.  q or r may be null.
  function bnpDivRemTo(m,q,r) {
    var pm = m.abs();
    if(pm.t <= 0) return;
    var pt = this.abs();
    if(pt.t < pm.t) {
      if(q != null) q.fromInt(0);
      if(r != null) this.copyTo(r);
      return;
    }
    if(r == null) r = nbi();
    var y = nbi(), ts = this.s, ms = m.s;
    var nsh = this.DB-nbits(pm[pm.t-1]);	// normalize modulus
    if(nsh > 0) { pm.lShiftTo(nsh,y); pt.lShiftTo(nsh,r); }
    else { pm.copyTo(y); pt.copyTo(r); }
    var ys = y.t;
    var y0 = y[ys-1];
    if(y0 == 0) return;
    var yt = y0*(1<<this.F1)+((ys>1)?y[ys-2]>>this.F2:0);
    var d1 = this.FV/yt, d2 = (1<<this.F1)/yt, e = 1<<this.F2;
    var i = r.t, j = i-ys, t = (q==null)?nbi():q;
    y.dlShiftTo(j,t);
    if(r.compareTo(t) >= 0) {
      r[r.t++] = 1;
      r.subTo(t,r);
    }
    BigInteger.ONE.dlShiftTo(ys,t);
    t.subTo(y,y);	// "negative" y so we can replace sub with am later
    while(y.t < ys) y[y.t++] = 0;
    while(--j >= 0) {
      // Estimate quotient digit
      var qd = (r[--i]==y0)?this.DM:Math.floor(r[i]*d1+(r[i-1]+e)*d2);
      if((r[i]+=y.am(0,qd,r,j,0,ys)) < qd) {	// Try it out
        y.dlShiftTo(j,t);
        r.subTo(t,r);
        while(r[i] < --qd) r.subTo(t,r);
      }
    }
    if(q != null) {
      r.drShiftTo(ys,q);
      if(ts != ms) BigInteger.ZERO.subTo(q,q);
    }
    r.t = ys;
    r.clamp();
    if(nsh > 0) r.rShiftTo(nsh,r);	// Denormalize remainder
    if(ts < 0) BigInteger.ZERO.subTo(r,r);
  }

  // (public) this mod a
  function bnMod(a) {
    var r = nbi();
    this.abs().divRemTo(a,null,r);
    if(this.s < 0 && r.compareTo(BigInteger.ZERO) > 0) a.subTo(r,r);
    return r;
  }

  // Modular reduction using "classic" algorithm
  function Classic(m) { this.m = m; }
  function cConvert(x) {
    if(x.s < 0 || x.compareTo(this.m) >= 0) return x.mod(this.m);
    else return x;
  }
  function cRevert(x) { return x; }
  function cReduce(x) { x.divRemTo(this.m,null,x); }
  function cMulTo(x,y,r) { x.multiplyTo(y,r); this.reduce(r); }
  function cSqrTo(x,r) { x.squareTo(r); this.reduce(r); }

  Classic.prototype.convert = cConvert;
  Classic.prototype.revert = cRevert;
  Classic.prototype.reduce = cReduce;
  Classic.prototype.mulTo = cMulTo;
  Classic.prototype.sqrTo = cSqrTo;

  // (protected) return "-1/this % 2^DB"; useful for Mont. reduction
  // justification:
  //         xy == 1 (mod m)
  //         xy =  1+km
  //   xy(2-xy) = (1+km)(1-km)
  // x[y(2-xy)] = 1-k^2m^2
  // x[y(2-xy)] == 1 (mod m^2)
  // if y is 1/x mod m, then y(2-xy) is 1/x mod m^2
  // should reduce x and y(2-xy) by m^2 at each step to keep size bounded.
  // JS multiply "overflows" differently from C/C++, so care is needed here.
  function bnpInvDigit() {
    if(this.t < 1) return 0;
    var x = this[0];
    if((x&1) == 0) return 0;
    var y = x&3;		// y == 1/x mod 2^2
    y = (y*(2-(x&0xf)*y))&0xf;	// y == 1/x mod 2^4
    y = (y*(2-(x&0xff)*y))&0xff;	// y == 1/x mod 2^8
    y = (y*(2-(((x&0xffff)*y)&0xffff)))&0xffff;	// y == 1/x mod 2^16
    // last step - calculate inverse mod DV directly;
    // assumes 16 < DB <= 32 and assumes ability to handle 48-bit ints
    y = (y*(2-x*y%this.DV))%this.DV;		// y == 1/x mod 2^dbits
    // we really want the negative inverse, and -DV < y < DV
    return (y>0)?this.DV-y:-y;
  }

  // Montgomery reduction
  function Montgomery(m) {
    this.m = m;
    this.mp = m.invDigit();
    this.mpl = this.mp&0x7fff;
    this.mph = this.mp>>15;
    this.um = (1<<(m.DB-15))-1;
    this.mt2 = 2*m.t;
  }

  // xR mod m
  function montConvert(x) {
    var r = nbi();
    x.abs().dlShiftTo(this.m.t,r);
    r.divRemTo(this.m,null,r);
    if(x.s < 0 && r.compareTo(BigInteger.ZERO) > 0) this.m.subTo(r,r);
    return r;
  }

  // x/R mod m
  function montRevert(x) {
    var r = nbi();
    x.copyTo(r);
    this.reduce(r);
    return r;
  }

  // x = x/R mod m (HAC 14.32)
  function montReduce(x) {
    while(x.t <= this.mt2)	// pad x so am has enough room later
      x[x.t++] = 0;
    for(var i = 0; i < this.m.t; ++i) {
      // faster way of calculating u0 = x[i]*mp mod DV
      var j = x[i]&0x7fff;
      var u0 = (j*this.mpl+(((j*this.mph+(x[i]>>15)*this.mpl)&this.um)<<15))&x.DM;
      // use am to combine the multiply-shift-add into one call
      j = i+this.m.t;
      x[j] += this.m.am(0,u0,x,i,0,this.m.t);
      // propagate carry
      while(x[j] >= x.DV) { x[j] -= x.DV; x[++j]++; }
    }
    x.clamp();
    x.drShiftTo(this.m.t,x);
    if(x.compareTo(this.m) >= 0) x.subTo(this.m,x);
  }

  // r = "x^2/R mod m"; x != r
  function montSqrTo(x,r) { x.squareTo(r); this.reduce(r); }

  // r = "xy/R mod m"; x,y != r
  function montMulTo(x,y,r) { x.multiplyTo(y,r); this.reduce(r); }

  Montgomery.prototype.convert = montConvert;
  Montgomery.prototype.revert = montRevert;
  Montgomery.prototype.reduce = montReduce;
  Montgomery.prototype.mulTo = montMulTo;
  Montgomery.prototype.sqrTo = montSqrTo;

  // (protected) true iff this is even
  function bnpIsEven() { return ((this.t>0)?(this[0]&1):this.s) == 0; }

  // (protected) this^e, e < 2^32, doing sqr and mul with "r" (HAC 14.79)
  function bnpExp(e,z) {
    if(e > 0xffffffff || e < 1) return BigInteger.ONE;
    var r = nbi(), r2 = nbi(), g = z.convert(this), i = nbits(e)-1;
    g.copyTo(r);
    while(--i >= 0) {
      z.sqrTo(r,r2);
      if((e&(1<<i)) > 0) z.mulTo(r2,g,r);
      else { var t = r; r = r2; r2 = t; }
    }
    return z.revert(r);
  }

  // (public) this^e % m, 0 <= e < 2^32
  function bnModPowInt(e,m) {
    var z;
    if(e < 256 || m.isEven()) z = new Classic(m); else z = new Montgomery(m);
    return this.exp(e,z);
  }

  // protected
  BigInteger.prototype.copyTo = bnpCopyTo;
  BigInteger.prototype.fromInt = bnpFromInt;
  BigInteger.prototype.fromString = bnpFromString;
  BigInteger.prototype.clamp = bnpClamp;
  BigInteger.prototype.dlShiftTo = bnpDLShiftTo;
  BigInteger.prototype.drShiftTo = bnpDRShiftTo;
  BigInteger.prototype.lShiftTo = bnpLShiftTo;
  BigInteger.prototype.rShiftTo = bnpRShiftTo;
  BigInteger.prototype.subTo = bnpSubTo;
  BigInteger.prototype.multiplyTo = bnpMultiplyTo;
  BigInteger.prototype.squareTo = bnpSquareTo;
  BigInteger.prototype.divRemTo = bnpDivRemTo;
  BigInteger.prototype.invDigit = bnpInvDigit;
  BigInteger.prototype.isEven = bnpIsEven;
  BigInteger.prototype.exp = bnpExp;

  // public
  BigInteger.prototype.toString = bnToString;
  BigInteger.prototype.negate = bnNegate;
  BigInteger.prototype.abs = bnAbs;
  BigInteger.prototype.compareTo = bnCompareTo;
  BigInteger.prototype.bitLength = bnBitLength;
  BigInteger.prototype.mod = bnMod;
  BigInteger.prototype.modPowInt = bnModPowInt;

  // "constants"
  BigInteger.ZERO = nbv(0);
  BigInteger.ONE = nbv(1);

  // jsbn2 stuff

  // (protected) convert from radix string
  function bnpFromRadix(s,b) {
    this.fromInt(0);
    if(b == null) b = 10;
    var cs = this.chunkSize(b);
    var d = Math.pow(b,cs), mi = false, j = 0, w = 0;
    for(var i = 0; i < s.length; ++i) {
      var x = intAt(s,i);
      if(x < 0) {
        if(s.charAt(i) == "-" && this.signum() == 0) mi = true;
        continue;
      }
      w = b*w+x;
      if(++j >= cs) {
        this.dMultiply(d);
        this.dAddOffset(w,0);
        j = 0;
        w = 0;
      }
    }
    if(j > 0) {
      this.dMultiply(Math.pow(b,j));
      this.dAddOffset(w,0);
    }
    if(mi) BigInteger.ZERO.subTo(this,this);
  }

  // (protected) return x s.t. r^x < DV
  function bnpChunkSize(r) { return Math.floor(Math.LN2*this.DB/Math.log(r)); }

  // (public) 0 if this == 0, 1 if this > 0
  function bnSigNum() {
    if(this.s < 0) return -1;
    else if(this.t <= 0 || (this.t == 1 && this[0] <= 0)) return 0;
    else return 1;
  }

  // (protected) this *= n, this >= 0, 1 < n < DV
  function bnpDMultiply(n) {
    this[this.t] = this.am(0,n-1,this,0,0,this.t);
    ++this.t;
    this.clamp();
  }

  // (protected) this += n << w words, this >= 0
  function bnpDAddOffset(n,w) {
    if(n == 0) return;
    while(this.t <= w) this[this.t++] = 0;
    this[w] += n;
    while(this[w] >= this.DV) {
      this[w] -= this.DV;
      if(++w >= this.t) this[this.t++] = 0;
      ++this[w];
    }
  }

  // (protected) convert to radix string
  function bnpToRadix(b) {
    if(b == null) b = 10;
    if(this.signum() == 0 || b < 2 || b > 36) return "0";
    var cs = this.chunkSize(b);
    var a = Math.pow(b,cs);
    var d = nbv(a), y = nbi(), z = nbi(), r = "";
    this.divRemTo(d,y,z);
    while(y.signum() > 0) {
      r = (a+z.intValue()).toString(b).substr(1) + r;
      y.divRemTo(d,y,z);
    }
    return z.intValue().toString(b) + r;
  }

  // (public) return value as integer
  function bnIntValue() {
    if(this.s < 0) {
      if(this.t == 1) return this[0]-this.DV;
      else if(this.t == 0) return -1;
    }
    else if(this.t == 1) return this[0];
    else if(this.t == 0) return 0;
    // assumes 16 < DB < 32
    return ((this[1]&((1<<(32-this.DB))-1))<<this.DB)|this[0];
  }

  // (protected) r = this + a
  function bnpAddTo(a,r) {
    var i = 0, c = 0, m = Math.min(a.t,this.t);
    while(i < m) {
      c += this[i]+a[i];
      r[i++] = c&this.DM;
      c >>= this.DB;
    }
    if(a.t < this.t) {
      c += a.s;
      while(i < this.t) {
        c += this[i];
        r[i++] = c&this.DM;
        c >>= this.DB;
      }
      c += this.s;
    }
    else {
      c += this.s;
      while(i < a.t) {
        c += a[i];
        r[i++] = c&this.DM;
        c >>= this.DB;
      }
      c += a.s;
    }
    r.s = (c<0)?-1:0;
    if(c > 0) r[i++] = c;
    else if(c < -1) r[i++] = this.DV+c;
    r.t = i;
    r.clamp();
  }

  BigInteger.prototype.fromRadix = bnpFromRadix;
  BigInteger.prototype.chunkSize = bnpChunkSize;
  BigInteger.prototype.signum = bnSigNum;
  BigInteger.prototype.dMultiply = bnpDMultiply;
  BigInteger.prototype.dAddOffset = bnpDAddOffset;
  BigInteger.prototype.toRadix = bnpToRadix;
  BigInteger.prototype.intValue = bnIntValue;
  BigInteger.prototype.addTo = bnpAddTo;

  //======= end jsbn =======

  // Emscripten wrapper
  var Wrapper = {
    abs: function(l, h) {
      var x = new goog.math.Long(l, h);
      var ret;
      if (x.isNegative()) {
        ret = x.negate();
      } else {
        ret = x;
      }
      HEAP32[tempDoublePtr>>2] = ret.low_;
      HEAP32[tempDoublePtr+4>>2] = ret.high_;
    },
    ensureTemps: function() {
      if (Wrapper.ensuredTemps) return;
      Wrapper.ensuredTemps = true;
      Wrapper.two32 = new BigInteger();
      Wrapper.two32.fromString('4294967296', 10);
      Wrapper.two64 = new BigInteger();
      Wrapper.two64.fromString('18446744073709551616', 10);
      Wrapper.temp1 = new BigInteger();
      Wrapper.temp2 = new BigInteger();
    },
    lh2bignum: function(l, h) {
      var a = new BigInteger();
      a.fromString(h.toString(), 10);
      var b = new BigInteger();
      a.multiplyTo(Wrapper.two32, b);
      var c = new BigInteger();
      c.fromString(l.toString(), 10);
      var d = new BigInteger();
      c.addTo(b, d);
      return d;
    },
    stringify: function(l, h, unsigned) {
      var ret = new goog.math.Long(l, h).toString();
      if (unsigned && ret[0] == '-') {
        // unsign slowly using jsbn bignums
        Wrapper.ensureTemps();
        var bignum = new BigInteger();
        bignum.fromString(ret, 10);
        ret = new BigInteger();
        Wrapper.two64.addTo(bignum, ret);
        ret = ret.toString(10);
      }
      return ret;
    },
    fromString: function(str, base, min, max, unsigned) {
      Wrapper.ensureTemps();
      var bignum = new BigInteger();
      bignum.fromString(str, base);
      var bigmin = new BigInteger();
      bigmin.fromString(min, 10);
      var bigmax = new BigInteger();
      bigmax.fromString(max, 10);
      if (unsigned && bignum.compareTo(BigInteger.ZERO) < 0) {
        var temp = new BigInteger();
        bignum.addTo(Wrapper.two64, temp);
        bignum = temp;
      }
      var error = false;
      if (bignum.compareTo(bigmin) < 0) {
        bignum = bigmin;
        error = true;
      } else if (bignum.compareTo(bigmax) > 0) {
        bignum = bigmax;
        error = true;
      }
      var ret = goog.math.Long.fromString(bignum.toString()); // min-max checks should have clamped this to a range goog.math.Long can handle well
      HEAP32[tempDoublePtr>>2] = ret.low_;
      HEAP32[tempDoublePtr+4>>2] = ret.high_;
      if (error) throw 'range error';
    }
  };
  return Wrapper;
})();

//======= end closure i64 code =======



// === Auto-generated postamble setup entry stuff ===

if (memoryInitializer) {
  if (ENVIRONMENT_IS_NODE || ENVIRONMENT_IS_SHELL) {
    var data = Module['readBinary'](memoryInitializer);
    HEAPU8.set(data, STATIC_BASE);
  } else {
    addRunDependency('memory initializer');
    Browser.asyncLoad(memoryInitializer, function(data) {
      HEAPU8.set(data, STATIC_BASE);
      removeRunDependency('memory initializer');
    }, function(data) {
      throw 'could not load memory initializer ' + memoryInitializer;
    });
  }
}

function ExitStatus(status) {
  this.name = "ExitStatus";
  this.message = "Program terminated with exit(" + status + ")";
  this.status = status;
};
ExitStatus.prototype = new Error();
ExitStatus.prototype.constructor = ExitStatus;

var initialStackTop;
var preloadStartTime = null;
var calledMain = false;

dependenciesFulfilled = function runCaller() {
  // If run has never been called, and we should call run (INVOKE_RUN is true, and Module.noInitialRun is not false)
  if (!Module['calledRun'] && shouldRunNow) run();
  if (!Module['calledRun']) dependenciesFulfilled = runCaller; // try this again later, after new deps are fulfilled
}

Module['callMain'] = Module.callMain = function callMain(args) {
  assert(runDependencies == 0, 'cannot call main when async dependencies remain! (listen on __ATMAIN__)');
  assert(__ATPRERUN__.length == 0, 'cannot call main when preRun functions remain to be called');

  args = args || [];

  ensureInitRuntime();

  var argc = args.length+1;
  function pad() {
    for (var i = 0; i < 4-1; i++) {
      argv.push(0);
    }
  }
  var argv = [allocate(intArrayFromString("/bin/this.program"), 'i8', ALLOC_NORMAL) ];
  pad();
  for (var i = 0; i < argc-1; i = i + 1) {
    argv.push(allocate(intArrayFromString(args[i]), 'i8', ALLOC_NORMAL));
    pad();
  }
  argv.push(0);
  argv = allocate(argv, 'i32', ALLOC_NORMAL);

  initialStackTop = STACKTOP;

  try {

    var ret = Module['_main'](argc, argv, 0);


    // if we're not running an evented main loop, it's time to exit
    if (!Module['noExitRuntime']) {
      exit(ret);
    }
  }
  catch(e) {
    if (e instanceof ExitStatus) {
      // exit() throws this once it's done to make sure execution
      // has been stopped completely
      return;
    } else if (e == 'SimulateInfiniteLoop') {
      // running an evented main loop, don't immediately exit
      Module['noExitRuntime'] = true;
      return;
    } else {
      if (e && typeof e === 'object' && e.stack) Module.printErr('exception thrown: ' + [e, e.stack]);
      throw e;
    }
  } finally {
    calledMain = true;
  }
}




function run(args) {
  args = args || Module['arguments'];

  if (preloadStartTime === null) preloadStartTime = Date.now();

  if (runDependencies > 0) {
    Module.printErr('run() called, but dependencies remain, so not running');
    return;
  }

  preRun();

  if (runDependencies > 0) return; // a preRun added a dependency, run will be called later
  if (Module['calledRun']) return; // run may have just been called through dependencies being fulfilled just in this very frame

  function doRun() {
    if (Module['calledRun']) return; // run may have just been called while the async setStatus time below was happening
    Module['calledRun'] = true;

    ensureInitRuntime();

    preMain();

    if (ENVIRONMENT_IS_WEB && preloadStartTime !== null) {
      Module.printErr('pre-main prep time: ' + (Date.now() - preloadStartTime) + ' ms');
    }

    if (Module['_main'] && shouldRunNow) {
      Module['callMain'](args);
    }

    postRun();
  }

  if (Module['setStatus']) {
    Module['setStatus']('Running...');
    setTimeout(function() {
      setTimeout(function() {
        Module['setStatus']('');
      }, 1);
      if (!ABORT) doRun();
    }, 1);
  } else {
    doRun();
  }
}
Module['run'] = Module.run = run;

function exit(status) {
  ABORT = true;
  EXITSTATUS = status;
  STACKTOP = initialStackTop;

  // exit the runtime
  exitRuntime();

  // TODO We should handle this differently based on environment.
  // In the browser, the best we can do is throw an exception
  // to halt execution, but in node we could process.exit and
  // I'd imagine SM shell would have something equivalent.
  // This would let us set a proper exit status (which
  // would be great for checking test exit statuses).
  // https://github.com/kripken/emscripten/issues/1371

  // throw an exception to halt the current execution
  throw new ExitStatus(status);
}
Module['exit'] = Module.exit = exit;

function abort(text) {
  if (text) {
    Module.print(text);
    Module.printErr(text);
  }

  ABORT = true;
  EXITSTATUS = 1;

  var extra = '\nIf this abort() is unexpected, build with -s ASSERTIONS=1 which can give more information.';

  throw 'abort() at ' + stackTrace() + extra;
}
Module['abort'] = Module.abort = abort;

// {{PRE_RUN_ADDITIONS}}

if (Module['preInit']) {
  if (typeof Module['preInit'] == 'function') Module['preInit'] = [Module['preInit']];
  while (Module['preInit'].length > 0) {
    Module['preInit'].pop()();
  }
}

// shouldRunNow refers to calling main(), not run().
var shouldRunNow = true;
if (Module['noInitialRun']) {
  shouldRunNow = false;
}


run();

// {{POST_RUN_ADDITIONS}}






// {{MODULE_ADDITIONS}}






