
  
  
  function malloc(buffer){
    var bufferSize=buffer.length*buffer.BYTES_PER_ELEMENT;
    var pointer=Module._malloc(bufferSize);
    var streamHeapLoc=new Uint8Array(Module.HEAPU8.buffer,pointer,bufferSize);
    streamHeapLoc.set(new Uint8Array(buffer));
    return pointer;
  }

  function free(pointer){
    Module._free(pointer);
  }
  
  function BuildStruct(signature,object,pointer){
    /*TODO be able to allocate pointer based on signature size*/
    object=object||{};
    function setProp(to,pointer,name,signature){
      Object.defineProperty(to,name,{
        enumerable:true,
        configurable:false,
        get:function(){
          var value=Module.getValue(pointer+signature[name][0],signature[name][1]);
          if(signature[name].length>2){
            return signature[name][2](value);
          }else
            return value;
        },
        set:function(value){
          if(signature[name].length==4)
            value=signature[name][3](value);
          Module.setValue(pointer+signature[name][0],value,signature[name][1]);  
        },
       
      });
    }
                   
    for(var prop in signature){
      setProp(object,pointer,prop,signature);
    }  
    return object;
  }
  
  function WMPStream(byteStream){//byte stream uint8Array
    var StreamOffset=0;
    var streamHeapLoc;
    var closePointer;
    
    var pointer=Module._malloc(44);
   // var struct=Runtime.generateStructInfo(['state','fMem','Close','EOS','Read','Write','SetPos','GetPos'],"WMPStream");
   // console.log(struct);
    
    function close(structPointer){
      Runtime.removeFunction(closePointer);
      Runtime.removeFunction(EOSPointer);
      Runtime.removeFunction(readPointer);
      Runtime.removeFunction(writePointer);
      Runtime.removeFunction(setPosPointer);
      Runtime.removeFunction(getPosPointer);
      
      Module._free(streamHeapLoc);
    }
    closePointer=Runtime.addFunction(close);
    Module.setValue(pointer+20,closePointer,'*');
    
    function EOS(dummy){
      if(StreamOffset===byteStream.length-1)
        return 1;
      else
        return 0;
    }
    var EOSPointer=Runtime.addFunction(EOS);
    Module.setValue(pointer+24,EOSPointer,'*');
    
    function read(dummy,buffer,size){
      if(size+StreamOffset>=byteStream.length){
        return -102;
      }
      if(buffer<0){
        var err=new Error();
        console.log(err.stack);
        return -1;
      }
      
      var to=new Uint8Array(Module.HEAPU8.buffer,buffer,size);
      var from=byteStream.subarray(StreamOffset,StreamOffset+size);
      to.set(from);
      
      StreamOffset+=size;


      return size;
    }
    var readPointer=Runtime.addFunction(read);
    Module.setValue(pointer+28,readPointer,'*');
    
    function write(dummy,buffer,size){
      if(size+StreamOffset>=byteStream.length)
        return -102;
      var tmp=new Uint8Array(byteStream,StreamOffset,size);
      tmp.set(new Uint8Array(Module.HEAPU8.buffer,buffer,size));
      
      StreamOffset+=size;
      return 0;
    }
    var writePointer=Runtime.addFunction(write);
    Module.setValue(pointer+32,writePointer,'*');
    
    function setPos(dummy,offset){
      StreamOffset=offset;
    }
    var setPosPointer=Runtime.addFunction(setPos);
    Module.setValue(pointer+36,setPosPointer,'*');

    
    function getPos(dummy,pointerOffset){
      Module.setValue(pointerOffset,StreamOffset,'i32');
    }
    var getPosPointer=Runtime.addFunction(getPos)
    Module.setValue(pointer+40,getPosPointer,'*');
    
    this.free=function(){
      close();
      Module._free(pointer);
    }
    
    this.pointer=pointer;
  }
  
  
  function CWMIStrCodecParam(){
    var WMPStream;
  /*      var struct=Runtime.generateStructInfo(['bVerbose',
                                           'uiDefaultQPIndex','uiDefaultQPIndexYLP','uiDefaultQPIndexYHP','uiDefaultQPIndexU',
                                           'uiDefaultQPIndexULP','uiDefaultQPIndexUHP','uiDefaultQPIndexV',
                                           'uiDefaultQPIndexVLP','uiDefaultQPIndexVHP','uiDefaultQPIndexAlpha',
                                           'cfColorFormat','bdBitDepth','olOverlap','bfBitstreamFormat','cChannel',
                                           'uAlphaMode','sbSubband','uiTrimFlexBits','pWStream','cbStream',
                                           'cNumOfSliceMinus1V','uiTileX','cNumOfSliceMinus1H','uiTileY',
                                           'nLenMantissaOrShift','nExpBias','bBlackWhite','bUseHardTileBoundaries',
                                           'bProgressiveMode','bYUVData','bUnscaledArith','fMeasurePerf'],"tagCWMIStrCodecParam");
    console.log(struct); */
  
  
    var pointer=Module._malloc(32860);
    
    function colorformatGet(value){
      switch(value){
        case 0:return 'Y_ONLY'; 
	      case 1:return 'YUV_420';
	      case 2:return 'YUV_422';
	      case 3:return 'YUV_444';
	      case 4:return 'CMYK';
	      case 6:return 'NCOMPONENT';
        case 7:return 'CF_RGB';
	      case 8:return 'CF_RGBE';
      }
    }
    function colorformatSet(value){
      switch(value){
        case 'Y_ONLY':return 0; 
	      case 'YUV_420':return 1;
	      case 'YUV_422':return 2;
	      case 'YUV_444':return 3;
	      case 'CMYK':return 4;
	      case 'NCOMPONENT':return 6;
        case 'CF_RGB':return 7;
	      case 'CF_RGBE':return 8;
      }
    }
    
    function bdBitDepthGet(value){
      switch(value){
        case 0:return 'BD_SHORT';
        case 1:return 'BD_LONG';
      }
    }

    function bdBitDepthSet(value){
      switch(value){
        case 'BD_SHORT':return 0;
        case 'BD_LONG':return 1;
      }
    }
    
    function olOverlapGet(value){
      switch(value){
        case 0:return 'OL_NONE';
        case 1:return 'OL_ONE';
        case 2:return 'OL_TWO';
      }
    }
    function olOverlapSet(value){
      switch(value){
        case 'OL_NONE':return 0;
        case 'OL_ONE':return 1;
        case 'OL_TWO':return 2;
      }
    }
    
    function bfBitstreamFormatGet(value){
      switch(value){
        case 0:return 'SPATIAL';
        case 1:return 'FREQUENCY'; 
      }
    }
    
    function bfBitstreamFormatSet(value){
      switch(value){
        case 'SPATIAL':return 0;
        case 'FREQUENCY':return 1; 
      }
    }
    
    var signature={cfColorFormat:[16,'i32',colorformatGet,colorformatSet],
                   bdBitDepth:[20,'i32',bdBitDepthGet,bdBitDepthSet],
                   olOverlap:[24,'i32',olOverlapGet,olOverlapSet,bfBitstreamFormatSet],
                   bfBitstreamFormat:[28,'i32',bfBitstreamFormatGet],
                   cChannel:[32,'i32'],uAlphaMode:[36,'i32'],
                   sbSubband:[40,'i32'],uiTrimFlexBits:[44,'i32']};
                   
    BuildStruct(signature,this,pointer);


    Object.defineProperty(this,'WMPStream',{
      enumerable:true,
      configurable:false,
      get:function(){
        return WMPStream;
      },
      set:function(val){
        WMPStream=val;
        Module.setValue(pointer+48,val.pointer,'*');  
      },
                               
    });
    
    this.free=function(){
      Module._free(pointer);
    }
    
    this.pointer=pointer;
  }


  function CWMImageInfo(){
    /*var struct=Runtime.generateStructInfo(['cWidth','cHeight','cfColorFormat','bdBitDepth',
                                          'cBitsPerUnit','cLeadingPadding','bRGB','cChromaCenteringX',
                                          'cChromaCenteringY','cROILeftX','cROIWidth','cROITopY','cROIHeight',
                                          'bSkipFlexbits','cThumbnailWidth','cThumbnailHeight','oOrientation',
                                          'cPostProcStrength','fPaddedUserBuffer'],"tagCWMImageInfo");
   console.log(struct);*/
    function colorformatGet(value){
      switch(value){
        case 0:return 'Y_ONLY'; 
	      case 1:return 'YUV_420';
	      case 2:return 'YUV_422';
	      case 3:return 'YUV_444';
	      case 4:return 'CMYK';
	      case 6:return 'NCOMPONENT';
        case 7:return 'CF_RGB';
	      case 8:return 'CF_RGBE';
      }
    }
    function colorformatSet(value){
      switch(value){
        case 'Y_ONLY':return 0; 
	      case 'YUV_420':return 1;
	      case 'YUV_422':return 2;
	      case 'YUV_444':return 3;
	      case 'CMYK':return 4;
	      case 'NCOMPONENT':return 6;
        case 'CF_RGB':return 7;
	      case 'CF_RGBE':return 8;
      }
    }
   
   
    function bdBitDepthGet(value){
      switch(value){
        case 0:return 'BD_1'; //White is foreground
        case 1:return 'BD_8';
        case 2:return 'BD_16';
        case 3:return 'BD_16S';
        case 4:return 'BD_16F';
        case 5:return 'BD_32';
        case 6:return 'BD_32S';
        case 7:return 'BD_32F';
        case 8:return 'BD_5';
        case 9:return 'BD_10';
        case 10:return 'BD_565';
      }
    }
    
    function bdBitDepthSet(value){
      switch(value){
        case 'BD_1':return 0; //White is foreground
        case 'BD_8':return 1;
        case 'BD_16':return 2;
        case 'BD_16S':return 3;
        case 'BD_16F':return 4;
        case 'BD_32':return 5;
        case 'BD_32S':return 6;
        case 'BD_32F':return 7;
        case 'BD_5':return 8;
        case 'BD_10':return 9;
        case 'BD_565':return 10;
      }
    }
   
    function oOrientationGet(value){
      switch(value){
        case 0:return 'O_NONE';
        case 1:return 'O_FLIPV';
        case 2:return 'O_FLIPH';
        case 3:return 'O_FLIPVH';
        case 4:return 'O_RCW';
        case 5:return 'O_RCW_FLIPV';
        case 6:return 'O_RCW_FLIPH';
        case 7:return 'O_RCW_FLIPVH';
      }
    }
   
    function oOrientationSet(value){
      switch(value){
        case 'O_NONE':return 0;
        case 'O_FLIPV':return 1;
        case 'O_FLIPH':return 2;
        case 'O_FLIPVH':return 3;
        case 'O_RCW':return 4;
        case 'O_RCW_FLIPV':return 5;
        case 'O_RCW_FLIPH':return 6;
        case 'O_RCW_FLIPVH':return 7;
      }
    }
    var pointer=Module._malloc(72);
    var signature={cWidth:[0,'i32'],cHeight:[4,'i32'],
                   cfColorFormat:[8,'i32',colorformatGet,colorformatSet],
                   bdBitDepth:[12,'i32',bdBitDepthGet,bdBitDepthSet],
                   cBitsPerUnit:[16,'i32'],cLeadingPadding:[20,'i32'],
                   bRGB:[24,'i32'],cChromaCenteringX:[28,'i8'],
                   cChromaCenteringY:[29,'i8'],cROILeftX:[32,'i32'],cROIWidth:[36,'i32'],
                   cROITopY:[40,'i32'],cROIHeight:[44,'i32'],bSkipFlexbits:[48,'i32'],
                   cThumbnailWidth:[52,'i32'],cThumbnailHeight:[56,'i32'],
                   oOrientation:[60,'i32',oOrientationGet,oOrientationSet],
                   cPostProcStrength:[64,'i8'],fPaddedUserBuffer:[68,'i32']};

    BuildStruct(signature,this,pointer);
    
    this.free=function(){
      Module._free(pointer);
    }
    
    this.pointer=pointer;    
  }
  

  
  function ImageBufferInfo(pointer){
    //var struct=Runtime.generateStructInfo(['pv','cLine','cbStride','uiFirstMBRow','uiLastMBRow','cLinesDecoded'],"tagCWMImageBufferInfo");
    //console.log(struct);
    
    var pointer=Module._malloc(24);
                //Image pointer,Line count     ,width             ,0                      ,height/16
    var signature={pv:[0,'i32'],cLine:[4,'i32'],cbStride:[8,'i32'],
                   uiFirstMBRow:[12,'i32'],uiLastMBRow:[16,'i32'],
                   cLinesDecoded:[20,'i32']};
                   
    BuildStruct(signature,this,pointer);
    
    this.free=function(){
      Module._free(pointer);
    }
    
    this.pointer=pointer;
  }