function BitsPerUnit(bitdepth,channels){
  switch(bitdepth){
    case 'BD_8':
      return 8*channels;
    case 'BD_16':
    case 'BD_16S':
    case 'BD_16F':
      return 16*channels;
    case 'BD_32':
    case 'BD_32S':
    case 'BD_32F':
      return 32*channels;
    case 'BD_5': return 5*channels;
    case 'BD_10': return 10*channels;
    case 'BD_565': return 16;
  }


}

var console={
  log:function(value){
    postMessage({
      'action' : 'stdout',
      'line' : value
    });
  }
};

var Module = {

 // 'noInitialRun' : true,
  //'noFSInit' : true,
  print:function(value){
    postMessage({
      'action' : 'stdout',
      'line' : value
    });
  },
  logReadFiles:true,
  printErr:function(val){
    postMessage({
      'action' : 'stderr',
      'line' : val
    });
  },
};

importScripts('jxrlib.js');
importScripts('binder.js');


var ImageStrDecGetInfo=Module.cwrap('ImageStrDecGetInfo', 'number', ['number','number']);
var ImageStrDecInit=Module.cwrap('ImageStrDecInit','number',['number','number','number']);
var ImageStrDecDecode=Module.cwrap('ImageStrDecDecode','number',['number','number','number']);
var ImageStrDecTerm=Module.cwrap('ImageStrDecTerm','number',['number']);

onmessage = function(event) {
  var message=event.data;
 //       console.log(message.action);
  switch(message.action){
    case 'decode':
      
      var image={};
      image.data=new WMPStream(message.buffer);
      image.info=new CWMImageInfo();
      image.codec=new CWMIStrCodecParam();
      image.codec.WMPStream=image.data;
      image.postdecode=false;
      
      if(ImageStrDecGetInfo(image.info.pointer,image.codec.pointer)<0){
        postMessage({action:'info',id:message.id,ret:false,result:"Error"});
        return;
      }
      
      image.info.cThumbnailWidth=image.info.cWidth;
      image.info.cROIWidth=image.info.cWidth;
      image.info.cThumbnailHeight=image.info.cHeight;
      image.info.cROIHeight=image.info.cHeight;
      image.info.cBitsPerUnit=BitsPerUnit(image.info.bdBitDepth,image.codec.cChannel);

      //Convert the info and codec structures into something that can be passed by postMessage
      var meta=new Object;
      meta.info={};
      for(var k in image.info){
        if(k!='pointer'&&k!='free'){
          meta.info[k]=image.info[k];
        }
      }
      
      meta.codec={};
      for(var k in image.codec){
        if(k!='pointer'&&k!='free'&&k!='WMPStream'){
          meta.codec[k]=image.codec[k];
        }
      }

      var decoderPointer=Module._malloc(4);
      
      //Initalize the image decoding.
      if(ImageStrDecInit(image.info.pointer,image.codec.pointer,decoderPointer)<0){
        ImageStrDecTerm(decoder);
        Module._free(decoderPointer);
        postMessage({action:'decode',id:message.id,ret:false,result:"Error"});
        return;
      }

      var stride=((image.info.cBitsPerUnit+7)>>3)*image.info.cWidth;
      
      var linesPerMBRow=16;//How many lines are in macro block
      var LastMBRow=(image.info.cHeight+linesPerMBRow-1)/linesPerMBRow+1;
      
      //Emscripten memory alloc for output image buffer
      var pv=Module._malloc(stride*image.info.cHeight);
      
      var imagebuffer=new ImageBufferInfo();
      imagebuffer.cLine=image.info.cHeight;
      imagebuffer.cbStride=stride;

      imagebuffer.pv=pv;
      
      //Get the decode struct pointer that was set by ImageStrDecInit
      var decoder=Module.getValue(decoderPointer,'i32'); 
      var decodedLines=Module._malloc(4);
      var totalDecodedLines=0;
      
      var startTime=Date.now();
      console.log('LastMBRow:'+LastMBRow);
      for(var i=0;i<LastMBRow;i++){
        imagebuffer.uiFirstMBRow=i;
        imagebuffer.uiLastMBRow=i;
        ret=ImageStrDecDecode(decoder,imagebuffer.pointer,decodedLines);
        totalDecodedLines+=Module.getValue(decodedLines,'i32');
      }

      var endTime=Date.now();
      if(ret==0){
        meta.info.HeightPadding=image.info.cHeight-meta.info.cHeight;
        meta.info.WidthPadding=image.info.cWidth-image.info.cWidth;
        var imageData=Module.HEAPU8.buffer.slice(imagebuffer.pv,imagebuffer.pv+stride*image.info.cHeight);
        var t=new Uint8Array(imageData);

        
        
        postMessage({action:'decode',
                     id:message.id,
                     ret:true,
                     image:imageData,
                     meta:meta,
                     runtime:endTime-startTime});
        image.postdecode=true;
      }
      
      ImageStrDecTerm(decoder);
     
      Module._free(decodedLines);
      Module._free(decoderPointer);
      Module._free(imagebuffer.pv);
      
      imagebuffer.free();
      image.codec.free();
      image.info.free();
      image.data.free();
      break;
  }
}

postMessage({
  'action' : 'ready'
});